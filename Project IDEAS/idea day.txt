Idea 1 line :

AI powered Voice Programming IDE

Sector:
Software Development

Problem statement(What you are trying to solve?):

Coding is a tiring and time consuming process. Every innovative software's have its developers working behind them who spends a huge amount of time and handwork. This problem can be solved if we create a setup(Software-IDE) that can learn all the requirements of the user and generate the Application by  itself.

If this system is used, every single person on earth could be able to develop his/her software's for themselves even though they do not have any programming skills. The tool would behave like an excellent programmer and all we have to do is simply communicate with it and convey our software requirements.

This would definitely make a revelation in the software Industry.

Proposed solution:

The proposed System is an Artificial Intelligence powered Voice Coding IDE that allows its users to create Applications from the start to end easily without even writing a single line of code by hand. The IDE is intelligent enough to 
decide what the user wants and then generates codes along with the User interface(UI) automatically. The user will be able to define his requirements(functionalities) for the Applications though this voice and the IDE will automatically blend those feature into the Application one after the other. 

The best part is that people with little or no knowledge about programming skills could make a complete useful Applications in less than a day!. And for the people already having enough programming skills, they can take their software to any extreme levels.

Uniqueness of the project:

No one has ever used the benefits of Artificial Intelligence(AI) in Voice Coding. 
Presently there are some open source projects on Speech to code conversion, but it is mandatory that the person who uses these tools should have all the programming skills just like others who write the code with bare hands. 

After all these projects are based on simple voice to text and classification techniques(Dictation and commanding control system), which won't be useful in efficient coding and does not saves any of the programmers time.

Apparently there are no software/libraries that are used to Voice Code the Graphical User Interface(GUI) of any Application.

Presently using substitutes:

Almost every people writes the code by typing. This is definitely a very long process especially when the size of the project is big. Only a very small percentage of the people uses other techniques like Voice to Code conversion, where they could convert what they speak into its plain text form. The programmer have to pronounce every single tokens that they uses inside their code one after the other. 

Currently most of the people who uses the Voice to Code method are either because of the physically disabilities or for only testing purposes. 

In the present system a huge amount of times is wasted writing codes, which intern is a costly process.


Fund amt:

2 Lakh

Fund utilization:

Good quality Voice Recording devices are mandatory for the project.

A powerful system is required to test and run the IDE smooothly.

Hire some Software Developers when needed.

MileStones:

Voice Analysys & recognision
1 m
Basic Plugin Development
2 m
Prototype
1 m
AI integration
5 m

https://docs.google.com/forms/d/e/1FAIpQLScSinwkekmz3ZOXAfHrfGmRvYM7OZKuBirMn4fd8ZMKVTgiXQ/formResponse?edit2=2_ABaOnufUBHbqCmcDPXgzzpJIGr36oSBylm_SZwuqyJVpR_omnnbFSA4Jx8Ta9pA


https://stdntpartners-my.sharepoint.com/:p:/g/personal/febin_mathew_studentpartner_com/EVptGI98dHtDie6HpALP95sBtmy9uiNheKJR2rwB8ly1UA?e=JcvvpM

https://youtu.be/C3ZC2HW0fac

https://www.youtube.com/watch?v=C3ZC2HW0fac&feature=youtu.be


/*Learn special voice terms to give comands
/* does not saves much time and not usefull unless you have a typing desies or if you caan some sort od diabilities
Python Library 

2008
/* do not have to say more individual thing
Dragon fly(more directed towards programming)


/*dectation and commanding control system for windows, if you sat english (for ex when writing an email)
/* in coding have to say every individual things
Stand Alone
Dragon Natural Speech recognision 

https://voicecode.io/
