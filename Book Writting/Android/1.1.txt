<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools" android:layout_width="match_parent"
    android:layout_height="match_parent"  tools:context=".MainActivity">


    <LinearLayout
        android:orientation="horizontal"
        android:layout_width="match_parent"
        android:layout_height="match_parent"

        >

        
        <EditText android:id="@+id/edit_message"
            android:layout_width="odp"
            android:layout_height="wrap_content"
            android:hint="@string/edit_msg"
            android:layout_weight="1
            "/>
        <Button
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="Click Here"
            android:id="@+id/button"
            android:layout_weight="0"
             />
    </LinearLayout>
</RelativeLayout>

