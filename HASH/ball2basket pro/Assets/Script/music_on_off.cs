﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class music_on_off : MonoBehaviour, IPointerUpHandler, IPointerDownHandler{
public Sprite musicon;
public Sprite musicoff;
public static bool soundon;
public GameObject sound;
	// Use this for initialization
	void Start () {
	//soundon=true;
	}
	void Update()
	{
	Debug.Log(soundon);
	if(!soundon)
	{
	this.GetComponent<Image>().sprite=musicoff;
	GameObject.Find("bg").GetComponent<AudioSource>().mute= true;
	}
	if(soundon)
	{
this.GetComponent<Image>().sprite=musicon;
	GameObject.Find("bg").GetComponent<AudioSource>().mute= false;
	}

	}
	public virtual void OnPointerUp(PointerEventData ped)
	{
	if(soundon)
	{
	soundon=false;
	}
	else if(!soundon)
	{
	soundon=true;
	}
	
	}
	public virtual void OnPointerDown(PointerEventData ped)
	{
	Instantiate(sound);
	}
}
