﻿using UnityEngine;
using System.Collections;

public class gamescript : MonoBehaviour {

public GameObject objover;
public GameObject objwon;
private ballscript bscript; 
public float failcount,woncount,score;

	void Start () {
	objover.SetActive(false);
	objwon.SetActive(false);
	failcount=0;
	woncount=0;
	bscript=GameObject.Find("mainball").GetComponent<ballscript>();
	}
	
	// Update is called once per frame
	void Update () {

	score=bscript.score;

	if(bscript.failed)
	{
	failcount++;
	}
	if(bscript.won)
	{
	woncount++;
	}

	if(failcount>120)
	{
	objover.SetActive(true);
    }

	if(woncount>20)
	{
	objwon.SetActive(true);
    }


	

	}
}
