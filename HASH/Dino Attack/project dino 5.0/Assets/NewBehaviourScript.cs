﻿using UnityEngine;
using System.Collections;

public class NewBehaviourScript : MonoBehaviour {
private float cp=50;
private Animator anim;
	// Use this for initialization
	void Start () {
	anim = GetComponent<Animator>(); 
	}
	
	// Update is called once per frame
	void Update () {

	transform.Translate(Vector3.left*Time.deltaTime);
	
	}
	void OnTriggerEnter2D (Collider2D col)
	{
	if(col.tag == "bullet" )
		{
		cp--;
		if(cp<0){
		anim.SetBool("trig",true);
	    transform.rotation = Quaternion.Slerp(transform.rotation,Quaternion.Euler(new Vector3(0, 0, 70)),5);
		GetComponent<Rigidbody2D>().gravityScale=1;
	}}
	
	
	}
}
