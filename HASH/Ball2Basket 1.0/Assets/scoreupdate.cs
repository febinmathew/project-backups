﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class scoreupdate : MonoBehaviour {
private mainscript mainscript;

public Text high;
	// Use this for initialization
	void Start () {
		
	mainscript = GameObject.Find("scripts").GetComponent<mainscript>();
	}
	
	// Update is called once per frame
	void Update () {
	GetComponent<Text>().text =  mainscript.winscore.ToString();
	if(mainscript.winscore> PlayerPrefs.GetInt("highscore") )
	{
		PlayerPrefs.SetInt("highscore", (int)mainscript.winscore);
	}
	high.text = PlayerPrefs.GetInt("highscore").ToString();
	
	}
}
