﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class pausebutton : MonoBehaviour, IPointerUpHandler, IPointerDownHandler{
 public GameObject pausecanvas;
 private bool one,two;
 private mainscript mainscript;
 Vector3 defaults;
	// Use this for initialization
	void Start () {
		defaults=GetComponent<RectTransform>().localScale;
	one=false;
		two=false;
		 mainscript = GameObject.Find("scripts").GetComponent<mainscript>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public virtual void OnPointerUp(PointerEventData ped)
	{
		two=true;
		GetComponent<RectTransform>().localScale= defaults;//new Vector3(GetComponent<RectTransform>().localScale.x,GetComponent<RectTransform>().localScale.y,1);
	if(one && two)
	{
		pausecanvas.SetActive(true);
			 mainscript.gamepaused=true;
	}
	 
	
	}
	public virtual void OnPointerDown(PointerEventData ped)
	{
		one=true;
		GetComponent<RectTransform>().localScale= new Vector3(0.78f,0.78f,1);
	
}
}
