﻿using UnityEngine;
using System.Collections;

public class spawner : MonoBehaviour {
	
public GameObject ball;
private float rand,p,time;
public Sprite[] sprite = new Sprite[5];

	// Use this for initialization
	void Start () {
	time=Random.Range(0.0f, 2.0f);
	}
	
	// Update is called once per frame
	void Update () {
		
		time += Time.deltaTime;
		if(time>3)
		{
		time=Random.Range(0.0f, 1.5f);
		GameObject ballinstance = (GameObject)Instantiate(ball, new Vector3(transform.position.x + (Random.Range(-10f, 10f)),transform.position.y,0), transform.rotation);
		ballinstance.GetComponent<SpriteRenderer>().sprite = sprite[Random.Range(0, 4)];
		p = Random.Range(2.2f, 6f);
		ballinstance.GetComponent<Transform>().localScale = new Vector3(p, p, 0);
	    }
		
	}
}
