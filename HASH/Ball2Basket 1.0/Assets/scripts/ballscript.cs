﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ballscript : MonoBehaviour {
	
    public GameObject[] balls = new GameObject[4];
	public GameObject textfire;
    public Transform[] points = new Transform[6];
    public Transform[] steam = new Transform[2];
    float px , ps;
	private mainscript mainscript;
	public GameObject sound;
	public GameObject sound2;
	
	
	// Use this for initialization
	void Start () {
		
		//addscore.GetComponent<Text>()
			mainscript = GameObject.Find("scripts").GetComponent<mainscript>();
        px = 0;
	}
	
	// Update is called once per frame
	void Update () {
       //GetComponent<Rigidbody2D>().AddForce(new Vector2(-10, -10));
        
       // transform.Translate(Vector3.up * Time.deltaTime * 30);
       // Debug.Log(GetComponent<Rigidbody2D>().velocity);
	   if(transform.position.y < -10)
	   {Destroy(gameObject);
   mainscript.failscore += 1;}
       
	}

    void OnDestroy()
    {
        /*for (int i = 0; i < 4; i++)
        {
            GameObject sdf = (GameObject)Instantiate(balls[i], transform.position, transform.rotation);
           
        }*/
        
    }


    void OnTriggerEnter2D(Collider2D col)
    {//Debug.Log(mainscript.failscore + " " + mainscript.winscore+ " " + mainscript.level);
	//Debug.Log(mainscript.gamefailed);
        if (col.tag == "basketblock")
        {    
            mainscript.failscore += 1;
			mainscript.contiballs=0;
			if(PlayerPrefs.GetInt("vfx") == 1)
			{
			Instantiate(sound2, transform.position, transform.rotation);
			}
			
            for (int i = 0; i < 6; i++)
            { 
                float p=Random.Range(0f, 0.8f);
                GameObject sdf = (GameObject)Instantiate(balls[Random.Range(0, 3)], points[i].position, points[i].rotation);
                sdf.GetComponent<Transform>().localScale += new Vector3(p, p, 0);
                ps=Random.Range(-200f, 200f);
                while(ps-px <35 && ps-px >-35 )
                {
                ps=Random.Range(-250f, 250f);
                }
                sdf.GetComponent<Rigidbody2D>().AddForce(new Vector2(ps, Random.Range(150f, 400f)));
               // Debug.Log(ps);
                px=ps;

                
            }






            Destroy(gameObject);

        }

         
        if (col.tag == "wonblock")
        {  
	
	        mainscript.winscore += mainscript.scoreupdate;
			mainscript.contiballs +=1;
			 if(PlayerPrefs.GetInt("vfx") == 1)
			{
			Instantiate(sound, transform.position, transform.rotation);
			}
            for (int i = 0,j=0; i < 6; i++)
            {   float ass=Random.Range(100f, 400f);
                float p;
            if (ass > 260)
            {
                j = 1;
                 p= Random.Range(1.1f, 2.3f);
            }
            else
            {
                j = 0;
                 p= Random.Range(1.1f, 2.3f);
            }
                Transform sdf = (Transform)Instantiate(steam[j], points[i].position+new Vector3(Random.Range(-0.5f, +0.5f),0f,0), points[i].rotation);
                sdf.GetComponent<Transform>().localScale = new Vector3(p, p, 0);

            }
			
			//GameObject texts = (GameObject)Instantiate(textfire, transform.position, Quaternion.Euler(0, 0, 0));
            Destroy(gameObject);
        }

    }
}
