﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class mainscript : MonoBehaviour {
    public bool gamestat,gamepaused,gameplay,musicsound,vfxsound;
	 public GameObject pausecanvas,failcanvas;
    public float failscore,winscore,level,scoreupdate,bonusupdate,contiballs;
	 private Text score;
	 
	


	// Use this for initialization
	void Start () {
		contiballs =0;
	
		scoreupdate =1;
		bonusupdate=5;
		if(!(PlayerPrefs.HasKey("gamename")))
		{
		PlayerPrefs.SetString("gamename", "ball2basket");
		PlayerPrefs.SetInt("music", 1);
		PlayerPrefs.SetInt("vfx", 1);
		PlayerPrefs.SetInt("highscore", 0);
		}
	    pausecanvas.SetActive(false);
		failcanvas.SetActive(false);
		if(PlayerPrefs.GetInt("music") == 1)
		{
		musicsound=true;
		}
		else
		{
			musicsound=false;
	}
		if(PlayerPrefs.GetInt("vfx") == 1)
		{
	    vfxsound=true;
		}
		else
		{
			vfxsound=false;
		}
		
        gamestat = true;
		gamepaused =false;
        failscore = 0;
		winscore = 0;
		level =0;
		gameplay=true;
		score = GameObject.Find("totalscore").GetComponent<Text>();
		
	}
	void dataupdate()
	{
		
	}
	// Update is called once per frame
	void Update () {
		//Debug.Log(vfxsound);
		
		
		
		score.text = winscore.ToString();
		
		if (Input.GetKeyDown(KeyCode.Escape)) 
		{
			 if(pausecanvas.activeSelf == true)
			 {
			 pausecanvas.SetActive(false);
			 gamepaused=false;
			 }
		     else 
			 {
			 pausecanvas.SetActive(true);
			 gamepaused=true;
			 }
		}

			
		

        if (failscore >=6)
        {
			pausecanvas.SetActive(false);
         failcanvas.SetActive(true);
		 gamepaused=true;
        }
		else{
			
		}

         
	
	}
}
