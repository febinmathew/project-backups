﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class gunscript : MonoBehaviour {

    private mainscript mainscript;
    private float randdegree,randdegree2,tempcount, timevar1,deltatime,timevar2,gamespeed,real;
    public Animator anim;
    private Text timeleft;
    public float timeintervel;
	 public GameObject test;
	 public GameObject fireball;
    public Transform firepoint,firestart;
    public GameObject[] balls = new GameObject[9];
    int times;
	private bool setup,gameok;
	public GameObject sound;
	

	void Start () {
		//Debug.Log(test);
		setup=true;
        mainscript = GameObject.Find("scripts").GetComponent<mainscript>();
        timeleft =GameObject.Find("timer").GetComponent<Text>();
        randdegree= 360;
        gamespeed=4.0f;
        deltatime = 0.10f;
        timeintervel = 4;
        tempcount = 0;
        timevar1 = 0;
		timevar2 = 0;
		gameok=false;
        anim = GetComponent<Animator>();
       
	}
	
	// Update is called once per frame
	void Update () {
		if(!gameok)
		{
			gameok=true;
			float pp=(int)mainscript.winscore/5;
			gamespeed=4.0f-pp*0.3f;
			if(gamespeed<0.5f)
				gamespeed=0.5f;
			
			real=Random.Range(0.2f, gamespeed);
		    
		}
		//Debug.Log(Random.Range(0f, 8f));
		if(mainscript.contiballs > 5 && setup)
		{
			gamespeed = gamespeed/1.5f;
			setup=false;
			
		}
		if(mainscript.contiballs == 0)
		{
			
			setup=true;
			
		}
		
		if (mainscript.gamepaused == false)
           { 
	   
	   
       
        times = 4-(int)timevar2;
        timevar2 += Time.deltaTime;
		timevar1 += Time.deltaTime;
		
       
        //timevar2=0;
 //////////to be removed
         /* fireball = (GameObject)Instantiate(balls[4], firepoint.position, firepoint.rotation);
           anim.SetTrigger("fire");
		    
           Vector2 direction = firepoint.transform.position - firestart.transform.position;
           //fireball.GetComponent<Rigidbody2D>().velocity = new Vector2(-13, -(randdegree2));
           fireball.GetComponent<Rigidbody2D>().AddForce(direction * 1000);*/
		   //Debug.Log(timevar1+ " " +real);
		   if( tempcount == 0 && timevar1>real)
		   {
			   gameok=false;
           tempcount = 1;
		  
       ///////////to be removed 

           //timevar1 = 0;
           
               randdegree = Random.Range(290f, 340f);
               randdegree2 = randdegree;
               randdegree2 = (randdegree2-360)/5;
               if (randdegree >= 360)
               {   
                   randdegree -= 360;
               } 

               //tempcount = 0;
           
       }
       
      transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(new Vector3(0, 0, randdegree)), deltatime);
       timeleft.text = " ";
	   
	   
      
      if ((int)transform.eulerAngles.z == (int) randdegree && tempcount == 1)
      
      {    if(mainscript.contiballs <4)
		   {
           //GameObject 
		   fireball = (GameObject)Instantiate(balls[Random.Range(0, 5)], firepoint.position, firepoint.rotation);
          //GameObject 
		  //fireball = (GameObject)Instantiate(balls[4], firepoint.position, firepoint.rotation);
		  anim.SetTrigger("fire");
           Vector2 direction = firepoint.transform.position - firestart.transform.position;
           //fireball.GetComponent<Rigidbody2D>().velocity = new Vector2(-13, -(randdegree2));
		   if(PlayerPrefs.GetInt("vfx") == 1)
			{
			Instantiate(sound, transform.position, transform.rotation);
			}
           fireball.GetComponent<Rigidbody2D>().AddForce(direction *900);
           
		   }
		   
		   
		   else
		   {
           //GameObject 
		   
		  
		   fireball = (GameObject)Instantiate(balls[Random.Range(0, 9)], firepoint.position, firepoint.rotation);
	       
	   
          //GameObject 
		  //fireball = (GameObject)Instantiate(balls[4], firepoint.position, firepoint.rotation);
		  anim.SetTrigger("fire");
           Vector2 direction = firepoint.transform.position - firestart.transform.position;
           //fireball.GetComponent<Rigidbody2D>().velocity = new Vector2(-13, -(randdegree2));
		   if(PlayerPrefs.GetInt("vfx") == 1)
			{
			Instantiate(sound, transform.position, transform.rotation);
			}
           fireball.GetComponent<Rigidbody2D>().AddForce(direction *900);
           
		   }
		   timevar1=0;
		   tempcount=0;
       }
      
	  
	}}
}
