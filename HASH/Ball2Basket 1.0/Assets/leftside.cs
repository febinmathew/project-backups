﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class leftside : MonoBehaviour, IPointerUpHandler, IPointerDownHandler, IDragHandler{
public float diff;
bool press,take;
 private shuttlemovement shuttlemovement;
public GameObject basket;
private mainscript mainscript;

	// Use this for initialization
	void Start () 
	{  
	 mainscript = GameObject.Find("scripts").GetComponent<mainscript>();
	press= false;
	 shuttlemovement = basket.GetComponent<shuttlemovement>();
	diff=Time.deltaTime*6;
	}
	
	void FixedUpdate()
	{ if (press && mainscript.gamepaused == false)
           { 
		basket.GetComponent<Transform>().localPosition -= new Vector3(diff, 0, 0);
		}
	  }
	 
	public virtual void OnDrag(PointerEventData ped)
	{
if (mainscript.gamepaused == false)
           { 
		basket.GetComponent<Transform>().localPosition -= new Vector3(diff, 0, 0);
		}
	

	}
	public virtual void OnPointerUp(PointerEventData ped)
	{
		
	press=false;
	
	}
	public virtual void OnPointerDown(PointerEventData ped)
	{
		
		press=true;
	
}}