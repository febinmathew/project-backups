﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class joy2 : MonoBehaviour, IDragHandler,IPointerDownHandler,IPointerUpHandler{
	private bool stat,moveright,moveleft,down,up;
	private float curr,pre;
	
void Start () {
	if(PlayerPrefs.GetInt("music")==1)
	{stat = true;
}
else{
	stat = false;
}
	moveright = true;
	moveleft = true;
	
	}
	
	bool swap(bool stats)
	{
			return !stats;		
	}
	public virtual void OnPointerUp(PointerEventData ped)
	{
	up=true;
	if(up && down){
		up=false;
		down=false;
		stat=swap(stat);
	}
	}
	
	public virtual void OnPointerDown(PointerEventData ped)
	{
	down=true;
	}
	
	void Update () {
		Debug.Log(PlayerPrefs.GetInt("music"));
		if( stat )
		{
			transform.position = new Vector2(0.53f,transform.position.y);
			PlayerPrefs.SetInt("music", 1);
			
		}
	else 
	{
		transform.position = new Vector2(1.1f,transform.position.y);
		PlayerPrefs.SetInt("music", 0);
	}
	}
	
	void OnTriggerEnter2D (Collider2D col)

	{
	
	if(col.tag == "rightlimit")
	{
		//Debug.Log("r");
	moveright=false;
	moveleft=true;
	}
	else if(col.tag == "leftlimit" )
	{
		//Debug.Log("l");
	moveleft=false;
	moveright=true;
	}
	else{
	moveleft=true;
	moveright=true;
	}
	
	
	}
	
	public virtual void OnDrag(PointerEventData ped)
	{   
	     Vector2 pos;
         RectTransformUtility.ScreenPointToLocalPointInRectangle(GetComponent<RectTransform>(),ped.position, ped.pressEventCamera, out pos);
		 
		 if (GetComponent<RectTransform>().TransformPoint(pos).x >= transform.position.x && moveright)
		 {
			 calc(pos);
			 
		 
		 moveleft=true;
		 }
		 else if (GetComponent<RectTransform>().TransformPoint(pos).x <= transform.position.x && moveleft)
		 {
			calc(pos);
		 
		 moveright=true;
         	
		 }
	}
	
	public void calc(Vector2 pos)
	{
		curr=GetComponent<RectTransform>().TransformPoint(pos).x;
		if(Mathf.Abs(curr-pre) <= 0.3f)
		{
		transform.position = new Vector3(GetComponent<RectTransform>().TransformPoint(pos).x,transform.position.y,0);
		}
		pre=transform.position.x; 
	}
}
