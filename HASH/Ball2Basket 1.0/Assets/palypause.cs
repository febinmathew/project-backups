﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class palypause : MonoBehaviour ,IPointerDownHandler,IPointerUpHandler{
public GameObject pausemenu;
private mainscript mainscript;
private bool up,down;
	// Use this for initialization
	void Start () {
	mainscript = GameObject.Find("scripts").GetComponent<mainscript>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public virtual void OnPointerUp(PointerEventData ped)
	{
	up=true;
	if(up && down){
		up=false;
		down=false;
		pausemenu.SetActive(false);
		mainscript.gamepaused=false;
	}
	}
	
	public virtual void OnPointerDown(PointerEventData ped)
	{
	down=true;
	}
}
