﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class canvasscript : MonoBehaviour {

	public GameObject completePanel;
	public Text ScoreCompleteText,scoreText,timeText,highScoreText;
	public GameObject mainmenupanel;
	public static canvasscript instance;
	public int score;
	public int objectHitCount;
	float timeis;
	void setCompleteScoreTExt()
	{
		this.ScoreCompleteText.text = "Congratulations you gained "+this.score+" score";
	}

	public void setScoreTExt(int increment)
	{
		this.score += increment;
		this.objectHitCount++;
		this.scoreText.text = "Score : "+score;

		if (this.objectHitCount >= 10) {
			showGameOver ();
		}
	}
	// Use this for initialization
	void Start () {
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		this.resetData ();
		instance = this;
	}
	void resetData()
	{
		this.score = 0;
		this.objectHitCount = 0;
		this.setScoreTExt (score);
		this.objectHitCount = 0;
		this.timeText.text = "01 : 00"; 
		this.timeis = 0;
	}
	int remainingTime;

	// Update is called once per frame
	void Update () {
		this.timeis += Time.deltaTime;

		this.remainingTime = (int)(60 - timeis);
		if (!sphereScript.instance.pause) {
			if (remainingTime >= 0) {
				timeText.text = "00 : " + (remainingTime < 10 ? "0" + remainingTime : remainingTime + ""); 
			} else {
				showGameOver ();
			}
		}

	}

	public void playButtonClick()
	{
		this.mainmenupanel.SetActive (false);
		this.completePanel.SetActive (false);
		this.resetData ();
		this.scoreText.enabled = true;
		this.timeText.enabled = true;
		sphereScript.instance.setPause(false);
		sphereScript.instance.resetCubes();
	}
	public void mainMenuButtonClick()
	{
		this.completePanel.SetActive (false);
		this.scoreText.enabled = false;
		this.timeText.enabled = false;
		this.mainmenupanel.SetActive (true);

	}
	public void retryButtonClick()
	{
		playButtonClick ();
	}
	public void showGameOver()
	{
		if(remainingTime>0)
		score += remainingTime;
		
		setCompleteScoreTExt ();
		this.completePanel.SetActive (true);
		this.mainmenupanel.SetActive (false);
		sphereScript.instance.setPause(true);

		int highscore = PlayerPrefs.GetInt("highscore",0);
		if (highscore < score) {
			PlayerPrefs.SetInt("highscore",score);
			this.highScoreText.text="New highscore : "+score;
		}
		else if(highscore>0)
			this.highScoreText.text="Current highscore : "+highscore;
		else
			this.highScoreText.enabled=false;
	}
}
