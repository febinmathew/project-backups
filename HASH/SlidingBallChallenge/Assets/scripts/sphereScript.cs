﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sphereScript : MonoBehaviour {
	Rigidbody rigid;
	bool isFlat=false;
	public GameObject cubeParent;
	public static sphereScript instance;
	public bool pause;
	AudioSource audioHit;
	public Transform camera;
	Vector3 cameraInitialPosition;
	// Use this for initialization
	void Start () {
		cameraInitialPosition = camera.position;
		this.pause = true;
		instance = this;
		this.rigid = GetComponent<Rigidbody> ();
		this.audioHit = GetComponent<AudioSource> ();
	}

	public void resetCubes()
	{
		for (int i = 0; i < cubeParent.transform.childCount; i++) 
		{
			GameObject child = cubeParent.transform.GetChild(i).gameObject;

			int selectedChild = Random.Range (0, 10);
			Vector3 newPos = cubeParent.transform.GetChild (selectedChild).gameObject.transform.localPosition;
			cubeParent.transform.GetChild (selectedChild).gameObject.transform.localPosition = child.transform.localPosition;
			child.transform.localPosition = newPos;
			child.transform.localRotation = Quaternion.Euler (0,Random.Range (0, 360),0);
			child.SetActive (true);
		}
	}

	public void setPause(bool action)
	{
		this.pause=action;
	}


	// Update is called once per frame
	void Update () {
		camera.position = new Vector3 (camera.position.x, camera.position.y, cameraInitialPosition.z+(transform.position.z/2));
		Vector3 movement = new Vector3 (Input.acceleration.x, 0.0f, Input.acceleration.y);

		rigid.velocity = new Vector3 ( Mathf.Clamp(rigid.velocity.x,-5.0f,5.0f), Mathf.Clamp(rigid.velocity.y,-5.0f,5.0f),Mathf.Clamp(rigid.velocity.z,-5.0f,5.0f));
		if(!pause)
			rigid.AddForce(movement * 1000 * Time.deltaTime);
}

void OnCollisionEnter(Collision coll) {
	if (coll.gameObject.tag == "redbox") {
		this.audioHit.Play ();
		coll.gameObject.SetActive (false);
		canvasscript.instance.setScoreTExt (15);


	} else if (coll.gameObject.tag == "bluebox") {
		this.audioHit.Play ();
		coll.gameObject.SetActive (false);
		canvasscript.instance.setScoreTExt (20);

	} else
		return;

}
}
