package com.hash.around;

import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.hash.around.OtherClasses.DataTypes.MediaData;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.OtherClasses.Interfaces.JobCompletionWithFailureListner;
import com.hash.around.Views.RecentPhotoViewRail;
import com.hash.around.Views.SquareFrameLayout;
import com.hash.around.Views.ViewFunctions;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;

import static com.hash.around.GalleryActivity.getSelectionStringFromMediaType;

public class GallerySubDirectoryActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>{


    /*DatabaseReference newPostRef;
    ChildEventListener childEventListener;


    RecyclerView rView;
    Adapter_Contacts adap;
    ArrayList<UserWholeData> finaldata;
    private LinearLayoutManager mLayoutManager;

    ArrayList<ChatData> forwardchatData;

    boolean chatforwardActivity=false;
    boolean dataBackwardActivity=false;*/

    ArrayList<Integer> supportedMediaTypes=new ArrayList<>();
    public ArrayList<MediaData> generateDummy() {
        ArrayList<MediaData> list = new ArrayList<MediaData>();

        list.add(new MediaData());
        list.add(new MediaData());
        list.add(new MediaData());
        list.add(new MediaData());
        list.add(new MediaData());
        list.add(new MediaData());
        list.add(new MediaData());
        list.add(new MediaData());
        list.add(new MediaData());
        list.add(new MediaData());
        list.add(new MediaData());list.add(new MediaData());
        list.add(new MediaData());
        list.add(new MediaData());
        list.add(new MediaData());
        list.add(new MediaData());
        list.add(new MediaData());
        list.add(new MediaData());
        list.add(new MediaData());




        return list;
    }

    RecyclerView rView;
    RecyclerView.LayoutManager layoutManager;
    GalleryAdapter adap;
    ArrayList<MediaData> finaldata = new ArrayList<MediaData>();

    String BUCKET_NAME,USER_ID=null;
    Integer GALLERY_MEDIA_TYPE=0;
    FloatingActionButton contactFloatButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportedMediaTypes.add(1);
        supportedMediaTypes.add(3);
        supportedMediaTypes.add(5);
        setContentView(R.layout.galleryactivity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        USER_ID=getIntent().getStringExtra("userid");
        //if(USER_ID==null)
        BUCKET_NAME=getIntent().getStringExtra("bucket");
        GALLERY_MEDIA_TYPE=getIntent().getIntExtra("mediaType",0);
        Log.e("MGG","bucket name "+BUCKET_NAME);

        contactFloatButton = (FloatingActionButton) findViewById(R.id.openNativeGallery);
        contactFloatButton.setVisibility(View.GONE);
        contactFloatButton.setImageResource(R.drawable.ic_send_white);
        contactFloatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendDataToCaller();
            }
        });


        rView = (RecyclerView) findViewById(R.id.rView);
        rView.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(getApplicationContext(), 4,LinearLayoutManager.HORIZONTAL,false);
        //new GridLayoutManager()
        rView.setLayoutManager(layoutManager);

        //finaldata = generateDummy();


        adap = new GalleryAdapter(GallerySubDirectoryActivity.this, finaldata,clickedListner);
        rView.setAdapter(adap);
        //getData();

        if(USER_ID==null) {
            getLoaderManager().initLoader(0, null, this);
        }
        else
        {
            populateWithUserMedia(getApplicationContext(),USER_ID);
        }

    }
    static CursorLoader dbCursor;
    void populateWithUserMedia(final Context context, final String userid)
    {
        getLoaderManager().initLoader(1, null, new LoaderManager.LoaderCallbacks<Cursor>() {

            @Override
            public Loader<Cursor> onCreateLoader(int id, Bundle args) {

                //final String[] projections=new String[]{DatabaseAccesser.chat_col_media_id};
                final String selection=DatabaseAccesser.chat_userid+"=? AND ";
                final String[] selectionArgs=new String[]{userid};

                dbCursor= new CursorLoader( context, Uri.parse(DatabaseAccesser.chat_table), null, null, null, null )
                {
                    @Override
                    public Cursor loadInBackground()
                    {
                        // You better know how to get your database.
                        SQLiteDatabase DB = TabbedActivity.db.getReadableDatabase();
                        SQLiteDatabase DBW = TabbedActivity.db.getReadableDatabase();

                        return DB.rawQuery(
                                "SELECT * FROM "+DatabaseAccesser.chat_table +" INNER JOIN "+DatabaseAccesser.media_table+" ON "+
                                        DatabaseAccesser.chat_table+"."+DatabaseAccesser.chat_col_media_id+" = "+ DatabaseAccesser.media_table+".rowid"+ " WHERE " +
                                        DatabaseAccesser.chat_userid + " =? AND "+DatabaseAccesser.chat_col_media_id+" IN(?,?,?)", new String[] { userid,1+"",3+"",5+"" });
                        // You can use any query that returns a cursor.

                        /*Cursor cur= DB.query(DatabaseAccesser.chat_table,null, selection, selectionArgs, null, null, null, null );

                        String[] finalprojectionArgs=new String[cur.getCount()];
                        while(cur.moveToNext())
                        {

                            finalprojectionArgs[cur.getPosition()]=cur.getInt(
                                    cur.getColumnIndexOrThrow(DatabaseAccesser.chat_col_media_id))+"";
                        }
                        String finalprojection="rowid"+" IN("+makePlaceholders(finalprojectionArgs.length)+")";

                        Log.e("MGG","total medias from chat: "+ cur.getCount());

                        return DBW.query(DatabaseAccesser.media_table,null,finalprojection,finalprojectionArgs,null,null,null,null);*/
                    }
                };

                return dbCursor;
            }

            String makePlaceholders(int len)
            {
                if (len < 1) {
                    // It will lead to an invalid query anyway ..
                    throw new RuntimeException("No placeholders");
                } else {
                    StringBuilder sb = new StringBuilder(len * 2 - 1);
                    sb.append("?");
                    for (int i = 1; i < len; i++) {
                        sb.append(",?");
                    }
                    return sb.toString();
                }
            }

            @Override
            public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

                Log.e("MGG","total media received: "+data.getCount());
                finaldata.clear();


                while (data.moveToNext())
                {
                    String localUri = data.getString(data.getColumnIndexOrThrow(DatabaseAccesser.media_local_uri));
                    int mediaType=data.getInt(data.getColumnIndexOrThrow(DatabaseAccesser.chat_type));

                    if(!localUri.equals("null") && supportedMediaTypes.contains(mediaType))
                    {
                        //String mimetype = data.getString(4);

                        MediaData temp = new MediaData();
                        temp.uri = localUri;
                        temp.type = mediaType;

                        finaldata.add(temp);
                    }
                }

                adap.notifyDataSetChanged();
            }

            @Override
            public void onLoaderReset(Loader<Cursor> loader) {

            }
        });
    }

    void sendDataToCaller()
    {
        Intent broadcast = new Intent("around.NEWCHAT");
        broadcast.putExtra("mediaarray",finalSelection);
        sendBroadcast(broadcast);


        Intent intent3 = new Intent(this, ChatWindow.class);
        intent3.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent3);
    }

    Uri BASE_URL;
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        //Log.e("MGG","kola status ");
        //String[] projection = {MediaStore.Images.Media.DATA,MediaStore.Images.Media.BUCKET_ID,MediaStore.Images.Media.BUCKET_DISPLAY_NAME};


           String[] PROJECTION = {
                   MediaStore.Files.FileColumns._ID,
                   MediaStore.Files.FileColumns.DATA,
                   MediaStore.Files.FileColumns.DATE_ADDED,
                   MediaStore.Files.FileColumns.MEDIA_TYPE,
                   MediaStore.Files.FileColumns.MIME_TYPE,
                   MediaStore.Files.FileColumns.TITLE,
                   MediaStore.Images.Media.BUCKET_DISPLAY_NAME
           };

           String SELECTION = "(" + getSelectionStringFromMediaType(GALLERY_MEDIA_TYPE) + ")" + " AND "
                   + MediaStore.Images.Media.BUCKET_DISPLAY_NAME + "='" + BUCKET_NAME + "'";
           BASE_URL = MediaStore.Files.getContentUri("external");


           return new CursorLoader(this, BASE_URL, PROJECTION, SELECTION, null, MediaStore.Files.FileColumns.DATE_MODIFIED + " DESC");


    }


    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        //Log.e("MGG","kola status ");
        //Log.e("MGG","count is "+data.getCount());
        //ArrayList<MediaData> media=new ArrayList<>();
        finaldata.clear();
        String currentFolder="";
        String gifMime=MimeTypeMap.getSingleton().getMimeTypeFromExtension("gif");
        while (data.moveToNext())
        {
            long fileid = data.getLong(data.getColumnIndexOrThrow(MediaStore.Files.FileColumns._ID));
            String fileuri = data.getString(1);
            String folderName=data.getString(6);
            String mimetype=data.getString(4);

                MediaData temp=new MediaData();
                temp.folderName=folderName;
                temp.uri=Uri.withAppendedPath(BASE_URL, Long.toString(fileid)).toString();
                temp.type=Integer.parseInt(data.getString(3));
                if(mimetype.equals(gifMime))
                    temp.type=5;

                //Log.e("MGG",folderName+" =mime is "+mimetype+" single ton "+ );
                finaldata.add(temp);
        }
        //Log.e("MGG","size is is "+finaldata.size());
       // finaldata=media;
        adap.notifyDataSetChanged();

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    RecentPhotoViewRail.OnItemClickedListener clickedListner=new RecentPhotoViewRail.OnItemClickedListener() {
        @Override
        public void onItemClicked(MediaData uri) {
            loadUploadActivity(uri);
        }

        @Override
        public void onItemSelected(ArrayList<MediaData> uriList) {
            finalSelection=uriList;
            invalidateOptionsMenu();

        }
    };

    public ArrayList<MediaData> finalSelection=new ArrayList<>();

    void loadUploadActivity(MediaData url)
    {

        for (MediaData uuu:finalSelection) {
     /* MediaData dat=new MediaData();
      dat.uri=uuu.uri;
      mediaData.add(dat);*/
            Log.e("MGG","pushing attt "+uuu.type);
            Log.e("MGG","pushing value "+uuu.uri);
        }


        Intent uploadIntent = new Intent(this, UploadActivity.class);
        uploadIntent.putExtra("type", 6);
        uploadIntent.putExtra("uri", url.uri);
        uploadIntent.putExtra("mediaarray", finalSelection);
        uploadIntent.putExtra("forChat", 1);


        uploadIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(uploadIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(finalSelection.size()<1)
        {
            menu.clear();
            if(contactFloatButton.getVisibility()==View.VISIBLE)
            ViewFunctions.independantAnimateOut(contactFloatButton,this);
        }
        else
        {
            getMenuInflater().inflate(R.menu.gallery_menu,menu);
            ViewFunctions.independantAnimateIn(contactFloatButton,this);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.action_cancel_send:

                finalSelection.clear();
                adap.selectedItems.clear();
                adap.notifyDataSetChanged();
                invalidateOptionsMenu();
                break;
            case R.id.action_send:
                sendDataToCaller();
                break;
        }

        return super.onOptionsItemSelected(item);

    }

    class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {

        Context mContext;
        ArrayList<MediaData> data;

        ArrayList<MediaData> selectedItems=new ArrayList<>();

        RecentPhotoViewRail.OnItemClickedListener listener;

        public GalleryAdapter(Context mContext, ArrayList<MediaData> data, RecentPhotoViewRail.OnItemClickedListener listener) {
            this.mContext = mContext;
            this.data = data;
            this.listener=listener;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recent_photo_view_item, parent, false);

            //v.getLayoutParams().height = (int) getResources().getDimension(R.dimen.gallery_items);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            if(selectedItems.contains(data.get(position)))
                holder.layout.setChecked(true);
            else
                holder.layout.setChecked(false);


            Uri uri=Uri.parse(data.get(position).uri);
            //Log.e("MGG","uri NOWW " + uri);
            Glide.with(mContext).load(uri)
                    .thumbnail(0.05f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(holder.imageView);
            //holder.windowTag.setVisibility(View.GONE);

            holder.layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                       // Intent intent=new Intent(GalleryActivity.this,);
                        //startActivity(intent);

                }
            });


            if(data.get(position).type==MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO) {
                holder.typeIndicator.setVisibility(View.VISIBLE);
                holder.typeIndicator.setImageResource(R.drawable.ic_video_white);
            }
            else if(data.get(position).type==5) {
                holder.typeIndicator.setVisibility(View.VISIBLE);
                holder.typeIndicator.setImageResource(R.drawable.ic_gif_white);
            }
            else
                holder.typeIndicator.setVisibility(View.GONE);

            holder.layout.setJobListner(new JobCompletionWithFailureListner() {
                @Override
                public void onJobFailed(String txt1) {
                    holder.layout.setChecked(false);
                    selectedItems.remove(data.get(position));
                    updateSendButtonStatus();

                    if (listener != null) listener.onItemSelected(selectedItems);

                }

                @Override
                public void onJobCompletion(String txt1) {
                    holder.layout.setChecked(true);
                    selectedItems.add(data.get(position));
                    updateSendButtonStatus();
                    if (listener != null) listener.onItemSelected(selectedItems);

                }
            });

            holder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("MGG","clicking NOW ");
                    holder.layout.setChecked(true);
                    if(!selectedItems.contains(data.get(position)))
                        selectedItems.add(data.get(position));


                    if (listener != null)
                    {
                        listener.onItemSelected(selectedItems);
                        listener.onItemClicked(data.get(position));

                    }

                }
            });

        }




        void updateSendButtonStatus()
        {
           /* if(selectedItems.size()==1)
            {
                if(sendButtonListner!=null)
                    sendButtonListner.onJobCompletion("");}
            else if(selectedItems.size()==0)
            { if(sendButtonListner!=null)
                sendButtonListner.onJobFailed("");}*/
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            ImageView imageView;
            //TextView windowTag;
            SquareFrameLayout layout;
            ImageView typeIndicator;

            //public Drawable temp=null;
            public ViewHolder(View item) {
                super(item);
                this.imageView = itemView.findViewById(R.id.thumbnail);
                //this.windowTag=itemView.findViewById(R.id.windowTag);
                this.layout=itemView.findViewById(R.id.wholeView);
                this.layout.doubleSelctionSize(2);
                this.typeIndicator=itemView.findViewById(R.id.typeIndicator);


            }
        }
    }
}