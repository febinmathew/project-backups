package com.hash.around.Utils;

import android.content.Context;
import android.text.format.Time;
import android.util.Log;

import com.hash.around.R;

import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Created by Febin on 6/13/2018.
 */

public class StringUtils {

    public static String randomStringGenerator()
    {
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for(
                int i = 0;
                i< 20;i++)

        {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();

    }

    static String[] memoryRepresentation=new String[]{"B","KB","MB","GB"};

    public static String mediaSizeToString(long size)
    {
        int count=0;
        while(size>1024)
        {
            count++;
            size= size/1024;
        }
        return size+" "+memoryRepresentation[count];

    }

    public static String generateVcardFromNumber(String name,String mob)
    {
        /*String sample="BEGIN:VCARD\n" +
                "VERSION:3.0\n" +
                "N:Ihrd;Adrash;Jio;;\n" +
                "FN:Adrash Jio Ihrd\n" +
                "item1.TEL;waid=917012153009:+91 70121 53009\n" +
                "item1.X-ABLabel:Mobile\n" +
                "END:VCARD";*/

        String vCard=new String();
        vCard+="BEGIN:VCARD\nVERSION:3.0\n";

        String[] names=name.split(" ");

        vCard+="N:;"+name+";;;\n";
        vCard+="FN:"+name+"\n";
        vCard+="item1.TEL;waid=+"+mob.replaceAll("\\+","")+":"+mob+"\n";
        vCard+="END:VCARD";
        Log.e("MGG","vcard is "+vCard);
        return vCard;


    }

    public static String getNameFromVcard(String vCard)
    {

        return vCard.split("FN:")[1].split("\n")[0];
    }

    public static String getShortExpirationDisplayValue( int expirationTime)
    {
        if (expirationTime <= 0) {
            return "0s";
        } else if (expirationTime < TimeUnit.MINUTES.toSeconds(1)) {
            return  expirationTime+"s";
        } else if (expirationTime < TimeUnit.HOURS.toSeconds(1)) {
            int minutes = expirationTime / (int)TimeUnit.MINUTES.toSeconds(1);
            return minutes+"m";
        } else if (expirationTime < TimeUnit.DAYS.toSeconds(1)) {
            int hours = expirationTime / (int)TimeUnit.HOURS.toSeconds(1);
            return hours+"h";
        } else if (expirationTime < TimeUnit.DAYS.toSeconds(7)) {
            int days = expirationTime / (int)TimeUnit.DAYS.toSeconds(1);
            return days+"d";
        } else {
            int weeks = expirationTime / (int)TimeUnit.DAYS.toSeconds(7);
            return weeks+"w";
        }
    }

    public static String getShortExpirationDisplayValueForTimestamp( long expirationTime)
    {
        return getShortExpirationDisplayValue((int)(expirationTime-System.currentTimeMillis())/1000);
    }

    public static String getExpirationDisplayValue(Context context, int expirationTime) {
        if (expirationTime <= 0) {
            return "off";
        } else if (expirationTime < TimeUnit.MINUTES.toSeconds(1)) {
            return context.getResources().getQuantityString(R.plurals.expiration_seconds, expirationTime, expirationTime);
        } else if (expirationTime < TimeUnit.HOURS.toSeconds(1)) {
            int minutes = expirationTime / (int)TimeUnit.MINUTES.toSeconds(1);
            return context.getResources().getQuantityString(R.plurals.expiration_minutes, minutes, minutes);
        } else if (expirationTime < TimeUnit.DAYS.toSeconds(1)) {
            int hours = expirationTime / (int)TimeUnit.HOURS.toSeconds(1);
            return context.getResources().getQuantityString(R.plurals.expiration_hours, hours, hours);
        } else if (expirationTime < TimeUnit.DAYS.toSeconds(7)) {
            int days = expirationTime / (int)TimeUnit.DAYS.toSeconds(1);
            return context.getResources().getQuantityString(R.plurals.expiration_days, days, days);
        } else {
            int weeks = expirationTime / (int)TimeUnit.DAYS.toSeconds(7);
            return context.getResources().getQuantityString(R.plurals.expiration_weeks, weeks, weeks);
        }
    }

    /*public static String getBriefRelativeTimeSpanString(final Context c, final Locale locale, final long timestamp) {
        if (isWithin(timestamp, 1, TimeUnit.MINUTES)) {
            return c.getString(R.string.DateUtils_just_now);
        } else if (isWithin(timestamp, 1, TimeUnit.HOURS)) {
            int mins = convertDelta(timestamp, TimeUnit.MINUTES);
            return c.getResources().getString(R.string.DateUtils_minutes_ago, mins);
        } else if (isWithin(timestamp, 1, TimeUnit.DAYS)) {
            int hours = convertDelta(timestamp, TimeUnit.HOURS);
            return c.getResources().getQuantityString(R.plurals.hours_ago, hours, hours);
        } else if (isWithin(timestamp, 6, TimeUnit.DAYS)) {
            return getFormattedDateTime(timestamp, "EEE", locale);
        } else if (isWithin(timestamp, 365, TimeUnit.DAYS)) {
            return getFormattedDateTime(timestamp, "MMM d", locale);
        } else {
            return getFormattedDateTime(timestamp, "MMM d, yyyy", locale);
        }
    }*/

    public static boolean isToday(long when) {
        Time time = new Time();
        time.set(when);

        int thenYear = time.year;
        int thenMonth = time.month;
        int thenMonthDay = time.monthDay;

        time.set(System.currentTimeMillis());
        return (thenYear == time.year)
                && (thenMonth == time.month)
                && (thenMonthDay == time.monthDay);
    }
    public  static boolean isYesterday(final long when) {
        return isToday(when + TimeUnit.DAYS.toMillis(1));
    }
    public static String convertMediaTypeToString(int type)
    {
        String lastmessage_type="";
        switch(type) {
            case 1:
                lastmessage_type = "Image";
                break;
            case 2:
                lastmessage_type = "Audio";
                break;
            case 3:
                lastmessage_type = "Video";
                break;
            case 4:
                lastmessage_type = "Location";
                break;
            case 5:
                lastmessage_type = "Gif";
                break;
            case 6:
                lastmessage_type = "File";
                break;
            case 7:
                lastmessage_type = "Contact";
                break;

        }
        return  lastmessage_type;
    }
}
