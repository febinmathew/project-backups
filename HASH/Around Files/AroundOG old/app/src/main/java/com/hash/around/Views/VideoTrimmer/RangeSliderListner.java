package com.hash.around.Views.VideoTrimmer;

/**
 * Created by Febin on 5/26/2018.
 */

public interface RangeSliderListner {
    void onCreate(AroundRangeSlider rangeSeekBarView, int index, float value);

    void onSeek(AroundRangeSlider rangeSeekBarView, int index, float value);

    void onSeekStart(AroundRangeSlider rangeSeekBarView, int index, float value);

    void onSeekStop(AroundRangeSlider rangeSeekBarView, int index, float value);

    void onRangeChange(AroundRangeSlider rangeSeekBarView,float index,float value);
}

