package com.hash.around.OtherClasses.DataTypes;

/**
 * Created by Febin on 26-08-2017.
 */
import java.io.Serializable;

public class ChatListData implements  Serializable {

    public String userid;
    public String name;
    public int msgFromMe;
    public String lastMessage;
    public int lastMessageSeen;   //NEW ENTRY
    public long rowId;     //NEW ENTRY
    public int lastMessagetype;
    public int type;
    public int chat_pinned;
    public int chat_hidden;
    public long timestamp;
    public int msgQueue;
    public String url;
}



