package com.hash.around.OtherClasses.Interfaces;

import android.location.Location;

public interface LocationReceiver {

    void  onLocationReceived(Location data);
}


