package com.hash.around;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.hash.around.OtherClasses.DataTypes.ContactListData;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.OtherClasses.DataTypes.GroupMemberData;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.vanniktech.emoji.EmojiEditText;

import java.util.ArrayList;
import java.util.Random;



import static com.hash.around.Profile_Activity.customNotificationSender;
import static com.hash.around.TabbedActivity.saveContactToDatabase;
import static com.hash.around.TabbedActivity.userdetail;


public class CreateNewGroup extends AppCompatActivity {
    Toolbar toolbar;
    View root;
    EmojiEditText name;
    ImageView emoticon;
    CircularImageView circleProfileImage;
    ImageButton chooseButton;
    FloatingActionButton completeButton;
    boolean locationError;
    Uri directory = null;

    private StorageReference mStorageRef;
    DatabaseReference databaseRef;
    ArrayList<String> groupMemberList;

    boolean readyForCreation=false;

    //public SharedPreferences prefs;
   // public SharedPreferences.Editor editor;
    //double latitute;
   // double longitude;
   // String locationName;
    StorageReference riversRef;



  /*  void saveGroupToDatabase(ContactListData post,String globalUrl)
    {
        SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();


        ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.user_user_id, post.user_id);
        values.put(DatabaseAccesser.user_name, post.user_name);
        values.put(DatabaseAccesser.user_status, post.status);
        if(post.user_id.equals(userdetail.getUid()))
        {
            values.put(DatabaseAccesser.user_user_type, 0);
        }
        else
        {
            values.put(DatabaseAccesser.user_user_type, post.type);
        }

        values.put(DatabaseAccesser.user_globalUrl,globalUrl);
        values.put(DatabaseAccesser.user_localUrl, post.img_url);



        long newRowId = db.insert(DatabaseAccesser.user_table, null, values);
    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_newgroup_activity);
        locationError = true;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
       // prefs = getSharedPreferences("user_preferences", MODE_PRIVATE);
       // editor = prefs.edit();

        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Create New Group");
        chooseButton = (ImageButton) findViewById(R.id.imageButton1);

        circleProfileImage = (CircularImageView) findViewById(R.id.circular_image);
        name = (EmojiEditText) findViewById(R.id.profilenametext);

        root = (RelativeLayout) findViewById(R.id.rootrelative_profilesetup);
        emoticon = (ImageView) findViewById(R.id.emotimg);


       // actions = new EmojIconActions(getApplicationContext(), root, name, emoticon);
        // actions.ShowEmojIcon();
        emoticon.setImageResource(R.drawable.ic_emoticon_semiblack);

        databaseRef = FirebaseDatabase.getInstance().getReference();

        mStorageRef = FirebaseStorage.getInstance().getReference();
        riversRef = mStorageRef.child("profile_images").child(randomStringGenerator() + ".png");


        completeButton = (FloatingActionButton) findViewById(R.id.completeButton);
        completeButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                if(!readyForCreation)
                {
                    Intent intent = new Intent(getApplicationContext(), AllUsersList.class);
                    //Bundle args = new Bundle();
                    //args.putSerializable("ARRAYLIST",(Serializable)chatsArray);
                    //intent.putExtra("BUNDLE",args);
                    intent.putExtra("returnData",true);
                    // intent.putExtra("chatsArray",chatsArray);

                    startActivityForResult(intent,2);
                }
                else {
                    if (directory != null) {
                        Log.e("MSGG","ready for image upload");
                        riversRef.putFile(directory).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                Log.e("MSGG","scuccess image upload");



                                groupMemberList.add(userdetail.getUid());

                                final Uri downloadUrl = taskSnapshot.getDownloadUrl();

                                final ContactListData map = new ContactListData();
                                map.user_name = name.getText().toString();
                                map.type = 15;
                                map.img_url = downloadUrl.toString();
                                map.status = "null";


                                DatabaseReference groupRef = databaseRef.child("user_profile").push();
                                final String key = groupRef.getKey();

                                map.user_id = key;


                                groupRef.setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            map.user_id = key;
                                            map.img_url = directory.toString();
                                            Log.e("MSGG","data success upload");
                                            saveContactToDatabase((UserWholeData) map, downloadUrl.toString(), true);

                                            //movetoMemberSelection();
                                            uploadGroupMemberDatas(key,new JobCompletionListner(){
                                                @Override
                                                public void onJobCompletion(String txt1) {
                                                    map.img_url=directory.toString();
                                                    Log.e("MSGG","updating chatlist");
                                                    updateChatlistWithGroup(map,key);
                                                    finish();
                                                }
                                            });

                                        }
                                    }
                                });



                               //





                            }
                        })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception exception) {
                                        Toast.makeText(CreateNewGroup.this, exception.getMessage() + exception.toString(), Toast.LENGTH_LONG).show();

                                    }
                                });


                    } else {
                        Toast.makeText(CreateNewGroup.this, "Please Choose an image", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

        chooseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickFromGallery();
            }
        });

        circleProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickFromGallery();
            }
        });




    }

    void uploadGroupMemberDatas(String key,JobCompletionListner listner)
    {
        Log.e("MSGG","upload members");
        DatabaseReference groupChatRef = databaseRef.child("chat_group").child(key).child("group_members");

       final SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();

        listner.onJobCompletion("null");
        for (int i=0;i<groupMemberList.size();i++)
        {
            GroupMemberData memberData= new GroupMemberData();
           final ContentValues values = new ContentValues();

            values.put(DatabaseAccesser.groupsmembers_group_id,key);
            values.put(DatabaseAccesser.groupsmembers_user_id, groupMemberList.get(i));
            memberData.userid=groupMemberList.get(i);
            if(userdetail.getUid().equals(groupMemberList.get(i))) {
                values.put(DatabaseAccesser.groupsmembers_member_type, 1);
                memberData.member_type=1;

                MainServiceThread.mainService.listentoSingleGroupChats(key, 0);
            }
            else {
                values.put(DatabaseAccesser.groupsmembers_member_type, 0);
                memberData.member_type=0;
                customNotificationSender(groupMemberList.get(i),15,key,null);
            }



            groupChatRef.child(groupMemberList.get(i)).setValue(memberData).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    long  newRowId = db.insert(DatabaseAccesser.groupsmembers_table, null, values);
                    Log.e("MSG","insert hre : "+newRowId);
                }
            });



        }
    }

    void updateChatlistWithGroup(ContactListData contact,String groupID)
    {
        SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();

        ContentValues values2 = new ContentValues();
        values2.put(DatabaseAccesser.chatList_lastmsg, "You created the group.");
        values2.put(DatabaseAccesser.chatList_lastmsg_type, 0);
        values2.put(DatabaseAccesser.chatList_lastmsgbyme, 0);
        values2.put(DatabaseAccesser.chatList_msgqueue, 0);
        values2.put(DatabaseAccesser.chatList_lastmsgRowId, 0);
        values2.put(DatabaseAccesser.chatList_last_msgseen, 0);
        values2.put(DatabaseAccesser.chatList_name, contact.user_name);
        values2.put(DatabaseAccesser.chatList_userid, groupID);
        values2.put(DatabaseAccesser.chatList_timestamp, System.currentTimeMillis());
        values2.put(DatabaseAccesser.chatList_url, contact.img_url);

        int rowln=db.update(DatabaseAccesser.chatList_table,values2, DatabaseAccesser.chatList_userid+" =?",new String[]{groupID});
        long srowln=0;
        if(rowln==0)
            srowln = db.insert(DatabaseAccesser.chatList_table, null, values2);

        Log.e("MSG",rowln+" "+srowln);

        Intent new_intent = new Intent();
        new_intent.setAction("CHAT_UPDATED");
        sendBroadcast(new_intent);
    }


    public void movetoMemberSelection() {
        //Intent intent = new Intent(this, TabbedActivity.class);
        finish();
        //startActivity(intent);
    }

    public String randomStringGenerator() {
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0;i < 20; i++)
        {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == RESULT_OK) {
            try {
                directory = data.getData();

                circleProfileImage.setImageURI(directory);

            } catch (Exception e) {
                Toast.makeText(this, e.getMessage() + e.toString(), Toast.LENGTH_LONG).show();
            }

        }

        else if (requestCode == 2 && resultCode == RESULT_OK) {
            try {
                Bundle args = data.getBundleExtra("BUNDLE");
                if(args!=null)

                {
                    groupMemberList = (ArrayList<String>) args.getSerializable("ARRAYLIST");

                    if(groupMemberList.size()>0)
                    {
                        readyForCreation=true;
                        Toast.makeText(this,"got total: "+groupMemberList.size() , Toast.LENGTH_SHORT).show();

                    }
                    else
                    {
                        Toast.makeText(this,"Ath least 1 member required" , Toast.LENGTH_SHORT).show();

                    }

                }

            } catch (Exception e) {
                Toast.makeText(this, e.getMessage() + e.toString(), Toast.LENGTH_LONG).show();
            }

        }


    }


    private void pickFromGallery() {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Not granted", Toast.LENGTH_SHORT).show();

                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.INTERNET}, 11);

                return;
            }


        }
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, "GET FILE"), 1);


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 10:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //locationok();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                           // locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0, listner);

                        }
                    }
                }
                break;
            case 11:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //locationok();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                                checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                                checkSelfPermission(Manifest.permission.INTERNET) == PackageManager.PERMISSION_GRANTED) {


                        }
                    }
                }
                break;

        }
    }




    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        //toggleGPSUpdates();
                    }
                });
        dialog.show();
    }



}