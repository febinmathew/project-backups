package com.hash.around.TabFragments;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.hash.around.ChatWindow;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.OtherClasses.DataTypes.FeedDataList;
import com.hash.around.OtherClasses.Interfaces.LocationReceiver;
import com.hash.around.OtherClasses.Interfaces.ProcessCompletedListner;
import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.hash.around.Profile_Activity;
import com.hash.around.R;
import com.hash.around.OtherClasses.Adapter_FeedData;
import com.hash.around.OtherClasses.DataTypes.FeedData;
import com.hash.around.TabbedActivity;
import com.hash.around.UploadActivity;
import com.hash.around.Utils.DatabaseManager;
import com.hash.around.Utils.MediaManager;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import static android.app.Activity.RESULT_OK;
import static com.hash.around.TabbedActivity.AROUND_FEED_RADIUS;
import static com.hash.around.TabbedActivity.UserName;
import static com.hash.around.TabbedActivity.fDatabase;
import static com.hash.around.TabbedActivity.fetchFusedLocation;

import static com.hash.around.TabbedActivity.mainStorageReference;
import static com.hash.around.TabbedActivity.prefs;
import static com.hash.around.TabbedActivity.userdetail;

public class TabFragment2 extends Fragment {
    public static double latitude, longitude;

    ProgressDialog progDialog;
    RecyclerView rView;
    CardView cView;
    static String key;
    LocationManager locationManager;
    LocationListener listner;
    private LinearLayoutManager mLayoutManager;
    Adapter_FeedData adap;
    ArrayList<FeedData> finaldata;
    ArrayList<FeedDataList> finaldatalist;
    SwipeRefreshLayout swipeRefreshFeedList;
    GeoFire geoFire;
    TextView locationText;
    GeoQuery geoQuery;
    ValueEventListener postListener;
    static String haxa = " ";
    LinearLayout noFeedsAvailable;

    public static Context mTab2Context;
    private BroadcastReceiver feedReciever = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            //Toast.makeText(getApplicationContext(), "received message in activity..!", Toast.LENGTH_SHORT).show();
            fillDataFromDatabase();
            adap.notifyDataSetChanged();

        }
    };

    //DatabaseReference newPostRef;
    /* public ArrayList<FeedData> generateDummy()
     {
         ArrayList<FeedData> list=new ArrayList<>();

         FeedData x=new FeedData();
         x.id=1;
         x.userid=1;
         x.text="Check out this game 99 Miles Runner!! When I first played it, I felt it like a grumpy game, Now it is what I love the most <3 <3 <3";
         x.imgDir="danial";
         x.type=1;
         list.add(x);

         x=new FeedData();
         x.id=4;
         x.userid=2;
         x.text="Accedent in the town.. Any one with O+ve blood? HURRY !";
         x.imgDir="acce";
         x.type=0;
         list.add(x);




         x=new FeedData();
         x.id=1;
         x.userid=1;
         x.text="Time on marine drive!!!....fun with besties #fun #around_mood #gamersconnect :)";
         x.imgDir="profile";
         x.type=2;
         list.add(x);

         x=new FeedData();
         x.id=3;
         x.userid=1;
         x.text="Kid playing Blue whale on the PC ! WOW! Kinda Funny!";
         x.imgDir="blue";
         x.type=0;
         list.add(x);


         x=new FeedData();
         x.id=2;
         x.userid=2;
         x.text="Time on marine drive!!!....fun with besties #fun #around_mood #gamersconnect :)";
         x.imgDir="profile";
         x.type=0;
         list.add(x);



         x=new FeedData();
         x.id=2;
         x.userid=2;
         x.text="Check out this game 99 Miles Runner!! When I first played it, I felt it like a grumpy game, Now it is what I love the most <3 <3 <3";
         x.imgDir="danial";
         x.type=2;
         list.add(x);

         x=new FeedData();
         x.id=4;
         x.userid=2;
         x.text="Accedent in the town.. Any one with O+ve blood? HURRY !";
         x.imgDir="acce";
         x.type=0;
         list.add(x);



         return list;



     }*/
    public String randomStringGenerator() {
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (
                int i = 0;
                i < 20; i++)

        {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.feed_list_fragment, container, false);
        //TextView textView = (TextView) rootView.findViewById(R.id.section_label);
        //textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
        rView = (RecyclerView) rootView.findViewById(R.id.rView);
        rView.setHasFixedSize(true);
        //cView=(CardView)rootView.findViewById(R.id.cView);
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);
        //  mLayoutManager=new GridLayoutManager(this,2,LinearLayoutManager.HORIZONTAL,false);
        // mLayoutManager=new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);

        rView.setLayoutManager(mLayoutManager);
        progDialog = new ProgressDialog(getActivity());
        finaldata = new ArrayList<FeedData>();
        finaldatalist = new ArrayList<FeedDataList>();
        noFeedsAvailable=(LinearLayout) rootView.findViewById(R.id.noFeedsAvailable);
        /*if(true)
        {
            noFeedsAvailable.setVisibility(View.VISIBLE);
        }*/
        locationText = (TextView) rootView.findViewById(R.id.locationStatus);
        adap = new Adapter_FeedData(getActivity(), finaldata, finaldatalist, rView);
        rView.setAdapter(adap);
        fillDataFromDatabase();
        //Toast.makeText(getActivity(), "total size : "+ finaldata.size(), Toast.LENGTH_SHORT).show();
        registerForContextMenu(rView);

        mTab2Context=getActivity();

        adap.setOnLoadMoreListener(new ProcessCompletedListner() {
            @Override
            public void processCompleteAction() {

            }

            @Override
            @Nullable
            public void newUserAdded(UserWholeData data) {

            }


        });


        //adap.notifyDataSetChanged();


        rootView.findViewById(R.id.upload_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_DEFAULT);
                startActivityForResult(Intent.createChooser(intent, "GET FILE"), 5);

            }
        });

        rootView.findViewById(R.id.upload_video).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("video/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_DEFAULT);
                startActivityForResult(Intent.createChooser(intent, "GET FILE"), 6);

            }
        });


        rootView.findViewById(R.id.upload_gif).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/gif");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                startActivityForResult(Intent.createChooser(intent, "GET FILE"), 7);
            }
        });


        rootView.findViewById(R.id.upload_text).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent uploadIntent = new Intent(getActivity().getApplicationContext(), UploadActivity.class);
                uploadIntent.putExtra("type", 4);
                startActivityForResult(uploadIntent, 15);
            }
        });


        swipeRefreshFeedList = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshFeedList);
        swipeRefreshFeedList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //swipeRefreshFeedList.setRefreshing(true);

                //geoQuery.removeAllListeners();
                updateContent();


                swipeRefreshFeedList.setRefreshing(false);
               /* (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshFeedList.setRefreshing(false);

                        int min = 65;
                        int max = 95;
                        Random random = new Random();
                        int i =random.nextInt(max-min+1)+min;
                        //textView.setText(String.valueOf(i));



                    }
                },3000);*/
            }
        });


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    && getActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                //Toast.makeText(this, "Not granted", Toast.LENGTH_SHORT).show();
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 10);


                return rootView;

            }

        }

        updateContent();
        IntentFilter intentFilter = new IntentFilter("FEED_UPDATED");
//Map the intent filter to the receiver
        getActivity().registerReceiver(feedReciever, intentFilter);


        //setHasOptionsMenu(true);
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        // updateContent();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().unregisterReceiver(feedReciever);
//        geoQuery.removeAllListeners();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 10:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //locationok();
                    updateContent();
                }
                break;
        }
    }


    void updateContent() {


        //Double lat = Double.parseDouble(prefs.getString("location_latitude", "0.0"));
        //Double lng = Double.parseDouble(prefs.getString("location_longitude", "0.0"));
        //String loc_name = prefs.getString("location_name", "Choose a location!");


        ArrayList<Location> source=new ArrayList<>();

        SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();

        Cursor cursor1 = db.rawQuery(" select * from " + DatabaseAccesser.location_table + " where " +
                DatabaseAccesser.loc_col_status + " = ? ", new String[]{"1"});
        if(cursor1.getCount()<1)
        {
            cursor1.close();
            return;
        }
        else
        {
            while (cursor1.moveToNext()) {

                Location temp=new Location("source");
                temp.setLatitude(cursor1.getDouble(
                        cursor1.getColumnIndexOrThrow(DatabaseAccesser.loc_col_latitude)));
                temp.setLongitude(cursor1.getDouble(
                        cursor1.getColumnIndexOrThrow(DatabaseAccesser.loc_col_longitude)));

                source.add(temp);
                Intent intent = new Intent("GET_FEED_UPDATED");
                intent.putExtra("lat", temp.getLatitude() + "");
                intent.putExtra("lng", temp.getLongitude() + "");
                getActivity().sendBroadcast(intent);

            }
        }
        cursor1.close();

        locationText.setText(source.size()+" locations");

        fillDataFromDatabase();




    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.tabbed_feed_window, menu);
    }


    public void grabImage(View v) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, "GET FILE"), 5);
       /* CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);

                */

    }

    public void grabVideo(View v) {
        Intent intent = new Intent();
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, "GET FILE"), 6);

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {


        int position = -1;
        String selectedUser = "null";
        String usersName = "null";
        position = Adapter_FeedData.optionPosition;
        selectedUser = adap.data.get(position).userid;
        //Toast.makeText(getActivity(), selectedUser, Toast.LENGTH_SHORT).show();
        usersName = adap.data.get(position).username;
        //adap.data.get(optionPosition)
       /* try {

        } catch (Exception e) {
            //Log.d(TAG, e.getLocalizedMessage(), e);

        }*/
        switch (item.getItemId()) {
            case R.id.view_profile:
                Intent indi = new Intent(getActivity().getApplicationContext(), Profile_Activity.class);
                indi.putExtra("userid", selectedUser);
                startActivity(indi);
                break;
            case R.id.comment_now:
                // do your stuff
                Intent indi2 = new Intent(getActivity().getApplicationContext(), ChatWindow.class);
                indi2.putExtra("userid", selectedUser);
                indi2.putExtra("username", usersName);
                startActivity(indi2);
                break;
            case R.id.delete_post:
                // Toast.makeText(getActivity().getApplicationContext(), "ffofofof", Toast.LENGTH_SHORT).show();
                long timestamp = adap.data.get(position).timestamp;
                deleteFeedPosts(selectedUser, timestamp);
                // do your stuff
                break;
            case R.id.report_post:
                // do your stuff
                break;
            case R.id.block_post:
                // do your stuff
                break;
        }
        //Toast.makeText(getActivity().getApplicationContext(), ""+position, Toast.LENGTH_SHORT).show();


        return super.onContextItemSelected(item);
    }

    void deleteFeedPosts(final String userId, final long timestamp) {

        // Context tempContext=getActivity().getApplicationContext();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Delete feed?");
        // builder.setTitle("Delete feed?");
        builder.setCancelable(true);
        builder.setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        if (userId.equals(TabbedActivity.userdetail.getUid())) {
            builder.setPositiveButton("DELETE FEED", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    deleteFeedLocal(userId, timestamp);
                    dialog.dismiss();
                }
            });
        }
        builder.setNegativeButton("DELETE FROM PHONE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteFeedLocal(userId, timestamp);

                dialog.dismiss();
               /* processcompleted(false);
                Toast.makeText(Process_Activity.this, "Press Back Again To Exit", Toast.LENGTH_SHORT).show();*/

            }
        });


        AlertDialog alert = builder.create();
        //alert.getButton(2).setEnabled(false);
        alert.show();


    }

    void deleteFeedLocal(String userId, long timestamp) {
        SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();
        db.delete(DatabaseAccesser.feed_table, DatabaseAccesser.feed_col_user_id + "=?",
                new String[]{userId});
        //String query = "DELETE FROM " + DatabaseAccesser.feed_table + " WHERE " + DatabaseAccesser.feed_col_user_id + " = ? ";
        //Cursor cursor = db.rawQuery(query, new String[]{userId});
        //finaldata.remove(Adapter_FeedData.optionPosition);
        //adap.notifyItemChanged(Adapter_FeedData.optionPosition);
        fillDataFromDatabase();
        adap.notifyDataSetChanged();
        //Toast.makeText(getActivity(), "cursor amount "+ cursor.getCount(), Toast.LENGTH_SHORT).show();

    }

    public void grabGif(View v) {
        Intent intent = new Intent();
        intent.setType("image/gif");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, "GET FILE"), 7);

    }

    private String getRealPathFromURI(Context c, Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(c, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 5 && resultCode == RESULT_OK) {

            //Toast.makeText(this, "Working", Toast.LENGTH_SHORT).show();

            Uri directory = data.getData();
            //Toast.makeText(getActivity(), directory.getEncodedPath(), Toast.LENGTH_SHORT).show();
            //  Toast.makeText(getActivity(), directory.getAuthority(), Toast.LENGTH_SHORT).show();
            //Toast.makeText(getActivity(), data.getType(), Toast.LENGTH_SHORT).show();
          /* if( directory.toString().substring(directory.toString().lastIndexOf(".") + 1)=="jpg") {
               try {
               Bitmap bmp = BitmapFactory.decodeFile(directory.getPath());
               File file = new File(Environment.getExternalStorageDirectory().getPath(),"Around Files");
               if(!file.exists())
               {
                   file.mkdirs();
               }
               //File tempFile = new File(file.getAbsolutePath(),"contact.vcf");



                   FileOutputStream outStream = new FileOutputStream(new File(file.getAbsolutePath(),"temp.png"));
                  boolean success=bmp.compress(Bitmap.CompressFormat.PNG, 100, outStream);

                   directory=Uri.parse(file.toURI().toString()+"temp.png");
                   outStream.flush();


                   outStream.close();


               } catch (FileNotFoundException e) {
                   e.printStackTrace();
               }
               catch (IOException e) {
                   e.printStackTrace();
               }
           }*/
            Intent uploadIntent = new Intent(getActivity().getApplicationContext(), UploadActivity.class);
            uploadIntent.putExtra("type", 5);
            uploadIntent.putExtra("uri", directory.toString());
            startActivityForResult(uploadIntent, 15);


            //final Uri outputUri = Uri.fromFile(new File(getCacheDir(), "EditFile.png"));
            // Crop.of(directory, outputUri).start(this);
            /*UCrop.of(directory,outputUri)
                    .start(TabbedActivity.this);*/


            /*CropImage.activity(directory)
                    .start(this);*/

        }
        if (requestCode == 6 && resultCode == RESULT_OK) {

            //Toast.makeText(this, "Working", Toast.LENGTH_SHORT).show();

            final Uri directory = data.getData();
            //Bitmap bmp = BitmapFactory.decodeFile(directory.getEncodedPath());

            Intent cropIntent = new Intent(getActivity().getApplicationContext(), UploadActivity.class);
            cropIntent.putExtra("type", 6);
            cropIntent.putExtra("uri", directory.toString());
            startActivityForResult(cropIntent, 15);
        }
        if (requestCode == 7 && resultCode == RESULT_OK) {

            //Toast.makeText(this, "Working", Toast.LENGTH_SHORT).show();

            final Uri directory = data.getData();

            Intent cropIntent = new Intent(getActivity().getApplicationContext(), UploadActivity.class);
            cropIntent.putExtra("type", 7);
            cropIntent.putExtra("uri", directory.toString());
            startActivityForResult(cropIntent, 1);
        }


        if (requestCode == 15 && resultCode == RESULT_OK) {
            // Toast.makeText(getActivity().getApplicationContext(), "Got Data", Toast.LENGTH_SHORT).show();

            String feed_text = data.getStringExtra("text");
            final String uri = data.getStringExtra("uri");
            String place = data.getStringExtra("locname");

            int type = data.getIntExtra("type", 0);
            int visi = data.getIntExtra("visibility", 4);
            long timestamp = System.currentTimeMillis();

            final FeedData x = new FeedData();

            x.userid = userdetail.getUid();
            x.username = UserName;
            x.location = place;
            x.timestamp = timestamp;
            x.visibility = visi;
            x.type = type;
            x.text = feed_text;
            x.ifseen = 1;
            //x.uri = uri;

            //String tempUri=uri;
            // progDialog.setTitle("Uploading");
            //progDialog.setMessage("Uploading data");
            //progDialog.show();


            if (uri.equals("null") || type == 0) {
                x.uri = "1";
                uploadContent(x);
                savePostToDatabase(x);

                fillDataFromDatabase();

            } else if (type == 1 || type == 2) {

                boolean bitmapcreated;
                Bitmap bitmap = null;
                Bitmap videoBitmap = null;


                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                //bitmap.compress(Bitmap.CompressFormat.PNG, 1, bytes);
                //final byte[] datas = bytes.toByteArray();

                //bitmap2= BitmapFactory.decodeByteArray(datas, 0, datas.length);

                //bitmap2=getResizedBitmap(bitmap,10);
                //bytes = new ByteArrayOutputStream();

                if (type == 1) {
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), Uri.parse(uri));
                        bitmapcreated = true;
                    } catch (IOException e) {
                        e.printStackTrace();
                        bitmapcreated = false;
                    }
                    //Log.e("MSG",bitmapcreated+" : bitmap");
                    getResizedBitmap(bitmap, 80).compress(Bitmap.CompressFormat.PNG, 1, bytes);
                } else if (type == 2) {

                    String semi_uri = data.getStringExtra("semi_uri");
                    //data.getByte
                    try {
                        videoBitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), Uri.parse(semi_uri));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    getResizedBitmap(videoBitmap, 80).compress(Bitmap.CompressFormat.PNG, 1, bytes);
                }

                final byte[] datas2 = bytes.toByteArray();


                String name = randomStringGenerator() + ".png";
                StorageReference sRef = mainStorageReference.child("feed_datas").child(userdetail.getUid()).child(name);
                final StorageReference sRef2 = mainStorageReference.child("feed_datas").child(userdetail.getUid()).child(randomStringGenerator() + ".png");
                sRef.putFile(Uri.parse(uri)).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                        if (task.isSuccessful()) {
                            final String path = task.getResult().getDownloadUrl().toString();


                            sRef2.putBytes(datas2).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> taskSnapshot) {
                                    if (taskSnapshot.isSuccessful()) {
                                        x.semi_uri = taskSnapshot.getResult().getDownloadUrl().toString();
                                        x.uri = path;
                                        uploadContent(x);
                                        long size;
                                        try {
                                            size =MediaManager.getMediaSize(getContext(),Uri.parse(uri));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            size=0;
                                        }
                                        x.uri= DatabaseManager.insertMediaToDatabase(x.uri, uri, x.semi_uri, "null",size )+"";

                                        savePostToDatabase(x);

                                        fillDataFromDatabase();
                                    } else {
                                        // Failed
                                    }
                                }
                            });


                            //locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0, listner);

                            //adds location


                        } else {
                            //progDialog.dismiss();
                            Toast.makeText(getActivity(), "Data could not be uploaded! Try Again", Toast.LENGTH_SHORT).show();
                        }
                    }
                });


            }


            //x.uri = uri;
            //x.semi_uri = "null";





        }


    }



    public static void sharePost(final FeedData feedData,Activity activity) {

        fetchFusedLocation(new LocationReceiver() {


            @Override
            public void onLocationReceived(Location data) {



            feedData.userid = TabbedActivity.userdetail.getUid();
                feedData.timestamp = System.currentTimeMillis();
                feedData.ifseen = 1;
                feedData.username = UserName;


                latitude = data.getLatitude();
                longitude = data.getLongitude();

                if (feedData.type == 0) {
                    feedData.uri = "null";
                    uploadContent(feedData);

                } else if (feedData.type == 1 || feedData.type == 2) {
                    SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();
                    Cursor cursor2 = db.query(DatabaseAccesser.media_table,
                            new String[]{
                                    DatabaseAccesser.media_global_uri,
                                    DatabaseAccesser.media_local_uri,
                                    DatabaseAccesser.media_lite_global_uri,
                                    DatabaseAccesser.media_lite_local_uri,
                            },
                            DatabaseAccesser.media_lite_local_uri+ " =?",
                            new String[]{feedData.semi_uri},
                            null, null, null,null);
                    if(cursor2.getCount()==1) {
                        cursor2.moveToNext();

                        feedData.uri= cursor2.getString(
                                cursor2.getColumnIndexOrThrow(DatabaseAccesser.media_global_uri));

                        feedData.semi_uri= cursor2.getString(
                                cursor2.getColumnIndexOrThrow(DatabaseAccesser.media_lite_global_uri));

                        uploadContent(feedData);
                    }



                }

            }
        },activity);



    }

    public static Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public static void uploadContent(FeedData x) {

        switch (x.visibility) {

            case 0:

            case 1:
                DatabaseReference newPostRef = fDatabase.child("feed_stack").push();
                //x.uri = path;
                newPostRef.setValue(x);
                key = newPostRef.getKey();

                DatabaseReference ref = FirebaseDatabase.getInstance().getReference("feeds_location");
                GeoFire geoFire = new GeoFire(ref);
                geoFire.setLocation(key, new GeoLocation(latitude, longitude), new GeoFire.CompletionListener() {
                    @Override
                    public void onComplete(String key, DatabaseError error) {
                        if (error != null) {
                            Toast.makeText(mTab2Context, "Error writing location", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(mTab2Context, "Success location", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                break;
            case 2:
                //contacts  only
                break;
            case 3:

                // only me
                break;

        }


    }

    private double meterDistanceBetweenPoints(float lat_a, float lng_a, float lat_b, float lng_b) {
        float pk = (float) (180.f / Math.PI);

        float a1 = lat_a / pk;
        float a2 = lng_a / pk;
        float b1 = lat_b / pk;
        float b2 = lng_b / pk;

        double t1 = Math.cos(a1) * Math.cos(a2) * Math.cos(b1) * Math.cos(b2);
        double t2 = Math.cos(a1) * Math.sin(a2) * Math.cos(b1) * Math.sin(b2);
        double t3 = Math.sin(a1) * Math.sin(b1);
        double tt = Math.acos(t1 + t2 + t3);

        return 6366000 * tt;
    }

    /*private double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return (dist);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }*/

boolean checkDistanceToAllLocation(ArrayList<Location> source,Location destination)
{
    for(int l=0;l<source.size();l++)
    {
        if((source.get(l).distanceTo(destination) / 1000)<=AROUND_FEED_RADIUS)
            return true;
    }
    return false;
}



    void fillDataFromDatabase() {

        getFeedsFromDatabase();
        if(finaldata.size()==0)
        {
            noFeedsAvailable.setVisibility(View.VISIBLE);
        }

    }
    void getFeedsFromDatabase() {
        finaldata.clear();
        finaldatalist.clear();
        SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();
        //DatabaseReference ref = FirebaseDatabase.getInstance().getReference("feeds_location");
        //geoFire = new GeoFire(ref);
       /*Cursor cursor = db.query(DatabaseAccesser.feed_table, new String[]{DatabaseAccesser.feed_col_user_id,
                        DatabaseAccesser.feed_col_username,
                        DatabaseAccesser.feed_col_timestamp, DatabaseAccesser.feed_col_type, DatabaseAccesser.feed_col_text,
                        DatabaseAccesser.feed_col_semi_url, DatabaseAccesser.feed_col_url},
                null,
                null,
                null, null, null,null);*/

        Double lat = Double.parseDouble(prefs.getString("location_latitude", "0.0"));
        Double lng = Double.parseDouble(prefs.getString("location_longitude", "0.0"));

        ArrayList<Location> source=new ArrayList<>();



        Cursor cursor1 = db.rawQuery(" select * from " + DatabaseAccesser.location_table + " where " +
                DatabaseAccesser.loc_col_status + " = ? ", new String[]{"1"});
        if(cursor1.getCount()<1)
        {
            cursor1.close();
            return;
        }
        else
        {
            while (cursor1.moveToNext()) {

                Location temp=new Location("source");
                temp.setLatitude(cursor1.getDouble(
                        cursor1.getColumnIndexOrThrow(DatabaseAccesser.loc_col_latitude)));
                temp.setLongitude(cursor1.getDouble(
                        cursor1.getColumnIndexOrThrow(DatabaseAccesser.loc_col_longitude)));

                source.add(temp);
            }
        }
        cursor1.close();

        Toast.makeText(getActivity().getApplicationContext(), source.size()+" - locations "+source.get(0), Toast.LENGTH_SHORT).show();

      //  LatLngBounds

        //double cc=Location.distanceBetween((double)source.getLatitude(),(double)source.getLongitude(),(double)destination.getLatitude(),(double)destination.getLongitude());
        //double b=meterDistanceBetweenPoints((float)source.getLatitude(),(float)source.getLongitude(),(float)destination.getLatitude(),(float)destination.getLongitude());

        //Toast.makeText(getActivity(), "dist: "+a+" / "+b+" / ", Toast.LENGTH_LONG).show();

        Cursor cursor=db.rawQuery(" select CAST(count(*) AS INTEGER) AS "+DatabaseAccesser.count_value+","+
                DatabaseAccesser.feed_col_user_id + ", " +
                DatabaseAccesser.feed_col_username + ", "+
                DatabaseAccesser.feed_col_timestamp + ", "+
                DatabaseAccesser.feed_col_type + ", "+
                DatabaseAccesser.feed_col_seen + ", "+
                DatabaseAccesser.feed_col_text + ", "+
                DatabaseAccesser.feed_col_media_id + ", "+
                //DatabaseAccesser.feed_col_url + ", "+
                DatabaseAccesser.feed_col_latitude + ", " +
                DatabaseAccesser.feed_col_longitude + " from " +
                DatabaseAccesser.feed_table + " group by "+DatabaseAccesser.feed_col_user_id+","+DatabaseAccesser.feed_col_seen + " order by "+DatabaseAccesser.feed_col_timestamp, null);
       haxa=" ";

        ArrayList<String> arrString=new ArrayList<String>();
        ArrayList<String> arrInt=new ArrayList<String>();

        while (cursor.moveToNext()) {
            FeedData temp = new FeedData();
            FeedDataList temp2 = new FeedDataList();

            temp.userid = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_user_id));
            temp.username = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_username));
            temp.timestamp = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_timestamp));
            temp.type = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_type));

            temp.ifseen = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_seen));

            temp.text = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_text));

            long type = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_media_id));
            if(type==1)
            {
                temp.semi_uri=temp.uri="null";
            }
            else
            {

                Cursor cursor2 = db.query(DatabaseAccesser.media_table,
                        new String[]{
                                "rowid,"+
                                DatabaseAccesser.media_global_uri,
                                DatabaseAccesser.media_local_uri,
                                DatabaseAccesser.media_lite_global_uri,
                                DatabaseAccesser.media_lite_local_uri,
                        },
                        "rowid "+ " =?",
                        new String[]{type+""},
                        null, null, null,null);
                if(cursor2.getCount()==1)
                {
                    cursor2.moveToNext();

                    temp.uri = cursor2.getString(
                            cursor2.getColumnIndexOrThrow(DatabaseAccesser.media_local_uri));
                    if(temp.uri.equals("null"))
                    {
                        temp.uri = cursor2.getString(
                                cursor2.getColumnIndexOrThrow(DatabaseAccesser.media_global_uri));
                    }

                    temp.semi_uri = cursor2.getString(
                            cursor2.getColumnIndexOrThrow(DatabaseAccesser.media_lite_local_uri));

                   /* Log.e("MSGG","value OFFF media: "+cursor2.getString(
                            cursor2.getColumnIndexOrThrow(DatabaseAccesser.media_lite_global_uri)));*/



                }
               // Log.e("MSGG","complted" );

            }

            Log.e("MSGG","FIRST: "+ temp.uri +" value : "+temp.text );

            double latitude = cursor.getDouble(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_latitude));
            double longitude = cursor.getDouble(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_longitude));

            int count = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.count_value));


            Location destination = new Location("destination");
            destination.setLatitude(latitude);
            destination.setLongitude(longitude);


            //float distance = ;



            temp2.seen = 0;
            temp2.unseen = 0;
            if (checkDistanceToAllLocation(source,destination)) {

            if (temp.ifseen == 0) {
                if (arrString.contains(temp.userid)) {
                    int pos = arrString.indexOf(temp.userid);
                    int c = Integer.parseInt(arrInt.get(pos));
                    int x = finaldatalist.get(c).seen;
                    temp2.seen = x;
                    finaldatalist.get(c).seen = 0;
                    finaldatalist.get(c).unseen = 0;
                    arrString.remove(pos);
                    arrInt.remove(pos);
                    //finaldata.remove();
                    //get final list data
                    //finaldatalist.remove();
                    temp2.unseen = count;
                    finaldata.add(temp);
                    finaldatalist.add(temp2);
                } else {
                    temp2.unseen = count;
                    finaldata.add(temp);
                    finaldatalist.add(temp2);
                    arrString.add(temp.userid);
                    arrInt.add((finaldatalist.indexOf(temp2) + ""));
                }
            } else {
                if (arrString.contains(temp.userid)) {
                    int c = Integer.parseInt(arrInt.get(arrString.indexOf(temp.userid)));
                    finaldatalist.get(c).seen = count;
                    //update final list data
                } else {
                    temp2.seen = count;
                    finaldata.add(temp);
                    finaldatalist.add(temp2);
                    arrString.add(temp.userid);
                    arrInt.add((finaldatalist.indexOf(temp2) + ""));
                }
            }

        }


        }
        //Toast.makeText(getActivity(), finaldata.size()+"/"+finaldatalist.size(), Toast.LENGTH_SHORT).show();
        for(int i=finaldatalist.size()-1;i>=0;i--)
        {
            if(finaldatalist.get(i).unseen==0&&finaldatalist.get(i).seen==0)
            {
                finaldatalist.remove(i);
                finaldata.remove(i);
            }
        }
        cursor.close();

        adap.notifyDataSetChanged();



    }

    void savePostToDatabase(FeedData post)
    {
        SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();

        //  Toast.makeText(getActivity(), "Now saving to database", Toast.LENGTH_SHORT).show();
        ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.feed_col_user_id, post.userid);
        values.put(DatabaseAccesser.feed_col_username, post.username);
        values.put(DatabaseAccesser.feed_col_timestamp, post.timestamp);
        values.put(DatabaseAccesser.feed_col_type, post.type);
        values.put(DatabaseAccesser.feed_col_seen, 1);
        values.put(DatabaseAccesser.feed_col_text, post.text);
        values.put(DatabaseAccesser.feed_col_media_id, post.uri);


        long newRowId = db.insert(DatabaseAccesser.feed_table, null, values);



    }
}


