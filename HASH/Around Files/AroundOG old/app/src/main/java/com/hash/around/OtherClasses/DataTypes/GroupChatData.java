package com.hash.around.OtherClasses.DataTypes;

import java.io.Serializable;

public class GroupChatData extends ChatData implements Serializable{

   public long chat_rece_time=0;
   public long chat_seen_time=0;
   public long chatRow;
   public String name;
   public int sender;
   public int msgseen=0;
   public long media_id;

   public GroupChatData()
   {}
   public GroupChatData(GroupChatData dat)
   {
      super(dat);
      this.name=dat.name;
      this.sender=dat.sender;
      this.msgseen=dat.msgseen;
      this.media_id=dat.media_id;

      this.chatRow=dat.chatRow;
   }
   public GroupChatData(int sender, int type, String msg, String temp_url, String final_url, int msgseen, long timestamp)
   {
      super(sender, type, msg, temp_url, final_url, msgseen, timestamp);

   }

}
