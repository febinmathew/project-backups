package com.hash.around;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.OtherClasses.Interfaces.ProcessCompletedListner;
import com.hash.around.Utils.CountryListLoader;

import java.util.ArrayList;

public class CountriesListActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<ArrayList<CountriesListActivity.CountryData>>{


    @Override
    public Loader<ArrayList<CountryData>> onCreateLoader(int id, Bundle args) {

        return new CountryListLoader(CountriesListActivity.this);
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<CountryData>> loader, ArrayList<CountryData> data) {
        Log.e("MGG","received data size "+data.size() );
        finaldata.clear();
        finaldata.addAll(data);

        //adap.set
        adap.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<CountryData>> loader) {

    }

    public static class CountryData
    {
        public String country_name;
        public String country_nickName;
        public int country_code;
    }

    RecyclerView rView;
    Adapter_Country adap;
    ArrayList<CountryData> finaldata=new ArrayList<>();
    private LinearLayoutManager mLayoutManager;
    ProcessCompletedListner processListner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.countries_list_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        rView=(RecyclerView)findViewById(R.id.rView);
        rView.setHasFixedSize(true);

        mLayoutManager=new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,true);

        mLayoutManager.setStackFromEnd(true);
        rView.setLayoutManager(mLayoutManager);

        processListner=new ProcessCompletedListner() {
            @Override
            public void processCompleteAction() {

            }

            @Override
            public void newUserAdded(UserWholeData data) {
                Intent result = getIntent();
                result.putExtra("country_name", data.user_name);
                result.putExtra("country_code", data.chatType);
                setResult(RESULT_OK, result);
                finish();
            }
        };

        adap=new Adapter_Country(CountriesListActivity.this,finaldata,processListner);
        rView.setAdapter(adap);


        getSupportLoaderManager().initLoader(0, null, this).forceLoad();





    }


    void getPhoneDeatils()
    {
        /*int count=0;
        Locale[] locales = Locale.getAvailableLocales();
        ArrayList<String> countries = new ArrayList<String>();
        for (Locale locale : locales) {

            String country = locale.getDisplayCountry();
            if (country.trim().length()>0 && !countries.contains(country)) {
                count++;
                countries.add(country);
                Log.e("MGG","country: "+country+" nick: "+locale.getCountry()+" code: ");
            }

        }
        Log.e("MGG","region count: "+count);*/



        //ArrayList< Map<String, String>> results=new ArrayList<>();


    }







    String phoneNumberValidation(String num)
    {
        return num.replaceAll("[()\\s-]+","");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.country_list_menu, menu);
        MenuItem sItem=menu.findItem(R.id.action_search_alluser);
        SearchView searchView =
                (SearchView) sItem.getActionView();
        sItem.expandActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // return false;
               // Toast.makeText(AllUsersList.this, "Submit- "+query, Toast.LENGTH_SHORT).show();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //Toast.makeText(AllUsersList.this, "Change- "+newText, Toast.LENGTH_SHORT).show();
                searchOnList(newText);
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);

    }

    void searchOnList(String searchTerm)
    {
        if(searchTerm.equals(""))
        {
            adap.updateDataList(finaldata);
           adap.notifyDataSetChanged();
            rView.scrollToPosition(0);
        return;
        }

        ArrayList<CountryData> temp=new ArrayList<>();
        for(int i=0;i<finaldata.size();i++)
        {
           if(finaldata.get(i).country_name.toLowerCase().contains(searchTerm.toLowerCase()))
            {
                temp.add(finaldata.get(i));
            }
            else
            {

            }
        }
        adap.updateDataList(temp);
        adap.notifyDataSetChanged();
        rView.scrollToPosition(0);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_new_group)
        {

        }

        return super.onOptionsItemSelected(item);
    }


    private class Adapter_Country extends RecyclerView.Adapter<CountryHolder> {

        Context mContext;
        ArrayList<CountryData> data;



        Snackbar snack;
        ProcessCompletedListner listner;


        public Adapter_Country(Context mcont,  ArrayList<CountryData> list,ProcessCompletedListner listn)
        {
            mContext=mcont;
            data=list;
            this.listner=listn;

        }

        public void updateDataList( ArrayList<CountryData> list)
        {
            data=list;
        }

        @Override
        public CountryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v;

            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.single_country_view, parent, false);
            // set the view's size, margins, paddings and layout parameters

            CountryHolder vh = new CountryHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(CountryHolder holder, final int position) {

            holder.countryName.setText(data.get(position).country_name);
            holder.countryNick.setText(data.get(position).country_nickName);
            holder.countryCode.setText("+"+data.get(position).country_code);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    UserWholeData dat=new UserWholeData();
                    dat.user_name=data.get(position).country_name;
                    dat.chatType=data.get(position).country_code;
                    listner.newUserAdded(dat);
                }
            });
        }

        @Override
        public int getItemCount() {
            return data.size();
        }
    }

    public static class CountryHolder extends RecyclerView.ViewHolder {

        public TextView countryName;
        public TextView countryNick;
        public TextView countryCode;

        public CountryHolder(View item)
        {
            super(item);
            countryName=(TextView)item.findViewById(R.id.countryName);
            countryNick=(TextView)item.findViewById(R.id.countryNick);
            countryCode=(TextView)item.findViewById(R.id.countryCode);
            /*status=(TextView)item.findViewById(R.id.contactStatus);
            proimg=(ImageView) item.findViewById(R.id.contactImg);

            imageSelectionTick=(CircularImageView) item.findViewById(R.id.imageSelectionTick);
            wholeView=(RelativeLayout)item.findViewById(R.id.singleContact);

            userOnlineStatus=(TextView)item.findViewById(R.id.userOnlineStatus);
            switchFollow=(SwitchCompat) item.findViewById(R.id. switchFollow);
            if(temp==null) {
                temp = wholeView.getBackground();
            }*/

        }
    }
}
