package com.hash.around.OtherClasses.Interfaces;

import com.hash.around.OtherClasses.DataTypes.GroupChatData;
import com.hash.around.OtherClasses.DataTypes.UserPrivacyData;

public interface ChatSentListner {
    void onChatAdded(GroupChatData data);
    void onChatUploaded(GroupChatData data);
}


