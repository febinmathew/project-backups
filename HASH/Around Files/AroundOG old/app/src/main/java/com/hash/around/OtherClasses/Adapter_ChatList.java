package com.hash.around.OtherClasses;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
//import android.support.v7.view.ActionMode;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.Nullable;

import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hash.around.ChatWindow;
import com.hash.around.OtherClasses.DataTypes.ChatListData;
import com.hash.around.R;
import com.hash.around.TabbedActivity;
import com.hash.around.Utils.StringUtils;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.vanniktech.emoji.EmojiTextView;

import java.util.ArrayList;
import java.util.Arrays;

import static com.hash.around.ChatWindow.convertTimeStampToText;

/**
 * Created by Febin on 26-08-2017.
 */

public class Adapter_ChatList extends RecyclerView.Adapter<Adapter_ChatList.ViewHolder> {



    public int chatSlectedColor=Color.parseColor("#30000000");
    Context mContext;
    ArrayList<ChatListData> data;
   // selectedUsers;
boolean isMultiSelect=false;
    SparseBooleanArray selectedUsers;
    private ActionMode.Callback mActionModeCallback;
   //public ActionMode mActionMode;
   static  Drawable temp=null;



   Activity activ;
    public Adapter_ChatList(Context mcont, ArrayList<ChatListData> list,Activity x)
    {
        activ=x;
        mContext=mcont;
        data=list;
        selectedUsers=new SparseBooleanArray();

         mActionModeCallback = new ActionMode.Callback() {

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {

                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.chatlistwindow_contextmenu, menu);



                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
               return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.action_delete:

                        //String s=new String();
                                for (int i = selectedUsers.size()-1; i>=0; i--) {
                                     //s=selectedUsers.keyAt(i)+" ";

                                    //Toast.makeText(mContext, data.get(selectedUsers.keyAt(i)).userid, Toast.LENGTH_SHORT).show();
                                    deleteFullchat(data.get(selectedUsers.keyAt(i)).userid);
                                    data.remove(selectedUsers.keyAt(i));

                                   // s+=selectedUsers.keyAt(i)+" ";
                                    // data.remove(selectedUsers.keyAt(1));


                                 }

                        mode.finish();
                        return true;

                    case R.id.action_mark_chat_read:
                        modifyChatListWithPinORHiddenORRead(null,null,1);
                        mode.finish();
                        return true;
                    case R.id.action_mark_chat_unread:
                        modifyChatListWithPinORHiddenORRead(null,null,0);
                        mode.finish();
                        return true;

                    case R.id.action_chat_hide:
                        modifyChatListWithPinORHiddenORRead(null,1,null);
                        mode.finish();
                        return true;
                    case R.id.action_chat_unhide:
                        modifyChatListWithPinORHiddenORRead(null,0,null);
                        mode.finish();
                        return true;
                    case R.id.action_pin_chattotop:

                        modifyChatListWithPinORHiddenORRead(1,null,null);

                        mode.finish();
                        return true;
                    case R.id.action_unpin_chatfromtop:
                        modifyChatListWithPinORHiddenORRead(0,null,null);

                        mode.finish();
                        return true;
                }
                return false;

            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                selectedUsers.clear();
                isMultiSelect=false;
                notifyDataSetChanged();
            }
        };


    }

    void modifyChatListWithPinORHiddenORRead(@Nullable Integer pinValue,@Nullable Integer hiddenValue,@Nullable Integer readValue)
    {
        SQLiteDatabase db =TabbedActivity.db.getWritableDatabase();
        ContentValues values2= new ContentValues();

        if(pinValue!=null)
            values2.put(DatabaseAccesser.chatList_chat_pinned,pinValue);
        if(hiddenValue!=null)
            values2.put(DatabaseAccesser.chatList_chat_hidden,hiddenValue);
        if(readValue!=null)
            values2.put(DatabaseAccesser.chatList_msgqueue,readValue);
        String user=null;
        for (int i = selectedUsers.size()-1; i>=0; i--) {
            user=data.get(selectedUsers.keyAt(i)).userid;
            int ln=db.update(DatabaseAccesser.chatList_table,values2,DatabaseAccesser.chatList_userid+" =?",new String[]{user});
            if(ln>0) {
                if(pinValue!=null)
                    data.get(selectedUsers.keyAt(i)).chat_pinned=pinValue;
                if(hiddenValue!=null)
                    data.get(selectedUsers.keyAt(i)).chat_hidden=hiddenValue;
                if(readValue!=null)
                    data.get(selectedUsers.keyAt(i)).msgQueue=readValue;
                notifyItemChanged(selectedUsers.keyAt(i));
            }
        }


    }


    void deleteFullchat(String userId)
    {
        SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();
       // String query ="DELETE FROM "+ DatabaseAccesser.chat_table+ " WHERE "+DatabaseAccesser.chat_userid+"=? ";
       // String query2 = "DELETE FROM "+ DatabaseAccesser.chatList_table+ " WHERE "+DatabaseAccesser.chatList_userid+"=? ";

        db.delete(DatabaseAccesser.chat_table, DatabaseAccesser.chat_userid + "=?", new String[]{userId});
        db.delete(DatabaseAccesser.chatList_table, DatabaseAccesser.chatList_userid + "=?", new String[]{userId});
        //db.rawQuery(query, new String[]{userId});
        // cursor = db.rawQuery(query2, new String[]{userId});
    }
public void test()
{
   /* String s=new String();
    for(int i=0;i<selectedUsers.size();i++)
    {
        s+=selectedUsers.keyAt(i)+" ";
    }
    Toast.makeText(mContext, s, Toast.LENGTH_SHORT).show();*/
}
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_chat, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {


         final int pos=position;
        final RelativeLayout rView=holder.wholeView;
        final Drawable dr=holder.temp;

        if(selectedUsers.get(position))
            rView.setBackgroundColor(chatSlectedColor);
        else
            rView.setBackground(dr);

        holder.chatName.setText(data.get(pos).name);

        long msgTime=data.get(pos).timestamp;

        holder.dateText.setText(convertTimeStampToText(msgTime,false));

        if(data.get(pos).msgFromMe==0)
        {
            holder.statusBubble.setVisibility(View.GONE);
        }
        else
        {
            switch(data.get(pos).lastMessageSeen) {
                case 0:
                    holder.statusBubble.setImageResource(R.drawable.ic_progress_clock);
                    break;
                case 1:
                    holder.statusBubble.setImageResource(R.drawable.ic_single_tick);
                    break;
                case 2:
                    holder.statusBubble.setImageResource(R.drawable.ic_double_tick);
                    break;
                case 3:
                    holder.statusBubble.setImageResource(R.drawable.ic_double_tick_seen_primary);
                    break;
            }
            holder.statusBubble.setVisibility(View.VISIBLE);
        }

        if(data.get(pos).msgQueue==0)// || data.get(pos).msgFromMe==1)
        {
            holder.chatBubble.setVisibility(View.GONE);

           // holder.lastMessage.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
        else
        {
            holder.chatBubble.setVisibility(View.VISIBLE);
            holder.chatBubble.setText(data.get(pos).msgQueue+"");

           // holder.lastMessage.setCompoundDrawables( mContext.getResources().getDrawable(R.drawable.ic_icon_chat_bubble_green),null,null,null);
        }

        if(data.get(pos).chat_pinned==0)
        {
        holder.wholeView.findViewById(R.id.starred).setVisibility(View.GONE);
        }
        else
        {
            holder.wholeView.findViewById(R.id.starred).setVisibility(View.VISIBLE);
        }
        holder.imageBubble.setVisibility(data.get(pos).lastMessagetype==0?View.GONE:View.VISIBLE);
        switch(data.get(pos).lastMessagetype)// || data.get(pos).msgFromMe==1)
        {
            case 0:
                holder.lastMessage.setText(data.get(pos).lastMessage);
                break;
            case 1:
                holder.imageBubble.setImageResource(R.drawable.ic_image_semiblack);
                break;
            case 2:
                holder.imageBubble.setImageResource(R.drawable.ic_audiotrack_white);
                break;
            case 3:
                holder.imageBubble.setImageResource(R.drawable.ic_video_white);
                break;
            case 4:
                holder.imageBubble.setImageResource(R.drawable.ic_icon_location_globe);
                //holder.lastMessage.setText(data.get(pos).lastMessage);

                //holder.lastMessage.stre
                break;
            case 5:
                holder.imageBubble.setImageResource(R.drawable.ic_gif_white);
                break;
            case 6:
                holder.imageBubble.setImageResource(R.drawable.ic_file_white);
                break;
            case 7:
                holder.imageBubble.setImageResource(R.drawable.ic_contacts_white);
                break;


            // holder.lastMessage.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
        if(data.get(pos).lastMessage.equals(""))
        {
            holder.lastMessage.setTypeface(holder.lastMessage.getTypeface(), Typeface.BOLD);
            holder.lastMessage.setText(StringUtils.convertMediaTypeToString(data.get(pos).lastMessagetype));
        }
        else
        {
            holder.lastMessage.setTypeface(holder.lastMessage.getTypeface(), Typeface.NORMAL);
            holder.lastMessage.setText(StringUtils.convertMediaTypeToString(data.get(pos).lastMessagetype)+(data.get(pos).lastMessagetype!=0?": ":"")+data.get(pos).lastMessage);
        }

        String dir=data.get(position).url;
        if(dir.equals("null"))
        {
            holder.profileimg.setImageResource(R.drawable.default_user);
        }
        else
        {
            holder.profileimg.setImageURI(Uri.parse(dir));
        }




       final  ViewHolder x=holder;
        holder.wholeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isMultiSelect)
                {
                    //multi_select(pos);
                    //rView.setBackgroundColor(Integer.parseInt("#44ff0000"));
                   // rView.setBackground(temp);
                    //rView.setBackgroundResource(R.attr.selectableItemBackground);

                    //is;MultiSelect = true
                    if (selectedUsers.get(pos))
                    {
                        selectedUsers.delete(pos);
                        rView.setBackground(dr);
                        if(selectedUsers.size()==0)
                        {
                            isMultiSelect=false;
                            TabbedActivity.uniActionMode.finish();
                        }
                        //TabbedActivity.uniActionMode.setTitle(selectedUsers.size()+" Selected");
                    }

                    else {
                        selectedUsers.put(pos,true);
                        rView.setBackgroundColor(chatSlectedColor);

                       // TabbedActivity.uniActionMode.setTitle(selectedUsers.size()+" Selected");

                    }
                    if(TabbedActivity.uniActionMode!=null)
                    TabbedActivity.uniActionMode.setTitle(selectedUsers.size()+" Selected");
                }
                else
                {
                    Context con = v.getContext();
                    //Toast.makeText(con, "Happesns", Toast.LENGTH_SHORT).show();

                    Intent indi = new Intent(con, ChatWindow.class);
                    indi.putExtra("userid",data.get(pos).userid);
                    indi.putExtra("username",data.get(pos).name);
                    indi.putExtra("user_type",data.get(pos).type);


                    /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        // Apply activity transition

                    Pair contact_image = Pair.create(holder.profileimg, "contact_image");
                    Pair contact_name = Pair.create(holder.chatName, "contact_name");

                    ActivityOptions compat=ActivityOptions.makeSceneTransitionAnimation(
                            activ,contact_image,contact_name);
                        con.startActivity(indi,compat.toBundle());
                    }
                    else
                    {*/
                        con.startActivity(indi);
                   // }
                    //ActivityOptionsCompat.makeSceneTransitionAnimation(activ,(View)holder.profileimg, "contact_image");

                   // ActivityOptions.makeSceneTransitionAnimation(activ, Pair.create(holder.profileimg,"contact_image"));






                        //((AppCompatActivity)mContext).finish();

                        x.chatBubble.setVisibility(View.GONE);
                }
            }
        });

        holder.wholeView.setOnLongClickListener(new View.OnLongClickListener() {

            public void multi_select() {


                isMultiSelect = true;
                if (selectedUsers.get(pos))
                {
                    selectedUsers.delete(pos);
                    rView.setBackground(dr);
                }

                else {
                    selectedUsers.put(pos,true);
                    rView.setBackgroundColor(Color.parseColor("#30000000"));
                    }
                //if(TabbedActivity.uniActionMode!=null)
                TabbedActivity.uniActionMode.setTitle(selectedUsers.size()+" Selected");
                updateMenuInContextMenu();

                if(selectedUsers.size()==0)
                {
                    isMultiSelect=false;
                    TabbedActivity.uniActionMode.finish();
                }



                //Toast.makeText(mContext, s, Toast.LENGTH_SHORT).show();

            }

            @Override
            public boolean onLongClick(View v) {



                if(!isMultiSelect)
                {


                    TabbedActivity.uniActionMode=  v.startActionMode(mActionModeCallback);
                    multi_select();


                }
                else
                {
                    multi_select();

                }


                return true;
            }
        });
    }

    void updateMenuInContextMenu()
    {
        Menu menu= TabbedActivity.uniActionMode.getMenu();
        int[] counts=new int[6]; //0-notpined 1-pinned 2-hidden 3- unhidden 4-read 5 unread
        Arrays.fill(counts, 0);

        for(int i=0;i<selectedUsers.size();i++ )
        {
            if(data.get(selectedUsers.keyAt(i)).chat_pinned==1)
            {
                counts[0]++;    //how many pinned
            }
            else
            {
                counts[1]++;
            }
            if(data.get(selectedUsers.keyAt(i)).chat_hidden==1)
            {
                counts[2]++;     //how many hidden chats
            }
            else
            {
                counts[3]++;
            }
            if(data.get(selectedUsers.keyAt(i)).msgQueue<1)
            {
                counts[4]++;   //how many msges not seen
               // Log.e("MSGG","msg not seen");
            }
            else
            {
               // Log.e("MSGG","msg Seen");
                counts[5]++;
            }
        }
        MenuItem [] menuItems=new MenuItem[6];
        menuItems[0]=menu.findItem(R.id.action_pin_chattotop);
        menuItems[1]=menu.findItem(R.id.action_unpin_chatfromtop);

        menuItems[2]=menu.findItem(R.id.action_chat_hide);
        menuItems[3]=menu.findItem(R.id.action_chat_unhide);

        menuItems[4]=menu.findItem(R.id.action_mark_chat_read);
        menuItems[5]=menu.findItem(R.id.action_mark_chat_unread);;


        for(int i=0;i<6;i++)
        {
            if(counts[i]<1)
            {
                menuItems[i].setVisible(true);
            }
            else
            {
                menuItems[i].setVisible(false);
            }
        }

    }


    @Override
    public int getItemCount() {
        if(data!=null) {
            return data.size();
        }
        return 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
       // public CardView cv;
        public TextView chatName;
        public EmojiTextView lastMessage;
        RelativeLayout wholeView;
        public TextView dateText;
        ImageView imageBubble,statusBubble;
        TextView chatBubble;
        public CircularImageView profileimg;
        //static Drawable temp;
       /* public ImageView img;
        */
        public Drawable temp=null;
        public ViewHolder(View item)
        {
            super(item);
            wholeView=(RelativeLayout)item.findViewById(R.id.singleChatItem);
           if(temp==null) {
               temp = wholeView.getBackground();
           }
            //cv=(CardView)item.findViewById(R.id.cView);
            chatName=(TextView)item.findViewById(R.id.chatname);
            lastMessage=(EmojiTextView)item.findViewById(R.id.lastMessageText);
            dateText=(TextView)item.findViewById(R.id.chatDate);
            chatBubble=(TextView) item.findViewById(R.id.chatBubble);
            imageBubble=(ImageView)item.findViewById(R.id.imageBubble);
            profileimg=(CircularImageView)item.findViewById(R.id.chatimg);
            statusBubble=(ImageView)item.findViewById(R.id.statusBubble);


        }
    }

}


