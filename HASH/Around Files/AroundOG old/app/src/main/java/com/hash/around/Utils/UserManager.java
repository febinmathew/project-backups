package com.hash.around.Utils;

import android.content.ContentValues;
import android.util.Log;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.OtherClasses.Interfaces.JobCompletionWithFailureListner;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Febin on 6/13/2018.
 */

public class UserManager {
    static FirebaseUser userInfo=null;

    public static FirebaseUser getUserInfo() {
        if(userInfo==null)
            userInfo= FirebaseAuth.getInstance().getCurrentUser();
        return userInfo;
    }

    public static void updateUserChatDestroAndSchedule(String userID,Integer destroTime,Long scheduleTime)
    {
        Log.e("MGG","schedule time "+scheduleTime);
        ContentValues values=new ContentValues();
        if(destroTime!=null)
            values.put(DatabaseAccesser.user_chat_expiretime,destroTime);
        if(scheduleTime!=null)
            values.put(DatabaseAccesser.user_chat_scheduletime,scheduleTime);

       long rowln= DatabaseManager.getDatabase().getWritableDatabase().update(DatabaseAccesser.user_table,values,
                DatabaseAccesser.user_user_id+"=?",new String[]{userID});
        Log.e("MGG","rows affect "+rowln);

    }
    public static void updateUserChatNotification(String userID,String not_uri,Integer vibration,Integer not_light,Long muteVal)
    {
        ContentValues values=new ContentValues();
        if(not_uri!=null)
        values.put(DatabaseAccesser.user_notification_sound,not_uri);
        if(vibration!=null)
            values.put(DatabaseAccesser.user_notification_vibration,vibration);
        if(not_light!=null)
            values.put(DatabaseAccesser.user_notification_light,not_light);
        if(muteVal!=null)
            values.put(DatabaseAccesser.user_mutetime,muteVal);


        DatabaseManager.getDatabase().getWritableDatabase().update(DatabaseAccesser.user_table,values,
                DatabaseAccesser.user_user_id+"=?",new String[]{userID});
    }
    public static void getuserChatDestro()
    {

    }

    public static void updateLastReceiveTime(int type, long sl, DatabaseReference dbRef, final JobCompletionWithFailureListner listener)

    {
        HashMap statusMap = new HashMap();
        statusMap.put("type",type);
        if(type==0)
        statusMap.put("last_msg",sl);
        else
            statusMap.put("last_msg",sl);
        statusMap.put("timestamp",System.currentTimeMillis());

        dbRef.push().setValue(statusMap).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                if (listener!=null)
                    listener.onJobCompletion("");
            }
        });
    }
}
