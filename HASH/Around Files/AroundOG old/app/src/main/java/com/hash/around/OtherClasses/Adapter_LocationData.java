package com.hash.around.OtherClasses;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;

import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.hash.around.AllUsersList;
import com.hash.around.OtherClasses.DataTypes.ChatData;
import com.hash.around.OtherClasses.DataTypes.LocationData;
import com.hash.around.R;
import com.hash.around.TabbedActivity;

import java.io.Serializable;
import java.util.ArrayList;

import static com.hash.around.TabbedActivity.editor;

/**
 * Created by Febin on 26-08-2017.
 */

public class Adapter_LocationData extends RecyclerView.Adapter<Adapter_LocationData.ViewHolder> {

    Context mContext;
    ArrayList<LocationData> data;

    public Adapter_LocationData(Context mcont, ArrayList<LocationData> list)
    {
        mContext=mcont;
        data=list;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v =  LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_location, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {



String temp;
        holder.cLocationText.setText(data.get(position).location_name);
        temp=data.get(position).latitude+", "+data.get(position).longitude;
        holder.cLatitudeText.setText(temp);

        if(data.get(position).status==1)
        {
            holder.switchLoc.setChecked(true);
        }
        else
        {
            holder.switchLoc.setChecked(false);
        }

        holder.switchLoc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                SQLiteDatabase db= TabbedActivity.db.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(DatabaseAccesser.loc_col_status, isChecked?1:0);
                int rowln=db.update(DatabaseAccesser.location_table,
                        values,
                        DatabaseAccesser.loc_col_latitude + " = ? AND " + DatabaseAccesser.loc_col_longitude + " = ?",
                        new String[]{data.get(position).latitude+"",data.get(position).longitude+""});

                if(rowln !=1)
                {
                    Toast.makeText(mContext, "not updated Error", Toast.LENGTH_SHORT).show();
                }

            }
        });

        holder.deleteLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteLocation(data.get(position).location_name+"",
                        data.get(position).latitude+"",
                        data.get(position).longitude+"",position);
            }
        });

        //final int pos=position;
        final SwitchCompat tempswitch=  holder.switchLoc;
holder.rootView.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        tempswitch.setChecked(! tempswitch.isChecked());
        editor.putString("location_name", data.get(position).location_name);
        editor.putString("location_latitude", String.valueOf(data.get(position).latitude));
        editor.putString("location_longitude", String.valueOf(data.get(position).longitude));
        editor.commit();
        //Toast.makeText(mContext, "Clickeyeeee", Toast.LENGTH_SHORT).show();
    }
});

holder.shareLoc.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        shareLocation(data.get(position));
    }
});
        //temp="Lng: "+data.get(position).longitude;
      //  holder.cLongitudeText.setText(temp);
     /* final int pos=position;

       if(data.get(position).userid==1)
            holder.proimg.setImageResource(R.drawable.nick);
        else
            holder.proimg.setImageResource(R.drawable.profile);
switch(data.get(position).id) {
    case 1:
    holder.img.setImageResource(R.drawable.danial);
        break;
    case 2:
        holder.img.setImageResource(R.drawable.profile);
        break;
    case 3:
        holder.img.setImageResource(R.drawable.blue);
        break;
    case 4:
        holder.img.setImageResource(R.drawable.acce);
        break;
}
        holder.tx.setText(data.get(position).text);

        holder.proimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Toast.makeText(mContext,data.get(pos).text, Toast.LENGTH_SHORT).show();
                //Intent in=new Intent(mContext, Profile_Activity.class);

               // mContext.startActivity(in);
            }
        });*/
    }

    void shareLocation(LocationData value)
    {
        ArrayList<ChatData> chatsArray = new ArrayList<ChatData>();

        ChatData temp=new ChatData();
        temp.type=4;
        temp.msg=value.location_name;
        temp.temp_url = value.latitude+"";
        temp.final_url = value.longitude+"";

        chatsArray.add(temp);

        Intent intent = new Intent(mContext, AllUsersList.class);
        Bundle args = new Bundle();
        args.putSerializable("ARRAYLIST",(Serializable)chatsArray);
        intent.putExtra("BUNDLE",args);

       // intent.putExtra("chatsArray",chatsArray);

        mContext.startActivity(intent);
    }

    void deleteLocation(String locationName, final String lat, final String lng, final int pos) {

        // Context tempContext=getActivity().getApplicationContext();

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Remove "+locationName+"?");


        builder.setCancelable(true);


            builder.setPositiveButton("DELETE", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                    SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();
                    int rowln=db.delete(DatabaseAccesser.location_table,
                            DatabaseAccesser.loc_col_latitude+" = ? AND " + DatabaseAccesser.loc_col_longitude + " = ?",
                            new String[]{lat,lng});

                   /* int rowln=db.delete(DatabaseAccesser.location_table,
                            DatabaseAccesser.loc_col_latitude+" = ? OR ''='' OR " + DatabaseAccesser.loc_col_longitude + " = ?",
                            new String[]{lat,lng});*/

                    if(rowln !=1)
                    {
                        Toast.makeText(mContext, "Error: Unable to delete location!", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        data.remove(pos);

                    }

                    notifyItemRemoved(pos);
                    dialog.dismiss();
                }
            });

        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
               dialog.dismiss();
                   }
        });


        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public int getItemCount() {
        if(data!=null) {
            return data.size();
        }
        return 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
      /*  public CardView cv;
        public TextView tx;
        public ImageView img;
        public ImageView proimg;*/

        public TextView cLocationText;
        public TextView cLatitudeText;
        View rootView;
        SwitchCompat switchLoc;

        ImageButton deleteLoc,shareLoc;

      //  public TextView cLongitudeText;

        public ViewHolder(View item) {
            super(item);
            /*cv=(CardView)item.findViewById(R.id.cView);

            img=(ImageView)item.findViewById(R.id.ccImage);
            proimg=(ImageView)item.findViewById(R.id.cProfileImage);*/
            cLocationText=(TextView)item.findViewById(R.id.cLocationText);
            cLatitudeText=(TextView)item.findViewById(R.id.cLatitudeText);
            rootView=item;
            switchLoc=(SwitchCompat) item.findViewById(R.id.switchLoc);

            deleteLoc=(ImageButton) item.findViewById(R.id.deleteLoc);
            shareLoc=(ImageButton) item.findViewById(R.id.shareLoc);
            //item.setOnClickListener();
                   // cLongitudeText=(TextView)item.findViewById(R.id.cLongitudeText);

        }
    }

}


