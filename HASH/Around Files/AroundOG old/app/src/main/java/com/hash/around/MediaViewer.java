package com.hash.around;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.hash.around.Views.Medias.AroundDownloadImageView;
import com.hash.around.Views.Medias.AroundDownloadVideoView;

public class MediaViewer extends AppCompatActivity/* implements AdapterView.OnItemSelectedListener*/{

    String uri;
    String uricropped;
    int type;
    LinearLayout parentLayout;
    ImageView uploadImg;
    TextView statustext;
    Uri content;
    VideoView uploadVideo;

    public static int visibilityMode;
    String[] visibilityNames={"Public","Nearby people","Contacts only","Only me"};

    double latitute;
    double longitude;
    String locationName;


    private LocationCallback mLocationCallback;
    private FusedLocationProviderClient mFusedLocationClient;
    LocationRequest mLocationRequest;

    TextView locationtxt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.media_viewer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.comtoolbar2);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        uri= getIntent().getStringExtra("uri");
      //  Toast.makeText(this, uri, Toast.LENGTH_SHORT).show();
        type=getIntent().getIntExtra("type",0);
        parentLayout=(LinearLayout)findViewById(R.id.parentLayout);
        statustext=(TextView)findViewById(R.id.image_feed_status) ;

        locationtxt=(TextView)findViewById(R.id.location);
        //Spinner spin = (Spinner) findViewById(R.id.visibilitySpinner);
       // spin.setOnItemSelectedListener(this);

       // VisibilitySpinnerAdapter customAdapter=new VisibilitySpinnerAdapter(getApplicationContext(),visibilityNames);
       // spin.setAdapter(customAdapter);


        View inflateView=null;
        switch (type)
        {
            case 4:
                inflateView=View.inflate(this, R.layout.view_upload_text,parentLayout);
                findViewById(R.id.defaultEditor).setVisibility(View.INVISIBLE);
                statustext=(EditText) inflateView.findViewById(R.id.textFeedPost);
                break;
            case 5:
                inflateView=View.inflate(this, R.layout.view_image,parentLayout);
               // parentLayout.addView(inflateView,0);
                AroundDownloadImageView aroundImage;
                aroundImage=inflateView.findViewById(R.id.singleAroundImageView);
                aroundImage.setLocallyAvailable(true);
                aroundImage.setImageViewUri(Uri.parse(uri));
                uricropped=uri;
                break;
            case 6:

                inflateView=View.inflate(this, R.layout.view_video,parentLayout);

                AroundDownloadVideoView aroundVideo;

                aroundVideo=inflateView.findViewById(R.id.singleAroundVideoView);
                aroundVideo.setLocallyAvailable(true);
                aroundVideo.setUpVideoUri(Uri.parse(uri));
                break;
            case 7:
                inflateView=View.inflate(this, R.layout.view_gif,parentLayout);
                break;

        }




    }


   /* @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
       Toast.makeText(getApplicationContext(), position+"", Toast.LENGTH_LONG).show();
        visibilityMode=position;
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            Toast.makeText(this, "ooohhhh", Toast.LENGTH_SHORT).show();
            uploadImg.setImageURI(null);
            uricropped=data.getStringExtra("uri");
            uploadImg.setImageURI(Uri.parse(uricropped));
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
      /*  getMenuInflater().inflate(R.menu.menu_tabbed, menu);
        mMenu=menu;*/

//menu.clear();

            getMenuInflater().inflate(R.menu.media_viewer_activity, menu);

        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_crop) {
            Intent cropIntent=new Intent(this,ImageCropActivity.class);
             cropIntent.putExtra("uri",uri);
            startActivityForResult(cropIntent,1);
           // Toast.makeText(this, "cc", Toast.LENGTH_SHORT).show();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }
}
