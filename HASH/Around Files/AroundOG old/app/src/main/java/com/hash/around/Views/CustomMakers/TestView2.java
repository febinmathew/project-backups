package com.hash.around.Views.CustomMakers;

/**
 * Created by Febin on 5/25/2018.
 */


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ViewDragHelper;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.TouchDelegate;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;

import com.hash.around.R;

/**
 * Create On 16/10/2016
 * @author wayne
 */
public class TestView2 extends ViewGroup {


    public TestView2(Context context) {
        super(context);
    }

    public TestView2(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TestView2(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        //inflate(context, R.layout.timer_expirationchat_dialogview, this);
    }


    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {

    }
    ViewDragHelper mDragHelper;
    ViewDragHelper.Callback callbacknow=new ViewDragHelper.Callback() {
        @Override
        public boolean tryCaptureView(View arg0, int pointerId) {
            Log.e("MGG","gettting3");
            return true;
        }
        @Override
        public int clampViewPositionVertical(View child, int top, int dy) {
            Log.e("MGG","gettting4");
            return top;
        }
        @Override
        public int getViewVerticalDragRange(View child) {
            Log.e("MGG","gettting5");
            return getRootView().getMeasuredHeight()-child.getMeasuredHeight();
        }
        @Override
        public void onViewReleased(View releasedChild, float xvel, float yvel) {
            Log.e("MGG","gettting6");
            super.onViewReleased(releasedChild, xvel, yvel);
            if(yvel>0) {
                mDragHelper.settleCapturedViewAt(releasedChild.getLeft(), getRootView().getMeasuredHeight()-releasedChild.getMeasuredHeight());
            } else {
                mDragHelper.settleCapturedViewAt(releasedChild.getLeft(), 0);
            }
            invalidate();
        }

           /* @Override
            public int getViewVerticalDragRange(View child) {
                return parent.getMeasuredHeight()-child.getMeasuredHeight();
            }*/
    };
    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        mDragHelper = ViewDragHelper.create(this, 1.0f,callbacknow );
    }
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        Log.e("MGG","gettting1");
        boolean shouldInterceptTouchEvent = mDragHelper.shouldInterceptTouchEvent(ev);
        return shouldInterceptTouchEvent;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.e("MGG","gettting2");
        mDragHelper.processTouchEvent(event);
        return true;
    }
    @Override
    public void computeScroll() {
        super.computeScroll();
        if(mDragHelper.continueSettling(true)) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }


}