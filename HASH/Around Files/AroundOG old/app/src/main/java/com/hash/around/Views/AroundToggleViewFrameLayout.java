package com.hash.around.Views;

import android.content.Context;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.hash.around.R;

/**
 * Created by Febin on 5/20/2018.
 */

public class AroundToggleViewFrameLayout extends FrameLayout {

    private View current;


    private final Animation inAnimation;
    private final Animation outAnimation;

    public AroundToggleViewFrameLayout(Context context) {
        this(context, null);
    }

    public AroundToggleViewFrameLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AroundToggleViewFrameLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.outAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.toggle_animation_out);
        this.inAnimation  = AnimationUtils.loadAnimation(getContext(), R.anim.toggle_animation_in);
        this.outAnimation.setInterpolator(new FastOutSlowInInterpolator());
        this.inAnimation.setInterpolator(new FastOutSlowInInterpolator());
    }

    @Override
    public void addView(@NonNull View child, int index, ViewGroup.LayoutParams params) {
        super.addView(child, index, params);

        if (getChildCount() == 1) {
            current = child;
            child.setVisibility(View.VISIBLE);
            Log.e("MSG","visible child is "+child);
        } else {
            child.setVisibility(View.GONE);
        }
        //child.setClickable(false);
    }

    public void display(@Nullable View view) {
        if (view == current) return;
        if (current != null) ViewFunctions.animateOut(current, outAnimation, View.GONE);
        if (view    != null) ViewFunctions.animateIn(view, inAnimation);

        current = view;
    }

    public void displayQuick(@Nullable View view) {
        if (view == current) return;
        if (current != null) current.setVisibility(View.GONE);
        if (view != null)    view.setVisibility(View.VISIBLE);

        current = view;
    }

    @Override
    public void onFinishInflate() {
        super.onFinishInflate();

        /*ImageView recordButtonFab =
        this.floatingRecordButton = new FloatingRecordButton(getContext(), recordButtonFab);

        View recordButton = ViewUtil.findById(this, R.id.quick_audio_toggle);
        recordButton.setOnTouchListener(this);*/
    }
    AudioRecordistener listener;
    int recordButtonStartX;
    public void setListner(AudioRecordistener listnerTemp)
    {
        listener=listnerTemp;
    }
    public void setUpRecordListner()
    {
        ImageButton recordButton=(ImageButton) this.findViewById(R.id.recordMsgButton);
        recordButton.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        actionInProgress = true;
                        recordButtonStartX = (int) event.getX();
                        if(listener!=null) listener.onRecordPressed(recordButtonStartX);
                        //updateRecordingstatus(true);
                        display(event.getX());
                        Log.e("MGG", "started");
                        break;
                    case MotionEvent.ACTION_MOVE:
                        if(actionInProgress) {

                            int tempX = (int) event.getX();
                            //int tempy = (int) event.getY();

                            if ((recordButtonStartX - tempX) > 200) {

                                actionInProgress = false;
                                if(listener!=null) listener.onRecordCanceled(recordButtonStartX);
                                hide();
                                Log.e("MGG", "hiding");

                            }
                            else {
                                if(listener!=null) listener.onRecordMoved(recordButtonStartX,event.getRawX());
                                //moveTo(event.getX());
                                //Log.e("MGG", (recordButtonStartX - tempX) + " hiding");
                            }
                        }
                        break;
                    case MotionEvent.ACTION_CANCEL:
                    case MotionEvent.ACTION_UP:
                        if(actionInProgress) {
                            actionInProgress = false;

                            if(listener!=null) listener.onRecordReleased(recordButtonStartX);
                            hide();
                        }
                        break;
                }

                return true;
            }
        });
    }

    public interface AudioRecordistener {
        void onRecordPressed(float x);
        void onRecordReleased(float x);
        void onRecordCanceled(float x);
        void onRecordMoved(float x, float absoluteX);
        void onRecordPermissionRequired();
    }

    boolean actionInProgress;
    void display(float x)
    {
        this.startPositionX = x;
        this.lastPositionX  = x;

        this.setVisibility(View.VISIBLE);

        float translation = ViewCompat.getLayoutDirection(this) ==
                ViewCompat.LAYOUT_DIRECTION_LTR ? -.25f : .25f;

        AnimationSet animation = new AnimationSet(true);
        /*animation.addAnimation(new TranslateAnimation(Animation.RELATIVE_TO_SELF, translation,
                Animation.RELATIVE_TO_SELF, translation,
                Animation.RELATIVE_TO_SELF, 0f,
                Animation.RELATIVE_TO_SELF, 0f));*/

        animation.addAnimation(new ScaleAnimation(.5f, 2f, .5f, 2f,
                Animation.RELATIVE_TO_SELF, .5f,
                Animation.RELATIVE_TO_SELF, .5f));

        animation.setFillBefore(true);
        animation.setFillAfter(true);
        animation.setDuration(200);
        animation.setInterpolator(new OvershootInterpolator());

        this.startAnimation(animation);
        final AroundToggleViewFrameLayout ccc=this;
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
               /* clearAnimation();
                ccc.setScaleX(2);
                ccc.setScaleY(2);*/
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /*public void moveTo(float x) {
        //this.clearAnimation();
        this.lastPositionX = x;

        float offset          = getOffset(x);
        int   widthAdjustment = getWidthAdjustment();

        AnimationSet animation = new AnimationSet(true);
        animation.addAnimation(this.getAnimation());
        Animation translateAnimation = new TranslateAnimation(Animation.ABSOLUTE, widthAdjustment + offset,
                Animation.ABSOLUTE, widthAdjustment + offset,
                Animation.RELATIVE_TO_SELF, .0f,
                Animation.RELATIVE_TO_SELF, .0f);

        animation.addAnimation(translateAnimation);
        animation.setDuration(0);
        animation.setFillAfter(true);
        animation.setFillBefore(true);

        this.startAnimation(animation);
    }*/

    void hide()
    {
        ViewFunctions.scaleView(this,2,1);
    }


    private float getOffset(float x) {
        return ViewCompat.getLayoutDirection(this) == ViewCompat.LAYOUT_DIRECTION_LTR ?
                -Math.max(0, startPositionX - x) : Math.max(0, x - startPositionX);
    }

    private int getWidthAdjustment() {
        int width = this.getWidth() / 4;
        return ViewCompat.getLayoutDirection(this) == ViewCompat.LAYOUT_DIRECTION_LTR ? -width : width;
    }
    private static float startPositionX;
    private static float lastPositionX;


}
