package com.hash.around.OtherClasses;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Febin on 14-10-2017.
 */

public class DatabaseAccesser extends SQLiteOpenHelper {

    public static final String count_value = "count_value";

    public static final String feed_table = "feed_data";

    public static final String feed_col_sr_no = "sr_no";
    public static final String feed_col_user_id = "user_id";
    public static final String feed_col_username = "user_name";
    public static final String feed_col_timestamp = "timestamp";
    public static final String feed_col_location = "location";
    public static final String feed_col_visibility = "visibility";
    public static final String feed_col_seen = "seen";
    public static final String feed_col_type = "type";
    public static final String feed_col_text = "text";
    public static final String feed_col_media_id = "media_id";
    public static final String feed_col_latitude = "latitude";
    public static final String feed_col_longitude = "longitude";

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "around.db";
    private static final String SQL_CREATE_FEEDDATA =
            "CREATE TABLE "+feed_table+"("+
                    feed_col_user_id + " TEXT NOT NULL,"+
                    feed_col_username + " TEXT NOT NULL,"+
                    feed_col_timestamp + " INTEGER NOT NULL,"+
                    feed_col_location + " TEXT,"+
                    feed_col_visibility + " INTEGER,"+
                    feed_col_type + " INTEGER NOT NULL,"+
                    feed_col_seen + " INTEGER,"+
                    feed_col_text + " TEXT," +
                    feed_col_media_id + " INTEGER DEFAULT 1,"+
                    feed_col_latitude + " REAL,"+
                    feed_col_longitude + " REAL)";
    private static final String SQL_DELETE_FEEDDATA =
            "DROP TABLE IF EXISTS " + feed_table;

    public static final String location_table = "location_data";

    public static final String loc_col_location_name = "location_name";
    public static final String loc_col_status = "status";
    public static final String loc_col_latitude = "latitude";
    public static final String loc_col_longitude = "longitude";


    private static final String SQL_CREATE_LOCATIONDATA =
            "CREATE TABLE "+location_table+"("+
                    loc_col_location_name + " TEXT," +
                    loc_col_status + " INTEGER NOT NULL,"+
                    loc_col_latitude + " REAL NOT NULL,"+
                    loc_col_longitude + " REAL NOT NULL)";
    private static final String SQL_DELETE_LOCATIONDATA =
            "DROP TABLE IF EXISTS " + location_table;




    public static final String chatList_table = "chat_list_data";

    public static final String chatList_userid = "chatlist_user_id";
    public static final String chatList_name = "name";
    public static final String chatList_lastmsgbyme = "last_msg_byme";
    public static final String chatList_lastmsg = "last_msg";
    public static final String chatList_lastmsgRowId = "last_msg_rowid";
    public static final String chatList_last_msgseen = "last_msg_seen";
    public static final String chatList_lastmsg_type = "last_msg_type";
    public static final String chatList_chat_pinned = "chat_pin";
    public static final String chatList_chat_hidden = "chat_hidden";
    public static final String chatList_timestamp = "chatlist_timestamp";
    public static final String chatList_msgqueue = "msg_queue";
    public static final String chatList_url = "img_id";



    private static final String SQL_CREATE_CHATLISTDATA =
            "CREATE TABLE "+chatList_table+"("+
                    chatList_userid + " TEXT PRIMARY KEY," +
                    chatList_name + " TEXT," +
                    chatList_lastmsgbyme + " INTEGER,"+
                    chatList_lastmsg+ " TEXT,"+
                    chatList_lastmsgRowId+ " INTEGER,"+
                    chatList_last_msgseen + " INTEGER DEFAULT 0,"+
                    chatList_lastmsg_type + " INTEGER,"+
                    chatList_chat_pinned+ " INTEGER DEFAULT 0,"+
                    chatList_chat_hidden+ " INTEGER DEFAULT 0,"+
                    chatList_timestamp + " REAL," +
                    chatList_msgqueue + " INTEGER DEFAULT 0,"+
                    chatList_url + " TEXT)";
    private static final String SQL_DELETE_CHATLISTDATA =
            "DROP TABLE IF EXISTS " + chatList_table;




    public static final String chat_table = "chat_data";
    public static final String chat_sl_no = "chat_sl_no";
    public static final String chat_userid = "chat_id";
    public static final String chat_groupid = "chat_groupid";
    public static final String chat_sender = "sender";
    public static final String chat_type = "type";
    public static final String chat_timestamp = "chat_timestamp";
    public static final String chat_rece_timestamp = "chat_rece_times";
    public static final String chat_seen_timestamp = "chat_seen_time";
    public static final String chat_schedule= "chat_schedule";
    public static final String chat_expire = "chat_expire";
    public static final String chat_text = "text";
    public static final String chat_ifseen = "chat_ifseen";
    public static final String chat_col_media_id = "chat_media_id";
    /*public static final String chat_temp_url = "temp_url";
    public static final String chat_final_url = "final_url";*/



    private static final String SQL_CREATE_CHATDATA =
            "CREATE TABLE "+chat_table+"("+
                    chat_sl_no + " INTEGER," + //want to change to long
                    chat_userid + " TEXT," +
                    chat_groupid + " TEXT DEFAULT 'null'," +
                    chat_sender + " INTEGER NOT NULL,"+
                    chat_type + " INTEGER NOT NULL,"+
                    chat_timestamp + " INTEGER NOT NULL,"+
                    chat_rece_timestamp + " INTEGER DEFAULT 0,"+
                    chat_seen_timestamp + " INTEGER DEFAULT 0,"+
                    chat_schedule + " INTEGER DEFAULT 0,"+
                    chat_expire + " INTEGER DEFAULT 0,"+
                    chat_text + " TEXT," +
                    chat_ifseen + " INTEGER DEFAULT 0,"+
                    chat_col_media_id + " INTEGER DEFAULT 1)";
    private static final String SQL_DELETE_CHATDATA =
            "DROP TABLE IF EXISTS " + chat_table;
/*
                    chat_temp_url + " TEXT,"+
                    chat_final_url + " TEXT*/


    // user type 0= own user..1=normal user..2=phone contacts ..3=follower..4=blocked..5=blocking.......15=group,16=broadcast
    public static final String user_table = "user_data";
    public static final String user_user_id = "userid";
    public static final String user_sl_id = "sl_id";
    public static final String user_user_type = "userme";
    public static final String user_user_follow_type = "follow_type";
    public static final String user_user_chat_type = "chat_type";
    public static final String user_name = "name";
    public static final String user_number = "number";
    public static final String user_timestamp = "timestamp";
    public static final String user_lastRec_sl = "lstrec_sl";

    public static final String user_status = "status";
    public static final String user_globalUrl = "global_url";
    public static final String user_localUrl = "local_url";
    public static final String user_music_globalUrl = "music_global";
    public static final String user_music_localUrl = "music_local";

   // public static final String user_remote_contact = "remote";

    public static final String user_priv_activity_status = "activity_status";
    public static final String user_priv_location_status = "location_status";
    public static final String user_priv_profile_picture_status = "profile_status";
    public static final String user_priv_status_status = "status_status";
    public static final String user_priv_music_status = "music_status";
    public static final String user_priv_number_status = "number_status";
    public static final String user_remote_contact = "remote_contact";

    public static final String user_follow_privacy = "follow_privacy";
    public static final String user_chat_privacy = "chat_privacy";


    public static final String user_chat_expiretime = "user_chat_destro"; //newly added
    public static final String user_chat_scheduletime = "user_chat_schedule";
    public static final String user_notification_sound = "user_chat_not_sound";
    public static final String user_notification_light = "user_chat_not_light";
    public static final String user_notification_vibration = "user_chat_not_vibration";
    public static final String user_mutetime = "user_chat_mute";


    //follow 0 none 1 me_following 2 follower 3 follow out requested 4 follow incomming reqest 5 follow rejected 11 me_blocked 12 others_blocking
    //chat 0 none 1 allowChat 3 chat request sent  5 chat rejected
    private static final String SQL_CREATE_USERDATA =
            "CREATE TABLE "+user_table+"("+
                    user_sl_id + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    user_user_id + " TEXT UNIQUE," +
                    user_user_type + " INTEGER," +
                    user_user_follow_type + " INTEGER DEFAULT 0," +
                    user_user_chat_type + " INTEGER DEFAULT 0," +
                    user_name + " TEXT," +
                    user_number+ " TEXT,"+
                    user_timestamp + " REAL," +
                    user_lastRec_sl + " INT DEFAULT 0," +
                    user_status+ " TEXT,"+
                    user_globalUrl + " TEXT,"+
                    user_localUrl + " TEXT,"+
                    user_music_globalUrl + " TEXT,"+
                    user_music_localUrl + " TEXT,"+
                    user_notification_sound+" TEXT,"+
                    user_notification_light+" INTEGER DEFAULT 0," +
                    user_notification_vibration+" INTEGER DEFAULT 0," +
                    user_mutetime+" INTEGER DEFAULT 0," +
                    user_chat_expiretime + " INTEGER DEFAULT 0," +
                    user_chat_scheduletime+ " INTEGER DEFAULT 0," +
                    user_priv_activity_status + " INTEGER DEFAULT 2," +
                    user_priv_location_status + " INTEGER DEFAULT 2," +
                    user_priv_profile_picture_status + " INT DEFAULT 2," +
                    user_priv_status_status + " INTEGER DEFAULT 2," +
                    user_priv_music_status + " INTEGER DEFAULT 2," +
                    user_priv_number_status + " INTEGER DEFAULT 2," +
                    user_follow_privacy+ " INTEGER DEFAULT 0," +
                    user_chat_privacy+ " INTEGER DEFAULT 0," +
                    user_remote_contact + " INTEGER DEFAULT 0)";
    private static final String SQL_DELETE_USERDATA =
            "DROP TABLE IF EXISTS " + user_table;


// type => 1= status  5=> maps search
    public static final String search_table = "search_data";
    public static final String search_type = "search_type";
    public static final String search_text = "search_text";
    public static final String search_time = "search_timestamp";

    private static final String SQL_CREATE_SEARCHDATA =
            "CREATE TABLE "+search_table+"("+
                    search_type + " INTEGER," +
                    search_text + " TEXT," +
                    search_time + " REAL)";
    private static final String SQL_DELETE_SEARCHDATA =
            "DROP TABLE IF EXISTS " + search_table;



    /*public static final String group_table = "group_table";
    public static final String group_grp_id = "groupid";
    public static final String group_sl = "grp_sl";
    public static final String group_type = "grp_type";
    public static final String group_name = "grp_name";
    //public static final String user_number = "number";
    public static final String group_fetch_timestamp = "fetch_timestamp";
    public static final String group_status = "grp_status";
    public static final String group_globalUrl = "global_url";
    public static final String group_localUrl = "local_url";



    private static final String SQL_CREATE_GROUPDATA =
            "CREATE TABLE "+group_table+"("+
                    group_sl + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    group_grp_id + " TEXT UNIQUE," +
                    group_type + " INTEGER," +
                    group_name + " TEXT," +
                    //user_number+ " TEXT,"+
                    group_fetch_timestamp + " REAL," +
                    group_status+ " TEXT,"+
                    group_globalUrl + " TEXT,"+
                    group_localUrl + " TEXT)";
    private static final String SQL_DELETE_GROUPDATA =
            "DROP TABLE IF EXISTS " + group_table;

*/



    public static final String groupsmembers_table = "group_members";
    public static final String groupsmembers_group_id = "groupid";
    public static final String groupsmembers_user_id = "userid";
    public static final String groupsmembers_group_sl_id = "sl_id";
    public static final String groupsmembers_last_rece_time = "last_rece_time";
    public static final String groupsmembers_last_seen_time = "last_seen_time";
    public static final String groupsmembers_member_type = "member_type";
    public static final String groupsmembers_chat_selfdestruction = "member_chat_selfdestro";





    private static final String SQL_CREATE_GROUPMEMBERDATA =
            "CREATE TABLE "+groupsmembers_table+"("+
                    groupsmembers_group_sl_id + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    groupsmembers_group_id + " TEXT," +
                    groupsmembers_user_id + " TEXT," +
                    groupsmembers_last_rece_time + " INT DEFAULT 0," +
                    groupsmembers_last_seen_time + " INT DEFAULT 0," +
                    groupsmembers_member_type + " INTEGER,"+
                    groupsmembers_chat_selfdestruction + " INTEGER DEFAULT 0)";
    private static final String SQL_DELETE_GROUPMEMBERDATA =
            "DROP TABLE IF EXISTS " + groupsmembers_table;


    public static final String notification_table = "notification_data";
    public static final String notification_col_sr_no = "sr_no";
    public static final String notification_user_id = "user_id";
    public static final String notification_location = "location";
    public static final String notification_latitude = "latitude";
    public static final String notification_longitude = "longitude";
    //public static final String feed_col_username = "user_name";
    public static final String notification_timestamp = "timestamp";
    public static final String notification_type = "type";
    public static final String notification_status = "status";
    public static final String notification_text = "text";
    public static final String notification_image_url = "image_url";
    public static final String notification_link = "link";


//type 0 = simple text ...1=poke.....2=share...3=follow accept..4=chat accept 15=new group  21=block 22=unblock  11=follow request 12=chat request
    //26 follow request rejected //27 chat rejected
    private static final String SQL_CREATE_NOTIFICATION =
            "CREATE TABLE "+notification_table+"("+
                    notification_user_id + " TEXT NOT NULL,"+
                    notification_timestamp + " INTEGER NOT NULL,"+
                    notification_location + " TEXT,"+
                    notification_latitude + " REAL,"+
                    notification_longitude + " REAL,"+
                    notification_type + " INTEGER NOT NULL,"+
                    notification_status + " INTEGER,"+
                    notification_text + " TEXT," +
                    notification_image_url + " TEXT,"+
                    notification_link + " TEXT)";
    private static final String SQL_DELETE_NOTIFICATION =
            "DROP TABLE IF EXISTS " + notification_table;

    // media
    public static final String media_table = "media_info";
    public static final String media_sl_id = "media_sl_id";
    public static final String media_global_uri = "media_global_uri";
    public static final String media_local_uri = "media_local_uri";
    public static final String media_lite_global_uri = "media_lite_global";
    public static final String media_lite_local_uri = "media_lite_local";
    public static final String media_timestamp = "media_time";
    public static final String media_size = "media_size";


    private static final String SQL_CREATE_MEDIA =
            "CREATE TABLE "+media_table+"("+
                    media_sl_id + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    media_global_uri + " TEXT UNIQUE,"+
                    media_local_uri + " TEXT,"+
                    media_lite_global_uri + " TEXT UNIQUE,"+
                    media_lite_local_uri + " TEXT,"+
                    media_timestamp + " INTEGER NOT NULL,"+
                    media_size + " INTEGER DEFAULT 0)";
    private static final String SQL_DELETE_MEDIA =
            "DROP TABLE IF EXISTS " + media_table;




    public DatabaseAccesser(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_FEEDDATA);
        db.execSQL(SQL_CREATE_LOCATIONDATA);
        db.execSQL(SQL_CREATE_CHATDATA);
        db.execSQL(SQL_CREATE_USERDATA);
        db.execSQL(SQL_CREATE_CHATLISTDATA);
        db.execSQL(SQL_CREATE_NOTIFICATION);
        db.execSQL(SQL_CREATE_SEARCHDATA);
        db.execSQL(SQL_CREATE_GROUPMEMBERDATA);
        db.execSQL(SQL_CREATE_MEDIA);

        insertnullmedia(db);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_FEEDDATA);
        db.execSQL(SQL_DELETE_LOCATIONDATA);
        db.execSQL(SQL_DELETE_CHATDATA);
        db.execSQL(SQL_DELETE_USERDATA);
        db.execSQL(SQL_DELETE_CHATLISTDATA);
        db.execSQL(SQL_DELETE_NOTIFICATION);
        db.execSQL(SQL_DELETE_SEARCHDATA);
        db.execSQL(SQL_DELETE_GROUPMEMBERDATA);
        db.execSQL(SQL_DELETE_MEDIA);
        onCreate(db);
    }

    public static void insertnullmedia(SQLiteDatabase db)
    {

        ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.media_sl_id, "1");
        values.put(DatabaseAccesser.media_lite_global_uri, "null");
        values.put(DatabaseAccesser.media_lite_local_uri, "null");
        values.put(DatabaseAccesser.media_global_uri, "null");
        values.put(DatabaseAccesser.media_local_uri, "null");
        values.put(DatabaseAccesser.media_timestamp, System.currentTimeMillis());

        final long newRowId = db.insert(DatabaseAccesser.media_table, null, values);

    }

    public void resetDatabase()
    {
        //getWritableDatabase().execSQL(SQL_DELETE_USERDATA);
       // getWritableDatabase().execSQL(SQL_CREATE_USERDATA);
         getWritableDatabase().execSQL(SQL_DELETE_CHATDATA);
         getWritableDatabase().execSQL(SQL_CREATE_CHATDATA);
        //getWritableDatabase().execSQL(SQL_DELETE_GROUPMEMBERDATA);
        //getWritableDatabase().execSQL(SQL_CREATE_GROUPMEMBERDATA);
    }


}
