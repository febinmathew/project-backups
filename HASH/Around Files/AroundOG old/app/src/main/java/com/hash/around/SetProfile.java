package com.hash.around;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.hash.around.OtherClasses.DataTypes.ContactListData;
import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.OtherClasses.DataTypes.UserPrivacyData;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.OtherClasses.Interfaces.ProcessCompletedListner;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.vanniktech.emoji.EmojiEditText;
import com.vanniktech.emoji.EmojiManager;
import com.vanniktech.emoji.ios.IosEmojiProvider;


import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import static com.hash.around.MainServiceThread.fetchUserDetailsbyIDOrPhone;
import static com.hash.around.MainServiceThread.updateOrInsertUserIfNone;


public class SetProfile extends AppCompatActivity {
    UserPrivacyData privacyInfo=null;
    Toolbar toolbar;
    TextView locationtxt;

    EmojiEditText name;
    ImageView emoticon;
    ImageButton locationCheck;
    ProgressBar profileloading;

    CircularImageView circleProfileImage;
    ImageButton chooseButton;
    Button completeButton;
    boolean locationError;
    Uri directory = null;
    Uri outputUri;
    LocationManager locationManager;
    LocationListener listner;
    private StorageReference mStorageRef;
    FirebaseUser userdetail;
    DatabaseReference databaseRef;
    public SharedPreferences prefs;
    public SharedPreferences.Editor editor;
    double latitute;
    double longitude;
    String locationName;
    StorageReference riversRef;
    String provider;
    private LocationCallback mLocationCallback;
    private FusedLocationProviderClient mFusedLocationClient;
    LocationRequest mLocationRequest;


    void savePostToDatabase(ContactListData post,String globalUrl)
    {
        SQLiteDatabase db = tempDb.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.user_user_id, post.user_id);
        values.put(DatabaseAccesser.user_name, post.user_name);
        values.put(DatabaseAccesser.user_status, post.status);
        values.put(DatabaseAccesser.user_number, post.number);
        if(post.user_id.equals(userdetail.getUid()))
        {
            values.put(DatabaseAccesser.user_user_type, 0);
        }
        else
        {
            values.put(DatabaseAccesser.user_user_type, post.type);
        }
        values.put(DatabaseAccesser.user_globalUrl,globalUrl);
        values.put(DatabaseAccesser.user_localUrl, post.img_url);


        long newRowId = db.insert(DatabaseAccesser.user_table, null, values);
        db.close();

    }
    public DatabaseAccesser tempDb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        EmojiManager.install(new IosEmojiProvider());
        setContentView(R.layout.set_profile_activity);

        tempDb = new DatabaseAccesser(getApplicationContext());

        locationError = true;
        toolbar = (Toolbar) findViewById(R.id.commontoolbar);
        prefs = getSharedPreferences("user_preferences", MODE_PRIVATE);
        editor = prefs.edit();

        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Profile Setup");
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        locationtxt = (TextView) findViewById(R.id.locationEdit);
        circleProfileImage = (CircularImageView) findViewById(R.id.circular_image);
        //root = (RelativeLayout) findViewById(R.id.rootrelative_profilesetup);
        emoticon = (ImageView) findViewById(R.id.emotimg);
        name = (EmojiEditText) findViewById(R.id.profilenametext);
        /*actions = new EmojIconActions(getApplicationContext(), root, name, emoticon);
        actions.ShowEmojIcon();*/
        emoticon.setImageResource(R.drawable.ic_emoticon_semiblack);
        userdetail = FirebaseAuth.getInstance().getCurrentUser();
        databaseRef = FirebaseDatabase.getInstance().getReference();
        locationCheck=findViewById(R.id.locationCheck);
        profileloading=findViewById(R.id.profileloading);
        if (userdetail == null)
        {
            Intent intent = new Intent(this, RegisterActivity.class);
            SetProfile.this.finish();
            startActivity(intent);
        } else {
            Toast.makeText(this, userdetail.getUid(), Toast.LENGTH_SHORT).show();
        }
        mStorageRef = FirebaseStorage.getInstance().getReference();
        riversRef = mStorageRef.child("profile_images").child(randomStringGenerator() + ".png");
        chooseButton = (ImageButton) findViewById(R.id.imageButton1);

        completeButton = (Button) findViewById(R.id.completeButton);
        completeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                if (directory != null) {
                    riversRef.putFile(directory)
                            .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    // Get a URL to the uploaded content
                                    final Uri downloadUrl = taskSnapshot.getDownloadUrl();
                                    // downloadUrl.toString()

                                    final ContactListData map = new ContactListData();
                                    map.user_name = name.getText().toString();
                                    map.user_id = userdetail.getUid();
                                    map.type = 1;
                                    map.img_url = downloadUrl.toString();
                                    map.status = "null";
                                    map.location=locationName;
                                    map.number=userdetail.getPhoneNumber();
                                    map.music_url="null";


                                    Toast.makeText(SetProfile.this, locationName, Toast.LENGTH_SHORT).show();


                                    databaseRef.child("user_profile").child(userdetail.getUid()).setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful())
                                            {

                                                DatabaseReference ref = FirebaseDatabase.getInstance().getReference("user_location");
                                                GeoFire geoFire = new GeoFire(ref);
                                                geoFire.setLocation(userdetail.getUid(), new GeoLocation(latitute, longitude), new GeoFire.CompletionListener() {
                                                    @Override
                                                    public void onComplete(String key, DatabaseError error) {
                                                        if (error != null) {
                                                            Toast.makeText(getApplicationContext(), "Error writing location", Toast.LENGTH_SHORT).show();
                                                        } else {
                                                            //Toast.makeText(getApplicationContext(), "Success location", Toast.LENGTH_SHORT).show();

                                                            map.img_url=directory.toString();
                                                            //directory =
                                                            map.type=0;
                                                            savePostToDatabase(map,downloadUrl.toString());
                                                            TabbedActivity.saveContactMusicToDatabase(map.user_id,"null",null);
                                                            movetotabbedactivity();
                                                            //map.type=1;


                                                            editor.putString("location_name", locationName);
                                                            editor.putString("location_latitude", String.valueOf(latitute));
                                                            editor.putString("location_longitude", String.valueOf(longitude));
                                                        }
                                                    }
                                                });


                                                /*HashMap privacymap = new HashMap();
                                                privacymap.put("profile_picture",0);
                                                privacymap.put("profile_status",0);
                                                privacymap.put("number",0);
                                                privacymap.put("activity_status",0);
                                                privacymap.put("location",1);
                                                privacymap.put("anyonefollow",0);
                                                privacymap.put("anyonemessage",0);*/


                                                if(privacyInfo==null) {
                                                    privacyInfo = new UserPrivacyData();
                                                    privacyInfo.profile_picture = 0;
                                                    privacyInfo.profile_status = 0;
                                                    privacyInfo.number = 0;
                                                    privacyInfo.activity_status = 0;
                                                    privacyInfo.location = 1;
                                                    privacyInfo.anyonefollow = 0;
                                                    privacyInfo.anyonemessage = 0;
                                                    privacyInfo.profile_music = 0;
                                                }
                                                DatabaseReference databaseRef;
                                                databaseRef = FirebaseDatabase.getInstance().getReference();

                                                databaseRef.child("user_profile").child(userdetail.getUid()).child("user_privacy").setValue(privacyInfo).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (task.isSuccessful()) {
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    });


                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    Toast.makeText(SetProfile.this, exception.getMessage() + exception.toString(), Toast.LENGTH_LONG).show();

                                    // ...
                                }
                            });


                } else {
                    Toast.makeText(SetProfile.this, "Please Choose an image", Toast.LENGTH_SHORT).show();
                }

            }
        });
        chooseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(SetProfile.this, "Not granted", Toast.LENGTH_SHORT).show();
                        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.INTERNET}, 11);

                        return;
                    }

                }

                pickFromGallery();

            }
        });

        /*actions.setKeyboardListener(new EmojIconActions.KeyboardListener() {
            @Override
            public void onKeyboardOpen() {
               /// Log.e("Keyboard","open");
            }

            @Override
            public void onKeyboardClose() {
               // Log.e("Keyboard","close");
            }
        });*/

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        final Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        if (android.support.v4.app.ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && android.support.v4.app.ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = locationManager.getLastKnownLocation(provider);

        //Toast.makeText(SetProfile.this, "Default :"+location.getLatitude() + "/" + location.getLongitude(), Toast.LENGTH_SHORT).show();

        /*******Playyyyyyyservices*/

       /* mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            Toast.makeText(SetProfile.this, "Default :"+location.getLatitude() + "/" + location.getLongitude(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });*/


        /*******Playyyyyyyservices*/
        listner = new LocationListener() {
            public void onStatusChanged(String provider, int status, Bundle extras) {
                Toast.makeText(SetProfile.this, "Enthokkeyo", Toast.LENGTH_SHORT).show();
                // called when the location provider status changes. Possible status: OUT_OF_SERVICE, TEMPORARILY_UNAVAILABLE or AVAILABLE.
            }

            public void onProviderEnabled(String provider) {
                // called when the location provider is enabled by the user

                Toast.makeText(SetProfile.this, "Enabled", Toast.LENGTH_SHORT).show();
            }

            public void onProviderDisabled(String provider) {
                Toast.makeText(SetProfile.this, "Disabled", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
                // called when the location provider is disabled by the user. If it is already disabled, it's called immediately after requestLocationUpdates
            }

            public double round(double value, int places) {
                if (places < 0) throw new IllegalArgumentException();

                long factor = (long) Math.pow(10, places);
                value = value * factor;
                long tmp = Math.round(value);
                return (double) tmp / factor;
            }

            public void onLocationChanged(Location location) {
                latitute = location.getLatitude();
                longitude = location.getLongitude();

                editor.putString("location_latitude", String.valueOf(latitute));
                editor.putString("location_longitude", String.valueOf(longitude));

                Toast.makeText(SetProfile.this, latitute + "/" + longitude, Toast.LENGTH_SHORT).show();

                locationtxt.setText(latitute + "/" + longitude);

            }
        };
        locationCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationfinder();
            }
        });
        locationfinder();
        preFetchDataFromDatabaseIfAvailable();

    }
    void preFetchDataFromDatabaseIfAvailable()
    {
        fetchUserDetailsbyIDOrPhone(userdetail.getUid(),true,new ProcessCompletedListner() {
            @Override
            public void processCompleteAction() {
            }

            @Override
            public void newUserAdded(UserWholeData data) {

                name.setText(data.user_name);
                privacyInfo=data.getUserPrivacyData();
                installUserPrivacyToSettings(privacyInfo);
                profileloading.setVisibility(View.VISIBLE);
                MainServiceThread.downloadFileAndReturnUriString(data.img_url, new JobCompletionListner() {
                    @Override
                    public void onJobCompletion(String txt1) {

                        directory = Uri.parse(txt1);
                        circleProfileImage.setImageURI(directory);
                        profileloading.setVisibility(View.GONE);

                    }
                });

            }
        });

    }

    void installUserPrivacyToSettings(UserPrivacyData privacyData)
    {
        //CODE 47 create codes to save user preference from web
    }
    public void fetchAnotherName(Location location)
    {
        Geocoder gcd = new Geocoder(getApplicationContext(), Locale.getDefault());
        List<Address> addresses = null;

        try {
            addresses = gcd.getFromLocation(
                    location.getLatitude(),
                    location.getLongitude(),
                    // In this sample, get just a single address.
                    1);
        } catch (IOException ioException) {
            // Catch network or other I/O problems.
            Toast.makeText(SetProfile.this, "Service no avaialble", Toast.LENGTH_SHORT).show();
        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
            Toast.makeText(SetProfile.this, "invalid latlong", Toast.LENGTH_SHORT).show();
        }

        // Handle case where no address was found.
        if (addresses == null || addresses.size() == 0) {
            Toast.makeText(SetProfile.this, "no results", Toast.LENGTH_SHORT).show();
        } else {


            //locationtxt.setText(" ");
            if (addresses.size() > 0) {
                String tempText=addresses.get(0).getLocality();
                if(tempText==null)
                    tempText=addresses.get(0).getFeatureName();

                if(tempText==null)
                {
                    Toast.makeText(SetProfile.this, "nextcall", Toast.LENGTH_SHORT).show();
                    location.setLatitude(location.getLatitude()+Math.random()%0.001999f);
                    location.setLongitude(location.getLongitude()+Math.random()%0.001999f);
                    fetchAnotherName(location);
                    return;
                }
                else
                {

                    latitute = location.getLatitude();
                    longitude = location.getLongitude();
                    editor.putString("location_latitude", String.valueOf(latitute));
                    editor.putString("location_longitude", String.valueOf(longitude));

                    locationtxt.setText(tempText+", " + addresses.get(0).getCountryName());

                    editor.putString("location_name", locationtxt.getText().toString());
                    locationName=locationtxt.getText().toString();
                }
                ///DEPRETIATED
            }


            Toast.makeText(SetProfile.this, "success", Toast.LENGTH_SHORT).show();
        }
    }

    public void movetotabbedactivity() {
        Intent intent = new Intent(this, TabbedActivity.class);
        SetProfile.this.finish();
        startActivity(intent);


    }

    public String randomStringGenerator() {
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (
                int i = 0;
                i < 20; i++)

        {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == RESULT_OK) {
            try {
                directory = data.getData();

                circleProfileImage.setImageURI(directory);

            } catch (Exception e) {
                Toast.makeText(this, e.getMessage() + e.toString(), Toast.LENGTH_LONG).show();
            }

            //Toast.makeText(this, directory, Toast.LENGTH_LONG).show();
        }


    }


    private void pickFromGallery() {
       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE,
                    getString(R.string.permission_read_storage_rationale),
                    REQUEST_STORAGE_READ_ACCESS_PERMISSION);
        }*/

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, "GET FILE"), 1);

        // Crop.pickImage(SetProfile.this);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 10:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //locationok();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                            // locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0, listner);

                        }
                    }
                }
                break;
            case 11:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //locationok();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                                checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                                checkSelfPermission(Manifest.permission.INTERNET) == PackageManager.PERMISSION_GRANTED) {


                        }
                    }
                }
                break;

        }
    }

    public void locationfinder()
    {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                    checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Not granted", Toast.LENGTH_SHORT).show();
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 10);
                return;

            }
        }
        fetchLocation();

    }

    public void findloc() {
        if (isLocationEnabled()) {
            Toast.makeText(this, "everythings perfect", Toast.LENGTH_SHORT).show();
            toggleGPSUpdates();
        } else {
            Toast.makeText(this, "showalert", Toast.LENGTH_SHORT).show();
            showAlert();
        }
    }


    public void fetchLocation() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {


                Location location=locationResult.getLastLocation();


                mFusedLocationClient.removeLocationUpdates(mLocationCallback);

                fetchAnotherName(location);



            }
        };

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (android.support.v4.app.ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && android.support.v4.app.ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                mLocationCallback,
                null /* Looper */);
    }


    private boolean isLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }


    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        toggleGPSUpdates();
                    }
                });
        dialog.show();
    }



    public void toggleGPSUpdates() {
        /*if(!checkLocation())
            return;
        Button button = (Button) view;
        if(button.getText().equals(getResources().getString(R.string.pause))) {
            locationManager.removeUpdates(locationListenerGPS);
            button.setText(R.string.resume);
        }*/


        //  button.setText(R.string.pause);

        //Toast.makeText(this, "everythings perfect", Toast.LENGTH_SHORT).show();


        if (android.support.v4.app.ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && android.support.v4.app.ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(provider, 400, 1, listner);
           /* locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, 2 * 60*1000, 10, listner);*/
        // button.setText(R.string.pause);


    }
}