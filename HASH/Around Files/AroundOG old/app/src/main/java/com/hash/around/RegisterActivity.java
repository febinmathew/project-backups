package com.hash.around;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.OtherClasses.Interfaces.JobCompletionWithFailureListner;
import com.hash.around.OtherClasses.Interfaces.ProcessCompletedListner;
import com.hash.around.OtherClasses.Interfaces.TimerListner;
import com.hash.around.Views.AroundChatInputSection;

import java.lang.Object;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static android.view.View.GONE;
import static com.hash.around.Utils.CountryListLoader.getRegionDisplayName;
import static com.hash.around.Views.ViewFunctions.independantAnimateIn;
import static com.hash.around.Views.ViewFunctions.slideLeft;
import static com.hash.around.Views.ViewFunctions.slideRight;

public class RegisterActivity extends AppCompatActivity {

    TextView countrySpinner,editNumber,resend,errorText,verificationInfoText;
    Toolbar toolbar;
    Button nextButton,reg_verify_button;
    //TextInputLayout codelayout;
    EditText countrycode;
    EditText phonenumber, codedata;
    Spinner spinner;
    boolean CountryChoosed;
    String result, mVerificationId;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private FirebaseAuth mAuth;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    AroundChatInputSection.RecordTimer smsTimer=new AroundChatInputSection.RecordTimer();
    TextView smsTimerText;
    TimerListner smsTimerListner=new TimerListner() {
        @Override
        public void onTimerEnd(Long timestamp, String time) {

            nextSMSRequestEnable(true);
        }

        @Override
        public void onSpecifiedIntervel(Long timestamp, String time) {
            smsTimerText.setText(String.format("| in %s seconds",time));
        }
    };

    LinearLayout numberInputSection,verificationSection;

    void nextSMSRequestEnable(boolean enable)
    {
        if(enable)
        {
            resend.setEnabled(true);
            resend.setTextColor(getResources().getColor(R.color.blue_500));
            smsTimerText.setVisibility(GONE);
            nextButton.setEnabled(true);
            nextButton.setTextColor(getResources().getColor(R.color.white));

        }
        else
        {
            smsTimer.setTimerListner(1000 * 59L, null,smsTimerListner);
            smsTimer.setReverse(true);
            smsTimer.display();
            resend.setEnabled(false);
            resend.setTextColor(getResources().getColor(R.color.grey_400));
            smsTimerText.setVisibility(View.VISIBLE);
            nextButton.setEnabled(false);
            nextButton.setTextColor(getResources().getColor(R.color.grey_400));
        }
    }
    String finalPhoneNumber=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_activity);

        CountryChoosed = false;
        if (savedInstanceState != null) {
            onRestoreInstanceState(savedInstanceState);
        }

        toolbar = (Toolbar) findViewById(R.id.commontoolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

//     getSupportActionBar().setTitle("Register");
        mAuth = FirebaseAuth.getInstance();
        spinner = (Spinner) findViewById(R.id.statsSpin);
        nextButton = (Button) findViewById(R.id.reg_next_button);

        reg_verify_button = (Button) findViewById(R.id.reg_verify_button);
        resend=(TextView) findViewById(R.id.reg_resend_button);
        resend.setPaintFlags(resend.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        //codelayout = (TextInputLayout) findViewById(R.id.codetext);
        phonenumber = (EditText) findViewById(R.id.phnumber);
        codedata = (EditText) findViewById(R.id.codetextdata);
        countrycode = (EditText) findViewById(R.id.countrycode);
        countrySpinner = findViewById(R.id.countrySpinner);
        numberInputSection = findViewById(R.id.numberInputSection);
        verificationSection = findViewById(R.id.verificationSection);
        editNumber=findViewById(R.id.editNumber);
        editNumber.setPaintFlags(editNumber.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        errorText=findViewById(R.id.errorText);
        verificationInfoText=findViewById(R.id.verificationInfoText);
        smsTimerText=findViewById(R.id.smsTimerText);
        nextSMSRequestEnable(true);
        showVerificationSection(false);
        reg_verify_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(codedata.getText())) {
                    PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, codedata.getText().toString());
                    signInWithPhoneAuthCredential(credential);
                }
                else
                {
                    errorText.setVisibility(View.VISIBLE);
                    errorText.setText("Enter the code.");
                }
                //showPhoneNumberIncorrectDialog(RegisterActivity.this,"");
            }
        });
        editNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showVerificationSection(false);
            }
        });
        //countrySpinner.setdrawa
        for (Drawable xx : countrySpinner.getCompoundDrawables()) {
            if (xx != null)
                xx.setColorFilter(ContextCompat.getColor(this, R.color.black), android.graphics.PorterDuff.Mode.SRC_IN);
        }
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    CountryChoosed = false;
                } else {
                    CountryChoosed = true;
                    result = getResources().getStringArray(R.array.countrys_result)[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
               codedata.setText(credential.getSmsCode());
               //signInWithPhoneAuthCredential(credential);
                signInWithPhoneAuthCredential(credential);
                Toast.makeText(RegisterActivity.this, "Already Completed", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onVerificationFailed(FirebaseException e) {

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    Toast.makeText(RegisterActivity.this, "Invalid Request / " + e.toString(), Toast.LENGTH_SHORT).show();

                } else if (e instanceof FirebaseTooManyRequestsException) {
                    Toast.makeText(RegisterActivity.this, "Please wait some time before senting next request", Toast.LENGTH_SHORT).show();
                } else if (e instanceof FirebaseNetworkException) {
                    Toast.makeText(RegisterActivity.this, "Not connected to network", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                showVerificationSection(true);

                mResendToken = token;

                mVerificationId = verificationId;
                //nextButton.setTag(1);
                //nextButton.setText("VERIFY");
                Toast.makeText(RegisterActivity.this, "OTP sent", Toast.LENGTH_SHORT).show();
                nextSMSRequestEnable(false);
                //codelayout.setVisibility(View.VISIBLE);
                //resend.setVisibility(View.VISIBLE);
            }
        };

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               String user_phonenumbder=phonenumber.getText().toString();
               String user_countrycode=countrycode.getText().toString();

                if (TextUtils.isEmpty(phonenumber.getText())) {
                    Toast.makeText(RegisterActivity.this,"Please specify the Phonenumber", Toast.LENGTH_LONG).show();
                    return;
                }

                else if (TextUtils.isEmpty(countrycode.getText())) {
                    Toast.makeText(RegisterActivity.this, "Please specify the Country code", Toast.LENGTH_LONG).show();
                    return;
                }

/*error*/

                             else {
                    Log.e("MGG","number ");
                    PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
                    Phonenumber.PhoneNumber swissNumberProto=null;
                    String regionCode=phoneUtil.getRegionCodeForCountryCode(Integer.parseInt(user_countrycode));

                    String countryName=countrySpinner.getText().toString();
                    Log.e("MGG","number "+user_phonenumbder+"/"+regionCode);
                    try {
                        swissNumberProto=phoneUtil.parse(user_phonenumbder, regionCode);

                        finalPhoneNumber=phoneUtil.format(swissNumberProto, PhoneNumberUtil.PhoneNumberFormat.E164);
                        Log.e("MGG","number "+finalPhoneNumber);

                    } catch (Exception e) {
                        e.printStackTrace();
                        finalPhoneNumber="+"+user_countrycode+user_phonenumbder;
                    }

                    //final String phone2=finalPhoneNumber;
                    if(swissNumberProto!=null)
                    {
                        if(phoneUtil.isValidNumber(swissNumberProto))
                        {
                            showConfirmationDialog(RegisterActivity.this,"Confirm?",
                                    String.format("We are senting an OTP to %s. Make sure that your device is ready to receive " +
                                            "the SMS.",finalPhoneNumber),
                                    new JobCompletionWithFailureListner() {
                                        @Override
                                        public void onJobFailed(String txt1) {
                                            phonenumber.requestFocus();
                                        }

                                        @Override
                                        public void onJobCompletion(String txt1) {
                                            verificationInfoText.setText(String.format("OTP has been sent to the number %s",finalPhoneNumber));
                                            startPhoneNumberVerification(finalPhoneNumber);
                                    }});
                        }else
                        {
                            showPhoneNumberIncorrectDialog(RegisterActivity.this,
                                    "Invalid number",String.format("%s is not a valid number in %s. Please enter a valid one.",
                                            finalPhoneNumber,countryName));
                        }
                    }
                    else
                    {
                        showPhoneNumberIncorrectDialog(RegisterActivity.this,
                                "Invalid number",String.format("%s is not a valid number.",
                                        finalPhoneNumber));
                    }

                }




                       /* try {
                            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, codedata.getText().toString());
                            signInWithPhoneAuthCredential(credential);
                        } catch (Exception e) {
                            Toast.makeText(RegisterActivity.this, e.getMessage() + e.toString(), Toast.LENGTH_LONG).show();
                        }*/





            }
        });


        nextButton.setTag(0);

        resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                        if(finalPhoneNumber!=null)
                        resendPhoneNumberVerification(finalPhoneNumber, mResendToken);

            }
        });


        //getPhoneDeatils();
        countrySpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent countryList = new Intent(RegisterActivity.this, CountriesListActivity.class);
                startActivityForResult(countryList, 1);
            }
        });


        countryCodeWather = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Integer code;
                try {
                    code = Integer.parseInt(s.toString());
                } catch (Exception e) {
                    //Log.e("MGG","Choose country: ");
                    countrySpinner.setText("Choose country");
                    //countrycode.setText("");
                    return;
                }
                //Log.e("MGG","Code val: "+code);
                String nick = PhoneNumberUtil.getInstance().getRegionCodeForCountryCode(code);

                String name = getRegionDisplayName(nick);

                if ((nick == null) || (name == null)) {
                    Log.e("MGG", "unkown area: ");
                } else {
                    Log.e("MGG", "searcResult: " + nick + " / " + name);
                    countrySpinner.setText(name);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        countrycode.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus)
                    countrycode.addTextChangedListener(countryCodeWather);
            }
        });

       /* if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        else {*/
           // final String localNumber = ((TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE)).getLine1Number();

        //String simCountryIso = ((TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE)).getSimCountryIso();

     //}

        ///if (TextUtils.isEmpty(localNumber)) return Optional.absent();


initializeNumber();

    }

    public static boolean checkIsValidNumber(String number) {
        return number.matches("^\\+[0-9]{10,}")  ||
                number.matches("^\\+685[0-9]{5}") ||
                number.matches("^\\+376[0-9]{6}") ||
                number.matches("^\\+299[0-9]{6}") ||
                number.matches("^\\+597[0-9]{6}") ||
                number.matches("^\\+298[0-9]{6}") ||
                number.matches("^\\+240[0-9]{6}") ||
                number.matches("^\\+687[0-9]{6}") ||
                number.matches("^\\+689[0-9]{6}");
    }

    void showPhoneNumberIncorrectDialog(Context context, String title, String message)
    {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle(Html.fromHtml(title));
        dialog.setMessage(message);
        dialog.setIcon(R.drawable.ic_report_red);
        dialog.setPositiveButton(android.R.string.ok, null);
        dialog.show();
    }

    public void showConfirmationDialog(Context context, String title, String message,final JobCompletionWithFailureListner listner) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle(Html.fromHtml(title));
        dialog.setMessage(message);

        dialog.setIcon(R.drawable.ic_info_semiblack);
        dialog.setPositiveButton("CONFIRM", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                listner.onJobCompletion("");
                dialog.dismiss();

            }
        });
        dialog.setNegativeButton("EDIT", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                listner.onJobFailed("");
                dialog.dismiss();
            }
        });

        AlertDialog dd=dialog.show();
        //dd.getme
    }

    private void initializeNumber() {
        String simCountryIso = ((TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE)).getSimCountryIso();
        if(simCountryIso != null ) {
            simCountryIso = simCountryIso.toUpperCase();


            int code = PhoneNumberUtil.getInstance().getCountryCodeForRegion(simCountryIso);

            String name = getRegionDisplayName(simCountryIso);
            if(!(name!=null || code !=0 || !name.equals(""))) {
                countrySpinner.setText(name);
               countrycode.setText(code + "");
                Log.e("MGG", "goot " + code);
            }

        }
    }

    void showVerificationSection(boolean show)
    {
        if(show && verificationSection.getVisibility()==View.GONE)
        {
            slideLeft(numberInputSection, getApplicationContext(), 1000,new JobCompletionListner() {
                @Override
                public void onJobCompletion(String txt1) {
                    independantAnimateIn(verificationSection,getApplicationContext());
                }
            });

        }
        else if(!show && numberInputSection.getVisibility()==View.GONE)
        {
            slideRight(verificationSection, getApplicationContext(), 800,new JobCompletionListner() {
                @Override
                public void onJobCompletion(String txt1) {
                    independantAnimateIn(numberInputSection,getApplicationContext());
                }
            });
            /*numberInputSection.setVisibility(View.VISIBLE);
            verificationSection.setVisibility(View.GONE);*/
        }
    }

    TextWatcher countryCodeWather;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK && requestCode==1)
        {
            String country_name=data.getStringExtra("country_name");
            int country_code=data.getIntExtra("country_code", 0);
            //countrycode.removeTextChangedListener();
            countrycode.removeTextChangedListener(countryCodeWather);
            phonenumber.requestFocus();
            countrySpinner.setText(country_name);
            countrycode.setText(country_code+"");
        }
    }

    public void startPhoneNumberVerification(String phno)
    {

          // OnVerificationStateChangedCallbacks

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phno,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);

        Toast.makeText(this, "No : "+phno, Toast.LENGTH_SHORT).show();
    }


    public void resendPhoneNumberVerification(String phno,PhoneAuthProvider.ForceResendingToken token)
    {

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phno,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,              // Activity (for callback binding)
                mCallbacks,
                token);       // OnVerificationStateChangedCallbacks

        Toast.makeText(this, "Re : "+phno, Toast.LENGTH_SHORT).show();
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(Task<AuthResult> task) {


                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                // Log.d(TAG, "signInWithCredential:success");
                                Toast.makeText(RegisterActivity.this, "Verification SuCCESS", Toast.LENGTH_SHORT).show();

                                moveToProfileSetup();
                                // FirebaseUser user = task.getResult().getUser();
                                // [START_EXCLUDE]
                                //updateUI(STATE_SIGNIN_SUCCESS, user);
                                // [END_EXCLUDE]
                            } else {
                                // Sign in failed, display a message and update the UI
                                // Log.w(TAG, "signInWithCredential:failure", task.getException());
                                errorText.setVisibility(View.VISIBLE);
                                errorText.setText("The OTP you entered is incorrect. Try resending the code, or wait until yo get the right OTP");
                                mAuth.signOut();
                                // [START_EXCLUDE silent]
                                // Update UI
                                // updateUI(STATE_SIGNIN_FAILED);
                                // [END_EXCLUDE]
                            }


                        }


                });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_help) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    void moveToProfileSetup()
    {

        Intent intent=new Intent(this,SetProfile.class);
        RegisterActivity.this.finish();
        startActivity(intent);
    }


}
