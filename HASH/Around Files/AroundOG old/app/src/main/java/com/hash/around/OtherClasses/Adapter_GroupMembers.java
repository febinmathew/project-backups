package com.hash.around.OtherClasses;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hash.around.ChatWindow;
import com.hash.around.OtherClasses.DataTypes.ContactListData;
import com.hash.around.OtherClasses.DataTypes.GroupMemberData;
import com.hash.around.Profile_Activity;
import com.hash.around.R;
import com.hash.around.TabbedActivity;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;

//import android.support.v7.view.ActionMode;

/**
 * Created by Febin on 26-08-2017.
 */

public class Adapter_GroupMembers extends RecyclerView.Adapter<Adapter_GroupMembers.ViewHolder> implements View.OnCreateContextMenuListener{

    Context mContext;
    public ArrayList<ContactListData> contactdata;
    ArrayList<GroupMemberData> memberdata;
    public static int optionPosition=-1;


    public Adapter_GroupMembers(Context mcont, ArrayList<ContactListData> contact, ArrayList<GroupMemberData> member)
    {
        mContext=mcont;
        this.contactdata=contact;
        this.memberdata=member;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;

                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.single_contact_view, parent, false);
                ViewHolder vh = new ViewHolder(v);
                return vh;


    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        final int pos = position;
        final RelativeLayout rView = holder.wholeView;

        holder.imageSelectionTick.setVisibility(View.GONE);
        holder.name.setText(contactdata.get(position).user_name);
        String status=contactdata.get(position).status;
        if(status.equals("null"))
            status="Wanna know an App called Around?";

        if(contactdata.get(position).type==1)
            holder.adminstat.setText("ADMIN");
        else
            holder.adminstat.setText("");

        holder.status.setText(status);

        String dir=contactdata.get(position).img_url;
        if(dir.equals("null"))
        {
            holder.proimg.setImageResource(R.drawable.nick);
        }
        else
        {
            holder.proimg.setImageURI(Uri.parse(dir));
        }

        holder.wholeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    Context cont = mContext;
                    Intent indi = new Intent(cont, ChatWindow.class);
                    indi.putExtra("userid", contactdata.get(pos).user_id);
                    indi.putExtra("username", contactdata.get(pos).user_name);
                    ((AppCompatActivity) mContext).finish();
                    cont.startActivity(indi);

            }
        });

        holder.wholeView.setOnCreateContextMenuListener(this);
        holder.wholeView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
            Log.e("MSGG","long click");

                        optionPosition = pos;
                        v.showContextMenu();

                return true;
            }
        });

    }




    @Override
    public int getItemCount() {
        if(memberdata!=null) {
            return memberdata.size();
        }
        return 0;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        menu.clear();
        MenuInflater inflater = new MenuInflater(mContext);
        inflater.inflate(R.menu.groupmembers_pop_up, menu);
        // menu.setHeaderTitle(data.get(optionPosition).name);
        //menu.setQwertyMode(true);

        menu.findItem(R.id.member_admin).setVisible(false);
        menu.findItem(R.id.member_remove).setVisible(false);


        Toast.makeText(mContext, "Privilage "+ Profile_Activity.userPrivilage, Toast.LENGTH_SHORT).show();

            if(Profile_Activity.userPrivilage==null)
            {

            }
            else if(Profile_Activity.userPrivilage==1)
            {
                menu.findItem(R.id.member_admin).setVisible(true);
                if(contactdata.get(optionPosition).type==0)
                    menu.findItem(R.id.member_admin).setTitle("Make admin");
                else
                    menu.findItem(R.id.member_admin).setTitle("Revoke admin");



                if(contactdata.get(optionPosition).user_id.equals(TabbedActivity.userdetail.getUid())) {
                    menu.findItem(R.id.member_remove).setVisible(false);
                }
                else
                {
                    menu.findItem(R.id.member_remove).setVisible(true);
                    menu.findItem(R.id.member_remove).setTitle("Remove "+(contactdata.get(optionPosition).user_name));
                }
            }
            else
            {


            }



        if(contactdata.get(optionPosition).user_id.equals(TabbedActivity.userdetail.getUid())) {

            menu.findItem(R.id.chat_message).setVisible(false);

            menu.findItem(R.id.view_profile).setTitle("My profile");

        }


        //menu.findItem(R.id.chat_message).setTitle("Message "+(contactdata.get(optionPosition).user_name));
        //menu.findItem(R.id.member_remove).setTitle("Remove "+(contactdata.get(optionPosition).user_name));


    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name;
        RelativeLayout wholeView;

        public TextView status;
        public ImageView proimg;
        public Drawable temp=null;

        public TextView adminstat;

        public CircularImageView imageSelectionTick;
        public ViewHolder(View item)
        {
            super(item);
            name=(TextView)item.findViewById(R.id.contactName);
            status=(TextView)item.findViewById(R.id.contactStatus);
            proimg=(ImageView) item.findViewById(R.id.contactImg);

            adminstat=(TextView)item.findViewById(R.id.chatDate);

            imageSelectionTick=(CircularImageView) item.findViewById(R.id.imageSelectionTick);
            wholeView=(RelativeLayout)item.findViewById(R.id.singleContact);



        }
    }

}


