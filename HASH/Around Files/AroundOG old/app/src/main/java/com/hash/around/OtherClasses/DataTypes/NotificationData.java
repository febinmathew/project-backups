package com.hash.around.OtherClasses.DataTypes;

/**
 * Created by Febin on 26-08-2017.
 */
import java.io.Serializable;

public class NotificationData  {

    public long rowId;
    public String userid;
    public String location;
    public double latitude;
    public double longitude;
    public long timestamp;
    public int type;
    public int status;
    public String text;
    public String image="null";
    public String link;



}


