package com.hash.around;

import android.app.ActivityOptions;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
//import com.hash.around.TabFragments.TabFragment2.get;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.util.Log;
import android.util.Pair;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import com.hash.around.OtherClasses.DataTypes.ChatData;
import com.hash.around.OtherClasses.ChatDataAdapter;
import com.hash.around.OtherClasses.DataTypes.MediaData;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.OtherClasses.Interfaces.ChatSchedulerListener;
import com.hash.around.OtherClasses.Interfaces.ChatSentListner;
import com.hash.around.OtherClasses.DataTypes.GroupChatData;
import com.hash.around.Views.CustomMakers.ChatView_DateHeader;
import com.hash.around.Views.CustomMakers.ChatView_ItemHeader;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.OtherClasses.Interfaces.TimerListner;
import com.hash.around.OtherClasses.RecyclerViewTouchListner;
import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.hash.around.Utils.ChatSender;
import com.hash.around.Utils.DatabaseManager;
import com.hash.around.Utils.MediaManager;
import com.hash.around.Utils.LocalNotificationManager;
import com.hash.around.Utils.NotifyTimer;
import com.hash.around.Utils.StringUtils;
import com.hash.around.Views.AroundChatInputSection;
import com.hash.around.Views.AttachmentTypeSelector;
import com.hash.around.Views.CustomMakers.ChatView_UnseenHeader;
import com.hash.around.Views.Medias.AroundCircularImageView;
import com.hash.around.Views.TimerWithTextView;
import com.hash.around.Views.ViewFunctions;
import com.vanniktech.emoji.EmojiEditText;
import com.vanniktech.emoji.EmojiManager;
import com.vanniktech.emoji.ios.IosEmojiProvider;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;


import static com.hash.around.OtherClasses.Adapter_Contacts.checkIfPrivacyShouldBeActivie;

public class ChatWindow extends AppCompatActivity {

    class ChatWindowReceivers extends BroadcastReceiver{

        public ChatWindowReceivers()
        {
            registerReceiver(this, new IntentFilter("around.NEWCHAT"));
        }
        ArrayList<MediaData> receiverData;
        @Override
        public void onReceive(Context context, Intent intent) {
            receiverData=(ArrayList<MediaData>)intent.getSerializableExtra("mediaarray");
            for(int i=0;i<receiverData.size();i++)
            {

                addNewQuickChat(receiverData.get(i).type,receiverData.get(i).uri,receiverData.get(i).text);

            }
            Log.e("MGG","listner recievedd "+receiverData.size());
        }

    }

    boolean multiSelectChat=false;
    public static ActionMode chatActionMode=null;
    private ActionMode.Callback chatActionModeCallback;
    private int chatActionModeType=0;   // 1 search
    // EmojiconEditText name;

    View root;
    //EmojIconActions actions;

    public static RecyclerView chatView;
    static FirebaseUser userdetail;
    private LinearLayoutManager mLayoutManager;

    LinearLayout profileTitle;

    public static ChatDataAdapter adap;

    public static String uId;
    TextView chatUserName;
    AroundCircularImageView chatUserImage;
    TextView online_status;
    static int userType;

    UserWholeData userData;
    Integer userPrivilage=null;
    public static ArrayList<GroupChatData> finaldata=new ArrayList<GroupChatData>();

    boolean isChat=true;
    View chatTypeBlock;
    AroundChatInputSection chatTypeSection;
    public static ChatWindow chatwindowInstance;

    private View                        scrollToBottomButton;
    private TextView                    scrollDateHeader;

public static ChatWindow instance;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance=this;
        LocalNotificationManager.cancelChatNotitication();
        //Log.e("MGG","mp3 "+ MimeTypeMap.getSingleton().getMimeTypeFromExtension("mp3"));
        new ChatWindowReceivers();
        chatwindowInstance=this;
        EmojiManager.install(new IosEmojiProvider());
        setContentView(R.layout.chat_window);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);


        ActionBar ab=getSupportActionBar();

        if (ab != null) {
            // ab.setLogo(R.mipmap.ic_launcher);

            //ab.setDisplayUseLogoEnabled(true);
            ab.setTitle(null);
            ab.setDisplayShowTitleEnabled(false);
            //ab.setDisplayShowCustomEnabled(false);
            ab.setDisplayHomeAsUpEnabled(false);
            toolbar.findViewById(R.id.up_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ChatWindow.super.onBackPressed();
                }
            });
            //ab.setHomeAsUpIndicator(R.drawable.ic_arrow_left_white_50dp);
            //ab.setLogo(R.mipmap.logo1);

            // ab.setDisplayHomeAsUpEnabled(false);
            //ab.setDisplayShowHomeEnabled(true);
        }



        chatUserName=(TextView)findViewById(R.id.chatUserName);

        profileTitle=(LinearLayout)findViewById(R.id.conversation_image);
        online_status=(TextView)findViewById(R.id.online_status);
        chatUserImage=(AroundCircularImageView) findViewById(R.id.chatUserImage);
        chatView=(RecyclerView)findViewById(R.id.rView_message_list);

        scrollToBottomButton =  findViewById (R.id.scroll_to_bottom_button);
        scrollToBottomButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    chatView.smoothScrollToPosition(finaldata.size()-1);

            }
        });
        scrollDateHeader     =  findViewById(R.id.scroll_date_header);
        ConversationScrollListener conversationDateHeader   = new ConversationScrollListener(this);
        //RecyclerView.OnScrollListener scrollListener = new ConversationScrollListener(this);
        chatView.addOnScrollListener(conversationDateHeader);

        // getSupportActionBar().setIcon(R.drawable.ic_action_keyboard);
        //getSupportActionBar().setIcon(R.drawable.nick);

       /* FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        uId=getIntent().getStringExtra("userid");
//        startForeground();

        //uName=getIntent().getStringExtra("username");
        //userType=getIntent().getIntExtra("user_type",1);


        userData=returnSingleUserDetails(uId);

        userType=userData.type;
        chatUserName.setText(userData.user_name);

        if (checkIfPrivacyShouldBeActivie(userData.priv_profile_picture,userData.remoteContact))
        {
            if(userData.img_url.equals("null"))
            {
                chatUserImage.setImageResource(R.drawable.default_user);
            }
            else
            {
                chatUserImage.setImageURI(Uri.parse(userData.img_url));
            }
        }
        else
        {
            chatUserImage.setImageResource(R.drawable.default_user);
        }


        profileTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                viewSelectedUserProfile(uId);
                //Toast.makeText(con, "Happesns", Toast.LENGTH_SHORT).show();


            }
        });

        userdetail= FirebaseAuth.getInstance().getCurrentUser();


        chatTypeBlock=findViewById(R.id.chatTypeBlock);
        chatTypeSection=findViewById(R.id.chatTypeSection);
        chatTypeSection.setUserData(this.userData);
        chat_message=chatTypeSection.getEditTextView();




        mLayoutManager=new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);
        mLayoutManager.setStackFromEnd(true);
        // mLayoutManager.setReverseLayout(true);



        chatView.setLayoutManager(mLayoutManager);
        adap=new ChatDataAdapter(this,finaldata,isChat,chatView);
        chatView.setAdapter(adap);

        // ArrayList<ChatListData> finaldata=generateDummy();


        chatTypeBlock.setVisibility(View.GONE);
        if(userData.type==5)
        {

            checkGroupOnlineStatus();


            getGroupChatsFromDatabase();
            isChat=false;
            updateGroupUserSeenTimestamp(uId);
            if(userPrivilage==null)
            {
                chatTypeSection.setVisibility(View.GONE);
                chatTypeBlock.setVisibility(View.VISIBLE);
                TextView chatTypeBlockText= (TextView)findViewById(R.id.chatTypeBlockText);
                chatTypeBlockText.setText("You are no longer a member of this group");
            }

        }
        else if(userData.type==6)
        {
            chatTypeSection.setVisibility(View.GONE);
            chatTypeBlock.setVisibility(View.VISIBLE);
            TextView chatTypeBlockText= (TextView)findViewById(R.id.chatTypeBlockText);

            if(userPrivilage==null)
            {
                chatTypeBlockText.setText("You are no longer a lister of this channel");
            }
            else
            {
                switch(userPrivilage)
                {
                    case 0:
                        chatTypeBlockText.setText("You are not an admin. Only admins can send messages in a channel");
                        break;
                    case 1:
                        chatTypeBlock.setVisibility(View.GONE);
                        chatTypeSection.setVisibility(View.VISIBLE);
                        break;
                }
            }

        }
        else
        {
            checkSingleOnlineStatus();
            //databaseRef = FirebaseDatabase.getInstance().getReference().child("chat_data").child(uId);
            getUserChatsFromDatabase();
            isChat=true;
           // DatabaseManager.updateSingleUserSeenSl(uId);


        }

       /* finaldata.add(new GroupChatData(0,4,"","","",1,254534L));
        finaldata.add(new GroupChatData(0,2,"","","",1,254534L));
        finaldata.add(new GroupChatData(0,6,"","","",1,254534L));
        finaldata.add(new GroupChatData(0,7,"","","",1,254534L));*/



        ChatView_DateHeader decorator=new ChatView_DateHeader(this,adap);
        chatView.addItemDecoration(decorator);


        //finaldata.add(new ChatData(0,4,"Kaithamattathil","11.24553","22.3335",1,0));




        chatActionModeCallback = new ActionMode.Callback() {

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {

                // MenuInflater inflater = mode.getMenuInflater();
                getMenuInflater().inflate(R.menu.context_single_chat_selection, menu);
                /*chatMenu=menu;
                if(chatActionModeType==1) {
                    //xxxx
                    //menu.findItem(R.id.action_searchbartempo).setVisible(true);
                    MenuItem sItem=menu.findItem(R.id.action_context_search_chats);
                    Log.e("MGG","search id: "+sItem);
                    SearchView searchView =
                            (SearchView) sItem.getActionView();
                    Log.e("MGG","search id: "+searchView);
                    searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                        @Override
                        public boolean onQueryTextSubmit(String query) {
                            // return false;
                            // Toast.makeText(AllUsersList.this, "Submit- "+query, Toast.LENGTH_SHORT).show();
                            return true;
                        }

                        @Override
                        public boolean onQueryTextChange(String newText) {

                            Log.e("MGG","search id: "+newText);
                           // searchOnList(newText);
                            return false;
                        }
                    });

                    //searchView.setIconified(false);
                    searchView.setIconifiedByDefault(false);

                    //menu.findItem(R.id.action_search_chat_moveup).setVisible(false);
                    //menu.findItem(R.id.action_search_chat_movedown).setVisible(false);
                }*/

                return true;
            }


            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.action_chat_delete:

                        adap.deleteSelectedItems();
                        mode.finish();
                        return true;

                    case R.id.action_chat_forward:

                        shareSlectedChats();

                        mode.finish();

                        return true;
                }
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                chatActionModeType=0;
                adap.clearSelections();

                //notifyDataSetChanged();
                //Toast.makeText(ChatWindow.this, "destroyed", Toast.LENGTH_SHORT).show();
                multiSelectChat=false;
            }
        };

        /*this.microphoneRecorderView = findViewById(R.id.recorder_view);
        this.microphoneRecorderView.setListener(new MicrophoneRecorderView.Listener() {
            @Override
            public void onRecordPressed(float x) {
                recordButtonStartX=(int)x;
            }

            @Override
            public void onRecordReleased(float x) {

            }

            @Override
            public void onRecordCanceled(float x) {

            }

            @Override
            public void onRecordMoved(float x, float absoluteX) {
                int   direction = ViewCompat.getLayoutDirection(microphoneRecorderView);
                float position  = absoluteX / audioRecordingStatus.getWidth();
Log.e("MGG","position "+position);
                if (direction == ViewCompat.LAYOUT_DIRECTION_LTR && position <= 0.5 ||
                        direction == ViewCompat.LAYOUT_DIRECTION_RTL && position >= 0.6)
                {
                    microphoneRecorderView.cancelAction();
                }

                //int tempy = (int) event.getY();
                Log.e("MGG","position "+(recordButtonStartX - x) );
                if ((recordButtonStartX - x) > 200) {
                    microphoneRecorderView.cancelAction();
                }
            }

            @Override
            public void onRecordPermissionRequired() {

            }
        });*/

        chatView.addOnItemTouchListener(new RecyclerViewTouchListner(this, chatView, new RecyclerViewTouchListner.RecyclerClick_Listener()
        {
            @Override
            public void onClick(View view, int position) {
                if (multiSelectChat) {
                    // return;
                    myToggleSelection(position);
                }
                else
                {



                }
            }

            @Override
            public void onLongClick(View view, int position) {

                if (multiSelectChat) {
                    // return;
                    myToggleSelection(position);
                }
                else
                {
                    chatActionMode = startActionMode(chatActionModeCallback);

                    myToggleSelection(position);


                }


                Toast.makeText(getApplicationContext(), position+" Long Press", Toast.LENGTH_SHORT).show();
            }
        }));

        final ImageView attachments=chatTypeSection.getAtachmentView();
        attachments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (attachmentTypeSelector == null) {
                    attachmentTypeSelector = new AttachmentTypeSelector(getApplicationContext(), getSupportLoaderManager(), new AttachmentTypeListener());
                }

                attachmentTypeSelector.show(ChatWindow.this, attachments);

                                              /* Intent intent = new Intent();
                                               intent.setType("image/*");
                                               intent.setAction(Intent.ACTION_GET_CONTENT);
                                               //intent.addCategory(Intent.CATEGORY_OPENABLE);
                                               startActivityForResult(Intent.createChooser(intent, "GET FILE"), 5);*/
            }
        });



       /* TextView slide_to_cancel=(TextView)findViewById(R.id.slide_to_cancel);
        slide_to_cancel.setOnTouchListener(new View.OnTouchListener() {
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return true;
    }
});*/


        chatTypeSection.getSendMsgButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                addNewQuickChat(0,null,chat_message.getText().toString());

            }
        });


        registerForContextMenu(chatView);


    }

    private class ConversationScrollListener extends RecyclerView.OnScrollListener {


        private final ChatView_ItemHeader.ConversationDateHeader conversationDateHeader;

        private boolean wasAtBottom           = true;
        private boolean wasAtZoomScrollHeight = false;
        private long    lastPositionId        = -1;

        Context mContext;

        ConversationScrollListener(@NonNull Context context) {
            //this.scrollButtonInAnimation  = AnimationUtils.loadAnimation(context, R.anim.fade_scale_in);
           // this.scrollButtonOutAnimation = AnimationUtils.loadAnimation(context, R.anim.fade_scale_out);
            this.conversationDateHeader   = new ChatView_ItemHeader.ConversationDateHeader(context, scrollDateHeader);
            mContext=context;

           // this.scrollButtonInAnimation.setDuration(100);
            //this.scrollButtonOutAnimation.setDuration(50);
        }
        private int getHeaderPositionId() {
            return ((LinearLayoutManager)chatView.getLayoutManager()).findLastVisibleItemPosition();
        }
        private boolean isAtBottom() {
            if (chatView.getChildCount() == 0) return true;

            //View    bottomView       = chatView.getChildAt(finaldata.size()-1);
            int     firstVisibleItem = ((LinearLayoutManager) chatView.getLayoutManager()).findLastVisibleItemPosition();
            boolean isAtBottom       = (firstVisibleItem == finaldata.size()-1);

            return isAtBottom;
        }

        private boolean isAtZoomScrollHeight() {
            return ((LinearLayoutManager) chatView.getLayoutManager()).findLastCompletelyVisibleItemPosition() < finaldata.size()-10;
        }


        @Override
        public void onScrolled(final RecyclerView rv, final int dx, final int dy) {

            boolean currentlyAtBottom           = isAtBottom();
            boolean currentlyAtZoomScrollHeight = isAtZoomScrollHeight();
            int     positionId                  = getHeaderPositionId();

            if (currentlyAtBottom && !wasAtBottom) {
                Log.e("MGG","animateButtonBurstOUTTT ");
                //ViewUtil.fadeOut(composeDivider, 50, View.INVISIBLE);
                //ViewUtil.animateOut(scrollToBottomButton, scrollButtonOutAnimation, View.INVISIBLE);
                ViewFunctions.independantAnimateOut(scrollToBottomButton,mContext);
            } else if (!currentlyAtBottom && wasAtBottom) {
               // ViewUtil.fadeIn(composeDivider, 500);
            }

            if (currentlyAtZoomScrollHeight && !wasAtZoomScrollHeight) {
                Log.e("MGG","animateButtonBurstINNN ");
                ViewFunctions.animateButtonBurstIn(scrollToBottomButton,500,false);
            }

            if (positionId != lastPositionId) {
                bindScrollHeader(conversationDateHeader, positionId);
            }

            wasAtBottom           = currentlyAtBottom;
            wasAtZoomScrollHeight = currentlyAtZoomScrollHeight;
            lastPositionId        = positionId;

        }
        private void bindScrollHeader(ChatView_ItemHeader.HeaderViewHolder headerViewHolder, int positionId) {
            //if (((ConversationAdapter)list.getAdapter()).getHeaderId(positionId) != -1) {
                adap.onBindDateItemDecorator(headerViewHolder, positionId);
           // }
        }

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                conversationDateHeader.show();
            } else if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                conversationDateHeader.hide();
            }
        }



    }
    ChatView_UnseenHeader chatLastSeenDecorator;

    public void setLastSeenTimestamp(long time)
    {
        if(chatLastSeenDecorator!=null)
            chatView.removeItemDecoration(chatLastSeenDecorator);

        chatLastSeenDecorator=new ChatView_UnseenHeader(this,adap,time);
        chatView.addItemDecoration(chatLastSeenDecorator);
    }
    int getPositionOfChatInFinalData(long sl)
    {
        for (int i=finaldata.size()-1;i>=0;i--)
        {
            if(finaldata.get(i).sl==sl)
                return i;
        }
        return 0;
    }

    EmojiEditText chat_message;
    /*@Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if(interceptTouch )
        {
            //interceptTouch=false;
            return false;
        }
        return super.dispatchTouchEvent(ev);
    }*/



    private AttachmentTypeSelector attachmentTypeSelector;
    boolean actionInProgress;

    boolean interceptTouch;
    int recordButtonStartX;

    View.OnTouchListener microPhoneListner;


    private class AttachmentTypeListener implements AttachmentTypeSelector.AttachmentClickedListener {
        @Override
        public void onClick(int type) {
            addAttachment(type);
        }

        @Override
        public void onQuickAttachment(ArrayList<Uri> uri) {


        }
    }
    /*AttachmentTypeSelector.MediaArrayReturn mediaReturnListner=new AttachmentTypeSelector.MediaArrayReturn() {
        @Override
        public void onMediaReturn(ArrayList<MediaData> uri) {
           // for (MediaData mediaTemp : uri) {

                Log.e("MGG","listner received "+uri.size());


           // }
        }
    };*/
    public static final int PICK_GALLERY      = 21;
    public static final int PICK_DOCUMENT     = 22;
    public static final int PICK_AUDIO        = 23;
    public static final int PICK_CONTACT_INFO = 24;
    public static final int GROUP_EDIT        = 25;
    public static final int TAKE_PHOTO        = 26;
    public static final int ADD_CONTACT       = 27;
    public static final int PICK_LOCATION     = 28;
    public static final int PICK_GIF          = 29;
    public static final int SMS_DEFAULT       = 10;

    private void addAttachment(int type) {
        Log.w("ComposeMessageActivity", "Selected: " + type);
        switch (type) {
            case AttachmentTypeSelector.ADD_GALLERY:
                /*Intent intent = new Intent();
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_PICK);
                startActivityForResult(Intent.createChooser(intent,"Select Picture"), 1);*/
                Intent intent=new Intent(this,GalleryActivity.class);
                startActivityForResult(intent,this.PICK_GALLERY);
                break;
            case AttachmentTypeSelector.ADD_DOCUMENT:
                Intent intent2 = new Intent();
                intent2.setType("*/*");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    // intent2.setAction(Intent.ACTION_OPEN_DOCUMENT);
                }
                //else {

                intent2.setAction(Intent.ACTION_GET_CONTENT);
                //}
                startActivityForResult(Intent.createChooser(intent2, "Select a document"),
                        this.PICK_DOCUMENT);
                break;
            case AttachmentTypeSelector.ADD_SOUND:

                Intent intent3 = new Intent(Intent.ACTION_PICK);
                intent3.setType("audio/*");
                startActivityForResult(Intent.createChooser(intent3, "Select an audio"),
                        this.PICK_AUDIO);

                break;
            case AttachmentTypeSelector.ADD_CONTACT_INFO:
                Intent intent4 = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);

                startActivityForResult(intent4, this.PICK_CONTACT_INFO);

                break;
            case AttachmentTypeSelector.ADD_LOCATION:
                break;
            case AttachmentTypeSelector.TAKE_PHOTO:

                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                Intent takeVideoIntent = new Intent(MediaStore.INTENT_ACTION_STILL_IMAGE_CAMERA);
                // takePictureIntent.putExtra(Intent.EXTRA_INTENT,takeVideoIntent);
                // Intent chooserIntent = new Intent(Intent.ACTION_CHOOSER);
                //Intent contentSelectionIntent = new Intent(Intent.ACTION_GET_CONTENT);
                //contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE);
                //contentSelectionIntent.setType("*/*");
                //Intent[] intentArray = new Intent[]{takePictureIntent,takeVideoIntent};
                /*chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent);
                chooserIntent.putExtra(Intent.EXTRA_TITLE, "Choose an action");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);*/
                startActivityForResult(takePictureIntent, this.TAKE_PHOTO);

                break;
            case AttachmentTypeSelector.ADD_GIF:
                Intent intent7=new Intent(this,GalleryActivity.class);
                intent7.putExtra("mediaType",5);
                startActivityForResult(intent7,this.PICK_GIF);
                break;
        }
    }




    /*DOCUMETS SELECT*/
    /*Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
    File image = AndroidUtilities.generatePicturePath();
                if (image != null) {
        if (Build.VERSION.SDK_INT >= 24) {
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(getParentActivity(), BuildConfig.APPLICATION_ID + ".provider", image));
            takePictureIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        } else {
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(image));
        }
        currentPicturePath = image.getAbsolutePath();
    }
    startActivityForResult(takePictureIntent, 0);
     */

    /*DOCUMETS SELECT*/





    public void viewSelectedUserProfile(String userID)
    {
        Intent indi=new Intent(getApplicationContext(),Profile_Activity.class);
        indi.putExtra("userid",userID);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // Apply activity transition

            Pair contact_image = Pair.create(chatUserImage, "contact_image");
            Pair contact_name = Pair.create(chatUserName, "contact_name");
            Pair contact_online_status = Pair.create(online_status, "contact_online_status");

            ActivityOptions compat=ActivityOptions.makeSceneTransitionAnimation(
                    ChatWindow.this,contact_image,contact_name);
            startActivity(indi,compat.toBundle());
        }
        else
        {
            startActivity(indi);
        }

    }


    public static UserWholeData returnSingleUserDetails(String user)
    {


        //SQLiteDatabase db = DatabaseManager.getDatabase().getReadableDatabase();
        SQLiteDatabase db = DatabaseManager.getDatabase().getReadableDatabase();

        Cursor cursor = db.query(DatabaseAccesser.user_table,
               /* new String[]{
                        DatabaseAccesser.user_user_id,
                        DatabaseAccesser.user_name,
                        DatabaseAccesser.user_user_type,
                        DatabaseAccesser.user_status,
                        DatabaseAccesser.user_localUrl,
                        DatabaseAccesser.user_globalUrl,
                        DatabaseAccesser.user_music_localUrl,
                        DatabaseAccesser.user_music_globalUrl,
                        DatabaseAccesser.user_number,
                        DatabaseAccesser.user_user_follow_type,
                        DatabaseAccesser.user_user_chat_type,
                        DatabaseAccesser.user_remote_contact,
                        DatabaseAccesser.user_chat_expiretime,
                        DatabaseAccesser.user_notification_sound,
                        DatabaseAccesser.user_notification_light,
                        DatabaseAccesser.user_notification_vibration,
                        DatabaseAccesser.user_mutetime,
                        DatabaseAccesser.user_priv_profile_picture_status,
                        DatabaseAccesser.user_priv_activity_status,
                        DatabaseAccesser.user_priv_location_status,
                        DatabaseAccesser.user_priv_number_status,
                        DatabaseAccesser.user_priv_status_status,
                        DatabaseAccesser.user_follow_privacy,
                        DatabaseAccesser.user_chat_privacy

                }*/null,
                DatabaseAccesser.user_user_id + " =?",
                new String[]{user},
                null, null, null,null);


        //Toast.makeText(this, cursor.getCount(), Toast.LENGTH_SHORT).show();

        UserWholeData data=new UserWholeData();
        if (cursor.moveToNext()) {

            data.type=cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_user_type));

            data.user_name=cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_name));

            data.status=cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_status));
            data.img_url=cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_localUrl));
            data.followType=cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_user_follow_type));
            data.remoteContact=cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_remote_contact));
            data.priv_profile_picture=cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_priv_profile_picture_status));
            data.priv_activity_status=cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_priv_activity_status));
            data.priv_location=cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_priv_location_status));
            data.priv_number=cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_priv_number_status));
            data.priv_profile_status=cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_priv_status_status));
            data.chatType=cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_user_chat_type));
            data.number=cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_number));
            data.user_id=cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_user_id));
            data.priv_follow_privacy=cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_follow_privacy));
            data.priv_chat_privacy=cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_chat_privacy));


            data.music_url=cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_music_localUrl));

            if(data.music_url.equals("null"))
            {
                data.music_url=cursor.getString(
                        cursor.getColumnIndexOrThrow(DatabaseAccesser.user_music_globalUrl));
            }

            data.chat_expiretime =cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseAccesser.user_chat_expiretime));
            data.not_uri=cursor.getString(cursor.getColumnIndexOrThrow(DatabaseAccesser.user_notification_sound));
            data.not_light=cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseAccesser.user_notification_light));
            data.not_vibration=cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseAccesser.user_notification_vibration));
            data.muteTime=cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseAccesser.user_mutetime));
            if(data.not_uri==null)
                data.not_uri="";

            data.chat_scheduletime=cursor.getLong(cursor.getColumnIndexOrThrow(DatabaseAccesser.user_chat_scheduletime));

        }

        cursor.close();
        return data;
    }

    int fetchUserprivilageInGroup()
    {
        SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();

        Cursor cursor = db.query(DatabaseAccesser.groupsmembers_table,
                new String[]{
                        DatabaseAccesser.groupsmembers_member_type},
                DatabaseAccesser.groupsmembers_group_id + " =? AND "+
                        DatabaseAccesser.groupsmembers_user_id + " =?",
                new String[]{uId,userdetail.getUid()},
                null, null, null,null);
        if(cursor.moveToNext())
        {
            int membertype=cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseAccesser.groupsmembers_member_type));

            return membertype;
        }
        return 0;
    }



    void updateGroupUserSeenTimestamp(String targetGroupID)
    {
        SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();

        Cursor cursor = db.query(DatabaseAccesser.groupsmembers_table,
                new String[]{
                        DatabaseAccesser.groupsmembers_last_rece_time},
                DatabaseAccesser.groupsmembers_group_id + " =? AND "+DatabaseAccesser.groupsmembers_user_id+ " =?",
                new String[]{targetGroupID,userdetail.getUid()},
                null, null, null,null);
        if(cursor.moveToNext())
        {
            long last_rece_time=cursor.getLong(cursor.getColumnIndexOrThrow(DatabaseAccesser.groupsmembers_last_rece_time));

            DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference().child("chat_group").child(targetGroupID).child("chat_status");
            databaseRef.child(userdetail.getUid()).child("last_seen_time").setValue(last_rece_time);


        }
    }


    public static void sendPokeInGroup(String targetUser,String targetGroup)
    {
        GroupChatData x=new GroupChatData();

        x.userid=userdetail.getUid();
        x.sender=0;
        x.type=20;
        x.timestamp= System.currentTimeMillis();
        x.msg=targetUser;
        x.msgseen=0;
        x.temp_url="null";
        x.final_url="null";

        //x.userid=userdetail.getUid();
        Log.e("MSGG","poke initiated");
        DatabaseReference chatRef = FirebaseDatabase.getInstance().getReference().child("chat_group").child(targetGroup).child("chats").push();


        chatRef.setValue((GroupChatData)x).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid)
            {
                Log.e("MSGG","poke success");
            }
        });



    }

    void removeSingleUserToGroup(String userId,String targetGroup)
    {
        GroupChatData x=new GroupChatData();

        x.userid=userdetail.getUid();
        x.sender=0;
        x.type=22;
        x.timestamp= System.currentTimeMillis();
        x.msg=userId;
        x.msgseen=0;
        x.temp_url="null";
        x.final_url="null";


        // x.sl = saveGroupChatToDatabase(x, uId);


        /*finaldata.add(x);
        adap.notifyItemInserted(finaldata.size()-1);
        chatView.scrollToPosition(finaldata.size() - 1);*/

        DatabaseReference chatRef = FirebaseDatabase.getInstance().getReference().child("chat_group").child(targetGroup).child("chats").push();


        chatRef.setValue((ChatData)x).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid)
            {

               /* if(!isChat)
                    getGroupChatsFromDatabase();
                else
                    getUserChatsFromDatabase();   //heavy code Do Somethings
                adap.notifyDataSetChanged();
                refreshChatList();*/
            }
        });
    }

    void addSingleUserToGroup(String userId,String targetGroup)
    {
        GroupChatData x=new GroupChatData();

        x.userid=userdetail.getUid();
        x.sender=0;
        x.type=21;
        x.timestamp= System.currentTimeMillis();
        x.msg=userId;
        x.msgseen=0;
        x.temp_url="null";
        x.final_url="null";

        //x.sl = saveGroupChatToDatabase(x, uId);

       /* finaldata.add(x);
        adap.notifyItemInserted(finaldata.size()-1);
        chatView.scrollToPosition(finaldata.size() - 1);*/

        DatabaseReference chatRef = FirebaseDatabase.getInstance().getReference().child("chat_group").child(targetGroup).child("chats").push();


        chatRef.setValue((ChatData)x).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid)
            {

               /* if(!isChat)
                    getGroupChatsFromDatabase();
                else
                    getUserChatsFromDatabase();   //heavy code Do Somethings
                adap.notifyDataSetChanged();
                refreshChatList();*/

            }
        });
    }


    public void getcontentfromdatabase()
    {
        if(userType!=5)
        {
            getUserChatsFromDatabase();
        }
        else
        {
            getGroupChatsFromDatabase();
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {


        int pos= ChatDataAdapter.optionPosition;
        String selectedUser = adap.data.get(pos).userid;
        switch (item.getItemId()) {
            case R.id.chat_poke:

                Log.e("MSGG","POke occured");
                sendPokeInGroup(selectedUser,uId);
                break;
        }
        return super.onContextItemSelected(item);

    }


    ArrayList<String> userlist=new ArrayList<>();
    SparseBooleanArray userOnline=new SparseBooleanArray();
    void checkGroupOnlineStatus()
    {

        SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();

        Cursor cursor = db.query(DatabaseAccesser.groupsmembers_table,
                new String[]{

                        DatabaseAccesser.groupsmembers_group_id,
                        DatabaseAccesser.groupsmembers_user_id,
                        DatabaseAccesser.groupsmembers_member_type},
                DatabaseAccesser.groupsmembers_group_id + " =?",
                new String[]{uId},
                null, null, null,null);

       /* Cursor cursor = db.query(DatabaseAccesser.groups_participant_table, new String[]{DatabaseAccesser.group_user_id,DatabaseAccesser.group_group_id},
                DatabaseAccesser.group_group_id,
                new String[]{uId},
                null, null, null);*/


        //Log.e("MSG","Cusror count : "+cursor.getCount());
        Log.e("MSG","name :"+cursor.getCount());

        while (cursor.moveToNext()) {

            final String name = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.groupsmembers_user_id));
            userlist.add(name);

            if(name.equals(userdetail.getUid()))
            {
                userPrivilage=cursor.getInt(
                        cursor.getColumnIndexOrThrow(DatabaseAccesser.groupsmembers_member_type));
            }


        }
        cursor.close();
        userOnline.clear();
        checkGroupOnlineStatusWithListners(userlist, userOnline, new JobCompletionListner() {
            @Override
            public void onJobCompletion(String txt1) {
                showGroupOnlineStatus();
            }
        });

    /*for()
    {
    FirebaseDatabase.getInstance().getReference().child("user_profile").child(uId).
    }*/


    }

    public static void checkGroupOnlineStatusWithListners(final ArrayList<String> userlist, final SparseBooleanArray userOnline, final JobCompletionListner listner)
    {
        for (final String name:userlist)
        {

            final long currentTimestamp=System.currentTimeMillis();
            FirebaseDatabase.getInstance().getReference().child("user_profile").child(name).
                    child("online_status").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    String status=dataSnapshot.child("status").getValue().toString();
                    long timeData=Long.parseLong(dataSnapshot.child("timestamp").getValue().toString());

                    if(status.equals("online") && timeData+60000<currentTimestamp)
                    {
                        userOnline.put(userlist.indexOf(name),true);
                    }
                    else
                    {
                        userOnline.put(userlist.indexOf(name),false);
                    }

                    if(listner!=null)
                    {
                        listner.onJobCompletion("");
                    }


                    // online_status.setVisibility(View.VISIBLE);
                    // online_status.setText(status);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }
    void showGroupOnlineStatus()
    {
        int count=0;
        for(int i=0;i<userOnline.size();i++)
        {
            if(userOnline.get(userOnline.keyAt(i))==true)
            {
                count++;
            }

        }
        online_status.setVisibility(View.VISIBLE);
        online_status.setText(count+" online");
    }
    void checkSingleOnlineStatus() {

        if (checkIfPrivacyShouldBeActivie(userData.priv_activity_status,userData.remoteContact)) {
            checkSingleOnlineStatusWithListners(uId, new JobCompletionListner() {
                @Override
                public void onJobCompletion(String txt1) {
                    online_status.setVisibility(View.VISIBLE);
                    online_status.setText(txt1);
                }
            });
        }
    }

    public static void checkSingleOnlineStatusWithListners(String user,final JobCompletionListner listner)
    {
        FirebaseDatabase.getInstance().getReference().child("user_profile").child(user).
                child("online_status").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                Long time=Long.parseLong(dataSnapshot.child("timestamp").getValue().toString());
                String status=dataSnapshot.child("status").getValue().toString();

                listner.onJobCompletion(convertTimeStampToLastSeen(time,status));

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public static String convertTimeStampToText(long timestampValue,boolean shortText)
    {
        String timeText;
   /* long curTime=System.currentTimeMillis();
    Date dt=new Date(timestampValue);
    SimpleDateFormat format;
    if((curTime-86400000)<timestampValue) {
        format = new SimpleDateFormat("hh:mm a");
       timeText=format.format(dt);
    }
    else if((curTime-172800000)<timestampValue)
    {
        // format = new SimpleDateFormat("EEE MMM dd hh:mm:ss zzz yyyy a");
        timeText="YESTERDAY";
    }
    else
    {
        format = new SimpleDateFormat("EEE MMM dd");
        timeText=format.format(dt);
    }*/
        //format.setTimeZone(TimeZone.getTimeZone("GMT"));

        Calendar targetTime = Calendar.getInstance();
        targetTime.setTimeInMillis(timestampValue);

        Calendar now = Calendar.getInstance();
        now.setTimeInMillis(System.currentTimeMillis());

        final String timeFormatString = "h:mm aa";
        final String sameMonthFormatString = "EEE | d MMM";
        final String monthFormatString = "d MMM";
        final String yearFormatString = "MMM yyyy";
        Log.e("MGG","calender - "+now.get(Calendar.DATE)+" other "+targetTime.get(Calendar.DATE));
        boolean sameYear=now.get(Calendar.YEAR) == targetTime.get(Calendar.YEAR);
        boolean sameMonth=now.get(Calendar.MONTH) == targetTime.get(Calendar.MONTH);
        if(System.currentTimeMillis()-timestampValue<30000){
            return "Just Now";
        }
        else if (now.get(Calendar.DATE) == targetTime.get(Calendar.DATE) && sameYear && sameMonth) {
            return DateFormat.format(timeFormatString, targetTime)+"";
        } else if (now.get(Calendar.DATE) - targetTime.get(Calendar.DATE) == 1 && sameYear && sameMonth ){
            return "Yesterday " + DateFormat.format(timeFormatString, targetTime);
        }else if (sameMonth && sameYear) {
            return DateFormat.format(sameMonthFormatString, targetTime) + "";
        }
        else if (sameYear) {
            return DateFormat.format(monthFormatString, targetTime)+"";
        } else {
            return DateFormat.format(yearFormatString, targetTime).toString();
        }
        //return timeText;
    }


    public static String convertTimeStampToLastSeen(long timestampData,String onlineText)
    {

        if((timestampData+15000)>System.currentTimeMillis() && onlineText.equals("online"))
        {
            return onlineText;
        }
        else
        {
            return "Last seen "+DateUtils.getRelativeTimeSpanString(timestampData);
        }

    }

    void shareSlectedChats()
    {
        ArrayList<ChatData> chatsArray = new ArrayList<ChatData>();



        for(int i=0;i<adap.selectedItems.size();i++)
        {
            ChatData temp=new ChatData();
            temp.type=finaldata.get(adap.selectedItems.keyAt(i)).type;
            temp.msg=finaldata.get(adap.selectedItems.keyAt(i)).msg;
            temp.temp_url = finaldata.get(adap.selectedItems.keyAt(i)).temp_url;
            temp.final_url = finaldata.get(adap.selectedItems.keyAt(i)).final_url;

            chatsArray.add(temp);
        }


        Intent intent = new Intent(this, AllUsersList.class);
        Bundle args = new Bundle();
        args.putSerializable("ARRAYLIST",(Serializable)chatsArray);
        intent.putExtra("BUNDLE",args);

        // intent.putExtra("chatsArray",chatsArray);

        startActivity(intent);
    }




    private void myToggleSelection(int idx) {
        multiSelectChat=true;
        adap.toggleSelection(idx);


        chatActionMode.setTitle(adap.getSelectedItemCount()+" Selected");

        if(adap.getSelectedItemCount()==0)
        {
            multiSelectChat=false;
            chatActionMode.finish();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);




        if (requestCode == 10 && resultCode == RESULT_OK) {

            //Toast.makeText(this, "Working", Toast.LENGTH_SHORT).show();

            Uri directory = data.getData();
            Intent uploadIntent = new Intent(getApplicationContext(), UploadActivity.class);
            uploadIntent.putExtra("type", 5);
            uploadIntent.putExtra("uri", directory.toString());
            startActivityForResult(uploadIntent, 15);
        }
        else if(requestCode==this.PICK_DOCUMENT && resultCode == RESULT_OK)
        {
            //ArrayList<MediaData> medias=(ArrayList<MediaData>)getIntent().getSerializableExtra("mediaarray");
            Log.e("MGG","data recieved : "+data.getData());
            // PICK_DOCUMENT PICK_AUDIO PICK_CONTACT_INFO TAKE_PHOTO

            Long mediaSize  = null;
            String fileName = null;
            String mimeType = null;
            try {
                //Log.e("MGG","document starting check: ");
                mediaSize = MediaManager.getMediaSize(this, data.getData());
                mimeType =getContentResolver().getType(data.getData());
                if(mimeType==null) {
                    String extension = MediaManager.getFileExtention(data.getData().toString());
                    mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension.toLowerCase());
                    //fileName= data.getData().getLastPathSegment();

                    fileName=data.getData().getLastPathSegment();

                }
                if(fileName==null)
                {
                    Log.e("MGG","is URI: " +data.getData().getPath());
                    fileName =getFileName(data.getData());
                }



                Log.e("MGG","document: "+data.getData()+" size: "+mediaSize+" name: "+fileName+" mimetype: "+mimeType);
            }
            catch (Exception e)
            {
                Log.e("MGG","second exception: ");
            }

            addNewQuickChat(6,data.getData().toString(),"");


        }
        else if(requestCode==this.PICK_AUDIO && resultCode == RESULT_OK)
        {
            Log.e("MGG","audio: "+data.getData());

            addNewQuickChat(2,data.getData().toString(),"");
        }
        else if(requestCode==this.PICK_CONTACT_INFO && resultCode == RESULT_OK)
        {
            //Log.e("MGG","contact: "+data.getData());
            /*ContactAccessor contactDataList = ContactAccessor.getInstance();
            ContactData contactData = contactDataList.getContactData(this, contactUri);
            getContactData(context, getNameFromContact(context, uri),  Long.parseLong(uri.getLastPathSegment()*/

            Long ID=Long.parseLong(data.getData().getLastPathSegment());
            Log.e("MGG","ID is : "+ID);
            Cursor cursor = null;
            String name=null;
            String number=null;
            try {
                cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                        new String[] {ID+ ""}, null);


                while(cursor != null && cursor.moveToNext())
                {
                     name  =cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    int type         = cursor.getInt(cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.TYPE));
                    String label     = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.LABEL));
                    number    = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    String typeLabel = ContactsContract.CommonDataKinds.Phone.getTypeLabel(getResources(), type, label).toString();
                    Log.e("MGG","contact: "+name+ " number "+number);
                }
                //Log.e("MGG","contact: "+cursor.getString(0)+" id "+);

            } finally {
                if (cursor != null)
                    cursor.close();
            }

            addNewQuickChat(7,null,StringUtils.generateVcardFromNumber(name,number));

        }
        else if(requestCode==this.TAKE_PHOTO && resultCode == RESULT_OK)
        {
            MediaData tempMedia=new MediaData();
            tempMedia.type=1;
            //tempMedia.uri=data.getData().toString();
            Log.e("MGG","photo: "+data.getData());
        }
        else if(requestCode==this.PICK_LOCATION && resultCode == RESULT_OK)
        {

        }
        else if (requestCode == 15 && resultCode == RESULT_OK) {
            final String uri = data.getStringExtra("uri");
            String media_text = data.getStringExtra("text");
            addNewQuickChat(1,uri,media_text);
        }
    }

    void addNewQuickChat(int type,String uri,String msg)
    {
        ChatSender.AIMessageSender(getApplicationContext(),type, uri, msg, userData, new ChatSentListner() {
            @Override
            public void onChatAdded(GroupChatData data) {
                finaldata.add(data);
                //adap.notifyItemInserted(finaldata.size()-1);
                adap.notifyDataSetChanged();
                chatView.scrollToPosition(finaldata.size() - 1);
                chat_message.setText("");

                //Log.e("MGG","KKreeeeee final url is "+Uri.parse(data.final_url));
                Log.e("MGG","PPreeeeee final url is "+Uri.parse(data.final_url));
            }

            @Override
            public void onChatUploaded(GroupChatData data)
            {
                int pos=getPositionOfChatInFinalData(data.sl);
                if(pos==0)
                    Log.e("MGG","Last EEOOOOORRRRR msg is "+pos);

                Log.e("MGG","Last updated msg is "+pos);
                finaldata.get(pos).msgseen=1;
                finaldata.get(pos).temp_url=data.temp_url;
                finaldata.get(pos).final_url=data.final_url;
                adap.notifyItemChanged(pos);
            }
        });



    }
    public String getFileName(Uri uri) {
        String result = null;
        Log.e("MGG","file check ");
        if (uri.getScheme().equals("content")) {
            Log.e("MGG","cursor check ");
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(MediaStore.Files.FileColumns.DISPLAY_NAME));
                }
            } finally
            {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }



    @Override
    protected void onStop() {

        super.onStop();
        MediaManager.resetMediaPlayer();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        uId=null;
        NotifyTimer.removeAllListner();
    }

    @Override
    public void onBackPressed() {
        DatabaseManager.clearMessageQueue(this,uId);
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.single_user_chat_window, menu);
        //menu.findItem(R.id.menu_expiring_messages).setIconTintList(new ColorStateList(Color.GREEN,Color.GREEN))


//yyyy
        this.chatMenu=menu;

        if(userData.followType==11)
        {
            menu.findItem(R.id.action_chat_block_user).setTitle("UnBlock User");
        }
        else
        {
            menu.findItem(R.id.action_chat_block_user).setTitle("Block User");
        }
        if(userData.type==2)
        {
            menu.findItem(R.id.action_add_contact).setTitle("View in Phonebook");
        }
        else
        {
            menu.findItem(R.id.action_add_contact).setTitle("Add to Phonebook");
        }
        updateMenuScheduleTime(menu);
        updateMenuExpireTime(menu);


        MenuItem sItem=menu.findItem(R.id.action_search_chat);
        SearchView searchView =
                (SearchView) sItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // return false;
                // Toast.makeText(AllUsersList.this, "Submit- "+query, Toast.LENGTH_SHORT).show();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                Log.e("MGG","search id: "+newText);
                searchOnList(newText);
                return false;
            }
        });

        //searchView.setIconified(true);
        //searchView.setIconifiedByDefault(false);


        return true;
    }

    void updateMenuExpireTime(Menu menu)
    {
        TimerWithTextView destroTimer=(TimerWithTextView) menu.findItem(R.id.menu_expiring_messages).getActionView().findViewById(R.id.destroyTimerMenu);
        if(userData.chat_expiretime >0)
        {
            menu.findItem(R.id.menu_expiring_messages).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            destroTimer.setText(StringUtils.getShortExpirationDisplayValue(userData.chat_expiretime));
        }
        else
        {
            menu.findItem(R.id.menu_expiring_messages).setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        }

        menu.findItem(R.id.menu_expiring_messages).getActionView().setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                openExpireAlert();
            }
        });
    }
    void updateMenuScheduleTime(Menu menu)
    {
        //userData.chat_scheduletime=System.currentTimeMillis()+9000;
        Log.e("MGG","schedule got time "+userData.chat_scheduletime);

        TimerWithTextView scheduleTimer=(TimerWithTextView) menu.findItem(R.id.menu_schedule_messages).getActionView().findViewById(R.id.destroyTimerMenu);
        scheduleTimer.setImageResource(R.drawable.ic_timer_calender_semiblack);
        //int toSchedule=(int)(userData.chat_scheduletime-System.currentTimeMillis())/1000;
        if(userData.chat_scheduletime > System.currentTimeMillis())
        {
            Log.e("MGG","force always show");
            menu.findItem(R.id.menu_schedule_messages).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            scheduleTimer.setText(StringUtils.getShortExpirationDisplayValueForTimestamp(userData.chat_scheduletime));
            scheduleTimer.setExpirationTime(null,userData.chat_scheduletime);
            scheduleTimer.setTimerListener(new TimerListner() {
                @Override
                public void onTimerEnd(Long timestamp, String time) {
                    updateSchedule(timestamp);
                }

                @Override
                public void onSpecifiedIntervel(Long timestamp, String time) {

                }
            });
        }
        else
        {
            Log.e("MGG","force NEVER show");
            menu.findItem(R.id.menu_schedule_messages).setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        }

        menu.findItem(R.id.menu_schedule_messages).getActionView().setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                openScheduleAlert();
            }
        });
    }

    void openExpireAlert()
    {

        AroundAlertBoxManager.showChatExpireDialog(ChatWindow.this, userData.chat_expiretime, new AroundAlertBoxManager.OnClickListener() {
            @Override
            public void onClick(int expirationTime) {
                updateExpireTime(expirationTime);
                chatTypeSection.updateExpireTime(expirationTime);

            }
        });

    }
    void openScheduleAlert()
    {

        AroundAlertBoxManager.showChatSchedulerDiolog(ChatWindow.this, new ChatSchedulerListener() {
            @Override
            public void onSchedule(Long timestamp) {
                updateSchedule(timestamp);
                chatTypeSection.updateScheduleTime(timestamp);
            }
        });

    }
    public void updateExpireTime(int expirationTime)
    {

        userData.chat_expiretime =expirationTime;
        updateMenuExpireTime(chatMenu);
    }
    public void updateSchedule(long expirationTime)
    {

        userData.chat_scheduletime =expirationTime;
        updateMenuScheduleTime(chatMenu);
    }

    //SparseBooleanArray selectedChats=new SparseBooleanArray();
    int currentVisibleSearch;
    Menu chatMenu;
    void searchOnList(String searchTerm)
    {
        adap.selectedItems.clear();
        adap.notifyDataSetChanged();

        if(searchTerm.equals(""))
        {

            //return;
        }
        else {
            for (int i = 0; i < finaldata.size(); i++) {

                if (finaldata.get(i).msg.toLowerCase().contains(searchTerm.toLowerCase())) {
                    adap.selectedItems.put(i, true);
                    adap.notifyItemChanged(i);
                }
            }
        }



        if(adap.selectedItems.size()>0)
        {
            currentVisibleSearch=adap.selectedItems.size()-1;
            chatView.scrollToPosition(adap.selectedItems.keyAt(currentVisibleSearch));
            chatMenu.findItem(R.id.action_search_chat_moveup).setVisible(true);
            chatMenu.findItem(R.id.action_search_chat_movedown).setVisible(true);

        }
        else
        {
            chatMenu.findItem(R.id.action_search_chat_moveup).setVisible(false);
            chatMenu.findItem(R.id.action_search_chat_movedown).setVisible(false);
        }
    }

    void moveSearchUporDown(boolean up)
    {
        int tempCurrent=currentVisibleSearch;
        if(up)
            currentVisibleSearch--;
        else
            currentVisibleSearch++;
        if(currentVisibleSearch<adap.selectedItems.size() && currentVisibleSearch>=0)
        {

            //chatView.scrollToPosition(adap.selectedItems.keyAt(currentVisibleSearch));
        }
        else
        {
            currentVisibleSearch=tempCurrent;
        }

        chatView.scrollToPosition(adap.selectedItems.keyAt(currentVisibleSearch));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Log.e("MGG","id is "+item.getItemId());
        switch (item.getItemId()) {

           case R.id.menu_expiring_messages:
               openExpireAlert();
                break;
            case R.id.menu_schedule_messages:
                openScheduleAlert();
                break;
            case R.id.action_search_chat_moveup:
                moveSearchUporDown(true);

                break;
            case R.id.action_search_chat_movedown:
                moveSearchUporDown(false);

                break;
            case R.id.action_add_contact:
                Log.e("MGG","qqqqqqqqq");
                if(userData.type==2)
                {  Log.e("MGG","kolakola");
                    // menu.findItem(R.id.action_add_contact).setTitle("View in Phonebook");
                    viewSingleContactOnPhoneBook(userData.number);
                }
                else
                {
                    insertRawContact();
                }
                

                break;
            case R.id.action_user_profile:
                viewSelectedUserProfile(uId);
                break;
            case   R.id.action_user_media:
                Intent mediaIntent=new Intent(this,GallerySubDirectoryActivity.class);
                mediaIntent.putExtra("userid",uId);
                startActivity(mediaIntent);
                break;
            case R.id.action_chat_clear:

                adap.clearChatHistory(uId);

                return true;
            case R.id.action_chat_block_user:

                if(userData.followType==11)
                {
                    Profile_Activity.newUserUnBlock(uId);
                }
                else
                {
                    Profile_Activity.newUserBlock(uId);
                }

                return true;
        }

        return super.onOptionsItemSelected(item);
    }
    void insertRawContact()
    {
        Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
        intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
        // Intent addPersonIntent = new Intent(Intent.ACTION_INSERT);
        ///addPersonIntent.setType(ContactsContract.Contacts.CONTENT_TYPE);

        intent.putExtra(ContactsContract.Intents.Insert.NAME, "Febin Mathew");
        intent.putExtra(ContactsContract.Intents.Insert.PHONE, "+919633333333");


        //startActivityForResult(intent, CREATE_NEW);
        startActivity(intent);

        /*Intent
                intent
                = new Intent(Intent.ACTION_INSERT_OR_EDIT);
        intent.setType(ContactsContract.Contacts.CONTENT_ITEM_TYPE);
        intent.putExtra(
                ContactsContract.Intents.Insert.NAME,
                name);
        intent.putExtra(
                ContactsContract.Intents.Insert.EMAIL,
                email);*/

    }

    void viewSingleContactOnPhoneBook(String phoneNum) {

        Log.e("MGG", "NUM " + phoneNum);
        final Cursor cursor
                = getContentResolver()
                .query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        new String[]{
                                ContactsContract.PhoneLookup._ID,
                                ContactsContract.PhoneLookup.LOOKUP_KEY},
                        ContactsContract.CommonDataKinds.Phone.NUMBER+
                                "=?",
                        new String[]{phoneNum},
                        null);
        Log.e(
                "MGG",
                "Count " + cursor.getCount());
        if (cursor.moveToFirst()) {
            final String id = cursor.getString(0);
            Log.e("MGG", "ID " + id);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            String lookupKey
                    = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup.LOOKUP_KEY));
            long contactId = cursor.getLong(
                    cursor.getColumnIndexOrThrow(
                            ContactsContract.PhoneLookup._ID));
            Uri uri = ContactsContract.Contacts.getLookupUri(contactId, lookupKey);
            intent.setData(uri);

            startActivity(intent);

        }
        cursor.close();
    }


    static void getGroupChatsFromDatabase()
    {
        finaldata.clear();
        SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();
        String userSelection;
        Cursor cursor;
        userSelection = DatabaseAccesser.chat_userid;
        cursor= db.rawQuery(
                "SELECT * FROM "+DatabaseAccesser.chat_table +" INNER JOIN "+DatabaseAccesser.user_table+" ON "+
                        DatabaseAccesser.chat_table+"."+DatabaseAccesser.chat_userid+" = "+ DatabaseAccesser.user_table+"."+DatabaseAccesser.user_user_id +
                        " INNER JOIN "+DatabaseAccesser.media_table+" ON "+
                        DatabaseAccesser.chat_table+"."+DatabaseAccesser.chat_col_media_id+" = "+ DatabaseAccesser.media_table+".rowid"+
                        " WHERE " +
                        userSelection + " = ?", new String[]{uId});

        while (cursor.moveToNext()) {


            GroupChatData temp = new GroupChatData();


            temp.sl = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_sl_no));
            temp.userid = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_userid));
            temp.sender = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_sender));

            temp.type = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_type));
            temp.timestamp = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_timestamp));
            temp.msg = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_text));

            temp.msgseen = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_ifseen));

            temp.mediaSize = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.media_size));

            temp.temp_url = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.media_local_uri));

            if(temp.temp_url.equals("null"))
            {
                temp.temp_url = cursor.getString(
                        cursor.getColumnIndexOrThrow(DatabaseAccesser.media_global_uri));
            }

            temp.final_url = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.media_lite_local_uri));


            //Toast.makeText(getActivity(), temp.uri, Toast.LENGTH_SHORT).show();
            if(temp.userid.equals(TabbedActivity.userdetail.getUid()))
                temp.name="Me";
            else
                temp.name=cursor.getString(cursor.getColumnIndexOrThrow(DatabaseAccesser.user_name));
            // long itemId =
            finaldata.add(temp);
        }
        cursor.close();
    }

    void getUserChatsFromDatabase()
    {
        //yyy
        finaldata.clear();
        SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();
        String userSelection;
        Cursor cursor;

        userSelection = DatabaseAccesser.chat_userid;
           /* cursor= db.query(DatabaseAccesser.chat_table, new String[]{
                            DatabaseAccesser.chat_sl_no,
                            DatabaseAccesser.chat_userid,
                            DatabaseAccesser.chat_sender,
                            DatabaseAccesser.chat_type,
                            DatabaseAccesser.chat_timestamp,
                            DatabaseAccesser.chat_text,
                            DatabaseAccesser.chat_ifseen,
                            DatabaseAccesser.chat_temp_url,
                            DatabaseAccesser.chat_final_url},
                    userSelection + " =? AND "+DatabaseAccesser.chat_groupid+" =?",
                    new String[] { uId,"null" },
                    null, null, null,null);*/

        cursor= db.rawQuery(
                "SELECT *,"+DatabaseAccesser.chat_table+".rowid AS rowID"+" FROM "+DatabaseAccesser.chat_table +" INNER JOIN "+DatabaseAccesser.media_table+" ON "+
                        DatabaseAccesser.chat_table+"."+DatabaseAccesser.chat_col_media_id+" = "+ DatabaseAccesser.media_table+".rowid"+ " WHERE " +
                        userSelection + " =? AND "+DatabaseAccesser.chat_groupid+" =? ORDER BY "+DatabaseAccesser.chat_timestamp, new String[] { uId,"null" });




        Log.e("MSG","chat count : "+cursor.getCount());
        //Log.e("MSG","user selection: "+userSelection);
        //Log.e("MSG","group id: "+uId);
        // int cnt=cursor.getCount();
//        Toast.makeText(this, cnt, Toast.LENGTH_SHORT).show();
        while (cursor.moveToNext()) {


            GroupChatData temp = new GroupChatData();



            temp.chatRow=cursor.getLong(
                    cursor.getColumnIndexOrThrow("rowID"));
            temp.sl = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_sl_no));

            temp.userid = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_userid));
            temp.sender = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_sender));

            temp.type = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_type));
            temp.timestamp = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_timestamp));
            temp.chat_rece_time=cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_rece_timestamp));
            temp.chat_seen_time=cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_seen_timestamp));
            temp.msg = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_text));

            temp.msgseen = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_ifseen));

            temp.chatExpire = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_expire));
            temp.chatSchedule = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_schedule));


            temp.mediaSize = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.media_size));


            /*temp.temp_url = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_temp_url));
            temp.final_url = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_final_url));*/


            temp.final_url = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.media_local_uri));
            if(temp.final_url.equals("null"))
            {
                temp.final_url = cursor.getString(
                        cursor.getColumnIndexOrThrow(DatabaseAccesser.media_global_uri));
                /*if(!temp.final_url.contains("https"))
                {

                    temp.final_url=temp.final_url+">>"+cursor.getInt(
                            cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_col_media_id))+"";
                }*/
            }

            temp.media_id=cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_col_media_id));

            temp.temp_url = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.media_lite_local_uri));

            if(temp.msgseen==2 && temp.sender==1 )
            {
                if(finaldata.size()>0)
            {
                if((finaldata.get(finaldata.size()-1).sender==0||(finaldata.get(finaldata.size()-1).sender==1 && finaldata.get(finaldata.size()-1).msgseen==3)))
                {
                    Log.e("MSG","this.lastSeenTimestamp1 "+temp.timestamp);
                    setLastSeenTimestamp(temp.timestamp-1);
                }
            }

            }

            //Toast.makeText(getActivity(), temp.uri, Toast.LENGTH_SHORT).show();
            // long itemId =
            finaldata.add(temp);
        }
        Log.e("MSG","lastFinalData "+(finaldata.get(finaldata.size()-2).sender==1 && finaldata.get(finaldata.size()-2).msgseen==3)
        +" / "+finaldata.get(finaldata.size()-2).msgseen+" / "+finaldata.get(finaldata.size()-2).sender+" : "+finaldata.get(finaldata.size()-2).msg);

        cursor.close();
    }



}
