package com.hash.around.Views.Medias;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnPausedListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.R;
import com.hash.around.Views.ViewFunctions;
import com.pitt.library.fresh.FreshDownloadView;

import java.io.File;
import java.io.IOException;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;


public class AroundDownloadGifView extends RelativeLayout {

    private boolean centerCrop;
    private int imageMaxHeight;


    boolean layoutChanged=false;
    View rootView;
    Context mContext;
    ImageView imageContent;
    GifImageView defaultGifView;
    FreshDownloadView downloadView;
    ImageButton playButton;
    TextView sizeText;
    JobCompletionListner jobComplete=null;


    public AroundDownloadGifView(Context context) {
        super(context);
        init(context,null);
    }

    public AroundDownloadGifView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context,attrs);
    }

    public AroundDownloadGifView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context,attrs);
        //inflate(context, R.layout.around_gifview, this);
    }

    public void setOnJobCompletionListner(JobCompletionListner job)
    {
        this.jobComplete=job;
    }

    public void setDownloadClickListner(String uri)
    {
        downloadView.setVisibility(VISIBLE);
        imageContent.setVisibility(VISIBLE);
        playButton.setVisibility(GONE);
        defaultGifView.setVisibility(GONE);
       // videotexture.setVisibility(GONE);

        final String url=uri;
        downloadView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadFile(url);
            }
        });
    }
    public void disableDownloadClick()
    {
        Log.e("MSG","disable");
        downloadView.setVisibility(GONE);
        imageContent.setVisibility(GONE);
        playButton.setVisibility(VISIBLE);
        sizeText.setVisibility(GONE);
        defaultGifView.setVisibility(VISIBLE);

    }
    public void enableDownloadClick()
    {
        Log.e("MSG","disable");
        downloadView.setVisibility(VISIBLE);
        imageContent.setVisibility(VISIBLE);
        playButton.setVisibility(GONE);
        sizeText.setVisibility(VISIBLE);
        defaultGifView.setVisibility(GONE);

    }

   /* @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        if(layoutChanged)
        {
            //heightMeasureSpec=200;
            Log.e("MSG",heightMeasureSpec+"///"+vid.getHeight()+"///"+MeasureSpec.makeMeasureSpec(vid.getHeight(),MeasureSpec.AT_MOST)+"///"+MeasureSpec.makeMeasureSpec(vid.getHeight(),MeasureSpec.EXACTLY)+"///"+MeasureSpec.makeMeasureSpec(vid.getHeight(),MeasureSpec.UNSPECIFIED));
            //heightMeasureSpec=MeasureSpec.makeMeasureSpec(vid.getHeight(),MeasureSpec.UNSPECIFIED);
        }


        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

    }*/

    public void setUpGifUri(Uri url)
    {
        defaultGifView.setImageURI(url);
        ((GifDrawable) defaultGifView.getDrawable()).stop();
        defaultGifView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
               if(((GifDrawable) defaultGifView.getDrawable()).isPlaying()) {
                   ((GifDrawable) defaultGifView.getDrawable()).stop();
                   ViewFunctions.independantAnimateIn(playButton, mContext);
               }
            }
        });
        playButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((GifDrawable) defaultGifView.getDrawable()).start();
                ViewFunctions.independantAnimateOut(playButton, mContext);
            }
        });
    }

    public void setLocallyAvailable(boolean locallyAvailable)
    {
        if(locallyAvailable)
        {
            disableDownloadClick();
        }
        else
        {
            enableDownloadClick();
        }
    }


    private void init(Context context, @Nullable AttributeSet attrs) {

        mContext=context;
        rootView = inflate(context, R.layout.around_gifview, this);
        downloadView =(FreshDownloadView) rootView.findViewById(R.id.downloadView);
        defaultGifView=rootView.findViewById(R.id.gifView);
        imageContent = (ImageView) rootView.findViewById(R.id.imageContent);
        playButton=(ImageButton) rootView.findViewById(R.id.playButton);
        sizeText=rootView.findViewById(R.id.sizeText);
       /*
        //videotexture= (TextureView) rootView.findViewById(R.id.videoContent);


        vid=(VideoView) rootView.findViewById(R.id.videoOnly);
        playButton=(ImageButton) rootView.findViewById(R.id.playButton);*/
        /*if(attrs!=null)
        {
            TypedArray ta = context.getTheme().obtainStyledAttributes(attrs, R.styleable.AroundDownloadImageView, 0, 0);
            centerCrop = ta.getBoolean(R.styleable.AroundDownloadImageView_imageCenterCrop, false);
            if(centerCrop)
            imageContent.setScaleType(ImageView.ScaleType.CENTER_CROP);

            imageMaxHeight = ta.getDimensionPixelSize(R.styleable.AroundDownloadImageView_imageMaxHeight, imageContent.getHeight());
            imageContent.setMaxHeight(imageMaxHeight);

        }*/

    }

    GifImageView getGifView()
    {
        return defaultGifView;
    }
    public ImageView getImageView()
    {
        return imageContent;
    }
    FreshDownloadView getDownloadView()
    {
        return downloadView;
    }

    void downloadFile(String urlToDownload)
    {
        downloadView.startDownload();

        File localFile = null;
        final String localFilename;
        try {
            localFile = File.createTempFile("videos", "mp4");

        } catch (IOException e) {
            e.printStackTrace();
        }
        localFilename=localFile.toURI().toString();
        StorageReference httpsReference = FirebaseStorage.getInstance().getReferenceFromUrl(urlToDownload);
        httpsReference.getFile(localFile).addOnProgressListener(new OnProgressListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onProgress(FileDownloadTask.TaskSnapshot taskSnapshot) {
                float percent=((float)taskSnapshot.getBytesTransferred()/(float)taskSnapshot.getTotalByteCount())*100;
               // Log.e("MSG",((float)taskSnapshot.getBytesTransferred()/(float)taskSnapshot.getTotalByteCount())*100+"//\\"+
                        //((float)taskSnapshot.getBytesTransferred()/(float)taskSnapshot.getTotalByteCount())+"//\\"+percent);
                downloadView.upDateProgress((int)percent);
            }
        }).addOnFailureListener(new OnFailureListener() {

            @Override
            public void onFailure(@NonNull Exception e) {
                downloadView.showDownloadError();
            }
        }).addOnPausedListener(new OnPausedListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onPaused(FileDownloadTask.TaskSnapshot taskSnapshot) {
                downloadView.reset();
                Log.e("MSG","paused");

            }
        }).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
               // downloadView.showDownloadOk();
                Log.e("MSG","succeded");
                //disableDownloadClick();
                if(jobComplete!=null)
                {
                    jobComplete.onJobCompletion(localFilename);
                }
                //downloadView.clearAnimation();

            }
        })

        ;
    }
}
