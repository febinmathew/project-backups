package com.hash.around;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Toast;

import com.isseiaoki.simplecropview.CropImageView;
//import com.theartofdev.edmodo.cropper.CropImageView;
import com.isseiaoki.simplecropview.callback.CropCallback;
import com.isseiaoki.simplecropview.callback.LoadCallback;
import com.isseiaoki.simplecropview.callback.SaveCallback;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ImageCropActivity extends AppCompatActivity {
//CropImageView cropImageView;
CropImageView cropImageView;
    String uri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.crop_image_activity);
       cropImageView=(CropImageView)findViewById(R.id.cropImageView);
       uri= getIntent().getStringExtra("uri");
        cropImageView.load(Uri.parse(uri)).execute(new LoadCallback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Throwable e) {

            }
        });
       // Toast.makeText(this, uri, Toast.LENGTH_SHORT).show();
        cropImageView.setCropMode(CropImageView.CropMode.FREE);

        cropImageView.setHandleShowMode(CropImageView.ShowMode.SHOW_ALWAYS);
        cropImageView.setGuideShowMode(CropImageView.ShowMode.SHOW_ON_TOUCH);

cropImageView.setAnimationEnabled(true);
        cropImageView.setInterpolator(new AccelerateDecelerateInterpolator());


    }
    public void rotateleft(View v)
    {
        cropImageView.rotateImage(CropImageView.RotateDegrees.ROTATE_M90D); // rotate counter-clockwise by 90 degrees
        //cropImageView.rotateImage(-90);
    }
    public void rotateright(View v)
    {
        //cropImageView.rotateImage(90);
        cropImageView.rotateImage(CropImageView.RotateDegrees.ROTATE_90D); // rotate counter-clockwise by 90 degrees
    }
    public void cancelCrop(View v)
    {
        //cropImageView.rotateImage(CropImageView.RotateDegrees.ROTATE_M90D); // rotate counter-clockwise by 90 degrees
        //cropImageView.rotateImage(-90);
        super.onBackPressed();
    }
    public void cropButton(View v)
    {
        File cacheDir = new File(getApplicationContext().getExternalCacheDir(), "editfile2.png");
        //cacheDir.mkdir();

        //Toast.makeText(this, cacheDir.toString(), Toast.LENGTH_SHORT).show();
        cropImageView.crop(Uri.parse(uri))
                .execute(new CropCallback() {
                    @Override public void onSuccess(Bitmap cropped)
                    {

                        try {
//getExternalFilesDir("temp")
                           File myExternalFile = new File(getApplicationContext().getExternalCacheDir(), "editfile2.png");

                            FileOutputStream fos = new FileOutputStream(myExternalFile);
                            cropped.compress(Bitmap.CompressFormat.PNG, 90, fos);
                            fos.close();
                            Intent intent = new Intent();
                            intent.putExtra("uri", myExternalFile.toURI().toString());
                            setResult(RESULT_OK, intent);
                            finish();
                        } catch (FileNotFoundException e) {
                            Toast.makeText(ImageCropActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                        } catch (IOException e) {
                            Toast.makeText(ImageCropActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                        }



                       /* cropImageView.save(cropped).execute(outputUri, new SaveCallback() {
                                    @Override
                                    public void onSuccess(Uri uri) {

                                        Intent intent = new Intent();
                                        intent.putExtra("uri", uri.toString());
                                        setResult(RESULT_OK, intent);
                                        finish();



                                        /*public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == 1) {
         if(resultCode == RESULT_OK) {
             String strEditText = data.getStringExtra("editTextValue");
         }
    }
} */
                                   /* }

                                    @Override
                                    public void onError(Throwable e) {
                                        Toast.makeText(ImageCropActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                                    }
                                });*/
                    }

                    @Override public void onError(Throwable e) {
                    }
                });

    }
}
