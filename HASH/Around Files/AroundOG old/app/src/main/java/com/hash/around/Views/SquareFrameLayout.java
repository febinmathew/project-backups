package com.hash.around.Views;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build.VERSION_CODES;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

//import org.thoughtcrime.securesms.R;
import com.hash.around.OtherClasses.Interfaces.JobCompletionWithFailureListner;
import com.hash.around.R;

public class SquareFrameLayout extends FrameLayout {

  private final boolean squareHeight;
    private final boolean squareAsWidth;

  @SuppressWarnings("unused")
  public SquareFrameLayout(Context context) {
    this(context, null);
  }

  @SuppressWarnings("unused")
  public SquareFrameLayout(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  @TargetApi(VERSION_CODES.HONEYCOMB) @SuppressWarnings("unused")
  public SquareFrameLayout(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);

    if (attrs != null) {
      TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.SquareFrameLayout, 0, 0);
      this.squareHeight = typedArray.getBoolean(R.styleable.SquareFrameLayout_square_height, false);
      this.squareAsWidth=typedArray.getBoolean(R.styleable.SquareFrameLayout_square_width, false);
      typedArray.recycle();
    }
    else {
      this.squareHeight = false;
      this.squareAsWidth=false;
    }
    //
  }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        initViews();
    }

    public void doubleSelctionSize(int doubleSize)
    {
        FrameLayout checkBoxArea= findViewById(R.id.checkBoxArea);
        //checkBoxArea.measure(checkBoxArea.getMeasuredWidth()*doubleSize,checkBoxArea.getMeasuredHeight()*doubleSize);
        checkBoxArea.setLayoutParams(new LayoutParams(
                (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, getResources().getDisplayMetrics()), (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, getResources().getDisplayMetrics())));
        //checkBoxArea.measure(checkBoxArea.getMeasuredWidth()*doubleSize,checkBoxArea.getMeasuredHeight()*doubleSize);

    }
  ImageView itemSelection;
  FrameLayout checkBoxArea;
  JobCompletionWithFailureListner listner;
  boolean selection;

 public  void setJobListner(JobCompletionWithFailureListner listnerTemp)
  {
    this.listner=listnerTemp;
  }

  public void setChecked(boolean checked)
  {
    this.itemSelection.setVisibility(checked?VISIBLE:GONE);
    selection=checked;
  }

  public void disableSelection()
  {
      if(this.checkBoxArea!=null)
          this.checkBoxArea.setVisibility(GONE);
  }

  public void initViews()
  {
    //SquareFrameLayout layout=(SquareFrameLayout) LayoutInflater.from(context).inflate(R.layout.recent_photo_view_item, this, true);
    this.itemSelection = findViewById(R.id.itemSelection);
    //this.itemSelection.setVisibility(INVISIBLE);
    this.checkBoxArea = findViewById(R.id.checkBoxArea);

      Log.e("MGG","value " +this.itemSelection);
    this.checkBoxArea.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        if(listner!=null)
        {
            //selection=!selection;
          if(selection) {
              //selection=false;
            //itemSelection.setVisibility(INVISIBLE);
            listner.onJobFailed("");
          }
          else {
              //selection=true;
            //itemSelection.setVisibility(VISIBLE);
            listner.onJobCompletion("");
          }

        }
        invalidate();
      }
    });

  }
  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    //noinspection SuspiciousNameCombination
    if (squareHeight) super.onMeasure(heightMeasureSpec, heightMeasureSpec);
    else              super.onMeasure(widthMeasureSpec, widthMeasureSpec);


  }
}
