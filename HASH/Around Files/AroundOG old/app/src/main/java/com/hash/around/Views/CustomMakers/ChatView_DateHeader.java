package com.hash.around.Views.CustomMakers;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hash.around.OtherClasses.ChatDataAdapter;
import com.hash.around.R;

/**
 * Created by Febin on 6/22/2018.
 */
public class ChatView_DateHeader extends ChatView_ItemHeader{

    @Override
    public boolean hasHeader(int adapPos,RecyclerView par)
    {
        return adapter.hasTimeHeader(adapPos,isReverseLayout(par));
    }

    public ChatView_DateHeader(Context context, ChatDataAdapter adap) {
        super(context, adap);
    }


    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        int position     = parent.getChildAdapterPosition(view);
        int headerHeight = 0;

        if (position != RecyclerView.NO_POSITION && hasHeader(position,parent)) {
            View header = getHeader(parent, mContext, position,true).itemView;
            headerHeight = header.getHeight();
        }
        outRect.set(0, headerHeight, 0, 0);
    }



    @Override
    protected int getHeaderTop(RecyclerView parent, View child, View header, int adapterPos,
                               int layoutPos)
    {
        int headerHeight = header.getHeight();
        int top = getChildY(parent, child) - headerHeight;
        /*if (true && layoutPos == 0) {
            final int count = parent.getChildCount();
            final long currentId = 1;
            // find next view with header and compute the offscreen push if needed
            for (int i = 1; i < count; i++) {
                int adapterPosHere = parent.getChildAdapterPosition(parent.getChildAt(translatedChildPosition(parent, i)));
                if (adapterPosHere != RecyclerView.NO_POSITION) {
                    long nextId = 1;
                    if (nextId != currentId) {
                        final View next = parent.getChildAt(translatedChildPosition(parent, i));
                        final int offset = getChildY(parent, next) - (headerHeight + getHeader(parent, mContext, adapterPosHere,false).itemView.getHeight());
                        if (offset < 0) {
                            return offset;
                        } else {
                            break;
                        }
                    }
                }
            }

            if (true) top = 0;
        }*/

        return top;
    }



    @Override
    RecyclerView.ViewHolder getHeader(RecyclerView parent, Context context, int position,boolean forHeight)
    {
        final RecyclerView.ViewHolder holder = new HeaderViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chat_tag_date, parent, false));
        final View header = holder.itemView;

        RecyclerView.ViewHolder  viewHolder= new HeaderViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chat_tag_date, parent, false));
        this.adapter.onBindDateItemDecorator(viewHolder,position);

        int widthSpec  = View.MeasureSpec.makeMeasureSpec(parent.getWidth(), View.MeasureSpec.EXACTLY);
        int heightSpec = View.MeasureSpec.makeMeasureSpec(parent.getHeight(), View.MeasureSpec.UNSPECIFIED);

        int childWidth  = ViewGroup.getChildMeasureSpec(widthSpec, parent.getPaddingLeft() + parent.getPaddingRight(), viewHolder.itemView.getLayoutParams().width);
        int childHeight = ViewGroup.getChildMeasureSpec(heightSpec, parent.getPaddingTop() + parent.getPaddingBottom(), viewHolder.itemView.getLayoutParams().height);

        viewHolder.itemView.measure(childWidth, childHeight);
        viewHolder.itemView.layout(0, 0, viewHolder.itemView.getMeasuredWidth(), viewHolder.itemView.getMeasuredHeight());


        //headerCache.put(key, holder);

        return viewHolder;

    }


}