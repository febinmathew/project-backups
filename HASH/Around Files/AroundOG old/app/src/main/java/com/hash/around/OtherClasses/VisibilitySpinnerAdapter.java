package com.hash.around.OtherClasses;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hash.around.R;

import java.util.ArrayList;

/**
 * Created by Febin on 18-10-2017.
 */

public class VisibilitySpinnerAdapter extends BaseAdapter {
    Context context;
   // int flags[];
    String[] visibilityNames;
    LayoutInflater inflter;

    public VisibilitySpinnerAdapter(Context applicationContext,  String[] Names) {
        this.context = applicationContext;
      //  this.flags = flags;
        this.visibilityNames = Names;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return visibilityNames.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.spinner_view_visibility, null);
       // ImageView icon = (ImageView) view.findViewById(R.id.visibilityImage);
        TextView names = (TextView) view.findViewById(R.id.visibilityStatus);
        //icon.setImageResource(flags[i]);
        names.setText(visibilityNames[i]);
        return view;
    }
}