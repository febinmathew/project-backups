package com.hash.around.Views.CustomMakers;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hash.around.R;
import com.hash.around.Views.ViewFunctions;


public class NotificationTag extends RelativeLayout {

    public NotificationTag(Context context) {
        this(context, null);
    }

    public NotificationTag(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }


    RelativeLayout wholeView;
    public LinearLayout notificationSection,requestSection;
    TextView notificationAmount,requestAmount;
    int notificationRealAmount=0,requestRealAmount=0;

    public NotificationTag(final Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        inflate(context, R.layout.notification_tag_view, this);
        wholeView=findViewById(R.id.wholeView);
        notificationSection=findViewById(R.id.notificationSection);
        //notificationSection.setVisibility(GONE);
        requestSection=findViewById(R.id.requestSection);
        //requestSection.setVisibility(GONE);
        notificationAmount=findViewById(R.id.notificationAmount);
        requestAmount=findViewById(R.id.requestAmount);

        if (attrs != null) {
           // TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CircularRelativeLayout, 0, 0);
            //this.radius   = typedArray.getDimension(R.styleable.CircularRelativeLayout_radius, 20);
           // dimen=typedArray.getDimensionPixelSize(R.styleable.CircularRelativeLayout_radius, 4);

            //typedArray.recycle();
        }
        //init(context);
    }

    public void setNotificationAmount(int amount)
    {
        if(amount==0)
        {
            ViewFunctions.fadeOut(notificationSection);
            return;
        }
        notificationAmount.setText(amount+"");
        ViewFunctions.animateButtonBurstIn(notificationSection,50,true);
    }
    public void incrementNotificationAmount(int amount)
    {
        amount+=notificationRealAmount;
        notificationRealAmount=amount;

        notificationAmount.setText(amount+"");
        ViewFunctions.animateButtonBurstIn(notificationSection,50,true);
    }
    public void setRequestAmount(int amount)
    {
        if(amount==0)
        {
            ViewFunctions.fadeOut(requestSection);
            return;
        }
        requestAmount.setText(amount+"");
        ViewFunctions.animateButtonBurstIn(requestSection,50,true);
    }

    public void  hideNotification()
    {
        ViewFunctions.fadeOut(wholeView);
    }
    public void  showNotification()
    {
        ViewFunctions.fadeIn(wholeView);
    }



}
