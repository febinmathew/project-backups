package com.hash.around.OtherClasses;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hash.around.OtherClasses.DataTypes.NotificationData;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.R;
import com.hash.around.TabbedActivity;

import java.util.ArrayList;

import static com.hash.around.MainServiceThread.updateUserChatStatus;
import static com.hash.around.MainServiceThread.updateUserFollowStatus;
import static com.hash.around.Profile_Activity.customNotificationSender;
import static com.hash.around.Profile_Activity.newUserPoke;
import static com.hash.around.Profile_Activity.updateNotificationstatus;


public class Adapter_NoificationData extends RecyclerView.Adapter implements View.OnCreateContextMenuListener{
    Context mContext;
    ArrayList<NotificationData> data;
    public Adapter_NoificationData(Context mcont,ArrayList<NotificationData> data1) {
        mContext = mcont;
        data=data1;

    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v=null;

        switch (viewType)
        {
            case 0:

                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.single_notification, parent, false);
                // set the view's size, margins, paddings and layout parameters

                Adapter_NoificationData.NotificationView vh = new Adapter_NoificationData.NotificationView(v);
                return vh;

            case 1:

                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.single_request_notification, parent, false);
                // set the view's size, margins, paddings and layout parameters

                Adapter_NoificationData.NotificationView1 vh1 = new Adapter_NoificationData.NotificationView1(v);
                return vh1;

        }
        return  null;

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {


        switch (data.get(position).type)
        {
            case 0:
                ((NotificationView)holder).notificationText.setText(data.get(position).text);
                ((NotificationView)holder).actionButton.setVisibility(View.GONE);

                break;
            case 1:
                ((NotificationView)holder).notificationText.setText(data.get(position).text);
                if(data.get(position).status==0) {
                    ((NotificationView) holder).actionButton.setText("Pock Back");
                    ((NotificationView) holder).actionButton.setVisibility(View.VISIBLE);
                    ((NotificationView) holder).actionButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            newUserPoke(data.get(position).userid);
                            data.get(position).status = 1;
                            updateNotificationstatus(data.get(position));
                            notifyItemChanged(position);
                        }
                    });
                }
                else
                {
                    ((NotificationView) holder).actionButton.setText("You Pocked!");
                    ((NotificationView) holder).actionButton.setVisibility(View.VISIBLE);
                }
                break;
            case 2:
                ((NotificationView)holder).notificationText.setText(data.get(position).text);
                ((NotificationView)holder).notification_image.setImageResource(R.drawable.nick);
                ((NotificationView)holder).notificationText.setText("Amal Antoney Shared your image");
                ((NotificationView)holder).actionButton.setVisibility(View.GONE);
               // ((NotificationView)holder).itemView.set

                break;

            case 3:
                ((NotificationView)holder).notificationText.setText(data.get(position).text);
                ((NotificationView)holder).notification_image.setImageResource(R.drawable.nick);
                ((NotificationView)holder).notificationText.setText("Amal Antoney Accepted You Follow Request");
                ((NotificationView)holder).actionButton.setVisibility(View.GONE);
                // ((NotificationView)holder).itemView.set

                break;
            case 4:
                ((NotificationView)holder).notificationText.setText(data.get(position).text);
                ((NotificationView)holder).notification_image.setImageResource(R.drawable.nick);
                ((NotificationView)holder).notificationText.setText("Amal Antoney Accepted your chat Request");
                ((NotificationView)holder).actionButton.setVisibility(View.GONE);
                // ((NotificationView)holder).itemView.set

                break;

            case 11:
                ((NotificationView1)holder).notificationText.setText(data.get(position).text);
                ((NotificationView1)holder).notification_image.setImageResource(R.drawable.nick);
                ((NotificationView1)holder).notificationText.setText("Amal made a follow request");
                ((NotificationView1)holder).actionButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {

                        customNotificationSender(data.get(position).userid, 3, "", new JobCompletionListner() {
                                @Override
                                public void onJobCompletion(String txt1) {
                                    //((NotificationView1)holder).actionButton.
                                    //(Te)v.findViewById(R.id.actionButton)
                                    updateUserFollowStatus(data.get(position).userid,2);
                                    removeSingleNotificationFromDatabase(data.get(position).rowId);
                                }
                            });
                    }
                });
                ((NotificationView1)holder).actionButtonCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        customNotificationSender(data.get(position).userid, 26, "", new JobCompletionListner() {
                                @Override
                                public void onJobCompletion(String txt1) {
                                    //((NotificationView1)holder).actionButton.
                                    updateUserFollowStatus(data.get(position).userid,5);
                                    removeSingleNotificationFromDatabase(data.get(position).rowId);
                                }
                            });

                    }
                });
                break;

            case 12:
                ((NotificationView1)holder).notificationText.setText(data.get(position).text);
                ((NotificationView1)holder).notification_image.setImageResource(R.drawable.nick);
                ((NotificationView1)holder).notificationText.setText("Amal made a chat request");
                ((NotificationView1)holder).actionButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                            updateUserChatStatus(data.get(position).userid,1);
                            customNotificationSender(data.get(position).userid, 4, TabbedActivity.userdetail.getPhoneNumber(), new JobCompletionListner() {
                                @Override
                                public void onJobCompletion(String txt1) {
                                    //((NotificationView1)holder).actionButton.
                                }
                            });

                    }
                });

                ((NotificationView1)holder).actionButtonCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                            customNotificationSender(data.get(position).userid, 27, "", new JobCompletionListner() {
                                @Override
                                public void onJobCompletion(String txt1) {
                                    //((NotificationView1)holder).actionButton.
                                    removeSingleNotificationFromDatabase(data.get(position).rowId);
                                }
                            });

                    }
                });
                break;


        }
    }

    static void removeSingleNotificationFromDatabase(long row)
    {
        SQLiteDatabase db=TabbedActivity.db.getWritableDatabase();

        int quantity = db.delete(DatabaseAccesser.notification_table," rowid =?",new String[]{row+""});
        Log.e("MSGG","notification deleted : "+quantity);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(data.get(position).type<10)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

    }

    public static class NotificationView extends RecyclerView.ViewHolder {

        public TextView actionButton;
        public ImageView notification_image;
        public TextView notificationText;
        public TextView notificationDate;
        public NotificationView(View item) {
            super(item);

            actionButton=(TextView)item.findViewById(R.id.actionButton);
            notification_image=(ImageView)item.findViewById(R.id.notification_image);
            notificationText=(TextView)item.findViewById(R.id.notificationText);
            notificationDate=(TextView)item.findViewById(R.id.notificationDate);

        }
    }
    public static class NotificationView1 extends RecyclerView.ViewHolder {

        public TextView actionButton,actionButtonCancel;
        public ImageView notification_image;
        public TextView notificationText;
        public TextView notificationDate;
        public NotificationView1(View item) {
            super(item);

            actionButton=(TextView)item.findViewById(R.id.actionButton);
            notification_image=(ImageView)item.findViewById(R.id.notification_image);
            notificationText=(TextView)item.findViewById(R.id.notificationText);
            notificationDate=(TextView)item.findViewById(R.id.notificationDate);
            actionButtonCancel=(TextView)item.findViewById(R.id.actionButtonCancel);

        }
    }

}