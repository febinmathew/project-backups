package com.hash.around;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Point;
import android.location.Location;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.SwitchPreferenceCompat;
import android.support.v7.widget.AppCompatSeekBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.hash.around.OtherClasses.Adapter_GroupMembers;
import com.hash.around.OtherClasses.Adapter_NoificationData;
import com.hash.around.OtherClasses.DataTypes.ContactListData;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.OtherClasses.DataTypes.GroupMemberData;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.OtherClasses.DataTypes.NotificationData;
import com.hash.around.OtherClasses.DataTypes.UserPrivacyData;
import com.hash.around.OtherClasses.Interfaces.UserPrivacyStatusListner;
import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.hash.around.Utils.StringUtils;
import com.hash.around.Utils.UserManager;
import com.hash.around.Views.CustomMakers.AroundListPreferenceView;
import com.hash.around.Views.CustomMakers.AroundPreferenceView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static android.support.v4.graphics.TypefaceCompatUtil.getTempFile;
import static android.view.View.GONE;
import static com.hash.around.ChatWindow.checkGroupOnlineStatusWithListners;
import static com.hash.around.ChatWindow.checkSingleOnlineStatusWithListners;
import static com.hash.around.MainServiceThread.downloadFileAndReturnUriString;
import static com.hash.around.MainServiceThread.newUserFollowRequestWithDoubleCheck;

import static com.hash.around.MainServiceThread.removeSingleUserFromGroup;

import static com.hash.around.MainServiceThread.updateUserFollowStatus;
import static com.hash.around.OtherClasses.AdapterPeopleNearby.locationDistanceToString;
import static com.hash.around.OtherClasses.Adapter_Contacts.checkIfPrivacyShouldBeActivie;
import static com.hash.around.TabbedActivity.saveContactMusicToDatabase;
import static com.hash.around.TabbedActivity.userdetail;

public class Profile_Activity extends AppCompatActivity {
    Toolbar toolbar;
    float WindowHeight;
    ScrollView sView;
    static String userId;
    int userType;
    ImageView imgView;
    TextView userStatus;
    TextView profileName;
    RecyclerView notificationRview;
    Adapter_NoificationData adap;
    Adapter_GroupMembers memberadap;
    private LinearLayoutManager mLayoutManager;
//String defaultProfileImageUri;
FloatingActionButton userFloatingButtonFollow;
    ImageButton statusEditButton,changeProfileImage,profileNameEdit,bgmusicEditButton;

    ArrayList<NotificationData> finaldata=new ArrayList<>();
    ArrayList<GroupMemberData> memberdata=new ArrayList<>();
    ArrayList<ContactListData> contactdata=new ArrayList<>();


    TextView quitGroup;
    TextView reportGroup;
    public static Integer userPrivilage=null;

    UserWholeData userData;

   TextView lastSeenText,userLcoationText;

    public static void updateNotificationstatus(NotificationData data)
    {
        SQLiteDatabase db= TabbedActivity.db.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseAccesser.notification_status, 1);
        int rowln=db.update(DatabaseAccesser.notification_table,values,
                "rowid"+" =? ",new String[]{data.rowId+""});
    }

    void fillFinaldataFromDatabase()
    {
        SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();

        Cursor cursor = db.query(DatabaseAccesser.notification_table, new String[]{"rowid",DatabaseAccesser.notification_user_id,
                        DatabaseAccesser.notification_timestamp,
                        DatabaseAccesser.notification_type,
                        DatabaseAccesser.notification_status,
                        DatabaseAccesser.notification_text,
                        DatabaseAccesser.notification_image_url,
                        DatabaseAccesser.notification_link},
                null,
                null,
                null, null, DatabaseAccesser.notification_timestamp+"",null);

        Log.e("MSG","Size of cursor :"+cursor.getCount());
        while (cursor.moveToNext()) {


            NotificationData temp = new NotificationData();

            temp.rowId= cursor.getLong(
                    cursor.getColumnIndexOrThrow("rowid"));
            temp.userid = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.notification_user_id));
            temp.timestamp = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.notification_timestamp));
            temp.type = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.notification_type));
            temp.status = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.notification_status));

            temp.text = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.notification_text));
            temp.image = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.notification_image_url));
            temp.link = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.notification_link));

            finaldata.add(temp);
        }
        cursor.close();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profilepage_activity);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
// DO NOT ever call getBaseContext() unless you know what you're doing. "this" or "getApplicationContext() is perfect. Google the difference.
        String ringtonePreference = prefs.getString("ringtonePref", "DEFAULT_RINGTONE_URI");
// The key of preference was "@string/ringtonePref" which is useless since you're hardcoding the string here anyway.
        Uri ringtoneuri = Uri.parse(ringtonePreference);
        Log.e("MGG","ringtone is "+ringtoneuri);


        toolbar= (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        userId=getIntent().getStringExtra("userid");

       /* FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
       getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //getSupportActionBar().setLogo(R.mipmap.logo1);


        //getSupportActionBar().setTitle("Around");
        //getSupportActionBar().setSubtitle("online");
        //getSupportActionBar().setTitle("Alan");



        Point x= new Point();
        getWindowManager().getDefaultDisplay().getSize(x);
        WindowHeight=x.y;
        //sView= (ScrollView)findViewById(R.id.sScrollView);
        imgView =(ImageView) findViewById(R.id.imgView);
        profileName=(TextView)findViewById(R.id.profileName);
        userStatus=(TextView)findViewById(R.id.userStatus);
        notificationRview=(RecyclerView) findViewById(R.id.notificationRview);
        quitGroup=(TextView) findViewById(R.id.quitGroup);
        reportGroup=(TextView) findViewById(R.id.reportGroup);
        userFloatingButtonFollow=(FloatingActionButton)findViewById(R.id.userFloatingButtonFollow);
        profileNameEdit=(ImageButton)findViewById(R.id.profileNameEdit);
        changeProfileImage=(ImageButton)findViewById(R.id.changeProfileImage);
        statusEditButton=(ImageButton)findViewById(R.id.statusEditButton);
        bgmusicEditButton=(ImageButton)findViewById(R.id.bgmusicEditButton);
        lastSeenText=(TextView)findViewById(R.id.lastSeenText);
        userLcoationText=(TextView)findViewById(R.id.userLcoationText);
        lastSeenText.setVisibility(GONE);
        userLcoationText.setVisibility(GONE);
        AppBarLayout mAppBarLayout = (AppBarLayout) findViewById(R.id.appBar);
        mAppBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    isShow = true;
                    //showOption(R.id.action_info);
                    //Log.e("MGG","show now");        // heavy code do something CODE 47
                    followItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                } else if (isShow) {
                    isShow = false;
                    Log.e("MGG","disable now");
                    followItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
                    //hideOption(R.id.action_info);
                }
            }
        });

        // imgheight= imgView.getLayoutParams().height;
       // Toast.makeText(this, ""+imgheight, Toast.LENGTH_SHORT).show();
       // = (FrameLayout.LayoutParams) imgView.getLayoutParams();
        /*sView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {

               float alpha= sView.getScrollY()/(WindowHeight/2);
                if(alpha>=0 && alpha <=1)
                {
                    toolbar.setBackgroundColor(Color.argb((int) (alpha * 255),147, 108, 108));

                    //imgView.getLayoutParams().height = 200;


                   // params.height = (int)((1-alpha)*imgheight);
                   // imgView.setLayoutParams(params);

                }
            }
        });*/
       // accessDatabseForContent();


        userData=ChatWindow.returnSingleUserDetails(userId);
        Log.e("MGG", "==userchat type :" + userData.chatType);
        Log.e("MGG", "==userfollow type :" + userData.followType);
        Log.e("MGG", "==userprofile image  :" + userData.priv_profile_picture);
        Log.e("MGG", "==user status  :" + userData.priv_profile_status);
        Log.e("MGG", "==user number :" + userData.priv_number);
        userType=userData.type;
       // defaultProfileImageUri=userData.img_url;



        profileName.setText(userData.user_name);



       /* if(userData.status.equals("null"))
            userData.status="Wanna know an App called Around?";

        userStatus.setText(userData.status);
*/

        /*if(userData.img_url.equals("null"))
        {
            imgView.setImageResource(R.drawable.default_user);
        }
        else
        {
            imgView.setImageURI(Uri.parse(defaultProfileImageUri));
        }*/



            if(userId.equals(TabbedActivity.userdetail.getUid()) && userType==0) { //self

                defineAccountEditable(true);


                reportGroup.setVisibility(GONE);
                quitGroup.setVisibility(GONE);
               // findViewById(R.id.numberSection).setVisibility(GONE);
                mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
                mLayoutManager.setReverseLayout(true);
                mLayoutManager.setStackFromEnd(true);
                //  mLayoutManager=new GridLayoutManager(this,2,LinearLayoutManager.HORIZONTAL,false);
                // mLayoutManager=new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);

                notificationRview.setLayoutManager(mLayoutManager);

                //fillFinaldataWithDummy();
                fillFinaldataFromDatabase();
                Log.e("MSG", "Size of finaldata :" + finaldata.size());
                //adap = new Adapter_NoificationData(this,finaldata);
                //notificationRview.setAdapter(adap);
                notificationRview.setVisibility(GONE);


        }
        else if(userType==5) //group
        {
            setupGroupInWindow();
            if(userPrivilage==1)
            {
                defineAccountEditable(true);
            }
            checkGroupOnlineStatusProfile();

        }
        else//normal user
            {



                defineAccountEditable(false);

                reportGroup.setVisibility(GONE);
                quitGroup.setVisibility(GONE);
            notificationRview.setVisibility(GONE);
            findViewById(R.id.notificationDisplayInfo).setVisibility(GONE);


        }
        setFloatingButton();

        switch (userType)
        {
            case 0:
                showProfileImage(true);
                showProfileStatus(true);
                showProfileNumber(false);
                setUpBgMusicPlayback(true);
                break;
            case 5:
                showProfileImage(true);
                showProfileStatus(true);
                setUpBgMusicPlayback(true);
                break;
            case 6:
                showProfileImage(true);
                showProfileStatus(true);
                showProfileNumber(false);
                setUpBgMusicPlayback(true);
                break;
            default:

                if (checkIfPrivacyShouldBeActivie(userData.priv_profile_picture,userData.remoteContact))
                {
                    showProfileImage(true);
                }
                else
                {
                    showProfileImage(false);
                }
                if (checkIfPrivacyShouldBeActivie(userData.priv_profile_status,userData.remoteContact))
                {
                    showProfileStatus(true);
                }
                else
                {
                    showProfileStatus(false);
                }

                if (checkIfPrivacyShouldBeActivie(userData.priv_number,userData.remoteContact))
                {
                    showProfileNumber(true);
                }
                else
                {
                    showProfileNumber(false);
                }


                if (checkIfPrivacyShouldBeActivie(userData.priv_profile_music,userData.remoteContact))
                {
                    setUpBgMusicPlayback(true);
                }
                else
                {
                    setUpBgMusicPlayback(false);
                }

                if (checkIfPrivacyShouldBeActivie(userData.priv_activity_status,userData.remoteContact))
                {
                    checkSingleOnlineStatusWithListners(userData.user_id, new JobCompletionListner() {
                        @Override
                        public void onJobCompletion(String txt1) {
                            lastSeenText.setVisibility(View.VISIBLE);
                            lastSeenText.setText(txt1);
                        }
                    });
                }
                if (checkIfPrivacyShouldBeActivie(userData.priv_location,userData.remoteContact))
                {
                    double lat = Double.parseDouble(TabbedActivity.prefs.getString("location_latitude", "0.0"));
                    double lng = Double.parseDouble(TabbedActivity.prefs.getString("location_longitude", "0.0"));


                    if(userData.latitude==0 || userData.longitude==0 || lat==0 || lng==0)
                    {
                        Log.e("MGG","0000000000000");
                    }
                    else
                        {
                            Location target=new Location("target");
                            target.setLatitude(userData.latitude);
                            target.setLatitude(userData.longitude);
                            Location source=new Location("source");
                            source.setLatitude(lat);
                            source.setLongitude(lng);
                            lastSeenText.setVisibility(View.VISIBLE);
                            lastSeenText.setText(locationDistanceToString(source.distanceTo(target)));
                    }
                }
                break;

        }




        statusEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent editText = new Intent(Profile_Activity.this, EditAnyTextActivity.class);
                editText.putExtra("text_type", 1);
                startActivityForResult(editText,11);

            }
        });

        profileNameEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent editText = new Intent(Profile_Activity.this, EditAnyTextActivity.class);
                editText.putExtra("text_type", 0);
                startActivityForResult(editText,12);
            }
        });

        changeProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //getPickImageIntent(getApplicationContext());
                //Intent intent = new Intent(Intent.ACTION_PICK,
                       // android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_PICK);
                //intent.addCategory(Intent.CATEGORY_OPENABLE);
                startActivityForResult(Intent.createChooser(intent, "GET FILE"), 5);
            }
        });

        bgmusicEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //getPickImageIntent(getApplicationContext());
                //Intent intent = new Intent(Intent.ACTION_PICK,
                // android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                Intent intent = new Intent();
                intent.setType("audio/mpeg");
                intent.setAction(Intent.ACTION_PICK);
                //intent.addCategory(Intent.CATEGORY_OPENABLE);
                startActivityForResult(Intent.createChooser(intent, "GET FILE"), 20);
            }
        });

        profile_settings=new ProfileNotificationSettings();
        Bundle args = new Bundle();
        args.putString("notificationUri", userData.not_uri);
        args.putInt("vibration", userData.not_vibration);
        args.putInt("light", userData.not_light);

        profile_settings.setArguments(args);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.profile_fragment, profile_settings)
                .commit();

        /*getSupportFragmentManager().beginTransaction()
                .replace(R.id.profile_fragment, page)
                .commitAllowingStateLoss();*/
    }
    ProfileNotificationSettings profile_settings;
    SparseBooleanArray userOnline=new SparseBooleanArray();
    void checkGroupOnlineStatusProfile()
    {
        ArrayList<String> memberList=new ArrayList<>();
       for(int i=0;i<memberdata.size();i++)
       {
           memberList.add(memberdata.get(i).userid);
       }
        checkGroupOnlineStatusWithListners(memberList,userOnline,new JobCompletionListner(){
            @Override
            public void onJobCompletion(String txt1) {
                showGroupOnlineStatus();
            }
        });
    }
    void showGroupOnlineStatus()
    {
        int count=0;
        for(int i=0;i<userOnline.size();i++)
        {
            if(userOnline.get(userOnline.keyAt(i))==true)
            {
                count++;
            }

        }
        lastSeenText.setVisibility(View.VISIBLE);
        lastSeenText.setText(count+" online");
    }

    void showProfileImage(boolean show)
    {
        if (userData.img_url.equals("null") || !show)
        {
            imgView.setImageResource(R.drawable.default_user);
        } else {
            imgView.setImageURI(Uri.parse(userData.img_url));
        }
    }

    void showProfileStatus(boolean show)
    {
        if (!show)
        {
            findViewById(R.id.statusSection).setVisibility(GONE);
        } else
            {
            if(userData.status.equals("null"))
                userData.status=getResources().getString(R.string.default_user_status);;
            userStatus.setText(userData.status);
        }
    }
    void showProfileNumber(boolean show)
    {
        if (!show)
        {
            findViewById(R.id.numberSection).setVisibility(GONE);
        } else
        {
           /* if(userData.status.equals("null"))
                userData.status=getResources().getString(R.string.default_user_status);;*/
           TextView number=(TextView)  findViewById(R.id.userNumber);
            number.setText(userData.number);

        }
    }
    void setFloatingButton()
    {

        Log.e("MGG","user follwo "+ userData.followType);
        if (userData.followType==1)
        {
            userFloatingButtonFollow.setImageResource(R.drawable.ic_chat_bubble_white);
            userFloatingButtonFollow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    Intent indi = new Intent(Profile_Activity.this, ChatWindow.class);
                    indi.putExtra("userid",userData.user_id);
                    //indi.putExtra("username",data.get(pos).name);
                    //indi.putExtra("user_type",data.get(pos).type);
                    finish();

                }
            });
        }
        else if(userData.followType==3)
            {
                userFloatingButtonFollow.setImageResource(R.drawable.ic_send_white);
                userFloatingButtonFollow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Snackbar.make(findViewById(R.id.rootlayout), "Follow Request Already made", Snackbar.LENGTH_LONG).show();
                    }
                });
            }

            else {
                userFloatingButtonFollow.setImageResource(R.drawable.ic_notifications_white);
                userFloatingButtonFollow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        newUserFollowRequestWithDoubleCheck(userData.user_id, new JobCompletionListner() {
                            @Override
                            public void onJobCompletion(String txt1) {
                                userData.followType = Integer.parseInt(txt1);
                                updateFollowDetails();
                            }
                        });
                    }
                });
            }
           /* if(userData.status.equals("null"))
                userData.status=getResources().getString(R.string.default_user_status);;
            userStatus.setText(userData.status);*/

    }


    void defineAccountEditable(boolean editablePrivilage)
    {

        if(editablePrivilage)
        {
            statusEditButton.setVisibility(View.VISIBLE);
            profileNameEdit.setVisibility(View.VISIBLE);
            changeProfileImage.setVisibility(View.VISIBLE);
            bgmusicEditButton.setVisibility(View.VISIBLE);
        }
        else
        {
            statusEditButton.setVisibility(View.GONE);
            profileNameEdit.setVisibility(View.GONE);
            changeProfileImage.setVisibility(View.GONE);
            bgmusicEditButton.setVisibility(View.GONE);
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == 5 && resultCode == RESULT_OK) {

                Uri directory = data.getData();

                Intent cropIntent=new Intent(this,ImageCropActivity.class);
                cropIntent.putExtra("uri",directory.toString());
                startActivityForResult(cropIntent,1);

            }
            else  if (requestCode == 1 && resultCode == RESULT_OK) {


                String uricropped=data.getStringExtra("uri");
                //imgView.setImageURI(Uri.parse(uricropped));

                uploadProfileImageAndUpdateOnDatabase(Uri.parse(uricropped));

            }

           else if (requestCode == 11 && resultCode == RESULT_OK) {

                String staus=data.getStringExtra("result");
                userStatus.setText(staus);
            }

        else if (requestCode == 12 && resultCode == RESULT_OK) {

                profileName.setText(data.getStringExtra("result"));
        }

        else  if (requestCode == 20 && resultCode == RESULT_OK) {

                Uri directory = data.getData();

                uploadProfileMusicAndUpdateOnDatabase(directory);

               // Intent cropIntent=new Intent(this,ImageCropActivity.class);
                //cropIntent.putExtra("uri",directory.toString());
               // startActivityForResult(cropIntent,1);

            }





    }

    void uploadProfileMusicAndUpdateOnDatabase(final Uri newurl)
    {
        StorageReference uploadRef;

        Log.e("MSGG","uploading NOW " + newurl.toString());
        uploadRef = FirebaseStorage.getInstance().getReference().child("profile_musics").child(StringUtils.randomStringGenerator() + ".mp3");
        uploadRef.putFile(newurl)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        Log.e("MSGG","success NOW");
                        final String downloadUrl = taskSnapshot.getDownloadUrl().toString();
                        Log.e("MSGG","usrl NOW "+downloadUrl.toString());
                        DatabaseReference updateRef2 = FirebaseDatabase.getInstance().getReference("user_profile").child(userId).child("music_url");
                        Log.e("MSGG","pushing NOW ");
                        updateRef2.setValue(downloadUrl);
                        Log.e("MSGG","pushed NOW");
                        //imgView.setImageURI(newurl);
                        userData.music_url=newurl.toString();
                        resetMediaPlayer();
                        TabbedActivity.saveContactMusicToDatabase(userdetail.getUid(),newurl.toString(),downloadUrl);
                    }

                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("MSGG","failed NOW");
                //imgView.setImageURI(Uri.parse(userData.img_url));
            }
        });

       // setingspage=

    }

    public static class ProfileNotificationSettings extends PreferenceFragmentCompat implements
            SharedPreferences.OnSharedPreferenceChangeListener
    {
        @Override
        public void setArguments(@Nullable Bundle args) {
            notificationUri=args.getString("notificationUri");
            vibration=args.getInt("vibration");
            light=args.getInt("light");
            //updateData();
        }

        String notificationUri="Default";
        int vibration=0;
        int light=0;
        int mute=0;
        public ProfileNotificationSettings()
        {

        }

        AroundPreferenceView notificationSoundPreference;
        SwitchPreferenceCompat mutePreference;
        AroundListPreferenceView vibrationPreference;
        AroundListPreferenceView notificationLightreference;
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            addPreferencesFromResource(R.xml.userprofile_preference);
             notificationSoundPreference = (AroundPreferenceView) this.findPreference("pref_chat_notification_sound");
             mutePreference = (SwitchPreferenceCompat) this.findPreference("pref_chat_mute");
             vibrationPreference = (AroundListPreferenceView) this.findPreference("pref_chat_vibration");
             notificationLightreference = (AroundListPreferenceView) this.findPreference("pref_chat_notification_light");


            notificationSoundPreference.setIcon(R.drawable.ic_notifications_white);
            //mutePreference.setIcon(R.id.sou);
            notificationLightreference.setIcon(R.drawable.ic_brightness_white);
            vibrationPreference.setIcon(R.drawable.ic_vibration_primary);
            vibrationPreference.setSummary("Default");
            //mutePreference
            updateData();

        }
        void updateData()
        {String title;
            if(notificationUri!=null && !notificationUri.equals("") ){
            Ringtone ringtone = RingtoneManager.getRingtone(getActivity(), Uri.parse(notificationUri));
            title = ringtone.getTitle(getContext());

            title = Uri.parse(notificationUri).equals(Settings.System.DEFAULT_NOTIFICATION_URI) ? "Default" : title;

            //Log.e("MGG", "notificationUri " + Uri.parse(notificationUri));
            //Log.e("MGG", "DEFAULT_NOTIFICATION_URI  " + Settings.System.DEFAULT_NOTIFICATION_URI);
            // Ringtone ringtone=notificationSummery

        }
        else
            {title="Default";}
            notificationSoundPreference.setSummary(title);

            vibrationPreference.setValueIndex(vibration);
            vibrationPreference.setSummary(vibrationPreference.getEntry());

            notificationLightreference.setValueIndex(light);
            notificationLightreference.setSummary(notificationLightreference.getEntry());

            if(mute!=0 && mute>System.currentTimeMillis()+1000*30)
            mutePreference.setChecked(true);
            else
                mutePreference.setChecked(false);


        }
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            Preference pref = findPreference(key);
            if(key.equals("pref_chat_vibration"))
            {
               // Log.e("MGG","sharedPreferences "+);

                    vibration=Integer.parseInt(sharedPreferences.getString(key, "0"));
                    updateData();
                    UserManager.updateUserChatNotification(userId,null,vibration,null,null);



            }
            else if(key.equals("pref_chat_notification_light"))
            {
                //Log.e("MGG","sharedPreferences "+sharedPreferences.getString(key, "0"));
                light=Integer.parseInt(sharedPreferences.getString(key, "0"));
                updateData();
                UserManager.updateUserChatNotification(userId,null,null,light,null);
            }
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {

            if(resultCode==RESULT_OK && requestCode==2)
            {
                Uri uri = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
                if(uri!=null)
                {
                    notificationUri=uri.toString();
                updateData();
                UserManager.updateUserChatNotification(userId,uri.toString(),null,null,null);
                }
            }
            super.onActivityResult(requestCode, resultCode, data);

        }

        @Override
        public boolean onPreferenceTreeClick(Preference preference) {
            if (preference.getKey().equals("pref_chat_notification_sound")) {
                Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_NOTIFICATION);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, true);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_DEFAULT_URI, Settings.System.DEFAULT_NOTIFICATION_URI);


                if (notificationUri != null && !notificationUri.equals("")) {
                    intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, Uri.parse(notificationUri));
                } else {
                    intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, Settings.System.DEFAULT_NOTIFICATION_URI);
                }

                startActivityForResult(intent, 2);
                return true;

                /*Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_NOTIFICATION);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, true);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_DEFAULT_URI, Settings.System.DEFAULT_NOTIFICATION_URI);

                String existingValue = null;//getRingtonePreferenceValue(); // TODO
                if (existingValue != null) {
                    if (existingValue.length() == 0) {
                        // Select "Silent"
                        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, (Uri) null);
                    } else {
                        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, Uri.parse(existingValue));
                    }
                } else {
                    // No ringtone has been selected, set to the default
                    intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, Settings.System.DEFAULT_NOTIFICATION_URI);
                }

                startActivityForResult(intent, 2);*/
            }
            else if(preference.getKey().equals("pref_chat_mute"))
            {
                final SwitchPreferenceCompat mutePref=(SwitchPreferenceCompat)findPreference("pref_chat_mute");

                Log.e("MGG","switch  "+mutePref.isChecked());
                if(mutePref.isChecked()) {
                    mutePref.setChecked(false);
                    AroundAlertBoxManager.showConversationMuteDialog(getActivity(), new AroundAlertBoxManager.MuteSelectionListener() {
                        @Override
                        public void onMuted(long until, boolean notificationAlso) {
                            Log.e("MGG", "switch switched " + until);
                            mutePref.setChecked(true);
                            UserManager.updateUserChatNotification(userId,null,null,null,until*1000L);
                        }
                    });
                }
                else
                {
                    UserManager.updateUserChatNotification(userId,null,null,null,0L);
                }
                return true;
            }
            return super.onPreferenceTreeClick(preference);
        }
    }
    void uploadProfileImageAndUpdateOnDatabase(final Uri newurl)
    {
        StorageReference uploadRef;

        Log.e("MSGG","uploading NOW " + newurl.toString());
        uploadRef = FirebaseStorage.getInstance().getReference().child("profile_images").child(StringUtils.randomStringGenerator() + ".png");
        uploadRef.putFile(newurl)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        Log.e("MSGG","success NOW");
                        final String downloadUrl = taskSnapshot.getDownloadUrl().toString();
                        Log.e("MSGG","usrl NOW "+downloadUrl.toString());
                        DatabaseReference updateRef2 = FirebaseDatabase.getInstance().getReference("user_profile").child(userId).child("img_url");
                        Log.e("MSGG","pushing NOW ");
                        updateRef2.setValue(downloadUrl);
                        Log.e("MSGG","pushed NOW");
                        imgView.setImageURI(newurl);
                        userData.img_url=newurl.toString();
                        EditAnyTextActivity.updateNameOrStatusOrProfileImage(userdetail.getUid(),null,null,newurl.toString());
                    }

                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("MSGG","failed NOW");
                imgView.setImageURI(Uri.parse(userData.img_url));
            }
        });
    }
    public static Intent getPickImageIntent(Context context) {
        Intent chooserIntent = null;

        List<Intent> intentList = new ArrayList<>();

        Intent pickIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePhotoIntent.putExtra("return-data", true);
        takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(getTempFile(context)));
        intentList = addIntentsToList(context, intentList, pickIntent);
        intentList = addIntentsToList(context, intentList, takePhotoIntent);

        if (intentList.size() > 0) {
            chooserIntent = Intent.createChooser(intentList.get(intentList.size() - 1),"GET FILE");
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentList.toArray(new Parcelable[]{}));
        }

        return chooserIntent;
    }
    private static List<Intent> addIntentsToList(Context context, List<Intent> list, Intent intent) {
        List<ResolveInfo> resInfo = context.getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo resolveInfo : resInfo) {
            String packageName = resolveInfo.activityInfo.packageName;
            Intent targetedIntent = new Intent(intent);
            targetedIntent.setPackage(packageName);
            list.add(targetedIntent);
        }
        return list;
    }


    void setupGroupInWindow()
    {
        quitGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeSingleUserFromGroup(userdetail.getUid(),userId);
            }
        });
        //findViewById(R.id.numberSection).setVisibility(GONE);
        ((TextView)findViewById(R.id.notificationExtraText)).setText("GROUP MEMBERS");
        findViewById(R.id.notificationExtraButton).setVisibility(View.INVISIBLE);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);


        notificationRview.setLayoutManager(mLayoutManager);

        fillGroupMemberdetails(userId);
        Log.e("MSG","Size of memberdata :"+memberdata.size());
        memberadap = new Adapter_GroupMembers(this,contactdata,memberdata);
        notificationRview.setAdapter(memberadap);

        registerForContextMenu(notificationRview);



    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {


        int pos= Adapter_GroupMembers.optionPosition;
        String selectedUser = memberadap.contactdata.get(pos).user_id;
        switch (item.getItemId()) {
            case R.id.member_remove:

                Log.e("MSGG","removing another user");
                removeSingleUserFromGroup(selectedUser,userId);
                break;
        }
        return super.onContextItemSelected(item);

    }

    void fillGroupMemberdetails(String uID)
    {
        SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();


        Cursor cursor=db.rawQuery(
                "SELECT * FROM "+DatabaseAccesser.groupsmembers_table+" INNER JOIN "+DatabaseAccesser.user_table+" ON "+
                        DatabaseAccesser.groupsmembers_table+"."+DatabaseAccesser.groupsmembers_user_id+" = "+ DatabaseAccesser.user_table+"."+DatabaseAccesser.user_user_id
                        +" WHERE "+DatabaseAccesser.groupsmembers_table+"."+DatabaseAccesser.groupsmembers_group_id +" =?" ,
               new String[]{uID});

        //+" WHERE "+DatabaseAccesser.groups_participant_table+"."+DatabaseAccesser.group_user_id +" =?"


        Log.e("MSG","Cusror count : "+cursor.getCount());
        while (cursor.moveToNext()) {


            GroupMemberData temp = new GroupMemberData();
            ContactListData temp2=new ContactListData();

            temp.userid = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.groupsmembers_user_id));
            temp2.user_id = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_user_id));
            temp2.user_name = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_name));

            temp.member_type = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.groupsmembers_member_type));

            temp2.status = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_status));
            temp2.img_url=cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_localUrl));

            temp2.type= cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.groupsmembers_member_type));
            if(temp2.user_id.equals(userdetail.getUid()))
            {
                userPrivilage = temp2.type;
            }
            memberdata.add(temp);
            contactdata.add(temp2);
        }
        cursor.close();

    }


    void accessDatabseForContent()
    {

        SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();

        Cursor cursor = db.query(DatabaseAccesser.user_table,
                new String[]{
                        DatabaseAccesser.user_user_id,
                        DatabaseAccesser.user_name,
                        DatabaseAccesser.user_user_type,
                        DatabaseAccesser.user_status,
                        DatabaseAccesser.user_localUrl,
                        DatabaseAccesser.user_globalUrl},
                DatabaseAccesser.user_user_id + " =?",
                new String[]{userId},
                null, null, null,null);


        //Toast.makeText(this, cursor.getCount(), Toast.LENGTH_SHORT).show();

        while (cursor.moveToNext()) {

        userType=cursor.getInt(
        cursor.getColumnIndexOrThrow(DatabaseAccesser.user_user_type));

            String pro=cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_name));
            profileName.setText(pro);

            pro=cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_status));

            if(pro.equals("null"))
                pro="Wanna know an App called Around?";

            userStatus.setText(pro);


            userData.img_url=cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_localUrl));
            String status=cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_status));
            if( userData.img_url.equals("null"))
            {
                imgView.setImageResource(R.drawable.default_user);
            }
            else
            {
                imgView.setImageURI(Uri.parse( userData.img_url));
            }


            if(status.equals("null"))
            {
               // userStatus.setText();
            }
            else
            {
                userStatus.setText(status);
            }
            userId=cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_user_id));







        }
        cursor.close();
    }

    MenuItem followItem;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.user_profile_menu, menu);
        profileMenu=menu;
        if(userId.equals(userdetail.getUid()))
        {
            menu.findItem(R.id.action_poke_user).setVisible(false);
            menu.findItem(R.id.action_follow_user).setVisible(false);
        }
        followItem = menu.findItem(R.id.action_follow_user);
        //followItem.setVisible(false);


        if(userData.followType==11)
        {
            menu.findItem(R.id.action_chat_block_user).setTitle("UnBlock User");
        }
        else
        {
            menu.findItem(R.id.action_chat_block_user).setTitle("Block User");
        }
        if(userData.followType==1)
        {
            menu.findItem(R.id.action_follow_user).setTitle("UnFollow");
            menu.findItem(R.id.action_follow_user).setIcon(R.drawable.ic_chat_bubble_white);
        }
        else
        {
            menu.findItem(R.id.action_follow_user).setTitle("Follow");
            menu.findItem(R.id.action_follow_user).setIcon(R.drawable.ic_follow_white);
        }
        if(userData.type==2)
        {
            menu.findItem(R.id.action_view_contact_addressbook).setTitle("View in Phonebook");
        }
        else
        {
            menu.findItem(R.id.action_view_contact_addressbook).setTitle("Add to Phonebook");
        }

        if (checkIfPrivacyShouldBeActivie(userData.priv_number,userData.remoteContact))
        {
            //menu.findItem(R.id.action_view_contact_addressbook).setEnabled(false);
        }
        else
        {
            menu.findItem(R.id.action_view_contact_addressbook).setEnabled(false);
        }


        return true;
    }

    void updateFollowDetails()
    {
        setFloatingButton();
        if(userData.followType==1)
        { followItem.setIcon(R.drawable.ic_send_white);
        followItem.setTitle("UnFollow");}
        else
        { followItem.setIcon(R.drawable.ic_follow_white);
        followItem.setTitle("Follow");}
    }

    Menu profileMenu;
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        final MenuItem chageItem=item;
        switch (item.getItemId()) {

            case R.id.action_follow_user:

                if(userData.followType==1)
                {
                    updateUserFollowStatus(userData.user_id,0);
                    userData.followType=0;

                    updateFollowDetails();
                }
                else {
                    newUserFollowRequestWithDoubleCheck(userId, new JobCompletionListner() {
                        @Override
                        public void onJobCompletion(String txt1) {
                            userData.followType=Integer.parseInt(txt1);
                            updateFollowDetails();

                        }
                    });
                }

                return true;
            case R.id.action_poke_user:
                newUserPoke(userId);
                return true;
            case R.id.action_view_contact_addressbook:
                if(userData.type==2)
                {
                    // menu.findItem(R.id.action_add_contact).setTitle("View in Phonebook");
                    ChatWindow.chatwindowInstance.viewSingleContactOnPhoneBook(userData.number);
                }
                else
                {
                    ChatWindow.chatwindowInstance.insertRawContact();
                }
                return true;
            case R.id.action_chat_block_user:
                if(userData.followType==11)
                {
                    newUserUnBlock(userData.user_id);
                }
                else
                {
                    newUserBlock(userData.user_id);
                }
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static void newUserPoke(String uID)
    {

        NotificationData x=new NotificationData();

        x.userid=userdetail.getUid();
        x.type=1;
        x.timestamp= System.currentTimeMillis();
        x.text=TabbedActivity.UserName +" poked you!";

        DatabaseReference chatRef = FirebaseDatabase.getInstance().getReference().child("notifi_data").child(uID).push();
        chatRef.setValue(x).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid)
            {
               // Toast.makeText(Profile_Activity.this, "You have poked", Toast.LENGTH_SHORT).show();

            }
        });
    }
    public static void customNotificationSender(String receiver_uID,int type2,String text2,final JobCompletionListner listner) {
        NotificationData x=new NotificationData();

        x.userid=userdetail.getUid();
        x.type=type2;
        x.timestamp= System.currentTimeMillis();
        x.text=text2;

        DatabaseReference chatRef = FirebaseDatabase.getInstance().getReference().child("notifi_data").child(receiver_uID).push();
        chatRef.setValue(x).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid)
            {
                // Toast.makeText(Profile_Activity.this, "You have poked", Toast.LENGTH_SHORT).show();
                if(listner!=null)
                listner.onJobCompletion("null");
            }
        });
    }

    MediaPlayer mediaPlayer;
    AppCompatSeekBar bfmusicSeekbar;
    ImageButton playPauseButton;
    int mediaFileLengthInMilliseconds;
    Timer mediaPlayerTimer;
    TimerTask mediaPlayertask;



    public void setUpBgMusicPlayback(boolean show)
    {
        if(show) {
            playPauseButton = (ImageButton) findViewById(R.id.playPauseButton);
            bfmusicSeekbar = (AppCompatSeekBar) findViewById(R.id.bfmusicSeekbar);
            mediaFileLengthInMilliseconds = 0;
        }
        else
        {
            findViewById(R.id.bgmusicSection).setVisibility(GONE);
        }
    }


    void resetMediaPlayer()
    {
        if(mediaPlayer!=null)
        {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer=null;
        }
        if(mediaPlayerTimer!=null)
        {
            mediaPlayerTimer.cancel();
            mediaPlayerTimer.purge();
        }
        playPauseButton.setImageResource(R.drawable.ic_play_arrow_accent);
    }

    void setUpMediaPlayer()
    {
        if(mediaPlayer==null)
        {
        mediaPlayer=new MediaPlayer();
        if(userData.music_url.equals("null"))
            mediaPlayer = MediaPlayer.create(this, R.raw.music_file);
        else
            mediaPlayer = MediaPlayer.create(this, Uri.parse(userData.music_url));

        //mediaPlayer.prepare();
        mediaFileLengthInMilliseconds=mediaPlayer.getDuration();

        mediaPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
            @Override
            public void onBufferingUpdate(MediaPlayer mp, int percent) {
                bfmusicSeekbar.setSecondaryProgress(percent);
            }
        });

        mediaPlayertask = new TimerTask() {
            @Override
            public void run() {

                Log.e("MGG","changing  now");
                if (mediaPlayer.isPlaying()) {
                    float pos=((float)mediaPlayer.getCurrentPosition()/mediaFileLengthInMilliseconds)*100;
                    bfmusicSeekbar.setProgress((int)pos);
                    Log.e("MGG","installing to "+mediaPlayer.getCurrentPosition()+ " scroll is "+bfmusicSeekbar.getProgress());
                }

            }
        };
        mediaPlayerTimer = new Timer();

        mediaPlayerTimer.schedule(mediaPlayertask, 0, 400);


        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                playPauseButton.setImageResource(R.drawable.ic_play_arrow_accent);
                bfmusicSeekbar.setProgress(100);
            }
        });

        /*bfmusicSeekbar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
              int playPositionInMillisecconds = (mediaFileLengthInMilliseconds / 100) * bfmusicSeekbar.getProgress();
                // mediaPlayer.pause();

                //if(!mediaPlayer.isPlaying())
                pauseMediaPlayer();
                    mediaPlayer.seekTo(playPositionInMillisecconds);
                    Log.e("MGG","seeked to "+playPositionInMillisecconds+ " scroll is "+bfmusicSeekbar.getProgress());
                return false;
            }
        });*/





        bfmusicSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                //int playPositionInMillisecconds = (mediaFileLengthInMilliseconds / 100) * bfmusicSeekbar.getProgress();
                // mediaPlayer.pause();
                // if(!mediaPlayer.isPlaying())
                // mediaPlayer.seekTo(playPositionInMillisecconds);
                // mediaPlayer.start();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {


            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int playPositionInMillisecconds = (mediaFileLengthInMilliseconds / 100) * bfmusicSeekbar.getProgress();
                //pauseMediaPlayer();
                mediaPlayer.seekTo(playPositionInMillisecconds);
                Log.e("MGG","seeked to "+playPositionInMillisecconds+ " scroll is "+bfmusicSeekbar.getProgress());
            }
        });

    }
        if(mediaPlayer.isPlaying())
    {
        pauseMediaPlayer();}
        else{
        startMediaPlayer();
    }
    }
    public void playLocalAudio(View c) throws IOException {


            if(userData.music_url.contains("https"))
            {
                downloadFileAndReturnUriString(userData.music_url, new JobCompletionListner() {
                    @Override
                    public void onJobCompletion(String txt1) {
                        userData.music_url=txt1;
                        saveContactMusicToDatabase(userData.user_id,null,txt1);
                        setUpMediaPlayer();
                    }
                });
            }
            else
            {
                setUpMediaPlayer();
            }

    }

    void pauseMediaPlayer()
    {
        playPauseButton.setImageResource(R.drawable.ic_play_arrow_accent);
        mediaPlayer.pause();
        //mediaPlayerTimer.cancel();
    }
    void startMediaPlayer()
    {
        playPauseButton.setImageResource(R.drawable.ic_pause_accent);
        mediaPlayer.start();

    }

    private void primarySeekBarProgressUpdater() {
        bfmusicSeekbar.setProgress((int)(((float)mediaPlayer.getCurrentPosition()/mediaFileLengthInMilliseconds)*100)); // This math construction give a percentage of "was playing"/"song length"
        if (mediaPlayer.isPlaying()) {
            Runnable notification = new Runnable() {
                public void run()
                {
                    primarySeekBarProgressUpdater();
                }
            };
           // handler.postDelayed(notification,1000);
        }
    }


    protected void onResume() {
        super.onResume();
        profile_settings.getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(profile_settings);
    }

    protected void onPause() {
        super.onPause();
        profile_settings.getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(profile_settings);
        resetMediaPlayer();
    }

    static void fecthUserPrivacyStatus(String user, final UserPrivacyStatusListner lstn) {
        FirebaseDatabase.getInstance().getReference().child("user_profile").child(user).child("user_privacy")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        UserPrivacyData post=new UserPrivacyData();

                        post.profile_picture=Integer.parseInt(dataSnapshot.child("profile_picture").getValue().toString());
                        post.profile_status=Integer.parseInt(dataSnapshot.child("profile_status").getValue().toString());
                        post.number=Integer.parseInt(dataSnapshot.child("number").getValue().toString());
                        post.activity_status=Integer.parseInt(dataSnapshot.child("activity_status").getValue().toString());
                        post.location=Integer.parseInt(dataSnapshot.child("location").getValue().toString());
                        post.anyonefollow=Integer.parseInt(dataSnapshot.child("anyonefollow").getValue().toString());
                        post.anyonemessage=Integer.parseInt(dataSnapshot.child("anyonemessage").getValue().toString());


                        //xxx
                           if(lstn!=null)
                               lstn.onUserStatusReceieved(post);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }




    public static void newUserBlock(final String userID)
    {

        customNotificationSender(userID, 21, "null", new JobCompletionListner() {
            @Override
            public void onJobCompletion(String txt1) {
                FirebaseDatabase.getInstance().getReference().child("user_blocking").child(userId).
                        child(userID).child(userdetail.getUid()).setValue("1");

                SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();
                ContentValues values = new ContentValues();

                values.put(DatabaseAccesser.user_user_follow_type, 11);


                long newRowId = db.update(DatabaseAccesser.user_table, values, DatabaseAccesser.user_user_id + " =? ", new String[]{userID});

                /*if (newRowId == 0) {
                    fetchAndSaveNewUser(userID,null,3);
                }*/
            }
        });
    }

    public static void newUserUnBlock(final String userID)
    {

        customNotificationSender(userID, 22, "null", new JobCompletionListner() {
            @Override
            public void onJobCompletion(String txt1) {

                FirebaseDatabase.getInstance().getReference().child("user_blocking").child(userId).
                        child(userID).child(userdetail.getUid()).removeValue();

                SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();
                ContentValues values = new ContentValues();

                values.put(DatabaseAccesser.user_user_follow_type, 0);


                long newRowId = db.update(DatabaseAccesser.user_table, values, DatabaseAccesser.user_user_id + " =? ", new String[]{userID});

                /*if (newRowId == 0) {
                    fetchAndSaveNewUser(userID,null,3);
                }*/
            }
        });




    }


}
