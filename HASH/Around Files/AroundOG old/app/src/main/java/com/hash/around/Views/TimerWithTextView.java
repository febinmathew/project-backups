package com.hash.around.Views;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION_CODES;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hash.around.OtherClasses.Interfaces.JobCompletionWithFailureListner;
import com.hash.around.OtherClasses.Interfaces.TimerListner;
import com.hash.around.R;
import com.hash.around.Utils.StringUtils;


public class TimerWithTextView extends LinearLayout implements Runnable {
    ImageView menu_badge_icon;
    TextView expiration_badge;

  public TimerWithTextView(Context context) {
    this(context, null);
  }


  public TimerWithTextView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }
    Drawable imageDrawable;

  @TargetApi(VERSION_CODES.HONEYCOMB) @SuppressWarnings("unused")
  public TimerWithTextView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
      inflate(context, R.layout.timer_withtext_view, this);

      this.menu_badge_icon = findViewById(R.id.menu_badge_icon);
      this.expiration_badge = findViewById(R.id.expiration_badge);


    if (attrs != null) {


      TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.TimerWithTextView, defStyleAttr, 0);
        imageDrawable = typedArray.getDrawable(R.styleable.TimerWithTextView_customImage);
        if(imageDrawable!=null)
            this.menu_badge_icon.setImageDrawable(imageDrawable);
        boolean hideText = typedArray.getBoolean(R.styleable.TimerWithTextView_hideText,false);
        if(hideText)expiration_badge.setVisibility(GONE);
        int textColor = typedArray.getColor(R.styleable.TimerWithTextView_textColor,expiration_badge.getCurrentTextColor());
        expiration_badge.setTextColor(textColor);

      boolean textBold=typedArray.getBoolean(R.styleable.TimerWithTextView_textBold, true);
      if(!textBold)
        expiration_badge.setTypeface(null, Typeface.NORMAL);


      float textSize=typedArray.getDimensionPixelSize(R.styleable.TimerWithTextView_textSize, 0);
      if(textSize!=0)
      expiration_badge.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);

      int imageTint = typedArray.getResourceId(R.styleable.TimerWithTextView_customImageTint,255);
        if(imageTint!=255)
           menu_badge_icon.setColorFilter(ContextCompat.getColor(getContext(), imageTint), android.graphics.PorterDuff.Mode.SRC_IN);
      typedArray.recycle();
    }

  }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        initViews();
    }
    public ImageView getImageView()
    {
        return menu_badge_icon;
    }
    public void setImageResource(int resID)
    {
        menu_badge_icon.setImageResource(resID);
    }
    public void setImageTint(int color)
    {
        menu_badge_icon.getDrawable().setColorFilter(ContextCompat.getColor(getContext(), color), android.graphics.PorterDuff.Mode.SRC_IN);
    }
    public TextView getTextView()
    {
        return expiration_badge;
    }
    public void setText(String text)
    {
        expiration_badge.setText(text);
    }
    public void setTextColor(int color)
    {
        this.expiration_badge.setTextColor(color);
    }
    public void setTextVisibility(int visibility)
    {
        this.expiration_badge.setVisibility(visibility);
    }

  public void initViews()
  {


  }
  long startedAt; long expiresIn;
boolean timerStop=false;
  public void setExpirationTime(Long startedAt, long expiresIn) {
    //this.startedAt = startedAt;
    this.expiresIn = expiresIn;
    expiration_badge.setText(StringUtils.getShortExpirationDisplayValueForTimestamp(expiresIn));
    timerStop=false;
startAnimation();
    //calculateProgress(this.startedAt, this.expiresIn);
  }



  private float calculateProgress(long startedAt, long expiresIn) {
    long  progressed      = System.currentTimeMillis() - startedAt;
    float percentComplete = (float)progressed / (float)expiresIn;

    return percentComplete * 100;
  }

  public static Handler handler = new Handler(Looper.getMainLooper());
  public void startAnimation() {
    synchronized (this) {
     /* visible = true;
      if (!stopped) return;
      else          stopped = false;*/
    }

    handler.postDelayed(this, 0);
  }

  TimerListner listener=null;
  public void setTimerListener(TimerListner list)
  {
    this.listener=list;
  }
  @Override
  public void run() {
    if(expiresIn<=System.currentTimeMillis())
    {timerStop=true;
    if(listener!=null)
      listener.onTimerEnd(expiresIn,null);
    }
    synchronized (this) {
      if (timerStop) {
        return;
      }
    }

    expiration_badge.setText(StringUtils.getShortExpirationDisplayValueForTimestamp(expiresIn));
    //timerView.setExpirationTime(timerView.startedAt, timerView.expiresIn);

   /* synchronized (timerView) {
      if (!timerView.visible) {
        timerView.stopped = true;
        return;
      }
    }*/


    handler.postDelayed(this, 1000);
  }
  public void stopAnimation() {
    synchronized (this) {
      timerStop = true;
    }
  }
}
