package com.hash.around;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.widget.Toast;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.hash.around.OtherClasses.DataTypes.ChatData;
import com.hash.around.OtherClasses.DataTypes.ContactListData;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.OtherClasses.DataTypes.FeedData;
import com.hash.around.OtherClasses.DataTypes.GroupChatData;
import com.hash.around.OtherClasses.DataTypes.GroupMemberData;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.OtherClasses.DataTypes.NotificationData;
import com.hash.around.OtherClasses.Interfaces.JobCompletionWithFailureListner;
import com.hash.around.OtherClasses.Interfaces.ProcessCompletedListner;
import com.hash.around.OtherClasses.DataTypes.UserPrivacyData;
import com.hash.around.OtherClasses.Interfaces.UserPrivacyStatusListner;
import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.hash.around.Utils.ChatSender;
import com.hash.around.Utils.DatabaseManager;
import com.hash.around.Utils.LocalNotificationManager;
import com.hash.around.Utils.StringUtils;
import com.hash.around.Utils.UserManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;


import static com.hash.around.Profile_Activity.customNotificationSender;
import static com.hash.around.Profile_Activity.fecthUserPrivacyStatus;
import static com.hash.around.TabbedActivity.AROUND_FEED_RADIUS;
import static com.hash.around.TabbedActivity.checkForMediaAvailablility;
import static com.hash.around.TabbedActivity.fDatabase;


public class MainServiceThread extends Service{
    ChildEventListener childEventListener,chatReceiveEventListener,childEventListenerForNotification;
    DatabaseReference newPostRef,chatReceiveRef,notificationReceiveRef,groupsRef;
    static String x=null;
    GeoFire geoFire;
    Double lat=0.0;
    Double lng=0.0;
    //TextView locationText;

    GeoQuery geoQuery;
    ValueEventListener postListener;
    //DatabaseAccesser dbAccess;
    private BroadcastReceiver getFeedReciever,phoneContactsUpdateReceiver;

    SQLiteDatabase writableDB,readableDB;

    static FirebaseUser userdetail;

    public static MainServiceThread mainService;
    @Override
    public void onCreate() {
        super.onCreate();


        final IntentFilter theFilter = new IntentFilter("GET_FEED_UPDATED");
        getFeedReciever = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                Double x;
                Double y;
                try {
                    x = Double.parseDouble(intent.getStringExtra("lat"));
                    y = Double.parseDouble(intent.getStringExtra("lng"));
                }
                catch(Exception e)
                {
                    // Toast.makeText(context, "noo error", Toast.LENGTH_SHORT).show();
                    return;
                }
                //Log.e("MSG","===lat: "+x+" long :"+y);
                //Toast.makeText(context, Double.doubleToLongBits(lat)+"/"+x+" "+lng+"/"+y, Toast.LENGTH_LONG).show();
                if(Double.doubleToLongBits(lat) == Double.doubleToLongBits(x) && Double.doubleToLongBits(lng) ==Double.doubleToLongBits(y))
                {
                    //Log.e("MSG","updating the feed now00");

                }
                else
                {
                    if(lat == 0&& lng==0)
                    {
                        // Log.e("MSG","updating the feed now01");
                    }
                    else
                    {
                        geoQuery.removeAllListeners();


                        // Log.e("MSG","updating the feed now02");


                    }
                    lat=x;
                    lng=y;
                    update_feed(lat,lng);
                    // Log.e("MSG","updating the feed now03");

                }
            }
        };
        // Registers the receiver so that your service will listen for
        // broadcasts

        registerReceiver(getFeedReciever, theFilter);

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("feeds_location");
        geoFire = new GeoFire(ref);

        x=null;



        final IntentFilter contactFilter = new IntentFilter("GET_CONTACTS_UPDATED");
        phoneContactsUpdateReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

               // Log.e("MSGG","=broad cast recieved");
                final String[] PROJECTION =
                        {
                                Contacts._ID,
                                Contacts.LOOKUP_KEY,
                                Build.VERSION.SDK_INT
                                        >= Build.VERSION_CODES.HONEYCOMB ?
                                        Contacts.DISPLAY_NAME_PRIMARY :
                                        Contacts.DISPLAY_NAME,
                                Contacts.HAS_PHONE_NUMBER,
                                Contacts._ID

                        };

                final String SELECTION = ContactsContract.CommonDataKinds.Phone.HAS_PHONE_NUMBER+ " >?";
                final String[] mSelectionArgs = { "0" };

                String prevNumber="9999999999";
                String thisUserNumber=userdetail.getPhoneNumber();
                int count=0;
                ArrayList<ContactListData> phoneContacts=new ArrayList<>();

                Cursor cur=getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null,SELECTION,mSelectionArgs,null);
                while(cur.moveToNext())
                {

                    String contactNumber = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    String contactName = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));


                    if(!(PhoneNumberUtils.compare(contactNumber, prevNumber)||PhoneNumberUtils.compare(contactNumber, thisUserNumber)))
                    {
                        prevNumber=contactNumber;
                        // Log.e("MSGG","name "+contactName+" numner : "+contactNumber+" status : "+!PhoneNumberUtils.compare(contactNumber, contactNumber));

                        count++;
                        ContactListData data=new ContactListData();
                        data.user_name=contactName;
                        data.number=phoneNumberValidation(contactNumber);
                        phoneContacts.add(data);
                    }
                    else
                    {
                        // Log.e("MSGG","==REMOVED "+contactName+" numner : "+contactNumber+" status : "+!PhoneNumberUtils.compare(contactNumber, contactNumber));

                    }

                }
                //Log.e("MSGG","size : "+phoneContacts.size()+" count : "+cur.getCount());


                nativeUpdateContacts(phoneContacts,false);

                syncPhoneContactsWithAppContacts(phoneContacts);

            }
        };
        registerReceiver(phoneContactsUpdateReceiver, contactFilter);
        /*******broadcast for conatct update*******/


        //ContactsContentObserver contactObserver=new ContactsContentObserver();
        //getContentResolver().registerContentObserver(Contacts.CONTENT_URI,true,contactObserver);



    }
    String phoneNumberValidation(String num)
    {
        return num.replaceAll("[()\\s-]","");
    }

    /*private class ContactsContentObserver extends ContentObserver {
        public ContactsContentObserver() {
            super(null);
        }

        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            Log.e("MSGG", "voooo changed");


        }
    }*/

    void syncPhoneContactsWithAppContacts(ArrayList<ContactListData> data)
    {
        // ArrayList<String> data1=new ArrayList<>();
        ArrayList<String> data2=new ArrayList<>();
        for(int i=0;i<data.size();i++) {
            data2.add(data.get(i).number);
        }

        ArrayList<String> appContacts1=new ArrayList<String>();
        ArrayList<String> appContacts2=new ArrayList<String>();

        /*data1.add("8138908161");
        data1.add("8606543912");
        data1.add("9731592516");
        data1.add("09443085174");


        for(int i=0;i<data1.size();i++) {
            Log.e("MSGG","num : "+data1.get(i)+" Formated: "+ PhoneNumberUtils.formatNumber(data1.get(i),"IN"));
        }*/


        SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();


        Cursor cursor = db.query(DatabaseAccesser.user_table,
                new String[]{

                        DatabaseAccesser.user_user_id,
                        DatabaseAccesser.user_name,
                        DatabaseAccesser.user_number,
                        DatabaseAccesser.user_localUrl,
                        DatabaseAccesser.user_globalUrl},
                DatabaseAccesser.user_user_type + " =?",
                new String[]{"2"},
                null, null, null,null);

        while (cursor.moveToNext()) {

            ContactListData tempContact=new ContactListData();
            tempContact.number=cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_number));
            tempContact.user_id=cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_user_id));

            appContacts1.add(tempContact.user_id);
            appContacts2.add(tempContact.number);

        }
        cursor.close();

        for(int i=0;i<appContacts2.size();i++)
        {
            if(!data2.contains(appContacts2.get(i)))
            {
                //Log.e("MSGG","removed"+ appContacts2.get(i));
                FirebaseDatabase.getInstance().getReference().child("user_profile").child(userdetail.getUid()).child("contacts").child(appContacts1.get(i)).removeValue();
                removeContactBasedOnPhoneNumber(appContacts2.get(i));
            }
        }

        for(int i=0;i<data2.size();i++)
        {
            if(!appContacts2.contains(data2.get(i)))
            {
                //Log.e("MSGG","add "+ appContacts.get(i).number);
                // CODDE TO FETCH NEW CONTACTS THAT ARE NOT INSIDE THE APP
            }

        }
    }

    void removeContactBasedOnPhoneNumber(String phoneNumber)
    {
        SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();
        db.delete(DatabaseAccesser.user_table, DatabaseAccesser.user_number + "=?",
                new String[]{phoneNumber});
    }

    void nativeUpdateContacts(final ArrayList<ContactListData> phonecontacts,boolean basedOnID)
    {

        for(int i=0;i<phonecontacts.size();i++)
        {
            final int k=i;
            fetchUserDetailsbyIDOrPhone(phonecontacts.get(i).number,basedOnID, new ProcessCompletedListner() {
                @Override
                public void processCompleteAction() {}

                @Override
                public void newUserAdded(UserWholeData data) {
                   // Log.e("MSGG","=new user fetched " + data.number);
                    data.user_name=phonecontacts.get(k).user_name;
                    updateOrInsertUserIfNone(data, new JobCompletionListner() {
                        @Override
                        public void onJobCompletion(String txt1) {

                            //if(AllUsersList.)
                            //Log.e("MSG","=new user added");
                        }
                    });


                }
            });
        }
    }

    public static void updateOrInsertUserIfNone(final UserWholeData contact, final JobCompletionListner listn)
    {

        SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();

        Cursor cursor = db.query(DatabaseAccesser.user_table,
                new String[]{
                        DatabaseAccesser.user_user_id,
                        DatabaseAccesser.user_name,
                        DatabaseAccesser.user_number,
                        DatabaseAccesser.user_localUrl,
                        DatabaseAccesser.user_globalUrl,
                        DatabaseAccesser.user_music_globalUrl},
                DatabaseAccesser.user_user_id + " =?",
                new String[]{contact.user_id},
                null, null, null,null);

        if (cursor.getCount() == 1)
        {

            cursor.moveToNext();
            //Log.e("MSGG","=update "+cursor.getString(cursor.getColumnIndexOrThrow(DatabaseAccesser.user_number)));
            if(!contact.img_url.equals(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseAccesser.user_globalUrl))))
            {

                //Log.e("MSGG","=url not same");
                downloadFileAndReturnUriString(contact.img_url, new JobCompletionListner() {
                    @Override
                    public void onJobCompletion(String txt1) {

                        String globalUrl= contact.img_url;
                        contact.img_url = txt1;
                        TabbedActivity.saveContactToDatabase(contact,globalUrl,false);
                        if(listn!=null)
                            listn.onJobCompletion("true");

                    }
                });
            }
            else
            {
                TabbedActivity.updateContactStatusAndPrivacy(contact);
                if(listn!=null)
                    listn.onJobCompletion("true");
                Log.e("MSGG","=url IS same\n");//+contact.img_url+"\n"+ cursor.getString(cursor.getColumnIndexOrThrow(DatabaseAccesser.user_globalUrl)));
            }

            //Log.e("MGG","media is "+ contact.music_url );
            //Log.e("MGG","media is null for "+ contact.user_name);
            //Log.e("MGG","media is "+ cursor.getString(cursor.getColumnIndexOrThrow(DatabaseAccesser.user_music_globalUrl)) );

            if(!contact.music_url.equals(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseAccesser.user_music_globalUrl))))
            {
                TabbedActivity.saveContactMusicToDatabase(contact.user_id,contact.music_url,null);
            }


        }
        else
        {
            //Log.e("MSGG","=insert");
            downloadFileAndReturnUriString(contact.img_url, new JobCompletionListner() {
                @Override
                public void onJobCompletion(String txt1) {


                    FirebaseDatabase.getInstance().getReference().child("user_profile").child(userdetail.getUid()).child("contacts").child(contact.user_id).setValue("1");

                    String globalUrl= contact.img_url;
                    contact.img_url = txt1;
                    TabbedActivity.saveContactToDatabase(contact,globalUrl,true);
                    TabbedActivity.saveContactMusicToDatabase(contact.user_id,contact.music_url,null);  // CODE 10 can merge this fn with "saveContactToDatabase". i did this to save time
                    if(listn!=null)
                        listn.onJobCompletion("true");
                }
            });

        }
    }


    static void fetchRemoteUserStatusOfThisUser(String tragetID,final JobCompletionListner listner)
    {
        FirebaseDatabase.getInstance().getReference().child("user_profile").child(tragetID).child("contacts").
                child(userdetail.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if(dataSnapshot.getValue()==null)
                    listner.onJobCompletion("0");
                else
                    listner.onJobCompletion("1");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }



//simply fetches a user detail

    public static void fetchUserDetailsbyIDOrPhone(String userData,boolean byUserName, final  ProcessCompletedListner listn)
    {
        if(!byUserName)
        {
            FirebaseDatabase.getInstance().getReference().child("user_profile").orderByChild("number").equalTo(userData).limitToFirst(1)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {

                                ContactListData dat=postSnapshot.getValue(ContactListData.class);
                                dat.music_url=postSnapshot.child("music_url").getValue().toString();

                                dat.type=2;

                                UserPrivacyData privacy =getPrivacyFromuserSnapshot(postSnapshot);
                                final UserWholeData data=new UserWholeData(dat,privacy);
                                fetchRemoteUserStatusOfThisUser(data.user_id, new JobCompletionListner() {
                                    @Override
                                    public void onJobCompletion(String txt1) {
                                        data.remoteContact=Integer.parseInt(txt1);
                                        listn.newUserAdded(data);
                                    }
                                });


                            }

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                            listn.processCompleteAction();

                        }
                    });
        }
        else
        {
            FirebaseDatabase.getInstance().getReference().child("user_profile").child(userData)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            ContactListData dat=dataSnapshot.getValue(ContactListData.class);
                            UserPrivacyData privacy =getPrivacyFromuserSnapshot(dataSnapshot);
                            UserWholeData data=new UserWholeData(dat,privacy);
                            listn.newUserAdded(data);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                            listn.processCompleteAction();

                        }
                    });
        }
    }

    public static UserPrivacyData getPrivacyFromuserSnapshot(DataSnapshot dataSnapshot)
    {
        DataSnapshot temp=dataSnapshot.child("user_privacy");

        final UserPrivacyData postPrivacy=new UserPrivacyData();
        postPrivacy.activity_status=Integer.parseInt(temp.child("activity_status").getValue().toString());
        postPrivacy.profile_picture=Integer.parseInt(temp.child("profile_picture").getValue().toString());
        postPrivacy.profile_status=Integer.parseInt(temp.child("profile_status").getValue().toString());
        postPrivacy.number=Integer.parseInt(temp.child("number").getValue().toString());
        postPrivacy.location=Integer.parseInt(temp.child("location").getValue().toString());
        postPrivacy.anyonefollow=Integer.parseInt(temp.child("anyonefollow").getValue().toString());
        postPrivacy.anyonemessage=Integer.parseInt(temp.child("anyonemessage").getValue().toString());

        return postPrivacy;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if(x==null)
        {
            writableDB = new DatabaseAccesser(getApplicationContext()).getWritableDatabase();
            readableDB=new DatabaseAccesser(getApplicationContext()).getReadableDatabase();

            mainService=this;


            userdetail= FirebaseAuth.getInstance().getCurrentUser();
            if(userdetail==null)
            {
                x=null;
                Toast.makeText(this, "No creta", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(this, "creeeeeta", Toast.LENGTH_SHORT).show();
                x=userdetail.getUid();
                newPostRef = FirebaseDatabase.getInstance().getReference().child("chat_data").child(x);
                chatReceiveRef = FirebaseDatabase.getInstance().getReference().child("chat_status").child(x);

                notificationReceiveRef = FirebaseDatabase.getInstance().getReference().child("notifi_data").child(x);

                groupsRef = FirebaseDatabase.getInstance().getReference().child("chat_group");
                update_content();
                listenToGroupChats();
                update_chat_status();
                update_notifications();

            }

            /*SQLiteDatabase db = new DatabaseAccesser(getApplicationContext()).getReadableDatabase();

            Cursor cursor = db.query(DatabaseAccesser.user_table,
                    new String[]{

                            DatabaseAccesser.user_user_id,DatabaseAccesser.user_userme},
                    DatabaseAccesser.user_userme + " =?",
                    new String[]{"1"},
                    null, null, null,null);

            while (cursor.moveToNext()) {

                x = cursor.getString(
                        cursor.getColumnIndexOrThrow(DatabaseAccesser.user_user_id));

            }
            cursor.close();*/
            if(x!=null) {

            }

        }
        return super.onStartCommand(intent, flags, startId);


    }


    void update_notifications()
    {


        //final Context p=getActivity();
        // if(childEventListener==null)


        if(childEventListenerForNotification==null) {

            childEventListenerForNotification = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                    final NotificationData post = dataSnapshot.getValue(NotificationData.class);
                    //post.sender = 1;
                    //finaldata.add(post);
                    switch(post.type)
                    {
                        case 0:
                            saveNotificationToDatabase(post);

                            break;
                        case 1:
                            saveNotificationToDatabase(post);
                            break;
                        case 2:

                            StorageReference httpsReference = FirebaseStorage.getInstance().getReferenceFromUrl(post.image);
                            File localFile = null;
                            try {
                                localFile = File.createTempFile("images", "png");
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            httpsReference.getBytes(1024 * 1024).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                                @Override
                                public void onSuccess(byte[] bytes) {
                                    try {

                                        File file = new File(Environment.getExternalStorageDirectory().getPath(), "Around Files/Media");
                                        if (!file.exists()) {
                                            file.mkdirs();
                                        }
                                        String name = StringUtils.randomStringGenerator() + ".png";
                                        FileOutputStream fos = new FileOutputStream(new File(file.getAbsolutePath(), name));
                                        fos.write(bytes);
                                        fos.close();
                                        post.image = file.toURI().toString() + name;
                                        saveNotificationToDatabase(post);

                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    // Handle any errors
                                }
                            });

                            break;

                        case 3:
                            UserFollowApproved(post.userid);
                            saveNotificationToDatabase(post);

                            //follow accepted
                            break;
                        case 4:
                            UserChatApproved(post.userid);
                            saveNotificationToDatabase(post);
                            //chat accepted
                            break;
                        case 26:  //follow rejected


                            break;
                        case 27:  //chat rejected


                            break;

                        case 11:
                           // Log.e("MGG","new follow REQUEST");
                            //CODE UNKNOWN showin deffault notification if folly type is not private
                            saveNotificationToDatabase(post);
                            //follow request
                            break;
                        case 12:
                           // Log.e("MGG","new chat REQUEST");
                            saveNotificationToDatabase(post);
                            //chat request
                            break;
                        case 15:

                            addIncommingNewGroup(post.text);
                            break;

                        case 21: //block

                            addNewIncommingBlockAndUnblock(post.userid,true);

                            break;

                        case 22:  //unblock
                            addNewIncommingBlockAndUnblock(post.userid,false);

                            break;


                    }
                    dataSnapshot.getRef().removeValue();


                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            };


//                      Toast.makeText(getActivity(), "hurray", Toast.LENGTH_SHORT).show();
            // ...
            //newPostRef.goOffline();
            //newPostRef.removeEventListener(childEventListener);

            // Toast.makeText(this, "creeeeeta", Toast.LENGTH_SHORT).show();
            notificationReceiveRef.addChildEventListener(childEventListenerForNotification);

        }

    }


    static void fecthSingleUserPrivacyStatus(String user,final UserPrivacyStatusListner lstn) {
        FirebaseDatabase.getInstance().getReference().child("user_profile").child(user).child("user_privacy")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        UserPrivacyData post=new UserPrivacyData();

                        post.profile_picture=Integer.parseInt(dataSnapshot.child("profile_picture").getValue().toString());
                        post.profile_status=Integer.parseInt(dataSnapshot.child("profile_status").getValue().toString());
                        post.number=Integer.parseInt(dataSnapshot.child("number").getValue().toString());
                        post.activity_status=Integer.parseInt(dataSnapshot.child("activity_status").getValue().toString());
                        post.location=Integer.parseInt(dataSnapshot.child("location").getValue().toString());
                        post.anyonefollow=Integer.parseInt(dataSnapshot.child("anyonefollow").getValue().toString());
                        post.anyonemessage=Integer.parseInt(dataSnapshot.child("anyonemessage").getValue().toString());



                        if(lstn!=null)
                            lstn.onUserStatusReceieved(post);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public static void newUserFollowRequestWithDoubleCheck(final String uID, final JobCompletionListner listner) {

        fecthUserPrivacyStatus(uID, new UserPrivacyStatusListner() {
            @Override
            public void onUserStatusReceieved(UserPrivacyData privacy) {
                int aa = privacy.anyonefollow;
                if (aa == 2) {
                    customNotificationSender(uID, 11, "", new JobCompletionListner() {
                        @Override
                        public void onJobCompletion(String txt1) {
                            updateUserFollowStatus(uID,3);
                            if (listner != null)
                                listner.onJobCompletion("3");
                        }
                    });
                } else {
                    UserFollowApproved(uID);

                    if (listner != null)
                        listner.onJobCompletion("1");
                }
            }
        });
    }


    static void UserFollowApproved(String userID)
    {
        updateUserFollowStatus(userID,1);
    }

    public  static void updateUserFollowStatus(final String uID,int status)
    {
        final SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();
        final ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.user_user_follow_type, status);


        long newRowId = db.update(DatabaseAccesser.user_table, values, DatabaseAccesser.user_user_id + " =? ", new String[]{uID});

        if (newRowId == 0) {
            lightningEasyUserUpdateListner(uID, null, new JobCompletionListner() {
                @Override
                public void onJobCompletion(String txt1) {
                    long newRowId= db.update(DatabaseAccesser.user_table, values, DatabaseAccesser.user_user_id + " =? ", new String[]{uID});
                }
            });
        }
    }

    public static void newUserChatRequestWithDoubleCheck(final String uID, final JobCompletionListner listner) {

        fecthUserPrivacyStatus(uID, new UserPrivacyStatusListner() {
            @Override
            public void onUserStatusReceieved(UserPrivacyData privacy) {
                int aa = privacy.anyonemessage;
                if (aa == 2) {
                    customNotificationSender(uID, 11, "", new JobCompletionListner() {
                        @Override
                        public void onJobCompletion(String txt1) {
                            updateUserChatStatus(uID,3);
                            if (listner != null)
                                listner.onJobCompletion("Request sent");
                        }
                    });
                } else {

                    UserChatApproved(uID);
                    if (listner != null)
                        listner.onJobCompletion("Chat Now");
                }
            }
        });
    }


    static  void UserChatApproved(String userID)
    {
        updateUserChatStatus(userID,1);
    }

    public static void updateUserChatStatus(final String userID,int type)
    {
        final SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();
        final ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.user_user_chat_type, type);

        long newRowId = db.update(DatabaseAccesser.user_table, values, DatabaseAccesser.user_user_id + " =? ", new String[]{userID});

        if (newRowId == 0) {
            lightningEasyUserUpdateListner(userID, null, new JobCompletionListner() {
                @Override
                public void onJobCompletion(String txt1) {
                    long newRowId = db.update(DatabaseAccesser.user_table, values, DatabaseAccesser.user_user_id + " =? ", new String[]{userID});
                }
            });
        }
    }

    void addNewIncommingBlockAndUnblock(final String userID, boolean block)
    {
        final SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();

        final ContentValues values = new ContentValues();

        /*if(block)
            values.put(DatabaseAccesser.user_user_follow_type, 5);
        else
            values.put(DatabaseAccesser.user_user_follow_type, 1);*/

       /* lightningEasyUserUpdateListner(userID, block?12:0,new JobCompletionListner() {
            @Override
            public void onJobCompletion(String txt1) {
                long newRowId = db.update(DatabaseAccesser.user_table, values, DatabaseAccesser.user_user_id + " =? ", new String[]{userID});
            }
        });*/



    }

    public static void lightningEasyUserUpdateListner(String userID,final Integer follow_type,final JobCompletionListner allComplete)
    {
        fetchUserDetailsbyIDOrPhone(userID, true, new ProcessCompletedListner() {
            @Override
            public void processCompleteAction() {}

            @Override
            public void newUserAdded(UserWholeData data) {

                if(follow_type!=null)
                {
                    data.followType=follow_type;
                }
                updateOrInsertUserIfNone(data, new JobCompletionListner() {
                    @Override
                    public void onJobCompletion(String txt1) {
                        allComplete.onJobCompletion("done");
                    }
                });

            }
        });
    }

    void addIncommingNewGroup(final String groupID)
    {
        //Log.e("MSGG","inclimming group");
        FirebaseDatabase.getInstance().getReference().child("user_profile").child(groupID)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        //final ContactListData post= dataSnapshot.getValue(ContactListData.class);
                        final ContactListData contact2 = new ContactListData();
                        //Log.e("MSG",""+groupID);
                        contact2.img_url= dataSnapshot.child("img_url").getValue().toString();
                        contact2.type= Integer.parseInt(dataSnapshot.child("type").getValue().toString());
                        contact2.user_id= dataSnapshot.child("user_id").getValue().toString();
                        contact2.user_name= dataSnapshot.child("user_name").getValue().toString();
                        contact2.status=dataSnapshot.child("status").getValue().toString();

                        //Log.e("MSG",contact2.img_url);

                        downloadFileAndReturnUriString(contact2.img_url, new JobCompletionListner() {
                            @Override
                            public void onJobCompletion(String txt1) {



                                               /*StorageReference httpsReference = FirebaseStorage.getInstance().getReferenceFromUrl(contact2.img_url);
                                               httpsReference.getBytes(1024 * 1024).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                                                   @Override
                                                   public void onSuccess(byte[] bytes) {
                                                       File file = new File(Environment.getExternalStorageDirectory().getPath(), "Around Files/Profile Pictures");
                                                       if (!file.exists()) {
                                                           file.mkdirs();
                                                       }
                                                       //File tempFile = new File(file.getAbsolutePath(),"contact.vcf");

                                                       String name = contact2.user_name + ".png";
                                                       FileOutputStream fos = null;
                                                       try {
                                                           fos = new FileOutputStream(new File(file.getAbsolutePath(), name));
                                                           fos.write(bytes);
                                                           fos.close();
                                                       } catch (IOException e) {
                                                           e.printStackTrace();
                                                       }*/


                                //Toast.makeText(getActivity(),  , Toast.LENGTH_SHORT).show();

                                String globalUrl = contact2.img_url;
                                contact2.img_url = txt1;

                                //Log.e("MSGG","Now add to chatlist");
                                updateChatlistWithGroup(contact2,groupID);


                                // Log.e("MSGG","Now listining for members");
                                FirebaseDatabase.getInstance().getReference().
                                        child("chat_group").child(groupID).child("group_members").addChildEventListener(new ChildEventListener() {
                                    @Override
                                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                        GroupMemberData memberData= dataSnapshot.getValue(GroupMemberData.class);
                                        updateGroupMemberInDatabase(memberData,groupID);
                                        //Log.e("MSGG","new member member :" +memberData.userid);

                                        if(memberData.userid.equals(TabbedActivity.userdetail.getUid()))
                                        {
                                            listentoSingleGroupChats(groupID, 0);
                                        }
                                    }

                                    @Override
                                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                                    }

                                    @Override
                                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                                    }

                                    @Override
                                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });
                                // saveGroupToDatabase(contact2, globalUrl);
                                saveContactToDatabase(contact2, globalUrl);



                            }
                        });
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

        // updateChatlistWithGroup

    }


    void updateGroupMemberInDatabase(GroupMemberData data,String grp)
    {

        ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.groupsmembers_group_id,grp);
        values.put(DatabaseAccesser.groupsmembers_user_id, data.userid);
        values.put(DatabaseAccesser.groupsmembers_member_type, data.member_type);

        long  newRowId = writableDB.insert(DatabaseAccesser.groupsmembers_table, null, values);

    }


    void updateChatlistWithGroup(ContactListData contact,String groupID)
    {
        //SQLiteDatabase db = dbAccess.getWritableDatabase();
        ContentValues values2 = new ContentValues();
        values2.put(DatabaseAccesser.chatList_lastmsg, "You were added!");
        values2.put(DatabaseAccesser.chatList_lastmsg_type, 0);
        values2.put(DatabaseAccesser.chatList_lastmsgbyme, 0);
        values2.put(DatabaseAccesser.chatList_msgqueue, 1);
        values2.put(DatabaseAccesser.chatList_lastmsgRowId, 0);
        values2.put(DatabaseAccesser.chatList_last_msgseen, 0);
        values2.put(DatabaseAccesser.chatList_name, contact.user_name);
        values2.put(DatabaseAccesser.chatList_userid, groupID);
        values2.put(DatabaseAccesser.chatList_timestamp, System.currentTimeMillis());
        values2.put(DatabaseAccesser.chatList_url, contact.img_url);

        int rowln=writableDB.update(DatabaseAccesser.chatList_table,values2, DatabaseAccesser.chatList_userid+" =?",new String[]{groupID});
        long srowln=0;
        if(rowln==0) {
            // Log.e("MSGG","=== insert");
            srowln = writableDB.insert(DatabaseAccesser.chatList_table, null, values2);

        }

        //Log.e("MSGG",rowln+ " :@: "+srowln);

        Intent new_intent = new Intent();
        new_intent.setAction("CHAT_UPDATED");
        sendBroadcast(new_intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        newPostRef.removeEventListener(childEventListener);
        chatReceiveRef.removeEventListener(chatReceiveEventListener);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    void update_group_chat_status(final String grpID)
    {
        DatabaseReference databaseRef = groupsRef.child(grpID).child("chat_status");
        databaseRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                //Log.e("MSG","keeee : "+dataSnapshot.getKey());
                update_GroupChatSeenOnDatabase(dataSnapshot.getKey(),
                        dataSnapshot.child("last_rece_time").getValue().toString(),true,grpID);
                update_GroupChatSeenOnDatabase(dataSnapshot.getKey(),
                        dataSnapshot.child("last_seen_time").getValue().toString(),false,grpID);

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    void update_chat_status()
    {//xxx code 1
        if(chatReceiveEventListener==null) {
            chatReceiveEventListener=new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                    onChildChanged(dataSnapshot,s);
                    //Toast.makeText(MainServiceThread.this, "aaa added01", Toast.LENGTH_SHORT).show();
                    Log.e("GGG","child added ");

                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {


                    for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                        Log.e("GGG","lastFinalData child change ");
                        //Log.e("GGG","datasnapshot " +postSnapshot.child("last_msg").getValue());

                        int type=Integer.parseInt(postSnapshot.child("type").getValue().toString());
                        String timestamp=postSnapshot.child("timestamp").getValue().toString();
                        String lastsl=postSnapshot.child("last_msg").getValue().toString();

                        update_chatSeenOnDatabase(dataSnapshot.getKey(),lastsl,type,timestamp);

                        postSnapshot.getRef().removeValue();
                    }

                    //update_chatSeenOnDatabase(dataSnapshot.getKey(),dataSnapshot.child("last_rece_msg").getValue().toString(),2);
                    //update_chatSeenOnDatabase(dataSnapshot.getKey(),dataSnapshot.child("last_seen_msg").getValue().toString(),3);

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            };

            chatReceiveRef.addChildEventListener(chatReceiveEventListener);
        }

    }

    void update_GroupChatSeenOnDatabase(String userID,String timeValue,boolean isreceive,final String grp)
    {

        //SQLiteDatabase db= TabbedActivity.db.getWritableDatabase();
        ContentValues values = new ContentValues();
        int status=0;
        if(isreceive) {
            values.put(DatabaseAccesser.groupsmembers_last_rece_time, timeValue);

            status=2;
        }
        else {
            values.put(DatabaseAccesser.groupsmembers_last_seen_time, timeValue);
            status = 3;
        }



        int rowln=writableDB.update(DatabaseAccesser.groupsmembers_table,values,
                DatabaseAccesser.groupsmembers_group_id+" =? AND "+
                        DatabaseAccesser.groupsmembers_user_id+" =?",new String[]{grp,userID});


        ContentValues values2 = new ContentValues();

        long timeData=getLeastTimeAlluserSeenChatInGroup(grp,status);
        timeData++;

        if(status==2)
            Log.e("MSGG","least time : "+timeData);

        values2.put(DatabaseAccesser.chat_ifseen, status);

        int rowln2=writableDB.update(DatabaseAccesser.chat_table,values2,
                DatabaseAccesser.chat_groupid+" =? AND "+
                        DatabaseAccesser.chat_timestamp+" <=? AND "+
                        DatabaseAccesser.chat_ifseen+" <?",new String[]{grp,timeData+"",status+""});

        //Log.e("MSGG","total chat update /: " + rowln2+" status :"+ status);

        ChatSender.updateChatWindowHEAVY(grp);
        /*if(ChatWindow.uId!=null)
        {
            if(ChatWindow.uId.equals(grp))
            {

                ChatWindow.instance.getcontentfromdatabase();   //heavy code Do Somethings
                ChatWindow.adap.notifyDataSetChanged();      //


            }
        }*/

        /*ContentValues values2 = new ContentValues();
        values2.put(DatabaseAccesser.chatList_last_msgseen, status);
        int rowln2=writableDB.update(DatabaseAccesser.chatList_table,values2,
        DatabaseAccesser.chatList_lastmsgRowId+" <=? and "+DatabaseAccesser.chatList_userid+" =?",
        new String[]{lastRow,userID});*/


        Intent new_intent = new Intent();
        new_intent.setAction("CHAT_UPDATED");
        sendBroadcast(new_intent);

    }


    long getLeastTimeAlluserSeenChatInGroup(String gpID,int status)
    {

        Cursor cursor = readableDB.query(DatabaseAccesser.groupsmembers_table,
                new String[]{
                        DatabaseAccesser.groupsmembers_last_rece_time,
                        DatabaseAccesser.groupsmembers_last_seen_time},
                DatabaseAccesser.groupsmembers_group_id + " =?",
                new String[]{gpID},
                null, null, null,null);

        long lastRece,lastSeen,lastSeenTemp,lastReceTemp;
        lastRece=lastSeen=System.currentTimeMillis();
        while (cursor.moveToNext()) {


            lastReceTemp = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.groupsmembers_last_rece_time));
            lastSeenTemp = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.groupsmembers_last_seen_time));

            lastRece= lastRece>lastReceTemp?lastReceTemp:lastRece;
            lastSeen= lastSeen>lastSeenTemp?lastSeenTemp:lastSeen;
        }

        if(status==2)
            return lastRece;
        else if(status==3)
            return lastSeen;
        else
            return 0;


    }

    void update_chatSeenOnDatabase(String userID,String lastRow,int status,String timestamp)
    {
        //SQLiteDatabase db= TabbedActivity.db.getWritableDatabase();
        status=status==0?2:3;
        ContentValues values = new ContentValues();
        values.put(DatabaseAccesser.chat_ifseen, status);
        if(status==2)
        {
            values.put(DatabaseAccesser.chat_rece_timestamp, timestamp);
        }
        else if(status ==3)
        {
            values.put(DatabaseAccesser.chat_seen_timestamp, timestamp);
        }
        int rowln=writableDB.update(DatabaseAccesser.chat_table,values,"rowid"+" =? AND "+
                DatabaseAccesser.chat_userid+" =? AND "+
                DatabaseAccesser.chat_ifseen+" <?",new String[]{lastRow,userID,status+""});

        Log.e("MSGG"," updateddd " +rowln+" / "+status+" / "+lastRow);

        //ChatSender.updateChatWindow(getApplicationContext(),0,Long.parseLong(lastRow),userID,status);
        ChatSender.updateChatWindowHEAVY(userID);
       /* if(ChatWindow.uId!=null)
        {
            if(ChatWindow.uId.equals(userID))
            {

                ChatWindow.getcontentfromdatabase();   //heavy code Do Somethings
                ChatWindow.adap.notifyDataSetChanged();      //


            }
        }*/

        ContentValues values2 = new ContentValues();
        values2.put(DatabaseAccesser.chatList_last_msgseen, status);
        int rowln2=writableDB.update(DatabaseAccesser.chatList_table,values2,DatabaseAccesser.chatList_lastmsgRowId+" <=? and "+DatabaseAccesser.chatList_userid+" =?",new String[]{lastRow,userID});
        Intent new_intent = new Intent();
        new_intent.setAction("CHAT_UPDATED");
        sendBroadcast(new_intent);

    }

    void listenToGroupChats()
    {
        //SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();

        Cursor cursor = readableDB.query(DatabaseAccesser.user_table,
                new String[]{DatabaseAccesser.user_user_id},
                DatabaseAccesser.user_user_type + " =?",
                new String[]{"5"},
                null, null, null,null);

        while (cursor.moveToNext()) {


            final String groupID = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_user_id));


            final  long lastReceived= retrieveLastReceivedTime(groupID);
            listentoSingleGroupChats(groupID,lastReceived);



        }}


    public void listentoSingleGroupChats(final String groupIDFinal,long lastReceived)
    {
        ChildEventListener groupChatListener;
        groupChatListener=new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                //Log.e("MSG","child "+dataSnapshot.getKey()+" last : "+s+" count : ");


                final GroupChatData post = dataSnapshot.getValue(GroupChatData.class);
                String userGroupID=post.userid;
                post.userid=groupIDFinal;
                //lastReceived=post.timestamp;
                updateLastReceivedTime(post.userid,post.timestamp);
                if(post.userid.equals(userdetail.getUid()))
                    return;
                post.sender = 1;


                /**************************/



                /****************/


                String urlToDownload = "null";
                boolean autodownload=false;

                Log.e("MGG","vhat type "+post.type);
                switch(post.type) {

                    case 0:
                        saveGroupChatToDatabase(post, userGroupID);
                        break;



                    case 1:
                        if (checkSearchMultiSelectPref("data_usage_chat", TabbedActivity.PREF_IMAGE)) {
                            autodownload = true;}
                        downloadChatMediaAndSave(post,autodownload,userGroupID);
                        break;
                    case 3:
                        if (checkSearchMultiSelectPref("data_usage_chat", TabbedActivity.PREF_VIDEO))//vvvv
                        {autodownload = true;}

                        downloadChatMediaAndSave(post,autodownload,userGroupID);
                        break;
                    //11 - poke// 12 user addes //13 user removed
                    case 20:
                       // Log.e("MSGG","poke recievedx");
                        if(post.msg.equals(userdetail.getUid())) {
                            Toast.makeText(MainServiceThread.this, "Poke from "+post.userid, Toast.LENGTH_SHORT).show();
                            if(ChatWindow.uId!=null)
                            {
                                if(ChatWindow.uId.equals(post.userid))
                                {

                                    Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                    // Vibrate for 500 milliseconds
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                        v.vibrate(VibrationEffect.createOneShot(500,VibrationEffect.DEFAULT_AMPLITUDE));
                                    }else{
                                        //deprecated in API 26
                                        v.vibrate(500);
                                    }
                                }
                            }
                            dataSnapshot.getRef().removeValue();
                        }
                        break;


                    case 21:
                        Toast.makeText(MainServiceThread.this, "added "+post.msg, Toast.LENGTH_SHORT).show();
                        post.sender=2;
                        addSingleUserFromGroup(post.msg,post.userid);
                        saveGroupChatToDatabase(post, userGroupID);

                        break;
                    case 22:
                        Toast.makeText(MainServiceThread.this, "remove "+post.msg, Toast.LENGTH_SHORT).show();
                        post.sender=2;
                        removeSingleUserFromGroup(post.msg,post.userid);
                        saveGroupChatToDatabase(post, userGroupID);

                        break;
                }


                DatabaseReference databaseRef = groupsRef.child(post.userid).child("chat_status");
                databaseRef.child(userdetail.getUid()).child("last_rece_time").setValue(post.timestamp);

                updateGroupWithLastReceiveMessageTime(post.timestamp,post.userid);


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };



        groupsRef.child(groupIDFinal).child("chats").orderByChild("timestamp").startAt(lastReceived).addChildEventListener(groupChatListener);
        update_group_chat_status(groupIDFinal);


        // groupsRef.child(groupID).child("chats").keepSynced(true);


    }

    public static void removeSingleUserFromGroup(String userId,String grpId)
    {
        TabbedActivity.db.getWritableDatabase().delete(DatabaseAccesser.groupsmembers_table,DatabaseAccesser.groupsmembers_group_id+" =? AND "+
                DatabaseAccesser.groupsmembers_user_id+" =?",new String[]{grpId,userId});
    }

    public static void addSingleUserFromGroup(String userId,String grpId)
    {
        GroupMemberData memberData= new GroupMemberData();
        ContentValues values = new ContentValues();
        values.put(DatabaseAccesser.groupsmembers_group_id,grpId);
        values.put(DatabaseAccesser.groupsmembers_user_id, userId);

        values.put(DatabaseAccesser.groupsmembers_member_type, 0);

        long  newRowId =  TabbedActivity.db.getWritableDatabase()
                .insert(DatabaseAccesser.groupsmembers_table, null, values);
        //Log.e("MSG","insert hre : "+newRowId);

    }

    void updateLastReceivedTime(String grp,long timeData)
    {


        ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.user_timestamp, timeData);

        //long newRowId = writableDB.insert(DatabaseAccesser.user_table, null, values);
        int rowln=writableDB.update(DatabaseAccesser.user_table,values, DatabaseAccesser.user_user_id+" =?",new String[]{grp});
        Log.e("MSGG","last update  :"+timeData+" row : "+rowln);

    }

    long retrieveLastReceivedTime(String grp)
    {

        //SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();

        Cursor cursor = readableDB.query(DatabaseAccesser.user_table,
                new String[]{
                        DatabaseAccesser.user_timestamp},
                DatabaseAccesser.user_user_id + " =?",
                new String[]{grp},
                null, null, null,null);

        cursor.moveToNext();

        //cursor.getInt(
        //cursor.getColumnIndexOrThrow(DatabaseAccesser.user_timestamp));
        long time= cursor.getLong(
                cursor.getColumnIndexOrThrow(DatabaseAccesser.user_timestamp));
       // Log.e("MSGG","last rec :"+time);
        time++;
        return time;
    }


    void saveGroupChatToDatabase(final GroupChatData post, String userGroupID)
    {

        ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.chat_userid, post.userid);
        values.put(DatabaseAccesser.chat_sender, post.sender);
        values.put(DatabaseAccesser.chat_type, post.type);
        values.put(DatabaseAccesser.chat_timestamp, System.currentTimeMillis()); //chattimestamp changes made here deafult was system time
        values.put(DatabaseAccesser.chat_text, post.msg);
        //values.put(DatabaseAccesser.chat_temp_url, post.temp_url);
        values.put(DatabaseAccesser.chat_col_media_id, post.final_url);
        values.put(DatabaseAccesser.chat_groupid, userGroupID);

        final long newRowId = writableDB.insert(DatabaseAccesser.chat_table, null, values);

        ContentValues values2 = new ContentValues();

        values2.put(DatabaseAccesser.chatList_lastmsg, post.msg);
        values2.put(DatabaseAccesser.chatList_timestamp, System.currentTimeMillis());
        values2.put(DatabaseAccesser.chatList_lastmsg_type, post.type);
        values2.put(DatabaseAccesser.chatList_lastmsgbyme, 0);
        values2.put(DatabaseAccesser.chatList_msgqueue, 1);
        values2.put(DatabaseAccesser.chatList_lastmsgRowId, newRowId);
        //values2.put(DatabaseAccesser.chatList_last_msgseen, post.msgseen);


        //int rowln=writableDB.update(DatabaseAccesser.chatList_table,values2, DatabaseAccesser.chatList_userid+" =?",new String[]{groupID});
        writableDB.rawQuery("UPDATE "+DatabaseAccesser.chatList_table+
                " SET "+DatabaseAccesser.chatList_msgqueue+" = "+DatabaseAccesser.chatList_msgqueue+"+1"+ " WHERE "+ DatabaseAccesser.chatList_userid+" =?",new String[]{post.userid});
        if(ChatWindow.uId!=null)
        {
            if(ChatWindow.uId.equals(post.userid))
            {

                ChatWindow.finaldata.add(post);
                ChatWindow.adap.notifyItemInserted(ChatWindow.finaldata.size()-1);
                ChatWindow.chatView.scrollToPosition(ChatWindow.finaldata.size() - 1);

                DatabaseReference databaseRef = groupsRef.child(post.userid).child("chat_status");


                databaseRef.child(userdetail.getUid()).child("last_seen_time").setValue(post.timestamp);
            }
        }

        Intent new_intent = new Intent();
        new_intent.setAction("CHAT_UPDATED");
        sendBroadcast(new_intent);


    }

    static void uploadFileAndReturnDownloadUrl(StorageReference hRef, Uri url, final JobCompletionListner listner)
    {
      /* hRef.putFile(url).add
               .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                   @Override
                   public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                   }

               }).;*/
    }
    public static void downloadFileAndReturnUriString(String url, final JobCompletionListner listner)
    {
        if(url.equals("null")|| !url.contains("http")) {
            listner.onJobCompletion("null");
            return;
        }
        StorageReference httpsReference = FirebaseStorage.getInstance().getReferenceFromUrl(url);
       /* File localFile = null;
        try {
            localFile = File.createTempFile("images", "png");
        } catch (IOException e) {
            e.printStackTrace();
        }*/


        //Log.e("MGG","file name : "+httpsReference.getName());
        final String name = StringUtils.randomStringGenerator() + "."+httpsReference.getName().split("\\.")[1];
        // httpsReference.getName().split(">>")[1];
        //Log.e("MGG","file name : "+name);

        httpsReference.getBytes(1024 * 1024).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {

                try {

                    File file = new File(Environment.getExternalStorageDirectory().getPath(), "Around Files/Media");
                    if (!file.exists()) {
                        file.mkdirs();
                    }



                    FileOutputStream fos = new FileOutputStream(new File(file.getAbsolutePath(), name));
                    fos.write(bytes);
                    fos.close();

                    listner.onJobCompletion(file.toURI().toString() + name);
                }
                catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
            }
        });
    }

    void update_content()
    {
        if(childEventListener==null) {

            childEventListener = new ChildEventListener() {
                @Override
                public void onChildAdded(final DataSnapshot dataSnapshot, String s) {

                    final GroupChatData post = dataSnapshot.getValue(GroupChatData.class);
                    post.sender = 1;
                    post.msgseen =2;

                    // String urlToDownload = "null";
                    boolean autodownload=false;
                    switch(post.type)
                    {
                        case 0:
                            post.media_id=1;
                            saveChatToDatabase(post,null);
                            break;

                        case 1:
                            if (checkSearchMultiSelectPref("data_usage_chat", TabbedActivity.PREF_IMAGE)) {
                                autodownload = true;}
                            downloadChatMediaAndSave(post,autodownload,null);
                            break;
                        case 2:
                            if (checkSearchMultiSelectPref("data_usage_chat", TabbedActivity.PREF_AUDIO))//vvvv
                            {autodownload = true;}
                            downloadChatMediaAndSave(post,autodownload,null);
                            break;

                        case 3:
                            if (checkSearchMultiSelectPref("data_usage_chat", TabbedActivity.PREF_VIDEO))//vvvv
                            {autodownload = true;}

                            downloadChatMediaAndSave(post,autodownload,null);
                            break;
                        case 5:
                            if (checkSearchMultiSelectPref("data_usage_chat", TabbedActivity.PREF_GIF))//vvvv
                            {autodownload = true;}
                            downloadChatMediaAndSave(post,autodownload,null);
                            break;
                        case 6:
                            if (checkSearchMultiSelectPref("data_usage_chat", TabbedActivity.PREF_FIlE))//vvvv
                            {autodownload = true;}
                            downloadChatMediaAndSave(post,autodownload,null);
                            break;
                        case 7:
                            post.media_id=1;
                            saveChatToDatabase(post,null);
                            break;
                        case 4:
                            post.media_id=1;
                            saveChatToDatabase(post,null);
                            break;
                    }


                    //DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference().child("chat_status").child(post.userid);
//yyyy
                    //databaseRef.child(userdetail.getUid()).child("last_rece_msg").setValue(post.sl);
                    UserManager.updateLastReceiveTime(0,post.sl,FirebaseDatabase.getInstance().getReference().child("chat_status").child(post.userid).child(userdetail.getUid()), new JobCompletionWithFailureListner() {
                        @Override
                        public void onJobFailed(String txt1) {  }

                        @Override
                        public void onJobCompletion(String txt1) {
                            dataSnapshot.getRef().removeValue();

                            updateUserWithLastReceiveMessageSl(post.sl,post.userid);
                        }
                    });





                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            };


//                      Toast.makeText(getActivity(), "hurray", Toast.LENGTH_SHORT).show();
            // ...
            //newPostRef.goOffline();
            //newPostRef.removeEventListener(childEventListener);

            Toast.makeText(this, "creeeeeta", Toast.LENGTH_SHORT).show();
            newPostRef.addChildEventListener(childEventListener);

        }

    }

    void downloadChatMediaAndSave(final GroupChatData post,final boolean autodownload,final @Nullable String groupID)
    {
        long temp = checkForMediaAvailablility(post.final_url);
        if (temp != 0) {
            post.media_id = temp;
            if(groupID==null)
                saveChatToDatabase(post,null);
            else
                saveGroupChatToDatabase(post,groupID);

            //return post;
        }

       // if()


        downloadFileAndReturnUriString(post.temp_url, new JobCompletionListner() {
            @Override
            public void onJobCompletion(String txt1) {
                final String litelocal = txt1;
                if (autodownload) {
                    downloadFileAndReturnUriString(post.final_url, new JobCompletionListner() {
                        @Override
                        public void onJobCompletion(String txt1) {
                            final String local = txt1;
                            post.media_id = DatabaseManager.insertMediaToDatabase(post.final_url, local, post.temp_url, litelocal,post.mediaSize);
                            post.temp_url=litelocal;
                            if(groupID==null)
                                saveChatToDatabase(post,null);
                            else
                                saveGroupChatToDatabase(post,groupID);

                        }
                    });
                } else {
                    post.media_id = DatabaseManager.insertMediaToDatabase(post.final_url, "null", post.temp_url, litelocal,post.mediaSize);
                    post.temp_url=litelocal;
                    if(groupID==null)
                        saveChatToDatabase(post,null);
                    else
                        saveGroupChatToDatabase(post,groupID);
                }

            }
        });
    }

    void updateUserWithLastReceiveMessageSl(long serialID,String user)
    {

        ContentValues values2 = new ContentValues();

        values2.put(DatabaseAccesser.user_lastRec_sl, serialID);

        int rowln=writableDB.update(DatabaseAccesser.user_table,values2, DatabaseAccesser.user_user_id+" =?",new String[]{user});

    }
    void updateGroupWithLastReceiveMessageTime(long time,String gpID)
    {

        ContentValues values2 = new ContentValues();

        values2.put(DatabaseAccesser.groupsmembers_last_rece_time, time);

        int rowln=writableDB.update(DatabaseAccesser.groupsmembers_table,values2, DatabaseAccesser.groupsmembers_group_id + " =? AND "+DatabaseAccesser.groupsmembers_user_id+ " =?",
                new String[]{gpID,userdetail.getUid()});

    }

    void saveNotificationToDatabase(final NotificationData post)
    {
        //SQLiteDatabase db = dbAccess.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.notification_user_id, post.userid);
        values.put(DatabaseAccesser.notification_timestamp, System.currentTimeMillis());
        values.put(DatabaseAccesser.notification_status, 0);
        values.put(DatabaseAccesser.notification_text, post.text);
        values.put(DatabaseAccesser.notification_type, post.type);
        values.put(DatabaseAccesser.notification_image_url, post.image);
        values.put(DatabaseAccesser.notification_link, post.link);

        final long newRowId = writableDB.insert(DatabaseAccesser.notification_table, null, values);
       // Log.e("MSG","Size of row :"+newRowId);

    }

    public long saveChatToDatabase(final GroupChatData post,@Nullable String groupID)
    {


        /*ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.chat_userid, post.userid);
        values.put(DatabaseAccesser.chat_sender, post.sender);
        values.put(DatabaseAccesser.chat_type, post.type);
        values.put(DatabaseAccesser.chat_timestamp, System.currentTimeMillis());
        values.put(DatabaseAccesser.chat_text, post.msg);
        //values.put(DatabaseAccesser.chat_temp_url, post.temp_url);
        values.put(DatabaseAccesser.chat_col_media_id, post.media_id);
        if (groupID!=null)
            values.put(DatabaseAccesser.chat_groupid, groupID);

        final long newRowId = writableDB.insert(DatabaseAccesser.chat_table, null, values);

        updateOrInsertIntoChatlist(post,newRowId,System.currentTimeMillis());*/

        final long newRowId = DatabaseManager.saveChatToDatabase(getApplicationContext(),post,groupID);
        post.chatRow=newRowId;

        if(ChatWindow.uId!=null)
        {
            if(ChatWindow.uId.equals(post.userid))
            {

                ChatWindow.finaldata.add(post);
                //ChatWindow.adap.notifyItemInserted(ChatWindow.finaldata.size()-1);
                ChatWindow.adap.notifyDataSetChanged();
                ChatWindow.chatView.scrollToPosition(ChatWindow.finaldata.size() - 1);

                //UserManager.updateLastReceiveTime(1,post.sl,FirebaseDatabase.getInstance().getReference().child("chat_status").child(post.userid).child(userdetail.getUid()), null);
                //DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference().child("chat_status").child(post.userid);
                //databaseRef.child(userdetail.getUid()).child("last_seen_msg").setValue(post.sl);

            }
        }
        else
        {
            //NotificationC

            LocalNotificationManager.sentNewChatNotitication(getApplicationContext(),post.userid,post.msg);
            /*LocalNotificationManager mNotificationManager =
                    (LocalNotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            Intent intenttest = new Intent(getApplicationContext(), TabbedActivity.class);
            PendingIntent pIntent = PendingIntent.getActivity(getApplicationContext(), (int) System.currentTimeMillis(), intenttest, 0);

            Notification.InboxStyle inboxStyle = new Notification.InboxStyle();
            inboxStyle.setBigContentTitle("Messages:");
            inboxStyle.setSummaryText(" New chats");
            inboxStyle.addLine(post.msg);



            Notification n  =  new Notification.Builder(getApplicationContext())
                    .setContentTitle("SpotIt")
                    .setContentText(" New chats")
                    .setSmallIcon(R.drawable.nick)
                    .setContentIntent(pIntent)
                    .setStyle(inboxStyle)
                    .setPriority(Notification.PRIORITY_MAX)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true).build();

            mNotificationManager.notify(0,n);*/

            Intent new_intent = new Intent();
            new_intent.setAction("CHAT_UPDATED");
            sendBroadcast(new_intent);
        }

        return newRowId;
    }

    public static void updateOrInsertIntoChatlist(final Context context,final GroupChatData post,final long  newRowId,long timedata)
    {

          final  SQLiteDatabase db=DatabaseManager.getDatabase(context).getWritableDatabase();
        //values2.put(DatabaseAccesser.chatList_last_msgseen, post.msgseen);

        final ContentValues values2 = new ContentValues();

        values2.put(DatabaseAccesser.chatList_lastmsg, post.msg);
        values2.put(DatabaseAccesser.chatList_timestamp,timedata);
        values2.put(DatabaseAccesser.chatList_lastmsg_type, post.type);
        values2.put(DatabaseAccesser.chatList_lastmsgRowId, newRowId);
        if(post.sender==0)
        {
            values2.put(DatabaseAccesser.chatList_lastmsgbyme, 1);
            values2.put(DatabaseAccesser.chatList_last_msgseen, post.msgseen);
        }
        else
        {
            values2.put(DatabaseAccesser.chatList_lastmsgbyme, 0);
        }



        int rowln=db.update(DatabaseAccesser.chatList_table,values2, DatabaseAccesser.chatList_userid+" =?",new String[]{post.userid});

        if(post.sender==1) {
            db.execSQL("UPDATE " + DatabaseAccesser.chatList_table +
                    " SET " + DatabaseAccesser.chatList_msgqueue + " =" + DatabaseAccesser.chatList_msgqueue + "+1" + " WHERE " + DatabaseAccesser.chatList_userid + " ='" + post.userid + "'");
            // Log.e("MSGG","kolAA==update : "+rowln+" cur time "+timedata);
        }


        if(rowln<1)
        {


            final String msg=post.msg;
            final String UserId=post.userid;


            fetchUserDetailsbyIDOrPhone(post.userid, true, new ProcessCompletedListner() {
                @Override
                public void processCompleteAction() {

                }

                @Override
                public void newUserAdded(UserWholeData data) {
                    final ContactListData contact=data;
                    updateOrInsertUserIfNone(data, new JobCompletionListner() {
                        @Override
                        public void onJobCompletion(String txt1) {
                            //ContentValues values3 = new ContentValues();
                            values2.put(DatabaseAccesser.chatList_userid, UserId);
                            values2.put(DatabaseAccesser.chatList_name, contact.user_name);   //not needed - name already in user data
                            //values3.put(DatabaseAccesser.chatList_lastmsgbyme, 0);
                            //values3.put(DatabaseAccesser.chatList_lastmsg, msg);
                            //values3.put(DatabaseAccesser.chatList_lastmsgRowId, newRowId);
                            //values3.put(DatabaseAccesser.chatList_lastmsg_type, post.type);
                            //values3.put(DatabaseAccesser.chatList_timestamp,System.currentTimeMillis());
                            if(post.sender==1)
                            values2.put(DatabaseAccesser.chatList_msgqueue, 1);
                            values2.put(DatabaseAccesser.chatList_url, contact.img_url);   //very much needed needed - url already in user data

                            long rowln=db.insert(DatabaseAccesser.chatList_table, null, values2);
                            Intent new_intent = new Intent();
                            new_intent.setAction("CHAT_UPDATED");
                            context.sendBroadcast(new_intent);
                        }
                    });
                }
            });

        }
    }

    boolean checkSearchMultiSelectPref(String key,int val)
    {

        SharedPreferences pref=PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Set<String>  selections =pref.getStringSet(key,null);

        if(selections!=null) {
            if (selections.contains(val + "")) {
                return true;
            }
        }
        return false;
    }


    // use final variables   ////use public static

    void update_feed(Double lat,Double lng)
    {
        //Toast.makeText(getApplicationContext(), "Searching new Feeds", Toast.LENGTH_SHORT).show();
        geoQuery = geoFire.queryAtLocation(new GeoLocation(lat, lng),AROUND_FEED_RADIUS);
        // geoQuery = geoFire.q
        geoQuery.addGeoQueryEventListener(new GeoQueryEventListener() {
            @Override
            public void onKeyEntered(String key, final GeoLocation location) {

                //Log.e("MSG",location.latitude+" "+location.longitude);

                DatabaseReference newPostRef = fDatabase.child("feed_stack").child(key);

                postListener = new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        // Get Post object and use the values to update the UI
                        final FeedData post= dataSnapshot.getValue(FeedData.class);

                        post.ifseen=0;

                       /* post.userid;
                        post.username;
                        post.type;
                        post.timestamp;
                        post.text;
                        post.semi_uri;
                        post.uri;*/
                        // Toast.makeText(getActivity(), "data from user " + post.userid, Toast.LENGTH_SHORT).show();

                        // SQLiteDatabase db = dbAccess.getReadableDatabase();

                        Cursor cursor = readableDB.rawQuery(" select " + DatabaseAccesser.feed_col_user_id + " " +
                                DatabaseAccesser.feed_col_timestamp + " from " + DatabaseAccesser.feed_table + " where " +
                                DatabaseAccesser.feed_col_user_id + " = ? AND " +
                                DatabaseAccesser.feed_col_timestamp + " = ?", new String[]{post.userid , post.timestamp + ""});
                        //sqlDatabase.rawQuery("select docid as _id, recipeID from " + TABLE_RECIPE_NAME +
                        // " where " + KEY_ownerID + " = ? AND  " + KEY_partnerID + " = ? AND  " + KEY_advertiserID + " = ? AND " + KEY_chefID + " = ?", new String[] { ownerID, partnerID, advertiserID, chefID });



                        if (cursor.getCount() == 0) {
                           // Log.e("MSGG","====new post");

                            if (post.type == 0) {
                                // finaldata.add(post);
                                // adap.notifyItemInserted(finaldata.size() - 1);

                                // adap.notifyItemChanged(finaldata.size() - 1);
                                // rView.scrollToPosition(finaldata.size() - 1);
                                post.uri="1";
                                saveFeedToDatabase(post,location);
                            }
                            else{
                                long temp=checkForMediaAvailablility(post.uri);
                                if(temp!=0)
                                {
                                    post.uri=temp+"";
                                    saveFeedToDatabase(post, location);
                                    cursor.close();
                                    return;
                                }

                                String urlToDownload="null";
                                final boolean autodownload;

                                if(post.type==1) {
                                    if (checkSearchMultiSelectPref("data_usage_feed", TabbedActivity.PREF_IMAGE))
                                    {
                                        urlToDownload = post.uri;
                                        autodownload = true;
                                    } else {
                                        urlToDownload = post.semi_uri;
                                        autodownload = false;
                                    }
                                }
                                else
                                {
                                    if (checkSearchMultiSelectPref("data_usage_feed", TabbedActivity.PREF_VIDEO))//vvvv
                                    {
                                        urlToDownload = post.uri;
                                        autodownload = true;
                                    } else {
                                        urlToDownload = post.semi_uri;
                                        autodownload = false;
                                    }
                                }
                                // final String local,litelocal;
                               // Log.e("MSGG","1");
                                downloadFileAndReturnUriString(post.semi_uri, new JobCompletionListner() {
                                    @Override
                                    public void onJobCompletion(String txt1) {
                                        final String litelocal=txt1;
                                        if(autodownload) {
                                            downloadFileAndReturnUriString(post.uri, new JobCompletionListner() {
                                                @Override
                                                public void onJobCompletion(String txt1) {
                                                    final String local = txt1;
                                                    //Log.e("MSGG","2");
                                                    post.uri= DatabaseManager.insertMediaToDatabase(post.uri, local, post.semi_uri, litelocal,post.mediaSize)+"";
                                                    saveFeedToDatabase(post, location);
                                                }});}
                                        else
                                        {
                                            //post.semi_uri +=">>"+ file.toURI().toString() + name;
                                            //Log.e("MSGG","3");
                                            post.uri=DatabaseManager.insertMediaToDatabase(post.uri, "null", post.semi_uri, litelocal,post.mediaSize)+"";
                                            saveFeedToDatabase(post, location);
                                            //post.uri="null";
                                        }

                                    }
                                });


                            }
                        }


                        cursor.close();
//                      Toast.makeText(getActivity(), "hurray", Toast.LENGTH_SHORT).show();
                        // ...
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        // Getting Post failed, log a message
                        // Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
                        // ...
                    }
                };

                newPostRef.addListenerForSingleValueEvent(postListener);


                //System.out.println(String.format("Key %s entered the search area at [%f,%f]", key, location.latitude, location.longitude));
            }

            @Override
            public void onKeyExited(String key) {
                //System.out.println(String.format("Key %s is no longer in the search area", key));
            }

            @Override
            public void onKeyMoved(String key, GeoLocation location) {
                //System.out.println(String.format("Key %s moved within the search area to [%f,%f]", key, location.latitude, location.longitude));
            }

            @Override
            public void onGeoQueryReady() {
                //System.out.println("All initial data has been loaded and events have been fired!");
            }

            @Override
            public void onGeoQueryError(DatabaseError error) {
                //System.err.println("There was an error with this query: " + error);
            }
        });


    }

   /* void saveGroupToDatabase(ContactListData post,String globalUrl)
    {

        ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.group_grp_id, post.user_id);
        values.put(DatabaseAccesser.group_name, post.user_name);
        values.put(DatabaseAccesser.group_status, post.status);
        values.put(DatabaseAccesser.group_type, post.type);
        values.put(DatabaseAccesser.group_globalUrl,globalUrl);
        values.put(DatabaseAccesser.group_localUrl, post.img_url);
        values.put(DatabaseAccesser.group_fetch_timestamp, System.currentTimeMillis());



        long newRowId = writableDB.insert(DatabaseAccesser.group_table, null, values);
    }*/


    void saveContactToDatabase(ContactListData post,String globalUrl)
    {
        //SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();


        ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.user_user_id, post.user_id);
        values.put(DatabaseAccesser.user_name, post.user_name);
        values.put(DatabaseAccesser.user_status, post.status);
        if(post.user_id.equals(userdetail.getUid()))
        {
            values.put(DatabaseAccesser.user_user_type, 0);
        } else
        {
            values.put(DatabaseAccesser.user_user_type, post.type);
        }
        values.put(DatabaseAccesser.user_globalUrl,globalUrl);
        values.put(DatabaseAccesser.user_localUrl, post.img_url);
        values.put(DatabaseAccesser.user_timestamp, System.currentTimeMillis());



        long newRowId = writableDB.insert(DatabaseAccesser.user_table, null, values);
    }

    void saveFeedToDatabase(FeedData post,GeoLocation location)
    {
        // SQLiteDatabase db = dbAccess.getWritableDatabase();

        //  Toast.makeText(getActivity(), "Now saving to database", Toast.LENGTH_SHORT).show();
        ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.feed_col_user_id, post.userid);
        values.put(DatabaseAccesser.feed_col_username, post.username);
        values.put(DatabaseAccesser.feed_col_timestamp, post.timestamp);
        values.put(DatabaseAccesser.feed_col_type, post.type);
        values.put(DatabaseAccesser.feed_col_seen, post.ifseen);
        values.put(DatabaseAccesser.feed_col_text, post.text);
        values.put(DatabaseAccesser.feed_col_media_id, post.uri);
        //values.put(DatabaseAccesser.feed_col_url, post.uri);
        values.put(DatabaseAccesser.feed_col_latitude, location.latitude);
        values.put(DatabaseAccesser.feed_col_longitude, location.longitude);


        long newRowId = writableDB.insert(DatabaseAccesser.feed_table, null, values);


        Intent new_intent = new Intent();
        new_intent.setAction("FEED_UPDATED");
        sendBroadcast(new_intent);
    }


}
