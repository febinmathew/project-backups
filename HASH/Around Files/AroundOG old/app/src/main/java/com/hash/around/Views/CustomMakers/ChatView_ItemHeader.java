package com.hash.around.Views.CustomMakers;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Build;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.hash.around.OtherClasses.ChatDataAdapter;
import com.hash.around.R;
import com.hash.around.Views.ViewFunctions;

public class ChatView_ItemHeader extends  RecyclerView.ItemDecoration{
    Context mContext;
    ChatDataAdapter adapter;
    public ChatView_ItemHeader(Context context, ChatDataAdapter adap)
    {
        // mDivider = ContextCompat.getDrawable(context, R.drawable.ic_fingerprint_color_primary);
        mContext=context;
        this.adapter=adap;
    }
    public boolean isReverseLayout(final RecyclerView parent) {
        return (parent.getLayoutManager() instanceof LinearLayoutManager) &&
                ((LinearLayoutManager)parent.getLayoutManager()).getReverseLayout();
    }
    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        final int count = parent.getChildCount();

        for (int layoutPos = 0; layoutPos < count; layoutPos++) {
            final View child = parent.getChildAt(translatedChildPosition(parent, layoutPos));

            final int adapterPos = parent.getChildAdapterPosition(child);

            if (adapterPos != RecyclerView.NO_POSITION && hasHeader(adapterPos,parent)) {
                View header = getHeader(parent, mContext,adapterPos,false).itemView;

                c.save();
                final int left = child.getLeft();
                final int top = getHeaderTop(parent, child, header, adapterPos, layoutPos);
                c.translate(left, top);
                header.draw(c);
                c.restore();


            }
        }
    }
    
    public  boolean hasHeader(int adapPos,RecyclerView par)
    {
        return false;
    }
    public int translatedChildPosition(RecyclerView parent, int position) {
        return isReverseLayout(parent) ? parent.getChildCount() - 1 - position : position;
    }

    public int getChildY(RecyclerView parent, View child) {
        if (Build.VERSION.SDK_INT < 11) {
            Rect rect = new Rect();
            parent.getChildVisibleRect(child, rect, null);
            return rect.top;
        } else {
            return (int) ViewCompat.getY(child);
        }
    }
    protected int getHeaderTop(RecyclerView parent, View child, View header, int adapterPos,
                               int layoutPos)
    {
        return 0;
    }
    RecyclerView.ViewHolder getHeader(RecyclerView parent, Context context, int position,boolean forHeight)
    {
        return null;
    }

    public static class HeaderViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;

        HeaderViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.decorator_text);
        }
        HeaderViewHolder(TextView itemView) {
            super(itemView);
            textView = itemView;
        }
    }
    public  static class ConversationDateHeader extends HeaderViewHolder {

        private final Animation animateIn;
        private final Animation animateOut;

        private boolean pendingHide = false;

        public ConversationDateHeader(Context context, TextView textView) {
            super(textView);
            this.animateIn = AnimationUtils.loadAnimation(context, R.anim.toggle_animation_in);
            this.animateOut = AnimationUtils.loadAnimation(context, R.anim.toggle_animation_out);

            this.animateIn.setDuration(100);
            this.animateOut.setDuration(100);
        }

        public void show() {
            if (pendingHide) {
                pendingHide = false;
            } else {
                ViewFunctions.animateIn(textView, animateIn);
            }
        }

        public void hide() {
            pendingHide = true;

            textView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (pendingHide) {
                        pendingHide = false;
                        ViewFunctions.animateOut(textView, animateOut, View.GONE);
                    }
                }
            }, 400);
        }
    }
}


