package com.hash.around;

import android.*;
import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.os.Vibrator;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import java.io.File;

/**
 * Created by Febin on 5/31/2018.
 */

public class AroundStaticInfo {


    public static String getDefaultAudioDirectory()
    {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "Around Files/Audio");
        if (!file.exists()) {
            file.mkdirs();
        }
        return file.getPath();
    }

    public static boolean requestPermissionsOneByOne(Activity mActivity,String [] permissions,int requestCode)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            for (String perm:permissions) {


            if (mActivity.checkSelfPermission(perm) != PackageManager.PERMISSION_GRANTED) {


                ActivityCompat.requestPermissions(mActivity,new String[]{perm}, requestCode);


                return false;
            }

            }

        }

        return true;
    }

    public static void vibrateInMilliSeconds(Context mContext,long time)
    {
        Vibrator vibrator = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(time);
    }
}
