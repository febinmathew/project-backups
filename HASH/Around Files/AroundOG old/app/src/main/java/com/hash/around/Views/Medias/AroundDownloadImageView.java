package com.hash.around.Views.Medias;

import android.content.Context;
import android.content.res.TypedArray;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnPausedListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.hash.around.OtherClasses.Interfaces.FileUploadListner;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.R;
import com.hash.around.Utils.ChatSender;
import com.hash.around.Utils.MediaManager;
import com.pitt.library.fresh.FreshDownloadView;

import java.io.File;
import java.io.IOException;




public class AroundDownloadImageView extends RelativeLayout {

    private boolean centerCrop;
    private int imageMaxHeight;


    View rootView;
    Context mContext;
    ImageView imageContent;
    FreshDownloadView downloadView;
    TextView sizeText;
    JobCompletionListner jobComplete = null;


    public AroundDownloadImageView(Context context) {
        super(context);
        init(context, null);
    }

    public AroundDownloadImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public AroundDownloadImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public void setOnJobCompletionListner(JobCompletionListner job) {
        this.jobComplete = job;
    }
    FileUploadListner uploadListner = null;
    public void setOnFileUploadListener(FileUploadListner job) {
        this.uploadListner = job;
    }

    public void setDownloadClickListner(String uri) {
        downloadView.setVisibility(VISIBLE);
        sizeText.setVisibility(VISIBLE);
        final String url = uri;
        downloadView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadFile(url);
            }
        });
    }

    public void setUploadClickListner(String uri,final StorageReference ref) {
        downloadView.setVisibility(VISIBLE);
        sizeText.setVisibility(VISIBLE);
        final String url = uri;
        downloadView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadFile(url,ref);
            }
        });
    }

    public void enableDownloadClick()
    {
        downloadView.setVisibility(VISIBLE);

        sizeText.setVisibility(VISIBLE);
    }
    public void disableDownloadClick() {
        Log.e("MSG", "disable");
        downloadView.setVisibility(GONE);
        downloadView.setOnClickListener(null);
        sizeText.setVisibility(GONE);
    }


    private void init(Context context, @Nullable AttributeSet attrs) {

        mContext = context;
        rootView = inflate(context, R.layout.around_imageview, this);
        imageContent = (ImageView) rootView.findViewById(R.id.imageContent);
        downloadView = (FreshDownloadView) rootView.findViewById(R.id.downloadView);
        sizeText=rootView.findViewById(R.id.sizeText);


        if (attrs != null) {
            TypedArray ta = context.getTheme().obtainStyledAttributes(attrs, R.styleable.AroundDownloadImageView, 0, 0);
            /*centerCrop = ta.getBoolean(R.styleable.AroundDownloadImageView_imageCenterCrop, false);
            if (centerCrop)
                imageContent.setScaleType(ImageView.ScaleType.CENTER_CROP);

            imageMaxHeight = ta.getDimensionPixelSize(R.styleable.AroundDownloadImageView_imageMaxHeight, imageContent.getHeight());
            imageContent.setMaxHeight(imageMaxHeight);*/

            /*imageContent.setImageResource(ta.ge(R.styleable.AroundDownloadImageView_imageCenterCrop,
                    R.drawable.ic_more_options_black));*/


        }

    }

    public ImageView getImageView() {
        return imageContent;
    }
    public void setImageViewUri(Uri url) {

        this.imageContent.setImageURI(url);
    }

    public void setLocallyAvailable(boolean locallyAvailable)
    {
        if(locallyAvailable)
        {
            disableDownloadClick();
        }
        else
        {
            enableDownloadClick();
        }
    }
    FreshDownloadView getDownloadView() {
        return downloadView;
    }

    void downloadFile(String urlToDownload) {
        Log.e("MSG", "download "+urlToDownload);
        downloadView.startDownload();

        File localFile = null;
        final String localFilename;
        try {
            localFile = File.createTempFile("images", "png");

        } catch (IOException e) {
            e.printStackTrace();
        }
        localFilename = localFile.toURI().toString();
        StorageReference httpsReference = FirebaseStorage.getInstance().getReferenceFromUrl(urlToDownload);
        httpsReference.getFile(localFile).addOnProgressListener(new OnProgressListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onProgress(FileDownloadTask.TaskSnapshot taskSnapshot) {
                float percent = ((float) taskSnapshot.getBytesTransferred() / (float) taskSnapshot.getTotalByteCount()) * 100;
                // Log.e("MSG",((float)taskSnapshot.getBytesTransferred()/(float)taskSnapshot.getTotalByteCount())*100+"//\\"+
                //((float)taskSnapshot.getBytesTransferred()/(float)taskSnapshot.getTotalByteCount())+"//\\"+percent);
                downloadView.upDateProgress((int) percent);
            }
        }).addOnFailureListener(new OnFailureListener() {

            @Override
            public void onFailure(@NonNull Exception e) {
                downloadView.showDownloadError();
            }
        }).addOnPausedListener(new OnPausedListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onPaused(FileDownloadTask.TaskSnapshot taskSnapshot) {
                downloadView.reset();
                Log.e("MSG", "paused");

            }
        }).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                // downloadView.showDownloadOk();
                Log.e("MSG", "succeded");
                disableDownloadClick();
                if (jobComplete != null) {
                    jobComplete.onJobCompletion(localFilename);
                }
                //downloadView.clearAnimation();

            }
        })

        ;
    }

    void uploadFile(String urlToUpload,StorageReference ref) {

        Log.e("MSGG","uri is : "+urlToUpload );
        MediaManager.uploadResourceAndReturnUrls(getContext(),1, urlToUpload, ref,new FileUploadListner() {
            @Override
            public void onFileUploaded(String global, String globalLite,String c) {


                uploadListner.onFileUploaded(global,globalLite,null);
               // updateMediaInDatabase(mediaPosition,global,null,globalLite,null);
               // x.userid=userdetail.getUid();
               // uploadChat(x,null);

            }
        });
    }
}
