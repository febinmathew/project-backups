package com.hash.around.Views.Medias;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.gms.common.util.IOUtils;
import com.hash.around.OtherClasses.DataTypes.MediaData;
import com.hash.around.R;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


public class ChatReplayLayout extends RelativeLayout {

    int radius;

    public ChatReplayLayout(Context context) {
        this(context, null);
    }

    public ChatReplayLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ChatReplayLayout(final Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        inflate(context, R.layout.chat_replay_viewsection, this);
        if (attrs != null) {
            /*TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CircularRelativeLayout, 0, 0);
            this.radius   = typedArray.getInt(R.styleable.CircularRelativeLayout_radius, 4);
            typedArray.recycle();*/
        }
        //MediaData dat = new MediaData();
        //dat.uri = "uriuriuriuriuri";
        //Log.e("MGG","dat : "+dat.uri);

        try {
           // toByteArray(dat);
           // serialize(dat);
            //MediaData dat2=(MediaData)deserialize();
            //Log.e("MGG","dat2 : "+dat2.uri);


        } catch (Exception e) {

        }


    }

    /*public void toByteArray(Object obj) throws IOException {

        Log.i("MGG", "Done conversion");
        outputstream.close();
    }

    public void fromByteArray(Object obj) throws IOException {
        FileInputStream inputstream = new FileInputStream(new File("/storage/emulated/0/Download/your_file.bin"));
        inputstream.read((byte[]) obj);
        Log.i("MGG", "Done conversion");
        outputstream.close();

    }*/
    public static byte[] serialize(Object obj) throws IOException {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        ObjectOutputStream o = new ObjectOutputStream(b);
        o.writeObject(obj);

        FileOutputStream outputstream = new FileOutputStream(new File("/storage/emulated/0/Download/your_file.bin"));
        outputstream.write(b.toByteArray());

            return b.toByteArray();

    }



    public static Object deserialize() throws IOException, ClassNotFoundException {

        FileInputStream inputstream = new FileInputStream(new File("/storage/emulated/0/Download/your_file.bin"));
        //inputstream.read((byte[]) obj);
        //IOUtils.toByteArray(inputstream);

        ByteArrayInputStream b = new ByteArrayInputStream(IOUtils.toByteArray(inputstream));
        ObjectInputStream o = new ObjectInputStream(b);
        return o.readObject();


    }

    @Override
    protected void onDraw(Canvas canvas) {
       super.onDraw(canvas);

        /*drawRect.left = 0;
        drawRect.top = 0;
        drawRect.right = getWidth();
        drawRect.bottom = getHeight();

        clipPath.reset();


        clipPath.addRoundRect(drawRect, dimen, dimen, Path.Direction.CW);
        canvas.clipPath(clipPath);*/

        /*Path clipPath = new Path();
        float radius = 10.0f;
        float padding = radius / 2;
        int w = this.getWidth();
        int h = this.getHeight();
        clipPath.addRoundRect(new RectF(padding, padding, w - padding, h - padding), radius, radius, Path.Direction.CW);
        canvas.clipPath(clipPath);
        super.onDraw(canvas);*/


    }


    private final RectF drawRect = new RectF();
    private final Path  clipPath = new Path();
    int dimen=getResources().getDimensionPixelSize(R.dimen.default_corner_radius);
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        // compute the path
       /* float halfWidth = w / 2f;
        float halfHeight = h / 2f;
        float centerX = halfWidth;
        float centerY = halfHeight;
        path.reset();
        path.addCircle(centerX, centerY, Math.min(halfWidth, halfHeight), Path.Direction.CW);
        path.close();*/
        drawRect.left = 0;
        drawRect.top = 0;
        drawRect.right = getWidth();
        drawRect.bottom = getHeight();
        clipPath.reset();
        clipPath.addRoundRect(drawRect, dimen, dimen, Path.Direction.CCW);
        clipPath.close();
        }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        int save = canvas.save();
        canvas.clipPath(clipPath);
        super.dispatchDraw(canvas);
        canvas.restoreToCount(save);
    }
}