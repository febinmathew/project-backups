package com.hash.around.OtherClasses;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.hash.around.OtherClasses.DataTypes.FeedData;
import com.hash.around.OtherClasses.DataTypes.FeedDataList;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.OtherClasses.Interfaces.ProcessCompletedListner;
import com.hash.around.R;
import com.hash.around.TabFragments.FeedFullScreen;
import com.hash.around.TabbedActivity;
import com.hash.around.Utils.DatabaseManager;
import com.hash.around.Views.Medias.AroundDownloadImageView;
import com.hash.around.Views.Medias.AroundDownloadVideoView;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static com.hash.around.TabFragments.TabFragment2.sharePost;


/**
 * Created by Febin on 26-08-2017.
 */

public class Adapter_FeedData extends RecyclerView.Adapter implements View.OnCreateContextMenuListener{

    public static int optionPosition=-1;
    Activity mContext;
    public ArrayList<FeedData> data;
    ArrayList<FeedDataList> data2;
    private ProcessCompletedListner onLoadMoreListener;
    private int visibleThreshold = 1;
    private int lastVisibleItem,prevlastVisibleItem=-2, totalItemCount;
    private boolean loading;
    public Adapter_FeedData(Activity mcont, ArrayList<FeedData> list,ArrayList<FeedDataList> list2,RecyclerView recyclerView)
    {
        mContext=mcont;
        data=list;
        data2=list2;


        final Context ccc=mContext;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                .getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrolled(RecyclerView recyclerView,
                                           int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);

                        totalItemCount = linearLayoutManager.getItemCount();
                        lastVisibleItem = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
                        if ( lastVisibleItem == 0) {
                            // End has been reached
                            if(!loading) {
                                prevlastVisibleItem = lastVisibleItem;

                                Toast.makeText(ccc, "feeds: " + totalItemCount + " " + lastVisibleItem, Toast.LENGTH_SHORT).show();
                                // Do something
                                if (onLoadMoreListener != null) {
                                    onLoadMoreListener.processCompleteAction();

                                }
                                loading = false;
                            }

                        }
                    }
                });

    }

    public void setOnLoadMoreListener(ProcessCompletedListner processListner) {
        this.onLoadMoreListener = processListner;
    }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v=null;

        switch(viewType)
        {
            case 0:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.feed_text, parent, false);
                // set the view's size, margins, paddings and layout parameters

                ViewHolder0 vh = new ViewHolder0(v);
                return vh;

            case 1:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.feed_image, parent, false);
                // set the view's size, margins, paddings and layout parameters

                ViewHolder1 vh1 = new ViewHolder1(v);
                return vh1;

            case 2:

                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.feed_video, parent, false);
                // set the view's size, margins, paddings and layout parameters

                ViewHolder2 vh_video = new ViewHolder2(v);
                return vh_video;

        }

return null;

    }




   /* public static void updateMediaTableFromMediaRowID(long rowID,long global)
    {
        //xxxx
        SQLiteDatabase db= TabbedActivity.db.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseAccesser.media_global_uri,global);
        values.put(DatabaseAccesser.media_local_uri,locallite);
        int rowln=db.update(DatabaseAccesser.media_table,
                values,
                " media_lite_local"+ " = ?",
                new String[]{locallite});
    }*/

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final int pos=position;


        String profurl="null";

        SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();


        Cursor cursor = db.rawQuery(" select " +
                DatabaseAccesser.user_localUrl + " from " + DatabaseAccesser.user_table + " where " +
                DatabaseAccesser.user_user_id + " = ?"+" limit 1", new String[]{data.get(pos).userid});

        Log.e("MSG","counts of user "+cursor.getCount());
        while (cursor.moveToNext()) {

            profurl = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_localUrl));
        }
        Log.e("MSG","profile url "+profurl);

        cursor.close();


        switch(data.get(pos).type)
        {
            case 0:
                if(profurl.equals("null"))
                    ((ViewHolder0) holder).proimg.setImageResource(R.drawable.default_user);
                else
                    ((ViewHolder0) holder).proimg.setImageURI(Uri.parse(profurl));
                ((ViewHolder0) holder).userName.setText(data.get(pos).username);

                ((ViewHolder0) holder).tx.setText(data.get(position).text+" "+data2.get(pos).unseen+"-"+data2.get(pos).seen);
                ((ViewHolder0) holder).cv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       // Context con=v.getContext();
                        //Toast.makeText(con, "Happesns", Toast.LENGTH_SHORT).show();
                        Intent indi=new Intent(mContext,FeedFullScreen.class);
                        indi.putExtra("username",data.get(pos).username);
                        indi.putExtra("userid",data.get(pos).userid);
                        mContext.startActivity(indi);
                    }
                });
                ((ViewHolder0) holder).cTimeText.setText(getTimeFromMils(data.get(pos).timestamp));

                setupprogress(((ViewHolder0) holder).progressLayout,data2.get(pos).seen,data2.get(pos).unseen);


                break;

            case 1:
                /*Image Starts Here*/
                final ViewHolder1 feedView=((ViewHolder1) holder);
                   if(profurl.equals("null"))
                   {
                       Log.e("MSG","user : "+data.get(pos).userid);
                   feedView.proimg.setImageResource(R.drawable.default_user);
                       final int pos1=pos;


                   }
                else
                   {

                           ((ViewHolder1) holder).proimg.setImageURI(Uri.parse(profurl));

                   }

                feedView.userName.setText(data.get(pos).username);


                ((ViewHolder1)holder).popup_menu.setOnCreateContextMenuListener(this);

                ((ViewHolder1)holder).popup_menu.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        optionPosition= pos;
                        view.showContextMenu();
                    }
                });

                /*((ViewHolder1)holder).popup_menu.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        PopupMenu popup = new PopupMenu(mContext, view);
                        MenuInflater inflater = popup.getMenuInflater();
                        inflater.inflate(R.menu.feed_pop_up, popup.getMenu());
                        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                //do your things in each of the following cases
                                switch (item.getItemId()) {
                                    case R.id.block_post:
                                        return true;
                                    case R.id.report:
                                        return true;
                                    case R.id.view_profile:
                                        return true;
                                    default:
                                        return false;
                                }
                            }
                        });
                        popup.show();
                    }
                });*/
                Log.e("MSGG","SECOND: "+ data.get(pos).uri+" value :"+data.get(pos).text );
                if(data.get(position).uri.contains("https"))
                {
                    ((ViewHolder1)holder).img.setImageURI(Uri.parse(data.get(position).semi_uri));
                    ((ViewHolder1)holder).aroundImageView.setDownloadClickListner(data.get(position).uri);
                    ((ViewHolder1)holder).aroundImageView.setOnJobCompletionListner(new JobCompletionListner() {
                        @Override
                        public void onJobCompletion(String text1) {


                            DatabaseManager.updateMediaTableFromGlobalUrl(data.get(pos).uri,text1);

                            //Log.e("MSG","completed");


                            data.get(pos).uri=text1;
                            //((ViewHolder1)holder).aroundImageView.disableDownloadClick();
                            notifyItemChanged(pos);


                        }
                    });

                }
                else
                {

                    ((ViewHolder1)holder).img.setImageURI(Uri.parse(data.get(position).uri));
                    ((ViewHolder1)holder).aroundImageView.disableDownloadClick();
                    //((ViewHolder1)holder).aroundImageView.setDownloadClickListner("https://firebasestorage.googleapis.com/v0/b/aroundsocialmedia77.appspot.com/o/feed_datas%2Fk5GGMdHoOIcBDWJBqmKaOSLAC7s1%2Fzttaqmubddlcfiwwmgqy.png?alt=media&token=a5d2d64b-f7fa-4ea6-9a49-cbb22a5f7754");
                }


                ((ViewHolder1)holder).cv.setOnCreateContextMenuListener(this);


                ((ViewHolder1)holder).cv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Context con=v.getContext();
                        //Toast.makeText(con, "Happesns", Toast.LENGTH_SHORT).show();
                        Intent indi=new Intent(mContext,FeedFullScreen.class);
                        indi.putExtra("username",data.get(pos).username);
                        indi.putExtra("userid",data.get(pos).userid);
                        mContext.startActivity(indi);

                    }
                });


                ((ViewHolder1)holder).cv.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        optionPosition= pos;
                        view.showContextMenu();
                        //Toast.makeText(mContext, "long click", Toast.LENGTH_SHORT).show();
                        return true;
                    }
                });
                ((ViewHolder1) holder).tx.setText(data.get(position).text+" "+data2.get(pos).unseen+"-"+data2.get(pos).seen);
                //((ViewHolder1) holder).tx.setText(((ViewHolder1) holder).tx.getText()+" "+data.get(position).longitude);
                final TextView p=((ViewHolder1)holder).tx;
                ((ViewHolder1) holder).imgexpand.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // p.animate().setMaxLines(1).setDuration(500);
                        p.setMaxLines(10);

                        Toast.makeText(mContext,data.get(pos).text, Toast.LENGTH_SHORT).show();
                        //Intent in=new Intent(mContext, Profile_Activity.class);

                        // mContext.startActivity(in);
                    }
                });

                ((ViewHolder1) holder).cTimeText.setText(getTimeFromMils(data.get(pos).timestamp));


                setupprogress(((ViewHolder1) holder).progressLayout,data2.get(pos).seen,data2.get(pos).unseen);

                if(data.get(pos).userid.equals(TabbedActivity.userdetail.getUid()))

                {
                    ((ViewHolder1) holder).respondPanel.setVisibility(View.GONE);
                }
                else
                {
                    ((ViewHolder1) holder).respondPanel.setVisibility(View.VISIBLE);
                    ((ViewHolder1) holder).respondPanel.findViewById(R.id.sharePost).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            shareSelectedPost(data.get(pos));
                        }
                    });
                }



                //((ViewHolder1) holder).mElasticDownloadView.startIntro();
                //((ViewHolder1) holder).mElasticDownloadView.setProgress(25);




                /* Image Ends here */
                break;
            case 2:

                if(profurl.equals("null"))
                    ((ViewHolder2) holder).proimg.setImageResource(R.drawable.default_user);
                else
                    ((ViewHolder2) holder).proimg.setImageURI(Uri.parse(profurl));
                ((ViewHolder2) holder).userName.setText(data.get(pos).username);

                if(data.get(position).uri.contains("https"))
                {
                    ((ViewHolder2)holder).aroundVideoView.getImageView().setImageURI(Uri.parse(data.get(position).semi_uri));
                    ((ViewHolder2)holder).aroundVideoView.setDownloadClickListner(data.get(position).uri);
                    ((ViewHolder2)holder).aroundVideoView.setOnJobCompletionListner(new JobCompletionListner() {
                        @Override
                        public void onJobCompletion(String text1) {


                            DatabaseManager.updateMediaTableFromGlobalUrl(data.get(pos).uri,text1);

                            data.get(pos).uri=text1;
                            //((ViewHolder1)holder).aroundImageView.disableDownloadClick();
                            notifyItemChanged(pos);


                        }
                    });

                }
                else
                {
                     Log.e("MSG","content available");
                    ((ViewHolder2)holder).aroundVideoView.setLocallyAvailable(true);
                    ((ViewHolder2)holder).aroundVideoView.setUpVideoUri( Uri.parse(data.get(position).uri));

                    //((ViewHolder1)holder).aroundImageView.setDownloadClickListner("https://firebasestorage.googleapis.com/v0/b/aroundsocialmedia77.appspot.com/o/feed_datas%2Fk5GGMdHoOIcBDWJBqmKaOSLAC7s1%2Fzttaqmubddlcfiwwmgqy.png?alt=media&token=a5d2d64b-f7fa-4ea6-9a49-cbb22a5f7754");
                }

                /*final VideoView vidtemp=((ViewHolder2)holder).video;


                if(data.get(position).uri.equals("null"))
                {
                    ((ViewHolder2)holder).imageOverlay.setImageURI(Uri.parse(data.get(position).semi_uri));
                    ((ViewHolder2)holder).video.setVisibility(View.INVISIBLE);
                    ((ViewHolder2)holder).imageOverlay.setVisibility(View.VISIBLE);
                }
                else
                {
                    ((ViewHolder2)holder).video.setVideoURI(Uri.parse(data.get(pos).uri));
                    ((ViewHolder2)holder).imageOverlay.setVisibility(View.INVISIBLE);
                    ((ViewHolder2)holder).video.setVisibility(View.VISIBLE);
                }


                vidtemp.seekTo(10);*/
                ((ViewHolder2)holder).cv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //startinividual();
                        Context con=v.getContext();
                        //Toast.makeText(con, "Happesns", Toast.LENGTH_SHORT).show();
                        Intent indi=new Intent(con,FeedFullScreen.class);
                        con.startActivity(indi);
                    }
                });

               /* ((ViewHolder2)holder).video.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        //Toast.makeText(mContext, data.get(pos).text, Toast.LENGTH_SHORT).show();

                        //vidtemp.start();
                        return true;
                    }
                });*/

                       // startinividual(v.getContext());




                ((ViewHolder2) holder).tx.setText(data.get(position).text);
                final TextView p2=((ViewHolder2)holder).tx;
                ((ViewHolder2) holder).imgexpand.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // p.animate().setMaxLines(1).setDuration(500);
                        p2.setMaxLines(10);

                        Toast.makeText(mContext,data.get(pos).text, Toast.LENGTH_SHORT).show();
                        //Intent in=new Intent(mContext, Profile_Activity.class);

                        // mContext.startActivity(in);
                    }
                });

                ((ViewHolder2) holder).cTimeText.setText(getTimeFromMils(data.get(pos).timestamp));
                /*Video Ends Here*/
                break;

        }

    }

    /*boolean checkContentIsLocalyAvailable(String val)
    {
        if(val.split(">>").length>1)
            return true;
        else
            return false;
    }
    String convertUrlsToUri(String val)
    {
        if(val.split(">>").length>1)
            return val.split(">>")[1];
        else
            return val.split(">>")[0];
    }*/

    void shareSelectedPost(final FeedData feed)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage("Share post?");
        // builder.setTitle("Delete feed?");
        builder.setCancelable(true);
        builder.setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("SHARE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                sharePost(feed,mContext);

                dialog.dismiss();

            }
        });

        AlertDialog alert = builder.create();
        alert.show();


    }

    void setupprogress(LinearLayout progressLayout,int seen,int unseen)
    {
        LayoutInflater vi = (LayoutInflater) mContext.getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ProgressBar progressView;
        progressLayout.removeAllViews();

        for(int i=0;i<seen;i++)
        {


            View v = vi.inflate(R.layout.progress_bar_round, null);
            progressView=(ProgressBar) v.findViewById(R.id.feedProgress);
            //ProgressBar pp=progressView;
            progressView.setLayoutParams(new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,1.0f));

            progressView.setProgress(1);


            progressLayout.addView(progressView);

        }
        for(int i=0;i<unseen;i++)
        {


            View v = vi.inflate(R.layout.progress_bar_round, null);
            progressView=(ProgressBar) v.findViewById(R.id.feedProgress);
            //ProgressBar pp=progressView;
            progressView.setLayoutParams(new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,1.0f));

            progressView.setProgress(0);


           progressLayout.addView(progressView);

        }
    }
    String getTimeFromMils(long msgTime)
    {
        long curTime=System.currentTimeMillis();
        Date dt=new Date(msgTime);
        SimpleDateFormat format;
        if((curTime-86400000)<msgTime) {
            format = new SimpleDateFormat("hh:mm a");
            return format.format(dt);
        }
        else if((curTime-172800000)<msgTime)
        {
            // format = new SimpleDateFormat("EEE MMM dd hh:mm:ss zzz yyyy a");
            return "YESTERDAY";
        }
        else
        {
            format = new SimpleDateFormat("EEE MMM dd");
            return format.format(dt);
        }
       // return "";
    }


    @Override
    public int getItemViewType(int position) {
        switch(data.get(position).type)
        {
            case 0:
                return 0;

            case 1:
                return 1;

            case 2:
                return 2;

        }
        return 4;
    }

    @Override
    public int getItemCount() {
        if(data!=null) {
            return data.size();
        }
        return 0;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo contextMenuInfo) {

//mContext.getApplicationContext()
        menu.clear();
        MenuInflater inflater = new MenuInflater(mContext);
        inflater.inflate(R.menu.feed_pop_up, menu);
        menu.setHeaderTitle(data.get(optionPosition).username);
        menu.setQwertyMode(true);
        if(data.get(optionPosition).userid.equals(TabbedActivity.userdetail.getUid())) {

            menu.findItem(R.id.block_post).setVisible(false);
            menu.findItem(R.id.report_post).setVisible(false);
            menu.findItem(R.id.comment_now).setVisible(false);
            menu.findItem(R.id.view_profile).setTitle("My profile");
        }
       /* menu..setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                Toast.makeText(mContext, "hey", Toast.LENGTH_SHORT).show();
                return true;
            }
        });*/
        //menu.setHeaderTitle("Select The Action");
        //menu.add(0, v.getId(), 0, "Call");
       // menu.add(0, v.getId(), 0, "SMS");
    }




    public static class ViewHolder0 extends RecyclerView.ViewHolder {
        public CardView cv;
        public TextView tx;
        public CircularImageView proimg;
        public TextView userName;
        public TextView cTimeText;
        LinearLayout progressLayout;
        public ViewHolder0(View item) {
            super(item);
            cv=(CardView)item.findViewById(R.id.cView);
            tx=(TextView)item.findViewById(R.id.cText);
            proimg=(CircularImageView)item.findViewById(R.id.cProfileImage);
            userName=(TextView)item.findViewById(R.id.cUserNameText);
            cTimeText=(TextView)item.findViewById(R.id.cTimeText);

            progressLayout=(LinearLayout)item.findViewById(R.id.progressLayout);
        }
    }


    public static class ViewHolder1 extends RecyclerView.ViewHolder {
        public CardView cv;
        public TextView tx;
        public ImageView img;
        public CircularImageView proimg;
        public ImageButton imgexpand;
        public TextView userName;
        public TextView cTimeText;
        LinearLayout progressLayout;


        //DownloadProgressBar downloadProgressBar;
        AroundDownloadImageView aroundImageView;
        //FreshDownloadView fhView;

        LinearLayout respondPanel;

        ImageButton popup_menu;
        public ViewHolder1(View item) {
            super(item);
            cv=(CardView)item.findViewById(R.id.cView);
            tx=(TextView)item.findViewById(R.id.cText);

            proimg=(CircularImageView) item.findViewById(R.id.cProfileImage);
            imgexpand=(ImageButton)item.findViewById(R.id.button_toggle);
            userName=(TextView)item.findViewById(R.id.cUserNameText);
            cTimeText=(TextView)item.findViewById(R.id.cTimeText);
            progressLayout=(LinearLayout)item.findViewById(R.id.progressLayout);
            popup_menu=(ImageButton)item.findViewById(R.id.popup_menu);

            respondPanel=(LinearLayout) item.findViewById(R.id.respondPanel);


            //mElasticDownloadView=(ElasticDownloadView)item.findViewById(R.id.elastic_download_view);
           // downloadProgressBar = (DownloadProgressBar)item.findViewById(R.id.download_progress_view);
            aroundImageView=(AroundDownloadImageView) item.findViewById(R.id.ccImage);
            img=aroundImageView.getImageView();
            //fhView=aroundImageView.getDownloadView();


        }
    }


    public static class ViewHolder2 extends RecyclerView.ViewHolder {
        public CardView cv;
        public TextView tx;
       // public VideoView video;
        public CircularImageView proimg;
        //public ImageView imageOverlay;

        public ImageButton imgexpand;
        public TextView userName;
        public TextView cTimeText;

        AroundDownloadVideoView aroundVideoView;

        public ViewHolder2(View item) {
            super(item);
            cv=(CardView)item.findViewById(R.id.cView);
            tx=(TextView)item.findViewById(R.id.cText);
            //video=(VideoView) item.findViewById(R.id.feedVideo);
            proimg=(CircularImageView) item.findViewById(R.id.cProfileImage);
            //imageOverlay=(ImageView) item.findViewById(R.id.imageOverlay);
            imgexpand=(ImageButton)item.findViewById(R.id.button_toggle);
            userName=(TextView)item.findViewById(R.id.cUserNameText);
            cTimeText=(TextView)item.findViewById(R.id.cTimeText);

            aroundVideoView=(AroundDownloadVideoView) item.findViewById(R.id.feedVideo);

        }
    }

}


