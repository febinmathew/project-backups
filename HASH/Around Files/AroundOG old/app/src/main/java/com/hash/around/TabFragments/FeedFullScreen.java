package com.hash.around.TabFragments;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.carlosmuvi.segmentedprogressbar.SegmentedProgressBar;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.OtherClasses.DataTypes.FeedData;
import com.hash.around.Profile_Activity;
import com.hash.around.R;
import com.hash.around.TabbedActivity;
import com.hash.around.Views.Medias.AroundDownloadImageView;
import com.hash.around.Views.Medias.AroundDownloadVideoView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class FeedFullScreen extends AppCompatActivity {

Toolbar toolbar;
    VideoView videoView;
    SegmentedProgressBar sBar;
    ViewPager viewPager;
    ProgressBar progressView;
String userName,userId;
int initFeedNumner;
    int check=0,pos=0,lastPage;
    ArrayList<FeedData> finaldata=new ArrayList<FeedData>();
    TextView feeduserNameFull;
    LinearLayout progressLayout;
    SQLiteDatabase db;
    TextView timeText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feed_full_screen);
check=1;
        toolbar = (Toolbar) findViewById(R.id.comtoolbar);
//videoView.setOnTouchListener(new );
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //videoView=(VideoView)findViewById(R.id.videoViewMain);
        //sBar=(SegmentedProgressBar)findViewById(R.id.prog);

        feeduserNameFull=(TextView)findViewById(R.id.feeduserNameFull);

        timeText=(TextView)findViewById(R.id.feedFullTime);
        userName=getIntent().getStringExtra("username");

        userId=getIntent().getStringExtra("userid");

        initFeedNumner= getIntent().getIntExtra("feednumber",0);

        feeduserNameFull.setText(userName);
        db = TabbedActivity.db.getReadableDatabase();


         Cursor cursor = db.query(DatabaseAccesser.feed_table, new String[]{
                         DatabaseAccesser.feed_col_user_id ,
                                 DatabaseAccesser.feed_col_username ,
                                 DatabaseAccesser.feed_col_timestamp ,
                                 DatabaseAccesser.feed_col_type,
                                 DatabaseAccesser.feed_col_seen ,
                                 DatabaseAccesser.feed_col_text,
                                 DatabaseAccesser.feed_col_media_id},
                DatabaseAccesser.feed_col_user_id + " =?",
                new String[] { userId },
                null, null, DatabaseAccesser.feed_col_seen+" desc",null);

        /*Cursor cursor=db.rawQuery(" select "+
                DatabaseAccesser.feed_col_user_id + ", " +
                DatabaseAccesser.feed_col_username + ", "+
                DatabaseAccesser.feed_col_timestamp + ", "+
                DatabaseAccesser.feed_col_type + ", "+
                DatabaseAccesser.feed_col_seen + ", "+
                DatabaseAccesser.feed_col_text + ", "+
                DatabaseAccesser.feed_col_semi_url + ", "+
                DatabaseAccesser.feed_col_url + " from " +
                DatabaseAccesser.feed_table + " where "+DatabaseAccesser.feed_col_user_id+"='"+userId+"' order by "+DatabaseAccesser.feed_col_seen+" desc", null);*/

        int p=cursor.getCount();
        Toast.makeText(this, p+" "+userId, Toast.LENGTH_SHORT).show();

        while (cursor.moveToNext()) {
            FeedData temp = new FeedData();
            //FeedDataList temp2 = new FeedDataList();



            temp.userid = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_user_id));
            temp.username = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_username));
            temp.timestamp = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_timestamp));
            temp.type = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_type));

            temp.ifseen = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_seen));


            temp.text = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_text));


            long type = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_media_id));

            if(type==1)
            {
                temp.semi_uri=temp.uri="null";
            }
            else {

                Cursor cursor2 = db.query(DatabaseAccesser.media_table,
                        new String[]{
                                "rowid," +
                                        DatabaseAccesser.media_global_uri,
                                DatabaseAccesser.media_local_uri,
                                DatabaseAccesser.media_lite_global_uri,
                                DatabaseAccesser.media_lite_local_uri,
                        },
                        "rowid " + " =?",
                        new String[]{type + ""},
                        null, null, null, null);
                if (cursor2.getCount() == 1) {
                    cursor2.moveToNext();

                    temp.uri = cursor2.getString(
                            cursor2.getColumnIndexOrThrow(DatabaseAccesser.media_local_uri));
                    if (temp.uri.equals("null")) {
                        temp.uri = cursor2.getString(
                                cursor2.getColumnIndexOrThrow(DatabaseAccesser.media_global_uri));
                    }

                    temp.semi_uri = cursor2.getString(
                            cursor2.getColumnIndexOrThrow(DatabaseAccesser.media_lite_local_uri));

                }
            }

            finaldata.add(temp);
            if(check==1 && temp.ifseen==0)
            {
                pos=finaldata.size()-1;
            }
            check=temp.ifseen;

           }
        /*Uri videouri = Uri.parse("android.resource://com.hash.around" + "/"
                + R.raw.movie);*/
        /// videoView.setMediaController(new MediaController(getApplicationContext()));
        //  videoView.setVideoURI(videouri);

        // final VideoView p=videoView;
       /* videoView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                p.start();
                return true;
            }
        });*/
      /* Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        viewPager = (ViewPager) findViewById(R.id.feedViewPager);
        MyPagerAdapter mMyPagerAdapter=new MyPagerAdapter();

        viewPager.setAdapter(mMyPagerAdapter);

        if(initFeedNumner!=0)
            pos=initFeedNumner;

        viewPager.setCurrentItem(pos);
        lastPage=pos;
        timeText.setText(getTimeFromMils(finaldata.get(pos).timestamp));

        /*LayoutInflater vi = (LayoutInflater) getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View v = vi.inflate(R.layout.progress_bar_round, null);
        progressView=(ProgressBar)v.findViewById(R.id.feedProgress);*/

        progressLayout=(LinearLayout)findViewById(R.id.progressLayout);

        LayoutInflater vi = (LayoutInflater) getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for(int i=0;i<finaldata.size();i++)
        {


            View v = vi.inflate(R.layout.progress_bar_round, null);
            progressView=(ProgressBar) v.findViewById(R.id.feedProgress);
            //ProgressBar pp=progressView;
            progressView.setLayoutParams(new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,1.0f));
            if(finaldata.get(i).ifseen==1)
                progressView.setSecondaryProgress(0);
            else
                progressView.setSecondaryProgress(1);
            progressLayout.addView(progressView);

        }

        ((ProgressBar)progressLayout.getChildAt(pos)).setProgress(1);




        //sBar.setSegmentCount(mMyPagerAdapter.getCount());
        viewPager.setPageTransformer(true, new DepthPageTransformer());
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                //updateFeedViewed(position);
            }

            @Override
            public void onPageSelected(int position) {
                Toast.makeText(FeedFullScreen.this, "mm  "+lastPage, Toast.LENGTH_SHORT).show();
                if(lastPage>position)
                {
                    updateFeedViewed(lastPage);
                }
                else if(lastPage<position)
                        {
                            updateFeedViewed(lastPage);
                }

                ((ProgressBar)progressLayout.getChildAt(lastPage)).setProgress(0);
                ((ProgressBar)progressLayout.getChildAt(lastPage)).setSecondaryProgress(0);
                lastPage=position;
                ((ProgressBar)progressLayout.getChildAt(lastPage)).setProgress(1);



                timeText.setText(getTimeFromMils(finaldata.get(position).timestamp));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
    public void post_userDetailsClicked(View v)
    {
        Intent intent=new Intent(this, Profile_Activity.class);
        intent.putExtra("userid",userId);
        finish();
        startActivity(intent);
    }


    void updateFeedViewed(int position) {
        if (finaldata.get(position).ifseen == 0) {
            ContentValues values2 = new ContentValues();

            values2.put(DatabaseAccesser.feed_col_seen, 1);
        /*values2.put(DatabaseAccesser.chatList_lastmsgbyme, 0);
        values2.put(DatabaseAccesser.chatList_msgqueue, 1);
        values2.put(DatabaseAccesser.chatList_timestamp, System.currentTimeMillis());*/
            int rowln = db.update(DatabaseAccesser.feed_table, values2, DatabaseAccesser.feed_col_user_id + " =? AND " +
                    DatabaseAccesser.feed_col_timestamp + " = ?", new String[]{finaldata.get(position).userid, finaldata.get(position).timestamp + ""});
        }
    }

    public static class DepthPageTransformer implements ViewPager.PageTransformer {
        private static final float MIN_SCALE = 0.75f;

        public void transformPage(View view, float position) {
            int pageWidth = view.getWidth();

            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.setAlpha(0);

            } else if (position <= 0) { // [-1,0]
                // Use the default slide transition when moving to the left page
                view.setAlpha(1);
                view.setTranslationX(0);
                view.setScaleX(1);
                view.setScaleY(1);

            } else if (position <= 1) { // (0,1]
                // Fade the page out.
                view.setAlpha(1 - position);

                // Counteract the default slide transition
                view.setTranslationX(pageWidth * -position);

                // Scale the page down (between MIN_SCALE and 1)
                float scaleFactor = MIN_SCALE
                        + (1 - MIN_SCALE) * (1 - Math.abs(position));
                view.setScaleX(scaleFactor);
                view.setScaleY(scaleFactor);

            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.setAlpha(0);
            }
        }
    }
    @Override
    protected void onStart() {
        super.onStart();
      // videoView.start();
    }

    @Override
    protected void onDestroy() {
        updateFeedViewed(lastPage);
        Intent new_intent = new Intent();
        new_intent.setAction("FEED_UPDATED");
        sendBroadcast(new_intent);
        super.onDestroy();
    }

    class MyPagerAdapter extends PagerAdapter
    {

        public Object instantiateItem(ViewGroup collection, int position) {
            ViewGroup layout;
            int resId = 0;

           switch (finaldata.get(position).type) {
               case 0:

                   layout = (ViewGroup) getLayoutInflater().inflate(R.layout.view_text, null);
                   TextView viewTextTxt=(TextView) layout.findViewById(R.id.viewTextTxt);
                   viewTextTxt.setText(finaldata.get(position).text);
                   /*ImageView img=(ImageView)layout.findViewById(R.id.singleImage);
                    img.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(FeedFullScreen.this, "Clicked", Toast.LENGTH_SHORT).show();
                        }
                    });*/
                   break;
                case 1:
                    layout = (ViewGroup) getLayoutInflater().inflate(R.layout.view_image, null);

                    AroundDownloadImageView aroundImage;
                    aroundImage=layout.findViewById(R.id.singleAroundImageView);
                    aroundImage.setLocallyAvailable(true);
                    aroundImage.setImageViewUri(Uri.parse(finaldata.get(position).uri));

                    /*img.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(FeedFullScreen.this, "Clicked", Toast.LENGTH_SHORT).show();
                        }
                    });*/
                    break;
                case 2:
                    layout = (ViewGroup) getLayoutInflater().inflate(R.layout.view_gif,null);

                    break;
                case 3:
                    layout = (ViewGroup) getLayoutInflater().inflate(R.layout.view_video, null);

                    Uri videouri = Uri.parse("android.resource://com.hash.around"+ "/"
                            + R.raw.movie);
                    AroundDownloadVideoView aroundVideo;

                    aroundVideo=layout.findViewById(R.id.singleAroundVideoView);
                    aroundVideo.setLocallyAvailable(true);
                    aroundVideo.setUpVideoUri(videouri);
                    break;
                default:
                    layout = null;


            }


          //  LayoutInflater inflater = LayoutInflater.from(mContext);
           // ViewGroup layout = (ViewGroup) inflater.inflate(customPagerEnum.getLayoutResId(), collection, false);
           // collection.addView(layout);
           // layout = (ViewGroup) getLayoutInflater().inflate(R.layout.view_gif,null);

            collection.addView(layout);
            return layout; // return selected view.
        }

        @Override
        public int getCount() {
            return finaldata.size(); // number of maximum views in View Pager.
        }
        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((View) view);
        }



        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1; // return true if both are equal.
        }
    }


    public class SectionsPagerAdapter extends PagerAdapter {

        Context mContext;
        public SectionsPagerAdapter(Context mContext) {
           this.mContext=mContext;
        }

       /* @Override
        public Object instantiateItem(ViewGroup collection, int position) {
            View v = getLayoutInflater().inflate(R.layout.view_image, null);

            collection.addView(v);
            return v;
        }*/

        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((View) view);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return false;
        }


        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "CHATS";
                case 1:
                    return "FEEDS";
                case 2:
                    return "LOCATION";
            }
            return null;
        }

    }

    String getTimeFromMils(long msgTime)
    {
        long curTime=System.currentTimeMillis();
        Date dt=new Date(msgTime);
        SimpleDateFormat format;
        if((curTime-86400000)<msgTime) {
            format = new SimpleDateFormat("hh:mm a");
            return format.format(dt);
        }
        else if((curTime-172800000)<msgTime)
        {
            // format = new SimpleDateFormat("EEE MMM dd hh:mm:ss zzz yyyy a");
            return "YESTERDAY";
        }
        else
        {
            format = new SimpleDateFormat("EEE MMM dd");
            return format.format(dt);
        }
        // return "";
    }

}
