package com.hash.around.OtherClasses;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.FirebaseDatabase;
import com.hash.around.MediaViewer;
import com.hash.around.OtherClasses.DataTypes.GroupChatData;
import com.hash.around.OtherClasses.DataTypes.LocationData;
import com.hash.around.OtherClasses.Interfaces.AudioPlayerListener;
import com.hash.around.OtherClasses.Interfaces.ChatSentListner;
import com.hash.around.Views.CustomMakers.ChatView_ItemHeader;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.OtherClasses.Interfaces.JobCompletionWithFailureListner;
import com.hash.around.OtherClasses.Interfaces.ProcessCompletedListner;
import com.hash.around.R;
import com.hash.around.TabbedActivity;
import com.hash.around.Utils.ChatSender;
import com.hash.around.Utils.DatabaseManager;
import com.hash.around.Utils.MediaManager;
import com.hash.around.Utils.StringUtils;
import com.hash.around.Utils.UserManager;
import com.hash.around.Views.CircleColorImageView;
import com.hash.around.Views.Medias.AroundChatImageThumbline;
import com.hash.around.Views.TimerWithTextView;
import com.vanniktech.emoji.EmojiTextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


import static com.hash.around.TabbedActivity.checkLocationExists;
import static com.hash.around.TabbedActivity.mainStorageReference;
import static com.hash.around.TabbedActivity.saveLocationToDatabase;

import static com.hash.around.TabbedActivity.userdetail;

/**
 * Created by Febin on 08-10-2017.
 */

public class ChatDataAdapter extends RecyclerView.Adapter implements View.OnCreateContextMenuListener{



    Context mContext;
    public ArrayList<GroupChatData> data;
    //Activity act;
    public static int optionPosition=-1;

    public SparseBooleanArray selectedItems;
    boolean isChat=true;

    private ProcessCompletedListner onLoadMoreListener;
    private int visibleThreshold = 1;
    private int lastVisibleItem,prevlastVisibleItem=-2, totalItemCount;
    private boolean loading;

    public void toggleSelection(int pos) {
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
        }
        else {
            selectedItems.put(pos, true);
        }
        notifyItemChanged(pos);
    }

    public void clearSelections() {
        selectedItems.clear();
        notifyDataSetChanged();
    }


    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items =
                new ArrayList<Integer>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }

    public int deleteSelectedItems() {
        //List<Integer> items =new ArrayList<Integer>(selectedItems.size());
        ArrayList<Long> ids = new ArrayList<Long>();
        for (int i = selectedItems.size()-1; i >= 0; i--) {

            int pos=selectedItems.keyAt(i);
            ids.add(data.get(pos).sl);
            data.remove(selectedItems.keyAt(i));
        }

        String[] names=new String[ids.size()];
        for(int i=0;i<names.length;i++)
        {
            names[i]=ids.get(i)+"";
        }
        SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();
        String query = "DELETE FROM "+ DatabaseAccesser.chat_table+ " WHERE "+DatabaseAccesser.chat_sl_no+" IN (" + makePlaceholders(names.length) + ")";
        Cursor cursor = db.rawQuery(query, names);

        selectedItems.clear();
        notifyDataSetChanged();
        return cursor.getCount();
    }

    public void clearChatHistory(String userId)
    {
        SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();

        db.delete(DatabaseAccesser.chat_table, DatabaseAccesser.chat_userid + "=?", new String[]{userId});
        data.clear();

        ContentValues values2 = new ContentValues();

        values2.put(DatabaseAccesser.chatList_lastmsg, "");
        values2.put(DatabaseAccesser.chatList_lastmsg_type, 0);
        values2.put(DatabaseAccesser.chatList_msgqueue, 0);

        int rowln=db.update(DatabaseAccesser.chatList_table,values2, DatabaseAccesser.chatList_userid+" =?",new String[]{userId});
        //db.delete(DatabaseAccesser.chatList_table, DatabaseAccesser.chatList_userid + "=?", new String[]{userId});
        notifyDataSetChanged();
    }


    String makePlaceholders(int len) {
        if (len < 1) {
            // It will lead to an invalid query anyway ..
            throw new RuntimeException("No placeholders");
        } else {
            StringBuilder sb = new StringBuilder(len * 2 - 1);
            sb.append("?");
            for (int i = 1; i < len; i++) {
                sb.append(",?");
            }
            return sb.toString();
        }
    }


    public ChatDataAdapter(Context mcont, ArrayList<GroupChatData> list,boolean chat,RecyclerView recyclerView)
    {

        mContext=mcont;
        data=list;
       // act=activity;
        selectedItems=new SparseBooleanArray();
        isChat=chat;
        final Context ccc=mContext;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                .getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView,
                                   int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();
                 if (lastVisibleItem <=0) {
                // End has been reached
                     if(!loading && prevlastVisibleItem!=lastVisibleItem) {
                         prevlastVisibleItem = lastVisibleItem;
                         Toast.makeText(ccc, "chat : " + totalItemCount + " " + lastVisibleItem, Toast.LENGTH_SHORT).show();
                         // Do something
                         if (onLoadMoreListener != null) {
                             onLoadMoreListener.processCompleteAction();
                         }
                         //loading = true;
                     }
                }


            }
        });
    }

    /*public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }*/

    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        super.onViewRecycled(holder);
        ((TimerWithTextView)holder.itemView.findViewById(R.id.expireTimer)).stopAnimation();


    }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v=null;
        switch(viewType) {
            case 0:

                 v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_sent, parent, false);
                // set the view's size, margins, paddings and layout parameters

                TextChatView vh = new TextChatView(v);
                //vh.refreshExpireTimer(mContext,viewData);
                return vh;
            case 1:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_receive, parent, false);
                // set the view's size, margins, paddings and layout parameters

                ChatDataAdapter.TextChatView vh1 = new ChatDataAdapter.TextChatView(v);
                return vh1;
            case 2:

                v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_sent_image, parent, false);
                // set the view's size, margins, paddings and layout parameters

                ChatDataAdapter.ChatImageView vh2 = new ChatDataAdapter.ChatImageView(v);
                return vh2;
            case 3:

                v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_receive_image, parent, false);
                // set the view's size, margins, paddings and layout parameters

                ChatDataAdapter.ChatImageView vh6 = new ChatDataAdapter.ChatImageView(v);
                return vh6;
            //4-audiosent//5-audioreceive// 6-videosent//7-videoreceive//11-gifsent//12-gif receive
            // 13- filesent//14 filereceive//15-contactsent//16-contactreceive
            case 4:

                v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_sent_audio, parent, false);
                // sent audio

                ChatDataAdapter.AudioView vh9 = new ChatDataAdapter.AudioView(v);
                return vh9;

            case 5:

                v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_receive_audio, parent, false);
                // receive audio

                ChatDataAdapter.AudioView vh10 = new ChatDataAdapter.AudioView(v);
                return vh10;

           /* case 6:

                v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_sent_video, parent, false);
                // sent video

                ChatDataAdapter.ViewHolder5 vh7 = new ChatDataAdapter.ViewHolder5(v);
                return vh7;

            case 7:

                v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_receive_image, parent, false);
                // receive video

                ChatDataAdapter.ViewHolder5 vh8 = new ChatDataAdapter.ViewHolder5(v);
                return vh8;*/



            case 8:

                v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_sent_location, parent, false);
                // set the view's size, margins, paddings and layout parameters

                ChatDataAdapter.LocationView vh4 = new ChatDataAdapter.LocationView(v);
                return vh4;

            case 11:

                v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_sent_image, parent, false);
                // sent contact

                ChatDataAdapter.ChatImageView vh11 = new ChatDataAdapter.ChatImageView(v);
                return vh11;

            case 12:

                v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_receive_image, parent, false);
                // receive contact

                ChatDataAdapter.ChatImageView vh12 = new ChatDataAdapter.ChatImageView(v);
                return vh12;

            case 13:

                v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_sent_file, parent, false);
                // sent file

                ChatDataAdapter.DocumentView vh13 = new ChatDataAdapter.DocumentView(v);
                return vh13;
            case 14:

                v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_receive_file, parent, false);
                // receive file

                ChatDataAdapter.DocumentView vh14 = new ChatDataAdapter.DocumentView(v);
                return vh14;

            case 15:

                v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_sent_contacts, parent, false);
                // receive file

                ChatDataAdapter.ContactView vh15 = new ChatDataAdapter.ContactView(v);
                return vh15;

            case 16:

                v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_receive_contacts, parent, false);

                ChatDataAdapter.ContactView vh16= new ChatDataAdapter.ContactView(v);
                return vh16;

            case 20:

                v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_extra_info, parent, false);
                // set the view's size, margins, paddings and layout parameters

                ChatDataAdapter.ChatInfoView vh5 = new ChatDataAdapter.ChatInfoView(v);
                return vh5;

        }
        return null;
    }

    public void onBindDateItemDecorator(RecyclerView.ViewHolder holder, int position) {

        String dateText;
        long timestamp=data.get(position).timestamp;
        if (StringUtils.isToday(timestamp)) {
            dateText= "Today";
        } else if (StringUtils.isYesterday(timestamp)) {
            dateText= "Yesterday";
        } else {
             dateText = new SimpleDateFormat("EEE, MMM d, yyyy", Locale.ENGLISH).format(new Date(timestamp));
        }
        ((ChatView_ItemHeader.HeaderViewHolder)holder).textView.setText(dateText);
    }
    public void onBindUnseenItemDecorator(RecyclerView.ViewHolder holder, int position) {

        String dateText="5 UNSEEN MESSAGES";
        long timestamp=data.get(position).timestamp;
        /*if (StringUtils.isToday(timestamp)) {
            dateText= "Today";
        } else if (StringUtils.isYesterday(timestamp)) {
            dateText= "Yesterday";
        } else {
            dateText = new SimpleDateFormat("EEE, MMM d, yyyy", Locale.ENGLISH).format(new Date(timestamp));
        }*/
        ((ChatView_ItemHeader.HeaderViewHolder)holder).textView.setText(dateText);
    }
   //static Calendar lastTimerDate;
    public boolean hasTimeHeader(int position, boolean isReverse) {

        if (isReverse && position == data.size() - 1 )
            return false;
        else if(!isReverse && position == 0)
        {
            return true;
        }
            int previous = position + (isReverse ? 1 : -1);
            Calendar headerId = Calendar.getInstance();
            headerId.setTimeInMillis(data.get(position).timestamp);
            Calendar previousHeaderId = Calendar.getInstance();
        previousHeaderId.setTimeInMillis(data.get(previous).timestamp);

        //Date dt=new Date(data.get(position).timestamp);
        SimpleDateFormat format = new SimpleDateFormat("hh:mm a");
        //format.format(dt)


        Log.e("MSG","datedate: "+headerId.get(Calendar.DAY_OF_YEAR)+" / "+previousHeaderId.get(Calendar.DAY_OF_YEAR)+" || pos: "+data.get(position).msg+" / "+position+" - "+previous+" time: "+format.format(new Date(data.get(position).timestamp))+" / "+
                format.format(new Date(data.get(previous).timestamp)));


        if(/*lastTimerDate !=null && */headerId.get(Calendar.YEAR)==previousHeaderId.get(Calendar.YEAR) && headerId.get(Calendar.DAY_OF_YEAR)==previousHeaderId.get(Calendar.DAY_OF_YEAR))
        {
            return false;
        }
       return true;
    }


    public boolean hasUnseenHeader(int position,boolean isReverse,long timestamp)
    {
        /*if (isActiveCursor()) {
            return false;
        }

        if (lastSeenTimestamp <= 0) {
            return false;
        }*/

        if (isReverse && position == data.size() - 1 )
            return false;
        else if(!isReverse && position == 0)
        {
            return false;
        }

        int sender=data.get(position).sender;
        long currentSeen  = data.get(position).timestamp;
        long previousSeen = data.get(position-1).timestamp;

        return currentSeen>timestamp && previousSeen<=timestamp;
        //return (currentSeen != 3 && sender==1) && previousSeen == 3;
    }


    static class LastSeenHeader extends ChatView_ItemHeader {

        public LastSeenHeader(Context context, ChatDataAdapter adap) {
            super(context, adap);
        }



    }
        @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        final int pos=position;
        Date dt=new Date(data.get(position).timestamp);
        SimpleDateFormat format = new SimpleDateFormat("hh:mm a");

        if(selectedItems.get(position))
            holder.itemView.setBackgroundColor( ContextCompat.getColor(mContext, R.color.chat_selection));
        else
            holder.itemView.setBackgroundColor( ContextCompat.getColor(mContext, R.color.transparent));
        final GroupChatData viewData=data.get(pos);
        long curMilliSecond=System.currentTimeMillis();
        switch(data.get(position).sender) {
            case 0:
                switch(data.get(position).type){
                    case 0:
                        if(isChat)
                        {((TextChatView)holder).username.setVisibility(View.GONE);}
                        else
                            {
                            ((TextChatView) holder).username.setVisibility(View.VISIBLE);
                            ((TextChatView) holder).username.setText(data.get(pos).name);
                            ((TextChatView) holder).username.setOnCreateContextMenuListener(this);
                            ((TextChatView) holder).username.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    optionPosition = pos;
                                    view.showContextMenu();
                                }
                            });
                        }
                        ((TextChatView)holder).msg.setText(data.get(position).msg);
                        ((TextChatView)holder).date.setText(format.format(dt));

                        setMsgSeenStatus(((TextChatView)holder).msgStatus,data.get(position).msgseen);

                        Log.e("GGG","schedule "+data.get(position).msgseen);

                        refreshScheduleData( ((TextChatView)holder).msgScheduler,viewData);

                        ((TextChatView) holder).refreshExpireTimer(mContext,viewData);
                        //refreshExpireData( ((TextChatView)holder).expireTimer,viewData);


                        break;
                    case 1:
                        ((ChatImageView) holder).aroundImage.setMediaType(data.get(pos).type);
                        setMsgSeenStatus(((ChatImageView)holder).msgStatus,data.get(position).msgseen);
                        ((ChatImageView)holder).date.setText(format.format(dt));

                        if(data.get(pos).msg.equals("null"))
                            ((ChatImageView)holder).chatSentText.setVisibility(View.GONE);
                        else
                        ((ChatImageView)holder).chatSentText.setText(data.get(pos).msg);


                        if(data.get(position).final_url.contains("https") || data.get(position).final_url.contains("null")) {

                            ((ChatImageView) holder).aroundImage.setSizeText(StringUtils.mediaSizeToString(data.get(pos).mediaSize));
                           /* Glide.with(mContext).load(Uri.parse(data.get(position).temp_url))
                                    .thumbnail(0.4f)
                                    .crossFade()
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .into(((ChatImageView) holder).chatImage);*/
                            ((ChatImageView) holder).chatImage.setImageURI(Uri.parse(data.get(position).temp_url));

                            //uncomment this CODE
                            ((ChatImageView)holder).aroundImage.setUploadClickListner(data.get(position).temp_url,data.get(pos)
                                    ,data.get(position).media_id,mainStorageReference.child("chat_datas").child(userdetail.getUid()));

                            ((ChatImageView)holder).aroundImage.setOnFileUploadListener(new ChatSentListner() {
                                @Override
                                public void onChatAdded(GroupChatData data) {

                                }

                                @Override
                                public void onChatUploaded(GroupChatData data) {
                                    //data.get(pos).final_url=data.get(pos).temp_url;

                                    ((ChatImageView) holder).aroundImage.setLocallyAvailable(true);
                                }
                            });

                            }
                            else {

                            /*Glide.with(mContext).load(Uri.parse(data.get(position).final_url))
                                    .thumbnail(0.4f)
                                    .crossFade()
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .into(((ChatImageView) holder).chatImage);*/
                        ((ChatImageView) holder).chatImage.setImageURI(Uri.parse(data.get(position).final_url));

                            //CODE 47
                        ((ChatImageView)holder).aroundImage.disableDownloadClick();
                    }

                        ((ChatImageView)holder).chatImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view)
                            {
                                Intent intent = new Intent(mContext.getApplicationContext(), MediaViewer.class);
                                intent.putExtra("type", 5);
                                intent.putExtra("uri",data.get(pos).final_url);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                               /* ActivityOptionsCompat options = ActivityOptionsCompat
                                        .makeSceneTransitionAnimation(act,view, "robot");*/
                                mContext.startActivity(intent);
                            }
                        });
                    break;
                    case 2:

                        ((AudioView)holder).sizeText.setText(StringUtils.mediaSizeToString(data.get(pos).mediaSize));
                        setMsgSeenStatus(((AudioView)holder).msgStatus,data.get(position).msgseen);
                        ((AudioView)holder).date.setText(format.format(dt));

                        if(data.get(pos).msg.equals("null"))
                            ((AudioView)holder).chatSentText.setVisibility(View.GONE);
                        else
                            ((AudioView)holder).chatSentText.setText(data.get(pos).msg);

                        ((AudioView)holder).playPause.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                MediaManager.playAudio(mContext, data.get(pos).final_url,
                                        ((AudioView) holder).audioSeek, new AudioPlayerListener() {
                                            @Override
                                            public void onStarted() {
                                                Log.e("MGG","audioo startssssss ");
                                                ((AudioView)holder).playPause.setImageResource(R.drawable.ic_pause_accent);
                                            }

                                            @Override
                                            public void onPaused() {
                                                Log.e("MGG","audioo paused ");
                                                ((AudioView)holder).playPause.setImageResource(R.drawable.ic_play_arrow_white);
                                            }

                                            @Override
                                            public void onCompleted() {
                                                Log.e("MGG","audioo complted ");
                                                ((AudioView)holder).playPause.setImageResource(R.drawable.ic_play_arrow_white);
                                            }
                                        });
                            }
                        });

                        break;
                    case 3:


                        ((ChatImageView) holder).aroundImage.setMediaType(data.get(pos).type);
                        setMsgSeenStatus(((ChatImageView)holder).msgStatus,data.get(position).msgseen);
                        ((ChatImageView)holder).date.setText(format.format(dt));

                        if(data.get(pos).msg.equals("null"))
                            ((ChatImageView)holder).chatSentText.setVisibility(View.GONE);
                        else
                            ((ChatImageView)holder).chatSentText.setText(data.get(pos).msg);


                        Log.e("MGG","final url is "+Uri.parse(data.get(position).final_url));
                        Log.e("MGG","show url is "+Uri.parse(data.get(position).temp_url));
                        ((ChatImageView) holder).chatImage.setImageURI(Uri.parse(data.get(position).temp_url));

                        if(data.get(position).final_url.contains("https") || data.get(position).final_url.contains("null")) {


                            /*Glide.with(mContext).load(Uri.parse(data.get(position).temp_url))
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .into(((ChatImageView) holder).chatImage);*/


                            ((ChatImageView) holder).aroundImage.setSizeText(StringUtils.mediaSizeToString(data.get(pos).mediaSize));
                            ((ChatImageView)holder).aroundImage.setUploadClickListner(data.get(position).temp_url,data.get(pos)
                                    ,data.get(position).media_id,mainStorageReference.child("chat_datas").child(userdetail.getUid()));



                            //data.get(pos).final_url=data.get(pos).temp_url;

                            ((ChatImageView) holder).aroundImage.setLocallyAvailable(false);

                            ((ChatImageView)holder).aroundImage.setOnFileUploadListener(new ChatSentListner() {
                                @Override
                                public void onChatAdded(GroupChatData data) {

                                }

                                @Override
                                public void onChatUploaded(GroupChatData data) {
                                    ((ChatImageView) holder).aroundImage.setLocallyAvailable(true);
                                }
                            });
                        }
                        else
                        {

                           // File file = new File(Environment.getExternalStorageDirectory().getPath(), "Around Files/Media/cache");
                            //Log.e("MGG","new file : "+new File(file,"sfdfsfds.png").toURI().toString());

                            //((ChatImageView) holder).chatImage.setImageURI(Uri.parse(data.get(position).temp_url));
                            ((ChatImageView)holder).aroundImage.setLocallyAvailable(true);
                        }



                        break;
                    case 4:
                        setMsgSeenStatus(((LocationView)holder).msgStatus,data.get(position).msgseen);

                        ((LocationView)holder).locationText.setText(data.get(position).msg);

                        ((LocationView)holder).latitudeText.setText(data.get(position).temp_url+", "+data.get(position).final_url);



                        ((LocationView)holder).date.setText(format.format(dt));

                        ((LocationView)holder).addLocation.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                final LocationData temp =new LocationData();
                                temp.status=0;
                                temp.location_name=data.get(pos).msg;
                                temp.latitude=Double.parseDouble(data.get(pos).temp_url);
                                temp.longitude=Double.parseDouble(data.get(pos).final_url);

                                tempFun(temp,pos);


                            }
                        });
                        break;
                    case 5:
                        ((ChatImageView) holder).aroundImage.setMediaType(data.get(pos).type);
                        setMsgSeenStatus(((ChatImageView)holder).msgStatus,data.get(position).msgseen);
                        ((ChatImageView)holder).date.setText(format.format(dt));

                        if(data.get(pos).msg.equals("null"))
                            ((ChatImageView)holder).chatSentText.setVisibility(View.GONE);
                        else
                            ((ChatImageView)holder).chatSentText.setText(data.get(pos).msg);

                        if(data.get(position).final_url.contains("https") || data.get(position).final_url.contains("null")) {


                            ((ChatImageView) holder).chatImage.setImageURI(Uri.parse(data.get(position).temp_url));

                            ((ChatImageView) holder).aroundImage.setSizeText(StringUtils.mediaSizeToString(data.get(pos).mediaSize));
                            ((ChatImageView)holder).aroundImage.setUploadClickListner(data.get(position).temp_url,data.get(pos)
                                    ,data.get(position).media_id,mainStorageReference.child("chat_datas").child(userdetail.getUid()));



                            //data.get(pos).final_url=data.get(pos).temp_url;

                            ((ChatImageView) holder).aroundImage.setLocallyAvailable(false);

                            ((ChatImageView)holder).aroundImage.setOnFileUploadListener(new ChatSentListner() {
                                @Override
                                public void onChatAdded(GroupChatData data) {

                                }

                                @Override
                                public void onChatUploaded(GroupChatData data) {
                                    ((ChatImageView) holder).aroundImage.setLocallyAvailable(true);
                                }
                            });
                        }
                        else
                        {
                            ((ChatImageView) holder).chatImage.setImageURI(Uri.parse(data.get(position).temp_url));
                            ((ChatImageView)holder).aroundImage.setLocallyAvailable(true);
                        }


                        break;
                    case 6:
                        Log.e("MGG","filed name is "+data.get(pos).final_url);
                        String ext=MediaManager.getFileExtention(data.get(pos).final_url);
                        ((DocumentView)holder).sizeText.setText(ext+" | "+StringUtils.mediaSizeToString(data.get(pos).mediaSize));
                        setMsgSeenStatus(((DocumentView)holder).msgStatus,data.get(position).msgseen);
                        ((DocumentView)holder).date.setText(format.format(dt));
                        ((DocumentView)holder).indicationText.setText("."+ext);
                        ((DocumentView)holder).documentName.setText(MediaManager.getFileName(data.get(pos).final_url));

                        ((DocumentView)holder).chatBody.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                MediaManager.openAnyFile(mContext,data.get(pos).final_url);
                            }
                        });
                        //((DocumentView)holder).documentName.setText();
                        break;
                    case 7:
                        setMsgSeenStatus(((ContactView)holder).msgStatus,data.get(position).msgseen);
                        ((ContactView)holder).date.setText(format.format(dt));
                        ((ContactView)holder).contactName.setText(StringUtils.getNameFromVcard(data.get(pos).msg));
                        break;
                }
                break;
            case 1:
                if(viewData.msgseen==2)
                {
                    viewData.chat_seen_time=System.currentTimeMillis();
                    UserManager.updateLastReceiveTime(1, viewData.sl, FirebaseDatabase.getInstance().getReference().child("chat_status").child(viewData.userid).child(userdetail.getUid()), new JobCompletionWithFailureListner() {
                    @Override
                    public void onJobFailed(String txt1) {

                    }

                    @Override
                    public void onJobCompletion(String txt1) {
                        viewData.msgseen=3;
                        DatabaseManager.updateSelfChatstatus(viewData.chatRow,3);

                    }
                });}

                holder.itemView.findViewById(R.id.bubbleLayout).getBackground().setColorFilter(mContext.getResources().getColor(R.color.chatreceive_bubble), PorterDuff.Mode.MULTIPLY);
                switch(data.get(position).type){
                    case 0:

                        //((TextChatView) holder).background.getBackground().setColorFilter(ContextCompat.getColor(mContext, R.color.blue_500), PorterDuff.Mode.SRC_IN);
                        if(isChat)
                        {((TextChatView)holder).username.setVisibility(View.GONE);}
                        else
                        {
                            ((TextChatView) holder).username.setVisibility(View.VISIBLE);
                            ((TextChatView) holder).username.setText(data.get(pos).name);
                            ((TextChatView) holder).username.setOnCreateContextMenuListener(this);
                            ((TextChatView) holder).username.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    optionPosition = pos;
                                    view.showContextMenu();
                                }
                            });
                        }
                        ((TextChatView)holder).msg.setText(data.get(position).msg);
                        ((TextChatView)holder).date.setText(format.format(dt));
                        refreshScheduleData( ((TextChatView)holder).msgScheduler,viewData);
                        refreshExpireData( mContext,((TextChatView)holder).expireTimer,viewData);

                        break;
                    case 1:
                        ((ChatImageView)holder).date.setText(format.format(dt));

                        if(data.get(pos).msg.equals("null"))
                            ((ChatImageView)holder).chatSentText.setVisibility(View.GONE);
                        else
                            ((ChatImageView)holder).chatSentText.setText(data.get(pos).msg);
                        //((ChatImageView) holder).background.getBackground().setColorFilter(ContextCompat.getColor(mContext, R.color.blue_500), PorterDuff.Mode.SRC_IN);



                        Log.e("MSGG","final url is CC : "+data.get(position).final_url);
                        Log.e("MSGG","temp url is CC : "+data.get(position).temp_url);

                        if(data.get(position).final_url.contains("https")) {

                            ((ChatImageView) holder).aroundImage.setSizeText(StringUtils.mediaSizeToString(data.get(pos).mediaSize));
                            ((ChatImageView) holder).chatImage.setImageURI(Uri.parse(data.get(position).temp_url));
                            //CODE 47
                            ((ChatImageView)holder).aroundImage.setDownloadClickListner(data.get(position).final_url);
                            ((ChatImageView)holder).aroundImage.setOnJobCompletionListner(new JobCompletionListner() {
                                @Override
                                public void onJobCompletion(String text1) {


                                    DatabaseManager.updateMediaTableFromGlobalUrl(data.get(pos).final_url,text1);

                                    data.get(pos).final_url=text1;
                                    //((TextChatView)holder).aroundImageView.disableDownloadClick();
                                    notifyItemChanged(pos)

                                    ;


                                }
                            });

                        }
                        else {

                            ((ChatImageView) holder).chatImage.setImageURI(Uri.parse(data.get(position).final_url));

                            //CODE 47
                            ((ChatImageView)holder).aroundImage.setLocallyAvailable(true);
                        }


                        ((ChatImageView)holder).chatImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(mContext.getApplicationContext(), MediaViewer.class);
                                intent.putExtra("type", 5);
                                intent.putExtra("uri",data.get(pos).final_url);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                               /* ActivityOptionsCompat options = ActivityOptionsCompat
                                        .makeSceneTransitionAnimation(act,view, "robot");*/
                                mContext.startActivity(intent);
                            }
                        });
                        break;
                    case 3:
                        setUpVideoView(((ChatImageView)holder),data.get(pos).sender,data.get(pos));
                        ((ChatImageView)holder).date.setText(format.format(dt));
                        if(data.get(position).final_url.contains("https") || data.get(position).final_url.contains("null")) {
                            ((ChatImageView) holder).aroundImage.setSizeText(StringUtils.mediaSizeToString(data.get(pos).mediaSize));
                        }
                        else {
                            ((ChatImageView)holder).aroundImage.setLocallyAvailable(true);
                        }

                        break;
                    case 5:
                        ((ChatImageView) holder).aroundImage.setMediaType(data.get(pos).type);
                        //setMsgSeenStatus(((ChatImageView)holder).msgStatus,data.get(position).msgseen);
                        ((ChatImageView)holder).date.setText(format.format(dt));

                        if(data.get(pos).msg.equals("null"))
                            ((ChatImageView)holder).chatSentText.setVisibility(View.GONE);
                        else
                            ((ChatImageView)holder).chatSentText.setText(data.get(pos).msg);

                        ((ChatImageView) holder).chatImage.setImageURI(Uri.parse(data.get(position).temp_url));
                        if(data.get(position).final_url.contains("https") || data.get(position).final_url.contains("null")) {
                                                        ((ChatImageView) holder).aroundImage.setSizeText(StringUtils.mediaSizeToString(data.get(pos).mediaSize));
                        }
                        else
                        {
                            ((ChatImageView)holder).aroundImage.setLocallyAvailable(true);
                        }


                        break;
                    case 6:
                        Log.e("MGG","filed name is "+data.get(pos).final_url);
                        String ext=MediaManager.getFileExtention(data.get(pos).final_url);
                        ((DocumentView)holder).sizeText.setText(ext+" | "+StringUtils.mediaSizeToString(data.get(pos).mediaSize));
                        //setMsgSeenStatus(((DocumentView)holder).msgStatus,data.get(position).msgseen);
                        ((DocumentView)holder).date.setText(format.format(dt));
                        ((DocumentView)holder).indicationText.setText("."+ext);
                        ((DocumentView)holder).documentName.setText(MediaManager.getFileName(data.get(pos).final_url));

                        ((DocumentView)holder).chatBody.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                MediaManager.openAnyFile(mContext,data.get(pos).final_url);
                            }
                        });
                        //((DocumentView)holder).documentName.setText();
                        break;
                    case 7:
                        //setMsgSeenStatus(((ContactView)holder).msgStatus,data.get(position).msgseen);
                        ((ContactView)holder).date.setText(format.format(dt));
                        ((ContactView)holder).contactName.setText(StringUtils.getNameFromVcard(data.get(pos).msg));
                        break;
                }
                break;
        }


        switch(data.get(position).type)
        {
            case 21:
                if(data.get(position).msg.equals(data.get(position).userid))
                ((ChatInfoView)holder).info.setText(data.get(position).name+" joined");
                else
                    ((ChatInfoView)holder).info.setText(data.get(position).name+" added X");
                break;
            case 22:
                if(data.get(position).msg.equals(data.get(position).userid))
                    ((ChatInfoView)holder).info.setText(data.get(position).name+" left");
                else
                    ((ChatInfoView)holder).info.setText(data.get(position).name+" removed X");
                break;
        }
    }

    void setUpVideoView(ChatImageView holder, int sender, GroupChatData data2)
    {
        if(sender==0)
            setMsgSeenStatus(((ChatImageView)holder).msgStatus,data2.msgseen);

        ((ChatImageView) holder).aroundImage.setMediaType(data2.type);
        //((ChatImageView)holder).date.setText(format.format(dt));

        if(data2.msg.equals("null"))
            ((ChatImageView)holder).chatSentText.setVisibility(View.GONE);
        else
            ((ChatImageView)holder).chatSentText.setText(data2.msg);
        ((ChatImageView) holder).chatImage.setImageURI(Uri.parse(data2.temp_url));
    }

    void tempFun(final LocationData temp,int pos)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder( mContext);

        builder.setCancelable(true);

        if(!checkLocationExists(temp)) {
            builder.setMessage("Add "+data.get(pos).msg+"?");
            builder.setPositiveButton("ADD", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    saveLocationToDatabase(temp);
                    dialog.dismiss();
                }});
            builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

        }
        else
        {
            builder.setTitle("Location already exist!");
            builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public int getItemViewType(int position) {
        //4-audiosent//5-audioreceive// 6-videosent//7-videoreceive//11-gifsent//12-gif receive
        // 13- filesent//14 filereceive//15-contactsent//16-contactreceive
        switch(data.get(position).sender)
        {
            case 0:
                switch(data.get(position).type)
                {
                    //0 text//1 image //2 audio //3video //4location //5gif //6 file //7contact
                    case 0:
                        return 0;
                    case 1:
                        return 2;
                    case 2:
                        return 4;
                    case 3:
                        return 2;
                    case 4:
                        return 8;
                    case 5:
                        return 11;
                    case 6:
                        return 13;
                    case 7:
                        return 15;

                }
                //return 0;

            case 1:
                switch(data.get(position).type)
                {
                    case 0:
                        return 1;
                    case 1:
                        return 3;
                    case 2:
                        return 5;
                    case 3:
                        return 3;
                    case 4:
                        return 8;
                    case 5:
                        return 12;
                    case 6:
                        return 14;
                    case 7:
                        return 16;

                }
                //return 1;

        }
        switch(data.get(position).type) {
        case 21:
            case 22:
                return 20;
        }




        return Integer.parseInt(null);
    }

    @Override
    public int getItemCount()
    {
        if(data!=null) {
            return data.size();
        }
        return 0;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        menu.clear();
        MenuInflater inflater = new MenuInflater(mContext);
        inflater.inflate(R.menu.chatwindow_pop_up, menu);
        // menu.setHeaderTitle(data.get(optionPosition).name);
        //menu.setQwertyMode(true);
        if(data.get(optionPosition).userid.equals(TabbedActivity.userdetail.getUid())) {

            menu.findItem(R.id.chat_poke).setVisible(false);
            menu.findItem(R.id.chat_message).setVisible(false);

            menu.findItem(R.id.view_profile).setTitle("My profile");
        }
        else
        {
            menu.findItem(R.id.chat_message).setTitle("Message "+(data.get(optionPosition).name));
            menu.findItem(R.id.chat_poke).setTitle("Poke "+(data.get(optionPosition).name));
        }
    }
    void setMsgSeenStatus(ImageView stat,int val)
    {
        switch (val) {
            case 0:
                stat.setImageResource(R.drawable.ic_progress_clock);
                break;
            case 1:
                stat.setImageResource(R.drawable.ic_single_tick);
                break;
            case 2:
                stat.setImageResource(R.drawable.ic_double_tick);
                break;
            case 3:
                stat.setImageResource(R.drawable.ic_double_tick_seen_primary);
                break;
        }
    }
    void refreshScheduleData(final TimerWithTextView view,final GroupChatData chatData)
    {
        if(chatData.chatSchedule==0)
            view.setVisibility(View.GONE);
        else if(chatData.chatSchedule<=System.currentTimeMillis())
        {
            view.setVisibility(View.VISIBLE);
            view.setImageTint(R.color.semiblack_drawable);
            view.setTextVisibility(View.VISIBLE);
            view.stopAnimation();
            view.setText("<"+StringUtils.getShortExpirationDisplayValue((int)(System.currentTimeMillis()-chatData.chatSchedule)/1000));

        }
        else
        {
            view.setVisibility(View.VISIBLE);
            view.setTextVisibility(View.VISIBLE);

            view.setExpirationTime(null,chatData.chatSchedule);

            /*new NotifyTimer().addNewListnerWithIntervel(false,chatData.chatSchedule, new TimerListner() {
                @Override
                public void onTimerEnd(Long timestamp, String time) {

                }

                @Override
                public void onSpecifiedIntervel(Long timestamp, String time) {
                    view.setText(StringUtils.getShortExpirationDisplayValueForTimestamp(chatData.chatSchedule));
                }
            });*/

        }

    }

    public static void refreshExpireData(Context context,final TimerWithTextView view,final GroupChatData chatData)
    {

        if(chatData.chatExpire==0)
            view.setVisibility(View.GONE);
        else
        {
            view.setVisibility(View.VISIBLE);
            view.setTextVisibility(View.VISIBLE);
            //Log.e("MSGG","chat seentime "+chatData.chat_seen_time+ " / "+(chatData.sender==0 && chatData.chat_seen_time==0));
            if(chatData.sender==0 && chatData.chat_seen_time==0 ){
                    view.stopAnimation();
                    view.setImageTint(R.color.green_500);
                    view.setText(StringUtils.getShortExpirationDisplayValue((int)chatData.chatExpire));

            }
            else
            {
               /* view.setImageTint(R.color.red_500);
                view.setText(StringUtils.getShortExpirationDisplayValueForTimestamp(chatData.chatExpire*1000+chatData.chat_seen_time));
                Log.e("GGG","starting deletion"+((chatData.chatExpire*1000+chatData.chat_seen_time)-System.currentTimeMillis()));
                ChatSender.newMessageDeletionSchedule(mContext,chatData.chatExpire*1000+chatData.chat_seen_time,chatData.chatRow,chatData.userid);

                */
                view.setImageTint(R.color.red_500);
                /*new NotifyTimer().addNewListnerWithIntervel(false,(chatData.chatExpire*1000+chatData.chat_seen_time), new TimerListner() {
                    @Override
                    public void onTimerEnd(Long timestamp, String time) {

                    }

                    @Override
                    public void onSpecifiedIntervel(Long timestamp, String time) {
                        view.setText(StringUtils.getShortExpirationDisplayValueForTimestamp(chatData.chatExpire*1000+chatData.chat_seen_time));

                    }
                });*/
                view.setExpirationTime(null,chatData.chatExpire*1000+chatData.chat_seen_time);
                Log.e("MSGG","starting deletion"+((chatData.chatExpire*1000+chatData.chat_seen_time)-System.currentTimeMillis()));

                ChatSender.newMessageDeletionSchedule(context,chatData.chatExpire*1000+chatData.chat_seen_time,chatData.chatRow,chatData.userid);
            }
            /*view.setVisibility(View.VISIBLE);
            view.setTextVisibility(View.VISIBLE);
            view.setImageTint(R.color.red_500);
            view.setText(StringUtils.getShortExpirationDisplayValueForTimestamp(chatData.chatExpire));*/
        }
    }

    public static class TextChatView extends RecyclerView.ViewHolder {
       
        TimerWithTextView msgScheduler,expireTimer;

        public ImageView msgStatus;
        public EmojiTextView msg;
        public TextView date;
        public TextView username;
        RelativeLayout bubbleLayout;

        public TextChatView(View item) {
            super(item);
            bubbleLayout=item.findViewById(R.id.bubbleLayout);
            msgStatus=(ImageView)item.findViewById(R.id.statusBubble);
            msg=(EmojiTextView) item.findViewById(R.id.chatText);
            date=(TextView) item.findViewById(R.id.chatDateText);
            username=(TextView) item.findViewById(R.id.grpusername);
            msgScheduler=item.findViewById(R.id.msgScheduler);
            expireTimer=item.findViewById(R.id.expireTimer);

        }
        void refreshExpireTimer(Context context,final GroupChatData chatData) {
            ChatDataAdapter.refreshExpireData(context, expireTimer, chatData);
        }
    }
    

    public static class ChatImageView extends RecyclerView.ViewHolder {

        public ImageView chatImage;
        public AroundChatImageThumbline aroundImage;
        public ImageView msgStatus;
        public TextView date;
        public EmojiTextView chatSentText;
        //public RelativeLayout background;

        public ChatImageView(View item) {
            super(item);
            //background=item.findViewById(R.id.background);
            msgStatus=(ImageView)item.findViewById(R.id.statusBubble);
            aroundImage=item.findViewById(R.id.chatMainImageView);
            chatImage=aroundImage.getImageView();
            msgStatus=(ImageView)item.findViewById(R.id.statusBubble);
            date=(TextView) item.findViewById(R.id.chatDateText);
            chatSentText=item.findViewById(R.id.chatText);
        }
    }

    public static class LocationView extends RecyclerView.ViewHolder {

        public TextView locationText;
        public TextView latitudeText;
        public ImageView msgStatus;
        public TextView date;
        public TextView addLocation;
       // public TextView date;
        //public EmojiconTextView msg;

        public LocationView(View item) {
            super(item);

            locationText=(TextView)item.findViewById(R.id.locationText);
            latitudeText=(TextView)item.findViewById(R.id.latitudeText);
            addLocation=(TextView)item.findViewById(R.id.addLocation);
            msgStatus=(ImageView)item.findViewById(R.id.statusBubble);
            date=(TextView) item.findViewById(R.id.chatDateText);


        }
    }

    public static class ContactView extends RecyclerView.ViewHolder {

        TextView contactName;
        TextView numContacts;
        TextView contactNum;
        public ImageView msgStatus;
        public TextView date;

        public ContactView(View item) {
            super(item);
            msgStatus=(ImageView)item.findViewById(R.id.statusBubble);
            date=(TextView) item.findViewById(R.id.chatDateText);
            contactName=item.findViewById(R.id.contactName);
        }
    }
    public static class AudioView extends RecyclerView.ViewHolder {


        public ImageView msgStatus;
        public TextView date,sizeText;
        public EmojiTextView chatSentText;
        public CircleColorImageView playPause;
        public SeekBar audioSeek;

        public AudioView(View item) {
            super(item);
            msgStatus=(ImageView)item.findViewById(R.id.statusBubble);
            date=(TextView) item.findViewById(R.id.chatDateText);
            chatSentText=item.findViewById(R.id.chatText);
            sizeText=item.findViewById(R.id.sizeText);
            playPause=item.findViewById(R.id.playPause);
            audioSeek=item.findViewById(R.id.audioSeek);

        }
    }
    public static class DocumentView extends RecyclerView.ViewHolder {


        public ImageView msgStatus;
        public TextView date,sizeText,indicationText,documentName;
        public RelativeLayout chatBody;

        public DocumentView(View item) {
            super(item);
            msgStatus=(ImageView)item.findViewById(R.id.statusBubble);
            date=(TextView) item.findViewById(R.id.chatDateText);
            sizeText=item.findViewById(R.id.sizeText);
            indicationText=item.findViewById(R.id.indicationText);
            documentName=item.findViewById(R.id.documentName);
            chatBody=item.findViewById(R.id.chatBody);

        }
    }
    public static class ChatInfoView extends RecyclerView.ViewHolder {

        public TextView info;


        public ChatInfoView(View item) {
            super(item);

            info = (TextView) item.findViewById(R.id.chat_info_text);


        }
    }



}