package com.hash.around.OtherClasses;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hash.around.ChatWindow;
import com.hash.around.R;
import com.hash.around.TabbedActivity;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

//import android.support.v7.view.ActionMode;

/**
 * Created by Febin on 26-08-2017.
 */

public class Adapter_EditAnyText extends RecyclerView.Adapter<Adapter_EditAnyText.ViewHolder>  implements View.OnCreateContextMenuListener{

    Context mContext;
    public ArrayList<String> data;


    public static int optionPosition=-1;

    //Activity activ;
    public Adapter_EditAnyText(Context mcont, ArrayList<String> list)
    {
        //activ=x;
        mContext=mcont;
        data=list;



    }

    void deleteFullchat(String userId)
    {
        SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();
       // String query ="DELETE FROM "+ DatabaseAccesser.chat_table+ " WHERE "+DatabaseAccesser.chat_userid+"=? ";
       // String query2 = "DELETE FROM "+ DatabaseAccesser.chatList_table+ " WHERE "+DatabaseAccesser.chatList_userid+"=? ";

        db.delete(DatabaseAccesser.chat_table, DatabaseAccesser.chat_userid + "=?", new String[]{userId});
        db.delete(DatabaseAccesser.chatList_table, DatabaseAccesser.chatList_userid + "=?", new String[]{userId});
        //db.rawQuery(query, new String[]{userId});
        // cursor = db.rawQuery(query2, new String[]{userId});
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_text_edit_any_text, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {


        holder.itemView.setOnCreateContextMenuListener(this);

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {

                optionPosition=position;
                v.showContextMenu();
                return true;
            }
        });
        holder.textContent.setText(data.get(position));

    }


    @Override
    public int getItemCount() {
        if(data!=null) {
            return data.size();
        }
        return 0;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.clear();
        MenuInflater inflater = new MenuInflater(mContext);
        inflater.inflate(R.menu.edit_any_text, menu);


        //menu.findItem(R.id.member_admin).setVisible(false);
        //menu.findItem(R.id.member_remove).setVisible(false);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView textContent;


        public Drawable temp=null;
        public ViewHolder(View item)
        {
            super(item);

            textContent=(TextView)item.findViewById(R.id.textContent);



        }
    }

}


