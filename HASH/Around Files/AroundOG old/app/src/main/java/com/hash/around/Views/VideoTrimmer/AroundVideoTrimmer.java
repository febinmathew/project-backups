package com.hash.around.Views.VideoTrimmer;


import android.content.Context;
import android.database.Cursor;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.hash.around.OtherClasses.Interfaces.JobCompletionWithFailureListner;
import com.hash.around.Profile_Activity;
import com.hash.around.R;
import com.hash.around.Views.CursorRecyclerViewAdapter;
import com.hash.around.Views.RecentPhotosLoader;
import com.hash.around.Views.SquareFrameLayout;

import java.io.File;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/*import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.MediaStoreSignature;*/
/*import org.thoughtcrime.securesms.database.CursorRecyclerViewAdapter;
import org.thoughtcrime.securesms.database.loaders.RecentPhotosLoader;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.util.ViewUtil;*/

public class AroundVideoTrimmer extends RelativeLayout{
  VideoTimeLineView mTimeLineView;

  AroundRangeSlider range_slider;
  public AroundVideoTrimmer(Context context) {
    this(context, null);
  }

  public AroundVideoTrimmer(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public AroundVideoTrimmer(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);

    inflate(context, R.layout.around_video_trimmer, this);

    range_slider=(AroundRangeSlider)findViewById(R.id.range_slider);


  }

public void setTrimmerUri(Uri uri)
{
  mTimeLineView = ((VideoTimeLineView) findViewById(R.id.timeLineView));
  mTimeLineView.setVideo(uri);
}

public void setRangeSlideListener(RangeSliderListner listner)
{
  range_slider.addOnRangeSlideListener(listner);
}

VideoView uploadVideo;
public void setUpVideo(VideoView video)
{
  uploadVideo=video;
  setRangeSlideListener(new RangeSliderListner() {
    @Override
    public void onCreate(AroundRangeSlider rangeSeekBarView, int index, float value) {

    }

    @Override
    public void onSeek(AroundRangeSlider rangeSeekBarView, int index, float value) {

    }

    @Override
    public void onSeekStart(AroundRangeSlider rangeSeekBarView, int index, float value) {

    }

    @Override
    public void onSeekStop(AroundRangeSlider rangeSeekBarView, int index, float value) {

    }

    @Override
    public void onRangeChange(AroundRangeSlider rangeSeekBarView, float index, float value) {
      Log.e("MSG","index: "+index);
      if(uploadVideo!=null)
      {

         int tempseekStartPosInVideo=(int)(index*uploadVideo.getDuration())/100;
         seekEndPosInVideo=(int)(value*uploadVideo.getDuration())/100;

         float posss=(tempseekStartPosInVideo*1.0f/uploadVideo.getDuration())*100;

        Log.e("MSG","move to : "+tempseekStartPosInVideo+" /indes is "+index+" /reindex "+posss);
        if(seekStartPosInVideo!=tempseekStartPosInVideo) {
          seekStartPosInVideo=tempseekStartPosInVideo;

          if(uploadVideo.getCurrentPosition()<seekStartPosInVideo) {
            uploadVideo.seekTo(seekStartPosInVideo);
            range_slider.setSeekerIndex(index);
            Log.e("MSG", "success to : " + uploadVideo.getCurrentPosition());
            seekchange = true;
          }
        }

        if(seekEndPosInVideo<=uploadVideo.getCurrentPosition()) {
          uploadVideo.seekTo(seekEndPosInVideo);
          uploadVideo.pause();
        }


      }

    }
  });

   mediaPlayertask = new TimerTask() {
    @Override
    public void run() {

      //Log.e("MGG","changing  now");
      if (uploadVideo.isPlaying()) {

        /*if(uploadVideo.getCurrentPosition()>=seekEndPosInVideo)
        {
          uploadVideo.seekTo(seekStartPosInVideo);
          uploadVideo.pause();

        }*/
        if(uploadVideo.getCurrentPosition()!=seekStartPosInVideo && seekchange) {
          uploadVideo.seekTo(seekStartPosInVideo);
          seekchange=false;
        }
        float index=uploadVideo.getCurrentPosition()*1.0f/uploadVideo.getDuration()*100.0f;

        Log.e("MSG","currpos "+uploadVideo.getCurrentPosition()+" /indexwww is "+index);
        sliderPos(index);
       //float pos=((float)uploadVideo.getCurrentPosition()/mediaFileLengthInMilliseconds)*100;
       // bfmusicSeekbar.setProgress((int)pos);
        Log.e("MGG","installing to ");//+mediaPlayer.getCurrentPosition()+ " scroll is "+bfmusicSeekbar.getProgress());
      }

    }
  };

  mediaPlayerTimer = new Timer();

  //mediaPlayerTimer.schedule(mediaPlayertask, 0, 5);
  //MyAsync temp=new MyAsync();
  //temp.execute();
}



  private class MyAsync extends AsyncTask<Void, Integer, Void>
  {
    int duration = 0;
    int current = 0;
    @Override
    protected Void doInBackground(Void... params) {

      //uploadVideo.start();
      uploadVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

        public void onPrepared(MediaPlayer mp) {
          duration = uploadVideo.getDuration();
        }
      });

      do {
        current = uploadVideo.getCurrentPosition();
        //System.out.println("duration - " + duration + " current- "
                //+ current);
        try {
          publishProgress((int) (current * 100 / duration));
          /*if(mProgressBar.getProgress() >= 100){
            break;
          }*/
        } catch (Exception e) {
        }
      } while (true);

      //return null;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
      super.onProgressUpdate(values);
      //System.out.println(values[0]);
      //mProgressBar.setProgress(values[0]);
      Log.e("MSG", "currpos ");
      //if (uploadVideo.isPlaying()) {
        if (uploadVideo.getCurrentPosition() != seekStartPosInVideo && seekchange) {
          uploadVideo.seekTo(seekStartPosInVideo);
          seekchange = false;
        }
        float index = uploadVideo.getCurrentPosition() * 1.0f / uploadVideo.getDuration() * 100.0f;

        Log.e("MSG", "currpos " + uploadVideo.getCurrentPosition() + " /indexwww is " + index);
        sliderPos(index);
      }
    //}
  }


void sliderPos(float index)
{
  range_slider.setSeekerIndex(index);
}
  Timer mediaPlayerTimer;
  TimerTask  mediaPlayertask;
  int seekStartPosInVideo=0;
  int seekEndPosInVideo;
boolean seekchange;

  void trimVideo(Uri mSrc)
  {
    MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
    mediaMetadataRetriever.setDataSource(getContext(), mSrc);
    long METADATA_KEY_DURATION = Long.parseLong(mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));

    final File file = new File(mSrc.getPath());

    int mTimeVideo=seekEndPosInVideo-seekStartPosInVideo;
    if (mTimeVideo < 3000) {

      /*if ((METADATA_KEY_DURATION - mEndPosition) > (MIN_TIME_FRAME - mTimeVideo)) {
        mEndPosition += (MIN_TIME_FRAME - mTimeVideo);
      } else if (mStartPosition > (MIN_TIME_FRAME - mTimeVideo)) {
        mStartPosition -= (MIN_TIME_FRAME - mTimeVideo);
      }*/
      Log.e("MSG","sixe too low: ms : "+mTimeVideo);
    }

    //notify that video trimming started


    BackgroundExecutor.execute(
            new BackgroundExecutor.Task("", 0L, "") {
              @Override
              public void execute() {
                try {
                //  TrimVideoUtils.startTrim(file, getDestinationPath(), seekStartPosInVideo, seekEndPosInVideo, null);
                } catch (final Throwable e) {
                  Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), e);
                }
              }
            }
    );
  }

  private String getDestinationPath() {
    //if (mFinalPath == null) {
      File folder = Environment.getExternalStorageDirectory();
      String mFinalPath = folder.getPath() + File.separator;
      //Log.d(TAG, "Using default path " + mFinalPath);
   // }
    return mFinalPath;
  }
}
