package com.hash.around;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.github.vipulasri.timelineview.TimelineView;
import com.hash.around.OtherClasses.DataTypes.GroupChatData;
import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.Utils.AppOnlineStatusManager;
import com.hash.around.Utils.DatabaseManager;
import com.hash.around.Views.AroundChatInfoTimelineView;
import com.hash.around.Views.Medias.AroundCircularImageView;

public class SingleChatInfoActivity extends AppCompatActivity {

    long chatID;
    AroundChatInfoTimelineView sentTimeInfo,receiveTimeInfo,seenTimeInfo;
    TextView chatReceiverName,chatReceiverType;
    AroundCircularImageView chatReceiverImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_info_activity);
        sentTimeInfo=findViewById(R.id.sentTimeInfo);
        receiveTimeInfo=findViewById(R.id.receiveTimeInfo);
        seenTimeInfo=findViewById(R.id.seenTimeInfo);
        chatReceiverName=findViewById(R.id.chatReceiverName);
        chatReceiverImage=findViewById(R.id.chatReceiverImage);
        chatReceiverType=findViewById(R.id.chatReceiverType);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar ab=getSupportActionBar();

        if (ab != null) {
            ab.setTitle(null);
            ab.setDisplayShowTitleEnabled(true);
            ab.setTitle(R.string.chat_details);
            ab.setDisplayHomeAsUpEnabled(false);

        }
        findViewById(R.id.up_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SingleChatInfoActivity.super.onBackPressed();
            }
        });

        chatID=getIntent().getLongExtra("chatRow",0);
        GroupChatData chat=returnSingleChatFromDatabase(getApplicationContext(),chatID);
        setUpWithChat(chat);
        setUpWithUser(chat.sender,ChatWindow.returnSingleUserDetails(getApplicationContext(),chat.userid));
    }
    @Override
    protected void onStart() {
        super.onStart();
        AppOnlineStatusManager.OnlineStatus.setActive(true);

    }
    @Override
    public void onStop() {
        AppOnlineStatusManager.OnlineStatus.setActive(false);
        super.onStop();
    }

    void setUpWithChat(GroupChatData dat)
    {
        if(dat.sender==1) {
            sentTimeInfo.setData(dat.chat_rece_time,getString(R.string.chat_status_sent),1);
            receiveTimeInfo.setData(dat.timestamp,getString(R.string.chat_status_received),2);
        }
        else
            {
            sentTimeInfo.setData(dat.timestamp,getString(R.string.chat_status_sent),1);
                Log.e("GGG","rece time is "+dat.chat_rece_time);
            receiveTimeInfo.setData(dat.chat_rece_time,getString(R.string.chat_status_received),2);
        }

        seenTimeInfo.setData(dat.chat_seen_time,getString(R.string.chat_status_seen),3);

        if(dat.chat_seen_time!=0)
            seenTimeInfo.setAsLast();
        else if(dat.chat_rece_time!=0)
            receiveTimeInfo.setAsLast();
        else if(dat.timestamp!=0)
            sentTimeInfo.setAsLast();

    }

    void setUpWithUser(int sender,UserWholeData dat)
    {
        if(sender==0)
            chatReceiverType.setText(R.string.chat_sent_to);
        else
            chatReceiverType.setText(R.string.chat_received_from);

        if(dat==null)
        {
            chatReceiverName.setVisibility(View.GONE);
            chatReceiverImage.setVisibility(View.GONE);
            return;
        }
        chatReceiverName.setText(dat.user_name);
        chatReceiverImage.setImageURI(Uri.parse(dat.img_url));
    }

    public static GroupChatData returnSingleChatFromDatabase(Context context,long rowID)
    {
        //finaldata.clear();
        SQLiteDatabase db = DatabaseManager.getDatabase(context).getReadableDatabase();
        String userSelection;
        Cursor cursor;
        //userSelection = DatabaseAccesser.chat_userid;


        cursor= db.rawQuery(
                "SELECT *,"+DatabaseAccesser.chat_table+".rowid AS rowID"+" FROM "+DatabaseAccesser.chat_table +
                        " INNER JOIN "+DatabaseAccesser.user_table+" ON "+
                        DatabaseAccesser.chat_table+"."+DatabaseAccesser.chat_userid+" = "+ DatabaseAccesser.user_table+"."+DatabaseAccesser.user_user_id+
                        " INNER JOIN "+DatabaseAccesser.media_table+" ON "+
                        DatabaseAccesser.chat_table+"."+DatabaseAccesser.chat_col_media_id+" = "+ DatabaseAccesser.media_table+".rowid"+
                        " INNER JOIN "+DatabaseAccesser.replay_table+" ON "+
                        DatabaseAccesser.chat_table+"."+DatabaseAccesser.chat_col_replay_id+" = "+ DatabaseAccesser.replay_table+".rowid"+
                        " WHERE " +
                        DatabaseAccesser.chat_table+".rowid" + " = ?", new String[]{String.valueOf(rowID)});
        GroupChatData temp = new GroupChatData();

        if (cursor.moveToNext()) {


            temp.chatRow=cursor.getLong(
                    cursor.getColumnIndexOrThrow("rowID"));

            temp.sl = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_sl_no));
            temp.media_id=cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_col_media_id));

            temp.userid = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_userid));
            temp.sender = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_sender));

            temp.type = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_type));
            temp.timestamp = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_timestamp));
            temp.msg = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_text));

            temp.msgseen = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_ifseen));

            temp.mediaSize = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.media_size));


            temp.chat_rece_time= cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_rece_timestamp));
            temp.chat_seen_time= cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_seen_timestamp));


            temp.final_url = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.media_local_uri));
            if(temp.final_url.equals("null"))
            {
                temp.final_url = cursor.getString(
                        cursor.getColumnIndexOrThrow(DatabaseAccesser.media_global_uri));
            }
            temp.temp_url = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.media_lite_local_uri));

            //Log.e("GGG","saving Useris is "+temp.userid);
            if(temp.userid.equals(TabbedActivity.userdetail.getUid()))
                temp.name=context.getString(R.string.chat_me);
            else
                temp.name=cursor.getString(cursor.getColumnIndexOrThrow(DatabaseAccesser.user_name));

        }
        cursor.close();
        return temp;
    }
}
