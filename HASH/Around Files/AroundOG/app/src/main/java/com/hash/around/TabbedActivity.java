package com.hash.around;


import android.app.Activity;

import android.app.AlarmManager;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.PendingIntent;
import android.app.SearchManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v13.app.FragmentPagerAdapter;

import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.ActionMode;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.OtherClasses.DataTypes.LocationData;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.OtherClasses.Interfaces.JobCompletionWithFailureListner;
import com.hash.around.OtherClasses.Interfaces.LocationReceiver;
import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.hash.around.OtherClasses.Interfaces.ProcessCompletedListner;
import com.hash.around.OtherClasses.Interfaces.ScrollListener;
import com.hash.around.TabFragments.TabFragment1;
import com.hash.around.TabFragments.TabFragment2;
import com.hash.around.TabFragments.TabFragment3;
import com.hash.around.Utils.AppForegroundStateManager;
import com.hash.around.Utils.AppOnlineStatusManager;
import com.hash.around.Utils.DatabaseManager;
import com.hash.around.Utils.LanguageHelper;
import com.hash.around.Utils.PermissionManager;
import com.hash.around.Utils.StringUtils;
import com.hash.around.Views.CustomMakers.NotificationTag;
import com.hash.around.Views.Medias.AroundCircularImageView;
import com.hash.around.Views.ViewFunctions;
import com.vanniktech.emoji.EmojiManager;
import com.vanniktech.emoji.EmojiTextView;
import com.vanniktech.emoji.ios.IosEmojiProvider;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static com.hash.around.ChatWindow.returnSingleUserDetails;
import static com.hash.around.TabFragments.TabFragment2.PICK_IMAGE;
import static com.hash.around.Utils.DatabaseManager.getDatabase;


public class TabbedActivity extends AppCompatActivity implements ScrollListener/*,AppForegroundStateManager.OnAppForegroundStateChangeListener*/ {
    /*static {
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }*/

    public static final String TEXT_SEPARATOR="<<>>";
    public static final int PREF_IMAGE = 0;
    public static final int PREF_AUDIO = 1;
    public static final int PREF_VIDEO = 2;
    public static final int PREF_FIlE = 3;
    public static final int PREF_GIF = 4;


    public static int AROUND_FEED_RADIUS=3;
    private SectionsPagerAdapter mSectionsPagerAdapter;
public static ActionMode uniActionMode=null;
Menu mMenu;
    TabLayout tabLayout;
    private ViewPager mViewPager;
    //FloatingActionMenu floatActionMenu;
    TabFragment1 fragment1;
    static TabFragment2 fragment2;
    public static DatabaseAccesser db;
    public static StorageReference mainStorageReference;
    public static DatabaseReference fDatabase;
    public static int position;
   public static  SharedPreferences prefs;
   public static SharedPreferences.Editor editor;
    public static FirebaseUser userdetail;

    public static FirebaseUser getUserDetail()
    {
        if(userdetail==null)
            userdetail=FirebaseAuth.getInstance().getCurrentUser();
        return userdetail;
    }

    public static Double defaultLatitide;
    public static Double defaultLongitude;
    public static String UserName;
    DatabaseReference userRef;
    static DatabaseReference userOnlineRef;
    static ArrayList<String> userList=new ArrayList<String>();

    public static  int[] notificationLocation = new int[4];



    //public static LocationManager locationManager;
   // public static LocationListener defaultLocationlistner=null;

    public static Context mTabbedContext;


   // public static  View slideView;
//static ProcessCompletedListner processListner;






   /*public  static void fetchAndSaveNewUser(final String userID,
                                           @Nullable final ProcessCompletedListner processListner, @Nullable final Integer uType) {



        FirebaseDatabase.getInstance().getReference().child("user_profile").child(userID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                final UserWholeData contact = (UserWholeData) dataSnapshot.getValue(ContactListData.class);



                    StorageReference httpsReference = FirebaseStorage.getInstance().getReferenceFromUrl(contact.img_url);

                    httpsReference.getBytes(1024 * 1024).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                        @Override
                        public void onSuccess(byte[] bytes) {
                            File file = new File(Environment.getExternalStorageDirectory().getPath(), "Around Files/Profile Pictures");
                            if (!file.exists()) {
                                file.mkdirs();
                            }
                            //File tempFile = new File(file.getAbsolutePath(),"contact.vcf");

                            String name = contact.user_name + ".png";
                            FileOutputStream fos = null;
                            try {
                                fos = new FileOutputStream(new File(file.getAbsolutePath(), name));
                                fos.write(bytes);
                                fos.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            String globalUrl = contact.img_url;
                            contact.img_url = file.toURI().toString() + name;



                            if(uType>2)
                                contact.followType= (uType==null)? contact.numberType:uType;
                            else
                           contact.numberType= (uType==null)? contact.numberType:uType;

                            if(uType>2)
                            {


                            }

                            saveContactToDatabase((UserWholeData) contact, globalUrl,true);




                            //if(processListner!=null)
                                //processListner.newUserAdded(contact);

    }




                        /*Intent new_intent = new Intent();
                        new_intent.setAction("CHAT_UPDATED");
                        sendBroadcast(new_intent);*/



                          //});


           // }

            //@Override
            //public void onCancelled(DatabaseError databaseError) {

            //}

       // });
       // }
    // */



    static void saveContactMusicToDatabase(String userID,@Nullable String global,@Nullable String localUrl,Long time)
    {
        SQLiteDatabase db = DatabaseManager.getDatabase().getWritableDatabase();

        ContentValues values = new ContentValues();

        if(localUrl!=null)
            values.put(DatabaseAccesser.user_music_localUrl, localUrl);
        else
            values.put(DatabaseAccesser.user_music_localUrl, "null");

        if(global!=null)
            values.put(DatabaseAccesser.user_music_globalUrl, global);

      if(time!=null)
        values.put(DatabaseAccesser.user_music_timestamp, time);

        long newRowId = db.update(DatabaseAccesser.user_table, values, DatabaseAccesser.user_user_id + " =? ", new String[]{userID});

    }

   static void saveContactToDatabase(UserWholeData post, String globalUrl, boolean newEntry) {
       SQLiteDatabase db = DatabaseManager.getDatabase().getWritableDatabase();

       ContentValues values = new ContentValues();

       values.put(DatabaseAccesser.user_name, post.user_name);
       values.put(DatabaseAccesser.user_status, post.status);
       Log.e("GGG","statustime is1 "+post.status_timestamp);
       values.put(DatabaseAccesser.user_status_timestamp, post.status_timestamp);

       if (post.user_id.equals(FirebaseAuth.getInstance().getUid())) {
           values.put(DatabaseAccesser.user_user_type, 0);
       } else
           {
           values.put(DatabaseAccesser.user_user_type, post.type);
       }
       Log.e("GGG","uriuri save url: "+post.img_url);

       values.put(DatabaseAccesser.user_globalUrl, globalUrl);
       values.put(DatabaseAccesser.user_localUrl, post.img_url);
       Log.e("MSGG","ulrimate == "+post.number);
       values.put(DatabaseAccesser.user_number, post.number);
       values.put(DatabaseAccesser.user_timestamp, System.currentTimeMillis());
       if(post.remoteContact!=null)
       values.put(DatabaseAccesser.user_remote_contact,post.remoteContact);
       if(post.remoteBlock!=null)
       values.put(DatabaseAccesser.user_remote_block,post.remoteBlock);


       values.put(DatabaseAccesser.user_priv_activity_status, post.priv_activity_status);
       values.put(DatabaseAccesser.user_priv_profile_picture_status, post.priv_profile_picture);
       values.put(DatabaseAccesser.user_priv_status_status, post.priv_profile_status);
       values.put(DatabaseAccesser.user_priv_number_status, post.priv_number);
       values.put(DatabaseAccesser.user_priv_location_status, post.priv_location);
       values.put(DatabaseAccesser.user_priv_music_status, post.priv_profile_music);

       values.put(DatabaseAccesser.user_follow_privacy, post.priv_follow_privacy);
       values.put(DatabaseAccesser.user_chat_privacy, post.priv_chat_privacy);


       if(!newEntry)
       {
           long newRowId = db.update(DatabaseAccesser.user_table, values, DatabaseAccesser.user_user_id + " =? ", new String[]{post.user_id});

       }
       else
       {
           Log.e("MSGG","== insert");
               values.put(DatabaseAccesser.user_user_id, post.user_id);
               long newRowId = db.insert(DatabaseAccesser.user_table, null, values);
               Log.e("MSGG","inserted at: "+newRowId);

       }





    }

    static void updateContactStatusAndPrivacy(UserWholeData post)
    {
        SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.user_status, post.status);
        values.put(DatabaseAccesser.user_status_timestamp, post.status_timestamp);
        values.put(DatabaseAccesser.user_number, post.number);

        values.put(DatabaseAccesser.user_user_type, post.type);
        values.put(DatabaseAccesser.user_priv_activity_status, post.priv_activity_status);
        values.put(DatabaseAccesser.user_priv_profile_picture_status, post.priv_profile_picture);
        values.put(DatabaseAccesser.user_priv_status_status, post.priv_profile_status);
        values.put(DatabaseAccesser.user_priv_number_status, post.priv_number);
        values.put(DatabaseAccesser.user_priv_location_status, post.priv_location);
        values.put(DatabaseAccesser.user_priv_music_status, post.priv_profile_music);

        values.put(DatabaseAccesser.user_follow_privacy, post.priv_follow_privacy);
        values.put(DatabaseAccesser.user_chat_privacy, post.priv_chat_privacy);



        long newRowId = db.update(DatabaseAccesser.user_table, values, DatabaseAccesser.user_user_id + " =? ", new String[]{post.user_id});

        Log.e("MSGG","updated with prof status : "+ post.priv_profile_picture);

    }

    void checkIfUserDetailAvailableInDatabase()
    {
        SQLiteDatabase db = new DatabaseAccesser(getApplicationContext()).getReadableDatabase();

        Cursor cursor = db.query(DatabaseAccesser.user_table,
                new String[]{
                        DatabaseAccesser.user_user_id,
                        DatabaseAccesser.user_name},
                DatabaseAccesser.user_user_type + " =?",
                new String[]{"0"},
                null, null, null,null);

        if(cursor.getCount()==0)
        {
            Log.e("GGG","intentintent SetProfile 3");
            Intent intent = new Intent(this,SetProfile.class);
            TabbedActivity.this.finish();
            startActivity(intent);
            finish();
        }
        cursor.close();

       Log.e("MGG","no of users with 0: "+ cursor.getCount()+"");
    }

    public static long checkForMediaAvailablility(String global)
    {

        SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();

        Cursor cursor = db.query(DatabaseAccesser.media_table,
                new String[]{" rowid ",DatabaseAccesser.media_global_uri, DatabaseAccesser.media_local_uri, DatabaseAccesser.media_timestamp},
                DatabaseAccesser.media_global_uri + " =?",
                new String[]{global},
                null, null, null,null);

        if(cursor.getCount()==1)
        {
            cursor.moveToNext();
            long localURL = cursor.getLong(0);
            return localURL;
        }
        else {
            return 0;
        }
    }



    NotificationTag notifyTag;
    public static TextView[] tab_pendingTexts=new TextView[2];
    public TextView[] tabHeadings=new TextView[2];
    public UserWholeData userData;
    EmojiTextView accountName;
    AroundCircularImageView userImage;

    void performDailyCheck()
    {
        long lastCheckTimestamp=prefs.getLong("version_check_date",0);
        if(StringUtils.isToday(lastCheckTimestamp));
            //return;
        Log.e("GGG","version STARTS " );
        FirebaseDatabase.getInstance().getReference().child("around_info").
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {


                        if(dataSnapshot==null)
                            return;

                        Integer lastVersionStart=Integer.parseInt(dataSnapshot.child("last_version_start").getValue().toString());
                        Integer lastVersionEnd=Integer.parseInt(dataSnapshot.child("last_version_end").getValue().toString());
                        final String updateUrlLink=dataSnapshot.child("update_url_link").getValue().toString();

                        Log.e("GGG","version " +lastVersionStart+" / " +lastVersionEnd);
                        if(BuildConfig.VERSION_CODE<lastVersionStart){
                            updateMandatory(updateUrlLink);
                            Log.e("GGG","version immediate update1 "+BuildConfig.VERSION_CODE+" / "+lastVersionStart);}
                        else if(BuildConfig.VERSION_CODE<=lastVersionEnd) {
                            Long last_version_expiry=Long.parseLong(dataSnapshot.child("last_version_expiry").getValue().toString());
                           if(last_version_expiry<System.currentTimeMillis())
                           {
                               Log.e("GGG","version immediate update2 "+last_version_expiry+" / "+System.currentTimeMillis());
                               updateMandatory(updateUrlLink);
                               return;
                           }
                            String deatilText;
                            String headText=getApplicationContext().getResources().getString(R.string.version_outdated_text);
                            int daysleft=(int)((last_version_expiry-System.currentTimeMillis())/ TimeUnit.DAYS.toMillis(1));
                            if(daysleft==0||StringUtils.isToday(last_version_expiry))
                                deatilText=getApplicationContext().getResources().getString(R.string.version_expiration_text_details_today);
                            else{
                                //Log.e("GGG","version not ugent "+daysleft+" / "+((float)(last_version_expiry-System.currentTimeMillis()))+" / "+TimeUnit.DAYS.toMillis(1));
                                deatilText=getApplicationContext().getResources().getQuantityString(R.plurals.version_expiration_text_details,daysleft,daysleft);}
                            if(fragment1!=null)
                            {  // fragment1=((TabFragment1)mSectionsPagerAdapter.getItem(0));

                            fragment1.setUpInfoArea(
                                    headText, deatilText,
                                    new JobCompletionWithFailureListner() {
                                        @Override
                                        public void onJobFailed(String txt1) {

                                        }

                                        @Override
                                        public void onJobCompletion(String txt1) {
                                           // String url = "http://www.example.com";
                                            Intent i = new Intent(Intent.ACTION_VIEW);
                                            Log.e("GGG","linkloc "+updateUrlLink);
                                            i.setData(Uri.parse(updateUrlLink));
                                            startActivity(i);
                                        }
                                    }
                            );
                            }

                        }
                        else{
                        editor.putLong("version_check_date",System.currentTimeMillis());
                        editor.commit();}

                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }

                    void updateMandatory(final String upda)
                    {
                        AroundAlertBoxManager.showExtraInfoText(TabbedActivity.this, 1, getString(R.string.version_expiration_text_details_expired), getString(R.string.version_expiration_text_expired_mini), null,
                                new JobCompletionWithFailureListner() {
                                    @Override
                                    public void onJobFailed(String txt1) {
                                        finish();
                                    }

                                    @Override
                                    public void onJobCompletion(String txt1) {
                                        //String url = "http://www.example.com";
                                        Intent i = new Intent(Intent.ACTION_VIEW);
                                        i.setData(Uri.parse(upda));
                                        startActivity(i);
                                    }
                                });
                    }
                });
    }
    public void restartService() {
        Toast.makeText(getApplicationContext(), "removeing now", Toast.LENGTH_SHORT).show();
        /*Intent restartIntent = new Intent(this, MainServiceThread.class);
        PendingIntent restartServicePI = PendingIntent.getService(getApplicationContext(), 1, restartIntent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmService.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + 1000, restartServicePI);
        */
        Intent intent = new Intent(this, MainServiceThread.class);
        stopService(intent);
        Intent in = new Intent(getApplicationContext(), MainServiceThread.class);
        in.putExtra("userid", userdetail.getUid());
        startService(in);
        //new Intent(MyService.MY_SERVICE)
    }
    LanguageHelper languageHelper=new LanguageHelper();
    public static TabbedActivity instance;
    FloatingActionButton floatButtonNewChat;
    FloatingActionButton chatPeopleNearby;
    FloatingActionMenu floatMainMenu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        instance=this;
        super.onCreate(savedInstanceState);
        languageHelper.onStart(this);
        //getDatabase(this).resetDatabase();
        EmojiManager.install(new IosEmojiProvider());



       //FirebaseDatabase.getInstance().setPersistenceEnabled(true);


        db=new DatabaseAccesser(getApplicationContext());
        //db.resetDatabase();
        mTabbedContext=this;

        userdetail= FirebaseAuth.getInstance().getCurrentUser();

        if(userdetail==null)
        {
            Intent intent = new Intent(this,RegisterActivity.class);
            TabbedActivity.this.finish();
            startActivity(intent);
            finish();
            return;
        }
        else
        {
            userData=returnSingleUserDetails(getApplicationContext(),userdetail.getUid());
            if(userData==null)
            {
                checkIfUserDetailAvailableInDatabase();
                return ;
            }

            userRef=FirebaseDatabase.getInstance().getReference().child("user_profile").child(userdetail.getUid());
            userOnlineRef=FirebaseDatabase.getInstance().getReference().child("user_profile").child(userdetail.getUid()).child("online_status");
            userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(!dataSnapshot.exists() || dataSnapshot.child("user_name").getValue() ==null)
                    {
                        Log.e("GGG","intentintent SetProfile 2");
                        Intent intent = new Intent(getApplicationContext(),SetProfile.class);
                        TabbedActivity.this.finish();
                        startActivity(intent);
                    }
                    else
                    {
                        UserName=dataSnapshot.child("user_name").getValue().toString();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

        if(userdetail.getUid()!=null) {
            Thread t = new Thread() {
                public void run() {
                    Intent in = new Intent(getApplicationContext(), MainServiceThread.class);

                    in.putExtra("userid", userdetail.getUid());
                    startService(in);
               /*getApplicationContext().bindService(
                        new Intent(getApplicationContext(), MyAndroidUpnpServiceImpl.class),
                        serviceConnection,
                        Context.BIND_AUTO_CREATE
                );*/
                }
            };
            t.start();
        }

        setContentView(R.layout.tabbed_activity);

         prefs = getSharedPreferences("user_preferences", MODE_PRIVATE);
         editor = prefs.edit();

        performDailyCheck();

        defaultLatitide=Double.parseDouble(prefs.getString("location_latitude", "0.0"));
        defaultLongitude=Double.parseDouble(prefs.getString("location_longitude", "0.0"));

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //getSupportActionBar().setTitle("Around");
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //getSupportActionBar().setLogo(R.mipmap.logo1);


        //getSupportActionBar().setTitle("Around");
       // getSupportActionBar().setSubtitle("online");
        //getSupportActionBar().setDisplayShowTitleEnabled(false);

        //findViewById(R.id.topcheck).setMinimumWidth(findViewById(R.id.topcheck).getHeight());
        mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        //mViewPager.bringToFront();
        mViewPager.setAdapter(mSectionsPagerAdapter);
         notifyTag=findViewById(R.id.notificationTag);
        //notifyTag.notificationSection.setVisibility(View.GONE);
        //notifyTag.requestSection.setVisibility(View.GONE);
        notifyTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNotificationActivity();
            }
        });
       // mViewPager.setCurrentItem(1);
        //floatActionMenu=(FloatingActionMenu)findViewById(R.id.floatMainMenu);
        //checkIfUserDetailAvailableInDatabase();
        //checkIfUserDetailAvailableInDatabase();

        //Log.e("MGG","userdata is not null");
        floatButtonNewChat=(FloatingActionButton)findViewById(R.id.floatButtonNewChat);
        floatButtonNewChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mapIntent=new Intent(getApplicationContext(),AllUsersList.class);
                //  uploadIntent.putExtra("numberType",5);
                //uploadIntent.putExtra("uri",directory.toString());
                startActivity(mapIntent);

                /*ChromePopUpHelper chromeHelpPopup = new ChromePopUpHelper(getActivity(),"Hello!");
                chromeHelpPopup.show(v);*/


            }
        });

        chatPeopleNearby=(FloatingActionButton)findViewById(R.id.chatPeopleNearby);
        chatPeopleNearby.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                overridePendingTransition(R.anim.slide_up_anim,R.anim.no_change_anim);

                Intent allUsers=new Intent(getApplicationContext(),PeopleNearYou.class);
                //  uploadIntent.putExtra("numberType",5);
                //uploadIntent.putExtra("uri",directory.toString());

                startActivity(allUsers);

            }
        });

        floatMainMenu=findViewById(R.id.floatMainMenu);

        accountName=findViewById(R.id.accountName);
        userImage=findViewById(R.id.userImage);

        accountName.setText(userData.user_name);
        userImage.setImageURI(Uri.parse(userData.img_url));
        userImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent indi=new Intent(getApplicationContext(),Profile_Activity.class);
                indi.putExtra("userid",userData.user_id);
                startActivity(indi);
            }
        });
        mainStorageReference= FirebaseStorage.getInstance().getReference();
        fDatabase=FirebaseDatabase.getInstance().getReference();
        //checkoptions(0);
        //invalidateOptionsMenu(1);


        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
              //  Adapter_ChatList.mActionMode.finish();

            }

            @Override
            public void onPageSelected(int pos)
            {
                //invalidateOptionsMenu();
                testfunction(pos);
                position=pos;

                setFloatVisibility(pos);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);




        LinearLayout lLayout = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.single_custom_tabs_, null);
        tabHeadings[0]=lLayout.findViewById(R.id.tabHeading);
        tab_pendingTexts[0]=lLayout.findViewById(R.id.tab_pendingText);
        tabHeadings[0].setText(R.string.chats);
        tabHeadings[0].setText(" "+tabHeadings[0].getText());

        tabHeadings[0].setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_chat_bubble_white, 0, 0, 0);
        tabLayout.getTabAt(0).setCustomView(lLayout);



        lLayout = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.single_custom_tabs_, null);
        tabHeadings[1]=lLayout.findViewById(R.id.tabHeading);
        tab_pendingTexts[1]=lLayout.findViewById(R.id.tab_pendingText);
        tabHeadings[1].setText(R.string.feeds);
        tabHeadings[1].setText(" "+tabHeadings[1].getText());

        tabHeadings[1].setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_feeds_white, 0, 0, 0);
        tabLayout.getTabAt(1).setCustomView(lLayout);


        //setTabEnabled(tabLayout.getTabAt(tabLayout.getSelectedTabPosition()),false);

        /*LinearLayout tabLayoutt = null;

        tabLayoutt = (LinearLayout) ((ViewGroup) tabLayout.getChildAt(0)).getChildAt(0);
        TextView tabTextView = (TextView) tabLayoutt.getChildAt(1);
        tabTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_message_text_white, 0, 0, 0);
        tabTextView.setTypeface(tabTextView.getTypeface(), Typeface.BOLD);*/


        /*tabLayoutt = (LinearLayout) ((ViewGroup) tabLayout.getChildAt(0)).getChildAt(1);
        tabTextView = (TextView) tabLayoutt.getChildAt(1);
        tabTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_feeds_white, 0, 0, 0);
        tabTextView.setTypeface(tabTextView.getTypeface(), Typeface.BOLD);*/


        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                    setTabEnabled(tab,true);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

                setTabEnabled(tab,false);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        initializeTab(mViewPager.getCurrentItem());

        /*if(slideView==null)
            Toast.makeText(this, "doomed", Toast.LENGTH_SHORT).show();*/
        //Toast.makeText(getApplicationContext(), Uri.parse("https://www.w3schools.com/tags/movie.mp4").toString(), Toast.LENGTH_SHORT).show();
        handleIntent(getIntent());

        findViewById(R.id.upload_audio).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("audio/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_DEFAULT);
                startActivityForResult(Intent.createChooser(intent, "GET FILE"), 5);


            }
        });

        findViewById(R.id.upload_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_DEFAULT);
                startActivityForResult(Intent.createChooser(intent, "GET FILE"), 5);*/
                //UploadActivity.receiverAcitvity=2;
                Intent intent=new Intent(getApplicationContext(),GalleryActivity.class);
                intent.putExtra("mediaType",1);
                intent.putExtra("forChat", 2);
                startActivityForResult(intent,PICK_IMAGE);

            }
        });

        findViewById(R.id.upload_video).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent = new Intent();
                intent.setType("video/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_DEFAULT);
                startActivityForResult(Intent.createChooser(intent, "GET FILE"), 6);*/
                //UploadActivity.receiverAcitvity=2;
                Intent intent=new Intent(getApplicationContext(),GalleryActivity.class);
                intent.putExtra("mediaType",3);
                intent.putExtra("forChat", 2);

                startActivityForResult(intent,PICK_IMAGE);

            }
        });


        findViewById(R.id.upload_gif).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  Intent intent = new Intent();
                intent.setType("image/gif");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                startActivityForResult(Intent.createChooser(intent, "GET FILE"), 7);*/
                //UploadActivity.receiverAcitvity=2;
                Intent intent=new Intent(getApplicationContext(),GalleryActivity.class);
                intent.putExtra("mediaType",5);
                intent.putExtra("forChat",2);
                startActivityForResult(intent,PICK_IMAGE);
            }
        });


        findViewById(R.id.upload_text).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //UploadActivity.receiverAcitvity=2;
                Intent uploadIntent = new Intent(getApplicationContext(), UploadActivity.class);
                uploadIntent.putExtra("forChat", 2);
                //uploadIntent.putExtra("numberType", 4);
                //uploadIntent.putExtra("mediaarray", );
                startActivityForResult(uploadIntent, 15);
            }
        });

        //AppForegroundStateManager.getInstance().addListener(this);
    }

    /*@Override
    public void onAppForegroundStateChange(AppForegroundStateManager.AppForegroundState newState) {
        if (AppForegroundStateManager.AppForegroundState.IN_FOREGROUND == newState) {
            Log.e("GGG","AppForegroundState App just entered foreground");
        } else {
            Log.e("GGG","AppForegroundState App just entered BACKGROUND");
        }
    }*/
    @Override
    protected void onDestroy() {


        db.close();
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();

       // updateOnlineStatus();
       // AppForegroundStateManager.getInstance().onActivityVisible(this);

        //Log.e("GGG","onlinestatus CHANGE TABB ONSTART "+(true?"online":"offline"));
       // AppOnlineStatusManager.OnlineStatus.setActive(true);

    }
    @Override
    public void onStop() {
        //AppForegroundStateManager.getInstance().onActivityNotVisible(this);
        //Log.e("GGG","onlinestatus CHANGE TABB ONSTOP "+(false?"online":"offline"));
        //AppOnlineStatusManager.OnlineStatus.setActive(false);
        super.onStop();

    }

    /*@Override
    protected void onResume() {
        super.onResume();

    }*/

    @Override
    protected void onPause() {
        Log.e("GGG","onlinestatus CHANGE TABB ONSTOP "+(false?"online":"offline"));
        AppOnlineStatusManager.OnlineStatus.setActive(false);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.e("GGG","onlinestatus CHANGE TABB ONSTART "+(true?"online":"offline"));
        AppOnlineStatusManager.OnlineStatus.setActive(true);

        notifyTag.initializeWithVals();
        languageHelper.onReset(this);

        PermissionManager.checkRecordAudioPermission(this);
        PermissionManager.checkContactsPermission(this);
    }

    @Override
    public void onUserStatusReceieved(int pos, int scrollX, int scrollY, int oldScrollX, int oldScrollY)
    {
        if(scrollY>oldScrollY)
        {
            if(chatPeopleNearby.getVisibility()!=View.GONE)
                ViewFunctions.independantAnimateOut(chatPeopleNearby,getApplicationContext());
            if(floatButtonNewChat.getVisibility()!=View.GONE)
                ViewFunctions.independantAnimateOut(floatButtonNewChat,getApplicationContext());
            if(floatMainMenu.getVisibility()!=View.GONE)
                ViewFunctions.independantAnimateOut(floatMainMenu,getApplicationContext());
        }
        else if(scrollY<oldScrollY)
            setFloatVisibility(pos);
    }

    void setFloatVisibility(int pos)
    {
        if(pos==0)
        {
            if(chatPeopleNearby.getVisibility()!=View.VISIBLE)
                ViewFunctions.independantAnimateIn(chatPeopleNearby,getApplicationContext());
            if(floatButtonNewChat.getVisibility()!=View.VISIBLE)
                ViewFunctions.independantAnimateIn(floatButtonNewChat,getApplicationContext());
            if(floatMainMenu.getVisibility()!=View.GONE)
                ViewFunctions.independantAnimateOut(floatMainMenu,getApplicationContext());
        }
        else{

            if(chatPeopleNearby.getVisibility()!=View.GONE)
                ViewFunctions.independantAnimateOut(chatPeopleNearby,getApplicationContext());
            if(floatButtonNewChat.getVisibility()!=View.GONE)
                ViewFunctions.independantAnimateOut(floatButtonNewChat,getApplicationContext());
            if(floatMainMenu.getVisibility()!=View.VISIBLE)
                ViewFunctions.independantAnimateIn(floatMainMenu,getApplicationContext());
        }
    }
    private void showNotificationActivity()
    {
        Intent editText = new Intent(this, PopUp_Notification.class);


        startActivity(editText);
    }

    public void setTabEnabled(TabLayout.Tab tab,boolean enable)
    {
        int color;
        if(enable)
        {
            tabHeadings[tab.getPosition()].setTypeface(tabHeadings[tab.getPosition()].getTypeface(), Typeface.BOLD);
            color=Color.WHITE;//getResources().getColor(R.color.white);
        }
        else
        {
            tabHeadings[tab.getPosition()].setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
            color=getResources().getColor(R.color.lite_white);
        }

        tabHeadings[tab.getPosition()].setTextColor(color);
        Drawable dr= DrawableCompat.wrap(tabHeadings[tab.getPosition()].getCompoundDrawables()[0]);
        DrawableCompat.setTint(dr, color);
        DrawableCompat.setTintMode(dr, PorterDuff.Mode.SRC_IN);
        tabHeadings[tab.getPosition()].setCompoundDrawables(dr,null,null,null);
    }


    public void initializeTab(int defaultTab)
    {
        for (int i=0;i<tabLayout.getTabCount();i++)
        {
            if(defaultTab==i)
                setTabEnabled(tabLayout.getTabAt(i),true);
            else
            setTabEnabled(tabLayout.getTabAt(i),false);
        }

    }


    public static void removeAllLocationListners()
    {
       // locationManager.removeUpdates(defaultLocationlistner);
    }
    public static void requestGPS()
    {
        //Toast.makeText(mTabbedContext, , Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        mTabbedContext.startActivity(intent);
    }




    public static void updateOnlineStatus(boolean status)
    {
        HashMap map=new HashMap();

        map.put("status","online");
        map.put("timestamp",System.currentTimeMillis());


        if(!status){
       // HashMap mapoffline=new HashMap();
        map.put("status","offline");}
        //map.put("timestamp",System.currentTimeMillis());

        //userOnlineRef.onDisconnect().setValue(mapoffline);

        userOnlineRef.setValue(map);
    }





    @Override
    public void onBackPressed() {
        if(mViewPager.getCurrentItem()==1)
        {
            mViewPager.setCurrentItem(0,true);
            return;
        }
        super.onBackPressed();
        HashMap map=new HashMap();
        map.put("status","offline");
        map.put("timestamp",System.currentTimeMillis());
        userRef.child("online_status").setValue(map);

    }

    @Override
    protected void onNewIntent(Intent intent) {

        handleIntent(intent);
    }
    private void handleIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            Toast.makeText(this, query, Toast.LENGTH_SHORT).show();
            //use the query to search your data somehow
        }
    }

    public void testfunction(int pos)
    {

        //fragment1.updateActionMode();
        if(uniActionMode!=null)
        uniActionMode.finish();
        //floatActionMenu.hideMenuButton(true);
       // floatActionMenu.hideMenu(true);
        //floatActionMenu.child
    }
    private void checkoptions(int position) {
        mMenu.clear();
        switch(position)
        {
            case 0:
                getMenuInflater().inflate(R.menu.tabbed_chat_window, mMenu);
            break;
            case 1:
                getMenuInflater().inflate(R.menu.tabbed_feed_window, mMenu);
                break;
            case 2:
                getMenuInflater().inflate(R.menu.menu_register, mMenu);
                break;

        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        switch(position)
        {
            case 0:

           /* case 1:

                getMenuInflater().inflate(R.menu.tabbed_feed_window, menu);
                sItem=menu.findItem(R.id.action_search_tabbed);
                searchView =
                        (SearchView) sItem.getActionView();

                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {

                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        ((TabFragment2)mSectionsPagerAdapter.getItem(1)).searchText(newText,false);
                        return false;
                    }
                });

                SharedPreferences pref= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                Set<String> filterSet =pref.getStringSet("filter_feed", null);
                if((filterSet!=null) && (!filterSet.contains(MainServiceThread.PEOPLE_NEAR_FILTER+"")||
                        !filterSet.contains(MainServiceThread.FOLLOWING_PEOPLE_FILTER+"")||
                        !filterSet.contains(MainServiceThread.SELECTED_LOCATION_FILTER+"")))
                {
                    menu.findItem(R.id.action_feed_filter).getIcon().
                    setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.red_black), android.graphics.PorterDuff.Mode.SRC_IN);
                }
                else
                {
                    menu.findItem(R.id.action_feed_filter).getIcon().
                            setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.white), android.graphics.PorterDuff.Mode.SRC_IN);

                }
                break;*/

        }

        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent in=new Intent(getApplicationContext(), ProfileSettings.class);
            in.putExtra("userid",userdetail.getUid());
            startActivity(in);
            return true;
        }
        else if(id==R.id.action_search_tabbed)
        {
            // tabLayout.
            //tabLayout.setVisibility(View.GONE);
        }
        else if(id==R.id.action_feed_sort)
        {
            fragment2.sortFeeds();
        }
        else if(id==R.id.action_feed_filter)
        {
            if(fragment2!=null)
            fragment2.filterFeeds();
            else
                ((TabFragment2)mSectionsPagerAdapter.getItem(1)).filterFeeds();
        }
        else if(id==R.id.action_get_notification)
        {

            showNotificationActivity();
            View menuView = findViewById(R.id.action_get_notification);



            /*new PopUp_Notification().showNotificationDialog(this,getFragmentManager());*/
        }
        else if(id==R.id.action_openlocations)
        {
            Intent intent=new Intent(this,LocationsWindow.class);
            startActivity(intent);

        }
        /*else if(id==R.id.action_openpopup_notification)
        {
            AroundAlertBoxManager.showExtraInfoText(TabbedActivity.this,1,"Hey there",null,null,null);
        }*/
        else if(id==R.id.action_show_hidden_chats)
        {
            item.setChecked(!item.isChecked());

            TabFragment1.showHiddenChats=item.isChecked();
            fragment1.getDataFromDatabase();
            fragment1.adap.notifyDataSetChanged();
            if(item.isChecked()){

            }else{
            }




        }
        else if(id==R.id.action_around_delete)
        {

            new DatabaseAccesser(getApplicationContext()).resetDatabase();
            /*fetchNativeLocation(new LocationReceiver() {
                @Override
                public void onLocationReceived(Location data) {

                    Log.e("MGG","GOOT lati - "+data.getLatitude());
                    Log.e("MGG","GOOT longi - "+data.getLongitude());

                }
            },this,false);*/

            /*fetchFusedLocation(new LocationReceiver() {
                @Override
                public void onLocationReceived(Location data) {

                    Log.e("MGG","GOOT lati - "+data.getLatitude());
                    Log.e("MGG","GOOT longi - "+data.getLongitude());

                }
            },this);*/
            //db.getWritableDatabase().delete(DatabaseAccesser.user_table,DatabaseAccesser.user_user_type+" !=?",new String[]{"0"});
            //new DatabaseAccesser(getApplicationContext()).onUpgrade(new DatabaseAccesser(getApplicationContext()).getWritableDatabase(),1,2);
        }
        else if(id==R.id.action_opentest)
        {
           getDatabase(this).resetDatabase();
            //restartService();
            /*Intent intt=new Intent(this,TestActivity.class);
            startActivity(intt);*/
           /* Intent intent=new Intent(this,SettingsActivity.class);
            intent.putExtra("stat",2);
            intent.putExtra("languageonly",true);
            startActivity(intent);*/
           /* Log.e("GGG","contact2 checking " );
            MainServiceThread.fetchUserDetailsbyIDOrPhone("+917012153008",false, new ProcessCompletedListner() {
                @Override
                public void processCompleteAction() {}

                @Override
                public void newUserAdded(UserWholeData data) {
                    Log.e("GGG","contact2 USER fetched " + data.number);

                }
            });*/

            /*MainServiceThread.fetchRemoteUserStatusOfThisUser("k5GGMdHoOIcBDWJBqmKaOSLAC7s1", new JobCompletionListner() {
                @Override
                public void onJobCompletion(String txt1) {
                    Log.e("GGG","contact5  FETCHI data "+userData+" / ");

                }
            });*/

        }
        if (id == R.id.action_new_group)
        {
            Intent intent=new Intent(this,CreateNewGroup.class);
            intent.putExtra("type",15);
            startActivity(intent);
        }
        else if (id == R.id.action_new_channel)
        {
            Intent intent=new Intent(this,CreateNewGroup.class);
            intent.putExtra("type",16);
            startActivity(intent);
        }


        return super.onOptionsItemSelected(item);
    }




    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm)
        {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).

            switch (position)
            {
                case 0:
                    fragment1 = new TabFragment1();
                    return fragment1;

                case 1:
                    fragment2 = new TabFragment2();
                    return fragment2;

                case 2:
                    TabFragment3 fragment3 = new TabFragment3();
                    return fragment3;
                default:
                    //fragment=null;
                    break;

            }
        return null;

        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 2;
        }


        @Override
        public CharSequence getPageTitle(int position) {
            /*switch (position) {
                case 0:
                    return " CHATS ";
                case 1:
                    return " FEEDS ";
                case 2:
                    return "LOCATION";
            }*/
            return null;
        }
    }

    /*public void grabImage(View v)
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, "GET FILE"), 5);
           // CropImage.activity()
            //    .setGuidelines(CropImageView.Guidelines.ON)
             //   .start(this);



    }

    public void grabVideo(View v)
    {
        Intent intent = new Intent();
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, "GET FILE"), 6);

    }
    public void grabGif(View v)
    {
        Intent intent = new Intent();
        intent.setType("image/gif");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, "GET FILE"),7);

    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 5 && resultCode == RESULT_OK) {

            //Toast.makeText(this, "Working", Toast.LENGTH_SHORT).show();

            final Uri directory = data.getData();

            Intent cropIntent=new Intent(this,UploadActivity.class);
            cropIntent.putExtra("numberType",5);
            cropIntent.putExtra("uri",directory.toString());
            startActivity(cropIntent);



            //final Uri outputUri = Uri.fromFile(new File(getCacheDir(), "EditFile.png"));
           // Crop.of(directory, outputUri).start(this);
            //UCrop.of(directory,outputUri)
                    .start(TabbedActivity.this);


            //CropImage.activity(directory)
                  /// .start(this);

        }
        if (requestCode == 6 && resultCode == RESULT_OK) {

            //Toast.makeText(this, "Working", Toast.LENGTH_SHORT).show();

            final Uri directory = data.getData();

            Intent cropIntent = new Intent(this, UploadActivity.class);
            cropIntent.putExtra("numberType", 6);
            cropIntent.putExtra("uri", directory.toString());
            startActivityForResult(cropIntent, 1);
        }
        if (requestCode == 7 && resultCode == RESULT_OK) {

            //Toast.makeText(this, "Working", Toast.LENGTH_SHORT).show();

            final Uri directory = data.getData();

            Intent cropIntent = new Intent(this, UploadActivity.class);
            cropIntent.putExtra("numberType", 7);
            cropIntent.putExtra("uri", directory.toString());
            startActivityForResult(cropIntent, 1);
        }


    }*/
    public static boolean checkLocationExists(LocationData post)
    {
        SQLiteDatabase dbs = db.getReadableDatabase();

        Cursor cursor = dbs.rawQuery(" select * from " + DatabaseAccesser.location_table + " where " +
                DatabaseAccesser.loc_col_latitude + " = ? AND " +
                DatabaseAccesser.loc_col_longitude + " = ?", new String[]{post.latitude+"", post.longitude + ""});

        if (cursor.getCount() == 0)
        {
            return false;
        }
        return true;

    }

    public static LocationData formatLocationDataIntoDecimal(LocationData post)
    {
        post.latitude=Double.parseDouble(String.format( Locale.ROOT, "%.8f",post.latitude));
        post.longitude=Double.parseDouble(String.format(Locale.ROOT,"%.8f",post.longitude));
        return post;
    }
    public static void saveLocationToDatabase(Context context,LocationData post)
    {

        SQLiteDatabase dbs = DatabaseManager.getDatabase(context).getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.loc_col_location_name, post.location_name);
        values.put(DatabaseAccesser.loc_col_status, post.status);
        values.put(DatabaseAccesser.loc_col_latitude,post.latitude);
        values.put(DatabaseAccesser.loc_col_longitude, post.longitude);

        long newRowId = dbs.insert(DatabaseAccesser.location_table, null, values);
        Intent intent = new Intent("HOTSPOTS_UPDATED");
        context.sendBroadcast(intent);
    }


    static private FusedLocationProviderClient mFusedLocationClient;
    static LocationRequest mLocationRequest;
    static LocationCallback  mLocationCallback;
    public static void fetchFusedLocation(final LocationReceiver listner, Activity act)
    {

        mLocationCallback= new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                //super.onLocationResult(locationResult);
                Location location=locationResult.getLastLocation();
                mFusedLocationClient.removeLocationUpdates(mLocationCallback);

                if(listner!=null)
                listner.onLocationReceived(location);
            }
        };
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(act);

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        if (android.support.v4.app.ActivityCompat.checkSelfPermission(act, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && android.support.v4.app.ActivityCompat.checkSelfPermission(act, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Log.e("MGG","requesting");
        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                mLocationCallback,
                null /* Looper */);
    }

    static String provider;

    static LocationManager locationManager;
    static LocationListener listener;

    public static boolean isLocationEnabled(Context context) {
        if(locationManager==null)
            locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    public static void fetchNativeLocation(final LocationReceiver listner, final Activity act,final boolean loop)
    {

        if (ActivityCompat.checkSelfPermission(act, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mTabbedContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        if(locationManager==null)
            locationManager = (LocationManager) act.getSystemService(Context.LOCATION_SERVICE);

        listener = new LocationListener() {
            public void onStatusChanged(String provider, int status, Bundle extras) {
                Log.e("GGG","locationcheck change");
                //Toast.makeText(act, "Enthokkeyo", Toast.LENGTH_SHORT).show();
                // called when the location provider status changes. Possible status: OUT_OF_SERVICE, TEMPORARILY_UNAVAILABLE or AVAILABLE.
            }

            public void onProviderEnabled(String provider) {
                Log.e("GGG","locationcheck enabled");
                // called when the location provider is enabled by the user

                //Toast.makeText(act, "Enabled", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onProviderDisabled(String provider) {
                Log.e("GGG"," locationcheck disabled");
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                act.startActivity(intent);
            }

            @Override
            public void onLocationChanged(Location location) {
                Log.e("GGG"," locationcheck goooooto "+location.getLatitude()+" / "+location.getLongitude());
                if(listner!=null)
                    listner.onLocationReceived(location);
                if(!loop)
                {
                    locationManager.removeUpdates(this);
                }

            }
        };



        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        String provider=LocationManager.GPS_PROVIDER;
        if(!loop) {
            Log.e("GGG","locationcheck requesting");
            //locationManager.requestSingleUpdate(criteria, listener, null);
            Location temp=locationManager.getLastKnownLocation(provider);

            if(temp!=null)
            {
                if(listner!=null)
                    listner.onLocationReceived(temp);
            }
           /* else
            {

            }*/
                locationManager.requestLocationUpdates(10000,5,criteria,listener,null);
        }
        else
        locationManager.requestLocationUpdates(10000,5,criteria,listener,null);



    }
}
