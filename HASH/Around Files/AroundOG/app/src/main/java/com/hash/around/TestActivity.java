package com.hash.around;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.Utils.DatabaseManager;
import com.hash.around.Utils.LocalNotificationManager;
import com.hash.around.Utils.StringUtils;
import com.hash.around.Utils.UserManager;
import com.hash.around.Views.scribbles.widget.ScribbleView;
import com.hash.around.Views.scribbles.widget.VerticalSlideColorPicker;
import com.vanniktech.emoji.EmojiEditText;

public class TestActivity extends AppCompatActivity implements  VerticalSlideColorPicker.OnColorChangeListener {
    ImageButton popup_menu;
    private VerticalSlideColorPicker colorPicker;
    RelativeLayout background;
    private ScribbleView scribbleView;
    com.vanniktech.emoji.EmojiEditText chat_message;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_activity);
        this.colorPicker   = findViewById(R.id.scribble_color_picker);
        this.scribbleView  = findViewById(R.id.scribble_view);
        colorPicker.setOnColorChangeListener(this);
        background=findViewById(R.id.backgroundtest);
        scribbleView.setDrawingMode(true);
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar ab=getSupportActionBar();

        if (ab != null) {

            ab.setTitle(null);
            ab.setDisplayShowTitleEnabled(false);
            //ab.setDisplayShowCustomEnabled(false);
            ab.setDisplayHomeAsUpEnabled(false);
        }

        TestView2 cc=new TestView2(this);*/
        ComposeKeyPressedListener ccccc=new ComposeKeyPressedListener();
        Log.e("GGG","keypress1");
        chat_message=findViewById(R.id.chat_message);
        chat_message.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.e("GGG","keypress1 "+keyCode+"/ "+KeyEvent.KEYCODE_ENTER+"/ "+event.getAction()+"  / "+KeyEvent.ACTION_DOWN);
                return false;
            }
        });
        //chat_message.addTextChangedListener(ccccc);
        //chat_message.setOnClickListener(ccccc);
        chat_message.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                Log.e("GGG","keypress2");
                return false;
            }
        });


       // chat_message.setOnFocusChangeListener(ccccc);

    }
    @Override
    public void onColorChange(int color) {
        if (color == 0) color = Color.RED;
        background.setBackgroundColor(color);

       // toolbar.setToolColor(color);
        scribbleView.setDrawingBrushColor(color);

       // changeTextEntityColor(color);
    }

    public void bt1(View v)
    {
        LocalNotificationManager.customTestNOT(this);
        //DatabaseManager.dbLink=new DatabaseAccesser(this);
        //LocalNotificationManager.sentNewChatNotitication(this, UserManager.getUserInfo().getUid(),"message");
    }

    private class ComposeKeyPressedListener implements View.OnKeyListener, View.OnClickListener, TextWatcher, View.OnFocusChangeListener {

        int beforeLength;

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            Log.e("GGG","keypress1 "+keyCode+"/ "+KeyEvent.KEYCODE_ENTER+"/ "+event.getAction()+"  / "+KeyEvent.ACTION_DOWN);
            //Toast.makeText(TestActivity.this, "hey", Toast.LENGTH_SHORT).show();
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    /*if (TextSecurePreferences.isEnterSendsEnabled(ConversationActivity.this)) {
                        sendButton.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER));
                        sendButton.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_ENTER));
                        return true;
                    }*/
                }
            }
            return false;
        }

        @Override
        public void onClick(View v) {
            //container.showSoftkey(composeText);
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,int after) {
            //beforeLength = composeText.getTextTrimmed().length();
        }

        @Override
        public void afterTextChanged(Editable s) {
            /*calculateCharactersRemaining();

            if (composeText.getTextTrimmed().length() == 0 || beforeLength == 0) {
                composeText.postDelayed(ConversationActivity.this::updateToggleButtonState, 50);
            }*/
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before,int count) {}

        @Override
        public void onFocusChange(View v, boolean hasFocus) {}
    }

}
