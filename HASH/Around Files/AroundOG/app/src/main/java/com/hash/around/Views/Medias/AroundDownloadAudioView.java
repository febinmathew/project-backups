package com.hash.around.Views.Medias;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.audiofx.Visualizer;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.TextureView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnPausedListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.hash.around.OtherClasses.DataTypes.FeedData;
import com.hash.around.OtherClasses.DataTypes.GroupChatData;
import com.hash.around.OtherClasses.DataTypes.GroupFeedData;
import com.hash.around.OtherClasses.Interfaces.ChatSentListner;
import com.hash.around.OtherClasses.Interfaces.FeedSentListner;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.R;
import com.hash.around.Utils.AroundStaticInfo;
import com.hash.around.Utils.PermissionManager;
import com.hash.around.Utils.StringUtils;
import com.hash.around.Views.CustomMakers.AroundDownloadView;
import com.hash.around.Views.ViewFunctions;
import com.pitt.library.fresh.FreshDownloadView;

import java.io.File;
import java.io.IOException;


public class AroundDownloadAudioView extends RelativeLayout {



    TextView sizeText;

    View rootView;
    Context mContext;
    ImageView imageContent;

    AroundDownloadView downloadView;
    LinearLayout downloadViewBG;
    ImageView playButton;
    AroundVisualizerView visualizerView;
    JobCompletionListner jobComplete=null;



    private MediaPlayer mMediaPlayer;
    private Visualizer mVisualizer;


    public AroundDownloadAudioView(Context context) {
        super(context);
        init(context,null);
    }

    public AroundDownloadAudioView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context,attrs);
    }

    public AroundDownloadAudioView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context,attrs);
    }


    private void init(Context context, @Nullable AttributeSet attrs) {

        mContext=context;


        rootView = inflate(context, R.layout.around_audioview, this);

        imageContent = (ImageView) rootView.findViewById(R.id.imageContent);
        downloadView = rootView.findViewById(R.id.downloadView);
        visualizerView=rootView.findViewById(R.id.visualizerView);
        downloadViewBG=rootView.findViewById(R.id.downloadViewBG);
        sizeText=rootView.findViewById(R.id.sizeText);
        playButton=(ImageView) rootView.findViewById(R.id.playButton);


    }

    public void setLocallyAvailable(boolean locallyAvailable)
    {
        if(locallyAvailable)
        {
            disableDownloadClick();
        }
        else
        {
            enableDownloadClick();
        }
    }

    public void setUpAudioUri(Uri url)
    {

       // setVolumeControlStream(AudioManager.STREAM_MUSIC);
        mMediaPlayer = MediaPlayer.create(mContext, url);

        if(PermissionManager.checkRecordAudioPermission((Activity)getContext())){
        setupVisualizerFxAndUI();}

        // Make sure the visualizer is enabled only when you actually want to
        // receive data, and
        // when it makes sense to receive data.

        // When the stream ends, we don't need to collect any more data. We
        // don't do this in
        // setupVisualizerFxAndUI because we likely want to have more,
        // non-Visualizer related code
        // in this callback.
        mMediaPlayer
                .setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mediaPlayer) {
                        mVisualizer.setEnabled(false);
                        mMediaPlayer.seekTo(1);
                        pauseControl();
                    }
                });

        playButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mMediaPlayer.isPlaying())
                {
                    pauseControl();
                }
                    else
                {
                    mVisualizer.setEnabled(true);
                    playButton.setImageResource(R.drawable.ic_pause_circle_light_grey);
                    mMediaPlayer.start();
                }
            }
        });
    }

    void pauseControl()
    {
        playButton.setImageResource(R.drawable.ic_play_circle);
        mMediaPlayer.pause();
    }
    private void setupVisualizerFxAndUI() {

        if(mMediaPlayer==null)return;
        // Create the Visualizer object and attach it to our media player.
        mVisualizer = new Visualizer(mMediaPlayer.getAudioSessionId());
        mVisualizer.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
        mVisualizer.setDataCaptureListener(
                new Visualizer.OnDataCaptureListener() {
                    public void onWaveFormDataCapture(Visualizer visualizer,
                                                      byte[] bytes, int samplingRate) {
                        visualizerView.updateVisualizer(bytes);
                    }

                    public void onFftDataCapture(Visualizer visualizer,
                                                 byte[] bytes, int samplingRate) {
                    }
                }, Visualizer.getMaxCaptureRate() / 2, true, false);
    }

    public void disableDownloadClick()
    {
        Log.e("MSG","disable");
        downloadView.setVisibility(GONE);
        downloadViewBG.setVisibility(GONE);
        imageContent.setVisibility(GONE);
        playButton.setVisibility(VISIBLE);
        visualizerView.setVisibility(VISIBLE);
        sizeText.setVisibility(GONE);


    }
    public void setDownloadClickListner(String uri,int mediaType)
    {
        enableDownloadClick();
        SingleViewDownloadHandler.setDownloadClickListner(getContext(),uri,mediaType,0,downloadView,jobComplete);
    }
    public void enableDownloadClick()
    {
        //Log.e("MSG","disable");
        downloadView.setVisibility(VISIBLE);
        downloadViewBG.setVisibility(VISIBLE);
        imageContent.setVisibility(VISIBLE);
        playButton.setVisibility(GONE);
        visualizerView.setVisibility(GONE);
        sizeText.setVisibility(VISIBLE);
    }

    public void setSizeText(String text)
    {
        sizeText.setText(text);
    }
    public void setChatUploadClickListner(String uri, final GroupChatData data, final StorageReference ref) {
        enableDownloadClick();
        downloadView.setIsUpload();
        SingleViewDownloadHandler.setUploadChatClickListner(getContext(),uri,0,data,downloadView,ref,uploadListner);

    }
    ChatSentListner uploadListner = null;
    public void setOnFileUploadListener(ChatSentListner job) {
        this.uploadListner = job;
    }
    public void setOnJobCompletionListner(JobCompletionListner job)
    {
        this.jobComplete=job;
    }
    public void setUpChatMedia(GroupChatData data, JobCompletionListner listener)
    {
        // setMediaType(data.type);
        if(data.sender==0)
        {
            if(data.msgseen==0) {
                setSizeText(StringUtils.mediaSizeToString(data.mediaSize));
                //setImageViewUri(Uri.parse(data.temp_url));
                setChatUploadClickListner(data.final_url,data
                        , FirebaseStorage.getInstance().getReference().child("chat_datas").child(data.userid));

                setOnFileUploadListener(new ChatSentListner() {
                    @Override
                    public void onChatAdded(GroupChatData data) {

                    }

                    @Override
                    public void onChatUploaded(GroupChatData data) {
                        setLocallyAvailable(true);
                    }
                });

            }
            else {

                if(AroundStaticInfo.checkUriExist(getContext(),Uri.parse(data.final_url)))
                {
                    setUpAudioUri(Uri.parse(data.final_url));

                }
                else
                {
                    //setImageViewUri(Uri.parse(data.temp_url));
                }
                setLocallyAvailable(true);
            }
        }
        else {
            if (data.final_url == null ||data.final_url.contains("null")||data.final_url.contains("http")) {
                setLocallyAvailable(false);
                //setImageViewUri(Uri.parse(data.temp_url));

                setSizeText(StringUtils.mediaSizeToString(data.mediaSize));

                setOnJobCompletionListner(listener);
                setDownloadClickListner(data.final_url, data.type);

            } else {
                if(AroundStaticInfo.checkUriExist(getContext(),Uri.parse(data.final_url)))
                {
                    setUpAudioUri(Uri.parse(data.final_url));

                }
                else
                {
                   // setImageViewUri(Uri.parse(data.temp_url));
                }
                setLocallyAvailable(true);
            }
        }

    }

    FeedSentListner feedSentListnerListner = null;
    public void setOnFeedFileUploadListener(FeedSentListner job) {
        this.feedSentListnerListner = job;
    }
    public void setFeedUploadClickListner(String uri, final GroupFeedData data) {
        downloadView.setVisibility(VISIBLE);
        sizeText.setVisibility(VISIBLE);
        final String url = uri;
        SingleViewDownloadHandler.setUploadFeedClickListner(getContext(),url,1,data,downloadView,feedSentListnerListner);

    }
    public void setUpFeedMedia(final GroupFeedData data, JobCompletionListner listener)
    {
        // setMediaType(data.type);
        if(data.userid.equals(FirebaseAuth.getInstance().getUid()))
        {
            if(data.type!=0&&data.ifseen==0) {

                setLocallyAvailable(false);

                setOnFeedFileUploadListener(new FeedSentListner() {
                    @Override
                    public void onFeedAdded(FeedData data) {

                    }

                    @Override
                    public void onFeedUploaded(FeedData data) {
                        setLocallyAvailable(true);
                    }
                });

                setFeedUploadClickListner(data.uri,data);

            }
            else
            {
                setLocallyAvailable(true);
            }
        }
        else {

            if(data.uri.contains("http")||data.uri.contains("null")||data.uri==null)
            {
                //setImageViewUri(Uri.parse(data.semi_uri));
                setSizeText(StringUtils.mediaSizeToString(data.mediaSize));
                setOnJobCompletionListner(listener);
                setDownloadClickListner(data.uri, data.type);
                setLocallyAvailable(false);
            }
            else
            {
               // setImageViewUri(Uri.parse(data.uri));
                setLocallyAvailable(true);
            }
        }

    }

}
