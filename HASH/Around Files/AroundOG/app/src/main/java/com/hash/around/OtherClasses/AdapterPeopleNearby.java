package com.hash.around.OtherClasses;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cleveroad.fanlayoutmanager.FanLayoutManager;
import com.hash.around.OtherClasses.DataTypes.ContactListData;
import com.hash.around.OtherClasses.DataTypes.UserPrivacyData;
import com.hash.around.R;
import com.hash.around.TabbedActivity;
import com.hash.around.Utils.StringUtils;

import java.util.ArrayList;

/**
 * Created by Febin on 18-10-2017.
 */

public class AdapterPeopleNearby extends RecyclerView.Adapter<AdapterPeopleNearby.ViewHolder> implements View.OnCreateContextMenuListener{

   public static final int PRIVACYTHRESHOLD =1;
    RecyclerView xx;
    FanLayoutManager yyy;
    Context mContext;
    public ArrayList<ContactListData> data;
    public ArrayList<UserPrivacyData> privacydata;
   public static Location deafultLocation;
    Location checkLocation;
    public static int optionPosition=-1;
    public static int userInDB=-1;

    public AdapterPeopleNearby(Context mcont, ArrayList<ContactListData> list,ArrayList<UserPrivacyData> privacylist,RecyclerView xxx,FanLayoutManager yyyy)
    {
        xx=xxx;
        yyy=yyyy;
        mContext=mcont;
        data=list;
        privacydata=privacylist;
        deafultLocation=new Location("source");
        checkLocation=new Location("destination");

        //deafultLocation.setLatitude(lat);
        //deafultLocation.setLongitude(lng);

       /* Intent i=new Intent();
        i.setflag()*/

    }

    @Override
    public AdapterPeopleNearby.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v =  LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_people_near, parent,false);

        AdapterPeopleNearby.ViewHolder vh = new AdapterPeopleNearby.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(AdapterPeopleNearby.ViewHolder holder, final int position) {

        holder.lLayout.setOnCreateContextMenuListener(this);
        holder.lLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(yyy.getSelectedItemPosition()==position) {
                    optionPosition = position;
                    v.showContextMenu();
                    Log.e("MSGG","item pos : "+yyy.getSelectedItemPosition());
                }
                else if(yyy.getSelectedItemPosition()>=0)
                {
                    yyy.switchItem(xx, yyy.getSelectedItemPosition());
                }
                else {
                    Log.e("MSGG", "item pos : " + yyy.getSelectedItemPosition());
                    yyy.switchItem(xx, position);
                    ContactListData temp=fetchAlluserDetailsInDatabaseSingle(data.get(position).user_id);
                    if(temp!=null)
                    {
                        data.get(position).type=temp.type;
                    }

                }
            }
        });
        holder.name.setText(data.get(position).user_name);
      //  holder.distance.setText(distanceInMeters(0,0,0,0)+" mt away");
        checkLocation.setLatitude(data.get(position).latitude);
        checkLocation.setLongitude(data.get(position).longitude);


        holder.distance.setText(StringUtils.locationDistanceToString(deafultLocation.distanceTo(checkLocation)));

            if(privacydata.get(position).profile_picture<=PRIVACYTHRESHOLD)
            {
                String dir=data.get(position).img_url;
                if(dir.equals("null"))
                {
                    holder.proimg.setImageResource(R.drawable.default_user);
                }
                else
                {
                    holder.proimg.setImageURI(Uri.parse(dir));
                }
            }
            else
            {
                holder.proimg.setImageResource(R.drawable.default_user);
            }
    }



    ContactListData fetchAlluserDetailsInDatabaseSingle(String UID)
    {
        SQLiteDatabase db= TabbedActivity.db.getReadableDatabase();

        Cursor cursor=db.query(DatabaseAccesser.user_table,new String[]{DatabaseAccesser.user_user_type},
                DatabaseAccesser.user_user_id+" =?",new String[]{UID},null,null,null);
        ContactListData contact=null;
        if(cursor.moveToNext())
        {
            contact=new ContactListData();
            contact.type=cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseAccesser.user_user_type));
            userInDB=1;
        }
        return contact;
    }

    @Override
    public int getItemCount() {
        if(data!=null) {
           // return data.size();
            return data.size();
        }
        return 0;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.clear();
        MenuInflater inflater = new MenuInflater(mContext);

        inflater.inflate(R.menu.people_nearby_menu, menu);

        if(privacydata.get(optionPosition).anyonefollow<=PRIVACYTHRESHOLD)
        {
            //Log.e("MSGG","Threshhold les + "+privacydata.get(optionPosition).anyonefollow);
            //menu.findItem(R.id.action_follow_user).setTitle("Sent Follow Request");
        }
        else
        {
            Log.e("MSGG","Threshhold mor + "+privacydata.get(optionPosition).anyonefollow);
            menu.findItem(R.id.action_follow_user).setTitle("Sent Follow Request");
        }

        if(privacydata.get(optionPosition).anyonemessage<=PRIVACYTHRESHOLD)
        {

        }
        else
        {
            menu.findItem(R.id.chat_message).setTitle("Sent Chat Request");
        }

        if(data.get(optionPosition).type==3)
        {
            menu.findItem(R.id.action_follow_user).setVisible(false);
        }


    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout lLayout;
        public TextView name;
        public TextView distance;
        public ImageView proimg;
        public ViewHolder(View item) {
            super(item);

            name=(TextView)item.findViewById(R.id.peopleNearProfileName);
            distance=(TextView)item.findViewById(R.id.peopleNearProfileDistance);
            proimg=(ImageView) item.findViewById(R.id.peopleNearProfileImage);
            lLayout=(LinearLayout) item.findViewById(R.id.clickableArea);
        }
    }

    private double distanceInMeters(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return (dist*1000);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }
}


