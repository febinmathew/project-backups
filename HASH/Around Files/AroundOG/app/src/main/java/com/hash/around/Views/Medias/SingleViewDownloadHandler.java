package com.hash.around.Views.Medias;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;

import com.google.firebase.storage.StorageReference;
import com.hash.around.MainServiceThread;
import com.hash.around.OtherClasses.DataTypes.DownloadUploadData;
import com.hash.around.OtherClasses.DataTypes.GroupChatData;
import com.hash.around.OtherClasses.DataTypes.GroupFeedData;
import com.hash.around.OtherClasses.Interfaces.ChatSentListner;
import com.hash.around.OtherClasses.Interfaces.FeedSentListner;
import com.hash.around.OtherClasses.Interfaces.FileDownloadListner;
import com.hash.around.OtherClasses.Interfaces.FileUploadListner;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.R;
import com.hash.around.TabbedActivity;
import com.hash.around.Utils.ChatSender;
import com.hash.around.Utils.FeedSender;
import com.hash.around.Utils.MediaManager;
import com.hash.around.Utils.PrefManager;
import com.hash.around.Views.CustomMakers.AroundDownloadView;

/**
 * Created by Febin on 7/27/2018.
 */

public class SingleViewDownloadHandler {


    public static void setDownloadClickListner(final Context context, String uri, final int mediaType,
                                               int chatOrFeed,final  AroundDownloadView downloadView,final JobCompletionListner jobComplete) {
        //enableDownloadClick();
        downloadView.setIsDownload();
        final String url = uri;
        Log.e("GGG","autodownload "+"trueee3");
        boolean autodownload=false;
        if(chatOrFeed==0) {
            if (PrefManager.checkSearchMultiSelectPref(context, "data_usage_chat",mediaType, R.array.data_usage_chat_default_values)) //vvvv
            {
                Log.e("GGG", "autodownload " + "trueee");
                autodownload = true;
                downloadChatFromSingleViewFile(context,url,mediaType,downloadView,jobComplete);
            }
            if (!autodownload) {
                downloadView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        downloadChatFromSingleViewFile(context,url,mediaType,downloadView,jobComplete);
                    }
                });
            }


        }
        else if(chatOrFeed==1)
        {
            if (PrefManager.checkSearchMultiSelectPref(context, "data_usage_feed", TabbedActivity.PREF_IMAGE,R.array.data_usage_feed_default_values))
            {
                Log.e("GGG", "autodownload " + "trueee");
                autodownload = true;
                //downloadFile(url);
                downloadFeedFileFromSingleView(context,url,mediaType,downloadView,jobComplete);
            }

            if (!autodownload) {
                downloadView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        downloadFeedFileFromSingleView(context,url,mediaType,downloadView,jobComplete);
                    }
                });
            }
        }
    }
    public static void setUploadChatClickListner(final Context context,final String uri, int chatOrFeed, final GroupChatData data, final  AroundDownloadView downloadView, final StorageReference ref,final ChatSentListner uploadlistener) {

        if(chatOrFeed==0) {

            downloadView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    uploadChatFromSingleViewFile(context,uri,ref,data,null,downloadView,uploadlistener);
                }
            });
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    DownloadUploadData data22 = MediaManager.getDownloadListItemArrayFromUri(uri);
                    if (data22 != null) {

                        uploadChatFromSingleViewFile(context,uri,ref,data,data22,downloadView,uploadlistener);
                    }
                }
            }, 1000);
        }
    }
        public static void setUploadFeedClickListner(final Context context,String uri,int chatOrFeed, final GroupFeedData data, final  AroundDownloadView downloadView, final FeedSentListner uploadlistener) {
        //enableDownloadClick();
        downloadView.setIsUpload();
        final String url = uri;
        downloadView.setClickable(false);


        if(chatOrFeed==1) {

            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    DownloadUploadData data22 = MediaManager.getDownloadListItemArrayFromUri(url);
                    if (data22 != null) {
                        SingleViewDownloadHandler.uploadFeedFileFromSingleView(context, data, downloadView, uploadlistener, data22);
                    } else if ((data.timestamp + (1000 * 20)) > System.currentTimeMillis()) {
                        new Handler(Looper.getMainLooper()).postDelayed(this, 1000);
                    }
                }
            }, 1000);
        }

    }

    static void uploadChatFromSingleViewFile(Context context,String urlToUpload, StorageReference ref, final GroupChatData data, DownloadUploadData data22,final AroundDownloadView downloadView, final ChatSentListner uploadListner) {
        //Log.e("MSGG","uri is : "+urlToUpload );
        //int type=mediaType;

        FileUploadListner uploadListner2=new FileUploadListner() {
            @Override
            public void onFileUploaded(String global, String globalLite, String liteLocal) {
               // disableDownloadClick();
                Log.e("GGG","downloadme1 COMPKETE DISABLE ");
                if (uploadListner != null) {
                    uploadListner.onChatUploaded(data);
                }
            }

            int prePercent=0;
            @Override
            public void onProgressListener(int percent) {
                Log.e("GGG","downloadme1 PERCENT "+percent);
                if(percent==100 && prePercent==100)
                    return;

                downloadView.upDateProgress(percent);
                prePercent=percent;
            }

            @Override
            public void onFailureListener(int percent) {
                downloadView.showDownloadError();
            }

            @Override
            public void onPausedListener(int percent) {
                downloadView.reset();
            }
        };
        if(data22!=null &&data22.progress>=0 ) {
            Log.e("GGG","downloadme1 with pro "+data22.progress);
            downloadView.startDownload();
            //downloadView.upDateProgress(data22.progress);
        }
        else {
            Log.e("GGG", "downloadme1 ");
            //downloadView.startDownload();}
        }
        Log.e("GGG","uploadpro "+(MediaManager.getDownloadListItemArrayFromUri(urlToUpload)!=null));

        if(MediaManager.getDownloadListItemArrayFromUri(urlToUpload)!=null)
        {
            // Log.e("GGG","downloadme1 ");
            MediaManager.uploadResourceAndReturnUrls(context,data.temp_url,data.type,urlToUpload,ref,uploadListner2);}
        else {
            Log.e("GGG","uploadpro SENTING FILE");
            ChatSender.uploadMediaData(urlToUpload, data, null, uploadListner,uploadListner2 ); }

        /*uploadResourceAndReturnUrls(numberType, urlToUpload, ref,new FileUploadListner() {
            @Override
            public void onFileUploaded(String global, String globalLite) {

                uploadListner.onFileUploaded(global,globalLite);
            }
        });*/
    }

    static void downloadChatFromSingleViewFile(Context context,String urlToDownload, int type,final AroundDownloadView downloadView,final  JobCompletionListner jobComplete) {
        downloadView.startDownload();
        Log.e("GGG","autodownload "+"startdownload");
        MainServiceThread.downloadFileAndReturnUriString(context,urlToDownload,false,0, type,new FileDownloadListner() {
            @Override
            public void onProgressListener(int percent) {
                downloadView.upDateProgress( percent);
            }

            @Override
            public void onSuccessListener(String localFilename) {

                if (jobComplete != null) {
                    jobComplete.onJobCompletion(localFilename);
                }
            }

            @Override
            public void onFailureListener(int percent) {
                downloadView.showDownloadError();
            }

            @Override
            public void onPausedListener(int percent) {
                downloadView.reset();
            }
        });

    }

    public static void downloadFeedFileFromSingleView(Context context, String  urlToDownload, int type,final AroundDownloadView downloadView, final JobCompletionListner jobComplete) {

        downloadView.startDownload();
        Log.e("GGG","autodownload "+"startdownload");
        MainServiceThread.downloadFileAndReturnUriString(context,urlToDownload,false,1,type,new FileDownloadListner() {
            @Override
            public void onProgressListener(int percent) {
                downloadView.upDateProgress( percent);
            }

            @Override
            public void onSuccessListener(String localFilename) {
                //disableDownloadClick();
                if (jobComplete != null) {
                    jobComplete.onJobCompletion(localFilename);
                }
            }

            @Override
            public void onFailureListener(int percent) {
                downloadView.showDownloadError();
            }

            @Override
            public void onPausedListener(int percent) {
                downloadView.reset();
            }
        });
    }



    public static void uploadFeedFileFromSingleView(Context context, final GroupFeedData data, final AroundDownloadView downloadView, final FeedSentListner uploadListner, DownloadUploadData data22) {

        FileUploadListner uploadListner2=new FileUploadListner() {
            @Override
            public void onFileUploaded(String global, String globalLite, String liteLocal) {
                //setLocallyAvailable(true);
                Log.e("GGG","downloadme1 COMPKETE DISABLE ");
                if (uploadListner != null) {
                    uploadListner.onFeedUploaded(data);
                }
            }

            int prePercent=0;
            @Override
            public void onProgressListener(int percent) {
                Log.e("GGG","downloadme1 PERCENT "+percent);
                if(percent==100 && prePercent==100)
                    return;

                downloadView.upDateProgress(percent);
                prePercent=percent;
            }

            @Override
            public void onFailureListener(int percent) {
                downloadView.showDownloadError();
            }

            @Override
            public void onPausedListener(int percent) {
                downloadView.reset();
            }
        };
        if(data22!=null &&data22.progress>=0 ) {
            Log.e("GGG","downloadme1 with pro "+data22.progress);
            downloadView.startDownload();

        }
        else {

        }
        Log.e("GGG","uploadpro "+(MediaManager.getDownloadListItemArrayFromUri(data.uri)!=null));

        if(MediaManager.getDownloadListItemArrayFromUri(data.uri)!=null)
        {
            MediaManager.uploadResourceAndReturnUrls(context,data.semi_uri,data.type,data.uri,FeedSender.getFeedStorageRef(),uploadListner2);}
        else {
            Log.e("GGG","uploadpro SENTING FILE");
            FeedSender.uploadFeedData(context,data.uri, data, uploadListner,uploadListner2);
        }

    }

}
