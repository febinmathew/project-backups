package com.hash.around;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;

public class ClearAvatarActivity extends Activity {

  @Override
  public void onResume() {
    super.onResume();

    new AlertDialog.Builder(this)
        .setTitle("Remove photo?")
        .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        })
        .setPositiveButton("Remove", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent result = new Intent();
                result.putExtra("delete", true);
                setResult(Activity.RESULT_OK, result);
                finish();
            }
        })
        .show();
  }

}
