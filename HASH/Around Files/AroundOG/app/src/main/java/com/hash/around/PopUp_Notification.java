package com.hash.around;


import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hash.around.TabFragments.NotificationList_Fragment;
import com.hash.around.TabFragments.TabFragment1;
import com.hash.around.Utils.AppOnlineStatusManager;
import com.hash.around.Utils.StringUtils;
import com.hash.around.Views.CustomMakers.ChatView_ItemHeader;
import com.hash.around.Views.CustomMakers.NotificationViewPager;
import com.hash.around.Views.ViewFunctions;

import cn.carbswang.android.numberpickerview.library.NumberPickerView;


public class PopUp_Notification extends  Activity /*implements View.OnTouchListener*/{

    private SectionsPagerAdapter mSectionsPagerAdapter;
    LinearLayout animView;
Menu mMenu;
    TabLayout tabLayout;
    private NotificationViewPager mViewPager;

    TabFragment1 fragment1;

    int widnowsize;



    private ChatView_ItemHeader.ConversationDateHeader swipeInfoHeader;
    TextView swipeInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);


        setContentView(R.layout.popup_notification);
        overridePendingTransition(R.anim.slide_up_anim_from_out, R.anim.slide_down_anim);
        DisplayMetrics dm=new DisplayMetrics();

        getWindowManager().getDefaultDisplay().getMetrics(dm);

        //widnowsize=dm.widthPixels-(int)(dm.widthPixels*0.9);

        getWindow().setLayout((int)(dm.widthPixels*0.9),(int)(dm.heightPixels*0.8));
        //setWindowParams();

        mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());

        mViewPager = (NotificationViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
       // baseLayout=findViewById(R.id.baseLayout);
       // baseLayout.setOnTouchListener(this);
       // mViewPager.ite
        swipeInfo=findViewById(R.id.swipeInfo);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);


        LinearLayout tabLayoutt = (LinearLayout) ((ViewGroup) tabLayout.getChildAt(0)).getChildAt(0);
        TextView tabTextView = (TextView) tabLayoutt.getChildAt(1);
        tabTextView.setTypeface(tabTextView.getTypeface(), Typeface.BOLD);
        tabTextView.setCompoundDrawables(null,getResources().getDrawable(R.drawable.ic_earth_white),null,null);
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_earth_white);


        tabLayoutt = (LinearLayout) ((ViewGroup) tabLayout.getChildAt(0)).getChildAt(1);
        tabTextView = (TextView) tabLayoutt.getChildAt(1);
        tabTextView.setTypeface(tabTextView.getTypeface(), Typeface.BOLD);

        tabTextView.setCompoundDrawables(null,getResources().getDrawable(R.drawable.ic_contacts_white),null,null);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_person_account_white);

        this.swipeInfoHeader   = new ChatView_ItemHeader.ConversationDateHeader(getApplicationContext(), swipeInfo);
        updateSwipeInfo();

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                updateSwipeInfo();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        // animView=(LinearLayout)findViewById(R.id.animView);
       // animView.setAnimation(AnimationUtils.loadAnimation(this,
                //R.anim.flot_anim));

        /*mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
              //  Adapter_ChatList.mActionMode.finish();

            }

            @Override
            public void onPageSelected(int pos)
            {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });






        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                           }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
*/

    }
    @Override
    protected void onStart() {
        super.onStart();
        AppOnlineStatusManager.OnlineStatus.setActive(true);

    }
    @Override
    public void onStop() {
        AppOnlineStatusManager.OnlineStatus.setActive(false);
        super.onStop();
    }
    @Override
    protected void onPause() {
        overridePendingTransition(0,0);
        super.onPause();
    }

    void updateSwipeInfo()
    {
        if(mViewPager.getCurrentItem()==0){
            swipeInfo.setText(R.string.swipe_right_to_dismiss);
          //  swipeInfo.setCompoundDrawablesWithIntrinsicBounds(null,null,getApplicationContext().getResources().getDrawable(R.drawable.ic_arrow_up_white),null);
            //swipeInfo.setCompoundDrawablesWithIntrinsicBounds();
        }
        else {
            swipeInfo.setText(R.string.swipe_left_to_dismiss);
            //swipeInfo.setCompoundDrawablesWithIntrinsicBounds(getApplicationContext().getResources().getDrawable(R.drawable.ic_arrow_left_semiblack),null,null,null);
            }

        swipeInfoHeader.show();
        swipeInfoHeader.hide(1500);
    }

    @Override
    public void onBackPressed() {
        overridePendingTransition(R.anim.slide_down_anim,R.anim.slide_up_anim_from_out);
        super.onBackPressed();

    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();

        View view = getWindow().getDecorView();

        WindowManager.LayoutParams lp = (WindowManager.LayoutParams) view.getLayoutParams();
        //lp.gravity = Gravity.BOTTOM;
        //lp.x = 0;
        //lp.y = TabbedActivity.notificationLocation[1]+TabbedActivity.notificationLocation[3];
        //lp.height=300;
        getWindowManager().updateViewLayout(view, lp);

        //View marker= findViewById(R.id.up_arrow_marker);

        //marker.setX((int)(TabbedActivity.notificationLocation[0]*0.9)-(marker.getWidth()/2));//+(TabbedActivity.notificationLocation[2])-10);


    }
/*
    @Override
    public void onBackPressed() {
        // finish() is called in super: we only override this method to be able to override the transition
        super.onBackPressed();

        //animView.setAnimation(AnimationUtils.loadAnimation(this,
               //R.anim.slide_out_from_in_anim));

    }


    @Override
    protected void onDestroy() {



        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();


    }



    @Override
    public void onStop() {
        super.onStop();



    }
*/








    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            NotificationList_Fragment fm1=new NotificationList_Fragment();

            Bundle bundle = new Bundle();
            switch (position) {
                case 0:
                     //fragment1 = new TabFragment1();

                    bundle.putBoolean("requests", false);
                    fm1.setArguments(bundle);

                    return fm1;

                case 1:
                    bundle.putBoolean("requests", true);
                    fm1.setArguments(bundle);
                    return fm1;

                case 2:
                    //TabFragment3 fragment3 = new TabFragment3();
                    //return fragment3;
                default:
                    //fragment=null;
                    break;

            }
        return null;

        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 2;
        }


        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getApplicationContext().getString(R.string.notification);
                case 1:
                    return getApplicationContext().getString(R.string.requests);
            }
            return null;
        }
    }

}
