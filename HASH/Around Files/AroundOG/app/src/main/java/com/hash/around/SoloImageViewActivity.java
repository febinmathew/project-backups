package com.hash.around;


import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hash.around.TabFragments.NotificationList_Fragment;
import com.hash.around.TabFragments.TabFragment1;
import com.hash.around.Utils.AppOnlineStatusManager;
import com.hash.around.Views.CustomMakers.ChatView_ItemHeader;
import com.hash.around.Views.CustomMakers.NotificationViewPager;
import com.hash.around.Views.scribbles.widget.ScribbleView;


public class SoloImageViewActivity extends  Activity /*implements View.OnTouchListener*/{


    LinearLayout animView;
    Menu mMenu;
    TabLayout tabLayout;
    private NotificationViewPager mViewPager;
    TabFragment1 fragment1;
    int widnowsize;
    private ChatView_ItemHeader.ConversationDateHeader swipeInfoHeader;
    TextView swipeInfo;

    ScribbleView scribbleView;
Uri targetImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);


        setContentView(R.layout.solo_image_activity);
        //overridePendingTransition(R.anim.slide_up_anim_from_out, R.anim.slide_down_anim);
        DisplayMetrics dm=new DisplayMetrics();

        getWindowManager().getDefaultDisplay().getMetrics(dm);

        //widnowsize=dm.widthPixels-(int)(dm.widthPixels*0.9);

       // getWindow().setLayout((int)(dm.widthPixels*0.9),(int)(dm.heightPixels*1));
        //setWindowParams();

        targetImage=getIntent().getData();
        scribbleView=findViewById(R.id.scribble_view);
        if(targetImage==null||targetImage.toString().equals("null"))
            scribbleView.setImageResource(R.drawable.default_user);
        else
            scribbleView.setImage(targetImage);
    }
    @Override
    protected void onStart() {
        super.onStart();
        AppOnlineStatusManager.OnlineStatus.setActive(true);

    }
    @Override
    public void onStop() {
        AppOnlineStatusManager.OnlineStatus.setActive(false);
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        overridePendingTransition(0,0);
        super.onPause();
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();

        View view = getWindow().getDecorView();

        WindowManager.LayoutParams lp = (WindowManager.LayoutParams) view.getLayoutParams();
        lp.gravity = Gravity.CENTER;
        //lp.x = 0;
        //lp.y = TabbedActivity.notificationLocation[1]+TabbedActivity.notificationLocation[3];
        //lp.height=300;
        getWindowManager().updateViewLayout(view, lp);

    }

}
