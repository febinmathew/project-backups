package com.hash.around.Utils;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.hash.around.Broadcasts.AroundBroadcastReceiver;
import com.hash.around.ChatWindow;
import com.hash.around.MainServiceThread;
import com.hash.around.OtherClasses.DataTypes.ChatData;
import com.hash.around.OtherClasses.DataTypes.DownloadUploadData;
import com.hash.around.OtherClasses.DataTypes.FeedData;
import com.hash.around.OtherClasses.DataTypes.GroupChatData;
import com.hash.around.OtherClasses.DataTypes.GroupFeedData;
import com.hash.around.OtherClasses.DataTypes.MediaData;
import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.OtherClasses.Interfaces.ChatSentListner;
import com.hash.around.OtherClasses.Interfaces.FeedSentListner;
import com.hash.around.OtherClasses.Interfaces.FileDownloadListner;
import com.hash.around.OtherClasses.Interfaces.FileUploadListner;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.OtherClasses.Interfaces.LocationReceiver;
import com.hash.around.TabFragments.TabFragment2;
import com.hash.around.TabbedActivity;
import com.hash.around.Views.CustomMakers.AroundDownloadView;

import java.util.ArrayList;

import static android.content.Context.ALARM_SERVICE;
import static com.hash.around.TabbedActivity.UserName;
import static com.hash.around.TabbedActivity.fetchFusedLocation;
import static com.hash.around.TabbedActivity.mTabbedContext;
import static com.hash.around.TabbedActivity.userdetail;
import static com.hash.around.Utils.MediaManager.uploadResourceAndReturnUrls;

/**
 * Created by Febin on 6/12/2018.
 */

/*
* features
* sent text messsages
*/
public class FeedSender {

    static DatabaseReference chatFeedbaseRef =null;
    static StorageReference feedStorageRef =null;


    static public DatabaseReference getFeedDatabaseRef() {
        if(chatFeedbaseRef ==null)
            chatFeedbaseRef = FirebaseDatabase.getInstance().getReference().child("feed_stack");
        return chatFeedbaseRef;
    }


    public static StorageReference getFeedStorageRef() {
        if(feedStorageRef ==null)
            feedStorageRef = FirebaseStorage.getInstance().getReference().child("feed_datas").child(userdetail.getUid());
        return feedStorageRef;
    }
    public static void newMessageSchedule(Context context,long timestamp,long chatRow,String userID,int usertype)
    {
        PendingIntent pendingIntent;
        Intent myIntent = new Intent(context, AroundBroadcastReceiver.class);
        myIntent.setAction("com.hash.around.MESSAGE_SCHEDULE");
        myIntent.putExtra("chatrow",chatRow);
        myIntent.putExtra("usertype",usertype);
        myIntent.putExtra("userid",userID);
        pendingIntent = PendingIntent.getBroadcast(context, (int)System.currentTimeMillis(), myIntent,PendingIntent.FLAG_ONE_SHOT);

        AlarmManager alarmManager = (AlarmManager)context.getSystemService(ALARM_SERVICE);

        if (Build.VERSION.SDK_INT >= 23) {
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP,
                    timestamp, pendingIntent);
        } else if (Build.VERSION.SDK_INT >= 19) {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, timestamp, pendingIntent);
        } else {
            alarmManager.set(AlarmManager.RTC_WAKEUP, timestamp, pendingIntent);
        }

        //alarmManager.set(AlarmManager.RTC, timestamp, pendingIntent);
        //Log.e("MGG","BroadCAST SET "+(timestamp-System.currentTimeMillis()));
    }
    public static void newMessageDeletionSchedule(Context context,long timestamp,long chatRow,String userID)
    {
        PendingIntent pendingIntent;
        Intent myIntent = new Intent(context, AroundBroadcastReceiver.class);
        myIntent.setAction("com.hash.around.MESSAGE_SCHEDULE");
        myIntent.putExtra("chatrow",chatRow);
        myIntent.putExtra("usertype",2); //temp value not used
        myIntent.putExtra("userid",userID);
        myIntent.putExtra("intent_type",1);
        pendingIntent = PendingIntent.getBroadcast(context, (int)timestamp, myIntent,PendingIntent.FLAG_ONE_SHOT);

        AlarmManager alarmManager = (AlarmManager)context.getSystemService(ALARM_SERVICE);

        if (Build.VERSION.SDK_INT >= 23) {
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP,
                    timestamp, pendingIntent);
        } else if (Build.VERSION.SDK_INT >= 19) {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, timestamp, pendingIntent);
        } else {
            alarmManager.set(AlarmManager.RTC_WAKEUP, timestamp, pendingIntent);
        }

        //alarmManager.set(AlarmManager.RTC, timestamp, pendingIntent);
        ////Log.e("GGG","BroadCAST SET "+(timestamp-System.currentTimeMillis()));
    }

    public static void updateChatWindow(Context context,int type,long rowID,String userID,Integer chatData)
    {
        //////Log.e("GGG","gotttttt the pos : "+pos);
        if(ChatWindow.uId!=null)
        {////Log.e("GGG","uid OK ");
            if(ChatWindow.uId.equals(userID))
            {////Log.e("GGG","userID OK ");

                Integer pos;
                switch (type)
                {
                    case 0://seen status updation
                         pos=updateChatWindowExtention_getPositionInFinalData(ChatWindow.finaldata,rowID);
                       // //Log.e("GGG","gotttttt the pos : "+pos);
                        if(pos!=null)
                        {
                            ChatWindow.finaldata.get(pos).msgseen=chatData;
                            ChatWindow.adap.notifyItemChanged(pos);
                            //ChatWindow.adap.notifyDataSetChanged();
                        }
                        break;
                    case 1:
                        ChatWindow.adap.notifyDataSetChanged();
                        break;
                    case 2://deletion

                        pos=updateChatWindowExtention_getPositionInFinalData(ChatWindow.finaldata,rowID);
                        //Log.e("GGG","removing "+pos);
                        if(pos!=null)
                        {
                            //Log.e("GGG","final datasize "+ChatWindow.finaldata.size());
                            ChatWindow.finaldata.remove((int)pos);
                            ChatWindow.adap.notifyItemRemoved(pos);
                            //ChatWindow.adap.notifyDataSetChanged();
                            //Log.e("GGG","final datasize "+ChatWindow.finaldata.size());

                        }

                        break;

                }




            }
        }
    }

    public static void updateChatWindowHEAVY(String userID)
    {
        if(ChatWindow.uId!=null)
        {
            if(ChatWindow.uId.equals(userID))
            {

                NotifyTimer.removeAllListner();
                ChatWindow.instance.getcontentfromdatabase();   //heavy code Do Somethings
                ChatWindow.adap.notifyDataSetChanged();      //


            }
        }
    }

    public static void updateChatLastSeen(String userID,Long time)
    {
        if(ChatWindow.uId!=null)
        {
            if(ChatWindow.uId.equals(userID))
            {

                ChatWindow.instance.setLastSeenTimestamp(time!=null?time:0);//System.currentTimeMillis()+2);   //heavy code Do Somethings

            }
        }
    }

   static Integer updateChatWindowExtention_getPositionInFinalData(ArrayList<GroupChatData> data,long rowID)
    {
        for(int i=data.size()-1;i>=0;i--)
        {
            //Log.e("GGG","checking "+data.get(i).chatRow+" with "+rowID);
            if(data.get(i).chatRow==rowID)
                return i;
        }
        return null;
    }
    static long millischange=0;
    public static void sentTextMessage(Context context,int type, String message, final UserWholeData userData, final ChatSentListner listener)
    {
        final GroupChatData x=new GroupChatData();

        x.chatSchedule=userData.chat_scheduletime;
        x.chatExpire=userData.chat_expiretime;
        x.userid=userData.user_id;
        x.sender=0;
        x.type=type;
        x.timestamp= System.currentTimeMillis();
        updateChatLastSeen(userData.user_id,0L);
        //millischange+=millischange;
        x.msg=message;
        x.msgseen=0;
        x.temp_url="null";
        x.final_url="null";
        x.media_id=1;

        if(userData.type==5)
        {
            //x.userid = userdetail.getUid();
            x.chatRow=x.sl = DatabaseManager.saveChatToDatabase(context,x,userData.user_id);
        }
        else
        {
            x.chatRow=x.sl=DatabaseManager.saveChatToDatabase(context,x,null);
        }
        //Log.e("GGG","row is " +x.sl);
        if(listener!=null)
        listener.onChatAdded(x);

        if(userData.chat_scheduletime>System.currentTimeMillis())
        {
            newMessageSchedule(mContext,userData.chat_scheduletime,x.sl,userData.user_id,userData.type);
            ////Log.e("MSG","updated x time is 1 "+x.timestamp);
            return;
        }
        uploadTextData(x,userData.user_id,userData.type,listener);

    }




    public static void uploadTextData(final GroupChatData x,final String userID,final int userType,final ChatSentListner listener)
    {
        //Log.e("MSG","updated x.chatSchedule 1 "+x.chatSchedule);
        if(x.chatSchedule>0)
            x.chatSchedule=x.timestamp;



        final String fUser=UserManager.getUserInfo().getUid();
        x.userid=fUser;
        DatabaseReference singeChatRef = getFeedDatabaseRef().child(userID).push();
        final long val=x.chatRow;

        //Log.e("MGG","rowrow "+x.sl+" / "+x.chatRow);
        //String key= chatRef.getKey();
        singeChatRef.setValue(new ChatData(x)).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid)
            {
                //x.sl;
               // DatabaseManager.updateSelfChatstatus(val,1);

                if(listener!=null)
                    listener.onChatUploaded(x);

                updateChatWindow(null,0,val,userID,1);

            }
        });
    }

    /*******STARTS********/

    static Context mContext;


    public static void sentFeed(final Context context,  MediaData mData, final UserWholeData userData, final FeedSentListner listener)
    {


        final String uri = mData.uri;

        final GroupFeedData x = new GroupFeedData();

        x.username=userData.user_name;
        x.userid = userdetail.getUid();
        x.username = UserName;
        x.location = mData.location;
        x.timestamp = System.currentTimeMillis();
        x.visibility = mData.privacy;
        x.type = mData.type;
        x.text = mData.text;
        x.ifseen = 0;
        x.allowdownload=PrefManager.getBooleanPreference(context,"anyonesavefeed",true)?1:0;
        Location temp=LocationUtils.getUserLocation(context);
        Log.e("GGG","Febin Idea UPLOAD LATITUDE: "+temp.getLatitude());
        Log.e("GGG","Febin Idea UPLOAD LATITUDE: "+temp.getLongitude());
        x.loc=temp;
        x.feedID="null";
        //x.loc.setLatitude(TabFragment2.latitude);
        //x.loc.setLongitude(TabFragment2.longitude);
        //Log.e("GGG"," 2nd visibility "+x.visibility);

        if (x.type == 0) {
            x.semi_uri="null";
            x.uri="null";
            x.media_id = 1;

            x.feedRow=x.sl = savePostToDatabase(context,x);

            if(listener!=null)
                listener.onFeedAdded(x);


            uploadFeedFromAnywhere(new FeedData(x),x.loc, new FeedSentListner() {
                @Override
                public void onFeedAdded(FeedData data) {

                }

                @Override
                public void onFeedUploaded(FeedData data) {
                    DatabaseManager.updateSelfFeedstatus(context,x.feedRow,1);
                    if(listener!=null)
                        listener.onFeedUploaded(x);
                }
            });

        }
        else {

            try {
                x.semi_uri=MediaManager.getLiteLocalMediaFromOrginalMedia(context,mData.uri,mData.type);
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(context, "Media file not supported", Toast.LENGTH_LONG).show();
                return;
            }
            String globalUri="null"+StringUtils.randomStringGenerator();
            if(mData.type==1||mData.type==3||mData.type==5)
                x.uri=globalUri;
            else
                x.uri=mData.uri;
            try {
                x.mediaSize=MediaManager.getMediaSize(mTabbedContext, Uri.parse(mData.uri));
                //Log.e("MGG","size is : "+x.mediaSize);
            } catch (Exception e) {
                //Log.e("MGG","size error : "+Uri.parse(fileUri)+" / "+e.getMessage());
                e.printStackTrace();
            }

            ////Log.e("MGG","added CC : "+fileUri);

            final long mediaPosition=DatabaseManager.insertMediaToDatabase(context,globalUri,mData.uri,globalUri, x.semi_uri, x.mediaSize);

            //Log.e("MGG","inserted media at : "+mediaPosition);

            x.media_id=mediaPosition;
            x.feedRow=x.sl=savePostToDatabase(context,x);

            if (listener!=null)
                listener.onFeedAdded(x);

            uploadFeedData(context,mData.uri,x,listener,null);

            }


        /*final GroupChatData x=new GroupChatData();

        x.userid=userData.user_id;
        x.chatSchedule=userData.chat_scheduletime;
        x.chatExpire=userData.chat_expiretime;
        x.sender=0;
        x.type=type;
        x.timestamp= System.currentTimeMillis();
        updateChatLastSeen(userData.user_id,0L);

        x.msg=message;
        x.msgseen=0;
        try {
            x.temp_url=MediaManager.getLiteLocalMediaFromOrginalMedia(context,fileUri,type);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "Media file not supported", Toast.LENGTH_LONG).show();
            return;
        }
        String globalUri="null"+StringUtils.randomStringGenerator();
        if(type==1||type==3||type==5)
        x.final_url=globalUri;
        else
        x.final_url=fileUri;
        try {
            x.mediaSize=MediaManager.getMediaSize(mTabbedContext,Uri.parse(fileUri));
            //Log.e("MGG","size is : "+x.mediaSize);
        } catch (Exception e) {
            //Log.e("MGG","size error : "+Uri.parse(fileUri)+" / "+e.getMessage());
            e.printStackTrace();
        }

        ////Log.e("MGG","added CC : "+fileUri);

        final long mediaPosition=DatabaseManager.insertMediaToDatabase(globalUri,fileUri,globalUri, x.temp_url, x.mediaSize);

        //Log.e("MGG","inserted media at : "+mediaPosition);

        x.media_id=mediaPosition;
        x.sl=DatabaseManager.saveChatToDatabase(context,x,null);

        if (listener!=null)
            listener.onChatAdded(x);

        ////Log.e("MGG","PPreeeeee2 final url is "+Uri.parse(x.final_url));
        if(userData.chat_scheduletime>System.currentTimeMillis())
        {
            newMessageSchedule(mContext,userData.chat_scheduletime,x.sl,userData.user_id,userData.type);
        }
        else
        uploadMediaData(fileUri,new GroupChatData(x),listener);*/

    }

    static long savePostToDatabase(Context context,GroupFeedData post)
    {
        SQLiteDatabase db =DatabaseManager.getDatabase(context).getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.feed_col_user_id, post.userid);
        values.put(DatabaseAccesser.feed_col_feed_id, post.feedID);
        values.put(DatabaseAccesser.feed_col_username, post.username);
        values.put(DatabaseAccesser.feed_col_timestamp, post.timestamp);
        values.put(DatabaseAccesser.feed_col_type, post.type);
        values.put(DatabaseAccesser.feed_col_allowdownload, post.allowdownload);
        values.put(DatabaseAccesser.feed_col_seen, post.ifseen);
        values.put(DatabaseAccesser.feed_col_text, post.text);
        values.put(DatabaseAccesser.feed_col_media_id, post.media_id);
        values.put(DatabaseAccesser.feed_col_latitude, post.loc.getLatitude());
        values.put(DatabaseAccesser.feed_col_longitude, post.loc.getLongitude());

        long newRowId = db.insert(DatabaseAccesser.feed_table, null, values);
        Log.e("GGG","feedtext row is : "+newRowId);
        return newRowId;


    }


    public static  void uploadFeedData(final Context context, final String fileUri, final GroupFeedData x, final FeedSentListner listener,final FileUploadListner fileUploadListner)
    {
        uploadResourceAndReturnUrls(context,x.semi_uri ,x.type,fileUri, getFeedStorageRef(), new FileUploadListner() {
            @Override
            public void onFileUploaded(String global, String globalLite, String liteLocal) {
                    DatabaseManager.updateMediaInDatabase(x.media_id,global,fileUri,globalLite,liteLocal);

                if(fileUploadListner!=null)
                    fileUploadListner.onFileUploaded(global, globalLite, liteLocal);

                     x.uri=global;
                     x.semi_uri=globalLite;

                    uploadFeedFromAnywhere(new FeedData(x), x.loc,new FeedSentListner() {
                        @Override
                        public void onFeedAdded(FeedData data) {

                        }

                        @Override
                        public void onFeedUploaded(FeedData data) {
                            DatabaseManager.updateSelfFeedstatus(context,x.feedRow,1);
                            if(listener!=null)
                            listener.onFeedUploaded(x);
                        }
                    });
            }
            @Override
            public void onProgressListener(int percent) {
                if(fileUploadListner!=null)
                    fileUploadListner.onProgressListener(percent);
            }

            @Override
            public void onFailureListener(int percent) {
                if(fileUploadListner!=null)
                    fileUploadListner.onFailureListener(percent);

            }

            @Override
            public void onPausedListener(int percent) {
                if(fileUploadListner!=null)
                    fileUploadListner.onPausedListener(percent);
            }
        });
    }



    public static void uploadFeedFromAnywhere(final FeedData x,Location loc,final @Nullable FeedSentListner listener)
    {

        switch (x.visibility) {

            case 0:

            case 1:
                DatabaseReference newPostRef = getFeedDatabaseRef().push();
                //x.uri = path;
                newPostRef.setValue((FeedData)x);
                String key = newPostRef.getKey();

                DatabaseReference ref = FirebaseDatabase.getInstance().getReference("feeds_location");
                GeoFire geoFire = new GeoFire(ref);
                geoFire.setLocation(key, new GeoLocation(loc.getLatitude(), loc.getLongitude()), new GeoFire.CompletionListener() {
                    @Override
                    public void onComplete(String key, DatabaseError error) {
                        if (error != null) {
                           // Toast.makeText(mTab2Context, "Error writing location", Toast.LENGTH_SHORT).show();
                        } else {


                            updateFeedUploadStatus(mContext,x.sl,key,3);
                            if(listener!=null)
                                listener.onFeedUploaded(x);
                           // Toast.makeText(mTab2Context, "Success location", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                break;
            case 2:
                //contacts  only
                break;
            case 3:

                // only me
                break;

        }}

        public static void updateFeedUploadStatus(Context context,long row,String feedID,int ifseen)
        {
            ContentValues vals=new ContentValues();
            vals.put(DatabaseAccesser.feed_col_feed_id,feedID);
            vals.put(DatabaseAccesser.feed_col_seen,ifseen);

            SQLiteDatabase db=DatabaseManager.getDatabase(context).getWritableDatabase();
            long roqUp=db.update(DatabaseAccesser.feed_table,vals,DatabaseAccesser.feed_table+".rowid =?",new String[]{String.valueOf(row)});
            Log.e("GGG","feedtext updated fedds: "+roqUp);

        }

    public static void sharePost(final GroupFeedData feedData,final Activity activity) {

        fetchFusedLocation(new LocationReceiver() {

            @Override
            public void onLocationReceived(Location data) {



                feedData.userid = FirebaseAuth.getInstance().getUid();
                feedData.timestamp = System.currentTimeMillis();
                // feedData.ifseen = 1;
                feedData.username = UserName;


                //latitude = data.getLatitude();
                //longitude = data.getLongitude();

                if (feedData.type == 0) {
                    feedData.uri = "null";
                    uploadFeedFromAnywhere(new FeedData(feedData),data,null);

                } else if (feedData.type == 1 || feedData.type == 2) {
                    SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();
                    Cursor cursor2 = db.query(DatabaseAccesser.media_table,
                            new String[]{
                                    DatabaseAccesser.media_global_uri,
                                    DatabaseAccesser.media_local_uri,
                                    DatabaseAccesser.media_lite_global_uri,
                                    DatabaseAccesser.media_lite_local_uri,
                            },
                            DatabaseAccesser.media_lite_local_uri+ " =?",
                            new String[]{feedData.semi_uri},
                            null, null, null,null);
                    if(cursor2.getCount()==1) {
                        cursor2.moveToNext();

                        feedData.uri= cursor2.getString(
                                cursor2.getColumnIndexOrThrow(DatabaseAccesser.media_global_uri));

                        feedData.semi_uri= cursor2.getString(
                                cursor2.getColumnIndexOrThrow(DatabaseAccesser.media_lite_global_uri));

                        uploadFeedFromAnywhere(new FeedData(feedData), data, new FeedSentListner() {
                            @Override
                            public void onFeedAdded(FeedData data) {
                                Intent new_intent = new Intent("FEED_UPDATED");
                                new_intent.putExtra("self",true);
                                activity.sendBroadcast(new_intent);
                            }

                            @Override
                            public void onFeedUploaded(FeedData data) {
                                Intent new_intent = new Intent("FEED_UPDATED");
                                new_intent.putExtra("self",true);
                                activity.sendBroadcast(new_intent);
                            }
                        });
                    }



                }

            }
        },activity);



    }

}