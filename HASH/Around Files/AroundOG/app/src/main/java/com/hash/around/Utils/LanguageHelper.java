package com.hash.around.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.Locale;

/**
 * Created by Febin on 7/31/2018.
 */

public class LanguageHelper {
    private  Locale current;
    public   void onStart(Activity activity )
    {

        current=LocationUtils.getSelectedLocaleFromLanguage(activity);
        Log.e("GGG","language1 start "+current);
        LocationUtils.setContextLocale(activity,current);
    }
    public void onReset(Activity activity)
    {
       /* if(current==null)
        {
            onStart(activity);
            return;
        }*/

        Locale temp=LocationUtils.getSelectedLocaleFromLanguage(activity);
        Log.e("GGG","language1 resume "+!temp.equals(current)+" / "+temp);
        if(!temp.equals(current))
        {
            LocationUtils.setContextLocale(activity,current);
            activity.recreate();
            /*Intent intent = activity.getIntent();
            activity.finish();
            activity.overridePendingTransition(0, 0);
            activity.startActivity(intent);
            activity.overridePendingTransition(0, 0);*/
        }

    }
}
