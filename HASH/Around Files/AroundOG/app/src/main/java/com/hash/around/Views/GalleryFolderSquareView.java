package com.hash.around.Views;

import android.annotation.TargetApi;
import android.content.Context;
import android.net.Uri;
import android.os.Build.VERSION_CODES;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.hash.around.OtherClasses.Interfaces.JobCompletionWithFailureListner;
import com.hash.around.R;
import com.hash.around.Utils.MediaManager;
import com.hash.around.Views.Glide.GlideApp;

//import org.thoughtcrime.securesms.R;

public class GalleryFolderSquareView extends FrameLayout {

  private final boolean squareHeight;
    private final boolean squareAsWidth;

  public GalleryFolderSquareView(Context context) {
    this(context, null);
  }


  public GalleryFolderSquareView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  @TargetApi(VERSION_CODES.HONEYCOMB) @SuppressWarnings("unused")
  public GalleryFolderSquareView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);

    /*if (attrs != null) {
      TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.SquareFrameLayout, 0, 0);
      this.squareHeight = typedArray.getBoolean(R.styleable.SquareFrameLayout_square_height, false);
      this.squareAsWidth=typedArray.getBoolean(R.styleable.SquareFrameLayout_square_width, false);
      typedArray.recycle();
    }
    else {
      this.squareHeight = false;
      this.squareAsWidth=false;
    }*/
    //
      this.squareHeight=true;
      this.squareAsWidth=false;
  }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        initViews();
    }

  JobCompletionWithFailureListner listner;


 public  void setJobListner(JobCompletionWithFailureListner listnerTemp)
  {
    this.listner=listnerTemp;
  }

    @Override
    public void setOnClickListener(@Nullable OnClickListener l) {
        clickableArea.setOnClickListener(l);
    }

    public void initViews()
  {
      clickableArea=findViewById(R.id.clickableArea);
      resourceAmount=findViewById(R.id.resourceAmount);
      windowTag=findViewById(R.id.windowTag);
      typeIndicator=findViewById(R.id.typeIndicator);
      thumbnail=findViewById(R.id.thumbnail);

  }
  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    //noinspection SuspiciousNameCombination
    if (squareHeight) super.onMeasure(heightMeasureSpec, heightMeasureSpec);
    else              super.onMeasure(widthMeasureSpec, widthMeasureSpec);


  }
    ImageView typeIndicator,thumbnail;
    TextView windowTag,resourceAmount;
    FrameLayout clickableArea;
  public void setFolderDetails(Context context,Integer type, Uri uri, String folderName, long count)
  {
      if(type==-1)
          typeIndicator.setVisibility(GONE);
      else{
          typeIndicator.setVisibility(VISIBLE);
          typeIndicator.setImageResource(MediaManager.getIconForResourseType(type));}

      thumbnail.setImageDrawable(null);
      Glide.with(context).load(uri)
              .thumbnail(0.4f)
              .into(thumbnail);

     /* GlideApp.with(this).load(uri)
              .diskCacheStrategy(DiskCacheStrategy.NONE)
              .fitCenter()
              .into(thumbnail);*/

      windowTag.setText(folderName);
      resourceAmount.setText(count+"");
  }
}
