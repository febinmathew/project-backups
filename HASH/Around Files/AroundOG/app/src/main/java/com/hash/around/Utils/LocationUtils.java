package com.hash.around.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.text.TextUtils;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.hash.around.OtherClasses.DataTypes.LocationData;
import com.hash.around.OtherClasses.DataTypes.VCardData;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.R;
import com.hash.around.TabbedActivity;
import com.hash.around.UploadActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Febin on 6/13/2018.
 */

public class LocationUtils {

    public static Locale getSelectedLocaleFromLanguage(Context context) {

        String language[] = TextUtils.split(PrefManager.getStringPreference(context,PrefManager.PREF_LANGUAGE,"zz"),"_");
        if (language[0].equals("zz")) {
            return new Locale("en");
        } else if (language.length == 2) {
            return new Locale(language[0], language[1]);
        } else {
            return new Locale(language[0]);
        }
    }
    public static void setContextLocale(Context context, Locale selectedLocale) {

        /*if (selectedLocale.getCountry().equals("zz")) {
            selectedLocale = Locale.ENGLISH;
        }*/
        Configuration configuration = context.getResources().getConfiguration();

        if (!configuration.locale.equals(selectedLocale)) {
            configuration.locale = selectedLocale;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                configuration.setLayoutDirection(selectedLocale);
            }
            context.getResources().updateConfiguration(configuration,
                    context.getResources().getDisplayMetrics());
        }
    }

    public static Location getUserLocation(Context context)
    {
        SharedPreferences prefs=context.getSharedPreferences("user_preferences", MODE_PRIVATE);
        Double lat = Double.parseDouble(prefs.getString("location_latitude", "0.0"));
        Double lng = Double.parseDouble(prefs.getString("location_longitude", "0.0"));
        String location = prefs.getString("location_name", "Current position");
        Location loc=new Location(location);
        loc.setLatitude(lat);
        loc.setLongitude(lng);
        return loc;
    }
    public static void setUserLocation(Context context,Location loc,Address addr)
    {
        SharedPreferences.Editor prefs=context.getSharedPreferences("user_preferences", MODE_PRIVATE).edit();
        prefs.putString("location_latitude", String.valueOf(loc.getLatitude()));
        prefs.putString("location_longitude",  String.valueOf(loc.getLongitude()));
        prefs.putString("location_name", addr.getLocality()+", "+addr.getCountryName());
        prefs.clear().commit();
    }


    public static Address getAddressFromLocation(final Activity context, Location location) {
        Geocoder gcd;
        Log.e("GGG","context value: "+context);
        if(context==null)
            return null;
        gcd= new Geocoder(context, Locale.getDefault());
        Address locationText=null;
        List<Address> addresses = null;

        try {
            addresses = gcd.getFromLocation(
                    location.getLatitude(),
                    location.getLongitude(),
                    // In this sample, get just a single address.
                    10);
        } catch (IOException ioException) {
            // Catch network or other I/O problems.
            Toast.makeText(context, "Service no avaialble", Toast.LENGTH_SHORT).show();
        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
            Toast.makeText(context, "invalid latlong", Toast.LENGTH_SHORT).show();
        }

        // Handle case where no address was found.
        if (addresses == null || addresses.size() == 0) {
            Toast.makeText(context, "no results", Toast.LENGTH_SHORT).show();
        } else {
            for (Address temp:addresses)
            {
                if (temp.getLocality()!=null && temp.getCountryName()!=null) {
                     locationText=temp;//.getLocality() + ", " + temp.getAddressLine(2)+", "+temp.getCountryName();
                     break;
                }
            }

        }
       /* if(locationText!=null) {
            location.setLatitude(location.getLatitude()+0.000006);
            location.setLongitude(location.getLongitude()+0.000006);
            return getAddressFromLocation(context,location);
        }
        else*/
            return locationText;
    }

    public static ArrayList<LocationData> getLocationAsList(Context context,boolean active_only)
    {
        SQLiteDatabase db = DatabaseManager.getDatabase().getReadableDatabase();

        Cursor cursor;
        if(active_only)
        {
            cursor = db.query(DatabaseAccesser.location_table, null,
                    DatabaseAccesser.loc_col_status+" =?",
                    new String[]{"1"},
                    null, null, null,null);
        }
        else {
            cursor = db.query(DatabaseAccesser.location_table, new String[]{DatabaseAccesser.loc_col_location_name,
                            DatabaseAccesser.loc_col_status, DatabaseAccesser.loc_col_latitude, DatabaseAccesser.loc_col_longitude},
                    null,
                    null,
                    null, null, null, null);
        }
        ArrayList<LocationData> finalData=new  ArrayList<LocationData>();
        while (cursor.moveToNext()) {

            LocationData temp = new LocationData();

            temp.location_name = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.loc_col_location_name));
            temp.status = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.loc_col_status));
            temp.latitude = cursor.getDouble(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.loc_col_latitude));
            temp.longitude = cursor.getDouble(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.loc_col_longitude));

            finalData.add(temp);
        }
        cursor.close();

        return finalData;
    }
}
