package com.hash.around.OtherClasses.Interfaces;

import com.hash.around.OtherClasses.DataTypes.FeedData;
import com.hash.around.OtherClasses.DataTypes.GroupChatData;

public interface FeedSentListner {
    void onFeedAdded(FeedData data);
    void onFeedUploaded(FeedData data);
}


