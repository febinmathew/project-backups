package com.hash.around.Views;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.vipulasri.timelineview.LineType;
import com.github.vipulasri.timelineview.TimelineView;
import com.hash.around.R;
import com.hash.around.Views.Medias.ChatReplayLayout;

import static com.hash.around.ChatWindow.convertTimeStampToText;


public class AroundChatInfoTimelineView extends LinearLayout {

  public AroundChatInfoTimelineView(Context context) {
    this(context, null);
  }


  public AroundChatInfoTimelineView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  TextView dateText,infoHeading,infoDescription;
  TimelineView time_marker;
  RecyclerView infoRecycler;
  Context mContext;

  public ChatReplayLayout replaySection;

  public AroundChatInfoTimelineView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    inflate(context, R.layout.chat_info_timeline_view, this);

    dateText=findViewById(R.id.dateText);
    infoHeading=findViewById(R.id.infoHeading);
    infoDescription =  findViewById(R.id.infoDescription);
    time_marker =  findViewById(R.id.time_marker);
    infoRecycler =  findViewById(R.id.infoRecycler);

    mContext=context;
    if (attrs != null) {
     /* TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.SquareFrameLayout, 0, 0);

      typedArray.recycle();*/
    }


  }


    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        initViews();
    }

    public void setData(long timestamp,String heading,int type)
    {
      if(timestamp==0)
      {setVisibility(GONE);
      return;}

      infoHeading.setText(heading);
      dateText.setText(convertTimeStampToText(getContext(),timestamp,false));
      infoDescription.setText(convertTimeStampToText(getContext(),timestamp,false));

      switch (type)
      {
        case 1:
          time_marker.setMarker(getContext().getResources().getDrawable(R.drawable.ic_single_tick));
          break;
        case 2:
          time_marker.setMarker(getContext().getResources().getDrawable(R.drawable.ic_double_tick));
          break;
        case 3:
          time_marker.setMarker(getContext().getResources().getDrawable(R.drawable.ic_double_tick_seen_primary));
          break;
      }

    }
  public void setAsLast()
  {
    time_marker.setEndLine(0,LineType.END);
  }
  void initViews()
  {

  }

}
