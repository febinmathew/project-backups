package com.hash.around.Utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.format.Time;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.SubscriptSpan;
import android.text.style.SuperscriptSpan;
import android.text.style.UnderlineSpan;
import android.util.Base64;
import android.util.Log;

import com.google.android.gms.flags.Flag;
import com.google.firebase.auth.FirebaseAuth;
import com.hash.around.OtherClasses.DataTypes.VCardData;
import com.hash.around.R;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import ezvcard.VCard;

import static com.hash.around.TabFragments.TabFragment2.getResizedBitmap;

public class StringUtils {

    public static String formatStringArray(String[] data,boolean newLine)
    {
        String returnString=new String();
        for (int i=0;i<data.length;i++)
        {

            if(i>0)
            {
                if(newLine)
                    returnString +="\n";
                else
                    returnString +=", ";
            }

            returnString += data[i];
        }
        return returnString;
    }

    /*public static String chatMeOrPersonNameFromId(Context context,String userID,String defaultText)
    {
        if(userID.equals(FirebaseAuth.getInstance().getUid()))
            return context.getString(R.string.chat_you);
        else
            return  defaultText;
    }*/
    public static String latLngToMapString(double latitude,double longitude)
    {
        String mapLInk="https://maps.google.com/?q="+latitude+"%2C"+longitude;
        return mapLInk;
    }

    public static SpannableStringBuilder makeHashTagText(Context context,String sourceString,boolean removeTags)
    {

        SpannableStringBuilder str = new SpannableStringBuilder(sourceString);
        //str.replace(,,"");

        Log.e("GGG","converted og is  "+sourceString);
        String compileString="";
        //String sourceString = "<b>" + id + "</b> " + name;
        boolean hashTagOnCheck=false;
        int hashTagStart=0;
        boolean boldOnCheck=false;
        int boldTagStart=0;
        boolean strikeOnCheck=false;
        int strikeTagStart=0;
        boolean italicOnCheck=false;
        int italicTagStart=0;
        boolean underlineOnCheck=false;
        int underlineTagStart=0;
        boolean bgColorOnCheck=false;
        int bgColorTagStart=0;
        boolean superscriptOnCheck=false;
        int superscriptTagStart=0;
        boolean subscriptOnCheck=false;
        int subscriptTagStart=0;
        ArrayList<Integer> removePos=new ArrayList<>();



        for (int i = 0 ; i<sourceString.length() ; i++) {
            if (sourceString.charAt(i) == '#') {
               // compileString+="<b>#";
                hashTagStart=i;
                hashTagOnCheck = true;
            }
            else if(hashTagOnCheck && sourceString.charAt(i) == ' ')
            {
               // sourceString("\\s",)
               // compileString+="</b> ";
                str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), hashTagStart, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                hashTagOnCheck=false;
            }
            else if(sourceString.charAt(i) == '*')
            {

                if(boldOnCheck && ((i==sourceString.length()-1)||sourceString.charAt(i+1)==' '))
                {
                    boldOnCheck=false;
                    //compileString+="</b>";
                    removePos.add(boldTagStart);
                    removePos.add(i);
                    Log.e("GGG","remove add : "+i+" and "+boldTagStart);

                    str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), boldTagStart+1, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
                else if(i==0||sourceString.charAt(i-1)==' ') {
                    boldTagStart=i;
                    boldOnCheck=true;
                    //compileString += "<b>";
                }
                /*else
                {
                    Log.e("GGG",boldOnCheck+"bold: "+(i+1)+" / "+(sourceString.length()-1)+" char at "+sourceString.charAt(i));
                }*/


                //boldOnCheck=!boldOnCheck;
            }
            else if(sourceString.charAt(i) == '_')
            {
                if(italicOnCheck && ((i==sourceString.length()-1)||sourceString.charAt(i+1)==' ')) {
                    removePos.add(italicTagStart);
                    removePos.add(i);
                    str.setSpan(new android.text.style.StyleSpan(Typeface.ITALIC), italicTagStart+1, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);}
                else if(i==0||sourceString.charAt(i-1)==' ') {
                    italicTagStart=i;}
                italicOnCheck=!italicOnCheck;
            }
            else if(sourceString.charAt(i) == '~')
            {
                if(strikeOnCheck && ((i==sourceString.length()-1)||sourceString.charAt(i+1)==' ')) {
                    removePos.add(strikeTagStart);
                    removePos.add(i);
                    str.setSpan(new StrikethroughSpan(), strikeTagStart+1, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);}
                   // str.setSpan(new UnderlineSpan(), strikeTagStart+1, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);}
                else if(i==0||sourceString.charAt(i-1)==' ') {
                    strikeTagStart=i;}
                strikeOnCheck=!strikeOnCheck;
            }
            else if(sourceString.charAt(i) == '-')
            {
                if(underlineOnCheck && ((i==sourceString.length()-1)||sourceString.charAt(i+1)==' ')) {
                    removePos.add(underlineTagStart);
                    removePos.add(i);
                   // str.setSpan(new StrikethroughSpan(), strikeTagStart, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    str.setSpan(new UnderlineSpan(), underlineTagStart+1, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);}
                else if(i==0||sourceString.charAt(i-1)==' ') {
                    underlineTagStart=i;}
                underlineOnCheck=!underlineOnCheck;
            }
            else if(sourceString.charAt(i) == '`')
            {
                if(bgColorOnCheck && ((i==sourceString.length()-1)||sourceString.charAt(i+1)==' ')) {
                    removePos.add(bgColorTagStart);
                    removePos.add(i);
                    // str.setSpan(new StrikethroughSpan(), strikeTagStart, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    str.setSpan(new android.text.style.BackgroundColorSpan(R.color.monospace), bgColorTagStart+1, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);}
                else if(i==0||sourceString.charAt(i-1)==' ') {
                    bgColorTagStart=i;}
                bgColorOnCheck=!bgColorOnCheck;
            }
            else if(sourceString.charAt(i) == '^')
            {
                if(superscriptOnCheck && ((i==sourceString.length()-1)||sourceString.charAt(i+1)==' ')) {
                    removePos.add(superscriptTagStart);
                    removePos.add(i);
                    //str.setSpan(new StrikethroughSpan(), borderTagStart + 1, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    str.setSpan(new android.text.style.AbsoluteSizeSpan(30), superscriptTagStart+1, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    str.setSpan(new android.text.style.SuperscriptSpan(), superscriptTagStart+1, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    // str.setSpan(new android.text.style.BulletSpan(10,R.color.black), borderTagStart+1, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    //str.setSpan(new android.text.style.DrawableMarginSpan(context.getResources().getDrawable(R.drawable.chat_sent_real)), borderTagStart+1, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }else if(i==0||sourceString.charAt(i-1)==' ') {
                    superscriptTagStart=i;}
                superscriptOnCheck=!superscriptOnCheck;
            }
            else if(sourceString.charAt(i) == '&')
            {
                if(subscriptOnCheck && ((i==sourceString.length()-1)||sourceString.charAt(i+1)==' ')) {
                    removePos.add(subscriptTagStart);
                    removePos.add(i);
                    str.setSpan(new android.text.style.AbsoluteSizeSpan(30), subscriptTagStart+1, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    str.setSpan(new android.text.style.SubscriptSpan(), subscriptTagStart+1, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }
                    else if(i==0||sourceString.charAt(i-1)==' ') {
                    subscriptTagStart=i;}
                subscriptOnCheck=!subscriptOnCheck;
            }
            /*else{android.text.style
                compileString+=sourceString.charAt(i);
            }*/

        }
        if(hashTagOnCheck) {
            //compileString += "</b> ";
            str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), hashTagStart, sourceString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        Log.e("GGG","remove size : "+removePos.size());

        Collections.sort(removePos, new Comparator<Integer>() {
            @Override
            public int compare(Integer s1, Integer s2) {
                return s1.compareTo(s2);
            }
        });

        for (int j=removePos.size()-1;j>=0;j--)
        {
            Log.e("GGG","remove at "+removePos.get(j)+" char: "+sourceString.charAt(removePos.get(j)));
            if(removeTags)
            str.replace(removePos.get(j),removePos.get(j)+1,"");
            else
                str.setSpan(new android.text.style.ForegroundColorSpan(Color.BLUE) ,removePos.get(j),removePos.get(j)+1,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        // compileString=compileString.replaceAll("\\#","<b>#");
        //sourceString=sourceString.replaceAll("#\\S+","</b> ");
        //sourceString.index

        /*String[] scource= sourceString.split("#");
       // String compileString=scource[0];

        for(int i=1;i<scource.length;i++)
        {
            compileString+="<b>#"+scource[i].(" ")[0]+"</b>";
            if(scource[i].split(" ").length>0)
                compileString+=" ";
        }*/
        Log.e("GGG","converted "+compileString);
        return str;
    }




    public static void makeHashTagTextForEditText(Context context, Editable sourceString, boolean removeTags)
    {
        StrikethroughSpan []toRemoveSpans = sourceString.getSpans(0, sourceString.length(), StrikethroughSpan.class);
        for (int i = 0; i < toRemoveSpans.length; i++)
            sourceString.removeSpan(toRemoveSpans[i]);
        android.text.style.StyleSpan []toRemoveSpans1 = sourceString.getSpans(0, sourceString.length(), android.text.style.StyleSpan.class);
        for (int i = 0; i < toRemoveSpans1.length; i++)
            sourceString.removeSpan(toRemoveSpans1[i]);
        UnderlineSpan []toRemoveSpans2 = sourceString.getSpans(0, sourceString.length(), UnderlineSpan.class);
        for (int i = 0; i < toRemoveSpans2.length; i++)
            sourceString.removeSpan(toRemoveSpans2[i]);
        BackgroundColorSpan []toRemoveSpans3 = sourceString.getSpans(0, sourceString.length(), BackgroundColorSpan.class);
        for (int i = 0; i < toRemoveSpans3.length; i++)
            sourceString.removeSpan(toRemoveSpans3[i]);
        SuperscriptSpan[]toRemoveSpans4 = sourceString.getSpans(0, sourceString.length(), SuperscriptSpan.class);
        for (int i = 0; i < toRemoveSpans4.length; i++)
            sourceString.removeSpan(toRemoveSpans4[i]);
        SubscriptSpan[]toRemoveSpans5 = sourceString.getSpans(0, sourceString.length(), SubscriptSpan.class);
        for (int i = 0; i < toRemoveSpans5.length; i++)
            sourceString.removeSpan(toRemoveSpans5[i]);

        ForegroundColorSpan[]toRemoveSpans6 = sourceString.getSpans(0, sourceString.length(), ForegroundColorSpan.class);
        for (int i = 0; i < toRemoveSpans6.length; i++)
            sourceString.removeSpan(toRemoveSpans6[i]);


        //sourceString.span
        Spannable str = sourceString;


        String compileString="";

        boolean hashTagOnCheck=false;
        int hashTagStart=0;
        boolean boldOnCheck=false;
        int boldTagStart=0;
        boolean strikeOnCheck=false;
        int strikeTagStart=0;
        boolean italicOnCheck=false;
        int italicTagStart=0;
        boolean underlineOnCheck=false;
        int underlineTagStart=0;
        boolean bgColorOnCheck=false;
        int bgColorTagStart=0;
        boolean superscriptOnCheck=false;
        int superscriptTagStart=0;
        boolean subscriptOnCheck=false;
        int subscriptTagStart=0;
        ArrayList<Integer> removePos=new ArrayList<>();


        for (int i = 0 ; i<sourceString.length() ; i++) {
            if (sourceString.charAt(i) == '#') {
                // compileString+="<b>#";
                hashTagStart=i;
                hashTagOnCheck = true;
            }
            else if(hashTagOnCheck && sourceString.charAt(i) == ' ') {
                // sourceString("\\s",)
                // compileString+="</b> ";
                str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), hashTagStart, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                hashTagOnCheck=false;
            }
            else if(sourceString.charAt(i) == '*') {

                if(boldOnCheck && ((i==sourceString.length()-1)||sourceString.charAt(i+1)==' '))
                {
                    boldOnCheck=false;
                    //compileString+="</b>";
                    removePos.add(boldTagStart);
                    removePos.add(i);
                    //Log.e("GGG","remove add : "+i+" and "+boldTagStart);

                    str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), boldTagStart+1, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
                else if(i==0||sourceString.charAt(i-1)==' ') {
                    boldTagStart=i;
                    boldOnCheck=true;
                    //compileString += "<b>";
                }
                /*else
                {
                    Log.e("GGG",boldOnCheck+"bold: "+(i+1)+" / "+(sourceString.length()-1)+" char at "+sourceString.charAt(i));
                }*/


                //boldOnCheck=!boldOnCheck;
            }
            else if(sourceString.charAt(i) == '_')
            {
                if(italicOnCheck && ((i==sourceString.length()-1)||sourceString.charAt(i+1)==' ')) {
                    removePos.add(italicTagStart);
                    removePos.add(i);
                    str.setSpan(new android.text.style.StyleSpan(Typeface.ITALIC), italicTagStart+1, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);}
                else if(i==0||sourceString.charAt(i-1)==' ') {
                    italicTagStart=i;}
                italicOnCheck=!italicOnCheck;
            }
            else if(sourceString.charAt(i) == '~')
            {
                if(strikeOnCheck && ((i==sourceString.length()-1)||sourceString.charAt(i+1)==' ')) {
                    removePos.add(strikeTagStart);
                    removePos.add(i);
                    str.setSpan(new StrikethroughSpan(), strikeTagStart+1, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);}
                // str.setSpan(new UnderlineSpan(), strikeTagStart+1, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);}
                else if(i==0||sourceString.charAt(i-1)==' ') {
                    strikeTagStart=i;}
                strikeOnCheck=!strikeOnCheck;
            }
            else if(sourceString.charAt(i) == '-')
            {
                if(underlineOnCheck && ((i==sourceString.length()-1)||sourceString.charAt(i+1)==' ')) {
                    removePos.add(underlineTagStart);
                    removePos.add(i);
                    // str.setSpan(new StrikethroughSpan(), strikeTagStart, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    str.setSpan(new UnderlineSpan(), underlineTagStart+1, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);}
                else if(i==0||sourceString.charAt(i-1)==' ') {
                    underlineTagStart=i;}
                underlineOnCheck=!underlineOnCheck;
            }
            else if(sourceString.charAt(i) == '`')
            {
                if(bgColorOnCheck && ((i==sourceString.length()-1)||sourceString.charAt(i+1)==' ')) {
                    removePos.add(bgColorTagStart);
                    removePos.add(i);
                    str.setSpan(new android.text.style.BackgroundColorSpan(R.color.monospace), bgColorTagStart+1, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);}
                else if(i==0||sourceString.charAt(i-1)==' ') {
                    bgColorTagStart=i;}
                bgColorOnCheck=!bgColorOnCheck;
            }
            else if(sourceString.charAt(i) == '^')
            {
                if(superscriptOnCheck && ((i==sourceString.length()-1)||sourceString.charAt(i+1)==' ')) {
                    removePos.add(superscriptTagStart);
                    removePos.add(i);
                    //str.setSpan(new StrikethroughSpan(), borderTagStart + 1, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    str.setSpan(new android.text.style.AbsoluteSizeSpan(30), superscriptTagStart+1, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    str.setSpan(new android.text.style.SuperscriptSpan(), superscriptTagStart+1, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    // str.setSpan(new android.text.style.BulletSpan(10,R.color.black), borderTagStart+1, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    //str.setSpan(new android.text.style.DrawableMarginSpan(context.getResources().getDrawable(R.drawable.chat_sent_real)), borderTagStart+1, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }else if(i==0||sourceString.charAt(i-1)==' ') {
                    superscriptTagStart=i;}
                superscriptOnCheck=!superscriptOnCheck;
            }
            else if(sourceString.charAt(i) == '&')
            {
                if(subscriptOnCheck && ((i==sourceString.length()-1)||sourceString.charAt(i+1)==' ')) {
                    removePos.add(subscriptTagStart);
                    removePos.add(i);
                    str.setSpan(new android.text.style.AbsoluteSizeSpan(30), subscriptTagStart+1, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    str.setSpan(new android.text.style.SubscriptSpan(), subscriptTagStart+1, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
                else if(i==0||sourceString.charAt(i-1)==' ') {
                    subscriptTagStart=i;}
                subscriptOnCheck=!subscriptOnCheck;
            }
        }
        if(hashTagOnCheck) {
            //compileString += "</b> ";
            str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), hashTagStart, sourceString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        Collections.sort(removePos, new Comparator<Integer>() {
            @Override
            public int compare(Integer s1, Integer s2) {
                return s1.compareTo(s2);
            }
        });
        for (int j=removePos.size()-1;j>=0;j--)
        {
            Log.e("GGG","remove at "+removePos.get(j)+" char: "+sourceString.charAt(removePos.get(j)));
            if(removeTags);
                //str.replace(removePos.get(j),removePos.get(j)+1,"");
            else
                str.setSpan(new android.text.style.ForegroundColorSpan(context.getResources().getColor(R.color.semiblack_drawable)) ,removePos.get(j),removePos.get(j)+1,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        Log.e("GGG","converted "+compileString);
       // return str;
    }


    public static String randomStringGenerator()
    {
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for(
                int i = 0;
                i< 20;i++)

        {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();

    }

    static String[] memoryRepresentation=new String[]{"B","KB","MB","GB"};

    public static String mediaSizeToString(long size)
    {
        int count=0;
        while(size>1024)
        {
            count++;
            size= size/1024;
        }
        return size+" "+memoryRepresentation[count];

    }

    public static String generateStringVcardFromNumber(VCardData vData)
    {

        /*String sample="BEGIN:VCARD\n" +
                "VERSION:3.0\n" +
                "N:Ihrd;Adrash;Jio;;\n" +
                "FN:Adrash Jio Ihrd\n" +
                "item1.TEL;waid=917012153009:+91 70121 53009\n" +
                "item1.X-ABLabel:Mobile\n" +
                "END:VCARD";*/

        String vCard=new String();
        vCard+="BEGIN:VCARD\nVERSION:3.0\n";

        String[] names=vData.name.split(" ");

        vCard+="N:;"+vData.name+";;;\n";
        vCard+="FN:"+vData.name+"\n";
        for(int i=0;i<vData.number.size();i++) {
            vCard += "item1.TEL;waid=" + vData.number.get(i).replaceAll("[\\+s]", "") + ":" + vData.number.get(i) + "\n";
            vCard += "item1.X-ABLabel:"+vData.numberLabel.get(i)+ "\n";
        }
        if(vData.photo!=null) {
            vCard+="PHOTO;BASE64:"+ vData.photo+"\n";
        }
        vCard+="END:VCARD";
        Log.e("MGG","vcard is "+vCard);
        return vCard;

    }

    public static String getNameFromVcard(String vCard)
    {

        return vCard.split("FN:")[1].split("\n")[0];
    }

    public static String getShortExpirationDisplayValue( int expirationTime)
    {
        if (expirationTime <= 0) {
            return "0s";
        } else if (expirationTime < TimeUnit.MINUTES.toSeconds(1)) {
            return  expirationTime+"s";
        } else if (expirationTime < TimeUnit.HOURS.toSeconds(1)) {
            int minutes = expirationTime / (int)TimeUnit.MINUTES.toSeconds(1);
            return minutes+"m";
        } else if (expirationTime < TimeUnit.DAYS.toSeconds(1)) {
            int hours = expirationTime / (int)TimeUnit.HOURS.toSeconds(1);
            return hours+"h";
        } else if (expirationTime < TimeUnit.DAYS.toSeconds(7)) {
            int days = expirationTime / (int)TimeUnit.DAYS.toSeconds(1);
            return days+"d";
        } else {
            int weeks = expirationTime / (int)TimeUnit.DAYS.toSeconds(7);
            return weeks+"w";
        }
    }

    public static String getShortExpirationDisplayValueForTimestamp( long expirationTime)
    {
        return getShortExpirationDisplayValue((int)(expirationTime-System.currentTimeMillis())/1000);
    }

    public static String getExpirationDisplayValue(Context context, int expirationTime) {
        if (expirationTime <= 0) {
            return "off";
        } else if (expirationTime < TimeUnit.MINUTES.toSeconds(1)) {
            return context.getResources().getQuantityString(R.plurals.expiration_seconds, expirationTime, expirationTime);
        } else if (expirationTime < TimeUnit.HOURS.toSeconds(1)) {
            int minutes = expirationTime / (int)TimeUnit.MINUTES.toSeconds(1);
            return context.getResources().getQuantityString(R.plurals.expiration_minutes, minutes, minutes);
        } else if (expirationTime < TimeUnit.DAYS.toSeconds(1)) {
            int hours = expirationTime / (int)TimeUnit.HOURS.toSeconds(1);
            return context.getResources().getQuantityString(R.plurals.expiration_hours, hours, hours);
        } else if (expirationTime < TimeUnit.DAYS.toSeconds(7)) {
            int days = expirationTime / (int)TimeUnit.DAYS.toSeconds(1);
            return context.getResources().getQuantityString(R.plurals.expiration_days, days, days);
        } else {
            int weeks = expirationTime / (int)TimeUnit.DAYS.toSeconds(7);
            return context.getResources().getQuantityString(R.plurals.expiration_weeks, weeks, weeks);
        }
    }

    /*public static String getBriefRelativeTimeSpanString(final Context c, final Locale locale, final long timestamp) {
        if (isWithin(timestamp, 1, TimeUnit.MINUTES)) {
            return c.getString(R.string.DateUtils_just_now);
        } else if (isWithin(timestamp, 1, TimeUnit.HOURS)) {
            int mins = convertDelta(timestamp, TimeUnit.MINUTES);
            return c.getResources().getString(R.string.DateUtils_minutes_ago, mins);
        } else if (isWithin(timestamp, 1, TimeUnit.DAYS)) {
            int hours = convertDelta(timestamp, TimeUnit.HOURS);
            return c.getResources().getQuantityString(R.plurals.hours_ago, hours, hours);
        } else if (isWithin(timestamp, 6, TimeUnit.DAYS)) {
            return getFormattedDateTime(timestamp, "EEE", locale);
        } else if (isWithin(timestamp, 365, TimeUnit.DAYS)) {
            return getFormattedDateTime(timestamp, "MMM d", locale);
        } else {
            return getFormattedDateTime(timestamp, "MMM d, yyyy", locale);
        }
    }*/

    public static boolean isToday(long when) {
        Time time = new Time();
        time.set(when);

        int thenYear = time.year;
        int thenMonth = time.month;
        int thenMonthDay = time.monthDay;

        time.set(System.currentTimeMillis());
        return (thenYear == time.year)
                && (thenMonth == time.month)
                && (thenMonthDay == time.monthDay);
    }
    public  static boolean isYesterday(final long when) {
        return isToday(when + TimeUnit.DAYS.toMillis(1));
    }
    public static String convertMediaTypeToString(int type)
    {
        String lastmessage_type="";
        switch(type) {
            case 1:
                lastmessage_type = "Image";
                break;
            case 2:
                lastmessage_type = "Audio";
                break;
            case 3:
                lastmessage_type = "Video";
                break;
            case 4:
                lastmessage_type = "Location";
                break;
            case 5:
                lastmessage_type = "Gif";
                break;
            case 6:
                lastmessage_type = "File";
                break;
            case 7:
                lastmessage_type = "Contact";
                break;

        }
        return  lastmessage_type;
    }
    public static String locationDistanceToString(float dist)
    {
        int distance=(int)dist;
        if(distance>1000)
        {distance/=1000;
            return distance+" km away";}
        else
        {
            return distance+" m away";}
    }
}
