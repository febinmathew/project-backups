package com.hash.around;

import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.firebase.auth.FirebaseAuth;
import com.hash.around.OtherClasses.DataTypes.MediaData;
import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.hash.around.OtherClasses.Interfaces.LocationReceiver;
import com.hash.around.OtherClasses.VisibilitySpinnerAdapter;
import com.hash.around.TabFragments.TabFragment2;
import com.hash.around.Utils.LocationUtils;
import com.hash.around.Views.AroundPhotoRail;
import com.hash.around.Views.EmojiIconImage;
import com.hash.around.Views.Medias.AroundCircularImageView;
import com.hash.around.Views.Medias.AroundDownloadAudioView;
import com.hash.around.Views.Medias.AroundDownloadGifView;
import com.hash.around.Views.Medias.AroundDownloadImageView;
import com.hash.around.Views.Medias.AroundDownloadVideoView;
import com.hash.around.Views.RecentPhotoViewRail;
import com.hash.around.Views.VideoTrimmer.AroundVideoTrimmer;
import com.hash.around.Views.scribbles.widget.ScribbleView;
import com.vanniktech.emoji.EmojiEditText;
import com.vanniktech.emoji.EmojiManager;
import com.vanniktech.emoji.ios.IosEmojiProvider;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.view.View.GONE;
import static com.hash.around.OtherClasses.Adapter_Contacts.checkIfPrivacyShouldBeActivie;
import static com.hash.around.TabbedActivity.fetchFusedLocation;

public class UploadActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    String uri;
    String uricropped;
    int type;
    LinearLayout parentLayout;
    ImageView uploadImg;
    EmojiEditText statustext;
    Uri content;
    VideoView uploadVideo;
    TextureView videoTexture;
    MediaPlayer mMediaPlayer;

    public static int visibilityMode;
    //String[] visibilityNames={"Public","Nearby people","Contacts only","Only me"};

    double latitute;
    double longitude;
    String locationName;


    static private LocationCallback mLocationCallback;
    static private FusedLocationProviderClient mFusedLocationClient;
    static LocationRequest mLocationRequest;

    TextView locationtxt;
    AroundPhotoRail photoRail;
    ArrayList<MediaData> finalData=new ArrayList<>();

    ViewPager viewPager;
    AroundVideoTrimmer videoTrimmer;

    int callerType;

    //1=chat //2=feed
    public int receiverAcitvity=0;
    EmojiIconImage emotimg;
    AroundCircularImageView chatUserImage;
    TextView mediaSharedUsername;
    Spinner visibilitySpinner;
    String userId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        EmojiManager.install(new IosEmojiProvider());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.upload_activity);



        Toolbar toolbar = (Toolbar) findViewById(R.id.comtoolbar2);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        toolbar.findViewById(R.id.up_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UploadActivity.super.onBackPressed();
            }});
        uri= getIntent().getStringExtra("uri");
        //  Toast.makeText(this, uri, Toast.LENGTH_SHORT).show();
        // type=getIntent().getIntExtra("numberType",0);
        receiverAcitvity=getIntent().getIntExtra("forChat",0);
        userId=getIntent().getStringExtra("userid");

        Log.e("GGG","uploadreceive "+receiverAcitvity+" / "+userId);
        //Bundle args = getIntent().getBundleExtra("BUNDLE");

        finalData=(ArrayList<MediaData>)getIntent().getSerializableExtra("mediaarray");
        if(finalData==null)
        {
            finalData=new ArrayList<>();
            MediaData xx=new MediaData();
            xx.type=0;
            xx.text=getIntent().getStringExtra("textSequence");
            finalData.add(xx);
        }

       /* RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mHolderTopView.getLayoutParams();
        lp.setMargins(marge - widthSeek, 0, marge - widthSeek, 0);
        mHolderTopView.setLayoutParams(lp);

        lp = (RelativeLayout.LayoutParams) mTimeLineView.getLayoutParams();
        lp.setMargins(marge, 0, marge, 0);
        mTimeLineView.setLayoutParams(lp);*/

        //videoTrimmer=(AroundVideoTrimmer)findViewById(R.id.videoTrimmer);



        photoRail=findViewById(R.id.photoRail);
        if(finalData==null || finalData.size()<=1)
        {
            photoRail.setVisibility(GONE);
            photoRail=null;
        }
        else {

            photoRail.setAdapterWithListner(finalData, Uri.parse(uri), new RecentPhotoViewRail.OnItemClickedListener() {
                @Override
                public void onItemClicked(MediaData uri) {

                    Log.e("MGG", "clicking NOw " + uri);
                    viewPager.setCurrentItem(positionOfUri(uri.uri), true);
                }

                @Override
                public void onItemSelected(ArrayList<MediaData> uriList) {

                }
            });
        }
       /* if(args!=null) {
            finalData = (ArrayList<ChatData>) args.getSerializable("listener");
            chatforwardActivity=true;

        }*/


        parentLayout=(LinearLayout)findViewById(R.id.parentLayout);
        statustext=findViewById(R.id.image_feed_status) ;
        emotimg = findViewById(R.id.emotimg);
        emotimg.setTargetText(statustext,findViewById(android.R.id.content));

        locationtxt=(TextView)findViewById(R.id.location);
        visibilitySpinner = (Spinner) findViewById(R.id.visibilitySpinner);
        visibilitySpinner.setOnItemSelectedListener(this);

        VisibilitySpinnerAdapter customAdapter=new VisibilitySpinnerAdapter(getApplicationContext(),getApplicationContext().getResources().getStringArray(R.array.profile_status_title));
        visibilitySpinner.setAdapter(customAdapter);
        //parentLayout.setVisibility(GONE);

        viewPager = (ViewPager) findViewById(R.id.uploadViewPager);
        UploadPagerAdapter mMyPagerAdapter=new UploadPagerAdapter();

        viewPager.setAdapter(mMyPagerAdapter);

        if(finalData.size()>1&&uri!=null)
            viewPager.setCurrentItem(positionOfUri(uri));

        //viewPager.setPageTransformer(true, new FeedFullScreen.DepthPageTransformer());
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                //updateFeedViewed(position);
            }

            @Override
            public void onPageSelected(int position) {
                Log.e("MGG","positin "+ position);
                invalidateOptionsMenu();
                if(photoRail!=null)
                    photoRail.setSelection(position);

               /* Toast.makeText(FeedFullScreen.this, "mm  "+lastPage, Toast.LENGTH_SHORT).show();
                if(lastPage>position)
                {
                    updateFeedViewed(lastPage);
                }
                else if(lastPage<position)
                {
                    updateFeedViewed(lastPage);
                }

                ((ProgressBar)progressLayout.getChildAt(lastPage)).setProgress(0);
                ((ProgressBar)progressLayout.getChildAt(lastPage)).setSecondaryProgress(0);
                lastPage=position;
                ((ProgressBar)progressLayout.getChildAt(lastPage)).setProgress(1);



                timeText.setText(getTimeFromMils(finaldata.get(position).timestamp));*/
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        View inflateView=null;
        /*switch (numberType)
        {
            case 4:
                inflateView=View.inflate(this, R.layout.view_upload_text,parentLayout);
                findViewById(R.id.defaultEditor).setVisibility(View.INVISIBLE);
                statustext=(EditText) inflateView.findViewById(R.id.textFeedPost);
                break;
            case 5:
                inflateView=View.inflate(this, R.layout.view_image,parentLayout);
               // parentLayout.addView(inflateView,0);
                uploadImg=(ImageView)inflateView.findViewById(R.id.singleImage);
                content = Uri.parse(uri);
                uploadImg.setImageURI(content);
                uricropped=uri;
                break;
            case 6:

                inflateView=View.inflate(this, R.layout.view_video,parentLayout);
                //parentLayout.addView(inflateView,0);
                //parentLayout.setLayoutParams(LinearLayout.GR);
                //uploadVideo=(VideoView)inflateView.findViewById(R.id.singleVideo);


                content = Uri.parse(uri);
                //uploadVideo.setVideoURI(content);
                //uploadVideo.seekTo(10);
                MediaController mediaControls=new MediaController(this);
                if (mediaControls == null) {

                    mediaControls = new MediaController(this);

                }

                final VideoView singleVideo=(VideoView) inflateView.findViewById(R.id.singleVideo);
                singleVideo.setVideoURI(content);
                //singleVideo.setMediaController(mediaControls);
                singleVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        singleVideo.seekTo(10);


                    }
                });
                singleVideo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        singleVideo.start();
                    }
                });

                videoTexture=(TextureView) inflateView.findViewById(R.id.singleVideoTexture);

                videoTexture.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {
                    @Override
                    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height) {
                        Surface surface = new Surface(surfaceTexture);
                        // = new MediaPlayer();
                        try {
                            mMediaPlayer = new MediaPlayer();
                            mMediaPlayer.setDataSource(getApplicationContext(), Uri.parse(uri));
                            mMediaPlayer.setSurface(surface);
                            mMediaPlayer.setLooping(true);


                            // don't forget to call MediaPlayer.prepareAsync() method when you use constructor for
                            // creating MediaPlayer
                            mMediaPlayer.prepareAsync();
                            // Play video when the media source is ready for playback.
                            mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                @Override
                                public void onPrepared(final MediaPlayer mediaPlayer) {
                                    videoTexture.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mediaPlayer.start();
                                        }
                                    });
                                    mediaPlayer.seekTo(10);

                                }
                            });

                        } catch (IllegalArgumentException e) {
                            //Log.d(TAG, e.getMessage());
                        } catch (SecurityException e) {
                            //Log.d(TAG, e.getMessage());
                        } catch (IllegalStateException e) {
                            //Log.d(TAG, e.getMessage());
                        } catch (IOException e) {
                            //Log.d(TAG, e.getMessage());
                        }
                    }

                    @Override
                    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

                    }

                    @Override
                    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                        return false;
                    }

                    @Override
                    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

                    }
                });




                break;
            case 7:
                inflateView=View.inflate(this, R.layout.view_gif,parentLayout);
                break;

        }*/
        locationtxt.setVisibility(GONE);
        if(receiverAcitvity==0)
        {
            UserWholeData userData=ChatWindow.returnSingleUserDetails(getApplicationContext(),userId);
            if(userData!=null)
            {

                chatUserImage=findViewById(R.id.chatUserImage);
                mediaSharedUsername=findViewById(R.id.mediaSharedUsername);
                if (checkIfPrivacyShouldBeActivie(userData.priv_profile_picture,userData.remoteContact))
                {
                    /*if(userData.img_url.equals("null"))
                    {
                        chatUserImage.setImageResource(R.drawable.default_user);
                    }
                    else
                    {*/
                    chatUserImage.setImageURI(Uri.parse(userData.img_url));
                    // }
                }
                else
                {
                    chatUserImage.setImageResource(R.drawable.default_user);
                }
                mediaSharedUsername.setText(userData.user_name);

            }
            else
            {
                chatUserImage.setVisibility(GONE);
                mediaSharedUsername.setVisibility(GONE);
            }
            visibilitySpinner.setVisibility(GONE);


        }
        else if(receiverAcitvity==2)
        {
            UserWholeData userData=ChatWindow.returnSingleUserDetails(getApplicationContext(), FirebaseAuth.getInstance().getUid());
            chatUserImage=findViewById(R.id.chatUserImage);
            chatUserImage.setImageURI(Uri.parse(userData.img_url));
            mediaSharedUsername=findViewById(R.id.mediaSharedUsername);
            mediaSharedUsername.setVisibility(GONE);
            visibilitySpinner.setVisibility(View.VISIBLE);
            //locationtxt.setVisibility(View.VISIBLE);
            fetchLocation();
        }




    }


    public void fetchLocation() {
        if (TabbedActivity.isLocationEnabled(this)) {

            TabbedActivity.fetchNativeLocation(new LocationReceiver() {
                @Override
                public void onLocationReceived(Location data) {
                    latitute = data.getLatitude();
                    longitude = data.getLongitude();
                    fetchAnotherName(data);
                }
            },this,false);
        } else
        {
            SetProfile.showGPSOffAlert(this);
        }
    }

    public void fetchAnotherName(Location location)
    {
        Address address= LocationUtils.getAddressFromLocation(this,location);
        if(address!=null){

            String tempText=address.getLocality();
            if(tempText==null)
                tempText=address.getFeatureName();
            if(tempText==null)
            {
                fetchLocation();
                return;
            }
            locationtxt.setVisibility(View.VISIBLE);
            locationtxt.setText(tempText+", " +address.getCountryName());
            //Log.e("GGG","Location name: "+address.getLocality() + ", " + address.getAddressLine(0)+", "+address.getCountryName());
            LocationUtils.setUserLocation(this,location,address);
            locationName=locationtxt.getText().toString();
            //locationFecthSuccess=true;
        }
        else
        {
            fetchLocation();
        }

    }

    int positionOfUri(String urll)
    {
        int position=finalData.size()-1;
        for (int i=0;i<finalData.size();i++) {

            if(urll.equals(finalData.get(i).uri))
            {
                // Log.e("MGG"," Now "+ urll.replace("/","")+" / "+finalData.get(i).uri.replace("/","")+" / "+i);

                position=i;

                break;
            }
        }
        return position;
    }

    class UploadPagerAdapter extends PagerAdapter
    {
        public ScribbleView scribble_view;
        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public Object instantiateItem(ViewGroup collection,final int position) {
            ViewGroup layout;
            int resId = 0;
            Log.e("MGG","doing attt "+finalData.get(position).type);
            scribble_view=null;
            switch (finalData.get(position).type) {
                case 0:

                    layout=(ViewGroup) getLayoutInflater().inflate(R.layout.view_upload_text,null);
                    findViewById(R.id.defaultEditor).setVisibility(View.INVISIBLE);
                    emotimg=layout.findViewById(R.id.emotimg);
                    statustext= layout.findViewById(R.id.textFeedPost);
                    emotimg.setTargetText(statustext,layout);

                    break;
                    /*layout = (ViewGroup) getLayoutInflater().inflate(R.layout.view_text, null);
                    findViewById(R.id.defaultEditor).setVisibility(View.INVISIBLE);
                    statustext=(EditText) layout.findViewById(R.id.textFeedPost);*/
                case 1:
                    Log.e("MGG","doing image");
                    layout = (ViewGroup) getLayoutInflater().inflate(R.layout.view_image, null);


                    AroundDownloadImageView aroundImage;
                    aroundImage=layout.findViewById(R.id.singleAroundImageView);
                    aroundImage.setLocallyAvailable(true);
                    aroundImage.setImageViewUri(Uri.parse(finalData.get(position).uri));
                    //aroundImage.setTag("imageView");
                   /* scribbleView=
                    ScribbleView scribble_view;*/
                    // scribble_view=layout.findViewById(R.id.scribble_view);
                    // scribble_view.setImage();
                    //scribble_view.setImage(Uri.parse(finalData.get(position).uri));
                    Log.e("GGG","bitbit5 seting image settings  is "+Uri.parse(finalData.get(position).uri));
                   /* scribble_view.setTouchListener(new MapsFragment.OnTouchListener() {
                        @Override
                        public void onTouch() {
                            viewPager.requestDisallowInterceptTouchEvent(true);
                        }
                    });
                    scribble_view.setMotionViewCallback(motionViewCallback);*/

                    // uploadImg=(ImageView)layout.findViewById(R.id.singleImage);
                    // content = Uri.parse(finalData.get(position).uri);
                    // uploadImg.setImageURI(content);
                    uricropped=uri;
                    break;
                case 2:
                    layout = (ViewGroup) getLayoutInflater().inflate(R.layout.view_audio, null);

                    //android:id="@+id/singleAroundVideoView"

                    AroundDownloadAudioView aroundAudio;

                    aroundAudio=layout.findViewById(R.id.singleAroundAudioView);
                    aroundAudio.setLocallyAvailable(true);
                    aroundAudio.setUpAudioUri(Uri.parse(finalData.get(position).uri));
                    break;
                case 3:

                    Log.e("MGG","doing video");
                    layout = (ViewGroup) getLayoutInflater().inflate(R.layout.view_video,null);

                    content = Uri.parse(finalData.get(position).uri);
                    final Uri uriVar=content;
                    AroundDownloadVideoView aroundVideo;

                    aroundVideo=layout.findViewById(R.id.singleAroundVideoView);
                    aroundVideo.setLocallyAvailable(true);
                    aroundVideo.setUpVideoUri(Uri.parse(finalData.get(position).uri));





                    //uploadVideo.setVideoURI(content);
                    //uploadVideo.seekTo(10);
                    /*videoTexture=(TextureView) layout.findViewById(R.id.singleVideoTexture);

                    videoTexture.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {
                        @Override
                        public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height) {
                            Surface surface = new Surface(surfaceTexture);
                            // = new MediaPlayer();
                            try {
                                mMediaPlayer = new MediaPlayer();
                                mMediaPlayer.setDataSource(getApplicationContext(), uriVar);
                                mMediaPlayer.setSurface(surface);
                                mMediaPlayer.setLooping(true);

                                // don't forget to call MediaPlayer.prepareAsync() method when you use constructor for
                                // creating MediaPlayer
                                mMediaPlayer.prepareAsync();
                                // Play video when the media source is ready for playback.
                                mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                    @Override
                                    public void onPrepared(final MediaPlayer mediaPlayer) {
                                        videoTexture.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                mediaPlayer.start();
                                            }
                                        });
                                        mediaPlayer.seekTo(10);

                                    }
                                });

                            } catch (IllegalArgumentException e) {
                                //Log.d(TAG, e.getMessage());
                            } catch (SecurityException e) {
                                //Log.d(TAG, e.getMessage());
                            } catch (IllegalStateException e) {
                                //Log.d(TAG, e.getMessage());
                            } catch (IOException e) {
                                //Log.d(TAG, e.getMessage());
                            }
                        }

                        @Override
                        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

                        }

                        @Override
                        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                            return false;
                        }

                        @Override
                        public void onSurfaceTextureUpdated(SurfaceTexture surface) {

                        }
                    });*/



                    break;
                case 5:
                    Log.e("MGG","doing gif");
                    layout = (ViewGroup) getLayoutInflater().inflate(R.layout.view_gif, null);

                    //android:id="@+id/singleAroundVideoView"

                    AroundDownloadGifView aroundGif;

                    aroundGif=layout.findViewById(R.id.singleAroundGifView);
                    aroundGif.setLocallyAvailable(true);
                    aroundGif.setUpGifUri(Uri.parse(finalData.get(position).uri));






                    break;
                default:
                    layout = null;


            }
            statustext.setText(finalData.get(position).text);

            statustext.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    finalData.get(position).text=s.toString();
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            collection.addView(layout);
            return layout; // return selected view.
        }

        @Override
        public int getCount() {
            return finalData.size(); // number of maximum views in View Pager.
        }
        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((View) view);
        }



        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1; // return true if both are equal.
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mMediaPlayer!= null) {
            // Make sure we stop video and release resources when activity is destroyed.
            mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }

    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    String store_bitmap_get_url(Bitmap bit)
    {
        File localFile = null;
        // File localFile2 = null;
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        getResizedBitmap(bit,30).compress(Bitmap.CompressFormat.PNG, 1, bytes);

        try {
            localFile = File.createTempFile("images", "png");
            FileOutputStream fos = new FileOutputStream(localFile);
            fos.write(bytes.toByteArray());
            fos.close();
            return localFile.toURI().toString();
            // localFile2 = File.createTempFile("images", "png");
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "null";


    }

    /*public void fetchLocation() {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (android.support.v4.app.ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && android.support.v4.app.ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                mLocationCallback,
                null );
    }*/

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
        Toast.makeText(getApplicationContext(), position+"", Toast.LENGTH_LONG).show();
        visibilityMode=position;
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK)
        {
            //Toast.makeText(this, "ooohhhh", Toast.LENGTH_SHORT).show();
            uploadImg.setImageURI(null);
            uricropped=data.getStringExtra("uri");
            uploadImg.setImageURI(Uri.parse(uricropped));
        }
        if (requestCode == 5 && resultCode == RESULT_OK)
        {


            Uri uri =data.getData();
            Log.e("GGG","bitbit5 got file success "+uri);
            //Log.e("GGG","scribble image is "+uri);

            finalData.get(viewPager.getCurrentItem()).uri=uri.toString();

            //AroundDownloadImageView aroundImage=new AroundDownloadImageView(this);
            //aroundImage=aroundImage.findViewWithTag("imageView");

            // imageView
            //viewPager.getAdapter().();
            viewPager.getAdapter().notifyDataSetChanged();



        }

    }

    public void uploadContent(View v)
    {
        /*Intent intent = new Intent();

        switch (numberType) {
            case 4:
                intent.putExtra("uri","null");
                intent.putExtra("numberType",0);

                break;
            case 5:
                intent.putExtra("uri",uricropped);
                intent.putExtra("numberType",1);
                break;
            case 6:
                intent.putExtra("uri",content.toString());

                if(videoTexture.getBitmap()==null)
                {
                    Log.e("MSG","again null");
                }
                else
                {
                    intent.putExtra("semi_uri",store_bitmap_get_url(videoTexture.getBitmap()));
                }

                intent.putExtra("numberType",2);

                break;
        }*/


        /*intent.putExtra("text",statustext.getText().toString());
        intent.putExtra("visibility",visibilityMode);
        intent.putExtra("locname",locationName);
        TabFragment2.latitude=latitute;
        TabFragment2.longitude=longitude;

        statustext.setText("");

        if(callerType==1)
        {
            setResult(RESULT_OK, intent);
            finish();
        }*/


        if(receiverAcitvity==0) {
            Intent broadcast = new Intent("around.NEWCHAT");
            broadcast.putExtra("mediaarray", finalData);
            sendBroadcast(broadcast);


           /* Intent intent3 = new Intent(this, ChatWindow.class);
            intent3.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent3);*/
            finish();
        }
        else if(receiverAcitvity==2)
        {
            for(int i=0;i<finalData.size();i++)
            {
                finalData.get(i).location=locationName;
                finalData.get(i).privacy=visibilityMode;
            }
            TabFragment2.latitude=latitute;
            TabFragment2.longitude=longitude;

            Intent broadcast = new Intent("around.NEWFEED");
            broadcast.putExtra("mediaarray", finalData);
            sendBroadcast(broadcast);

            //Intent intent3 = new Intent(this, TabbedActivity.class);
            // intent3.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP /*| Intent.FLAG_ACTIVITY_SINGLE_TOP*/);
            //startActivity(intent3);

            finish();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        menu.clear();
        getMenuInflater().inflate(R.menu.menu_image_upload, menu);
        if(finalData.get(viewPager.getCurrentItem()).type==1)
        {
            menu.findItem(R.id.action_editimage).setVisible(true);
        }
        else
        {
            menu.findItem(R.id.action_editimage).setVisible(false);
        }

        if(receiverAcitvity==2)
        {
            menu.findItem(R.id.action_settings).setVisible(true);
        }
        else
        {
            menu.findItem(R.id.action_settings).setVisible(false);
        }


        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_editimage) {
            Intent cropIntent=new Intent(this,MediaEditActivity.class);
            cropIntent.putExtra("uri",uri);
            startActivityForResult(cropIntent,5);
            //Toast.makeText(this, "cc", Toast.LENGTH_SHORT).show();
            return true;
        }
        else if(id==R.id.action_settings)
        {
            Intent intent=new Intent(this,SettingsActivity.class);
            intent.putExtra("stat",3);
            startActivity(intent);
        }
        /*if(id==R.id.action_brush){
            onBrushSelected(true);
        }
        else if(id==R.id.action_sticker){
            onStickerSelected(true);
        }
        else if(id==R.id.action_text){
            onTextSelected(true);
        }
        else if(id==R.id.action_undo){
            onPaintUndo();
        }*/




        return super.onOptionsItemSelected(item);
    }


}
