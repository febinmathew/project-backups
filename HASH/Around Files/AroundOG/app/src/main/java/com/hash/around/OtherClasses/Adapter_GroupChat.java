package com.hash.around.OtherClasses;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hash.around.MediaViewer;
import com.hash.around.OtherClasses.DataTypes.GroupChatData;
import com.hash.around.OtherClasses.DataTypes.LocationData;
import com.hash.around.OtherClasses.Interfaces.ProcessCompletedListner;
import com.hash.around.R;
import com.hash.around.TabbedActivity;
import com.vanniktech.emoji.EmojiTextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;



import static com.hash.around.TabbedActivity.checkLocationExists;
import static com.hash.around.TabbedActivity.saveLocationToDatabase;

/**
 * Created by Febin on 08-10-2017.
 */

public class Adapter_GroupChat extends RecyclerView.Adapter implements View.OnCreateContextMenuListener{

    Context mContext;
    ArrayList<GroupChatData> data;
    Activity act;
    public static int optionPosition=-1;
    public SparseBooleanArray selectedItems;

    private ProcessCompletedListner onLoadMoreListener;
    private int visibleThreshold = 1;
    private int lastVisibleItem,prevlastVisibleItem=-2, totalItemCount;
    private boolean loading;

    public void toggleSelection(int pos) {
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
        }
        else {
            selectedItems.put(pos, true);
        }
        notifyItemChanged(pos);
    }

    public void clearSelections() {
        selectedItems.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items =
                new ArrayList<Integer>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }

    public int deleteSelectedItems() {
        //List<Integer> items =new ArrayList<Integer>(selectedItems.size());
        ArrayList<Long> ids = new ArrayList<Long>();
        for (int i = selectedItems.size()-1; i >= 0; i--) {

            int pos=selectedItems.keyAt(i);
            ids.add(data.get(pos).sl);
            data.remove(selectedItems.keyAt(i));
        }

        String[] names=new String[ids.size()];
        for(int i=0;i<names.length;i++)
        {
            names[i]=ids.get(i)+"";
        }
        SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();
        String query = "DELETE FROM "+ DatabaseAccesser.chat_table+ " WHERE "+DatabaseAccesser.chat_sl_no+" IN (" + makePlaceholders(names.length) + ")";
        Cursor cursor = db.rawQuery(query, names);

        selectedItems.clear();
        notifyDataSetChanged();
        return cursor.getCount();
    }

    public void clearChatHistory(String userId)
    {
        SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();

        db.delete(DatabaseAccesser.chat_table, DatabaseAccesser.chat_userid + "=?", new String[]{userId});
        data.clear();

        ContentValues values2 = new ContentValues();

        values2.put(DatabaseAccesser.chatList_lastmsg, "");
        values2.put(DatabaseAccesser.chatList_lastmsg_type, 0);
        values2.put(DatabaseAccesser.chatList_msgqueue, 0);

        int rowln=db.update(DatabaseAccesser.chatList_table,values2, DatabaseAccesser.chatList_userid+" =?",new String[]{userId});
        //db.delete(DatabaseAccesser.chatList_table, DatabaseAccesser.chatList_userid + "=?", new String[]{userId});
        notifyDataSetChanged();
    }


    String makePlaceholders(int len) {
        if (len < 1) {
            // It will lead to an invalid query anyway ..
            throw new RuntimeException("No placeholders");
        } else {
            StringBuilder sb = new StringBuilder(len * 2 - 1);
            sb.append("?");
            for (int i = 1; i < len; i++) {
                sb.append(",?");
            }
            return sb.toString();
        }
    }


    public Adapter_GroupChat(Context mcont, ArrayList<GroupChatData> list, Activity activity, RecyclerView recyclerView)
    {
        mContext=mcont;
        data=list;
        act=activity;
        selectedItems=new SparseBooleanArray();

        final Context ccc=mContext;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                .getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView,
                                   int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();
                 if (lastVisibleItem <=0) {
                // End has been reached
                     if(!loading && prevlastVisibleItem!=lastVisibleItem) {
                         prevlastVisibleItem = lastVisibleItem;
                         Toast.makeText(ccc, "chat : " + totalItemCount + " " + lastVisibleItem, Toast.LENGTH_SHORT).show();
                         // Do something
                         if (onLoadMoreListener != null) {
                             onLoadMoreListener.processCompleteAction();
                         }
                         //loading = true;
                     }
                }


            }
        });
    }

    /*public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }*/

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v=null;
        switch(viewType) {
            case 0:

                 v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_sent, parent, false);
                // set the view's size, margins, paddings and layout parameters

                Adapter_GroupChat.ViewHolder0 vh = new Adapter_GroupChat.ViewHolder0(v);
                return vh;
            case 1:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_receive, parent, false);
                // set the view's size, margins, paddings and layout parameters

                Adapter_GroupChat.ViewHolder1 vh1 = new Adapter_GroupChat.ViewHolder1(v);
                return vh1;
            case 2:

                v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_sent_image, parent, false);
                // set the view's size, margins, paddings and layout parameters

                Adapter_GroupChat.ViewHolder2 vh2 = new Adapter_GroupChat.ViewHolder2(v);
                return vh2;
            case 3:

                v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_receive_image, parent, false);
                // set the view's size, margins, paddings and layout parameters

                Adapter_GroupChat.ViewHolder2 vh3 = new Adapter_GroupChat.ViewHolder2(v);
                return vh3;

            case 8:

                v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_sent_location, parent, false);
                // set the view's size, margins, paddings and layout parameters

                Adapter_GroupChat.ViewHolder3 vh4 = new Adapter_GroupChat.ViewHolder3(v);
                return vh4;

        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final int pos=position;
        Date dt=new Date(data.get(position).timestamp);
        SimpleDateFormat format = new SimpleDateFormat("hh:mm a");
        switch(data.get(position).sender) {
            case 0:
                switch(data.get(position).type){
                    case 0:
                        ((ViewHolder0)holder).username.setVisibility(View.VISIBLE);
                        ((ViewHolder0)holder).username.setText(data.get(pos).name);
                        ((ViewHolder0)holder).username.setOnCreateContextMenuListener(this);

                        ((ViewHolder0)holder).username.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                optionPosition= pos;
                                view.showContextMenu();
                            }
                        });


                        ((ViewHolder0)holder).msg.setText(data.get(position).msg);
                        ((ViewHolder0)holder).date.setText(format.format(dt));
                        switch(data.get(position).msgseen)
                        {
                            case 0:
                                ((ViewHolder0)holder).msgStatus.setImageResource(R.drawable.ic_progress_clock);
                                break;
                            case 1:
                                ((ViewHolder0)holder).msgStatus.setImageResource(R.drawable.ic_single_tick);
                                break;
                            case 2:
                                ((ViewHolder0)holder).msgStatus.setImageResource(R.drawable.ic_double_tick);
                                break;
                        }
                        if(selectedItems.get(position))
                        {
                            ((ViewHolder0)holder).itemView.setBackgroundColor( ContextCompat.getColor(mContext, R.color.chat_selection));

                        }
                        else
                        {
                            ((ViewHolder0)holder).itemView.setBackgroundColor( ContextCompat.getColor(mContext, R.color.transparent));

                        }
                        break;
                    case 1:
                        if(selectedItems.get(position))
                        {
                            ((ViewHolder2)holder).itemView.setBackgroundColor( ContextCompat.getColor(mContext, R.color.chat_selection));

                        }
                        else
                        {
                            ((ViewHolder2)holder).itemView.setBackgroundColor( ContextCompat.getColor(mContext, R.color.transparent));

                        }
                        ((ViewHolder2)holder).chatImage.setImageURI(Uri.parse(data.get(position).final_url));

                        ((ViewHolder2)holder).chatImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view)
                            {
                                Intent intent = new Intent(mContext.getApplicationContext(), MediaViewer.class);
                                intent.putExtra("numberType", 5);
                                intent.putExtra("uri",data.get(pos).final_url);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                               /* ActivityOptionsCompat options = ActivityOptionsCompat
                                        .makeSceneTransitionAnimation(act,view, "robot");*/
                                mContext.startActivity(intent);
                            }
                        });
                    break;
                    case 4:
                        if(selectedItems.get(position))
                        {
                            ((ViewHolder3)holder).itemView.setBackgroundColor( ContextCompat.getColor(mContext, R.color.chat_selection));

                        }
                        else
                        {
                            ((ViewHolder3)holder).itemView.setBackgroundColor( ContextCompat.getColor(mContext, R.color.transparent));

                        }
                        ((ViewHolder3)holder).locationText.setText(data.get(position).msg);

                        ((ViewHolder3)holder).latitudeText.setText(data.get(position).temp_url+", "+data.get(position).final_url);



                        switch(data.get(position).msgseen)
                        {
                            case 0:
                                ((ViewHolder3)holder).msgStatus.setImageResource(R.drawable.ic_progress_clock);
                                break;
                            case 1:
                                ((ViewHolder3)holder).msgStatus.setImageResource(R.drawable.ic_single_tick);
                                break;
                            case 2:
                                ((ViewHolder3)holder).msgStatus.setImageResource(R.drawable.ic_double_tick);
                                break;
                        }

                        ((ViewHolder3)holder).date.setText(format.format(dt));

                        ((ViewHolder3)holder).addLocation.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                final LocationData temp =new LocationData();
                                temp.status=0;
                                temp.location_name=data.get(pos).msg;
                                temp.latitude=Double.parseDouble(data.get(pos).temp_url);
                                temp.longitude=Double.parseDouble(data.get(pos).final_url);

                                tempFun(temp,pos);


                            }
                        });


                        break;
                }
                break;
            case 1:
                switch(data.get(position).type){
                    case 0:
                        ((ViewHolder1)holder).msg.setText(data.get(position).msg);
                        ((ViewHolder1)holder).date.setText(format.format(dt));
                        if(selectedItems.get(position))
                        {
                            ((ViewHolder1)holder).itemView.setBackgroundColor( ContextCompat.getColor(mContext, R.color.chat_selection));

                        }
                        else
                        {
                            ((ViewHolder1)holder).itemView.setBackgroundColor( ContextCompat.getColor(mContext, R.color.transparent));

                        }
                        break;
                    case 1:

                        if(selectedItems.get(position))
                        {
                            ((ViewHolder2)holder).itemView.setBackgroundColor( ContextCompat.getColor(mContext, R.color.chat_selection));

                        }
                        else
                        {
                            ((ViewHolder2)holder).itemView.setBackgroundColor( ContextCompat.getColor(mContext, R.color.transparent));

                        }
                        ((ViewHolder2)holder).chatImage.setImageURI(Uri.parse(data.get(position).final_url));

                        ((ViewHolder2)holder).chatImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(mContext.getApplicationContext(), MediaViewer.class);
                                intent.putExtra("numberType", 5);
                                intent.putExtra("uri",data.get(pos).final_url);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                               /* ActivityOptionsCompat options = ActivityOptionsCompat
                                        .makeSceneTransitionAnimation(act,view, "robot");*/
                                mContext.startActivity(intent);
                            }
                        });
                        break;
                }
                break;
        }
    }


    void tempFun(final LocationData temp,int pos)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder( mContext);

        builder.setCancelable(true);

        if(!checkLocationExists(temp)) {
            builder.setMessage("Add "+data.get(pos).msg+"?");
            builder.setPositiveButton("ADD", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    saveLocationToDatabase(mContext,temp);
                    dialog.dismiss();
                }});
            builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

        }
        else
        {
            builder.setTitle("Location already exist!");
            builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public int getItemViewType(int position) {
        switch(data.get(position).sender)
        {
            case 0:
                switch(data.get(position).type)
                {
                    case 0:
                        return 0;
                    case 1:
                        return 2;
                    case 4:
                        return 8;

                }
                return 0;

            case 1:
                switch(data.get(position).type)
                {
                    case 0:
                        return 1;
                    case 1:
                        return 3;

                }
                return 1;

        }




        return Integer.parseInt(null);
    }

    @Override
    public int getItemCount()
    {
        if(data!=null) {
            return data.size();
        }
        return 0;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        menu.clear();
        MenuInflater inflater = new MenuInflater(mContext);
        inflater.inflate(R.menu.chatwindow_pop_up, menu);
       // menu.setHeaderTitle(data.get(optionPosition).name);
        //menu.setQwertyMode(true);
        if(data.get(optionPosition).userid.equals(TabbedActivity.userdetail.getUid())) {

            menu.findItem(R.id.chat_poke).setVisible(false);
            menu.findItem(R.id.chat_message).setVisible(false);

            menu.findItem(R.id.view_profile).setTitle("My profile");
        }
        else
        {
            menu.findItem(R.id.chat_message).setTitle("Message "+(data.get(optionPosition).name));
            menu.findItem(R.id.chat_poke).setTitle("Poke "+(data.get(optionPosition).name));
        }
    }

    public static class ViewHolder0 extends RecyclerView.ViewHolder {
        // public CardView cv;
       // public TextView tx;
       // public ImageView img;
        public ImageView msgStatus;

       public TextView msg;
       public TextView date;
        public TextView username;

        public ViewHolder0(View item) {
            super(item);
            //cv=(CardView)item.findViewById(R.id.cView);
            //tx=(TextView)item.findViewById(R.id.chattextMin);
           // img=(ImageView)item.findViewById(R.id.ccImage);
            msgStatus=(ImageView)item.findViewById(R.id.statusBubble);
            msg=(TextView) item.findViewById(R.id.chatText);
            date=(TextView) item.findViewById(R.id.chatDateText);
            username=(TextView) item.findViewById(R.id.grpusername);

        }
    }

    public static class ViewHolder1 extends RecyclerView.ViewHolder {


        public EmojiTextView msg;
        public TextView date;

        public ViewHolder1(View item) {
            super(item);

            msg=(EmojiTextView)item.findViewById(R.id.chatText);
            date=(TextView) item.findViewById(R.id.chatDateText);

        }
    }

    public static class ViewHolder2 extends RecyclerView.ViewHolder {
        // public CardView cv;
        // public TextView tx;
       /* public ImageView img;*/
        public ImageView chatImage;
        public ImageView msgStatus;
        public TextView date;
        //public EmojiconTextView chatMsg;

        public ViewHolder2(View item) {
            super(item);
            //cv=(CardView)item.findViewById(R.id.cView);
            //tx=(TextView)item.findViewById(R.id.chattextMin);
           /* img=(ImageView)item.findViewById(R.id.ccImage);*/
            chatImage=(ImageView)item.findViewById(R.id.chatMainImageView);
            msgStatus=(ImageView)item.findViewById(R.id.statusBubble);
            date=(TextView) item.findViewById(R.id.chatDateText);
            //chatMsg=(EmojiconTextView)item.findViewById(R.id.chatMsg);

        }
    }

    public static class ViewHolder3 extends RecyclerView.ViewHolder {

        public TextView locationText;
        public TextView latitudeText;
        public ImageView msgStatus;
        public TextView date;
        public TextView addLocation;
       // public TextView date;
        //public EmojiconTextView chatMsg;

        public ViewHolder3(View item) {
            super(item);

            locationText=(TextView)item.findViewById(R.id.locationText);
            latitudeText=(TextView)item.findViewById(R.id.latitudeText);
            addLocation=(TextView)item.findViewById(R.id.addLocation);
            msgStatus=(ImageView)item.findViewById(R.id.statusBubble);
            date=(TextView) item.findViewById(R.id.chatDateText);


        }
    }



}