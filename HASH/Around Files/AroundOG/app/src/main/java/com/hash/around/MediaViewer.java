package com.hash.around;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.firebase.storage.FirebaseStorage;
import com.hash.around.OtherClasses.ChatDataAdapter;
import com.hash.around.OtherClasses.DataTypes.GroupChatData;
import com.hash.around.OtherClasses.DataTypes.MediaData;
import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.OtherClasses.Interfaces.ChatSentListner;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.Utils.AppOnlineStatusManager;
import com.hash.around.Utils.DatabaseManager;
import com.hash.around.Utils.StringUtils;
import com.hash.around.Views.Medias.AroundCircularImageView;
import com.hash.around.Views.Medias.AroundDownloadAudioView;
import com.hash.around.Views.Medias.AroundDownloadGifView;
import com.hash.around.Views.Medias.AroundDownloadImageView;
import com.hash.around.Views.Medias.AroundDownloadVideoView;
import com.hash.around.Views.scribbles.widget.ScribbleView;
import com.vanniktech.emoji.EmojiTextView;

import java.io.Serializable;
import java.util.ArrayList;

import static android.view.View.GONE;
import static com.hash.around.OtherClasses.Adapter_Contacts.checkIfPrivacyShouldBeActivie;

public class MediaViewer extends AppCompatActivity/* implements AdapterView.OnItemSelectedListener*/{

    Long mediaID;
    String uricropped;
    String userID;
    LinearLayout parentLayout;
    ImageView uploadImg;
    EmojiTextView statustext;
    Uri content;
    VideoView uploadVideo;
    ArrayList<GroupChatData> finaldata=new ArrayList<>();
    public static int visibilityMode;
    String[] visibilityNames={"Public","Nearby people","Contacts only","Only me"};

    double latitute;
    double longitude;
    String locationName;


    private LocationCallback mLocationCallback;
    private FusedLocationProviderClient mFusedLocationClient;
    LocationRequest mLocationRequest;

    TextView locationtxt;
    ViewPager viewPager;

    boolean foundItem=false;
    TextView mediaSharedUsername;
    UserWholeData userData;
    AroundCircularImageView chatUserImage;
    int pos=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.media_viewer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.comtoolbar2);

        toolbar.findViewById(R.id.up_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaViewer.super.onBackPressed();
            }
        });
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        mediaID= getIntent().getLongExtra("mediaID",-1);
        userID=getIntent().getStringExtra("userID");

        parentLayout=(LinearLayout)findViewById(R.id.parentLayout);
        statustext=(EmojiTextView)findViewById(R.id.image_feed_status) ;
        mediaSharedUsername=findViewById(R.id.mediaSharedUsername) ;
        chatUserImage=(AroundCircularImageView) findViewById(R.id.chatUserImage);

        locationtxt=(TextView)findViewById(R.id.location);
        finaldata=returnSingleUsersMedias(getApplicationContext(),userID);
        userData=ChatWindow.returnSingleUserDetails(this,userID);

        if(userData!=null)
        {
            mediaSharedUsername.setText(userData.user_name);

        if (checkIfPrivacyShouldBeActivie(userData.priv_profile_picture,userData.remoteContact))
        {
            if(userData.img_url.equals("null"))
                chatUserImage.setImageResource(R.drawable.default_user);
            else
                chatUserImage.setImageURI(Uri.parse(userData.img_url));
        }
        else
        {
            chatUserImage.setImageResource(R.drawable.default_user);}
        }
        else
        {
            chatUserImage.setVisibility(GONE);
            mediaSharedUsername.setVisibility(GONE);
        }

        viewPager = (ViewPager) findViewById(R.id.mediaViewPager);
        MediaPagerAdapter mMyPagerAdapter=new MediaPagerAdapter();

        viewPager.setAdapter(mMyPagerAdapter);

        if (mediaID != -1) {
            for (int i = 0; i < finaldata.size(); i++) {
                if (finaldata.get(i).media_id == mediaID) {
                    pos = i;
                    foundItem = true;
                    break;
                }}
        }
        if(foundItem)
            viewPager.setCurrentItem(pos);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setUpStatusText();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        setUpStatusText();

    }
    @Override
    protected void onStart() {
        super.onStart();
        AppOnlineStatusManager.OnlineStatus.setActive(true);

    }
    @Override
    public void onStop() {
        AppOnlineStatusManager.OnlineStatus.setActive(false);
        super.onStop();
    }
    void setUpStatusText()
    {
        String txt=finaldata.get(viewPager.getCurrentItem()).msg;
        if(txt==null || txt.equals("")||txt.equals("null"))
            statustext.setVisibility(GONE);
        else
            statustext.setText(txt);
    }
    class MediaPagerAdapter extends PagerAdapter
    {
        public ScribbleView scribble_view;
        @Override
        public Object instantiateItem(ViewGroup collection, final int position) {
            ViewGroup layout;
            int resId = 0;

            scribble_view=null;
            switch (finaldata.get(position).type) {
               /* case 0:

                    layout=(ViewGroup) getLayoutInflater().inflate(R.layout.view_upload_text,null);
                    findViewById(R.id.defaultEditor).setVisibility(View.INVISIBLE);
                    statustext=(EmojiTextView) layout.findViewById(R.id.textFeedPost);
                    break;*/
                    /*layout = (ViewGroup) getLayoutInflater().inflate(R.layout.view_text, null);
                    findViewById(R.id.defaultEditor).setVisibility(View.INVISIBLE);
                    statustext=(EditText) layout.findViewById(R.id.textFeedPost);*/
                case 1:
                    //Log.e("MGG","doing image");
                    layout = (ViewGroup) getLayoutInflater().inflate(R.layout.view_image, null);
                    final AroundDownloadImageView aroundImage;
                    aroundImage=layout.findViewById(R.id.singleAroundImageView);
                    aroundImage.setLocallyAvailable(true);
                    aroundImage.setMediaType(finaldata.get(position).type);

                    aroundImage.setUpChatMedia(finaldata.get(position),new JobCompletionListner() {
                        @Override
                        public void onJobCompletion(String text1) {

                            DatabaseManager.updateMediaTableFromGlobalUrl(finaldata.get(position).final_url, text1);

                            finaldata.get(position).final_url = text1;
                            //((TextChatView)holder).aroundImageView.disableDownloadClick();
                            //notifyItemChanged(pos);
                            aroundImage.setImageViewUri(Uri.parse(text1));
                            aroundImage.setLocallyAvailable(true);
                        }
                    });
                    /*
                    if(finaldata.get(position).sender==0)
                    {
                        if(finaldata.get(position).msgseen==0) {
                            aroundImage.setSizeText(StringUtils.mediaSizeToString(finaldata.get(pos).mediaSize));
                            aroundImage.setImageViewUri(Uri.parse(finaldata.get(position).temp_url));
                            aroundImage.setChatUploadClickListner(finaldata.get(position).final_url,finaldata.get(pos)
                                    , FirebaseStorage.getInstance().getReference().child("chat_datas").child(finaldata.get(pos).userid));

                            aroundImage.setOnFileUploadListener(new ChatSentListner() {
                                @Override
                                public void onChatAdded(GroupChatData data) {

                                }

                                @Override
                                public void onChatUploaded(GroupChatData data) {
                                    aroundImage.setLocallyAvailable(true);
                                }
                            });

                        }
                        else {
                            aroundImage.setImageViewUri(Uri.parse(finaldata.get(position).final_url));
                            aroundImage.setLocallyAvailable(true);
                        }
                    }
                    else {
                        if (finaldata.get(position).final_url == null || finaldata.get(position).final_url.contains("null")||finaldata.get(position).final_url.contains("http")) {
                            aroundImage.setLocallyAvailable(false);
                            aroundImage.setImageViewUri(Uri.parse(finaldata.get(position).temp_url));

                            aroundImage.setSizeText(StringUtils.mediaSizeToString(finaldata.get(pos).mediaSize));



                            aroundImage.setOnJobCompletionListner(new JobCompletionListner() {
                                @Override
                                public void onJobCompletion(String text1) {


                                    DatabaseManager.updateMediaTableFromGlobalUrl(finaldata.get(pos).final_url, text1);

                                    finaldata.get(pos).final_url = text1;
                                    //((TextChatView)holder).aroundImageView.disableDownloadClick();
                                    //notifyItemChanged(pos);
                                    aroundImage.setImageViewUri(Uri.parse(text1));
                                    aroundImage.setLocallyAvailable(true);
                                }
                            });
                            aroundImage.setDownloadClickListner(finaldata.get(position).final_url, TabbedActivity.PREF_IMAGE);

                        } else {
                            aroundImage.setLocallyAvailable(true);
                            aroundImage.setImageViewUri(Uri.parse(finaldata.get(position).final_url));
                        }
                    }*/

                    break;
                case 2:
                    layout = (ViewGroup) getLayoutInflater().inflate(R.layout.view_audio, null);

                   final  AroundDownloadAudioView aroundAudio;
                    aroundAudio=layout.findViewById(R.id.singleAroundAudioView);
                    aroundAudio.setLocallyAvailable(true);

                    aroundAudio.setUpChatMedia(finaldata.get(position),new JobCompletionListner() {
                        @Override
                        public void onJobCompletion(String text1) {

                            DatabaseManager.updateMediaTableFromGlobalUrl(finaldata.get(position).final_url, text1);

                            finaldata.get(position).final_url = text1;
                            //((TextChatView)holder).aroundImageView.disableDownloadClick();
                            //notifyItemChanged(pos);
                            aroundAudio.setUpAudioUri(Uri.parse(text1));
                            aroundAudio.setLocallyAvailable(true);
                        }
                    });

                    /*if(finaldata.get(position).final_url==null||finaldata.get(position).final_url.contains("null"))
                    {

                        aroundAudio.setLocallyAvailable(false);
                    }
                    else{
                        aroundAudio.setLocallyAvailable(true);
                        aroundAudio.setUpAudioUri(Uri.parse(finaldata.get(position).final_url));
                    }*/
                    break;
                case 3:

                    layout = (ViewGroup) getLayoutInflater().inflate(R.layout.view_video,null);

                    //content = Uri.parse(finalData.get(position).uri);
                    final Uri uriVar=content;
                    final AroundDownloadVideoView aroundVideo;

                    aroundVideo=layout.findViewById(R.id.singleAroundVideoView);
                    aroundVideo.setLocallyAvailable(true);

                    aroundVideo.setUpChatMedia(finaldata.get(position),new JobCompletionListner() {
                    @Override
                    public void onJobCompletion(String text1) {

                        DatabaseManager.updateMediaTableFromGlobalUrl(finaldata.get(position).final_url, text1);

                        finaldata.get(position).final_url = text1;
                        //((TextChatView)holder).aroundImageView.disableDownloadClick();
                        //notifyItemChanged(pos);
                        aroundVideo.setUpVideoUri(Uri.parse(text1));
                        aroundVideo.setLocallyAvailable(true);
                    }
                });
                   /* if(finaldata.get(position).final_url==null||finaldata.get(position).final_url.contains("null"))
                    {
                        aroundVideo.setUpVideoUri(Uri.parse(finaldata.get(position).temp_url));
                    }
                    else{
                        aroundVideo.setLocallyAvailable(true);
                        aroundVideo.setUpVideoUri(Uri.parse(finaldata.get(position).final_url));
                    }*/
                    //aroundVideo.setUpVideoUri(Uri.parse(finalData.get(position).uri));
                    break;
                case 5:
                    //Log.e("MGG","doing gif");
                    layout = (ViewGroup) getLayoutInflater().inflate(R.layout.view_gif, null);

                    final AroundDownloadGifView aroundGif;

                    aroundGif=layout.findViewById(R.id.singleAroundGifView);
                    aroundGif.setLocallyAvailable(true);

                    aroundGif.setUpChatMedia(finaldata.get(position),new JobCompletionListner() {
                        @Override
                        public void onJobCompletion(String text1) {

                            DatabaseManager.updateMediaTableFromGlobalUrl(finaldata.get(position).final_url, text1);

                            finaldata.get(position).final_url = text1;
                            //((TextChatView)holder).aroundImageView.disableDownloadClick();
                            //notifyItemChanged(pos);
                            aroundGif.setUpGifUri(Uri.parse(text1));
                            aroundGif.setLocallyAvailable(true);
                        }
                    });
                    /*if(finaldata.get(position).final_url==null||finaldata.get(position).final_url.contains("null"))
                    {
                        aroundGif.setLocallyAvailable(false);
                        //aroundGif.setUpGifUri(Uri.parse(finaldata.get(position).semi_uri));
                    }
                    else{
                        aroundGif.setLocallyAvailable(true);
                        aroundGif.setUpGifUri(Uri.parse(finaldata.get(position).final_url));
                    }*/




                    break;
                default:
                    layout = null;


            }


            collection.addView(layout);
            return layout; // return selected view.
        }

        @Override
        public int getCount() {
            return finaldata.size(); // number of maximum views in View Pager.
        }
        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((View) view);
        }



        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1; // return true if both are equal.
        }
    }


    public static ArrayList<GroupChatData> returnSingleUsersMedias(Context context, String userID)
    {
        SQLiteDatabase db= DatabaseManager.getDatabase(context).getReadableDatabase();
        Cursor cursor=db.rawQuery(
                "SELECT *,"+ DatabaseAccesser.chat_table+".rowid AS rowID"+" FROM "+DatabaseAccesser.chat_table +
                        " INNER JOIN "+DatabaseAccesser.media_table+" ON "+
                        DatabaseAccesser.chat_table+"."+DatabaseAccesser.chat_col_media_id+" = "+ DatabaseAccesser.media_table+".rowid"+
                        " INNER JOIN "+DatabaseAccesser.replay_table+" ON "+
                        DatabaseAccesser.chat_table+"."+DatabaseAccesser.chat_col_replay_id+" = "+ DatabaseAccesser.replay_table+".rowid"+
                        " WHERE " +   DatabaseAccesser.chat_table+"."+DatabaseAccesser.chat_userid + " =? AND "+
                        DatabaseAccesser.chat_table+"." +DatabaseAccesser.chat_type+ " IN(?,?,?,?) "+
                        " ORDER BY "+DatabaseAccesser.chat_timestamp, new String[] { userID,"1","2","3","5" });
        ArrayList<GroupChatData> dataList=new ArrayList<>();

        while (cursor.moveToNext()) {


            GroupChatData temp = new GroupChatData();


            temp.chatRow = cursor.getLong(
                    cursor.getColumnIndexOrThrow("rowID"));
            temp.sl = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_sl_no));


            temp.userid = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_userid));
            Log.e("GGG", "row retrival : " + temp.userid);
            temp.sender = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_sender));

            if (temp.sender == 0)
                temp.sl = temp.chatRow;

            temp.type = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_type));
            temp.timestamp = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_timestamp));
            temp.chat_rece_time = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_rece_timestamp));
            temp.chat_seen_time = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_seen_timestamp));
            temp.msg = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_text));

            temp.msgseen = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_ifseen));

            temp.chatExpire = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_expire));
            temp.chatSchedule = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_schedule));


            temp.mediaSize = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.media_size));


            temp.final_url = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.media_local_uri));
            if (temp.final_url.equals("null")) {
                temp.final_url = cursor.getString(
                        cursor.getColumnIndexOrThrow(DatabaseAccesser.media_global_uri));
            }

            temp.media_id = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_col_media_id));

            temp.temp_url = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.media_lite_local_uri));

            temp.replay_id = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_col_replay_id));

            dataList.add(temp);
        }

        return  dataList;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            Toast.makeText(this, "ooohhhh", Toast.LENGTH_SHORT).show();
            uploadImg.setImageURI(null);
            uricropped=data.getStringExtra("uri");
            uploadImg.setImageURI(Uri.parse(uricropped));
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


            getMenuInflater().inflate(R.menu.media_viewer_activity, menu);

        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
       /* if (id == R.id.action_crop) {
            Intent cropIntent=new Intent(this,ImageCropActivity.class);
             cropIntent.putExtra("uri",uri);
            startActivityForResult(cropIntent,1);
           // Toast.makeText(this, "cc", Toast.LENGTH_SHORT).show();
            return true;
        }*/
        if(id==R.id.action_chat_forward)
        {
            Intent intent = new Intent(this, ChatForwardActivity.class);
            ArrayList<GroupChatData> chatsArray=new ArrayList<>();
            chatsArray.add(finaldata.get(viewPager.getCurrentItem()));
            Bundle args = new Bundle();
            args.putSerializable("ARRAYLIST",(Serializable)chatsArray);
            intent.putExtra("BUNDLE",args);
            startActivity(intent);
        }
        else if(id==R.id.action_chat_delete)
        {
            //ChatDataAdapter.deleteChatsFromRowArray(getApplicationContext(),new String[]{finaldata.get(viewPager.getCurrentItem()).chatRow+""});
           //viewPager.set
        }


        return super.onOptionsItemSelected(item);
    }
}
