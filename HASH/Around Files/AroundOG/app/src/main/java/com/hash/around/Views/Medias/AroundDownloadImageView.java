package com.hash.around.Views.Medias;

import android.content.Context;
import android.content.res.TypedArray;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnPausedListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.hash.around.OtherClasses.ChatDataAdapter;
import com.hash.around.OtherClasses.DataTypes.FeedData;
import com.hash.around.OtherClasses.DataTypes.GroupChatData;
import com.hash.around.OtherClasses.DataTypes.GroupFeedData;
import com.hash.around.OtherClasses.Interfaces.ChatSentListner;
import com.hash.around.OtherClasses.Interfaces.FeedSentListner;
import com.hash.around.OtherClasses.Interfaces.FileUploadListner;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.OtherClasses.TouchImageView;
import com.hash.around.R;
import com.hash.around.TabbedActivity;
import com.hash.around.Utils.AroundStaticInfo;
import com.hash.around.Utils.ChatSender;
import com.hash.around.Utils.DatabaseManager;
import com.hash.around.Utils.MediaManager;
import com.hash.around.Utils.StringUtils;
import com.hash.around.Views.CustomMakers.AroundDownloadView;
import com.pitt.library.fresh.FreshDownloadView;

import java.io.File;
import java.io.IOException;




public class AroundDownloadImageView extends RelativeLayout {

    private boolean centerCrop;
    private int imageMaxHeight;


    View rootView;
    Context mContext;
    TouchImageView imageContent;
    AroundDownloadView downloadView;
    LinearLayout downloadViewBG;
    TextView sizeText;
    JobCompletionListner jobComplete = null;
    public void setSizeText(String text)
    {
        sizeText.setText(text);
    }

    public AroundDownloadImageView(Context context) {
        super(context);
        init(context, null);
    }

    public AroundDownloadImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public AroundDownloadImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public void setOnJobCompletionListner(JobCompletionListner job) {
        this.jobComplete = job;
    }
    ChatSentListner uploadListner = null;
    public void setOnFileUploadListener(ChatSentListner job) {
        this.uploadListner = job;
    }

    FeedSentListner feedSentListnerListner = null;
    public void setOnFeedFileUploadListener(FeedSentListner job) {
        this.feedSentListnerListner = job;
    }


    public void setDownloadClickListner(String uri,int mediaType) {
        downloadView.setVisibility(VISIBLE);
        sizeText.setVisibility(VISIBLE);
        final String url = uri;
        /*downloadView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadFile(url);
            }
        });*/
        SingleViewDownloadHandler.setDownloadClickListner(getContext(),url,mediaType,0,downloadView,jobComplete);
    }

    public void setChatUploadClickListner(String uri, final GroupChatData data,final StorageReference ref) {
        enableDownloadClick();
        downloadView.setIsUpload();

        final String url = uri;
        /*downloadView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadFile(url,ref);
            }
        });*/

        SingleViewDownloadHandler.setUploadChatClickListner(getContext(),url,0,data,downloadView,ref,uploadListner);

    }
    public void setFeedUploadClickListner(String uri, final GroupFeedData data) {
        downloadView.setVisibility(VISIBLE);
        sizeText.setVisibility(VISIBLE);
        final String url = uri;
        /*downloadView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadFile(url,ref);
            }
        });*/

        SingleViewDownloadHandler.setUploadFeedClickListner(getContext(),url,1,data,downloadView,feedSentListnerListner);

    }

    public void enableDownloadClick()
    {
        downloadView.setVisibility(VISIBLE);
        downloadViewBG.setVisibility(VISIBLE);

        sizeText.setVisibility(VISIBLE);
    }
    public void disableDownloadClick() {
        Log.e("MSG", "disable");
        downloadView.setVisibility(GONE);
        downloadViewBG.setVisibility(GONE);
        downloadView.setOnClickListener(null);
        sizeText.setVisibility(GONE);
    }


    private void init(Context context, @Nullable AttributeSet attrs) {

        mContext = context;
        rootView = inflate(context, R.layout.around_imageview, this);
        imageContent = (TouchImageView) rootView.findViewById(R.id.imageContent);
        downloadView = (AroundDownloadView) rootView.findViewById(R.id.downloadView);
        downloadViewBG=rootView.findViewById(R.id.downloadViewBG);
        sizeText=rootView.findViewById(R.id.sizeText);


        if (attrs != null) {
            TypedArray ta = context.getTheme().obtainStyledAttributes(attrs, R.styleable.AroundDownloadImageView, 0, 0);
            /*centerCrop = ta.getBoolean(R.styleable.AroundDownloadImageView_imageCenterCrop, false);
            if (centerCrop)
                imageContent.setScaleType(ImageView.ScaleType.CENTER_CROP);

            imageMaxHeight = ta.getDimensionPixelSize(R.styleable.AroundDownloadImageView_imageMaxHeight, imageContent.getHeight());
            imageContent.setMaxHeight(imageMaxHeight);*/

            /*imageContent.setImageResource(ta.ge(R.styleable.AroundDownloadImageView_imageCenterCrop,
                    R.drawable.ic_more_options_black));*/


        }

    }
    int mediaType;
    public void setMediaType(int type)
    {

        this.mediaType=type;
    }

    public ImageView getImageView() {
        return imageContent;
    }
    public void setImageViewUri(Uri url) {

        /*Glide.with(mContext).load(url)
                .into(  this.imageContent);*/
        Log.e("MGG","final_url2 "+url);
       //this.imageContent.setImageURI(url);
        //this.imageContent.setImageDrawable(null);

        RequestOptions options = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
        .placeholder(getContext().getResources().getDrawable(R.drawable.ic_image_semiblack))
                .priority(Priority.HIGH);

        Glide.with(getContext()).load(url)
                .apply(options)
                .into( this.imageContent);
        imageContent.invalidate();



        /*Glide.with(getContext()) //passing context
                .load(url) //passing your url to load image.
              //  .placeholder(placeHolderUrl) //this would be your default image (like default profile or logo etc). it would be loaded at initial time and it will replace with your loaded image once glide successfully load image using url.
               // .error(errorImageUrl)//in case of any glide exception or not able to download then this image will be appear . if you won't mention this error() then nothing to worry placeHolder image would be remain as it is.
                .diskCacheStrategy(DiskCacheStrategy.ALL) //using to load into cache then second time it will load fast.
                .animate(R.anim.fade_in) // when image (url) will be loaded by glide then this face in animation help to replace url image in the place of placeHolder (default) image.
                .fitCenter()//this method help to fit image into center of your ImageView
                .into(imageView);*/

    }

    public void setLocallyAvailable(boolean locallyAvailable)
    {
        if(locallyAvailable)
        {
            disableDownloadClick();
        }
        else
        {
            enableDownloadClick();
        }
    }
    AroundDownloadView getDownloadView() {
        return downloadView;
    }

    public void setUpChatMedia(GroupChatData data,JobCompletionListner listener)
    {
        setMediaType(data.type);
        if(data.sender==0)
        {
            if(data.msgseen==0) {
                setSizeText(StringUtils.mediaSizeToString(data.mediaSize));
                setImageViewUri(Uri.parse(data.temp_url));
                setChatUploadClickListner(data.final_url,data
                        , FirebaseStorage.getInstance().getReference().child("chat_datas").child(data.userid));

                setOnFileUploadListener(new ChatSentListner() {
                    @Override
                    public void onChatAdded(GroupChatData data) {

                    }

                    @Override
                    public void onChatUploaded(GroupChatData data) {
                        setLocallyAvailable(true);
                    }
                });

            }
            else {

                setImageViewUri(Uri.parse(AroundStaticInfo.checkUriExist(getContext(),Uri.parse(data.final_url))?data.final_url:data.temp_url));
                setLocallyAvailable(true);
            }
        }
        else {
            if (data.final_url == null ||data.final_url.contains("null")||data.final_url.contains("http")) {
                setLocallyAvailable(false);
                setImageViewUri(Uri.parse(data.temp_url));

                setSizeText(StringUtils.mediaSizeToString(data.mediaSize));

                setOnJobCompletionListner(listener);
                setDownloadClickListner(data.final_url, data.type);

            } else {
                setLocallyAvailable(true);
                setImageViewUri(Uri.parse(AroundStaticInfo.checkUriExist(getContext(),Uri.parse(data.final_url))?data.final_url:data.temp_url));
            }
        }

    }
    public void setUpFeedMedia(final GroupFeedData data,JobCompletionListner listener)
    {
        setMediaType(data.type);
        if(data.userid.equals(FirebaseAuth.getInstance().getUid()))
        {
            if(data.type!=0&&data.ifseen==0) {

               setLocallyAvailable(false);

               setOnFeedFileUploadListener(new FeedSentListner() {
                   @Override
                   public void onFeedAdded(FeedData data) {

                   }

                   @Override
                   public void onFeedUploaded(FeedData data) {
                       setLocallyAvailable(true);
                   }
               });

                setFeedUploadClickListner(data.uri,data);

            }
            else
            {
                setLocallyAvailable(true);
            }
        }
        else {

            if(data.uri.contains("http"))
            {
                setImageViewUri(Uri.parse(data.semi_uri));
               setSizeText(StringUtils.mediaSizeToString(data.mediaSize));

                setOnJobCompletionListner(listener);
               setDownloadClickListner(data.uri,TabbedActivity.PREF_IMAGE);
                setLocallyAvailable(false);
            }
            else
            {

                setImageViewUri(Uri.parse(data.uri));
                setLocallyAvailable(true);
            }
        }

    }

    /*new JobCompletionListner() {
                    @Override
                    public void onJobCompletion(String text1) {



                    }
                });*/
}
