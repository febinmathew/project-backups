package com.hash.around.Views;


import android.app.LoaderManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.hash.around.MediaViewer;
import com.hash.around.OtherClasses.ChatDataAdapter;
import com.hash.around.OtherClasses.DataTypes.GroupChatData;
import com.hash.around.OtherClasses.DataTypes.MediaData;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.OtherClasses.Interfaces.ChatSentListner;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.OtherClasses.Interfaces.JobCompletionWithFailureListner;
import com.hash.around.R;
import com.hash.around.TabFragments.FeedFullScreen;
import com.hash.around.TabFragments.MapsFragment;
import com.hash.around.TabbedActivity;
import com.hash.around.Utils.DatabaseManager;
import com.hash.around.Utils.StringUtils;

import java.util.ArrayList;


public class AroundUserMediaRailView extends RelativeLayout implements LoaderManager.LoaderCallbacks<Cursor> {

  private final RecyclerView recyclerView;
  final TextView headingTag;
  final TextView amountTag;

  private OnItemClickedListener listener;

  public AroundUserMediaRailView(Context context) {
    this(context, null);
  }

  public AroundUserMediaRailView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public AroundUserMediaRailView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);

    inflate(context, R.layout.recent_photo_view, this);
    this.recyclerView = findViewById(R.id.photo_list);
    this.headingTag = findViewById(R.id.headingTag);
    this.amountTag = findViewById(R.id.amountTag);


    this.recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
    this.recyclerView.setItemAnimator(new DefaultItemAnimator());
  }

  public void setListener(@Nullable OnItemClickedListener listener) {
    this.listener = listener;

    if (this.recyclerView.getAdapter() != null) {
      ((RecentPhotoAdapter) this.recyclerView.getAdapter()).setListener(listener);
    }
  }
  private MapsFragment.OnTouchListener touchListener=null;
  public void setTouchListener(MapsFragment.OnTouchListener listener) {
    touchListener = listener;
  }

  @Override
  public boolean dispatchTouchEvent(MotionEvent ev) {
    if(touchListener!=null)
    touchListener.onTouch();
    return super.dispatchTouchEvent(ev);
  }

  JobCompletionWithFailureListner sendButtonListner;

  public void setSendButtonListener(@Nullable JobCompletionWithFailureListner listener) {
    this.sendButtonListner = listener;

    Log.e("MGG", "adapter status " + this.recyclerView.getAdapter());
    if (this.recyclerView.getAdapter() != null) {
      ((RecentPhotoAdapter) this.recyclerView.getAdapter()).setSendButtonListener(listener);
    }
  }


  @Override
  public android.content.Loader<Cursor> onCreateLoader(int id, Bundle args) {
    Log.e("GGG","shared starting db ");
    String userid=null;
    if(args!=null){
     userid=args.getString("userid");
    String headingText;
    //if(userid.equals(FirebaseAuth.getInstance().getUid()))
      headingText="Shared medias";


    headingTag.setVisibility(VISIBLE);
    headingTag.setText(headingText);
    }
    else
    { headingTag.setVisibility(GONE);
    }

    return new SelfSharedMediaLoader(getContext(),userid);
  }

  @Override
  public void onLoadFinished(android.content.Loader<Cursor> loader, Cursor data) {
    Log.e("GGG","shared feed countis "+data.getCount());
    if(data.getCount()==0)
      this.setVisibility(GONE);
    else
    {
      this.setVisibility(VISIBLE);
      amountTag.setVisibility(VISIBLE);
      amountTag.setText(data.getCount()+"");
    }
    this.recyclerView.setAdapter(new RecentPhotoAdapter(getContext(), data, listener, sendButtonListner));
  }

  @Override
  public void onLoaderReset(android.content.Loader<Cursor> loader) {
    ((CursorRecyclerViewAdapter) this.recyclerView.getAdapter()).changeCursor(null);
  }




  private static class RecentPhotoAdapter extends CursorRecyclerViewAdapter<RecentPhotoAdapter.FeedRailItemViewHolder> {

    @SuppressWarnings("unused")
    private static final String TAG = RecentPhotoAdapter.class.getName();


    @Nullable
    private OnItemClickedListener clickedListener;

    private RecentPhotoAdapter(@NonNull Context context, @NonNull Cursor cursor, @Nullable OnItemClickedListener listener, @Nullable JobCompletionWithFailureListner listener2) {
      super(context, cursor);

      this.clickedListener = listener;
      this.sendButtonListner = listener2;
    }

    @Override
    public FeedRailItemViewHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
      View itemView = LayoutInflater.from(parent.getContext())
              .inflate(R.layout.feed_rail_item, parent, false);

      return new FeedRailItemViewHolder(itemView);
    }

    ArrayList<MediaData> selectedItems = new ArrayList<>();
    String gifMime = MimeTypeMap.getSingleton().getMimeTypeFromExtension("gif");

    @Override
    public void onBindItemViewHolder(final FeedRailItemViewHolder viewHolder, @NonNull Cursor cursor) {

GroupChatData chatData=new GroupChatData();

     //final long feedRow = cursor.getLong(cursor.getColumnIndexOrThrow("rowID"));
      int feedType = cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_type));
      chatData.type=feedType;
      int chat_seen = cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_ifseen));
      chatData.msgseen=chat_seen;
      int sender = cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_sender));
      chatData.sender=sender;
    final  String userID = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_userid));
      chatData.userid=userID;
      final  Long mediaID = cursor.getLong(cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_col_media_id));
      chatData.media_id=mediaID;
      String uri = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseAccesser.media_lite_local_uri));
      chatData.temp_url=uri;
      String final_uritemp = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseAccesser.media_local_uri));

      if(final_uritemp.equals("null"))
      {
        final_uritemp = cursor.getString(
                cursor.getColumnIndexOrThrow(DatabaseAccesser.media_global_uri));
      }
      chatData.final_url=final_uritemp;
      final String final_uri=final_uritemp;

     // String feedMessage = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_text));
      viewHolder.feedItem. setforRounderCorner(10);

     /* if(feedType==0)
      {
        viewHolder.feedItem.setText(feedMessage);
      }
      else */if(feedType==1||feedType==3||feedType==5)
      {
        if(uri==null||uri.equals("null"))
        {
          Log.e("GGG","nullnullnull");
        }
        else
        {
        viewHolder.feedItem.setImgResource(Uri.parse(uri));
          Log.e("GGG","notnulllll");
      }
      }
      else
        {
          Log.e("GGG","shittttttttt "+feedType);
        }

      viewHolder.feedItem.setOnClickListner(new OnClickListener() {
        @Override
        public void onClick(View v) {

         // openWithSelectedFeed(userID,user_name,feedRow);
          openWithSelectedMedia(userID,mediaID);
        }
      });


  if(sender==1) {
    if (final_uri.contains("https")) {

      viewHolder.feedItem.setImgResource(Uri.parse(uri));

      viewHolder.feedItem.setOnJobCompletionListner(new JobCompletionListner() {
        @Override
        public void onJobCompletion(String text1) {
          DatabaseManager.updateMediaTableFromGlobalUrl(final_uri, text1);
        }
      });
      viewHolder.feedItem.setDownloadClickListner(final_uri, TabbedActivity.PREF_IMAGE);
    } else {

      //((ChatImageView) holder).chatImage.layout(0, 0, 0, 0);
       /* Glide.with(mContext).load(Uri.parse(data.get(position).final_url))
                .into(((ChatImageView) holder).chatImage);*/
      viewHolder.feedItem.setLocallyAvailable(true);
    }
  }
  else
  {
    if(chat_seen==0) {

      viewHolder.feedItem.setLocallyAvailable(false);

      //CODE 47 need to create
      viewHolder.feedItem.setUploadClickListner(final_uri,chatData
              ,chatData.media_id,null);

      viewHolder.feedItem.setOnChatUploadListener(new ChatSentListner() {
        @Override
        public void onChatAdded(GroupChatData data) {

        }

        @Override
        public void onChatUploaded(GroupChatData data) {

          viewHolder.feedItem.setLocallyAvailable(true);
        }
      });

    }
    else
    {
      viewHolder.feedItem.setLocallyAvailable(true);
    }
  }

      /*final MediaData mediaData = new MediaData();

      mediaData.uri = Uri.withAppendedPath(baseUri, Long.toString(id)).toString();
      mediaData.type = mediaType;
      if (mimeType.equals(gifMime))
        mediaData.type = 5;

      //Log.e("MGG","uri NOW " + uri);
      //Key signature = new MediaStoreSignature(mimeType, dateModified, orientation);

      if (selectedItems.contains(mediaData))
        viewHolder.wholeView.setChecked(true);
      else
        viewHolder.wholeView.setChecked(false);


      viewHolder.wholeView.setJobListner(new JobCompletionWithFailureListner() {
        @Override
        public void onJobFailed(String txt1) {
          viewHolder.wholeView.setChecked(false);
          selectedItems.remove(mediaData);
          updateSendButtonStatus();

          if (clickedListener != null) clickedListener.onItemSelected(selectedItems);

        }

        @Override
        public void onJobCompletion(String txt1) {
          viewHolder.wholeView.setChecked(true);
          selectedItems.add(mediaData);
          updateSendButtonStatus();
          if (clickedListener != null) clickedListener.onItemSelected(selectedItems);

        }
      });
      Log.e("MGG", "gohh NOW " + mediaData.type);
      Glide.with(getContext()).load(Uri.parse(mediaData.uri))
              .thumbnail(0.4f)
              .crossFade()
              .diskCacheStrategy(DiskCacheStrategy.NONE)
              .into(viewHolder.imageView);

      if (mediaData.type == MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO) {
        viewHolder.typeIndicator.setVisibility(View.VISIBLE);
        viewHolder.typeIndicator.setImageResource(R.drawable.ic_video_white);
      } else if (mediaData.type == 5) {
        viewHolder.typeIndicator.setVisibility(View.VISIBLE);
        viewHolder.typeIndicator.setImageResource(R.drawable.ic_gif_white);
      } else
        viewHolder.typeIndicator.setVisibility(View.GONE);

      //viewHolder.imageView.setImageURI(uri);
      //viewHolder.imageView.setMaxWidth(viewHolder.imageView.getHeight());
      viewHolder.imageView.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
          Log.e("MGG", "clicking NOW ");
          viewHolder.wholeView.setChecked(true);
          if (!selectedItems.contains(mediaData))
            selectedItems.add(mediaData);
          if (clickedListener != null) {
            clickedListener.onItemSelected(selectedItems);
            clickedListener.onItemClicked(mediaData);

          }
        }
      });*/

    }

    void openWithSelectedMedia(String userID, Long targetMediaID)
    {


      Intent intent = new Intent(getContext().getApplicationContext(), MediaViewer.class);
      intent.putExtra("userID",userID);
      intent.putExtra("mediaID",targetMediaID);

      intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

      getContext().startActivity(intent);
    }
    void updateSendButtonStatus() {
      if (selectedItems.size() == 1) {
        if (sendButtonListner != null)
          sendButtonListner.onJobCompletion("");
      } else if (selectedItems.size() == 0) {
        if (sendButtonListner != null)
          sendButtonListner.onJobFailed("");
      }
    }

    public void setListener(@Nullable OnItemClickedListener listener) {
      this.clickedListener = listener;
    }

    JobCompletionWithFailureListner sendButtonListner;

    public void setSendButtonListener(JobCompletionWithFailureListner listener) {
      sendButtonListner = listener;
    }

    static class FeedRailItemViewHolder extends RecyclerView.ViewHolder {

    FeedRailItemView feedItem;

      FeedRailItemViewHolder(View itemView) {
        super(itemView);
        feedItem = itemView.findViewById(R.id.wholeView);
      }
    }
  }

  public interface OnItemClickedListener {
    void onItemClicked(MediaData uri);

    void onItemSelected(ArrayList<MediaData> uriList);
  }

  public static class SelfSharedMediaLoader extends android.content.CursorLoader {

   // private static final String SELECTION = DatabaseAccesser.feed_col_user_id + " =?";

    private final Context context;
    String userid;
    /*public SelfFeedLoader() {
      super(context);
      this.context = context;//.getApplicationContext();
    }*/

    public SelfSharedMediaLoader(Context context,String userid) {
      super(context);
      this.context = context;//.getApplicationContext();
      this.userid=userid;
      Log.e("GGG","shared inited db ");
    }

    @Override
    public Cursor loadInBackground() {

      Log.e("GGG","shared executing db ");
     SQLiteDatabase DB = DatabaseManager.getDatabase(getContext()).getReadableDatabase();


      return DB.rawQuery(
              "SELECT * FROM "+DatabaseAccesser.chat_table +" INNER JOIN "+DatabaseAccesser.media_table+" ON "+
                      DatabaseAccesser.chat_table+"."+DatabaseAccesser.chat_col_media_id+" = "+ DatabaseAccesser.media_table+".rowid"+ " WHERE " +
                      DatabaseAccesser.chat_userid + " =? AND "+DatabaseAccesser.chat_type+" IN(?,?,?,?)", new String[] { userid,"1","2","3","5" });

    //  return DatabaseManager.getDatabase(context).getReadableDatabase(). rawQuery("select "+DatabaseAccesser.feed_table+".rowid AS rowID"+",* FROM "+DatabaseAccesser.feed_table+" INNER JOIN "+DatabaseAccesser.media_table+" ON "+
             // DatabaseAccesser.feed_table+"."+DatabaseAccesser.feed_col_media_id+" = "+ DatabaseAccesser.media_table+".rowid"+ " where "+DatabaseAccesser.feed_col_user_id  +" =?"+" order by "+DatabaseAccesser.feed_col_timestamp+ " DESC",new String[]{this.userid});

    }
  }
}
