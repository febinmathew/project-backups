package com.hash.around.Utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.RemoteInput;
import android.util.Log;

import com.hash.around.ChatWindow;
import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.R;
import com.hash.around.TabbedActivity;

import java.io.IOException;
import java.util.ArrayList;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by Febin on 6/13/2018.
 */

public class LocalNotificationManager {

    private static NotificationManagerCompat chatNotificationManager;
    private static int chatNotitificationID=1;
    private static String chatChannelID="Chat_Channel";
    public static void cancelChatNotitication()
    {
        if(chatNotificationManager!=null)
            chatNotificationManager.cancel(chatNotitificationID);
    }

    public static void sentNewChatNotitication(Context context,String userID,String message)
    {

        UserWholeData userData=ChatWindow.returnSingleUserDetails(context,userID);
        Cursor cursor;/*=DatabaseManager.getDatabase(context).getReadableDatabase().query(DatabaseAccesser.chat_table,
                null,DatabaseAccesser.chat_ifseen+"=? AND "+
                        DatabaseAccesser.chat_sender+"=?",new String[]{"0","1"},null,null,
                DatabaseAccesser.chat_timestamp+"");*/

        cursor= DatabaseManager.getDatabase(context).getReadableDatabase().rawQuery(
                "SELECT * FROM "+DatabaseAccesser.chat_table +" INNER JOIN "+DatabaseAccesser.user_table+" ON "+
                        DatabaseAccesser.chat_table+"."+DatabaseAccesser.chat_userid+" = "+ DatabaseAccesser.user_table+"."+DatabaseAccesser.user_user_id +
                        " INNER JOIN "+DatabaseAccesser.media_table+" ON "+
                        DatabaseAccesser.chat_table+"."+DatabaseAccesser.chat_col_media_id+" = "+ DatabaseAccesser.media_table+".rowid"+
                        " WHERE " +
                        DatabaseAccesser.chat_ifseen+"=? AND "+
                        DatabaseAccesser.chat_sender+"=?" + " ORDER BY "+DatabaseAccesser.chat_timestamp,new String[]{"2","1"});

        //android.app.LocalNotificationManager mNotificationManager =
                //(android.app.LocalNotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Intent intenttest = new Intent(context, ChatWindow.class);
        intenttest.putExtra("userid",userID);
        intenttest.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent singleChatIntent = PendingIntent.getActivity(context, (int) System.currentTimeMillis(), intenttest, 0);

        Intent intenttest2 = new Intent(context, TabbedActivity.class);
        intenttest2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent allChatIntent = PendingIntent.getActivity(context, (int) System.currentTimeMillis(), intenttest2, 0);


        Intent snoozeIntent = new Intent(context, ChatWindow.class);
        snoozeIntent.setAction(Intent.ACTION_ANSWER);
        snoozeIntent.putExtra("dsff", 0);
        PendingIntent snoozePendingIntent =
                PendingIntent.getBroadcast(context, 0, snoozeIntent, 0);

        final String KEY_TEXT_REPLY = "key_text_reply";

        String replyLabel = "Repllly";
        RemoteInput remoteInput = new RemoteInput.Builder(KEY_TEXT_REPLY)
                .setLabel(replyLabel)
                .build();

        PendingIntent replyPendingIntent =
                PendingIntent.getBroadcast(context,
                        1,
                        snoozeIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);


        /*Notification.InboxStyle inboxStyle = new Notification.InboxStyle();
        inboxStyle.setBigContentTitle(userData.user_name);
        inboxStyle.setSummaryText("New chat");
        inboxStyle.addLine(message);




        Notification n  =  new Notification.Builder(context)
                .setSmallIcon(R.drawable.nick)
                .setContentTitle("SpotIt")
                .setContentText("Net chats")
                .setContentInfo("Photo")
                .setSubText("12 Chats from 2 Contacts")
                .setContentIntent(pIntent)
                .setStyle(inboxStyle)
                .setPriority(Notification.PRIORITY_MAX)
                .setDefaults(Notification.DEFAULT_ALL)
                .addAction(R.drawable.ic_forward_white, "Forward",
                        snoozePendingIntent)
                .addAction(R.drawable.ic_eye_show_white, "Mark Read",
                        snoozePendingIntent)
                .addAction(R.drawable.ic_follow_white, "Follow",
                        snoozePendingIntent)
                .setAutoCancel(true).build();
        //mNotificationManager.notify(0,n);*/


        NotificationCompat.InboxStyle inboxStyle2 =
                new NotificationCompat.InboxStyle();


        String lastmessage_by="";
        String lastmessage_type="";
        int count=0,secondaryCount=0;
        ArrayList<String> usersList=new ArrayList<>();
        int totalChatCount=0;

        if(cursor.getCount()<=0)
            return;
      while(cursor.moveToNext())
      {
          Integer chat_type=cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_type));
          String chat_message=cursor.getString(cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_text));
          String chat_userID=cursor.getString(cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_userid));
          String chat_userName=cursor.getString(cursor.getColumnIndexOrThrow(DatabaseAccesser.user_name));
          int user_type=cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseAccesser.user_user_type));

          if(!usersList.contains(chat_userID))
          {   usersList.add(chat_userID);
              totalChatCount++;
          }
          String currentLine="";


          if(count>(cursor.getCount()-7))
          {

              if(totalChatCount>1)
              {
                  if(user_type==5||user_type==6)
                      currentLine="\uD83D\uDC65 ";
                  currentLine+=chat_userName+": ";
              }
              String type= StringUtils.convertMediaTypeToString(chat_type);
              if (secondaryCount == 0) {
                  lastmessage_by = "Last message from " + userData.user_name;
                  lastmessage_type = type;
              }
              secondaryCount++;

              inboxStyle2.addLine(currentLine+(!type.equals("")?"("+type+") ":"")+(chat_message.equals("null")?"":StringUtils.makeHashTagText(context,chat_message,true)));
          }

          count++;
      }

        Bitmap bitmap=null;
      try {
          bitmap = getLargeIconFromUri(context, userData.img_url);
      }catch (Exception d)
      {

      }
      if(bitmap==null)
      {
          bitmap=BitmapFactory.decodeResource(context.getResources(),R.drawable.default_user);
      }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Name od channel";
            String description = "Channel description";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(chatChannelID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }


        PowerManager pm = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isScreenOn();
        if(isScreenOn==false)
        {
            PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK |PowerManager.ACQUIRE_CAUSES_WAKEUP |PowerManager.ON_AFTER_RELEASE,"MyLock");
            wl.acquire(10000);
            PowerManager.WakeLock wl_cpu = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,"MyCpuLock");

            wl_cpu.acquire(10000);
        }

        Log.e("GGG","lights "+inboxStyle2+" /cursor: "+cursor.getCount());
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context,chatChannelID)
                .setStyle(inboxStyle2)

                .setTicker(userData.user_name+": "+StringUtils.makeHashTagText(context,message,true))
                .setDefaults(Notification.DEFAULT_LIGHTS)
                .setSmallIcon(R.mipmap.logo1)
                .setGroupSummary(true)
                .setContentInfo(lastmessage_type)
                .setVisibility(NotificationCompat.VISIBILITY_PRIVATE)
                .setColor(Color.RED)
                /*.setLights(255, 0, 0)*/
                .setPriority(Notification.PRIORITY_MAX)
                .setLights(0xff0000, 2000, 800)
                /*.addAction(new NotificationCompat.Action.Builder(R.drawable.ic_forward_white,
                        "Replay", replyPendingIntent)
                        .addRemoteInput(remoteInput)
                        .build())*/
                .addAction(R.drawable.ic_chat_bubble_white, "Replay "+userData.user_name,
                        singleChatIntent)

                .setAutoCancel(true);

        //Notification notification=;


        if(bitmap!=null) {
            mBuilder.setLargeIcon(bitmap);
        }
        if(totalChatCount>1)
        {

            mBuilder.setContentText(String.format("%s Messages from %s Chats",count,totalChatCount));
            inboxStyle2.setSummaryText(String.format("%s Messages from %s Chats",count,totalChatCount));
            mBuilder.setContentTitle("New Message");
            inboxStyle2.setBigContentTitle("New Message");
            mBuilder.setSubText(lastmessage_by);
            mBuilder.setContentIntent(allChatIntent);
        }
        else
        {
            mBuilder.setContentText(String.format("%s Messages",count));
            inboxStyle2.setSummaryText(String.format("%s Messages",count));
            mBuilder.setContentTitle(userData.user_name);
            inboxStyle2.setBigContentTitle(userData.user_name);
            mBuilder.setContentIntent(singleChatIntent);

        }

        if(userData.muteTime>System.currentTimeMillis() || PrefManager.getBooleanPreference(context,PrefManager.PREF_CHAT_MUTE,false)) {

        }
        else{
            Log.e("GGG","preference sound "+userData.not_uri);
            if (userData.not_uri.equals("")|| Uri.parse(userData.not_uri).equals(Settings.System.DEFAULT_NOTIFICATION_URI)) {
                if(!PrefManager.getStringPreference(context,PrefManager.PREF_CHAT_NOTIFICATION_SOUND,"").equals("")){
                    Log.e("GGG","preference sound1 "+PrefManager.getStringPreference(context,PrefManager.PREF_CHAT_NOTIFICATION_SOUND,"dsdsds"));
                    mBuilder.setSound(Uri.parse(PrefManager.getStringPreference(context,PrefManager.PREF_CHAT_NOTIFICATION_SOUND,"")));}
                else {
                    Log.e("GGG","preference sound2 "+PrefManager.getStringPreference(context,PrefManager.PREF_CHAT_NOTIFICATION_SOUND,"dsdsds"));
                    mBuilder.setDefaults(Notification.DEFAULT_SOUND);
                }
            }
            else {
                mBuilder.setSound(Uri.parse(userData.not_uri));
            }

            Log.e("GGG","preference vib "+userData.not_vibration);
            if (userData.not_vibration==0) {
                if(!PrefManager.getStringPreference(context,PrefManager.PREF_CHAT_VIBRATION,"").equals(""))
                    //mBuilder.setDefaults(Integer.);{}
                {
                    int p=Integer.parseInt(PrefManager.getStringPreference(context,PrefManager.PREF_CHAT_VIBRATION,""));
                    Log.e("GGG","preference vib2 "+p);
                    if(p==0)
                        mBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
                    else if(p==1)
                        mBuilder.setVibrate(new long[] { 0, 200,200,200});
                }
                else{
                    Log.e("GGG","preference vib3 "+PrefManager.getStringPreference(context,PrefManager.PREF_CHAT_VIBRATION,"dfgfd"));
                    mBuilder.setDefaults(Notification.DEFAULT_VIBRATE);}
            }
            else if (userData.not_vibration==1) {
                mBuilder.setVibrate(new long[] { 0, 200,200,200});
            }
        }

        mBuilder.setPublicVersion(mBuilder.build());
        if(chatNotificationManager==null)
            chatNotificationManager=NotificationManagerCompat.from(context);

        Notification notification = mBuilder.build();
        notification.ledARGB = 0xffff0000; // Blue color light flash
        notification.ledOnMS = 2000; // LED is on for 2 second
        notification.ledOffMS = 800; // LED is off for 0.5 second
        notification.flags = Notification.FLAG_SHOW_LIGHTS;
       chatNotificationManager.notify(chatNotitificationID,notification);
        cursor.close();

        //NotificationManager notificationManager = (NotificationManager)context.getSystemService(NOTIFICATION_SERVICE);

       // notificationManager.notify(0, notification);
    }


    public static void customTestNOT(Context context)
    {
        NotificationManager notificationManager = (NotificationManager)context.getSystemService(NOTIFICATION_SERVICE);
        Notification notification = new Notification();
        notification.ledARGB = 0xff0000ff; // Blue color light flash
        notification.ledOnMS = 2000; // LED is on for 2 second
        notification.ledOffMS = 500; // LED is off for 0.5 second
        notification.flags = Notification.FLAG_SHOW_LIGHTS;
        notificationManager.notify(0, notification);
    }
    static Bitmap getLargeIconFromUri(Context context,String uri)
    {

        Bitmap bitmap= null;//BitmapFactory.decodeFile(new File(uri).getPath());
        try {
            bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), Uri.parse(uri));
        } catch (IOException e) {
            //e.printStackTrace();
            bitmap=null;
        }


        if(bitmap==null)
            bitmap= BitmapFactory.decodeResource(context.getResources(),R.drawable.default_user);

        bitmap=MediaManager.getSquareBitmap(bitmap);
        int large_icon_width=context.getResources().getDimensionPixelSize(android.R.dimen.notification_large_icon_width);
        int large_icon_height=context.getResources().getDimensionPixelSize(android.R.dimen.notification_large_icon_height);
        bitmap=Bitmap.createScaledBitmap(bitmap, large_icon_width, large_icon_height, true);
        return bitmap;
    }

}


