package com.hash.around.Views.CustomMakers;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.R;
import com.hash.around.Utils.DatabaseManager;
import com.hash.around.Views.ViewFunctions;


public class NotificationTag extends RelativeLayout {

    public NotificationTag(Context context) {
        this(context, null);
    }

    public NotificationTag(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public static NotificationTag instance;
    RelativeLayout wholeView;
    public LinearLayout notificationSection,requestSection;
    TextView notificationAmount,requestAmount;
    int notificationRealAmount=0,requestRealAmount=0;

    public NotificationTag(final Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        inflate(context, R.layout.notification_tag_view, this);
        wholeView=findViewById(R.id.wholeView);
        notificationSection=findViewById(R.id.notificationSection);
        notificationSection.setVisibility(GONE);
        requestSection=findViewById(R.id.requestSection);
        requestSection.setVisibility(GONE);
        notificationAmount=findViewById(R.id.notificationAmount);
        requestAmount=findViewById(R.id.requestAmount);

        //initializeWithVals();

        if (attrs != null) {
           // TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CircularRelativeLayout, 0, 0);
            //this.radius   = typedArray.getDimension(R.styleable.CircularRelativeLayout_radius, 20);
           // dimen=typedArray.getDimensionPixelSize(R.styleable.CircularRelativeLayout_radius, 4);

            //typedArray.recycle();
        }
        //init(context);
        instance=this;
    }

    public void setNotificationAmount(int amount)
    {
        if(amount<=0)
        {
            if(notificationSection.getVisibility()!=GONE)
                ViewFunctions.fadeOut(notificationSection);
            return;
        }
        notificationAmount.setText(amount+"");
        ViewFunctions.animateButtonBurstIn(notificationSection,50,true);
        notificationRealAmount=amount;
    }
    public void incrementNotificationAmount(int amount/*,boolean increment*/)
    {
        amount+=notificationRealAmount;
        notificationRealAmount=amount;
        //if(notificationRealAmount>0)

        setNotificationAmount(notificationRealAmount);
        //notificationAmount.setText(amount+"");
        //ViewFunctions.animateButtonBurstIn(notificationSection,50,true);
    }
    public void incrementRequestAmount(int amount)
    {
        amount+=requestRealAmount;
        requestRealAmount=amount;
        setRequestAmount(requestRealAmount);
        //requestAmount.setText(amount+"");
        //ViewFunctions.animateButtonBurstIn(requestSection,50,true);
    }

    public void incrementAINotification(int amount,int type)
    {
        if(type<7)
            incrementNotificationAmount(amount);
        else
            incrementRequestAmount(amount);
    }
    public void setRequestAmount(int amount)
    {
        if(amount<=0)
        {
            if(requestSection.getVisibility()!=GONE)
                ViewFunctions.fadeOut(requestSection);
            return;
        }
        requestAmount.setText(amount+"");
        if(requestSection.getVisibility()!=VISIBLE)
            ViewFunctions.animateButtonBurstIn(requestSection,50,true);
        requestRealAmount=amount;
    }

    public void  hideNotification()
    {
        ViewFunctions.fadeOut(wholeView);
    }
    public void  showNotification()
    {
        ViewFunctions.fadeIn(wholeView);
    }

    public void initializeWithVals()
    {
        SQLiteDatabase db = DatabaseManager.getDatabase(getContext()).getReadableDatabase();

        Cursor cursor;

        cursor = db.query(DatabaseAccesser.notification_table, null,
                    DatabaseAccesser.notification_seen_status+" =?",
                    new String[]{"2"},
                    null, null, null,null);
        int notCount=0,requestCount=0;
        while (cursor.moveToNext()) {
            int type = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.notification_type));
            if(type<7)
                notCount++;
            else
                requestCount++;
        }
        setRequestAmount(requestCount);
        setNotificationAmount(notCount);
        cursor.close();


    }

}
