package com.hash.around.Utils;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.util.Base64;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;

import static com.hash.around.TabFragments.TabFragment2.getResizedBitmap;

/**
 * Created by Febin on 6/30/2018.
 */

public class ByteUtils {

    public static byte[] serialize(Object obj) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ObjectOutputStream os = null;
        try {
            os = new ObjectOutputStream(out);
            os.writeObject(obj);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return out.toByteArray();
    }

    public static Object deserialize(byte[] data)  {

        ByteArrayInputStream in = new ByteArrayInputStream(data);
        ObjectInputStream is = null;
        try {
            is = new ObjectInputStream(in);
            return is.readObject();
        } catch (Exception e) {
            Log.e("GGG","ezception2 "+e.getMessage());
        }

        return null;
    }
    public static Object deserializeFile(Uri uri)  {

        File file = new File(uri.getPath());
        //int size = (int) file.length();
       // byte[] bytes = new byte[size];
        byte[] bytesArray = new byte[(int) file.length()];
        Log.e("GGG","desefile starting "+(bytesArray.length)+" / "+uri);
        FileInputStream ss=null;
        try {

            FileInputStream fis = new FileInputStream(file);
            fis.read(bytesArray); //read file into bytes[]
            fis.close();
            Log.e("GGG","desefile written "+bytesArray[0]);
            /*ss=new FileInputStream(uri);
            BufferedInputStream buf = new BufferedInputStream(ss);
            buf.read(bytes, 0, bytes.length);
            buf.close();*/
            return deserialize(bytesArray);
        } catch (Exception e) {
            Log.e("GGG","desefile ezception1 "+e.getMessage());
            return null;
        }

    }
    public static Bitmap getBitmapFromuri(String uri)
    {
        File file = new File(Uri.parse(uri).getPath());
        if(file.exists())
        {
            Log.e("GGG","local file exists");
        }
        else
        {
            Log.e("GGG","local file nNOT exists");
        }
        FileInputStream fis;
        Bitmap photo=null;
        try{
        fis = new FileInputStream(file);
        } catch (Exception e) {
            Log.e("GGG","local ezception1 "+e.getMessage()+" / "+file.getAbsolutePath());

            return null;
        }
        photo = BitmapFactory.decodeStream(fis);


        return photo;
    }

    public static  byte[] bitmapToByteArray(Bitmap bitmap,int maxQuality)
    {
        Log.e("GGG","local ezception2 "+bitmap);
        if(bitmap==null)
            return null;
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        final byte[] byteArray;
        getResizedBitmap(bitmap, maxQuality).compress(Bitmap.CompressFormat.PNG, 1, bytes);
        byteArray=bytes.toByteArray();
        Log.e("GGG","local ezception3 "+byteArray);
        return byteArray;
    }
    public static String byteToString(byte[] byteArray)
    {
        Log.e("GGG","local ezception4 "+byteArray);
        if(byteArray==null)
            return null;
        return Base64.encodeToString(byteArray,Base64.DEFAULT);
    }
    public static byte[] stringToByte(String byteArray)
    {   if(byteArray==null)
        return null;
        return Base64.decode(byteArray,Base64.DEFAULT);
    }

    public static  Bitmap byteArrayToPhoto(byte[] bitmapdata)
    {
        try {
            return BitmapFactory.decodeByteArray(bitmapdata, 0, bitmapdata.length);
        }
        catch (Exception e)
        {return null;}
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static boolean isLowMemory(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);

        return (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && activityManager.isLowRamDevice()) ||
                activityManager.getLargeMemoryClass() <= 64;
    }
}
