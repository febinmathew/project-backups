package com.hash.around;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beloo.widget.chipslayoutmanager.ChipsLayoutManager;
import com.beloo.widget.chipslayoutmanager.gravity.IChildGravityResolver;
import com.beloo.widget.chipslayoutmanager.layouter.breaker.IRowBreaker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.hash.around.OtherClasses.DataTypes.ContactListData;
import com.hash.around.OtherClasses.DataTypes.DownloadUploadData;
import com.hash.around.OtherClasses.DataTypes.UserPrivacyData;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.OtherClasses.DataTypes.GroupMemberData;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.hash.around.Utils.AppOnlineStatusManager;
import com.hash.around.Utils.ChatSender;
import com.hash.around.Utils.DatabaseManager;
import com.hash.around.Utils.MediaManager;

import com.hash.around.Utils.PermissionManager;
import com.hash.around.Views.CircleColorImageView;
import com.hash.around.Views.EmojiIconImage;
import com.hash.around.Views.ViewFunctions;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.vanniktech.emoji.EmojiEditText;
import com.vanniktech.emoji.EmojiManager;
import com.vanniktech.emoji.ios.IosEmojiProvider;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;


import static android.provider.MediaStore.EXTRA_OUTPUT;
import static com.hash.around.Profile_Activity.sentCustomNotificationToUser;
import static com.hash.around.TabbedActivity.saveContactToDatabase;
import static com.hash.around.TabbedActivity.userdetail;


public class CreateNewGroup extends AppCompatActivity {
    Toolbar toolbar;
    //View root;
    EmojiEditText name;
    EmojiIconImage emoticon;
    CircularImageView circleProfileImage;
    /*com.github.clans.fab.FloatingActionButton*/ImageButton chooseButton;
    FloatingActionButton completeButton;
    boolean locationError;
    Uri directory = null;

    private StorageReference mStorageRef;
    DatabaseReference databaseRef;


    //boolean readyForCreation=false;

    //public SharedPreferences prefs;
   // public SharedPreferences.Editor editor;
    //double latitute;
   // double longitude;
   // String locationName;
    StorageReference riversRef;



  /*  void saveGroupToDatabase(ContactListData post,String globalUrl)
    {
        SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();


        ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.user_user_id, post.user_id);
        values.put(DatabaseAccesser.user_name, post.user_name);
        values.put(DatabaseAccesser.user_status, post.status);
        if(post.user_id.equals(userdetail.getUid()))
        {
            values.put(DatabaseAccesser.user_user_type, 0);
        }
        else
        {
            values.put(DatabaseAccesser.user_user_type, post.numberType);
        }

        values.put(DatabaseAccesser.user_globalUrl,globalUrl);
        values.put(DatabaseAccesser.user_localUrl, post.img_url);



        long newRowId = db.insert(DatabaseAccesser.user_table, null, values);
    }*/

  int type;
    TextInputLayout fDescriptionLayout,fNameLayout;
    RecyclerView groupMembersList;
    TextView membersExtraText;
    Adapter_NewGroupMembers adap;
    ArrayList<UserWholeData> finaldata=new ArrayList<>();
    ArrayList<String> groupMemberList=new ArrayList<>();
    com.github.clans.fab.FloatingActionButton createGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        EmojiManager.install(new IosEmojiProvider());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_newgroup_activity);
        locationError = true;
        toolbar = (Toolbar) findViewById(R.id.commontoolbar);
       // prefs = getSharedPreferences("user_preferences", MODE_PRIVATE);
       // editor = prefs.edit();

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.type=getIntent().getIntExtra("type",0);


        chooseButton =  findViewById(R.id.imageButton1);

        circleProfileImage =  findViewById(R.id.circular_image);
        emoticon =  findViewById(R.id.emotimg);
        name = (EmojiEditText) findViewById(R.id.profilenametext);
        emoticon.setTargetText(name,findViewById(android.R.id.content));
        fDescriptionLayout=findViewById(R.id.fDescriptionLayout);
        fNameLayout=findViewById(R.id.fNameLayout);
        groupMembersList=findViewById(R.id.groupMembersList);
        membersExtraText=findViewById(R.id.membersExtraText);

        if(type==0)
            finish();
        else if(type==15)
        {
            circleProfileImage.setImageResource(R.drawable.ic_account_group_white);
            getSupportActionBar().setTitle(R.string.create_group);
        //name.setHint("Enter group name");
            fNameLayout.setHint(getString(R.string.group_name));
            fDescriptionLayout.setHint(getString(R.string.group_description));
            }
        else if(type==16)
        {
            circleProfileImage.setImageResource(R.drawable.ic_speak_white_image);
            getSupportActionBar().setTitle(R.string.create_channel);
            fNameLayout.setHint(getString(R.string.channel_name));
            fDescriptionLayout.setHint(getString(R.string.channel_description));
        }
        ChipsLayoutManager chipsLayoutManager = ChipsLayoutManager.newBuilder(this)
                //set vertical gravity for all items in a row. Default = Gravity.CENTER_VERTICAL
                .setChildGravity(Gravity.TOP)
                //whether RecyclerView can scroll. TRUE by default
                .setScrollingEnabled(true)
                //set maximum views count in a particular row
                .setMaxViewsInRow(2)
                //set gravity resolver where you can determine gravity for item in position.
                //This method have priority over previous one
                .setGravityResolver(new IChildGravityResolver() {
                    @Override
                    public int getItemGravity(int position) {
                        return Gravity.CENTER;
                    }
                })
                //you are able to break row due to your conditions. Row breaker should return true for that views
                .setRowBreaker(new IRowBreaker() {
                    @Override
                    public boolean isItemBreakRow(@IntRange(from = 0) int position) {
                        return position == 6 || position == 11 || position == 2;
                    }
                })
                //a layoutOrientation of layout manager, could be VERTICAL OR HORIZONTAL. HORIZONTAL by default
                .setOrientation(ChipsLayoutManager.HORIZONTAL)
                // row strategy for views in completed row, could be STRATEGY_DEFAULT, STRATEGY_FILL_VIEW,
                //STRATEGY_FILL_SPACE or STRATEGY_CENTER
                .setRowStrategy(ChipsLayoutManager.STRATEGY_CENTER_DENSE)
                // whether strategy is applied to last row. FALSE by default
                .withLastRow(true)
                .build();
        groupMembersList.setLayoutManager(chipsLayoutManager);
        groupMembersList.setNestedScrollingEnabled(false);
        fillfinalDataWithSelf();
        adap=new Adapter_NewGroupMembers(this, finaldata, new JobCompletionListner() {
            @Override
            public void onJobCompletion(String txt1) {
                updateUIwithFinalDataSize();
            }
        });
        groupMembersList.setAdapter(adap);
        updateUIwithFinalDataSize();
        //root = (RelativeLayout) findViewById(R.id.rootrelative_profilesetup);



       // actions = new EmojIconActions(getApplicationContext(), root, name, emoticon);
        // actions.ShowEmojIcon();
       // emoticon.setImageResource(R.drawable.ic_emoticon_semiblack);

        databaseRef = FirebaseDatabase.getInstance().getReference();

        mStorageRef = FirebaseStorage.getInstance().getReference();
        riversRef = mStorageRef.child("profile_images").child(randomStringGenerator() + ".png");

         createGroup=findViewById(R.id.createGroup);
        completeButton = (FloatingActionButton) findViewById(R.id.completeButton);
        completeButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                    
                    Intent intent = new Intent(getApplicationContext(), AllUsersList.class);
                    //Bundle args = new Bundle();
                    //args.putSerializable("ARRAYLIST",(Serializable)chatsArray);
                    intent.putExtra("selectedpeople",groupMemberList);
                    intent.putExtra("returnData",true);
                    // intent.putExtra("chatsArray",chatsArray);

                    startActivityForResult(intent,2);


            }
        });

        createGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateData())
                {
                if (directory != null) {
                    AroundAlertBoxManager.showProgress(CreateNewGroup.this,null,type==15?R.string.creating_group:R.string.creating_channel,false);
                    //Log.e("MSGG","ready for image upload");
                    riversRef.putFile(directory).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                            groupMemberList.add(TabbedActivity.getUserDetail().getUid());

                            final Uri downloadUrl = taskSnapshot.getDownloadUrl();

                            final ContactListData map = new ContactListData();
                            map.user_name = name.getText().toString();
                            map.type = type;
                            map.img_url = downloadUrl.toString();
                            Log.e("GGG","uriuri storage upload complete url: "+downloadUrl.toString());
                            map.status = "null";
                            DatabaseReference groupRef = FirebaseDatabase.getInstance().getReference().child("user_profile").push();
                            final String key = groupRef.getKey();
                            map.user_id = key;
                            map.location="null";
                            //map.number=userdetail.getPhoneNumber();
                            map.music_url="null";



                            groupRef.setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    Log.e("GGG","data com,pleted upload ");
                                    if (task.isSuccessful()) {
                                        map.user_id = key;
                                        map.img_url = directory.toString();
                                        Log.e("GGG","data success upload ");

                                        saveContactToDatabase(new UserWholeData(map,new UserPrivacyData().setFullPublicPrivacy()) , downloadUrl.toString(), true);

                                        TabbedActivity.saveContactMusicToDatabase(map.user_id,"null",map.music_url,System.currentTimeMillis());

                                        databaseRef.child("user_profile").child(key).child("user_privacy").setValue(new UserPrivacyData().setFullPublicPrivacy()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {

                                                }}});

                                        //movetoMemberSelection();
                                        addMemberToGroupDatas(key,new JobCompletionListner(){
                                            @Override
                                            public void onJobCompletion(String txt1) {
                                                map.img_url=directory.toString();
                                                Log.e("MSGG","updating chatlist");
                                                //updateChatlistWithGroup(map,key);
                                                AroundAlertBoxManager.cancelProgress();
                                                finish();
                                            }
                                        });

                                    }
                                    else
                                        AroundAlertBoxManager.cancelProgress();
                                }
                            });

                        }
                    })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    Toast.makeText(CreateNewGroup.this, exception.getMessage(), Toast.LENGTH_LONG).show();
                                    AroundAlertBoxManager.cancelProgress();
                                }
                            });


                } else {
                    Toast.makeText(CreateNewGroup.this, R.string.choose_an_image, Toast.LENGTH_SHORT).show();
                }
                }
            }
        });

        chooseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickFromGallery();
            }
        });

        circleProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickFromGallery();
            }
        });




    }
    @Override
    protected void onStart() {
        super.onStart();
        AppOnlineStatusManager.OnlineStatus.setActive(true);

    }
    @Override
    public void onStop() {
        AppOnlineStatusManager.OnlineStatus.setActive(false);
        super.onStop();
    }

    void updateUIwithFinalDataSize()
    {

        if(type==15)
        {
            membersExtraText.setText(getApplicationContext().getResources().getQuantityString(R.plurals.member_amount, finaldata.size(), finaldata.size()));
                    //finaldata.size()+" Members");
        }
        else if(type==16)
        {
            membersExtraText.setText(getApplicationContext().getResources().getQuantityString(R.plurals.listener_amount, finaldata.size(), finaldata.size()));

        }
    }

    boolean validateData()
    {
        if(name.getText().toString().trim().length()<=0)
        {
            fNameLayout.setErrorEnabled(true);
            fNameLayout.setError(getString(R.string.name_cannot_beempty_error));
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    fNameLayout.setErrorEnabled(false);
                }
            }, 3000);
            return false;
        }
        else if(finaldata.size()<2)
        {
            Toast.makeText(this, R.string.minimum_participants_error, Toast.LENGTH_SHORT).show();
            return false;
        }
        else
            return true;
    }

    void addMemberToGroupDatas(final String key,JobCompletionListner listner)
    {
        //Log.e("MSGG","upload members");
        for (int i=0;i<groupMemberList.size();i++)
        {
            MainServiceThread.addNewUserToGroup(getApplicationContext(),groupMemberList.get(i),key);
        }
        listner.onJobCompletion("");
    }

    void updateChatlistWithGroup(ContactListData contact,String groupID)
    {
        SQLiteDatabase db = DatabaseManager.getDatabase().getWritableDatabase();

        ContentValues values2 = new ContentValues();
        values2.put(DatabaseAccesser.chatList_lastmsg, "You created the group.");
        values2.put(DatabaseAccesser.chatList_lastmsg_type, 0);
        values2.put(DatabaseAccesser.chatList_lastmsgbyme, 0);
        values2.put(DatabaseAccesser.chatList_msgqueue, 0);
        values2.put(DatabaseAccesser.chatList_lastmsgRowId, 0);
        values2.put(DatabaseAccesser.chatList_last_msgseen, 0);
        values2.put(DatabaseAccesser.chatList_name, contact.user_name);
        values2.put(DatabaseAccesser.chatList_userid, groupID);
        values2.put(DatabaseAccesser.chatList_timestamp, System.currentTimeMillis());
        values2.put(DatabaseAccesser.chatList_url, contact.img_url);

        int rowln=db.update(DatabaseAccesser.chatList_table,values2, DatabaseAccesser.chatList_userid+" =?",new String[]{groupID});
        long srowln=0;
        if(rowln==0)
            srowln = db.insert(DatabaseAccesser.chatList_table, null, values2);

        Log.e("MSG",rowln+" "+srowln);

        Intent new_intent = new Intent();
        new_intent.setAction("CHAT_UPDATED");
        sendBroadcast(new_intent);
    }


    public void movetoMemberSelection() {
        //Intent intent = new Intent(this, TabbedActivity.class);
        finish();
        //startActivity(intent);
    }

    public String randomStringGenerator() {
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0;i < 20; i++)
        {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == 1 && resultCode == RESULT_OK) {

        if (data != null && data.getBooleanExtra("delete", false)) {
            // avatarBytes = null;
            circleProfileImage.setImageDrawable(getApplicationContext().getResources().getDrawable(R.drawable.default_user));
            directory=null;
        } else {

            Uri inputFile  = (data != null ? data.getData() : null);
            if (inputFile == null && photoCameraURI != null) {
                inputFile = photoCameraURI;
            }

            Log.e("GGG","filefile is "+inputFile);

            //directory = MediaManager.getMediaOgUriFromTemp2(getApplicationContext(),inputFile);
            Log.e("GGG","filefile2 is "+directory);
            /*if(directory==null)
            {
                Log.e("GGG","filefile NOTE FOUND IN TABLE "+inputFile);
                directory= MediaManager.getMediaUriAfterCopyingtoCache(getApplicationContext(), inputFile, null);
            }*/
            //circleProfileImage.setImageURI(inputFile);

            Intent cropIntent = new Intent(this, ImageCropActivity.class);
            cropIntent.putExtra("uri", inputFile.toString());
            cropIntent.putExtra("mode", 1);
            startActivityForResult(cropIntent, 5);

            // new Crop(inputFile).output(outputFile).asSquare().start(this);
        }
    }
        else if (requestCode == 5 && resultCode == RESULT_OK) {
            try {

                directory=data.getData();

                Log.e("GGG","filefile2 after CROP is "+directory);
               //directory=Uri.parse(data.getStringExtra("uri"));


                //MediaManager.copyFileFromUri(getApplicationContext(),directory,"sdfsdfdsfdf.png");
                //Log.e("GGG","uriuri got data: "+data.getData()+" / convertes  is "+directory);
                circleProfileImage.setImageURI(directory);

            } catch (Exception e) {
                Toast.makeText(this, e.getMessage() + e.toString(), Toast.LENGTH_LONG).show();
            }

        }

        else if (requestCode == 2 && resultCode == RESULT_OK) {
            try {
                Bundle args = data.getBundleExtra("BUNDLE");
                if(args!=null)

                {
                    groupMemberList = (ArrayList<String>) args.getSerializable("ARRAYLIST");

                    if(groupMemberList.size()>0)
                    {
                        //readyForCreation=true;
                        fillGroupMembersDetails(groupMemberList);
                        //Toast.makeText(this,"got total: "+groupMemberList.size() , Toast.LENGTH_SHORT).show();

                    }
                    else
                    {
                        //Toast.makeText(this,"At least 1 member required" , Toast.LENGTH_SHORT).show();

                    }

                }

            } catch (Exception e) {
                Toast.makeText(this, e.getMessage() + e.toString(), Toast.LENGTH_LONG).show();
            }

        }


    }

    void fillfinalDataWithSelf()
    {
        finaldata.add(ChatWindow.returnSingleUserDetails(this,FirebaseAuth.getInstance().getUid()));
    }

    void fillGroupMembersDetails(final ArrayList<String> memberUserID)
    {
        finaldata.clear();
        fillfinalDataWithSelf();
        /*Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {*/
                for (String test:memberUserID) {
                    UserWholeData ccc=ChatWindow.returnSingleUserDetails(getApplicationContext(),test);
                    if(ccc!=null)
                        finaldata.add(ccc);
                }
                adap.notifyDataSetChanged();
                updateUIwithFinalDataSize();

           /* }
        });
        thread.start();*/

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        if(photoCameraURI!=null)
        savedInstanceState.putString("contentUri", photoCameraURI.toString());

    }
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        String mCurrentPhotoPath=savedInstanceState.getString("contentUri");
        if(mCurrentPhotoPath!=null)
            photoCameraURI=Uri.parse(mCurrentPhotoPath);
    }
    Uri photoCameraURI;
    private void pickFromGallery() {
    Log.e("GGG","intentintent1 ");
        boolean cameraPermission=true;
        if (!PermissionManager.checkExternalStoragePermission(this))
        {
         return;
        }
        cameraPermission=PermissionManager.checkHasPermission(this);



        File photoFile=null;
        try {
            photoFile=MediaManager.createImageFile(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        photoCameraURI = FileProvider.getUriForFile(this,
                "com.hash.around.fileprovider",
                photoFile);
        Intent chooserIntent = createAvatarSelectionIntent(CreateNewGroup.this,photoCameraURI, directory!=null, cameraPermission);
        Log.e("GGG","intentintent8 ");
        startActivityForResult(chooserIntent, 1);

    }

    public static Intent createAvatarSelectionIntent(Context activity,@Nullable Uri captureFile, boolean includeClear, boolean includeCamera) {
        List<Intent> extraIntents  = new LinkedList<>();
        Log.e("GGG","intentintent3 ");
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        galleryIntent.setType("image/*");
        //intent3.addCategory("com.hash.around");
        //intent3.addCategory("com.hash.around");
        Log.e("GGG","intentintent3 ");
        if (!isResolvable(activity, galleryIntent)) {
            //Log.e("GGG","picker NOT resolvable");
            galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
            galleryIntent.setType("image/*");
        }
        Log.e("GGG","intentintent4 ");
       if (includeCamera) {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            if (captureFile != null && cameraIntent.resolveActivity(activity.getPackageManager()) != null) {
                cameraIntent.putExtra(EXTRA_OUTPUT, captureFile);
                extraIntents.add(cameraIntent);
            }
        }
        Log.e("GGG","intentintent5 ");
        if (includeClear) {
            extraIntents.add(new Intent("com.hash.around.action.CLEAR_PROFILE_PHOTO"));
        }
       /* Intent intent3 = new Intent(Intent.ACTION_PICK);
        intent3.setType("image/*");
        intent3.addCategory("com.hash.around");
        extraIntents.add(intent3);*/

        Intent chooserIntent = Intent.createChooser(galleryIntent, "Choose Image");
        Log.e("GGG","intentintent6 ");
        if (!extraIntents.isEmpty()) {
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents.toArray(new Intent[0]));
        }

        Log.e("GGG","intentintent7 ");
        return chooserIntent;
    }

    public static Intent createMusicSelectionIntent(Context activity, boolean includeClear)
    {

        Intent galleryIntent = new Intent(Intent.ACTION_PICK);
        galleryIntent.setType("audio/*");

        Log.e("GGG","intentintent3 ");
        if (!isResolvable(activity, galleryIntent)) {
            //Log.e("GGG","picker NOT resolvable");
            galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
            galleryIntent.setType("audio/*");
        }
        Intent chooserIntent = Intent.createChooser(galleryIntent, "Choose Audio");
        return chooserIntent;
    }

    public static boolean isResolvable(@NonNull Context context, @NonNull Intent intent) {
        List<ResolveInfo> resolveInfoList = context.getPackageManager().queryIntentActivities(intent, 0);
        return resolveInfoList != null && resolveInfoList.size() > 1;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 10:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //locationok();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                           // locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0, listner);

                        }
                    }
                }
                break;
            case 11:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //locationok();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                                checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                                checkSelfPermission(Manifest.permission.INTERNET) == PackageManager.PERMISSION_GRANTED) {


                        }
                    }
                }
                break;

        }
    }



    class Adapter_NewGroupMembers extends RecyclerView.Adapter<Adapter_NewGroupMembers.ViewHolder>{

        Context mContext;
        public ArrayList<UserWholeData> contactdata;
        //ArrayList<GroupMemberData> memberdata;
        public int optionPosition=-1;

        public Integer userPrivilage=null;
        JobCompletionListner listener;
        public Adapter_NewGroupMembers(Context mcont, ArrayList<UserWholeData> contact,JobCompletionListner listener)
        {
            mContext=mcont;
            this.contactdata=contact;
            //this.memberdata=member;
            this.userPrivilage=userPrivilage;
            this.listener=listener;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v;

            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.single_chip_contact_view, parent, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;

        }

        @Override
        public void onBindViewHolder(final Adapter_NewGroupMembers.ViewHolder holder, int position) {

            final int pos = position;



            holder.name.setText(contactdata.get(position).user_name);


            String dir=contactdata.get(position).img_url;
            if(dir.equals("null"))
            {
                holder.proimg.setImageResource(R.drawable.default_user);
            }
            else
            {
                holder.proimg.setImageURI(Uri.parse(dir));
            }

            if(contactdata.get(pos).user_id.equals(FirebaseAuth.getInstance().getUid()))
            {
                holder.quote_dismiss.setVisibility(View.GONE);
                holder.name.setText(R.string.chat_me);
            }
            else {
                holder.quote_dismiss.setVisibility(View.VISIBLE);

                holder.quote_dismiss.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                   /* Context cont = mContext;
                    Intent indi = new Intent(cont, ChatWindow.class);
                    indi.putExtra("userid", contactdata.get(pos).user_id);
                    indi.putExtra("username", contactdata.get(pos).user_name);
                    ((AppCompatActivity) mContext).finish();
                    cont.startActivity(indi);*/
                        ViewFunctions.independantAnimateOut(holder.itemView, mContext);
                        contactdata.remove(pos);
                        listener.onJobCompletion("");
                        notifyItemChanged(pos);


                    }
                });
            }
        }


        @Override
        public int getItemCount() {
            if(contactdata!=null) {
                return contactdata.size();
            }
            return 0;
        }


        public class ViewHolder extends RecyclerView.ViewHolder {

            public TextView name;

            public CircularImageView proimg;
            ImageView quote_dismiss;


            public ViewHolder(View item)
            {
                super(item);
                name=(TextView)item.findViewById(R.id.contactName);
                proimg= item.findViewById(R.id.contactImg);
                quote_dismiss= item.findViewById(R.id.quote_dismiss);
            }
        }

        }






}