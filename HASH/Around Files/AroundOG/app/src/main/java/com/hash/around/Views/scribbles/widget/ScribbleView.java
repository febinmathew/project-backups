/*
 * Copyright (C) 2016 Open Whisper Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.hash.around.Views.scribbles.widget;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.Target;

import com.hash.around.OtherClasses.Interfaces.BitmapRenderListener;
import com.hash.around.R;
//import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader.DecryptableUri;
//import org.thoughtcrime.securesms.mms.GlideRequests;
import com.hash.around.TabFragments.MapsFragment;
import com.hash.around.Utils.ByteUtils;
import com.hash.around.Views.Glide.GlideApp;
import com.hash.around.Views.Glide.GlideRequests;
import com.hash.around.Views.scribbles.widget.entity.MotionEntity;
import com.hash.around.Views.scribbles.widget.entity.TextEntity;
//import org.thoughtcrime.securesms.util.Util;
//import org.thoughtcrime.securesms.util.concurrent.ListenableFuture;
//import org.thoughtcrime.securesms.util.concurrent.SettableFuture;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.concurrent.ExecutionException;

public class ScribbleView extends FrameLayout {

  public TextEntity currentTextEntity() {
    if (getSelectedEntity() instanceof TextEntity) {
      return ((TextEntity) getSelectedEntity());
    } else {
      return null;
    }
  }

  private static final String TAG = ScribbleView.class.getSimpleName();

  private ImageView imageView;
  private MotionView motionView;
  private CanvasView canvasView;

  private @Nullable Uri imageUri;

  public ScribbleView(Context context) {
    super(context);
    initialize(context);
  }

  public ScribbleView(Context context, AttributeSet attrs) {
    super(context, attrs);
    initialize(context);
  }

  public ScribbleView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    initialize(context);
  }

  @TargetApi(Build.VERSION_CODES.LOLLIPOP)
  public ScribbleView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
    super(context, attrs, defStyleAttr, defStyleRes);
    initialize(context);
  }

  public void setImage(/*@NonNull GlideRequests glideRequests,*/ @NonNull Uri uri) {
    this.imageUri     = uri;

    /*glideRequests.load(new DecryptableUri(uri))
                 .diskCacheStrategy(DiskCacheStrategy.NONE)
                 .fitCenter()
                 .into(imageView);*/
    //imageView.setImageResource(R.drawable.default_user);
    Glide.with(getContext()).load(this.imageUri)
            .into( imageView);
    //imageView.setImageURI(this.imageUri);
  }
  public void setImageResource(int drawable) {

    Glide.with(getContext()).load(drawable)
            .into( imageView);

  }

  @SuppressLint("StaticFieldLeak")
  public @NonNull void getRenderedImage(final @NonNull Activity glideRequests, final BitmapRenderListener bitmapListener) {
   // final SettableFuture<Bitmap> future      = new SettableFuture<>();
    final Context                context     = getContext();
    final boolean                isLowMemory = ByteUtils.isLowMemory(context);

    if (imageUri == null) {
      bitmapListener.onSuccess(null);
      //return future;
    }

    new AsyncTask<Void, Void, Bitmap>() {
      @Override
      protected @Nullable Bitmap doInBackground(Void... params) {
        try {
          int width  = Target.SIZE_ORIGINAL;
          int height = Target.SIZE_ORIGINAL;

          if (isLowMemory) {
            width  = 768;
            height = 768;
          }
          BitmapFactory.Options options = new BitmapFactory.Options();
          //options.inScaled = false;
          options.inMutable = true;

          File xxx=new File(imageUri.getPath());
          Log.e("GGG","bitbit file "+xxx);

          InputStream inputStream;

          if ("file".equals(imageUri.getScheme())) {
            inputStream = new FileInputStream(imageUri.getPath());
          } else {
            inputStream = context.getContentResolver().openInputStream(imageUri);
          }
          Bitmap ccc=BitmapFactory.decodeStream(inputStream,new Rect(0,0,0,0),options);
          Log.e("GGG","bitbit1 "+ccc);



          /*Log.e("GGG","bitbit1");
          File image = new File(imageUri.getPath());
          Log.e("GGG","bitbit1.4");
          BitmapFactory.Options bmOptions = new BitmapFactory.Options();
          Log.e("GGG","bitbit1.5");
          //Bitmap bitmap = BitmapFactory.decodeFile(image.getPath(),bmOptions);
          Log.e("GGG","bitbit1.6");
         // bitmap = Bitmap.createScaledBitmap(bitmap,width,height,true);
          Log.e("GGG","bitbit1.7");
         // Log.e("GGG","bitbit2 "+ccc);*/

          return ccc;
          /*return  Glide                  .load(imageUri)
                  .diskCacheStrategy(DiskCacheStrategy.NONE)
                  .skipMemoryCache(true)
                  .into(width, height)
                  .get();*/

          /*return glideRequests.asBitmap()
                              .load(new DecryptableUri(imageUri))
                              .diskCacheStrategy(DiskCacheStrategy.NONE)
                              .skipMemoryCache(true)
                              .into(width, height)
                              .get();*/
        } catch (/*InterruptedException | ExecutionException*/ Exception e) {
          Log.w(TAG, e);
          return null;
        }
      }

      @Override
      protected void onPostExecute(@Nullable Bitmap bitmap) {
        Log.e("GGG","bitbit3 "+bitmap);
        if (bitmap == null) {
          bitmapListener.onSuccess(null);
          return;
        }

        Canvas canvas = new Canvas(bitmap);
        motionView.render(canvas);
        canvasView.render(canvas);
        Log.e("GGG","bitbit4 redercomplete"+bitmap);
        bitmapListener.onSuccess(bitmap);
      }
    }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    //return future;
  }

  private void initialize(@NonNull Context context) {
    inflate(context, R.layout.scribble_view, this);

    this.imageView  = findViewById(R.id.image_view);
    this.motionView = findViewById(R.id.motion_view);
    this.canvasView = findViewById(R.id.canvas_view);
  }

  public void setMotionViewCallback(MotionView.MotionViewCallback callback) {
    this.motionView.setMotionViewCallback(callback);

  }

  boolean drawMode=false;
  public void setDrawingMode(boolean enabled) {
    drawMode=enabled;
    this.canvasView.setActive(enabled);
    if (enabled) this.motionView.unselectEntity();
  }

  public void setDrawingBrushColor(int color) {
    this.canvasView.setPaintFillColor(color);
    this.canvasView.setPaintStrokeColor(color);
  }

  public void addEntityAndPosition(MotionEntity entity) {
    this.motionView.addEntityAndPosition(entity);
  }
  private MapsFragment.OnTouchListener touchListener=null;
  public void setTouchListener(MapsFragment.OnTouchListener listener) {
    touchListener = listener;
  }
  @Override
  public boolean dispatchTouchEvent(MotionEvent ev) {
    if(touchListener!=null && (drawMode||(getSelectedEntity()!=null)))//||ev.getAction()==MotionEvent.ACTION_DOWN))//.isActivated())) //||getSelectedEntity()!=null
    {
      touchListener.onTouch();

    }
      return super.dispatchTouchEvent(ev);

    //Log.e("GGG","successss");
    //return super.dispatchTouchEvent(ev);
    //return true;
  }

  public MotionEntity getSelectedEntity() {
    return this.motionView.getSelectedEntity();
  }

  public void deleteSelected() {
    this.motionView.deletedSelectedEntity();
  }

  public void clearSelection() {
    this.motionView.unselectEntity();
  }

  public void undoDrawing() {
    this.canvasView.undo();

  }

  public void startEditing(TextEntity entity) {
    this.motionView.startEditing(entity);
  }

  @Override
  public void onMeasure(int width, int height) {
    super.onMeasure(width, height);

    setMeasuredDimension(imageView.getMeasuredWidth(), imageView.getMeasuredHeight());

    canvasView.measure(MeasureSpec.makeMeasureSpec(imageView.getMeasuredWidth(), MeasureSpec.EXACTLY),
                       MeasureSpec.makeMeasureSpec(imageView.getMeasuredHeight(), MeasureSpec.EXACTLY));

    motionView.measure(MeasureSpec.makeMeasureSpec(imageView.getMeasuredWidth(), MeasureSpec.EXACTLY),
                       MeasureSpec.makeMeasureSpec(imageView.getMeasuredHeight(), MeasureSpec.EXACTLY));
  }

}
