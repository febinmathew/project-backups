package com.hash.around.OtherClasses.Interfaces;

import com.hash.around.OtherClasses.DataTypes.FeedData;
import com.hash.around.OtherClasses.DataTypes.GroupFeedData;

public interface FeedDownloadListner {
    void onFeedReceived(GroupFeedData data,boolean autoDownload);
    void onFeedDownloaded(GroupFeedData data);
}


