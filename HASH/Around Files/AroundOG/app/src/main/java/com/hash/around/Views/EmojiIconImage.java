package com.hash.around.Views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.support.annotation.Nullable;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.storage.StorageReference;
import com.hash.around.OtherClasses.DataTypes.GroupFeedData;
import com.hash.around.OtherClasses.Interfaces.FeedSentListner;
import com.hash.around.R;
import com.hash.around.Utils.MediaManager;
import com.hash.around.Utils.StringUtils;
import com.hash.around.Views.CustomMakers.AroundDownloadView;
import com.hash.around.Views.Medias.CircularRelativeLayout;
import com.hash.around.Views.Medias.SingleViewDownloadHandler;
import com.vanniktech.emoji.EmojiEditText;
import com.vanniktech.emoji.EmojiPopup;
import com.vanniktech.emoji.EmojiTextView;
import com.vanniktech.emoji.listeners.OnEmojiPopupDismissListener;

//import org.thoughtcrime.securesms.R;

public class EmojiIconImage extends ImageView {

  public EmojiIconImage(Context context) {
    super(context);
  }

  public EmojiIconImage(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
  }

  public EmojiIconImage(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

   EmojiPopup emojiPopup;
  public void setTargetText(EmojiEditText editText,View rootView)
  {
    setImageResource(R.drawable.ic_emoticon_semiblack);
     emojiPopup = EmojiPopup.Builder.fromRootView(rootView).setOnEmojiPopupDismissListener(new OnEmojiPopupDismissListener() {
      @Override
      public void onEmojiPopupDismiss() {
        setImageResource(R.drawable.ic_emoticon_semiblack);
      }
    }).build(editText);

    setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        if(emojiPopup.isShowing())
        {
          setImageResource(R.drawable.ic_emoticon_semiblack);
        }
        else {
          setImageResource(R.drawable.ic_keyboard_semiblack);
        }
        emojiPopup.toggle();
      }
    });
  }
}
