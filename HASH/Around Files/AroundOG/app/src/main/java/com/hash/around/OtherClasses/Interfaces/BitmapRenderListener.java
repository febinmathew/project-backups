package com.hash.around.OtherClasses.Interfaces;

import android.graphics.Bitmap;

import com.hash.around.OtherClasses.DataTypes.GroupFeedData;

public interface BitmapRenderListener {
    void onSuccess(Bitmap bitmap);
    void onFailure(Exception e);

}


