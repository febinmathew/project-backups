package com.hash.around.Views.Medias;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.widget.RelativeLayout;

import com.hash.around.R;


public class SquareRelativeLayout extends RelativeLayout {

    boolean sameAsHeight=true;
    public SquareRelativeLayout(Context context) {
        this(context, null);
    }

    public SquareRelativeLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SquareRelativeLayout(final Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);


        if (attrs != null) {
            TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.SquareRelativeLayout, 0, 0);
            this.sameAsHeight  = typedArray.getBoolean(R.styleable.SquareRelativeLayout_sameAsHeight, true);
            typedArray.recycle();
        }

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        if(sameAsHeight)
            super.onMeasure(heightMeasureSpec,heightMeasureSpec);
        else
            super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}
