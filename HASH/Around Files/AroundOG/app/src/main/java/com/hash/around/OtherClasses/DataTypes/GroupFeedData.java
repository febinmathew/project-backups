package com.hash.around.OtherClasses.DataTypes;

/**
 * Created by Febin on 26-08-2017.
 */
import android.location.Location;

import java.io.Serializable;
import java.util.ArrayList;

public class GroupFeedData extends FeedData implements  Serializable {


    public long media_id;
    public long feedRow;
    public int ifseen;
    public Location loc;

    public  int seen=0;
    public  int unseen=0;
    public  int seeingFeedNum=0;

    public String feedID="null";

    public double latitude;
    public double longitude;

    public GroupFeedData()
    {}
    public GroupFeedData(GroupFeedData dat)
    {
        super(dat);
        this.media_id=dat.media_id;
        this.feedRow=dat.feedRow;
        this.ifseen=dat.ifseen;
        this.loc=dat.loc;

        this.seen=dat.seen;
        this.unseen=dat.unseen;
        this.seeingFeedNum=dat.seeingFeedNum;
    }

    public static int checkFeedUserInArrayList(ArrayList<GroupFeedData> list,String userID)
    {
        for(int i=0;i<list.size();i++)
        {
            if(list.get(i).userid.equals(userID))
                return i;
        }
        return -1;
    }
}


