package com.hash.around.Views;


import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.hash.around.OtherClasses.DataTypes.FeedData;
import com.hash.around.OtherClasses.DataTypes.GroupFeedData;
import com.hash.around.OtherClasses.DataTypes.MediaData;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.OtherClasses.Interfaces.FeedSentListner;
import com.hash.around.OtherClasses.Interfaces.JobCompletionWithFailureListner;
import com.hash.around.R;
import com.hash.around.TabFragments.FeedFullScreen;
import com.hash.around.TabFragments.MapsFragment;
import com.hash.around.TabFragments.TabFragment2;
import com.hash.around.TabbedActivity;
import com.hash.around.Utils.DatabaseManager;

import java.util.ArrayList;


public class AroundFeedRailView extends RelativeLayout implements LoaderManager.LoaderCallbacks<Cursor> {

  private final RecyclerView recyclerView;
  final TextView headingTag;
  final TextView amountTag;

  private OnItemClickedListener listener;

  public AroundFeedRailView(Context context) {
    this(context, null);
  }

  public AroundFeedRailView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public AroundFeedRailView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);

    inflate(context, R.layout.recent_photo_view, this);
    this.recyclerView = findViewById(R.id.photo_list);
    this.headingTag = findViewById(R.id.headingTag);
    this.amountTag = findViewById(R.id.amountTag);


    this.recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
    this.recyclerView.setItemAnimator(new DefaultItemAnimator());
  }

  public void setListener(@Nullable OnItemClickedListener listener) {
    this.listener = listener;

    if (this.recyclerView.getAdapter() != null) {
      ((RecentPhotoAdapter) this.recyclerView.getAdapter()).setListener(listener);
    }
  }
  private MapsFragment.OnTouchListener touchListener=null;
  public void setTouchListener(MapsFragment.OnTouchListener listener) {
    touchListener = listener;
  }

  @Override
  public boolean dispatchTouchEvent(MotionEvent ev) {
    if(touchListener!=null)
    touchListener.onTouch();
    return super.dispatchTouchEvent(ev);
  }

  JobCompletionWithFailureListner sendButtonListner;

  public void setSendButtonListener(@Nullable JobCompletionWithFailureListner listener) {
    this.sendButtonListner = listener;

    Log.e("MGG", "adapter status " + this.recyclerView.getAdapter());
    if (this.recyclerView.getAdapter() != null) {
      ((RecentPhotoAdapter) this.recyclerView.getAdapter()).setSendButtonListener(listener);
    }
  }


  @Override
  public android.content.Loader<Cursor> onCreateLoader(int id, Bundle args) {
    String userid=null;
    if(args!=null){
     userid=args.getString("userid");
    String headingText;
    if(userid.equals(TabbedActivity.getUserDetail().getUid()))
      headingText="Your Feeds";
    else
      headingText="Feeds";

    headingTag.setVisibility(VISIBLE);
    headingTag.setText(headingText);
    }
    else
    { headingTag.setVisibility(GONE);
      headingTag.setText("Feeds");
    }

    return new SelfFeedLoader(getContext(),userid);
  }

  @Override
  public void onLoadFinished(android.content.Loader<Cursor> loader, Cursor data) {
    Log.e("GGG","total self feed countis "+data.getCount());
    if(data.getCount()==0)
      this.setVisibility(GONE);
    else
    {
      this.setVisibility(VISIBLE);
      amountTag.setVisibility(VISIBLE);
      amountTag.setText(data.getCount()+"");
    }
    this.recyclerView.setAdapter(new RecentPhotoAdapter(getContext(), data, listener, sendButtonListner));
  }

  @Override
  public void onLoaderReset(android.content.Loader<Cursor> loader) {
    ((CursorRecyclerViewAdapter) this.recyclerView.getAdapter()).changeCursor(null);
  }




  private static class RecentPhotoAdapter extends CursorRecyclerViewAdapter<RecentPhotoAdapter.FeedRailItemViewHolder> {

    @SuppressWarnings("unused")
    private static final String TAG = RecentPhotoAdapter.class.getName();


    @Nullable
    private OnItemClickedListener clickedListener;

    private RecentPhotoAdapter(@NonNull Context context, @NonNull Cursor cursor, @Nullable OnItemClickedListener listener, @Nullable JobCompletionWithFailureListner listener2) {
      super(context, cursor);

      this.clickedListener = listener;
      this.sendButtonListner = listener2;
    }

    @Override
    public FeedRailItemViewHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
      View itemView = LayoutInflater.from(parent.getContext())
              .inflate(R.layout.feed_rail_item, parent, false);

      return new FeedRailItemViewHolder(itemView);
    }

    ArrayList<MediaData> selectedItems = new ArrayList<>();
    String gifMime = MimeTypeMap.getSingleton().getMimeTypeFromExtension("gif");

    @Override
    public void onBindItemViewHolder(final FeedRailItemViewHolder viewHolder, @NonNull Cursor cursor) {
//      viewHolder.imageView.setImageDrawable(null);

//      long id = cursor.getLong(cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns._ID));

     /*final long feedRow = cursor.getLong(
              cursor.getColumnIndexOrThrow("rowID"));
      int feedType = cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_type));
      int feedSeen = cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_seen));
    final  String userID = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_user_id));
      final  String user_name = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_username));
      String uri = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseAccesser.media_lite_local_uri));
      String final_global_url = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseAccesser.media_global_uri));
      String feedMessage = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_text));
      //long feedSize = cursor.getLong(cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_s));*/

      final GroupFeedData feedDat=TabFragment2.returnSingleGroupFeedDataFromCursor(cursor);

  Log.e("GGG","rail seeen is : "+feedDat.ifseen);

      if(feedDat.type==0)
      {
        viewHolder.feedItem.setText(feedDat.text);
        //viewHolder.feedItem.hideImage();
      }
      else if(feedDat.type==2)
      {
        if(feedDat.uri==null||feedDat.uri.equals("null")||feedDat.uri.contains("http"))
        {
          Log.e("GGG","nullnullnull");
        }
        else {
          viewHolder.feedItem.setAudio(Uri.parse(feedDat.uri));
        }
      }
      else if(feedDat.type==1||feedDat.type==3||feedDat.type==5)
      {
        if(feedDat.uri==null||feedDat.uri.equals("null")||feedDat.uri.contains("http"))
        {
          Log.e("GGG","nullnullnull");
        }
        else
        {
        viewHolder.feedItem.setImgResource(Uri.parse(feedDat.uri));
          Log.e("GGG","notnulllll");
      }
      }

      else
        {
         // Log.e("GGG","shittttttttt "+feedType);
        }

      viewHolder.feedItem.setOnClickListner(new OnClickListener() {
        @Override
        public void onClick(View v) {
          openWithSelectedFeed(feedDat.userid,feedDat.username,feedDat.feedRow);
        }
      });

      if(feedDat.type!=0&&feedDat.ifseen==0) {

        viewHolder.feedItem.setLocallyAvailable(false);

       viewHolder.feedItem.setUploadClickListner(feedDat.uri,feedDat
        ,feedDat.media_id,null);

        viewHolder.feedItem.setOnFeedUploadListener(new FeedSentListner() {
          @Override
          public void onFeedAdded(FeedData data) {

          }

          @Override
          public void onFeedUploaded(FeedData data) {
            viewHolder.feedItem.setLocallyAvailable(true);
          }
        });

      }
      else
      {
        viewHolder.feedItem.setLocallyAvailable(true);
      }


      /*final MediaData mediaData = new MediaData();

      mediaData.uri = Uri.withAppendedPath(baseUri, Long.toString(id)).toString();
      mediaData.type = mediaType;
      if (mimeType.equals(gifMime))
        mediaData.type = 5;

      //Log.e("MGG","uri NOW " + uri);
      //Key signature = new MediaStoreSignature(mimeType, dateModified, orientation);

      if (selectedItems.contains(mediaData))
        viewHolder.wholeView.setChecked(true);
      else
        viewHolder.wholeView.setChecked(false);


      viewHolder.wholeView.setJobListner(new JobCompletionWithFailureListner() {
        @Override
        public void onJobFailed(String txt1) {
          viewHolder.wholeView.setChecked(false);
          selectedItems.remove(mediaData);
          updateSendButtonStatus();

          if (clickedListener != null) clickedListener.onItemSelected(selectedItems);

        }

        @Override
        public void onJobCompletion(String txt1) {
          viewHolder.wholeView.setChecked(true);
          selectedItems.add(mediaData);
          updateSendButtonStatus();
          if (clickedListener != null) clickedListener.onItemSelected(selectedItems);

        }
      });
      Log.e("MGG", "gohh NOW " + mediaData.type);
      Glide.with(getContext()).load(Uri.parse(mediaData.uri))
              .thumbnail(0.4f)
              .crossFade()
              .diskCacheStrategy(DiskCacheStrategy.NONE)
              .into(viewHolder.imageView);

      if (mediaData.type == MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO) {
        viewHolder.typeIndicator.setVisibility(View.VISIBLE);
        viewHolder.typeIndicator.setImageResource(R.drawable.ic_video_white);
      } else if (mediaData.type == 5) {
        viewHolder.typeIndicator.setVisibility(View.VISIBLE);
        viewHolder.typeIndicator.setImageResource(R.drawable.ic_gif_white);
      } else
        viewHolder.typeIndicator.setVisibility(View.GONE);

      //viewHolder.imageView.setImageURI(uri);
      //viewHolder.imageView.setMaxWidth(viewHolder.imageView.getHeight());
      viewHolder.imageView.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
          Log.e("MGG", "clicking NOW ");
          viewHolder.wholeView.setChecked(true);
          if (!selectedItems.contains(mediaData))
            selectedItems.add(mediaData);
          if (clickedListener != null) {
            clickedListener.onItemSelected(selectedItems);
            clickedListener.onItemClicked(mediaData);

          }
        }
      });*/

    }

    void openWithSelectedFeed(String userID,String userName, Long targetFeed)
    {
      Intent indi=new Intent(getContext(),FeedFullScreen.class);
      indi.putExtra("username",userName);
      indi.putExtra("userid",userID);
      indi.putExtra("targetFeed",targetFeed);
      getContext().startActivity(indi);
    }
    void updateSendButtonStatus() {
      if (selectedItems.size() == 1) {
        if (sendButtonListner != null)
          sendButtonListner.onJobCompletion("");
      } else if (selectedItems.size() == 0) {
        if (sendButtonListner != null)
          sendButtonListner.onJobFailed("");
      }
    }

    public void setListener(@Nullable OnItemClickedListener listener) {
      this.clickedListener = listener;
    }

    JobCompletionWithFailureListner sendButtonListner;

    public void setSendButtonListener(JobCompletionWithFailureListner listener) {
      sendButtonListner = listener;
    }

    static class FeedRailItemViewHolder extends RecyclerView.ViewHolder {

    FeedRailItemView feedItem;

      FeedRailItemViewHolder(View itemView) {
        super(itemView);
        feedItem = itemView.findViewById(R.id.wholeView);
      }
    }
  }

  public interface OnItemClickedListener {
    void onItemClicked(MediaData uri);

    void onItemSelected(ArrayList<MediaData> uriList);
  }

  public static class SelfFeedLoader extends android.content.CursorLoader {

    private static final String SELECTION = DatabaseAccesser.feed_col_user_id + " =?";

    private final Context context;
    String userid;
    /*public SelfFeedLoader() {
      super(context);
      this.context = context;//.getApplicationContext();
    }*/

    public SelfFeedLoader(Context context,String userid) {
      super(context);
      this.context = context;//.getApplicationContext();
      this.userid=userid;
    }

    @Override
    public Cursor loadInBackground() {

     /* return DatabaseManager.getDatabase(context).getReadableDatabase().query(DatabaseAccesser.feed_table,null,SELECTION,
              new String[]{TabbedActivity.userdetail.getUid()},null,null,DatabaseAccesser.feed_col_timestamp+ " DESC");*/

      return DatabaseManager.getDatabase(context).getReadableDatabase(). rawQuery("select "+DatabaseAccesser.feed_table+".rowid AS rowID"+",* FROM "+DatabaseAccesser.feed_table+" INNER JOIN "+DatabaseAccesser.media_table+" ON "+
              DatabaseAccesser.feed_table+"."+DatabaseAccesser.feed_col_media_id+" = "+ DatabaseAccesser.media_table+".rowid"+ " where "+DatabaseAccesser.feed_col_user_id  +" =?"+" order by "+DatabaseAccesser.feed_col_timestamp+ " DESC",new String[]{this.userid});


    }
  }
}
