package com.hash.around.Views.CustomMakers;

/**
 * Created by Febin on 5/25/2018.
 */


import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;

/**
 * Create On 16/10/2016
 * @author wayne
 */
public class NotificationViewPager extends ViewPager {


    DisplayMetrics dm;
    public NotificationViewPager(@NonNull Context context) {
        super(context);
    }

    public NotificationViewPager(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        dm=new DisplayMetrics();

        ((Activity)context). getWindowManager() .getDefaultDisplay().getMetrics(dm);
    }

    float downPOS;


    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        // Never allow swiping to switch between pages
        final int action = event.getAction();

            if (action == MotionEvent.ACTION_DOWN ) {
                downPOS=event.getX();
            }
            Log.e("GGG","touch Right ACTIVE ");
            if(getCurrentItem()==1 && action == MotionEvent.ACTION_MOVE && (downPOS-event.getX())>0) {
                return false;
            }
            else if(getCurrentItem()==0 && action == MotionEvent.ACTION_MOVE && (downPOS-event.getX())<0) {
                return false;
            }

       return  super.onInterceptTouchEvent(event);
    }


    boolean upActive;


}