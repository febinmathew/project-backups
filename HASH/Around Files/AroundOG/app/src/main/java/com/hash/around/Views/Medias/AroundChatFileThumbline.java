package com.hash.around.Views.Medias;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.storage.StorageReference;
import com.hash.around.MainServiceThread;
import com.hash.around.OtherClasses.DataTypes.DownloadUploadData;
import com.hash.around.OtherClasses.DataTypes.GroupChatData;
import com.hash.around.OtherClasses.Interfaces.ChatSentListner;
import com.hash.around.OtherClasses.Interfaces.FileDownloadListner;
import com.hash.around.OtherClasses.Interfaces.FileUploadListner;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.R;
import com.hash.around.TabbedActivity;
import com.hash.around.Utils.ChatSender;
import com.hash.around.Utils.MediaManager;
import com.hash.around.Utils.PrefManager;
import com.hash.around.Views.CustomMakers.AroundDownloadView;


public class AroundChatFileThumbline extends LinearLayout {
    AroundDownloadView downloadView;
    //ImageView itemIndication;
    //TextView timeIndication;
    public AroundChatFileThumbline(Context context) {
        super(context);
    }

    public AroundChatFileThumbline(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AroundChatFileThumbline(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);


    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        init();
    }
    void init()
    {
        downloadView=findViewById(R.id.downloadView);
        //timeIndication=findViewById(R.id.timeIndication);
        //itemIndication=findViewById(R.id.itemIndication);
    }

    public AroundChatFileThumbline(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setLocallyAvailable(boolean locallyAvailable)
    {
        if(locallyAvailable)
        {
            disableDownloadClick();
        }
        else
        {
            enableDownloadClick();
        }
    }

    public void enableDownloadClick()
    {
        downloadView.setVisibility(VISIBLE);
        //timeIndication.setVisibility(GONE);
        //itemIndication.setVisibility(GONE);
    }
    public void disableDownloadClick() {
        downloadView.setVisibility(GONE);
        //timeIndication.setVisibility(VISIBLE);
        //itemIndication.setVisibility(VISIBLE);
        downloadView.setOnClickListener(null);

    }

    boolean autodownload=false;
    public void setDownloadClickListner(String uri) {
        enableDownloadClick();
        downloadView.setIsDownload();
        final String url = uri;
        Log.e("GGG","autodownload "+"trueee3");
        if (PrefManager.checkSearchMultiSelectPref(getContext(),"data_usage_chat", TabbedActivity.PREF_FIlE,R.array.data_usage_chat_default_values)) //vvvv
        {
            Log.e("GGG","autodownload "+"trueee");
            autodownload = true;
            downloadFile(url);
        }

        if(!autodownload)
        {
            downloadView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    downloadFile(url);
                }
            });
        }
    }

    public void setUploadClickListner(String uri, final GroupChatData data,final long mediaRow,final StorageReference ref) {
        enableDownloadClick();
        downloadView.setIsUpload();
        final String url = uri;
        downloadView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadFile(url,ref,data,mediaRow,null);
            }
        });
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                DownloadUploadData data22=MediaManager.getDownloadListItemArrayFromUri(url);
                if(data22!=null)
                {

                    uploadFile(url,ref,data,mediaRow,data22);
                }
            }
        }, 1000);


    }
    public void setOnFileUploadListener(ChatSentListner job) {
        this.uploadListner = job;
    }
    ChatSentListner uploadListner = null;
    public void setOnJobCompletionListner(JobCompletionListner job) {
        this.jobComplete = job;
    }
    JobCompletionListner jobComplete = null;

    void downloadFile(String urlToDownload) {
        downloadView.startDownload();
        MainServiceThread.downloadFileAndReturnUriString(getContext(),urlToDownload,false,0,6, new FileDownloadListner() {
            @Override
            public void onProgressListener(int percent) {
                downloadView.upDateProgress( percent);
            }

            @Override
            public void onSuccessListener(String localFilename) {
                disableDownloadClick();
                if (jobComplete != null) {
                    jobComplete.onJobCompletion(localFilename);
                }
            }

            @Override
            public void onFailureListener(int percent) {
                downloadView.showDownloadError();
            }

            @Override
            public void onPausedListener(int percent) {
                downloadView.reset();
            }
        });
    }
    void uploadFile(String urlToUpload,StorageReference ref,final GroupChatData data,long mediaID,DownloadUploadData data22) {


        FileUploadListner uploadListner2=new FileUploadListner() {
            @Override
            public void onFileUploaded(String global, String globalLite, String liteLocal) {
                disableDownloadClick();
                Log.e("GGG","downloadme1 COMPKETE DISABLE ");
                if (uploadListner != null) {
                    uploadListner.onChatUploaded(data);
                }
            }

            int prePercent=0;
            @Override
            public void onProgressListener(int percent) {
                Log.e("GGG","downloadme1 PERCENT "+percent);
                if(percent==100 && prePercent==100)
                    return;

                downloadView.upDateProgress(percent);
                prePercent=percent;
            }

            @Override
            public void onFailureListener(int percent) {
                downloadView.showDownloadError();
            }

            @Override
            public void onPausedListener(int percent) {
                downloadView.reset();
            }
        };
        if(data22!=null &&data22.progress>=0 ) {
            Log.e("GGG","downloadme1 with pro "+data22.progress);
            downloadView.startDownload();

        }
        else {

        }
        Log.e("GGG","uploadpro "+(MediaManager.getDownloadListItemArrayFromUri(urlToUpload)!=null));

        if(MediaManager.getDownloadListItemArrayFromUri(urlToUpload)!=null)
        {
            MediaManager.uploadResourceAndReturnUrls(getContext(),data.temp_url,data.type,urlToUpload,ref,uploadListner2);}
        else {
            Log.e("GGG","uploadpro SENTING FILE");
            ChatSender.uploadMediaData(urlToUpload, data, null, uploadListner,uploadListner2 ); }

    }

}