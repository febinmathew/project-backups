package com.hash.around.Views;


import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.hash.around.OtherClasses.DataTypes.MediaData;
import com.hash.around.OtherClasses.Interfaces.JobCompletionWithFailureListner;
import com.hash.around.R;

import java.util.ArrayList;

/*import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.MediaStoreSignature;*/
/*import org.thoughtcrime.securesms.database.CursorRecyclerViewAdapter;
import org.thoughtcrime.securesms.database.loaders.RecentPhotosLoader;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.util.ViewUtil;*/
import com.hash.around.Views.Glide.GlideApp;
import com.hash.around.Views.RecentPhotoViewRail.OnItemClickedListener;

public class AroundPhotoRail extends FrameLayout{

  @NonNull  private final RecyclerView recyclerView;
  @Nullable private       OnItemClickedListener listener;

  public AroundPhotoRail(Context context) {
    this(context, null);
  }

  public AroundPhotoRail(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public AroundPhotoRail(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);

    inflate(context, R.layout.recent_photo_view, this);

    this.recyclerView = findViewById(R.id.photo_list);
    this.recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
    this.recyclerView.setItemAnimator(new DefaultItemAnimator());

  }

  public void setAdapterWithListner(ArrayList<MediaData> mData,Uri uData,@Nullable OnItemClickedListener listener)
  {
    this.listener = listener;
    this.recyclerView.setAdapter(new RecentPhotoAdapter(getContext(), mData, uData, listener));

  }
View prevView;
  public void setSelection(int pos)
  {
    ViewFunctions.scaleView(this.recyclerView.getChildAt(pos).findViewById(R.id.thumbnail),1,0.8f);
    if(prevView!=null)
    {
      ViewFunctions.scaleView(prevView,0.8f,1f);
    }
    prevView=this.recyclerView.getChildAt(pos).findViewById(R.id.thumbnail);
    /*this.listener = listener;
    this.recyclerView.setAdapter(new RecentPhotoAdapter(getContext(), mData, uData, listener));*/
  }
  /*public void setListener(@Nullable OnItemClickedListener listener) {
    this.listener = listener;

    if (this.recyclerView.getAdapter() != null) {
      ((RecentPhotoAdapter)this.recyclerView.getAdapter()).setListener(listener);
    }
  }*/

  @Override
  protected void onFinishInflate() {
    super.onFinishInflate();
    }

  //JobCompletionWithFailureListner sendButtonListner;

  /*public void setSendButtonListener(@Nullable JobCompletionWithFailureListner listener) {
    this.sendButtonListner = listener;

    Log.e("MGG","adapter status "+this.recyclerView.getAdapter());
    if (this.recyclerView.getAdapter() != null) {
      ((RecentPhotoAdapter)this.recyclerView.getAdapter()).setSendButtonListener(listener);
    }
  }*/


  private static class RecentPhotoAdapter extends RecyclerView.Adapter<RecentPhotoAdapter.RecentPhotoViewHolder>  {

    @SuppressWarnings("unused")
    private static final String TAG = RecentPhotoAdapter.class.getName();

    @NonNull  private final Uri defaultUri;
    @Nullable private OnItemClickedListener clickedListener;
    Context mContext;
    ArrayList<MediaData> data;
    private RecentPhotoAdapter(@NonNull Context context, ArrayList<MediaData> datalist,@Nullable Uri defaultUri, @Nullable OnItemClickedListener listener ){
      this.mContext=context;
      data=datalist;
      this.defaultUri         = defaultUri;
      this.clickedListener = listener;
    }

   /* public void onBindItemViewHolder(final RecentPhotoViewHolder viewHolder, @NonNull Cursor cursor) {
      viewHolder.imageView.setImageDrawable(null);



    }*/


    public void setListener(@Nullable OnItemClickedListener listener) {
      this.clickedListener = listener;
    }
    //JobCompletionWithFailureListner sendButtonListner;
    /*public void setSendButtonListener(JobCompletionWithFailureListner listener) {
      sendButtonListner=listener;
    }*/

    @Override
    public RecentPhotoAdapter.RecentPhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      View itemView = LayoutInflater.from(parent.getContext())
              .inflate(R.layout.recent_photo_view_item, parent, false);

      return new RecentPhotoViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecentPhotoAdapter.RecentPhotoViewHolder holder, final int position) {
     Glide.with(mContext).load(Uri.parse(data.get(position).uri))
              .thumbnail(0.4f)
              .into(holder.imageView);

     /* GlideApp.with(mContext).load(Uri.parse(data.get(position).uri))
              .diskCacheStrategy(DiskCacheStrategy.NONE)
              .fitCenter()
              .into(holder.imageView);*/
      holder.imageView.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
          //Log.e("MGG","clicking NOW ");
          if (clickedListener != null) clickedListener.onItemClicked(data.get(position));
        }
      });
    }

    @Override
    public int getItemCount() {
      return data.size();
    }

    static class RecentPhotoViewHolder extends RecyclerView.ViewHolder {

      SquareFrameLayout wholeView;
      ImageView imageView;
      ImageView typeIndicator;

      RecentPhotoViewHolder(View itemView) {
        super(itemView);
        wholeView=itemView.findViewById(R.id.wholeView) ;
        this.imageView = itemView.findViewById(R.id.thumbnail);
        this.typeIndicator=itemView.findViewById(R.id.typeIndicator);
        wholeView.disableSelection();
      }
    }
  }

  /*public interface OnItemClickedListener {
    void onItemClicked(Uri uri, ArrayList<Uri> uriList);
    void onItemSelected(ArrayList<Uri> uriList);
  }*/
}
