package com.hash.around.Views.VideoTrimmer;

/**
 * Created by Febin on 5/25/2018.
 */


        import android.annotation.SuppressLint;
        import android.content.Context;
        import android.graphics.Bitmap;
        import android.graphics.BitmapFactory;
        import android.graphics.Canvas;
        import android.graphics.Rect;
        import android.graphics.drawable.Drawable;
        import android.util.Log;
        import android.util.TypedValue;
        import android.view.View;
        import com.hash.around.R;

/**
 * Create On 16/10/2016
 * @author wayne
 */
@SuppressLint("ViewConstructor")
class ThumbView extends View {

    private static final int EXTEND_TOUCH_SLOP = 25;

    private final int mExtendTouchSlop;

    private Drawable mThumbDrawable;

    private boolean mPressed;

    private int mThumbWidth;
    private int mTickIndex;
    boolean withHandle;
    Bitmap unPressed,isPressed;

    public ThumbView(Context context, int thumbWidth, Drawable drawable,boolean withHandle) {
        super(context);

        this.withHandle=withHandle;
        mThumbWidth = thumbWidth;


        mThumbDrawable = drawable;
        mExtendTouchSlop = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                EXTEND_TOUCH_SLOP, context.getResources().getDisplayMetrics());
        setBackgroundDrawable(mThumbDrawable);

        unPressed=BitmapFactory.decodeResource(getResources(),R.drawable.ic_more_options_black);
        isPressed=BitmapFactory.decodeResource(getResources(), R.drawable.ic_audiotrack_white);

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(MeasureSpec.makeMeasureSpec(mThumbWidth, MeasureSpec.EXACTLY),
                MeasureSpec.makeMeasureSpec(MeasureSpec.getSize(heightMeasureSpec), MeasureSpec.EXACTLY));

        mThumbDrawable.setBounds(0, 0, mThumbWidth, getMeasuredHeight());
    }

    public void setThumbWidth(int thumbWidth) {
        mThumbWidth = thumbWidth;
    }

    public void setThumbDrawable(Drawable thumbDrawable) {
        mThumbDrawable = thumbDrawable;
    }

    public boolean inInTarget(int x, int y) {
        Rect rect = new Rect();
        getHitRect(rect);
        rect.left -= mExtendTouchSlop;
        rect.right += mExtendTouchSlop;
        rect.top -= mExtendTouchSlop;
        rect.bottom += mExtendTouchSlop;
        return rect.contains(x, y);
    }

    public int getRangeIndex() {
        return mTickIndex;
    }

    public void setTickIndex(int tickIndex) {
        mTickIndex = tickIndex;
    }

    @Override
    public boolean isPressed() {
        return mPressed;
    }

    @Override
    public void setPressed(boolean pressed) {

        Log.e("MSG","pressed ");
                mPressed = pressed;
                invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(this.withHandle)
        {
            if(!mPressed)
            myBitmap = unPressed;
            else
                myBitmap = isPressed;

            if(myBitmap!=null) {
                float height = myBitmap.getHeight() * ((getWidth() * 1.0f) / myBitmap.getWidth());

                Log.e("MSG", "height 1 " + (getWidth() * 1.0f) / myBitmap.getWidth());
                myBitmap = Bitmap.createScaledBitmap(myBitmap, getWidth(), (int) height, false);
                int y = (getHeight() / 2) - (myBitmap.getHeight() / 2);
                canvas.drawBitmap(myBitmap, 0, y, null);
            }
        }
    }
    Bitmap myBitmap;
}