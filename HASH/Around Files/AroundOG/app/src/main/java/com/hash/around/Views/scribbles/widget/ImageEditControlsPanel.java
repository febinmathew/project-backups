package com.hash.around.Views.scribbles.widget;

import android.animation.LayoutTransition;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.hash.around.R;

/**
 * Created by Febin on 7/19/2018.
 */

public class ImageEditControlsPanel extends LinearLayout implements View.OnClickListener{

    private ImageView brushView;
    private ImageView textView,undoView,cropView,deleteView,saveView;
    private ImageView stickerView;
    View separatorView;
    public ImageEditControlsPanel(Context context) {
        super(context);
        init(context);
    }

    public ImageEditControlsPanel(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ImageEditControlsPanel(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);

    }

    public ImageEditControlsPanel(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }
    private void init(Context context) {
        inflate(context, R.layout.scribble_image_edit_panel, this);
        this.brushView     = (ImageView) findViewById(R.id.brush_button);
        this.textView      = (ImageView) findViewById(R.id.text_button);
        this.stickerView   = (ImageView) findViewById(R.id.sticker_button);
        this.undoView = (ImageView) findViewById(R.id.undoView);
        this.cropView= (ImageView) findViewById(R.id.cropView);
        this.deleteView=(ImageView) findViewById(R.id.deleteView);
        this.separatorView=findViewById(R.id.separatorView);
        this.saveView=findViewById(R.id.saveView);

        this.brushView.setOnClickListener(this);
        this.textView.setOnClickListener(this);
        this.stickerView.setOnClickListener(this);
        this.undoView.setOnClickListener(this);
        this.cropView.setOnClickListener(this);
        this.deleteView.setOnClickListener(this);
        this.saveView.setOnClickListener(this);

        //this.foregroundUnselectedTint = getResources().getColor(R.color.transparent);
        this.background               = getResources().getDrawable(R.drawable.circle_tintable);
    }
    Drawable background;
    public Selected selected  = Selected.NONE;
    private enum Selected {
        NONE,
        STICKER,
        TEXT,
        BRUSH
    }
    @Nullable
    private ScribbleToolbarListener listener;
    public void setListener(@Nullable ScribbleToolbarListener listener) {
        this.listener = listener;
    }


    public interface ScribbleToolbarListener {
        public void onBrushSelected(boolean enabled);
        public void onPaintUndo();
        public void onTextSelected(boolean enabled);
        public void onStickerSelected(boolean enabled);
        public void onDeleteSelected();
        public void onSave();
        public void onCropSelected();
    }
    @Override
    public void onClick(View v) {
        //this.toolsView.getLayoutTransition().enableTransitionType(LayoutTransition.CHANGING);

        if (v == this.brushView) {
            boolean enabled = selected != Selected.BRUSH;
            setBrushSelected(enabled);
            if (listener != null) listener.onBrushSelected(enabled);
        } else if (v == this.stickerView) {
            setNoneSelected();
            if (listener != null) listener.onStickerSelected(true);
        } else if (v == this.textView) {
            boolean enabled = selected != Selected.TEXT;
            setTextSelected(enabled);
            if (listener != null) listener.onTextSelected(enabled);
        } else if (v == this.deleteView) {
            setNoneSelected();
            if (listener != null) listener.onDeleteSelected();
        } else if (v == this.undoView) {
            if (listener != null) listener.onPaintUndo();
        }else if (v == this.cropView) {
            setNoneSelected();
            if (listener != null) listener.onCropSelected();
        }
         else if (v == this.saveView) {
            if (listener != null) listener.onSave();
        }
    }
    public void setToolColor(int toolColor) {
        this.toolColor = toolColor;
        this.background.setColorFilter(new PorterDuffColorFilter(toolColor, PorterDuff.Mode.MULTIPLY));
    }
    public int getToolColor() {
        return this.toolColor;
    }

    //int toolColor;
    private int toolColor;
    //private int foregroundUnselectedTint;
    public void setStickerSelected(boolean enabled) {
        if (enabled) {
            this.brushView.setBackground(null);
            //this.brushView.setColorFilter(new PorterDuffColorFilter(foregroundUnselectedTint, PorterDuff.Mode.MULTIPLY));

            this.textView.setBackground(null);
            //this.textView.setColorFilter(new PorterDuffColorFilter(foregroundUnselectedTint, PorterDuff.Mode.MULTIPLY));

            this.separatorView.setVisibility(View.VISIBLE);
            this.undoView.setVisibility(View.GONE);
            this.deleteView.setVisibility(View.VISIBLE);
            this.selected = Selected.STICKER;
        } else {
            this.separatorView.setVisibility(View.GONE);
            this.deleteView.setVisibility(View.GONE);

            this.selected = Selected.NONE;
        }
    }

    private void setBrushSelected(boolean enabled) {
        if (enabled) {

            this.textView.setBackground(null);
            //this.textView.getBackground().setColorFilter(new PorterDuffColorFilter(foregroundUnselectedTint, PorterDuff.Mode.MULTIPLY));

            this.brushView.setBackground(background);
            //this.brushView.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY);

            //this.stickerView.setBackground(null);
            //this.stickerView.getBackground().setColorFilter(new PorterDuffColorFilter(foregroundUnselectedTint, PorterDuff.Mode.MULTIPLY));

            this.separatorView.setVisibility(View.VISIBLE);
            this.undoView.setVisibility(View.VISIBLE);
            this.deleteView.setVisibility(View.GONE);

            this.selected = Selected.BRUSH;
        } else {
            this.brushView.setBackground(null);
            //this.brushView.setColorFilter(new PorterDuffColorFilter(foregroundUnselectedTint, PorterDuff.Mode.MULTIPLY));
            this.separatorView.setVisibility(View.GONE);
            this.undoView.setVisibility(View.GONE);

            this.selected = Selected.NONE;
        }
    }
    public void setTextSelected(boolean enabled) {
        if (enabled) {
            this.brushView.setBackground(null);
            //this.brushView.setColorFilter(new PorterDuffColorFilter(foregroundUnselectedTint, PorterDuff.Mode.MULTIPLY));

            this.textView.setBackground(background);
            // this.textView.setColorFilter(new PorterDuffColorFilter(foregroundSelectedTint, PorterDuff.Mode.MULTIPLY));

            //this.stickerView.setBackground(null);
            //this.stickerView.setColorFilter(new PorterDuffColorFilter(foregroundUnselectedTint, PorterDuff.Mode.MULTIPLY));

            this.separatorView.setVisibility(View.VISIBLE);
            this.undoView.setVisibility(View.GONE);
            this.deleteView.setVisibility(View.VISIBLE);

            this.selected = Selected.TEXT;
        } else {
            this.textView.setBackground(null);
            //this.textView.setColorFilter(new PorterDuffColorFilter(foregroundUnselectedTint, PorterDuff.Mode.MULTIPLY));

            this.separatorView.setVisibility(View.GONE);
            this.deleteView.setVisibility(View.GONE);

            this.selected = Selected.NONE;
        }
    }
    public void setNoneSelected() {
        setBrushSelected(false);
        setStickerSelected(false);
        setTextSelected(false);

        this.selected = Selected.NONE;
    }

}
