package com.hash.around.Views;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.os.Handler;
import android.os.Looper;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.storage.StorageReference;
import com.hash.around.OtherClasses.DataTypes.DownloadUploadData;
import com.hash.around.OtherClasses.DataTypes.GroupChatData;
import com.hash.around.OtherClasses.DataTypes.GroupFeedData;
import com.hash.around.OtherClasses.Interfaces.ChatSentListner;
import com.hash.around.OtherClasses.Interfaces.FeedSentListner;
import com.hash.around.OtherClasses.Interfaces.FileUploadListner;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.R;
import com.hash.around.Utils.ChatSender;
import com.hash.around.Utils.FeedSender;
import com.hash.around.Utils.MediaManager;
import com.hash.around.Utils.StringUtils;
import com.hash.around.Views.CustomMakers.AroundDownloadView;
import com.hash.around.Views.Medias.CircularRelativeLayout;
import com.hash.around.Views.Medias.SingleViewDownloadHandler;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.vanniktech.emoji.EmojiTextView;

//import org.thoughtcrime.securesms.R;

public class FeedRailItemView extends CircularRelativeLayout {


  private final boolean squareHeight=true;

  ImageView imgView;
  EmojiTextView textView;
  RelativeLayout bodyView;
  LinearLayout audioView;
  AroundDownloadView downloadView;


  public FeedRailItemView(Context context) {
    this(context, null);
  }


  public FeedRailItemView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }


  public FeedRailItemView(Context context, AttributeSet attrs, int defStyleAttr) {

    super(context, attrs, defStyleAttr);
    Log.e("GGG","initial");

  }

  @Override
  protected void onFinishInflate() {
    super.onFinishInflate();

    initViews();
  }
  void setforRounderCorner(int radius)
  {
    //yyyy
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      setClipToOutline(true);
    }
    setBackground(null);
    olderVersion=true;
    this.dimen=radius;
    invalidate();

  }
  boolean olderVersion=false;

  void initViews()
  {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

      setClipToOutline(true);
      olderVersion=false;
    }
    else
    {
      olderVersion=true;
     // setOutlineProvider();
    }



    imgView=findViewById(R.id.imgView);
    textView=findViewById(R.id.textView);
    bodyView=findViewById(R.id.bodyView);
    audioView=findViewById(R.id.audioView);
    downloadView=findViewById(R.id.downloadView);
  }

  public void setLocallyAvailable(boolean locallyAvailable)
  {
    if(locallyAvailable)
    {
      downloadView.setVisibility(GONE);
    }
    else
    {
      downloadView.setVisibility(VISIBLE);
    }
  }
  public void setOnFeedUploadListener(FeedSentListner job) {
    this.uploadListner = job;
  }
  FeedSentListner uploadListner = null;
  public void setOnChatUploadListener(ChatSentListner job) {
    this.chatuploadListner = job;
  }
  ChatSentListner chatuploadListner = null;
  public void setOnJobCompletionListner(JobCompletionListner job) {
    this.jobComplete = job;
  }
  JobCompletionListner jobComplete;

  public void setUploadClickListner(String uri, final GroupFeedData data, final long mediaRow, final StorageReference ref) {
    //enableDownloadClick();
    downloadView.setIsUpload();
    final String url = uri;
    downloadView.setClickable(false);

    SingleViewDownloadHandler.setUploadFeedClickListner(getContext(),uri,1,data,downloadView,uploadListner);

  }

  public void setUploadClickListner(String uri, final GroupChatData data,final long mediaRow,final StorageReference ref) {
    //enableDownloadClick();
    downloadView.setIsUpload();
    final String url = uri;
    SingleViewDownloadHandler.setUploadChatClickListner(getContext(),uri,0,data,downloadView,ref,chatuploadListner);
  }

  public void setDownloadClickListner(String uri,int mediaType) {
   // enableDownloadClick();
    downloadView.setIsDownload();
    final String url = uri;

    SingleViewDownloadHandler.setDownloadClickListner(getContext(), uri, mediaType, 0, downloadView, jobComplete);
  }

  void setOnClickListner(OnClickListener listner)
  {
    bodyView.setOnClickListener(listner);
  }
  void setText(String tx)
  {
    textView.setVisibility(VISIBLE);
    textView.setText(StringUtils.makeHashTagText(getContext(),tx,true));
    imgView.setVisibility(GONE);
    audioView.setVisibility(GONE);
  }

  void setImgResource(Uri uri)
  {
    imgView.setVisibility(VISIBLE);
    imgView.setImageURI(uri);
    textView.setVisibility(GONE);
    audioView.setVisibility(GONE);
  }
  void setAudio(Uri uri)
  {
    audioView.setVisibility(VISIBLE);
    textView.setVisibility(GONE);
    imgView.setVisibility(GONE);

    ((TextView)findViewById(R.id.timeIndication)).setText(
            DateUtils.formatElapsedTime(
                    MediaManager.getMediaDuration(getContext(),uri.toString())/1000));

  }
  void hideImage()
  {
    imgView.setVisibility(GONE);

  }
  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    //noinspection SuspiciousNameCombination
    if (squareHeight) super.onMeasure(heightMeasureSpec, heightMeasureSpec);
    else              super.onMeasure(widthMeasureSpec, widthMeasureSpec);


  }

  /*@Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);
    BitmapShader shader;
    shader = new BitmapShader(BitmapFactory.decodeResource(getContext().getResources(),R.drawable.danial), Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);

    Paint paint = new Paint();
    paint.setAntiAlias(true);
    paint.setShader(shader);

    RectF rect = new RectF(0.0f, 0.0f, canvas.getWidth(), canvas.getHeight());

// rect contains the bounds of the shape
// radius is the radius in pixels of the rounded corners
// paint contains the shader that will texture the shape
    canvas.drawRoundRect(rect, 45, 45, paint);
  }*/

   /*private RectF rectF;
  private Path path = new Path();
  private float cornerRadius = 75;

  @Override
  protected void onSizeChanged(int w, int h, int oldw, int oldh) {
    super.onSizeChanged(w, h, oldw, oldh);
    rectF = new RectF(0, 0, w, h);
    resetPath();
  }

  @Override
  public void draw(Canvas canvas) {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {

    int save = canvas.save();
    canvas.clipPath(path);
    super.draw(canvas);
    canvas.restoreToCount(save);}
    else
      super.draw(canvas);
  }

  @Override
  protected void dispatchDraw(Canvas canvas) {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
      int save = canvas.save();
      canvas.clipPath(path);
      super.dispatchDraw(canvas);
      canvas.restoreToCount(save);
    }else
      super.dispatchDraw(canvas);
  }

  private void resetPath() {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
      path.reset();
      path.addRoundRect(rectF, cornerRadius, cornerRadius, Path.Direction.CW);
      path.close();
    }

  }*/



  private final RectF drawRect = new RectF();
  private final Path clipPath = new Path();
  int dimen=45;

  @Override
  protected void onSizeChanged(int w, int h, int oldw, int oldh) {
    super.onSizeChanged(w, h, oldw, oldh);
    if (olderVersion) {

    drawRect.left = 0;
    drawRect.top = 0;
    drawRect.right = getWidth();
    drawRect.bottom = getHeight();
    clipPath.reset();
    clipPath.addRoundRect(drawRect, dimen, dimen, Path.Direction.CCW);
    clipPath.close();}
  }

  @Override
  protected void dispatchDraw(Canvas canvas) {
    if (olderVersion){
      int save = canvas.save();
      /* Paint paint = new Paint();
      paint.setStrokeWidth(2);
      paint.setAntiAlias(true);
      paint.setStyle(Paint.Style.FILL);
      paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
      canvas.drawPath(clipPath,paint);*/
      canvas.clipPath(clipPath);
      super.dispatchDraw(canvas);
      canvas.restoreToCount(save);
    }
    else
      super.dispatchDraw(canvas);
  }
}
