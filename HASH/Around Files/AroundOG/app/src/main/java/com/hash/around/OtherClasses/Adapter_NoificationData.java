package com.hash.around.OtherClasses;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.SwipeDismissBehavior;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.hash.around.ChatWindow;
import com.hash.around.OtherClasses.DataTypes.NotificationData;
import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.OtherClasses.Interfaces.JobCompletionWithFailureListner;
import com.hash.around.Profile_Activity;
import com.hash.around.R;
import com.hash.around.TabFragments.FeedFullScreen;
import com.hash.around.TabbedActivity;
import com.hash.around.Utils.DatabaseManager;
import com.hash.around.Views.Medias.AroundCircularImageView;

import java.util.ArrayList;

import static com.hash.around.MainServiceThread.updateUserChatStatus;
import static com.hash.around.MainServiceThread.updateUserFollowStatus;
import static com.hash.around.Profile_Activity.sentCustomNotificationToUser;
import static com.hash.around.Profile_Activity.newUserPoke;
//import static com.hash.around.Profile_Activity.updateNotificationstatus;


public class Adapter_NoificationData extends RecyclerView.Adapter implements View.OnCreateContextMenuListener{
    Context mContext;
    ArrayList<NotificationData> data;
    boolean isRequests;
    public Adapter_NoificationData(Context mcont,ArrayList<NotificationData> data1,boolean isRequests) {
        mContext = mcont;
        data=data1;
        this.isRequests=isRequests;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v=null;

        switch (viewType)
        {
            case 0:

                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.single_notification, parent, false);
                // set the view's size, margins, paddings and layout parameters

                Adapter_NoificationData.NotificationView vh = new Adapter_NoificationData.NotificationView(v);
                return vh;

            case 1:

                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.single_request_notification, parent, false);
                // set the view's size, margins, paddings and layout parameters

                Adapter_NoificationData.NotificationView vh1 = new Adapter_NoificationData.NotificationView(v);
                return vh1;

        }
        return  null;

    }
    public static void updateNotificationStatus(NotificationData data)
    {
        SQLiteDatabase db= TabbedActivity.db.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseAccesser.notification_status, 1);
        int rowln=db.update(DatabaseAccesser.notification_table,values,
                "rowid"+" =? ",new String[]{data.rowId+""});
    }
    public static int updateNotificationSeenstatus(Context context,long rowId)
    {
        SQLiteDatabase db= DatabaseManager.getDatabase(context).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseAccesser.notification_seen_status, 3);
        int rowln=db.update(DatabaseAccesser.notification_table,values,
                "rowid"+" =? ",new String[]{rowId+""});
        return  rowln;
    }
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        if(!isRequests)
            implementSwipeDismiss(((NotificationView)holder).wholeView,SwipeDismissBehavior.SWIPE_DIRECTION_START_TO_END);
        else
            implementSwipeDismiss(((NotificationView)holder).wholeView,SwipeDismissBehavior.SWIPE_DIRECTION_END_TO_START);

        holder.itemView.findViewById(R.id.wholeView).setVisibility(View.VISIBLE);
        holder.itemView.findViewById(R.id.wholeView).clearAnimation();
        holder.itemView.findViewById(R.id.wholeView).setTranslationX(0);
       // ViewCompat.setScaleX( holder.itemView,1);
       // ViewCompat.setScaleY( holder.itemView,1);
        if(data.get(position).seen_status==2)
            holder.itemView.findViewById(R.id.notification_foreground).setBackgroundColor( ContextCompat.getColor(mContext, R.color.notification_unseeen));
        else
           holder.itemView.findViewById(R.id.wholeView).setBackgroundColor( ContextCompat.getColor(mContext, R.color.white));
            //holder.itemView.findViewById(R.id.notification_foreground).getBackground().setColorFilter(mContext.getResources().getColor(R.color.red_500), PorterDuff.Mode.SRC_IN);

        ((TextView)holder.itemView.findViewById(R.id.notificationDate)).setText(ChatWindow.convertTimeStampToText(mContext,data.get(position).timestamp,true));

        UserWholeData targetUser=ChatWindow.returnSingleUserDetails(mContext,data.get(position).userid);
        if (targetUser!=null) {

            if(data.get(position).type==1 ||data.get(position).type==3 ||
                    data.get(position).type==4||data.get(position).type== 11 ||data.get(position).type==12) {
                if (Adapter_Contacts.checkIfPrivacyShouldBeActivie(targetUser.priv_profile_picture, targetUser.remoteContact) && !targetUser.img_url.equals("null")) {
                    ((AroundCircularImageView) holder.itemView.findViewById(R.id.notification_image)).setImageURI(Uri.parse(targetUser.img_url));
                } else //if (data.get(position).type == 1) {
                    ((AroundCircularImageView) holder.itemView.findViewById(R.id.notification_image)).setImageResource(R.drawable.default_user);
               // }
                //else
                   // ;//((AroundCircularImageView) holder.itemView.findViewById(R.id.notification_image)).setVisibility(View.GONE);
            }
            else
                ((AroundCircularImageView) holder.itemView.findViewById(R.id.notification_image)).setVisibility(View.GONE);

            if(data.get(position).type==1 ||data.get(position).type==3 ||
                    data.get(position).type==4||data.get(position).type== 11 ||data.get(position).type==12) {
                ( holder.itemView.findViewById(R.id.notification_foreground)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.e("GGG", "notificationtext clickclik");
                        Intent indi = new Intent(mContext, Profile_Activity.class);
                        indi.putExtra("userid", data.get(position).userid);
                        mContext.startActivity(indi);
                    }
                });
            }
            else if(data.get(position).type==2)
            {
                UserWholeData currentUser=ChatWindow.returnSingleUserDetails(mContext, FirebaseAuth.getInstance().getUid());
                Intent indi=new Intent(mContext,FeedFullScreen.class);
                indi.putExtra("feedID",data.get(position).link);
                indi.putExtra("username",currentUser.user_name);
                indi.putExtra("userid",currentUser.user_id);
                mContext.startActivity(indi);
            }
        }
        else
        {
            ((AroundCircularImageView) holder.itemView.findViewById(R.id.notification_image)).setVisibility(View.GONE);
        }

        switch (data.get(position).type)
        {
            case 0:
                ((NotificationView)holder).notificationText.setText(data.get(position).text);
                ((NotificationView)holder).actionButton.setVisibility(View.GONE);

                break;
            case 1:
                if(targetUser!=null)
                    ((NotificationView)holder).notificationText.setText(mContext.getString(R.string.poked_you,targetUser.user_name));
                else
                    ((NotificationView)holder).notificationText.setText(mContext.getString(R.string.poked_you,data.get(position).username));

                if(data.get(position).status==0) {
                    ((NotificationView) holder).actionButton.setText(R.string.poke);
                    ((NotificationView) holder).actionButton.setVisibility(View.VISIBLE);
                    ((NotificationView) holder).actionButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            newUserPoke(mContext, data.get(position).userid, new JobCompletionWithFailureListner() {
                                @Override
                                public void onJobFailed(String txt1) {
                                    data.get(position).status = 1;
                                    updateNotificationStatus(data.get(position));
                                    notifyItemChanged(position);
                                }

                                @Override
                                public void onJobCompletion(String txt1) {

                                }
                            });

                        }
                    });

                }
                else
                {
                    ((NotificationView) holder).actionButton.setText(R.string.poked);
                    ((NotificationView) holder).actionButton.setVisibility(View.VISIBLE);
                }

                /*if (targetUser!=null) {

                }
                else {
                    ((NotificationView) holder).notification_image.setImageResource(R.drawable.default_user);
                }*/

                break;
            case 2:
                if(targetUser!=null)
                    ((NotificationView)holder).notificationText.setText(mContext.getString(R.string.shared_your_feed,targetUser.user_name));
                else
                    ((NotificationView)holder).notificationText.setText(mContext.getString(R.string.shared_your_feed,data.get(position).username));

                //((NotificationView)holder).notificationText.setText(data.get(position).text);
                //((NotificationView)holder).notification_image.setImageResource(R.drawable.nick);
                ((NotificationView)holder).actionButton.setVisibility(View.GONE);
               // ((NotificationView)holder).itemView.set

                break;

            case 3:

                if(targetUser!=null)
                    ((NotificationView)holder).notificationText.setText(mContext.getString(R.string.accepted_your_follow_request,targetUser.user_name));
                else
                    ((NotificationView)holder).notificationText.setText(mContext.getString(R.string.accepted_your_follow_request,data.get(position).username));


                //((NotificationView)holder).notificationText.setText(data.get(position).text);
                //((NotificationView)holder).notification_image.setImageResource(R.drawable.nick);
                //((NotificationView)holder).notificationText.setText("Amal Antoney Accepted You Follow Request");
                ((NotificationView)holder).actionButton.setVisibility(View.GONE);
                // ((NotificationView)holder).itemView.set

                break;
            case 4:
                if(targetUser!=null)
                    ((NotificationView)holder).notificationText.setText(mContext.getString(R.string.accepted_your_chat_request,targetUser.user_name));
                else
                    ((NotificationView)holder).notificationText.setText(mContext.getString(R.string.accepted_your_chat_request,data.get(position).username));

                //((NotificationView)holder).notificationText.setText(data.get(position).text);
                //((NotificationView)holder).notification_image.setImageResource(R.drawable.nick);
                //((NotificationView)holder).notificationText.setText("Amal Antoney Accepted your chat Request");
                ((NotificationView)holder).actionButton.setVisibility(View.GONE);
                // ((NotificationView)holder).itemView.set

                break;

            case 11:
                if(targetUser!=null)
                    ((NotificationView)holder).notificationText.setText(mContext.getString(R.string.wants_to_follow_you,targetUser.user_name));
                else
                    ((NotificationView)holder).notificationText.setText(mContext.getString(R.string.wants_to_follow_you,data.get(position).username));

                //((NotificationView1)holder).notificationText.setText(data.get(position).text);
                //((NotificationView1)holder).notification_image.setImageResource(R.drawable.nick);
                //((NotificationView1)holder).notificationText.setText("Amal made a follow request");
                ((NotificationView)holder).actionButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {

                        sentCustomNotificationToUser(mContext,data.get(position).userid, 3, "", new JobCompletionListner() {
                                @Override
                                public void onJobCompletion(String txt1) {
                                    //((NotificationView1)holder).actionButton.
                                    //(Te)v.findViewById(R.id.actionButton)
                                    //updateUserFollowStatus(data.get(position).userid,2);
                                    removeSingleNotificationFromDatabase(data.get(position).rowId);
                                }
                            });
                    }
                });
                ((NotificationView)holder).actionButtonCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        sentCustomNotificationToUser(mContext,data.get(position).userid, 26, "", new JobCompletionListner() {
                                @Override
                                public void onJobCompletion(String txt1) {
                                    //((NotificationView1)holder).actionButton.
                                    //updateUserFollowStatus(data.get(position).userid,5);
                                    removeSingleNotificationFromDatabase(data.get(position).rowId);
                                }
                            });

                    }
                });
                break;

            case 12:
                if(targetUser!=null)
                    ((NotificationView)holder).notificationText.setText(mContext.getString(R.string.wants_to_follow_you,targetUser.user_name));
                else
                    ((NotificationView)holder).notificationText.setText(mContext.getString(R.string.wants_to_follow_you,data.get(position).username));

                // ((NotificationView1)holder).notificationText.setText(data.get(position).text);
                //((NotificationView1)holder).notification_image.setImageResource(R.drawable.nick);
                //((NotificationView1)holder).notificationText.setText("Amal made a chat request");
                ((NotificationView)holder).actionButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                            updateUserChatStatus(mContext,data.get(position).userid,1);
                            sentCustomNotificationToUser(mContext,data.get(position).userid, 4, TabbedActivity.userdetail.getPhoneNumber(), new JobCompletionListner() {
                                @Override
                                public void onJobCompletion(String txt1) {
                                    //((NotificationView1)holder).actionButton.
                                }
                            });

                    }
                });

                ((NotificationView)holder).actionButtonCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                            sentCustomNotificationToUser(mContext,data.get(position).userid, 27, "", new JobCompletionListner() {
                                @Override
                                public void onJobCompletion(String txt1) {
                                    //((NotificationView1)holder).actionButton.
                                    removeSingleNotificationFromDatabase(data.get(position).rowId);
                                }
                            });

                    }
                });
                break;


        }
        if(data.get(position).seen_status==2)
        {
            if(updateNotificationSeenstatus(mContext,data.get(position).rowId)>0)
                data.get(position).seen_status=3;
        }
    }

    static void removeSingleNotificationFromDatabase(long row)
    {
        SQLiteDatabase db=TabbedActivity.db.getWritableDatabase();

        int quantity = db.delete(DatabaseAccesser.notification_table," rowid =?",new String[]{row+""});
        Log.e("MSGG","notification deleted : "+quantity);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(data.get(position).type<10)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

    }

    private void implementSwipeDismiss(View view,int direction) {
       /* SwipeDismissBehavior swipeDismissBehavior = new SwipeDismissBehavior();
        swipeDismissBehavior.setSwipeDirection(direction);//Swipe direction i.e any direction, here you can put any direction LEFT or RIGHT

        CoordinatorLayout.LayoutParams layoutParams =
                (CoordinatorLayout.LayoutParams) view.getLayoutParams();

        swipeDismissBehavior.setListener(new SwipeDismissBehavior.OnDismissListener() {

            @Override
            public void onDismiss(final View view) {
                //dialog.dismiss();
            }

            @Override
            public void onDragStateChanged(int i) {}

        });

        layoutParams.setBehavior(swipeDismissBehavior);*/
    }

    public static class NotificationView extends RecyclerView.ViewHolder {

        public TextView actionButton,actionButtonCancel;
        public AroundCircularImageView notification_image;
        public TextView notificationText;
        public TextView notificationDate;
        CoordinatorLayout wholeView;
        LinearLayout notification_foreground;
        public NotificationView(View item) {
            super(item);

            actionButton=(TextView)item.findViewById(R.id.actionButton);
            notification_image=item.findViewById(R.id.notification_image);
            notificationText=(TextView)item.findViewById(R.id.notificationText);
            notificationDate=(TextView)item.findViewById(R.id.notificationDate);
            wholeView=item.findViewById(R.id.wholeView);
            notification_foreground=item.findViewById(R.id.notification_foreground);
            actionButtonCancel=(TextView)item.findViewById(R.id.actionButtonCancel);


        }
    }
    /*public static class NotificationView1 extends RecyclerView.ViewHolder {

        public TextView actionButton,actionButtonCancel;
        public AroundCircularImageView notification_image;
        public TextView notificationText;
        public TextView notificationDate;
        CoordinatorLayout wholeView;
        public NotificationView1(View item) {
            super(item);



        }
    }*/

}