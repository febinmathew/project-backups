package com.hash.around.TabFragments;

import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.github.clans.fab.FloatingActionButton;
import com.hash.around.MapsActivity;
import com.hash.around.OtherClasses.Adapter_LocationData;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.OtherClasses.DataTypes.LocationData;
import com.hash.around.R;
import com.hash.around.TabbedActivity;

import java.util.ArrayList;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;
import static com.hash.around.TabbedActivity.saveLocationToDatabase;

public class TabFragment3 extends Fragment {

    public RecyclerView rView;
    private LinearLayoutManager mLayoutManager;
    FloatingActionButton floatButton;
    ArrayList<LocationData> finaldata;
    Adapter_LocationData adap;
    LinearLayout noFeedsAvailable;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.location_list_fragment, container, false);
        rView=(RecyclerView)rootView.findViewById(R.id.rView);
        noFeedsAvailable=(LinearLayout) rootView.findViewById(R.id.noFeedsAvailable);
        mLayoutManager=new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,true);
        mLayoutManager.setStackFromEnd(true);
        rView.setLayoutManager(mLayoutManager);
        rView.setNestedScrollingEnabled(false);
        finaldata=new ArrayList<LocationData>();
        getLocationData();
        adap=new Adapter_LocationData(getActivity(),finaldata,false);
        rView.setAdapter(adap);
        //setHasOptionsMenu(true);
        floatButton=(FloatingActionButton)rootView.findViewById(R.id.floatButton);
        floatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mapIntent=new Intent(getActivity().getApplicationContext(),MapsActivity.class);
              //  uploadIntent.putExtra("numberType",5);
                //uploadIntent.putExtra("uri",directory.toString());
                startActivityForResult(mapIntent,20);
            }
        });

        return rootView;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.map_activity_options, menu);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 20 && resultCode == RESULT_OK) {

            LocationData x=new LocationData();
            x.status=1;
            x.location_name=data.getStringExtra("locname");
            x.latitude=data.getDoubleExtra("lat",0);
            x.longitude=data.getDoubleExtra("lng",0);

            addLocationToList(x);

        }
        }


       public void addLocationToList(LocationData locationData)
        {
            locationData.latitude=Double.parseDouble(String.format( Locale.ROOT, "%.8f",locationData.latitude));
            locationData.longitude=Double.parseDouble(String.format(Locale.ROOT,"%.8f",locationData.longitude));
            finaldata.add(locationData);
            saveLocationToDatabase(getActivity(),locationData);
            adap.notifyItemInserted(finaldata.size()-1);
            rView.scrollToPosition(finaldata.size() - 1);
        }


    void getLocationData()
    {
        SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();

        Cursor cursor = db.query(DatabaseAccesser.location_table, new String[]{DatabaseAccesser.loc_col_location_name,
                        DatabaseAccesser.loc_col_status, DatabaseAccesser.loc_col_latitude, DatabaseAccesser.loc_col_longitude},
                null,
                null,
                null, null, null,null);

        while (cursor.moveToNext()) {


            LocationData temp = new LocationData();

            temp.location_name = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.loc_col_location_name));
            temp.status = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.loc_col_status));
            temp.latitude = cursor.getDouble(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.loc_col_latitude));
            temp.longitude = cursor.getDouble(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.loc_col_longitude));

            finaldata.add(temp);
        }
        cursor.close();

        if(finaldata.size()==0)
        {
            noFeedsAvailable.setVisibility(View.VISIBLE);
        }
    }
}


