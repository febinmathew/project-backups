package com.hash.around.Views;


import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

/*import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.MediaStoreSignature;*/

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.hash.around.OtherClasses.DataTypes.MediaData;
import com.hash.around.OtherClasses.Interfaces.JobCompletionWithFailureListner;
import com.hash.around.R;
import com.hash.around.Utils.MediaManager;
import com.hash.around.Utils.StringUtils;

import java.util.ArrayList;
/*import org.thoughtcrime.securesms.database.CursorRecyclerViewAdapter;
import org.thoughtcrime.securesms.database.loaders.RecentPhotosLoader;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.util.ViewUtil;*/

public class RecentPhotoViewRail extends RelativeLayout implements LoaderManager.LoaderCallbacks<Cursor> {

  @NonNull  private final RecyclerView recyclerView;
  @Nullable private       OnItemClickedListener listener;

  public RecentPhotoViewRail(Context context) {
    this(context, null);
  }

  public RecentPhotoViewRail(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public RecentPhotoViewRail(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);

    inflate(context, R.layout.recent_photo_view, this);

    this.recyclerView = findViewById(R.id.photo_list);
    this.recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
    this.recyclerView.setItemAnimator(new DefaultItemAnimator());
  }

  public void setListener(@Nullable OnItemClickedListener listener) {
    this.listener = listener;

    if (this.recyclerView.getAdapter() != null) {
      ((RecentPhotoAdapter)this.recyclerView.getAdapter()).setListener(listener);
    }
  }

  JobCompletionWithFailureListner sendButtonListner;
  public void setSendButtonListener(@Nullable JobCompletionWithFailureListner listener) {
    this.sendButtonListner = listener;

    Log.e("MGG","adapter status "+this.recyclerView.getAdapter());
    if (this.recyclerView.getAdapter() != null) {
      ((RecentPhotoAdapter)this.recyclerView.getAdapter()).setSendButtonListener(listener);
    }
  }

  @Override
  public Loader<Cursor> onCreateLoader(int id, Bundle args) {

    return new RecentPhotosLoader(getContext());
  }

  @Override
  public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

    this.recyclerView.setAdapter(new RecentPhotoAdapter(getContext(), data, RecentPhotosLoader.BASE_URL, listener,sendButtonListner));
  }

  @Override
  public void onLoaderReset(Loader<Cursor> loader) {
    ((CursorRecyclerViewAdapter)this.recyclerView.getAdapter()).changeCursor(null);
  }

  private static class RecentPhotoAdapter extends CursorRecyclerViewAdapter<RecentPhotoAdapter.RecentPhotoViewHolder> {

    @SuppressWarnings("unused")
    private static final String TAG = RecentPhotoAdapter.class.getName();

    @NonNull  private final Uri baseUri;
    @Nullable private OnItemClickedListener clickedListener;

    private RecentPhotoAdapter(@NonNull Context context, @NonNull Cursor cursor, @NonNull Uri baseUri, @Nullable OnItemClickedListener listener,@Nullable JobCompletionWithFailureListner listener2) {
      super(context, cursor);
      this.baseUri         = baseUri;
      this.clickedListener = listener;
      this.sendButtonListner=listener2;
    }

    @Override
    public RecentPhotoViewHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
      View itemView = LayoutInflater.from(parent.getContext())
                                    .inflate(R.layout.recent_photo_view_item, parent, false);

      return new RecentPhotoViewHolder(itemView);
    }

    ArrayList<MediaData> selectedItems=new ArrayList<>();
    String gifMime= MimeTypeMap.getSingleton().getMimeTypeFromExtension("gif");
    @Override
    public void onBindItemViewHolder(final RecentPhotoViewHolder viewHolder, @NonNull Cursor cursor) {
      viewHolder.imageView.setImageDrawable(null);

      long   id           = cursor.getLong(cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns._ID));
      //long   dateTaken    = cursor.getLong(cursor.getColumnIndexOrThrow(MediaStore.Images.ImageColumns.DATE_TAKEN));
      //long   dateModified = cursor.getLong(cursor.getColumnIndexOrThrow(MediaStore.Images.ImageColumns.DATE_MODIFIED));
      String mimeType     = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.ImageColumns.MIME_TYPE));
      int mediaType= Integer.parseInt(cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.MEDIA_TYPE)));
      //int    orientation  = cursor.getInt(cursor.getColumnIndexOrThrow(MediaStore.Images.ImageColumns.ORIENTATION));

      final MediaData mediaData=new MediaData();

      mediaData.uri= Uri.withAppendedPath(baseUri, Long.toString(id)).toString();
      mediaData.type=MediaManager.getMediTypeFromMime(mimeType,mediaType);

      //Log.e("MGG","uri NOW " + uri);
      //Key signature = new MediaStoreSignature(mimeType, dateModified, orientation);

      /*GlideApp.with(getContext().getApplicationContext())
              .load(uri)
              .signature(signature)
              .diskCacheStrategy(DiskCacheStrategy.NONE)
              .into(viewHolder.imageView);*/
      if(selectedItems.contains(mediaData))
        viewHolder.wholeView.setChecked(true);
      else
        viewHolder.wholeView.setChecked(false);

      try {
        mediaData.mediaSize=MediaManager.getMediaSize(getContext(),Uri.parse(mediaData.uri));
      } catch (Exception e) {
        e.printStackTrace();
        mediaData.mediaSize=-1;
      }
      viewHolder.wholeView.setMediaDetails(getContext(),mediaData.type,Uri.parse(mediaData.uri), mediaData.mediaSize);

      viewHolder.wholeView.setJobListner(new JobCompletionWithFailureListner() {
        @Override
        public void onJobFailed(String txt1) {
          viewHolder.wholeView.setChecked(false);
          selectedItems.remove(mediaData);
          updateSendButtonStatus();

          if (clickedListener != null) clickedListener.onItemSelected(selectedItems);

        }

        @Override
        public void onJobCompletion(String txt1) {
          viewHolder.wholeView.setChecked(true);
          selectedItems.add(mediaData);
          updateSendButtonStatus();
          if (clickedListener != null) clickedListener.onItemSelected(selectedItems);

        }
      });
      //Log.e("MGG","gohh NOW " +  mediaData.type);
     /* Glide.with(getContext()).load(Uri.parse(mediaData.uri))
              .thumbnail(0.4f)
              .crossFade()
              .diskCacheStrategy(DiskCacheStrategy.NONE)
              .into(viewHolder.imageView);

        if(mediaData.type==MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO) {
          viewHolder.typeIndicator.setVisibility(View.VISIBLE);
          viewHolder.typeIndicator.setImageResource(R.drawable.ic_video_white);
        }
        else if(mediaData.type==5) {
          viewHolder.typeIndicator.setVisibility(View.VISIBLE);
          viewHolder.typeIndicator.setImageResource(R.drawable.ic_gif_white);
        }
        else
          viewHolder.typeIndicator.setVisibility(View.GONE);*/

      //viewHolder.imageView.setImageURI(uri);
      //viewHolder.imageView.setMaxWidth(viewHolder.imageView.getHeight());
      viewHolder.wholeView.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
          Log.e("MGG","clicking NOW ");
          viewHolder.wholeView.setChecked(true);
          if(!selectedItems.contains(mediaData))
          selectedItems.add(mediaData);
          if (clickedListener != null)
          {
            clickedListener.onItemSelected(selectedItems);
            clickedListener.onItemClicked(mediaData);

          }
        }
      });

    }

    void updateSendButtonStatus()
    {
      if(selectedItems.size()==1)
      {
        if(sendButtonListner!=null)
          sendButtonListner.onJobCompletion("");}
      else if(selectedItems.size()==0)
      { if(sendButtonListner!=null)
        sendButtonListner.onJobFailed("");}
    }
    public void setListener(@Nullable OnItemClickedListener listener) {
      this.clickedListener = listener;
    }
    JobCompletionWithFailureListner sendButtonListner;
    public void setSendButtonListener(JobCompletionWithFailureListner listener) {
      sendButtonListner=listener;
    }

    static class RecentPhotoViewHolder extends RecyclerView.ViewHolder {

      SquareFrameLayout wholeView;
      ImageView imageView;
      ImageView typeIndicator;

      RecentPhotoViewHolder(View itemView) {
        super(itemView);
        wholeView=itemView.findViewById(R.id.wholeView) ;
        this.imageView = itemView.findViewById(R.id.thumbnail);
        this.typeIndicator=itemView.findViewById(R.id.typeIndicator);
      }
    }
  }

  public interface OnItemClickedListener {
    void onItemClicked(MediaData uri);
    void onItemSelected(ArrayList<MediaData> uriList);
  }
}
