package com.hash.around.OtherClasses.DataTypes;

/**
 * Created by Febin on 26-08-2017.
 */
import java.io.Serializable;

public class ChatListData implements  Serializable {
    public String groupID;


    public String userid;
    public String name;
    public int msgFromMe;
    public String lastMessage;
    public int lastMessageSeen;   //NEW ENTRY
    public long rowId;     //NEW ENTRY
    public int lastMessagetype;
    public int type;
    public int chat_pinned;
    public int chat_hidden;
    public long timestamp;
    public int msgQueue;
    public String url;
    public String media_uri;
    public String media_lite_uri;
    public int chatType;
    public int priv_profile_picture=0;
    public Integer remoteContact=null;
}



