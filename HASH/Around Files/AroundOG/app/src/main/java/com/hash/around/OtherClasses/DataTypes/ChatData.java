package com.hash.around.OtherClasses.DataTypes;

import java.io.Serializable;

 public class ChatData implements Serializable{

    public long sl;
    public String userid;
   public int type;
    public long timestamp;
    public String msg;
    public String temp_url;
    public String final_url;
     public String replay_url;
    public Long mediaSize=0L;


     public long chatSchedule=0;
     public long chatExpire=0;

    public ChatData()
    {}
     public ChatData(GroupChatData data)
     {
         this.temp_url=data.temp_url;
         this.final_url=data.final_url;
         this.msg=data.msg;
         this.timestamp=data.timestamp;
         this.type=data.type;
         this.userid=data.userid;
         this.sl=data.sl;
         this.mediaSize=data.mediaSize;
         this.chatSchedule=data.chatSchedule;
         this.chatExpire=data.chatExpire;
         this.replay_url=data.replay_url;


     }
    public ChatData(int sender,int type,String msg,String temp_url,String final_url,int msgseen,long timestamp)
    {

       this.type=type;
       this.msg=msg;
       this.temp_url=temp_url;
       this.final_url=final_url;

       this.timestamp=timestamp;

    }

}
