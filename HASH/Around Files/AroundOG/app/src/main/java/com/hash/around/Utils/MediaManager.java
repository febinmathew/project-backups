package com.hash.around.Utils;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ImageReader;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.TimedText;
import android.media.audiofx.Visualizer;
import android.net.Uri;
import android.os.Environment;
import android.os.Process;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.text.format.DateUtils;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.hash.around.OtherClasses.DataTypes.DownloadData;
import com.hash.around.OtherClasses.DataTypes.DownloadListData;
import com.hash.around.OtherClasses.DataTypes.DownloadUploadData;
import com.hash.around.OtherClasses.DataTypes.DownloadUploadListData;
import com.hash.around.OtherClasses.DataTypes.MediaData;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.OtherClasses.Interfaces.AudioPlayerListener;
import com.hash.around.OtherClasses.Interfaces.FileDownloadListner;
import com.hash.around.OtherClasses.Interfaces.FileUploadListner;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.OtherClasses.Interfaces.JobCompletionWithFailureListner;
import com.hash.around.R;
import com.hash.around.TabbedActivity;
import com.hash.around.Views.Medias.AroundVisualizerView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import pl.droidsonroids.gif.GifImageView;

import static com.hash.around.TabFragments.TabFragment2.getResizedBitmap;
import static com.hash.around.TabbedActivity.mTabbedContext;

/**
 * Created by Febin on 6/13/2018.
 */

public class MediaManager {

    public static String captureImageFromCamera(Context context, Activity act,int code)
    {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Intent takeVideoIntent = new Intent(MediaStore.INTENT_ACTION_STILL_IMAGE_CAMERA);

        if (takePictureIntent.resolveActivity(context.getPackageManager()) != null) {

            File photoFile = null;
            try {
                photoFile = createImageFile(context);
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {

                Uri photoURI = FileProvider.getUriForFile(context,
                        "com.hash.around.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);

                //act.startActivityForResult(takePictureIntent, code);

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                List<ResolveInfo> resolvedIntentActivities = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
                for (ResolveInfo resolvedIntentInfo : resolvedIntentActivities) {
                    String packageName = resolvedIntentInfo.activityInfo.packageName;

                    context.grantUriPermission(packageName, photoURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);

                }
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                act.startActivityForResult(intent, code);



              //  listener.onJobCompletion(photoURI.toString());
                return Uri.fromFile(photoFile).toString();
            }
        }


        return null;
    }
    public static File createImageFile(Context context) throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".png",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        //mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    public static void AIsetPicFaster(ImageView mImageView, String mCurrentPhotoPath) {
        // Get the dimensions of the View
        int targetW = 200;//mImageView.getWidth();
        int targetH = 200;//mImageView.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        mImageView.setImageBitmap(bitmap);
    }

    public static String gifMime= MimeTypeMap.getSingleton().getMimeTypeFromExtension("gif");
    public static int getMediTypeFromMime(String MIME,Integer mediaType)
    {
        if(MIME.equals(gifMime))
            return 5;

        else if(mediaType!=null)
            return mediaType;
        else if(MIME.contains("image"))
            return 1;
        else if(MIME.contains("audio"))
            return 2;
        else if(MIME.contains("video"))
            return 5;
        else
            return -1;
    }

    public static Integer getIconForResourseType(Integer mediaType)
    {
        if(mediaType==MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO) {
            return R.drawable.ic_video_white;
        }
        else if(mediaType==MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE) {
            return R.drawable.ic_photo_camera_white;
        }
        else if(mediaType==MediaStore.Files.FileColumns.MEDIA_TYPE_AUDIO) {
            return R.drawable.ic_music_note_white;
        }
        else if(mediaType==5) {
            return R.drawable.ic_gif_white;
        }
        return null;
    }
    public static long getMediaSize(Context context, Uri uri) throws Exception {

        InputStream in=null;
        long size = 0;
        try {
            /*in = new FileInputStream(new File(uri.getPath()));

            if(in==null)*/
            in = context.getContentResolver().openInputStream(uri);

            if (in == null) throw new IOException("Couldn't obtain input stream.");


            byte[] buffer = new byte[4096];
            int read;

            while ((read = in.read(buffer)) != -1) {
                size += read;
            }
        }
        catch (Exception ex)
        {
            Log.e("GGG","mediasize error 1 : "+ex.getMessage()+"/ "+ex.toString());
        }
        finally {
            if(in!=null)
                in.close();
        }
        in.close();

       // Log.e("GGG","mediasize is "+size);
        //Log.e("MGG","size is : "+size);
        return size;
    }

    public static String getFileExtention(Uri data)
    {
        return MimeTypeMap.getFileExtensionFromUrl(data.toString());
    }
    public static String getFileExtention(String data)
    {
        return MimeTypeMap.getFileExtensionFromUrl(data);


    }
    public static String getExtentionFromMimeType(String data)
    {
        return MimeTypeMap.getSingleton().getExtensionFromMimeType(data);
    }
    public static String getFileName(String data)
    {
        return Uri.parse(data).getLastPathSegment();
    }

    public static void openAnyFile(Context context,String fileUri)
    {
        //String url=Environment.getExternalStorageDirectory().getAbsolutePath()+"/Contacts Generator/"+file_name;
        Log.e("MSG","file uri "+URI.create(fileUri));
        Log.e("MSG","file uri "+fileUri);
        File file = new File(fileUri);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(fileUri));
        intent.setDataAndType(Uri.fromFile(file), mimeType);
        Intent intent1 = Intent.createChooser(intent, "Open With");
        context.startActivity(intent1);
    }


    public static void uploadByteFileAndReturnUrl(Context context, byte[] byteArray, final StorageReference storageRef, final FileUploadListner listner) {
        storageRef.child(StringUtils.randomStringGenerator() + ".bin").putBytes(byteArray).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> taskSnapshot) {
                if (taskSnapshot.isSuccessful()) {
                    String global = taskSnapshot.getResult().getDownloadUrl().toString();
                    //Log.e("MSGG", "locallite is is " + finalLitelocal);

                    listner.onFileUploaded(global, "null", "null");
                } else {
                    // Failed
                }
            }
        });
    }

    public static boolean addDownloadDataIfNotOrRemove(DownloadUploadData downloadData,boolean removal)
    {
        for(int i=0;i<downloadList.size();i++)
        {
            if(downloadList.get(i).uploadUrl.equals(downloadData.uploadUrl))
            {
                if(removal)
                {
                    downloadList.remove(i);
                    return true;
                }
                else
                {
                    downloadList.get(i).listener.add(downloadData.listener);
                    return true;
                }

            }

        }
        DownloadUploadListData ddd=new DownloadUploadListData();
        ddd.listener.add(downloadData.listener);
        ddd.uploadUrl=downloadData.uploadUrl;
        downloadList.add(ddd);
        return false;
    }

    public static DownloadUploadData getDownloadListItemArrayFromUri(String uri)
    {
        for(int i=0;i<finaldownloadList.size();i++)
        {
            if(finaldownloadList.get(i).uploadUrl.equals(uri));
            return finaldownloadList.get(i);
        }
        return null;
    }
    static ArrayList<DownloadUploadListData> downloadList=new ArrayList<>();
    final static  ArrayList<DownloadUploadData> finaldownloadList=new ArrayList<>();
    public static void removeDownload(DownloadUploadData downloadData)
    {
        for(int i=finaldownloadList.size()-1;i>=0;i--) {
            // Log.e("GGG","\n downloadme checking equality "+"\n"+downloadList.get(i).downloadUrl+" \n "+downloadData.downloadUrl+"\n"+(downloadList.get(i).downloadUrl.equals(downloadData.downloadUrl))+"\n");
            if ((finaldownloadList.get(i).uploadUrl.equals(downloadData.uploadUrl))) {
                finaldownloadList.remove(i);
            }
        }
    }
    public static boolean addDownload(DownloadUploadData downloadData)
    {
        for(int i=finaldownloadList.size()-1;i>=0;i--)
        {
            // Log.e("GGG","\n downloadme checking equality "+"\n"+downloadList.get(i).downloadUrl+" \n "+downloadData.downloadUrl+"\n"+(downloadList.get(i).downloadUrl.equals(downloadData.downloadUrl))+"\n");
            if((finaldownloadList.get(i).uploadUrl.equals(downloadData.uploadUrl)))
            {

                 Log.e("GGG","downloadme adding new listener "+finaldownloadList.get(i).uploadUrl+"\n"+downloadData.uploadUrl);
                //downloadList.get(i).listener.add(downloadData.listener);
                //Log.e("GGG","downloadme adding NOT unique complete "+downloadData.downloadUrl);
                finaldownloadList.add(downloadData);
                return true;


            }

        }
        Log.e("GGG","downloadme NEW URL "+downloadData.uploadUrl);
       /*DownloadListData ddd=new DownloadListData();
       ddd.listener.add(downloadData.listener);
       ddd.downloadUrl=downloadData.downloadUrl;*/

        finaldownloadList.add(downloadData);
        return false;
    }

    public static String getLiteLocalMediaFromOrginalMedia(Context context,String uri,int type)
    {
        // boolean bitmapcreated;
        Bitmap bitmap = null;
        Bitmap videoBitmap = null;
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        final byte[] byteArray;

        String lite_localUri=uri;

        if (type == 1) {
            try {
                bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), Uri.parse(uri));
                // bitmapcreated = true;
            } catch (IOException e) {
                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                bmOptions.inSampleSize = 3;
                bitmap = BitmapFactory.decodeFile(uri, bmOptions);
                //bitmapcreated = false;
            }
            Log.e("GGG",bitmap+" : bitmap");
            getResizedBitmap(bitmap, 120).compress(Bitmap.CompressFormat.PNG, 1, bytes);
            byteArray = bytes.toByteArray();
            lite_localUri=saveByteIntoCacheAsFile(context,byteArray,StringUtils.randomStringGenerator() + ".png");



        }
        else if (type == 3) {


            MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();

            mediaMetadataRetriever.setDataSource(context, Uri.parse(uri));

            bitmap = mediaMetadataRetriever.getFrameAtTime(10, MediaMetadataRetriever.OPTION_NEXT_SYNC);


            getResizedBitmap(bitmap, 1000).compress(Bitmap.CompressFormat.PNG, 1, bytes);
            byteArray = bytes.toByteArray();
            lite_localUri=saveByteIntoCacheAsFile(context,byteArray,StringUtils.randomStringGenerator() + ".png");


        }
        else if(type==5)
        {
            GifImageView ffff=new GifImageView(context);
            ffff.setImageURI(Uri.parse(uri));

            Drawable xxx=ffff.getDrawable();
            if(xxx.getIntrinsicWidth() <= 0 || xxx.getIntrinsicHeight() <= 0) {
                bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
            } else {
                bitmap = Bitmap.createBitmap(xxx.getIntrinsicWidth(), xxx.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);

            }
            Canvas canvas = new Canvas(bitmap);
            xxx.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            xxx.draw(canvas);


            getResizedBitmap(bitmap, 500).compress(Bitmap.CompressFormat.PNG, 1, bytes);

            byteArray = bytes.toByteArray();
            lite_localUri=saveByteIntoCacheAsFile(context,byteArray,StringUtils.randomStringGenerator() + ".png");


        }


        return lite_localUri;

    }


    public static void uploadResourceAndReturnUrls(Context context,String lite_localUri, final int type,final  String uri, final StorageReference storageRef, final FileUploadListner listner) {
        boolean bitmapcreated;
        Bitmap bitmap = null;
        Bitmap videoBitmap = null;


        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        final byte[] byteArray;

      // String lite_localUri=null;

            final DownloadUploadData dat=new DownloadUploadData();
            dat.listener=listner;
            dat.uploadUrl=uri;
            if(addDownload(dat))
                return;

        Log.e("GGG","downloadme1 downloadinf REAL");
        String name=null,name2=null;

        //if(lite_localUri==null){
        if (type == 1) {
            /*try {
                bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), Uri.parse(uri));
                // bitmapcreated = true;
            } catch (IOException e) {
                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                bmOptions.inSampleSize = 3;
                bitmap = BitmapFactory.decodeFile(uri, bmOptions);
                //bitmapcreated = false;
            }
            Log.e("GGG",bitmap+" : bitmap");
            getResizedBitmap(bitmap, 120).compress(Bitmap.CompressFormat.PNG, 1, bytes);
            name = StringUtils.randomStringGenerator() + ".png";
            byteArray = bytes.toByteArray();*/
            name = StringUtils.randomStringGenerator() + ".png";
            //lite_localUri=saveByteIntoCacheAsFile(byteArray,StringUtils.randomStringGenerator() + ".png");
        }
        else if (type == 3) {


            /*MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();

            mediaMetadataRetriever.setDataSource(context, Uri.parse(uri));

            bitmap = mediaMetadataRetriever.getFrameAtTime(10, MediaMetadataRetriever.OPTION_NEXT_SYNC);


            getResizedBitmap(bitmap, 1000).compress(Bitmap.CompressFormat.PNG, 1, bytes);
            byteArray = bytes.toByteArray();
            lite_localUri=saveByteIntoCacheAsFile(byteArray,StringUtils.randomStringGenerator() + ".png");*/
            name = StringUtils.randomStringGenerator() + (type==3?".mp4":".gif");

        }
        else if(type==5)
        {
            /*GifImageView ffff=new GifImageView(context);
            ffff.setImageURI(Uri.parse(uri));

            Drawable xxx=ffff.getDrawable();
            if(xxx.getIntrinsicWidth() <= 0 || xxx.getIntrinsicHeight() <= 0) {
                bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
            } else {
                bitmap = Bitmap.createBitmap(xxx.getIntrinsicWidth(), xxx.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);

            }
            Canvas canvas = new Canvas(bitmap);
            xxx.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            xxx.draw(canvas);


            getResizedBitmap(bitmap, 500).compress(Bitmap.CompressFormat.PNG, 1, bytes);

            byteArray = bytes.toByteArray();
            lite_localUri=saveByteIntoCacheAsFile(byteArray,StringUtils.randomStringGenerator() + ".png");*/
            name = StringUtils.randomStringGenerator() + ".gif";

        }




        else if (type==2)
        {
            byteArray=null;
            name = StringUtils.randomStringGenerator() + "."+MediaManager.getFileExtention(uri);

        }
        else if(type==6)
        {
            byteArray=null;
            name = StringUtils.randomStringGenerator() + "."+MediaManager.getFileExtention(uri);
            Log.e("MGG","file name is "+name);
        }
        else
        {
            byteArray=null;
            name = StringUtils.randomStringGenerator() + ".png";
        }



        final String finalLitelocal=lite_localUri;

        Log.e("GGG","chatimage finalLitelocal "+ finalLitelocal);

        Log.e("MGG","audio name "+name+" /\n "+Uri.parse(uri));
        storageRef.child(name).putFile(Uri.parse(uri)).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                Log.e("GGG","uploadtask complete");
                if (task.isSuccessful()) {

                    final String global = task.getResult().getDownloadUrl().toString();

                   // Log.e("MSGG","global is "+global);
                    Log.e("MGG","audio uploadcomplete ");
                    if(type==1||type==3||type==5) {
                        storageRef.child(StringUtils.randomStringGenerator() + ".png").putFile(Uri.parse(finalLitelocal))/*.putBytes(byteArray)*/.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> taskSnapshot) {
                                if (taskSnapshot.isSuccessful()) {
                                    String global_lite = taskSnapshot.getResult().getDownloadUrl().toString();
                                    Log.e("MSGG", "locallite is is " + finalLitelocal);

                                    //listner.onFileUploaded(global, global_lite, finalLitelocal);


                                   // DownloadUploadListData currentItem=getDownloadListItemArrayFromUri(uri);

                                    /*for(FileUploadListner list:currentItem.listener)
                                        if(list!=null)list.onFileUploaded(global, global_lite, finalLitelocal);*/
                                    for(int i=0;i<finaldownloadList.size();i++)
                                    {
                                        if(finaldownloadList.get(i).uploadUrl.equals(dat.uploadUrl))
                                        {
                                            Log.e("GGG","chatimage finalLitelocal2 "+ finalLitelocal);
                                           // Log.e("GGG","downloadme OUTGOING "+url+"\n"+Uri.fromFile(temp).toString());
                                            finaldownloadList.get(i).listener.onFileUploaded(global, global_lite, finalLitelocal);
                                        }
                                    }
                                    Log.e("GGG","downloadme size is  "+finaldownloadList.size());
                                    removeDownload(dat);
                                    Log.e("GGG","downloadme size is  "+finaldownloadList.size());

                                    //addDownloadDataIfNotOrRemove(dat,true);
                                } else {
                                    // Failed
                                }
                            }
                        });
                    }
                    else
                    {
                        Log.e("MGG","audio calling next step ");
                        listner.onFileUploaded(global, "null"+StringUtils.randomStringGenerator(), finalLitelocal);
                    }



                }
                else
                {
                    Log.e("MGG","audio failed upload^^ "+task.toString());
                }

            }}).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                    double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                    Log.e("GGG","uploadtask progress "+progress);
                    //float percent = ((float) taskSnapshot.getBytesTransferred() / (float) taskSnapshot.getTotalByteCount()) * 100;
                    /*DownloadUploadListData currentItem=getDownloadListItemArrayFromUri(uri);
                    for(FileUploadListner list:currentItem.listener)
                        if(list!=null) list.onProgressListener((int) progress);*/
                    for(int i=0;i<finaldownloadList.size();i++)
                    {
                        if(finaldownloadList.get(i).uploadUrl.equals(dat.uploadUrl))
                        {
                            // Log.e("GGG","downloadme OUTGOING "+url+"\n"+Uri.fromFile(temp).toString());
                            finaldownloadList.get(i).listener.onProgressListener((int) progress);
                            finaldownloadList.get(i).progress=(int) progress;
                        }
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e("GGG","uploadtask failed");
                    /*DownloadUploadListData currentItem=getDownloadListItemArrayFromUri(uri);
                    for(FileUploadListner list:currentItem.listener)
                        if(list!=null) list.onFailureListener((int) 0);*/
                }
            });

    }

    public static String saveByteIntoCacheAsFile(Context context,byte[] data,String name)
    {
        File file = new File(/*Environment.getExternalStorageDirectory().getPath(), "Around Files/Media/cache"*/
        AroundStaticInfo.getDefaultCacheDirectory(context));
        if (!file.exists()) {
            file.mkdirs();
        }
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(new File(file.getAbsolutePath(), name));
            fos.write(data);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String path=Uri.fromFile(new File(file,name)).toString();
        Log.e("MGG","new file : "+path+" / "+name);
        return path;
    }

    public static String copyFileFromUri(Context context,Uri source1, String name) throws IOException {


        ContentResolver cr = context.getContentResolver();
        InputStream initialStream = cr.openInputStream(source1);


        byte[] buffer = new byte[initialStream.available()];
        initialStream.read(buffer);
        File destFile = new File(Environment.getExternalStorageDirectory().getPath(), "Around Files/Media/cache2");

        if (!destFile.getParentFile().exists())
            destFile.getParentFile().mkdirs();

        if (!destFile.exists()) {
            destFile.createNewFile();
        }

        File targetFile = new File(destFile.getAbsolutePath(), name);
        OutputStream outStream = new FileOutputStream(targetFile);
        outStream.write(buffer);
        return targetFile.getAbsolutePath();

        /*


        FileChannel source = null;
        FileChannel destination = null;

        try {
            source = is.getChannel();
            destination = new FileOutputStream(new File(destFile.getAbsolutePath(), name)).getChannel();
            destination.wri
        } finally {
            if (source != null) {
                source.close();
            }
            if (destination != null) {
                destination.close();
            }
        }*/
    }


    private static MediaPlayer mediaPlayer;
    private static Timer mediaPlayerTimer;
    private static TimerTask mediaPlayertask;
    private static int mediaFileLengthInMilliseconds;
    private static SeekBar bfmusicSeekbar;
    private static String audioMediauri="";
    private static AudioPlayerListener audioListener;
    private static TextView updateText;

    public static void resetMediaPlayer()
    {

        if(mediaPlayer!=null)
        {
            try{
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer=null;}
            catch (Exception e)
            {}
        }
        if(mediaPlayerTimer!=null)
        {
            try{
            mediaPlayerTimer.cancel();
            mediaPlayerTimer.purge();}
             catch (Exception e)
            {}
        }

    }
    public static long getMediaDuration(Context context,String mediaUri)
    {

        Uri uri = Uri.parse(mediaUri);
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        mmr.setDataSource(context,uri);
        String durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        int millSecond = Integer.parseInt(durationStr);
        return millSecond;
    }

   public static void playAudio(Context context, String audioUri, SeekBar bfmusicSeek, TextView updateTxt, AroundVisualizerView visualizerView,AudioPlayerListener listner)
    {
        if(mediaPlayer!=null)
        {
            audioListener.onPaused();
        }
        if(bfmusicSeekbar!=null)
            bfmusicSeekbar=null;
       bfmusicSeekbar=bfmusicSeek;
        Log.e("MGG","audioo starting "+audioUri);
        audioListener=listner;

        if(!audioMediauri.equals(audioUri))
        {
            resetMediaPlayer();
            audioMediauri=audioUri;
        }
        updateText=updateTxt;

        setUpMediaPlayer(context,audioUri,visualizerView);
    }

    private static  void setUpMediaPlayer(Context context,String audioUri,AroundVisualizerView visualizerView)
    {
        if(mediaPlayer==null)
        {
            mediaPlayer=new MediaPlayer();
            if(audioUri.equals("null"))
                mediaPlayer = MediaPlayer.create(context, R.raw.music_file);
            else {
                //mediaPlayer = MediaPlayer.create(context, Uri.parse(audioUri));
               // String uriii=Environment.getExternalStorageDirectory().getPath()+"/Around Files/Audio/bcuaxmzuuyjdsgepnall.mp3";
               // File xxx=new File(uriii);
                //File xxx2=new File(audioUri);
                mediaPlayer = MediaPlayer.create(context, Uri.parse(audioUri));
                /*Log.e("MGG","audioo "+xxx.exists()+" / "+ xxx.canRead());
                Log.e("MGG","audioo "+xxx2.exists()+" / "+ xxx2.canRead());
                Log.e("MGG","audioo "+Uri.parse(audioUri)+" /\n / "+ Uri.parse(xxx.getAbsolutePath()));*/
            }
            if(mediaPlayer==null)
            {
                Toast.makeText(context, "Audio missing or corrupted", Toast.LENGTH_SHORT).show();
            }
            //mediaPlayer.prepare();
            mediaFileLengthInMilliseconds=mediaPlayer.getDuration();

            mediaPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                @Override
                public void onBufferingUpdate(MediaPlayer mp, int percent) {
                   // bfmusicSeekbar.setSecondaryProgress(percent);  /****/
                }
            });
           /* mediaPlayer.setOnTimedTextListener(new MediaPlayer.OnTimedTextListener() {
                @Override
                public void onTimedText(MediaPlayer mp, TimedText text) {
                    Log.e("MSG","media timer "+text);
                    updateText.setText(text.getText());
                }
            });*/
            mediaPlayertask = new TimerTask() {
                @Override
                public void run() {

                    Log.e("MGG","changing  now");
                    if (mediaPlayer!=null && mediaPlayer.isPlaying()) {

                        float pos=((float)mediaPlayer.getCurrentPosition()/mediaFileLengthInMilliseconds)*100;
                        bfmusicSeekbar.setProgress((int)pos);
                        Log.e("MGG","installing to "+mediaPlayer.getCurrentPosition()+ " scroll is "+bfmusicSeekbar.getProgress());
                       // if(updateText!=null)

                        audioListener.onIntervelUpdate(mediaPlayer.getCurrentPosition());

                    }

                }
            };

            mediaPlayerTimer = new Timer();

            mediaPlayerTimer.schedule(mediaPlayertask, 0, 400);


            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    //playPauseButton.setImageResource(R.drawable.ic_play_arrow_accent);
                    if (mVisualizer!=null)
                    mVisualizer.setEnabled(false);
                    bfmusicSeekbar.setProgress(0);
                    if(audioListener!=null)
                        audioListener.onCompleted();
                }
            });



            bfmusicSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    //int playPositionInMillisecconds = (mediaFileLengthInMilliseconds / 100) * bfmusicSeekbar.getProgress();
                    // mediaPlayer.pause();
                    // if(!mediaPlayer.isPlaying())
                    // mediaPlayer.seekTo(playPositionInMillisecconds);
                    // mediaPlayer.start();
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {


                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    int playPositionInMillisecconds = (mediaFileLengthInMilliseconds / 100) * bfmusicSeekbar.getProgress();
                    //pauseMediaPlayer();
                    mediaPlayer.seekTo(playPositionInMillisecconds);
                    Log.e("MGG","seeked to "+playPositionInMillisecconds+ " scroll is "+bfmusicSeekbar.getProgress());
                }
            });


            if(visualizerView!=null && PermissionManager.checkRecordAudioPermission((Activity)context)) {
                Log.e("GGG","visualizer SETTING");
                setupVisualizerFxAndUI(visualizerView);
            }


        }
        if(mediaPlayer.isPlaying())
        {
            pauseMediaPlayer();
        }
        else
            {

                Log.e("MGG","audioo medi is  playing ");
            startMediaPlayer();
        }
    }
    private static Visualizer mVisualizer;

    private static void setupVisualizerFxAndUI(final AroundVisualizerView visualizerView) {

        // Create the Visualizer object and attach it to our media player.
        mVisualizer = new Visualizer(mediaPlayer.getAudioSessionId());
        mVisualizer.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
        mVisualizer.setDataCaptureListener(
                new Visualizer.OnDataCaptureListener() {
                    public void onWaveFormDataCapture(Visualizer visualizer,
                                                      byte[] bytes, int samplingRate) {
                        visualizerView.updateVisualizer(bytes);
                    }

                    public void onFftDataCapture(Visualizer visualizer,
                                                 byte[] bytes, int samplingRate) {
                    }
                }, Visualizer.getMaxCaptureRate() / 2, true, false);
    }
    static void pauseMediaPlayer()
    {
        //playPauseButton.setImageResource(R.drawable.ic_play_arrow_accent);

        mediaPlayer.pause();
        if(audioListener!=null)
            audioListener.onPaused();
        //mediaPlayerTimer.cancel();
    }
    static void startMediaPlayer()
    {
        //playPauseButton.setImageResource(R.drawable.ic_pause_accent);
        if (mVisualizer!=null)
            mVisualizer.setEnabled(true);


        mediaPlayer.start();

        if(audioListener!=null)
            audioListener.onStarted();

    }
    public static Bitmap getSquareBitmap(Bitmap bitmap)
    {
        Bitmap dstBmp;
        if (bitmap.getWidth() >= bitmap.getHeight()){

            //Log.e("MGG","width greter ");
            dstBmp = Bitmap.createBitmap(
                    bitmap,
                    bitmap.getWidth()/2 - bitmap.getHeight()/2,
                    0,
                    bitmap.getHeight(),
                    bitmap.getHeight()
            );


        }else{

            dstBmp = Bitmap.createBitmap(
                    bitmap,
                    0,
                    bitmap.getHeight()/2 - bitmap.getWidth()/2,
                    bitmap.getWidth(),
                    bitmap.getWidth()
            );
        }
        return dstBmp;
    }

    public static MediaData returnMediaFromRow(Context context,long row)
    {

        SQLiteDatabase db = DatabaseManager.getDatabase(context).getReadableDatabase();

        Cursor cursor = db.query(DatabaseAccesser.media_table,
                null,
                DatabaseAccesser.media_table+".rowid" + " =?",
                new String[]{row+""},
                null, null, null,null);

        if(cursor.getCount()==1)
        {
            cursor.moveToNext();
            MediaData xx=new MediaData();
            xx.globalUri = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseAccesser.media_global_uri));
            xx.globalLiteUri=cursor.getString(cursor.getColumnIndexOrThrow(DatabaseAccesser.media_lite_global_uri));
            xx.localUri = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseAccesser.media_local_uri));
            xx.localLiteUri=cursor.getString(cursor.getColumnIndexOrThrow(DatabaseAccesser.media_lite_local_uri));
            return xx;
        }
        else {
            return null;
        }
    }
    public static Uri getMediaOgUriFromTemp2(Context context,Uri uris)
    {
        Uri uri = uris;
        String[] projection = { MediaStore.Files.FileColumns.DATA};

        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);

        if(cursor.moveToFirst()) {

            //Log.d(TAG, DatabaseUtils.dumpCursorToString(cursor));

            int columnIndex = cursor.getColumnIndex(projection[0]);
            if(columnIndex<0)
                return null;
            String picturePath = cursor.getString(columnIndex); // returns null
            cursor.close();
            return Uri.parse(picturePath);
        }
        cursor.close();
        return null;
    }

    private static InputStream openFileUri(Uri uri) throws IOException {
        FileInputStream fin   = new FileInputStream(uri.getPath());
        //int             owner = FileUtils.getFileDescriptorOwner(fin.getFD());

       // if (owner == -1 || owner == Process.myUid()) {
            fin.close();
           // throw new IOException("File owned by application");
        //}

        return fin;
    }
    public static Uri getMediaUriAfterCopyingtoCache(Context context,Uri uris,String mimeType) {
        try {
            if (uris == null) {
                return null;
            }
            if(mimeType==null)
                MimeTypeMap.getSingleton().getMimeTypeFromExtension(uris.toString());

            InputStream inputStream;

            if ("file".equals(uris.getScheme())) {
                inputStream = openFileUri(uris);
            } else {
                inputStream = context.getContentResolver().openInputStream(uris);
            }

            if (inputStream == null) {
                return null;
            }

            Cursor cursor   = context.getContentResolver().query(uris, new String[] {OpenableColumns.DISPLAY_NAME, OpenableColumns.SIZE}, null, null, null);
            String fileName = null;
            Long   fileSize = null;

            try {
                if (cursor != null && cursor.moveToFirst()) {
                    try {
                        fileName = cursor.getString(cursor.getColumnIndexOrThrow(OpenableColumns.DISPLAY_NAME));
                        fileSize = cursor.getLong(cursor.getColumnIndexOrThrow(OpenableColumns.SIZE));
                    } catch (IllegalArgumentException e) {
                       // Log.w(TAG, e);
                    }
                }
            } finally {
                if (cursor != null) cursor.close();
            }
            return createNewFile(context,inputStream,System.currentTimeMillis(),mimeType,fileName,fileSize);
        }
        catch (IOException ioe) {
           // Log.w(TAG, ioe);
            return null;
        }

    }
    public static String copyFile(Context context, Uri uris, File tempo, JobCompletionWithFailureListner listener)
    {//fileName+"."+getExtentionFromMimeType(mimeType)
        InputStream inputStream;
        //Uri uris=source;
        try {
            if ("file".equals(uris.getScheme())) {
                inputStream = new FileInputStream(uris.getPath());
            } else {
                inputStream =context.getContentResolver().openInputStream(uris);
            }
            Log.e("GGG","dirdir input  "+inputStream);
           // File tempo=new File(context.getCacheDir(), destination);//yyy
            //Log.e("GGG","intent fianl copy uri "+Uri.fromFile(tempo));
            OutputStream outputStream =new FileOutputStream(tempo);
            Log.e("GGG","dirdir outputStream  "+outputStream);
            copyFile(inputStream,outputStream);
            listener.onJobCompletion(tempo.getAbsolutePath());
            return  tempo.getAbsolutePath();

        }
        catch (Exception e){
            Log.e("GGG","dirdir fail2 "+e.getMessage()+" "+e.toString());
            listener.onJobFailed(e.getMessage()+" "+e.toString());
        }
        return null;
    }

    public static long copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[8192];
        int read;
        long total = 0;

        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
            total += read;
        }
        Log.e("GGG","intent fianl COPY COMPLETE");
        in.close();
        out.close();

        return total;
    }
    public static Uri createNewFile(@NonNull Context context,
                       //@NonNull  AttachmentSecret attachmentSecret,
                       @NonNull  InputStream input,
                       long id,
                       @NonNull  String mimeType,
                       @Nullable String fileName,
                       @Nullable Long fileSize)
    {
        //persistToDisk(context, attachmentSecret, id, input);


        try {
            File tempo=new File(context.getCacheDir(), fileName+"."+getExtentionFromMimeType(mimeType));//yyy
            Log.e("GGG","intent fianl copy uri "+Uri.fromFile(tempo));
            OutputStream xx =new FileOutputStream(tempo);

            copyFile(input, xx);
        } catch (IOException e) {
            e.printStackTrace();
        }

       /* final Uri uniqueUri = CONTENT_URI.buildUpon()
                .appendPath(mimeType)
                .appendPath(fileName)
                .appendEncodedPath(String.valueOf(fileSize))
                .appendEncodedPath(String.valueOf(System.currentTimeMillis()))
                .build();*/
       // Log.e("GGG","intent fianl return uri "+ContentUris.withAppendedId(uniqueUri, id));
        //return ContentUris.withAppendedId(uniqueUri, id);
        return Uri.fromFile(new File(context.getCacheDir(), fileName+".png"));
    }
   // private static final String     URI_STRING            = "content://com.hash.around/capture-new";
    //public  static final Uri        CONTENT_URI           = Uri.parse(URI_STRING);

    public static Uri getMediaOgUriFromTemp(Context context,Uri uri){
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);

        document_id = document_id.substring(document_id.lastIndexOf(":")+1);
        cursor.close();
       Uri BASE_URL=MediaStore.Files.getContentUri("external");
        cursor = context.getContentResolver().query(
                BASE_URL,
                null, MediaStore.Files.FileColumns._ID + " = ? ", new String[]{document_id}, null);

        Uri path=null;
        Log.e("GGG","intent countis  is "+cursor.getCount()+"/ "+BASE_URL);
        if(cursor.getCount()>0)
        {
            cursor.moveToFirst();
         path = Uri.withAppendedPath(BASE_URL,cursor.getString(0));}
        cursor.close();

        return path;
    }


}
