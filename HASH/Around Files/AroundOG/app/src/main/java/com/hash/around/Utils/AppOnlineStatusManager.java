package com.hash.around.Utils;


import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.hash.around.TabbedActivity;


public  class AppOnlineStatusManager {
//activityies eexluced = ClearAvatar...Countries list...Image crop..MapsActivity..Media Edit..RegisterActivity..Set profile
    //..Upload Activity
    public static class OnlineStatus{
        //Thread thread;
        static Boolean thread=false;
        static boolean active=true;
       // Handler hand;
        public static void setActive(final boolean activee)
        {
            active=activee;
            if(!thread)
            {
                thread = new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("GGG","onlinestatus CHANGE UPDATE "+(active?"online":"offline"));
                        if(active)
                        {

                            TabbedActivity.updateOnlineStatus(true);
                        }
                        else
                        {
                            TabbedActivity.updateOnlineStatus(false);
                        }
                            thread= new Handler(Looper.getMainLooper()).postDelayed(this,10000);
                    }
                }, 0);
            }
            Log.e("GGG","onlinestatus CHANGE "+(active?"online":"offline"));
        }
    }
}