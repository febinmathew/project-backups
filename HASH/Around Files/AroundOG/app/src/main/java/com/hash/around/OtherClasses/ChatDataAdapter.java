package com.hash.around.OtherClasses;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.util.TypedValue;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.hash.around.ChatForwardActivity;
import com.hash.around.ChatWindow;
import com.hash.around.MediaViewer;
import com.hash.around.OtherClasses.DataTypes.ChatReplayData;
import com.hash.around.OtherClasses.DataTypes.GroupChatData;
import com.hash.around.OtherClasses.DataTypes.LocationData;
import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.hash.around.OtherClasses.DataTypes.VCardData;
import com.hash.around.OtherClasses.Interfaces.AudioPlayerListener;
import com.hash.around.OtherClasses.Interfaces.ChatSentListner;
import com.hash.around.TabFragments.FeedFullScreen;
import com.hash.around.TabbedActivity;
import com.hash.around.Utils.AroundStaticInfo;
import com.hash.around.Utils.PrefManager;
import com.hash.around.Views.CustomMakers.AroundDownloadView;
import com.hash.around.Views.CustomMakers.ChatView_ItemHeader;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.OtherClasses.Interfaces.JobCompletionWithFailureListner;
import com.hash.around.OtherClasses.Interfaces.ProcessCompletedListner;
import com.hash.around.R;
//import com.hash.around.TabbedActivity;
import com.hash.around.Utils.ChatSender;
import com.hash.around.Utils.DatabaseManager;
import com.hash.around.Utils.MediaManager;
import com.hash.around.Utils.StringUtils;
import com.hash.around.Utils.UserManager;
import com.hash.around.Views.CircleColorImageView;
import com.hash.around.Views.Glide.GlideApp;
import com.hash.around.Views.Medias.AroundChatAudioThumbline;
import com.hash.around.Views.Medias.AroundChatFileThumbline;
import com.hash.around.Views.Medias.AroundChatImageThumbline;
import com.hash.around.Views.Medias.AroundCircularImageView;
import com.hash.around.Views.Medias.ChatReplayLayout;
import com.hash.around.Views.TimerWithTextView;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.vanniktech.emoji.EmojiEditText;
import com.vanniktech.emoji.EmojiTextView;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;


import static android.view.View.GONE;
import static com.hash.around.TabbedActivity.checkLocationExists;
import static com.hash.around.TabbedActivity.mainStorageReference;
import static com.hash.around.TabbedActivity.saveLocationToDatabase;

import static com.hash.around.TabbedActivity.userdetail;


public class ChatDataAdapter extends RecyclerView.Adapter implements View.OnCreateContextMenuListener{

    Context mContext;
    public ArrayList<GroupChatData> data;
    public static int optionPosition=-1;
    public SparseBooleanArray selectedItems;
    boolean isChat=true;
    private ProcessCompletedListner onLoadMoreListener;
    private int visibleThreshold = 1;
    private int lastVisibleItem,prevlastVisibleItem=-2, totalItemCount;
    private boolean loading;
    RecyclerView recyclerView;

    public void toggleSelection(int pos) {
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
        }
        else {
            selectedItems.put(pos, true);
        }
        notifyItemChanged(pos);
    }
    public boolean moveToChatSlPosition(long sl,String userIdOfTarget) {
        int pos=-1;
        for(int i=data.size()-1;i>=0;i--)
        {
            Log.e("GGG","row is: "+sl+" current is: "+data.get(i).sl+" row:"+data.get(i).chatRow);
            Log.e("GGG","row euality : "+data.get(i).userid+" to: "+userIdOfTarget);
            //CODE 99 for groups must check the username equality
            if(data.get(i).sl==sl && (data.get(i).sender==1?data.get(i).userid:userdetail.getUid()).equals(userIdOfTarget)){
                pos=i;
                break;
            }
        }
        if(pos!=-1){
            selectedItems.put(pos, true);
            notifyItemChanged(pos);
            recyclerView.scrollToPosition(selectedItems.keyAt(selectedItems.size()-1));
            return true;
        }
        else
            return false;
    }

    public void clearSelections() {
        selectedItems.clear();
        notifyDataSetChanged();
    }


    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items =
                new ArrayList<Integer>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }
    public GroupChatData getChatDataKeyAtPosition(int i)
    {
        return data.get(selectedItems.keyAt(i));
    }
    public int deleteSelectedItems() {
        //List<Integer> items =new ArrayList<Integer>(selectedItems.size());
        ArrayList<Long> ids = new ArrayList<Long>();

        for (int i = selectedItems.size()-1; i >= 0; i--) {

            int pos=selectedItems.keyAt(i);
            ids.add(data.get(pos).chatRow);
            data.remove(selectedItems.keyAt(i));
        }

        String[] names=new String[ids.size()];
        for(int i=0;i<names.length;i++)
        {
            names[i]=ids.get(i)+"";
        }
        deleteChatsFromRowArray(mContext,names);

        selectedItems.clear();
        notifyDataSetChanged();
        return 0;
    }

    public static void deleteChatsFromRowArray(Context context,String[] names)
    {

        SQLiteDatabase db = DatabaseManager.getDatabase(context).getWritableDatabase();
        /*String query = "DELETE FROM "+ DatabaseAccesser.chat_table+ " WHERE "+DatabaseAccesser.chat_table+".rowid "+" IN ( " + makePlaceholders(names.length) + " )";
        Cursor cursor = db.rawQuery(query, names);
        cursor.close();*/

        for (int i=0;i<names.length;i++)
        {
            db.delete(DatabaseAccesser.chat_table,DatabaseAccesser.chat_table+".rowid =? ",new String[]{names[i]});
        }
        Log.e("GGG","deletedelete "+names.length+ " / "+makePlaceholders(names.length));
    }

    public void clearChatHistory(String userId)
    {
        SQLiteDatabase db = DatabaseManager.getDatabase(mContext).getWritableDatabase();

        db.delete(DatabaseAccesser.chat_table, DatabaseAccesser.chat_userid + "=?", new String[]{userId});
        data.clear();

        ContentValues values2 = new ContentValues();

        values2.put(DatabaseAccesser.chatList_lastmsg, "");
        values2.put(DatabaseAccesser.chatList_lastmsg_type, 0);
        values2.put(DatabaseAccesser.chatList_msgqueue, 0);

        int rowln=db.update(DatabaseAccesser.chatList_table,values2, DatabaseAccesser.chatList_userid+" =?",new String[]{userId});
        //db.delete(DatabaseAccesser.chatList_table, DatabaseAccesser.chatList_userid + "=?", new String[]{userId});
        notifyDataSetChanged();
    }


    static String makePlaceholders(int len) {
        if (len < 1) {
            // It will lead to an invalid query anyway ..
            throw new RuntimeException("No placeholders");
        } else {
            StringBuilder sb = new StringBuilder(len * 2 - 1);
            sb.append("?");
            for (int i = 1; i < len; i++) {
                sb.append(",?");
            }
            return sb.toString();
        }
    }
    UserWholeData userdata;

    public ChatDataAdapter(Context mcont, ArrayList<GroupChatData> list, boolean chat, RecyclerView recyclerView, UserWholeData userdata)
    {

        this.userdata=userdata;
        mContext=mcont;
        data=list;
        this.recyclerView=recyclerView;
        // act=activity;
        selectedItems=new SparseBooleanArray();
        isChat=chat;
        final Context ccc=mContext;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                .getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView,
                                   int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();
                if (lastVisibleItem <=0) {
                    // End has been reached
                    if(!loading && prevlastVisibleItem!=lastVisibleItem) {
                        prevlastVisibleItem = lastVisibleItem;
                        //Toast.makeText(ccc, "chat : " + totalItemCount + " " + lastVisibleItem, Toast.LENGTH_SHORT).show();
                        // Do something
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.processCompleteAction();
                        }
                        //loading = true;
                    }
                }


            }
        });
    }

    /*public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }*/

    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        super.onViewRecycled(holder);
        if( ((TimerWithTextView)holder.itemView.findViewById(R.id.expireTimer))!=null)
        ((TimerWithTextView)holder.itemView.findViewById(R.id.expireTimer)).stopAnimation();


    }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v=null;
        switch(viewType) {
            case 0:

                v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_sent, parent, false);
                // set the view's size, margins, paddings and layout parameters

                TextChatView vh = new TextChatView(v);
                //vh.refreshExpireTimer(mContext,viewData);
                return vh;
            case 1:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_receive, parent, false);
                // set the view's size, margins, paddings and layout parameters

                ChatDataAdapter.TextChatView vh1 = new ChatDataAdapter.TextChatView(v);
                return vh1;
            case 2:

                v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_sent_image, parent, false);
                // set the view's size, margins, paddings and layout parameters

                ChatDataAdapter.ChatImageView vh2 = new ChatDataAdapter.ChatImageView(v);
                return vh2;
            case 3:

                v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_receive_image, parent, false);
                // set the view's size, margins, paddings and layout parameters

                ChatDataAdapter.ChatImageView vh6 = new ChatDataAdapter.ChatImageView(v);
                return vh6;
            //4-audiosent//5-audioreceive// 6-videosent//7-videoreceive//11-gifsent//12-gif receive
            // 13- filesent//14 filereceive//15-contactsent//16-contactreceive
            case 4:

                v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_sent_audio, parent, false);
                // sent audio

                ChatDataAdapter.AudioView vh9 = new ChatDataAdapter.AudioView(v);
                return vh9;

            case 5:

                v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_receive_audio, parent, false);
                // receive audio

                ChatDataAdapter.AudioView vh10 = new ChatDataAdapter.AudioView(v);
                return vh10;

           /* case 6:

                v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_sent_video, parent, false);
                // sent video

                ChatDataAdapter.ViewHolder5 vh7 = new ChatDataAdapter.ViewHolder5(v);
                return vh7;

            case 7:

                v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_receive_image, parent, false);
                // receive video

                ChatDataAdapter.ViewHolder5 vh8 = new ChatDataAdapter.ViewHolder5(v);
                return vh8;*/



            case 8:

                v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_sent_location, parent, false);

                ChatDataAdapter.LocationView vh4 = new ChatDataAdapter.LocationView(v);
                return vh4;
            case 9:

                v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_receive_location, parent, false);

                ChatDataAdapter.LocationView vh5 = new ChatDataAdapter.LocationView(v);
                return vh5;

            case 11:

                v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_sent_image, parent, false);
                // sent contact

                ChatDataAdapter.ChatImageView vh11 = new ChatDataAdapter.ChatImageView(v);
                return vh11;

            case 12:

                v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_receive_image, parent, false);
                // receive contact

                ChatDataAdapter.ChatImageView vh12 = new ChatDataAdapter.ChatImageView(v);
                return vh12;

            case 13:

                v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_sent_file, parent, false);
                // sent file

                ChatDataAdapter.DocumentView vh13 = new ChatDataAdapter.DocumentView(v);
                return vh13;
            case 14:

                v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_receive_file, parent, false);
                // receive file

                ChatDataAdapter.DocumentView vh14 = new ChatDataAdapter.DocumentView(v);
                return vh14;

            case 15:

                v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_sent_contacts, parent, false);
                // receive file

                ChatDataAdapter.ContactView vh15 = new ChatDataAdapter.ContactView(v);
                return vh15;

            case 16:

                v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_receive_contacts, parent, false);

                ChatDataAdapter.ContactView vh16= new ChatDataAdapter.ContactView(v);
                return vh16;

            case 20:

                v= LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_extra_info, parent, false);
                // set the view's size, margins, paddings and layout parameters

                ChatDataAdapter.ChatInfoView vh17 = new ChatDataAdapter.ChatInfoView(v);
                return vh17;

        }
        return null;
    }

    public void onBindDateItemDecorator(RecyclerView.ViewHolder holder, int position) {
        Log.e("GGG","first do bind:44");
        String dateText;
        Log.e("GGG","decorator pos "+position+" / total size is "+data.size());
        long timestamp=data.get(position).timestamp;
        if (StringUtils.isToday(timestamp)) {
            dateText= "Today";
        } else if (StringUtils.isYesterday(timestamp)) {
            dateText= "Yesterday";
        } else {
            dateText = new SimpleDateFormat("EEE, MMM d, yyyy", Locale.ENGLISH).format(new Date(timestamp));
        }
        ((ChatView_ItemHeader.HeaderViewHolder)holder).textView.setText(dateText);
    }
    public void onBindUnseenItemDecorator(RecyclerView.ViewHolder holder, int position) {

        //String dateText="5 UNSEEN MESSAGES";
        int count=0;
        //long timestamp=data.get(position).timestamp;
        for(int i=position;i<data.size();i++)
        {
            if(data.get(i).sender==1)
                count++;

        }
        /*if (StringUtils.isToday(timestamp)) {
            dateText= "Today";
        } else if (StringUtils.isYesterday(timestamp)) {
            dateText= "Yesterday";
        } else {
            dateText = new SimpleDateFormat("EEE, MMM d, yyyy", Locale.ENGLISH).format(new Date(timestamp));
        }*/
        //Log.e("MSG","count iss "+count);
        ((ChatView_ItemHeader.HeaderViewHolder)holder).textView.setText(
                mContext.getResources().getQuantityString(R.plurals.chat_unseen_messages,count,count));
    }
    //static Calendar lastTimerDate;
    public boolean hasTimeHeader(int position, boolean isReverse) {

        if (isReverse && position == data.size() - 1 )
            return false;
        else if(!isReverse && position == 0)
        {
            return true;
        }
        int previous = position + (isReverse ? 1 : -1);
        Calendar headerId = Calendar.getInstance();
        headerId.setTimeInMillis(data.get(position).timestamp);
        Calendar previousHeaderId = Calendar.getInstance();
        previousHeaderId.setTimeInMillis(data.get(previous).timestamp);

        //Date dt=new Date(data.get(position).timestamp);
        SimpleDateFormat format = new SimpleDateFormat("hh:mm a");
        //format.format(dt)


        // Log.e("MSG","datedate: "+headerId.get(Calendar.DAY_OF_YEAR)+" / "+previousHeaderId.get(Calendar.DAY_OF_YEAR)+" || pos: "+data.get(position).msg+" / "+position+" - "+previous+" time: "+format.format(new Date(data.get(position).timestamp))+" / "+
        //format.format(new Date(data.get(previous).timestamp)));


        if(/*lastTimerDate !=null && */headerId.get(Calendar.YEAR)==previousHeaderId.get(Calendar.YEAR) && headerId.get(Calendar.DAY_OF_YEAR)==previousHeaderId.get(Calendar.DAY_OF_YEAR))
        {
            return false;
        }
        return true;
    }


    public boolean hasUnseenHeader(int position,boolean isReverse,long timestamp)
    {
        /*if (isActiveCursor()) {
            return false;
        }

        if (lastSeenTimestamp <= 0) {
            return false;
        }*/

        if (isReverse && position == data.size() - 1 )
            return false;
        else if(!isReverse && position == 0)
        {
            return false;
        }

        int sender=data.get(position).sender;
        long currentSeen  = data.get(position).timestamp;
        long previousSeen = data.get(position-1).timestamp;

        return currentSeen>timestamp && previousSeen<=timestamp;
        //return (currentSeen != 3 && sender==1) && previousSeen == 3;
    }


    static class LastSeenHeader extends ChatView_ItemHeader {

        public LastSeenHeader(Context context, ChatDataAdapter adap) {
            super(context, adap);
        }



    }

    void openMediaViewer(GroupChatData dat)
    {
        Intent intent = new Intent(mContext.getApplicationContext(), MediaViewer.class);
        intent.putExtra("userID",userdata.user_id);
        intent.putExtra("mediaID",dat.media_id);
        //intent.putExtra("numberType", 5);
        //intent.putExtra("uri",data.get(pos).final_url);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                               /* ActivityOptionsCompat options = ActivityOptionsCompat
                                        .makeSceneTransitionAnimation(act,view, "robot");*/
        mContext.startActivity(intent);
    }

    void shareChat(int pos)
    {
        Intent intent = new Intent(mContext, ChatForwardActivity.class);
        ArrayList<GroupChatData> chatsArray=new ArrayList<>();
        chatsArray.add(data.get(pos));
        Bundle args = new Bundle();
        args.putSerializable("ARRAYLIST",(Serializable)chatsArray);
        intent.putExtra("BUNDLE",args);
        mContext.startActivity(intent);
    }
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        final int pos=position;
        Date dt=new Date(data.get(position).timestamp);
        SimpleDateFormat format = new SimpleDateFormat("hh:mm a");

        if(selectedItems.get(position))
            holder.itemView.setBackgroundColor( ContextCompat.getColor(mContext, R.color.chat_selection));
        else
            holder.itemView.setBackgroundColor( ContextCompat.getColor(mContext, R.color.transparent));
        final GroupChatData viewData=data.get(pos);

        if(!isChat)
            Log.e("GGG","usernameuser 1 "+data.get(pos).name);

        if(data.get(position).sender==1&& viewData.msgseen==2)
        {
            viewData.chat_seen_time=System.currentTimeMillis();
            if(DatabaseManager.addPendingProcess(mContext,data.get(position).chatRow,0,viewData.sl,viewData.userid)!=-1){
                Intent inn=new Intent("PENDING_UPDATED");
                mContext.sendBroadcast(inn);
            viewData.msgseen=3;
            DatabaseManager.updateSelfChatstatus(viewData.chatRow,3);
            }

        }

        switch(data.get(position).type)
        {
            case 21:
                Drawable follow=mContext.getResources().getDrawable(R.drawable.ic_follow_white);
                follow.setColorFilter(mContext.getResources().getColor(R.color.black), PorterDuff.Mode.SRC_IN);
                if(data.get(position).msg.equals(data.get(position).userid))
                   ((ChatInfoView)holder).info.setText(data.get(position).name+" joined");
                else
                    ((ChatInfoView)holder).info.setText(data.get(pos).name+" added "+ ChatWindow.returnSingleUserDetails(mContext,data.get(position).msg).user_name/*+data.get(position).msg*/);
                ((ChatInfoView)holder).info.setCompoundDrawablesWithIntrinsicBounds(follow,null,null,null);
                return;

            case 22:
                Drawable unfollow=mContext.getResources().getDrawable(R.drawable.ic_unfollow_white);
                unfollow.setColorFilter(mContext.getResources().getColor(R.color.black), PorterDuff.Mode.SRC_IN);
                if(data.get(position).msg.equals(data.get(position).userid))
                    ((ChatInfoView)holder).info.setText(data.get(position).name+" left");
                else
                    ((ChatInfoView)holder).info.setText(data.get(position).name+" removed "+ ChatWindow.returnSingleUserDetails(mContext,data.get(position).msg).user_name);
                ((ChatInfoView)holder).info.setCompoundDrawablesWithIntrinsicBounds(unfollow,null,null,null);
                return;

        }

        if(isChat)
        {holder.itemView.findViewById(R.id.grpusername).setVisibility(GONE);}
        else
        {
            Log.e("GGG","usernameuser "+data.get(pos).name);
            EmojiTextView usernameText=(EmojiTextView)holder.itemView.findViewById(R.id.grpusername);
            usernameText.setVisibility(View.VISIBLE);
            usernameText.setText(data.get(pos).name);
            //Log.e("GGG","usernameuser "+data.get(pos).name);
            usernameText.setOnCreateContextMenuListener(this);
            usernameText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    optionPosition = pos;
                    Log.e("GGG","groupchat optionPositionuser name click  "+optionPosition);
                    view.showContextMenu();
                }
            });
            usernameText.setLongClickable(false);
           // ((TextChatView) holder).username.setLongClickable(false);
        }
//Log.e("GGG","feedfeed "+data.get(pos).chatReplay.content_ID+" / "+data.get(pos).chatReplay.userID+" / "+data.get(pos).chatReplay.userName);
        ChatReplayLayout replaySection= (ChatReplayLayout) holder.itemView.findViewById(R.id.replaySection);
        if(data.get(position).chatReplay!=null) {
            replaySection.setReplayData(data.get(position).chatReplay,false);
            replaySection.disableCancelButton();
            replaySection.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ChatReplayData reDat=data.get(pos).chatReplay;
                    if(reDat.type==1)
                    {
                        Log.e("GGG","chatrepp ADAP "+reDat.content_sl+" / "+reDat.userID);

                        if(!moveToChatSlPosition(reDat.content_sl,reDat.userID))
                            Toast.makeText(mContext, "Chat not found", Toast.LENGTH_SHORT).show();
                    }
                    else if(reDat.type==2)
                    {
                        Log.e("GGG","content id is "+reDat.content_ID);
                        Intent indi=new Intent(mContext,FeedFullScreen.class);
                        indi.putExtra("feedID",reDat.content_ID);
                        //indi.putExtra("feedSL",reDat.content_sl);

                        indi.putExtra("username",reDat.userName);
                        indi.putExtra("userid",reDat.userID);

                        //indi.putExtra("targetFeed",reDat.content_sl);
                        mContext.startActivity(indi);
                    }
                }
            });
        }
        else
            replaySection.setVisibility(GONE);


        //long curMilliSecond=System.currentTimeMillis();

        try {
            if (data.get(position).msg.length() > 0) {
                ((EmojiTextView) holder.itemView.findViewById(R.id.chatText)).setVisibility(View.VISIBLE);
                ((EmojiTextView) holder.itemView.findViewById(R.id.chatText)).setText(StringUtils.makeHashTagText(mContext, data.get(position).msg, true));
                ((EmojiTextView) holder.itemView.findViewById(R.id.chatText)).setTextSize(TypedValue.COMPLEX_UNIT_SP, Integer.parseInt(PrefManager.getStringPreference(mContext,"pref_chat_font_size","16")));
            } else
                holder.itemView.findViewById(R.id.chatText).setVisibility(GONE);
        }
        catch (Exception d){}

        switch(data.get(position).type)
        {
            case 7:
                final VCardData vData=new VCardData();
                vData.encodeVcard(data.get(pos).msg);

                String[] numberArray = new String[vData.number.size()];
                numberArray = vData.number.toArray(numberArray);

                ((ContactView)holder).date.setText(format.format(dt));
                ((ContactView)holder).contactName.setText(vData.name);
                Bitmap image=null;//vData.getPhotoBitmap();

                if(image!=null)
                    ((ContactView)holder).contactImage.setImageBitmap(image);
                else
                    ((ContactView)holder).contactImage.setImageBitmap(BitmapFactory.decodeResource(mContext.getResources(),R.drawable.default_user));

                ((ContactView)holder).contactNumber.setText(StringUtils.formatStringArray(numberArray,true));

                if(numberArray.length>1)
                    ((ContactView)holder).noAmount.setText(mContext.getResources().getQuantityString(R.plurals.contact_amount, numberArray.length, numberArray.length));
                else
                    ((ContactView)holder).noAmount.setVisibility(GONE);

                ((ContactView)holder).inviteUser.setVisibility(View.VISIBLE);

                //Need to add userid to contact data
                if(false)
                {
                    if("sfddf".equals(FirebaseAuth.getInstance().getUid()))
                        ((ContactView)holder).inviteUser.setVisibility(GONE);
                    else
                        ((ContactView)holder).inviteUser.setText(R.string.message);

                }
                else
                {
                    ((ContactView)holder).inviteUser.setText(R.string.invite);
                }

                ((ContactView)holder).inviteUser.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(false) {

                        }else{

                        }
                    }
                });

                ((ContactView)holder).shareContact.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        shareChat(pos);
                    }
                });

                ((ContactView)holder).saveContact.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String[] numberArray2 = new String[vData.number.size()];
                        numberArray2 = vData.number.toArray(numberArray2);
                        ChatWindow.insertRawContact(mContext,vData.name,numberArray2);
                    }
                });
                break;
            case 4:


                final String[] msg=data.get(position).msg.split(TabbedActivity.TEXT_SEPARATOR);

                ((LocationView)holder).locationText.setText(msg[0]);
                ((LocationView)holder).latitudeText.setText(msg[1]+", "+msg[2]);

                ((EmojiTextView) holder.itemView.findViewById(R.id.chatText)).setText(StringUtils.latLngToMapString(
                        Double.parseDouble(msg[1]),
                        Double.parseDouble(msg[2])));

                ((LocationView)holder).date.setText(format.format(dt));

                ((LocationView)holder).forwardLocation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       /* Intent intent = new Intent(mContext, ChatForwardActivity.class);
                        ArrayList<GroupChatData> chatsArray=new ArrayList<>();
                        chatsArray.add(data.get(pos));
                        Bundle args = new Bundle();
                        args.putSerializable("ARRAYLIST",(Serializable)chatsArray);
                        intent.putExtra("BUNDLE",args);
                        mContext.startActivity(intent);*/
                        shareChat(pos);
                    }
                });

                ((LocationView)holder).addLocation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        final LocationData temp =new LocationData();
                        temp.status=0;
                        temp.location_name=msg[0];
                        temp.latitude=Double.parseDouble(msg[1]);
                        temp.longitude=Double.parseDouble(msg[2]);

                        addLocationToDB(temp,pos);


                    }
                });
                break;
            case 3:
                ((ChatImageView)holder).chatImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view)
                    {
                        openMediaViewer(data.get(pos));
                    }
                });
                break;
            case 5:
                ((ChatImageView)holder).chatImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view)
                    {
                        openMediaViewer(data.get(pos));
                    }
                });
                break;
            case 2:
                holder.itemView.findViewById(R.id.circularInfoArea).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view)
                    {
                        openMediaViewer(data.get(pos));
                    }
                });
                Log.e("MGG","audioo uri "+data.get(pos).final_url);

                ((AudioView)holder).sizeText.setText(StringUtils.mediaSizeToString(data.get(pos).mediaSize));

                ((AudioView)holder).date.setText(format.format(dt));

                ((AudioView)holder).timeIndication.setText(DateUtils.formatElapsedTime(
                        MediaManager.getMediaDuration(mContext,data.get(pos).final_url)/1000));

                Log.e("GGG","chatimageo seeen is "+"\n ms seen "+data.get(position).msgseen);


                final TextView xxx=((AudioView)holder).timeIndication;
                ((AudioView)holder).playPause.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MediaManager.playAudio(mContext, data.get(pos).final_url,
                                ((AudioView) holder).audioSeek, ((AudioView)holder).timeIndication,null,new AudioPlayerListener() {
                                    @Override
                                    public void onStarted() {

                                        ((AudioView)holder).playPause.setImageResource(R.drawable.ic_pause_accent);
                                    }

                                    @Override
                                    public void onPaused() {
                                        Log.e("MGG","audioo paused ");
                                        ((AudioView)holder).playPause.setImageResource(R.drawable.ic_play_arrow_white);
                                    }

                                    @Override
                                    public void onCompleted() {
                                        Log.e("MGG","audioo complted ");
                                        ((AudioView)holder).playPause.setImageResource(R.drawable.ic_play_arrow_white);
                                    }

                                    @Override
                                    public void onIntervelUpdate(final long timestamp) {
                                        ((Activity)mContext).runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                xxx.setText(DateUtils.formatElapsedTime(timestamp/1000));
                                            }
                                        });

                                    }
                                });
                    }
                });

                break;
        }
        switch(data.get(position).sender) {
            case 0:
                refreshScheduleData((TimerWithTextView)holder.itemView.findViewById(R.id.msgScheduler),viewData);
                refreshExpireData(mContext, (TimerWithTextView)holder.itemView.findViewById(R.id.expireTimer), viewData);
                switch(data.get(position).type){
                    case 0:



                        ((TextChatView)holder).date.setText(format.format(dt));
                        setMsgSeenStatus(((TextChatView)holder).msgStatus,data.get(position).msgseen);
                        break;
                    case 1:
                        ((ChatImageView) holder).aroundImage.setMediaType(data.get(pos).type);
                        setMsgSeenStatus(((ChatImageView)holder).msgStatus,data.get(position).msgseen);
                        ((ChatImageView)holder).date.setText(format.format(dt));

                       /*if(data.get(pos).msg.equals("null"))
                            ((ChatImageView)holder).chatMsg.setVisibility(GONE);
                        else
                            ((ChatImageView)holder).chatMsg.setText(StringUtils.makeHashTagText(mContext,data.get(position).msg,true));*/

                        Log.e("GGG","chatimageo final url is "+Uri.parse(data.get(position).final_url)+"\n");
                        Log.e("GGG","chatimageo temp_url url is "+    Uri.parse(data.get(position).temp_url));
                        Log.e("GGG","chatimageo seeen is "+"\n ms seen "+data.get(position).msgseen);
                        if(data.get(position).msgseen==0) {

                            ((ChatImageView) holder).aroundImage.setSizeText(StringUtils.mediaSizeToString(data.get(pos).mediaSize));
                           /* Glide.with(mContext).load(Uri.parse(data.get(position).temp_url))
                                    .into(((ChatImageView) holder).chatImage);*/

                            ((ChatImageView) holder).chatImage.setImageURI(Uri.parse(data.get(position).temp_url));

                            /*GlideApp.with(mContext).load(data.get(position).temp_url)
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .into(((ChatImageView) holder).chatImage);*/

                            //uncomment this CODE

                            ((ChatImageView)holder).aroundImage.setOnFileUploadListener(new ChatSentListner() {
                                @Override
                                public void onChatAdded(GroupChatData data) {

                                }

                                @Override
                                public void onChatUploaded(GroupChatData data) {
                                    //data.get(pos).final_url=data.get(pos).temp_url;

                                    ((ChatImageView) holder).aroundImage.setLocallyAvailable(true);
                                }
                            });

                            ((ChatImageView)holder).aroundImage.setUploadClickListner(data.get(position).final_url,data.get(pos)
                                    ,data.get(position).media_id, FirebaseStorage.getInstance().getReference().child("chat_datas").child(data.get(pos).userid));


                        }
                        else {


                            Log.e("GGG","chatimage final url is "+Uri.parse(data.get(position).final_url)+" / "+AroundStaticInfo.checkUriExist(mContext,Uri.parse(data.get(position).final_url)));
                            //((ChatImageView) holder).chatImage.setImageURI(Uri.parse(data.get(position).temp_url));

                            //((ChatImageView) holder).chatImage.layout(0, 0, 0, 0);
                            //MediaManager.AIsetPicFaster(((ChatImageView) holder).chatImage,data.get(position).temp_url);
                            //if(AroundStaticInfo.checkUriExist(Uri.parse(data.get(position).final_url))){
                                Glide.with(mContext).load(Uri.parse(AroundStaticInfo.checkUriExist(mContext,Uri.parse(data.get(position).final_url))?data.get(position).final_url:data.get(position).temp_url))
                                    .into(((ChatImageView) holder).chatImage);
                           // }                                    else
                            //{
                            //    ((ChatImageView) holder).chatImage.setImageURI(Uri.parse(data.get(position).temp_url));
                            //}

                            //MediaManager.AIsetPicFaster((ImageView) ((ChatImageView) holder).chatImage,data.get(position).final_url);
                           /* GlideApp.with(mContext).load(data.get(position).final_url)
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .into(((ChatImageView) holder).chatImage);*/

                            //CODE 47
                            ((ChatImageView)holder).aroundImage.disableDownloadClick();
                        }

                        ((ChatImageView)holder).chatImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view)
                            {
                                openMediaViewer(data.get(pos));
                            }
                        });

                        /*((AroundDownloadView) holder.itemView.findViewById(R.id.uploadView)).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ((AroundDownloadView) holder.itemView.findViewById(R.id.uploadView)).startDownload();
                            }
                        });*/
                        break;

                    case 2:
                        setMsgSeenStatus(((AudioView)holder).msgStatus,data.get(position).msgseen);
                        if(data.get(position).msgseen==0) {

                            ((AudioView)holder).contentSection.setLocallyAvailable(false);


                            ((AudioView)holder).contentSection.setOnFileUploadListener(new ChatSentListner() {
                                @Override
                                public void onChatAdded(GroupChatData data) {

                                }

                                @Override
                                public void onChatUploaded(GroupChatData data) {
                                    //data.get(pos).final_url=data.get(pos).temp_url;

                                    ((AudioView)holder).contentSection.setLocallyAvailable(true);
                                }
                            });
                            ((AudioView)holder).contentSection.setUploadClickListner(data.get(position).final_url,data.get(pos)
                                    ,data.get(position).media_id, FirebaseStorage.getInstance().getReference().child("chat_datas").child(FirebaseAuth.getInstance().getUid()));


                        }
                        else
                        {
                            ((AudioView)holder).contentSection.setLocallyAvailable(true);
                        }
                        break;
                    case 3:
                    case 5:


                        ((ChatImageView) holder).aroundImage.setMediaType(data.get(pos).type);
                        setMsgSeenStatus(((ChatImageView)holder).msgStatus,data.get(position).msgseen);
                        ((ChatImageView)holder).date.setText(format.format(dt));
/*
                        if(data.get(pos).msg.equals("null"))
                            ((ChatImageView)holder).chatMsg.setVisibility(GONE);
                        else
                            ((ChatImageView)holder).chatMsg.setText(StringUtils.makeHashTagText(mContext,data.get(position).msg,true));

*/
                        Log.e("MGG","final url is "+Uri.parse(data.get(position).final_url));
                        Log.e("MGG","show url is "+Uri.parse(data.get(position).temp_url));

                        // ((ChatImageView) holder).chatImage.setImageURI(Uri.parse(data.get(position).temp_url));

                        Glide.with(mContext).load(Uri.parse(data.get(position).temp_url))
                                .into(((ChatImageView) holder).chatImage);

                        if(data.get(position).msgseen==0/*final_url.contains("https") || data.get(position).final_url.contains("null")*/) {



                            ((ChatImageView) holder).aroundImage.setSizeText(StringUtils.mediaSizeToString(data.get(pos).mediaSize));
                            ((ChatImageView)holder).aroundImage.setUploadClickListner(data.get(position).temp_url,data.get(pos)
                                    ,data.get(position).media_id,mainStorageReference.child("chat_datas").child(userdetail.getUid()));


                            ((ChatImageView) holder).aroundImage.setLocallyAvailable(false);

                            ((ChatImageView)holder).aroundImage.setOnFileUploadListener(new ChatSentListner() {
                                @Override
                                public void onChatAdded(GroupChatData data) {

                                }

                                @Override
                                public void onChatUploaded(GroupChatData data) {
                                    ((ChatImageView) holder).aroundImage.setLocallyAvailable(true);
                                }
                            });
                        }
                        else
                        {

                            ((ChatImageView)holder).aroundImage.setLocallyAvailable(true);
                        }

                        ((ChatImageView)holder).chatImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view)
                            {
                                openMediaViewer(data.get(pos));
                            }
                        });

                        break;
                    case 4:
                        setMsgSeenStatus(((LocationView)holder).msgStatus,data.get(position).msgseen);





                        break;

                    case 6:
                        Log.e("MGG","filed name is "+data.get(pos).final_url);
                        String ext=MediaManager.getFileExtention(data.get(pos).final_url);
                        ((DocumentView)holder).sizeText.setText(ext+" | "+StringUtils.mediaSizeToString(data.get(pos).mediaSize));
                        setMsgSeenStatus(((DocumentView)holder).msgStatus,data.get(position).msgseen);
                        ((DocumentView)holder).date.setText(format.format(dt));
                        ((DocumentView)holder).indicationText.setText("."+ext);
                        ((DocumentView)holder).documentName.setText(MediaManager.getFileName(data.get(pos).final_url));

                        if(data.get(position).msgseen==0) {

                            ((DocumentView)holder).contentSection.setLocallyAvailable(false);

                            ((DocumentView)holder).contentSection.setUploadClickListner(data.get(position).final_url,data.get(pos)
                                    ,data.get(position).media_id, FirebaseStorage.getInstance().getReference().child("chat_datas").child(FirebaseAuth.getInstance().getUid()));

                            ((DocumentView)holder).contentSection.setOnFileUploadListener(new ChatSentListner() {
                                @Override
                                public void onChatAdded(GroupChatData data) {

                                }

                                @Override
                                public void onChatUploaded(GroupChatData data) {
                                    //data.get(pos).final_url=data.get(pos).temp_url;

                                    ((DocumentView)holder).contentSection.setLocallyAvailable(true);
                                    notifyItemChanged(pos);
                                }
                            });
                        }
                        else {
                            ((DocumentView)holder).contentSection.setLocallyAvailable(true);
                            ((DocumentView)holder).chatBody.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    MediaManager.openAnyFile(mContext,data.get(pos).final_url);
                                }
                            });
                        }
                        //((DocumentView)holder).documentName.setText();
                        break;
                    case 7:
                        setMsgSeenStatus(((ContactView)holder).msgStatus,data.get(position).msgseen);

                }
                break;
            case 1:



                refreshScheduleData((TimerWithTextView)holder.itemView.findViewById(R.id.msgScheduler),viewData);
                refreshExpireData(mContext, (TimerWithTextView)holder.itemView.findViewById(R.id.expireTimer), viewData);

                holder.itemView.findViewById(R.id.bubbleLayout).getBackground().setColorFilter(mContext.getResources().getColor(R.color.chatreceive_bubble), PorterDuff.Mode.SRC_IN);
                switch(data.get(position).type){
                    case 0:


                        //((TextChatView)holder).chatMsg.setText(StringUtils.makeHashTagText(mContext,data.get(position).msg,true));
                        ((TextChatView)holder).date.setText(format.format(dt));
                        //refreshScheduleData( ((TextChatView)holder).msgScheduler,viewData);
                        //refreshExpireData( mContext,((TextChatView)holder).expireTimer,viewData);


                        break;
                    case 1:
                        ((ChatImageView) holder).aroundImage.setMediaType(data.get(pos).type);
                        ((ChatImageView)holder).date.setText(format.format(dt));

                  /*      if(data.get(pos).msg.equals("null"))
                            ((ChatImageView)holder).chatMsg.setVisibility(GONE);
                        else
                            ((ChatImageView)holder).chatMsg.setText(StringUtils.makeHashTagText(mContext,data.get(position).msg,true));
                        //((ChatImageView) holder).background.getBackground().setColorFilter(ContextCompat.getColor(mContext, R.color.blue_500), PorterDuff.Mode.SRC_IN);
*/


                        Log.e("MSGG","final url is CC : "+data.get(position).final_url);
                        Log.e("MSGG","temp url is CC : "+data.get(position).temp_url);

                        if(data.get(position).final_url.contains("https")) {

                            ((ChatImageView) holder).aroundImage.setSizeText(StringUtils.mediaSizeToString(data.get(pos).mediaSize));
                            ((ChatImageView) holder).chatImage.setImageURI(Uri.parse(data.get(position).temp_url));
                            //CODE 47

                            Log.e("GGG","autodownload "+"trueee1");
                           ((ChatImageView)holder).aroundImage.setOnJobCompletionListner(new JobCompletionListner() {
                                @Override
                                public void onJobCompletion(String text1) {


                                    DatabaseManager.updateMediaTableFromGlobalUrl(data.get(pos).final_url,text1);

                                    data.get(pos).final_url=text1;
                                    //((TextChatView)holder).aroundImageView.disableDownloadClick();
                                    notifyItemChanged(pos)

                                    ;


                                }
                            });
                            ((ChatImageView)holder).aroundImage.setDownloadClickListner(data.get(position).final_url,TabbedActivity.PREF_IMAGE);


                        }
                        else {

                            //((ChatImageView) holder).chatImage.layout(0, 0, 0, 0);
                            Glide.with(mContext).load(Uri.parse(AroundStaticInfo.checkUriExist(mContext,Uri.parse(data.get(position).final_url))?data.get(position).final_url:data.get(position).temp_url))
                                    .into(((ChatImageView) holder).chatImage);
                            //((ChatImageView) holder).chatImage.setImageURI(Uri.parse(data.get(position).final_url));

                            //CODE 47
                            ((ChatImageView)holder).aroundImage.setLocallyAvailable(true);
                        }

                        ((ChatImageView)holder).chatImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(mContext.getApplicationContext(), MediaViewer.class);
                                intent.putExtra("userID",userdata.user_id);
                                intent.putExtra("mediaID",data.get(pos).media_id);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                               /* ActivityOptionsCompat options = ActivityOptionsCompat
                                        .makeSceneTransitionAnimation(act,view, "robot");*/
                                mContext.startActivity(intent);
                            }
                        });
                        break;
                    case 3:
                    case 5:
                        ((ChatImageView) holder).aroundImage.setMediaType(data.get(pos).type);
/*
                        if(data.get(pos).msg.equals("null"))
                            ((ChatImageView)holder).chatMsg.setVisibility(GONE);
                        else
                            ((ChatImageView)holder).chatMsg.setText(StringUtils.makeHashTagText(mContext,data.get(position).msg,true));
*/
                        //setUpVideoView(((ChatImageView)holder),data.get(pos).sender,data.get(pos));
                        Glide.with(mContext).load(Uri.parse(data.get(position).temp_url))
                                .into(((ChatImageView) holder).chatImage);

                        ((ChatImageView)holder).date.setText(format.format(dt));
                        if(data.get(position).final_url.contains("https") || data.get(position).final_url.contains("null")) {


                            ((ChatImageView) holder).aroundImage.setSizeText(StringUtils.mediaSizeToString(data.get(pos).mediaSize));
                            ((ChatImageView) holder).chatImage.setImageURI(Uri.parse(data.get(position).temp_url));

                            ((ChatImageView)holder).aroundImage.setOnJobCompletionListner(new JobCompletionListner() {
                                @Override
                                public void onJobCompletion(String text1) {

                                    DatabaseManager.updateMediaTableFromGlobalUrl(data.get(pos).final_url,text1);
                                    data.get(pos).final_url=text1;
                                    notifyItemChanged(pos);

                                }
                            });
                            ((ChatImageView)holder).aroundImage.setDownloadClickListner(data.get(position).final_url,data.get(pos).type);


                        }
                        else {

                            Glide.with(mContext).load(Uri.parse(data.get(position).final_url))
                                    .into(((ChatImageView) holder).chatImage);

                            ((ChatImageView)holder).aroundImage.setLocallyAvailable(true);
                        }

                        ((ChatImageView)holder).chatImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(mContext.getApplicationContext(), MediaViewer.class);
                                intent.putExtra("userID",userdata.user_id);
                                intent.putExtra("mediaID",data.get(pos).media_id);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                mContext.startActivity(intent);
                            }
                        });

                        break;

                    case 2:

                        if(data.get(position).final_url.contains("https")) {
                            ((AudioView)holder).contentSection.setDownloadClickListner(data.get(position).final_url);
                            ((AudioView)holder).contentSection.setOnJobCompletionListner(new JobCompletionListner() {
                                @Override
                                public void onJobCompletion(String text1) {

                                    DatabaseManager.updateMediaTableFromGlobalUrl(data.get(pos).final_url,text1);
                                    data.get(pos).final_url=text1;
                                    notifyItemChanged(pos)

                                    ;
                                }
                            });
                        }
                        else {
                            ((AudioView)holder).contentSection.setLocallyAvailable(true);
                        }
                        break;


                    case 6:
                        Log.e("MGG","filed name is "+data.get(pos).final_url);
                        String ext=MediaManager.getFileExtention(data.get(pos).final_url);
                        ((DocumentView)holder).sizeText.setText(ext+" | "+StringUtils.mediaSizeToString(data.get(pos).mediaSize));
                        //setMsgSeenStatus(((DocumentView)holder).msgStatus,data.get(position).msgseen);
                        ((DocumentView)holder).date.setText(format.format(dt));
                        ((DocumentView)holder).indicationText.setText("."+ext);
                        ((DocumentView)holder).documentName.setText(MediaManager.getFileName(data.get(pos).final_url));


                        if(data.get(position).final_url.contains("https")) {
                            ((DocumentView)holder).contentSection.setDownloadClickListner(data.get(position).final_url);
                            ((DocumentView)holder).contentSection.setOnJobCompletionListner(new JobCompletionListner() {
                                @Override
                                public void onJobCompletion(String text1) {

                                    DatabaseManager.updateMediaTableFromGlobalUrl(data.get(pos).final_url,text1);
                                    data.get(pos).final_url=text1;
                                    notifyItemChanged(pos);
                                }
                            });
                        }
                        else {
                            ((DocumentView)holder).contentSection.setLocallyAvailable(true);
                            ((DocumentView)holder).chatBody.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    MediaManager.openAnyFile(mContext,data.get(pos).final_url);
                                }
                            });
                        }
                        break;
                    case 7:
                        //setMsgSeenStatus(((ContactView)holder).msgStatus,data.get(position).msgseen);
                        //((ContactView)holder).date.setText(format.format(dt));
                        //((ContactView)holder).contactName.setText(StringUtils.getNameFromVcard(data.get(pos).msg));
                        break;
                }
                break;
        }



    }

    void setUpVideoView(ChatImageView holder, int sender, GroupChatData data2)
    {
        if(sender==0)
            setMsgSeenStatus(((ChatImageView)holder).msgStatus,data2.msgseen);

        ((ChatImageView) holder).aroundImage.setMediaType(data2.type);
        //((ChatImageView)holder).date.setText(format.format(dt));

       /* if(data2.msg.equals("null"))
            ((ChatImageView)holder).chatMsg.setVisibility(GONE);
        else
            ((ChatImageView)holder).chatMsg.setText(data2.msg);*/
        ((ChatImageView) holder).chatImage.setImageURI(Uri.parse(data2.temp_url));
    }

    void addLocationToDB(final LocationData temp,int pos)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder( mContext);

        builder.setCancelable(true);

        if(!checkLocationExists(temp)) {
            builder.setMessage(StringUtils.makeHashTagText(mContext,"Add *"+temp.location_name+"* to your hotspot list?",true));
            builder.setPositiveButton("ADD", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    saveLocationToDatabase(mContext,temp);
                    dialog.dismiss();
                }});
            builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

        }
        else
        {
            builder.setMessage(StringUtils.makeHashTagText(mContext,"*"+temp.location_name+"* already exists in your hotspot list!",true));
            builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public int getItemViewType(int position) {
        //4-audiosent//5-audioreceive// 6-videosent//7-videoreceive//11-gifsent//12-gif receive
        // 13- filesent//14 filereceive//15-contactsent//16-contactreceive
        switch(data.get(position).sender)
        {
            case 0:
                switch(data.get(position).type)
                {
                    //0 text//1 image //2 audio //3video //4location //5gif //6 file //7contact
                    case 0:
                        return 0;
                    case 1:
                        return 2;
                    case 2:
                        return 4;
                    case 3:
                        return 2;
                    case 4:
                        return 8;
                    case 5:
                        return 11;
                    case 6:
                        return 13;
                    case 7:
                        return 15;

                }
                //return 0;

            case 1:
                switch(data.get(position).type)
                {
                    case 0:
                        return 1;
                    case 1:
                        return 3;
                    case 2:
                        return 5;
                    case 3:
                        return 3;
                    case 4:
                        return 9;
                    case 5:
                        return 12;
                    case 6:
                        return 14;
                    case 7:
                        return 16;

                }
                //return 1;

        }
        switch(data.get(position).type) {
            case 21:
            case 22:
                return 20;
        }




        return Integer.parseInt(null);
    }

    @Override
    public int getItemCount()
    {
        if(data!=null) {
            return data.size();
        }
        return 0;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        menu.clear();
        MenuInflater inflater = new MenuInflater(mContext);
        inflater.inflate(R.menu.chatwindow_pop_up, menu);
        // menu.setHeaderTitle(data.get(optionPosition).name);
        //menu.setQwertyMode(true);
        Log.e("GGG","groupchat optionPosition "+optionPosition+" / "+data.size());
        if(data.get(optionPosition).userid.equals(TabbedActivity.userdetail.getUid())) {

            menu.findItem(R.id.chat_poke).setVisible(false);
            menu.findItem(R.id.chat_message).setVisible(false);

            menu.findItem(R.id.view_profile).setTitle("My profile");
        }
        else
        {
            menu.findItem(R.id.chat_message).setTitle("Message "+(data.get(optionPosition).name));
            menu.findItem(R.id.chat_poke).setTitle("Poke "+(data.get(optionPosition).name));
        }
    }
    void setMsgSeenStatus(ImageView stat,int val)
    {
        switch (val) {
            case 0:
                stat.setImageResource(R.drawable.ic_progress_clock);
                break;
            case 1:
                stat.setImageResource(R.drawable.ic_single_tick);
                break;
            case 2:
                stat.setImageResource(R.drawable.ic_double_tick);
                break;
            case 3:
                stat.setImageResource(R.drawable.ic_double_tick_seen_primary);
                break;
        }
    }
    void refreshScheduleData(final TimerWithTextView view,final GroupChatData chatData)
    {
        if(chatData.chatSchedule==0)
            view.setVisibility(GONE);
        else if(chatData.chatSchedule<=System.currentTimeMillis())
        {
            view.setVisibility(View.VISIBLE);
            view.setImageTint(R.color.semiblack_drawable);
            view.setTextVisibility(View.VISIBLE);
            view.stopAnimation();
            view.setText("<"+StringUtils.getShortExpirationDisplayValue((int)(System.currentTimeMillis()-chatData.chatSchedule)/1000));

        }
        else
        {
            view.setVisibility(View.VISIBLE);
            view.setTextVisibility(View.VISIBLE);

            view.setExpirationTime(null,chatData.chatSchedule);

            /*new NotifyTimer().addNewListnerWithIntervel(false,chatData.chatSchedule, new TimerListner() {
                @Override
                public void onTimerEnd(Long timestamp, String time) {

                }

                @Override
                public void onSpecifiedIntervel(Long timestamp, String time) {
                    view.setText(StringUtils.getShortExpirationDisplayValueForTimestamp(chatData.chatSchedule));
                }
            });*/

        }

    }

    public static void refreshExpireData(Context context,final TimerWithTextView view,final GroupChatData chatData)
    {

        if(chatData.chatExpire==0)
            view.setVisibility(GONE);
        else
        {
            view.setVisibility(View.VISIBLE);
            view.setTextVisibility(View.VISIBLE);
            //Log.e("MSGG","chat seentime "+chatData.chat_seen_time+ " / "+(chatData.sender==0 && chatData.chat_seen_time==0));
            if(chatData.sender==0 && chatData.chat_seen_time==0 ){
                view.stopAnimation();
                view.setImageTint(R.color.green_500);
                view.setText(StringUtils.getShortExpirationDisplayValue((int)chatData.chatExpire));

            }
            else
            {
               /* view.setImageTint(R.color.red_500);
                view.setText(StringUtils.getShortExpirationDisplayValueForTimestamp(chatData.chatExpire*1000+chatData.chat_seen_time));
                Log.e("GGG","starting deletion"+((chatData.chatExpire*1000+chatData.chat_seen_time)-System.currentTimeMillis()));
                ChatSender.newMessageDeletionSchedule(mContext,chatData.chatExpire*1000+chatData.chat_seen_time,chatData.chatRow,chatData.userid);

                */
                view.setImageTint(R.color.red_500);
                /*new NotifyTimer().addNewListnerWithIntervel(false,(chatData.chatExpire*1000+chatData.chat_seen_time), new TimerListner() {
                    @Override
                    public void onTimerEnd(Long timestamp, String time) {

                    }

                    @Override
                    public void onSpecifiedIntervel(Long timestamp, String time) {
                        view.setText(StringUtils.getShortExpirationDisplayValueForTimestamp(chatData.chatExpire*1000+chatData.chat_seen_time));

                    }
                });*/
                view.setExpirationTime(null,chatData.chatExpire*1000+chatData.chat_seen_time);
                Log.e("MSGG","starting deletion"+((chatData.chatExpire*1000+chatData.chat_seen_time)-System.currentTimeMillis()));

                ChatSender.newMessageDeletionSchedule(context,chatData.chatExpire*1000+chatData.chat_seen_time,chatData.chatRow,chatData.userid);
            }
            /*view.setVisibility(View.VISIBLE);
            view.setTextVisibility(View.VISIBLE);
            view.setImageTint(R.color.red_500);
            view.setText(StringUtils.getShortExpirationDisplayValueForTimestamp(chatData.chatExpire));*/
        }
    }

    public static class TextChatView extends RecyclerView.ViewHolder {

        TimerWithTextView msgScheduler,expireTimer;

        ChatReplayLayout replaySection;
        public ImageView msgStatus;
        public EmojiTextView chatMsg;
        public TextView date;
        //public TextView username;
        RelativeLayout bubbleLayout;

        public TextChatView(View item) {
            super(item);
            bubbleLayout=item.findViewById(R.id.bubbleLayout);
            msgStatus=(ImageView)item.findViewById(R.id.statusBubble);
            chatMsg =(EmojiTextView) item.findViewById(R.id.chatText);
            date=(TextView) item.findViewById(R.id.chatDateText);
            //username=(TextView) item.findViewById(R.id.grpusername);
            msgScheduler=item.findViewById(R.id.msgScheduler);
            expireTimer=item.findViewById(R.id.expireTimer);
            replaySection=item.findViewById(R.id.replaySection);

        }
        /*void refreshExpireTimer(Context context,final GroupChatData chatData) {
            ChatDataAdapter.refreshExpireData(context, expireTimer, chatData);
        }*/
    }


    public static class ChatImageView extends RecyclerView.ViewHolder {

        public ImageView chatImage;
        public AroundChatImageThumbline aroundImage;
        public ImageView msgStatus;
        public TextView date;
        public EmojiTextView chatMsg;
        //public RelativeLayout bubbleLayout;

        TimerWithTextView msgScheduler,expireTimer;

        public ChatImageView(View item) {
            super(item);
            //bubbleLayout=item.findViewById(R.id.bubbleLayout);
            aroundImage=item.findViewById(R.id.chatMainImageView);
            chatImage=aroundImage.getImageView();
            msgStatus=(ImageView)item.findViewById(R.id.statusBubble);
            date=(TextView) item.findViewById(R.id.chatDateText);
            chatMsg =item.findViewById(R.id.chatText);
            msgScheduler=item.findViewById(R.id.msgScheduler);
            expireTimer=item.findViewById(R.id.expireTimer);

        }
    }

    public static class LocationView extends RecyclerView.ViewHolder {

        public TextView locationText;
        public TextView latitudeText;
        public ImageView msgStatus;
        public TextView date;
        public TextView addLocation,forwardLocation;
        // public TextView date;
        //public EmojiconTextView chatMsg;

        public LocationView(View item) {
            super(item);

            locationText=(TextView)item.findViewById(R.id.locationText);
            latitudeText=(TextView)item.findViewById(R.id.latitudeText);
            addLocation=(TextView)item.findViewById(R.id.addLocation);
            msgStatus=(ImageView)item.findViewById(R.id.statusBubble);
            date=(TextView) item.findViewById(R.id.chatDateText);
            forwardLocation= item.findViewById(R.id.forwardLocation);


        }
    }

    public static class ContactView extends RecyclerView.ViewHolder {

        TextView contactName;
        CircularImageView contactImage;
        TextView contactNumber,noAmount;
        public ImageView msgStatus,saveContact,shareContact;
        public TextView date,inviteUser;

        public ContactView(View item) {
            super(item);
            msgStatus=(ImageView)item.findViewById(R.id.statusBubble);
            date=(TextView) item.findViewById(R.id.chatDateText);
            contactName=item.findViewById(R.id.contactName);
            contactImage=item.findViewById(R.id.contactImage);
            contactNumber=item.findViewById(R.id.contactNumber);
            noAmount=item.findViewById(R.id.noAmount);
            saveContact=item.findViewById(R.id.saveContact);
            shareContact=item.findViewById(R.id.shareContact);
            inviteUser=item.findViewById(R.id.inviteUser);
        }
    }
    public static class AudioView extends RecyclerView.ViewHolder {


        public ImageView msgStatus;
        public TextView date,sizeText,timeIndication;
        public EmojiTextView chatSentText;
        public CircleColorImageView playPause;
        public SeekBar audioSeek;
        AroundChatAudioThumbline contentSection;
        public AudioView(View item) {
            super(item);
            msgStatus=(ImageView)item.findViewById(R.id.statusBubble);
            date=(TextView) item.findViewById(R.id.chatDateText);
            chatSentText=item.findViewById(R.id.chatText);
            sizeText=item.findViewById(R.id.sizeText);
            playPause=item.findViewById(R.id.playPause);
            audioSeek=item.findViewById(R.id.audioSeek);
            timeIndication=item.findViewById(R.id.timeIndication);
            contentSection=item.findViewById(R.id.contentSection);

        }
    }
    public static class DocumentView extends RecyclerView.ViewHolder {


        public ImageView msgStatus;
        public TextView date,sizeText,indicationText,documentName;
        public RelativeLayout chatBody;
        AroundChatFileThumbline contentSection;
        public DocumentView(View item) {
            super(item);
            msgStatus=(ImageView)item.findViewById(R.id.statusBubble);
            date=(TextView) item.findViewById(R.id.chatDateText);
            sizeText=item.findViewById(R.id.sizeText);
            indicationText=item.findViewById(R.id.indicationText);
            documentName=item.findViewById(R.id.documentName);
            chatBody=item.findViewById(R.id.bubbleLayout);
            contentSection=item.findViewById(R.id.contentSection);

        }
    }
    public static class ChatInfoView extends RecyclerView.ViewHolder {

        public TextView info;


        public ChatInfoView(View item) {
            super(item);

            info = (TextView) item.findViewById(R.id.chat_info_text);


        }
    }



}