package com.hash.around.Utils;

import android.*;
import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import java.io.File;

/**
 * Created by Febin on 5/31/2018.
 */

public class AroundStaticInfo {

    static boolean debug=false;

    public static boolean checkUriExist(Context context,Uri uri)
    {
        if(uri.toString().contains("content")) {
            String[] projection = {MediaStore.MediaColumns.DATA};
            Cursor cur = context.getContentResolver().query(uri, projection, null, null, null);
            if (cur != null) {
                if (cur.moveToFirst()) {
                    String filePath = cur.getString(0);
                    if (new File(filePath).exists()) {
                      return true;
                    }
                }
            }
            return false;
        }
        else{
        File file = new File(uri.toString());
        return file.exists();}
    }

    public static String getDefaultAudioDirectory(Context context)
    {
        File file = new File(debug?Environment.getExternalStorageDirectory().getPath():context.getExternalCacheDir().getPath(), "Around Files/Audio");
        if (!file.exists())
        {
            file.mkdirs();
        }
        return file.getAbsolutePath();
        //Uri.fromFile(file).toString();
    }
    public static String getDefaultCacheDirectory(Context context)
    {
        File file = new File(debug?Environment.getExternalStorageDirectory().getPath():context.getExternalCacheDir().getPath(), "Around Files/cache");
        if (!file.exists()) {
            file.mkdirs();
        }
        return file.getAbsolutePath();
    }
    public static String getDefaultFeedDirectory(Context context)
    {
        File file = new File(debug?Environment.getExternalStorageDirectory().getPath():context.getExternalCacheDir().getPath(), "Around Files/Feed Data");
        if (!file.exists()) {
            file.mkdirs();
        }
        return file.getAbsolutePath();
    }
    public static String getDefaultPublicFeedDirectory(Context context)
    {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "Around Files/Feed Media");
        if (!file.exists()) {
            file.mkdirs();
        }
        return file.getAbsolutePath();
    }
    public static String getDefaultChatDirectory(Context context)
    {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "Around Files/Chat Media");
        if (!file.exists()) {
            file.mkdirs();
        }
        return file.getAbsolutePath();
    }
    public static String getDefaultDocumentsDirectory(Context context)
    {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "Around Files/Documents");
        if (!file.exists()) {
            file.mkdirs();
        }
        return file.getAbsolutePath();
    }
    public static String getDefaultProfileDirectory(Context context)
    {
        File file = new File(debug?Environment.getExternalStorageDirectory().getPath():context.getExternalCacheDir().getPath(), "Around Files/Profile");
        if (!file.exists()) {
            file.mkdirs();
        }
        return file.getAbsolutePath();
    }
    public static String getDefaultOthersDirectory(Context context)
    {
        File file = new File(debug?Environment.getExternalStorageDirectory().getPath():context.getExternalCacheDir().getPath(), "Around Files/Others");
        if (!file.exists()) {
            file.mkdirs();
        }
        return file.getAbsolutePath();
    }
    public static String getDefaultPeopleNearDirectory(Context context)
    {
        File file = new File(debug?Environment.getExternalStorageDirectory().getPath():context.getExternalCacheDir().getPath(), "Around Files/People Near");
        if (!file.exists()) {
            file.mkdirs();
        }
        return file.getAbsolutePath();
    }


    public static boolean requestPermissionsOneByOne(Activity mActivity,String [] permissions,int requestCode)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            for (String perm:permissions) {


            if (mActivity.checkSelfPermission(perm) != PackageManager.PERMISSION_GRANTED) {


                ActivityCompat.requestPermissions(mActivity,new String[]{perm}, requestCode);


                return false;
            }

            }

        }

        return true;
    }

    public static void vibrateInMilliSeconds(Context mContext,long time)
    {
        Vibrator vibrator = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(time);
    }
}
