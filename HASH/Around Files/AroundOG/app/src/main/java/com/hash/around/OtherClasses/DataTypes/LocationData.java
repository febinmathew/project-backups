package com.hash.around.OtherClasses.DataTypes;

/**
 * Created by Febin on 26-08-2017.
 */
import com.hash.around.TabbedActivity;

import java.io.Serializable;

public class LocationData implements  Serializable {

    public String location_name;
    public int status;
    public Double latitude;
    public Double longitude;

    public LocationData()
    {}

    public LocationData(Double lat,Double lng,String name)
    {

    }

    public String getStringValue()
    {
        String val;
        val=location_name+ TabbedActivity.TEXT_SEPARATOR+latitude+TabbedActivity.TEXT_SEPARATOR+longitude;
        return val;
    }

}


