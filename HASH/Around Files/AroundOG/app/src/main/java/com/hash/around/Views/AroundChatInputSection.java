package com.hash.around.Views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hash.around.AroundAlertBoxManager;
import com.hash.around.ChatWindow;
import com.hash.around.OtherClasses.DataTypes.MediaData;
import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.hash.around.OtherClasses.Interfaces.ChatSchedulerListener;
import com.hash.around.OtherClasses.Interfaces.TimerListner;
import com.hash.around.R;
import com.hash.around.Utils.StringUtils;
import com.hash.around.Utils.UserManager;
import com.hash.around.Views.Medias.ChatReplayLayout;
import com.vanniktech.emoji.EmojiEditText;
import com.vanniktech.emoji.EmojiPopup;
import com.vanniktech.emoji.listeners.OnEmojiPopupDismissListener;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import static com.hash.around.Utils.AroundStaticInfo.getDefaultAudioDirectory;
import static com.hash.around.Utils.AroundStaticInfo.vibrateInMilliSeconds;



public class AroundChatInputSection extends LinearLayout {

  public AroundChatInputSection(Context context) {
    this(context, null);
  }


  public AroundChatInputSection(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  ImageButton sendMsgButton,recordMsgButton;
  AroundToggleViewFrameLayout chatToggleButton;
  ImageView emoticon,attachments;
  EmojiEditText chat_message;
  LinearLayout chatSection,audioRecordingStatus,chatExtraSection;

  RecordTimer recordTimer;

  String fileName;
  MediaRecorder mMediaRecorder;

  Context mContext;

  ImageView extraButton,extraSectionClose;//,reminderSetup;
  TimerWithTextView destroyTimer,scheduledMessage;

  public ChatReplayLayout replaySection;

  public AroundChatInputSection(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);

    mContext=context;
    if (attrs != null) {
     /* TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.SquareFrameLayout, 0, 0);

      typedArray.recycle();*/
    }
    inflate(context, R.layout.chat_inputsection_textarea, this);
    chatToggleButton=(AroundToggleViewFrameLayout)findViewById(R.id.chatToggleButton);
    audioRecordingStatus=(LinearLayout)findViewById(R.id.audioRecordingStatus);
    emoticon = (ImageView) findViewById(R.id.emotimg);
    attachments = (ImageView) findViewById(R.id.attachments);
    emoticon.setImageResource(R.drawable.ic_emoticon_semiblack);
    sendMsgButton=(ImageButton)findViewById(R.id.sendMsgButton) ;
    recordMsgButton=(ImageButton)findViewById(R.id.recordMsgButton) ;
    chat_message=(EmojiEditText)findViewById(R.id.chat_message);
    extraButton=findViewById(R.id.addExtras);
    chatExtraSection=findViewById(R.id.chatExtraSection);
    chatSection=findViewById(R.id.chatSection);
    extraSectionClose=findViewById(R.id.extraSectionClose);
    scheduledMessage=findViewById(R.id.scheduledMessage);
    destroyTimer=findViewById(R.id.destroyTimer);
    replaySection=findViewById(R.id.replaySection);
    //reminderSetup=findViewById(R.id.reminderSetup);
    updateToggleButton();
  }

  public ImageView getAtachmentView()
  {
    return attachments;
  }
  public EmojiEditText getEditTextView()
  {
    return chat_message;
  }
  public ImageButton getSendMsgButton()
  {
    return sendMsgButton;
  }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        initViews();
    }

    public UserWholeData userData;
  public void updateExpireTime(int expirationTime)
  {
    UserManager.updateUserChatDestroAndSchedule(userData.user_id,expirationTime,null);
    refreshExpireTime();

  }

  void refreshExpireTime()
  {
    Log.e("MGG","updating down");
    if(userData.chat_expiretime >0) {
      destroyTimer.setImageTint(R.color.red_500);
      destroyTimer.setText(StringUtils.getShortExpirationDisplayValue(userData.chat_expiretime));
      destroyTimer.setTextVisibility(View.VISIBLE);
    }else{
      destroyTimer.setImageTint(R.color.semiblack_drawable);
      destroyTimer.setTextVisibility(View.GONE);
    }
  }
  public void updateScheduleTime(long expirationTime)
  {
    UserManager.updateUserChatDestroAndSchedule(userData.user_id,null,expirationTime);
    refreshScheduleTime();
  }
  void refreshScheduleTime()
  {
    Log.e("MGG","updating down");
    if(userData.chat_scheduletime >System.currentTimeMillis()) {
      scheduledMessage.setImageTint(R.color.green_dark);
      scheduledMessage.setText(StringUtils.getShortExpirationDisplayValueForTimestamp(userData.chat_scheduletime));
      scheduledMessage.setTextVisibility(View.VISIBLE);

      scheduledMessage.setExpirationTime(null,userData.chat_scheduletime);
      scheduledMessage.setTimerListener(new TimerListner() {
        @Override
        public void onTimerEnd(Long timestamp, String time) {
          refreshScheduleTime();
        }

        @Override
        public void onSpecifiedIntervel(Long timestamp, String time) {

        }
      });
    }else{
      scheduledMessage.setImageTint(R.color.semiblack_drawable);
      scheduledMessage.setTextVisibility(View.GONE);
    }

  }
public void setUserData(UserWholeData userDataArg)
{
  this.userData=userDataArg;
  refreshExpireTime();
  refreshScheduleTime();
}
/*public void setReplayLayoutData(String heading,String description,Uri uri)
{
  if(uri==null)
     replaySection.hideImageThumbnail();
  else
    replaySection.setImageUri(uri);

  replaySection.setHeadingAndDescription(heading,description);
  replaySection.show();
}*/
int lastMSG=0;
  public void initViews()
  {
    Log.e("GGG","keypress initialize");

    if(PreferenceManager.getDefaultSharedPreferences(getContext()).getBoolean("pref_chat_enter_send",false))
    {
      chat_message.setImeOptions(EditorInfo.IME_ACTION_SEND);
      chat_message.setRawInputType(InputType.TYPE_CLASS_TEXT);
    }

    chat_message.setOnKeyListener(new OnKeyListener() {
      @Override
      public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
          if (keyCode == KeyEvent.KEYCODE_ENTER) {

            sendMsgButton.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER));
            sendMsgButton.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_ENTER));
              return true;

          }
        }
        return false;
      }
    });

    chat_message.setOnEditorActionListener(new TextView.OnEditorActionListener() {
      @Override
      public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEND) {
          sendMsgButton.performClick();
          return true;
        }
        return false;
      }
    });
   //userData=ChatWindow.instance.userData;
    final View root=getRootView(); //changes here final View root=this.findViewById(android.R.id.content).getRootView();
    final EmojiPopup emojiPopup = EmojiPopup.Builder.fromRootView(root).setOnEmojiPopupDismissListener(new OnEmojiPopupDismissListener() {
      @Override
      public void onEmojiPopupDismiss() {
        emoticon.setImageResource(R.drawable.ic_emoticon_semiblack);
      }
    }).build(chat_message);

    chat_message.addTextChangedListener(new TextWatcher() {

      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        //lastMSG=s;
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        Log.e("GGG","keypres defference is "+lastMSG+" now "+s.length());
        //if(lastMSG!=s.length()){
        //chat_message.postDelayed(ChatWindow.this::updateToogleButton, 50);
        updateToggleButton();
       //   }


        StringUtils.makeHashTagTextForEditText(getContext(),chat_message.getText(),false);
       // Spannable str =

        /*int endOfString = chat_message.getText().toString().length();

        str.setSpan(new StyleSpan(Typeface.ITALIC), 0, endOfString, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);*/

      }

      @Override
      public void afterTextChanged(Editable s) {

      }
    });

    emoticon.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        // emoticon.setImageResource(R.drawable.ic_keyboard_semiblack);
        Log.e("MSGG","show status : "+emojiPopup.isShowing());

        if(emojiPopup.isShowing())
        {
          emoticon.setImageResource(R.drawable.ic_emoticon_semiblack);
          Log.e("MSGG","chnaged emoji: "+emojiPopup.isShowing());
        }
        else {
          emoticon.setImageResource(R.drawable.ic_keyboard_semiblack);
          Log.e("MSGG","chnaged keyboard: "+emojiPopup.isShowing());
        }

        emojiPopup.toggle(); // Toggles visibility of the Popup.
        // emojiPopup.dismiss(); // Dismisses the Popup.

      }
    });

    extraButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        Log.e("MGG","extraButton");
        showExtraWindow();

      }
    });
    extraSectionClose.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {

        hideExtraWindow();
      }
    });
    destroyTimer.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        AroundAlertBoxManager.showChatExpireDialog(getContext(), 5, new AroundAlertBoxManager.OnClickListener() {
          @Override
          public void onClick(int expirationTime) {

            if(ChatWindow.instance!=null)
            ChatWindow.instance.updateExpireTime(expirationTime);
            updateExpireTime(expirationTime);
          }
        });
        hideExtraWindow();
      }
    });

    /*reminderSetup.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        //AroundAlertBoxManager.showReminderDialog(getContext());
        AroundAlertBoxManager.showConversationMuteDialog(getContext(),null);
        Log.e("MGG","clicked on reminer");
      }
    });*/

    scheduledMessage.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        //AroundAlertBoxManager.showChatSchedulerDiolog((Activity)getContext());
       AroundAlertBoxManager.showChatSchedulerDiolog(getContext(), new ChatSchedulerListener() {
         @Override
         public void onSchedule(Long timestamp) {
           if(ChatWindow.instance!=null)
             ChatWindow.instance.updateSchedule(timestamp);
           updateScheduleTime(timestamp);
         }
       });
        hideExtraWindow();
      }
    });



    //sheduledMessage.setOnClickListener();

    recordTimer=new RecordTimer((TextView) findViewById(R.id.record_time));
    chatToggleButton.setListner(new AroundToggleViewFrameLayout.AudioRecordistener() {
      @Override
      public void onRecordPressed(float x) {

        vibrateInMilliSeconds(mContext,40);

        //if(requestPermissionsOneByOne((Activity)getContext(),new String[]{Manifest.permission.RECORD_AUDIO},15)) {
          //((Activity)getContext()).getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
          updateRecordingstatus(true);
          recordTimer.display();
          startRecording();

          /*if (getContext() instanceof Activity) {
           // Window window = ;*/

          //}
      }



      @Override
      public void onRecordReleased(float x) {
        stopRecording();
        long timelapse=recordTimer.hide();
        Log.e("MGG","timelpase "+timelapse);
        updateRecordingstatus(false);

        ((Activity)getContext()).getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if(timelapse>1000) {
          Intent broadcast = new Intent("around.NEWCHAT");
          ArrayList<MediaData> finalData = new ArrayList<>();
          MediaData dat = new MediaData();
          dat.type = 2;
          dat.uri = Uri.fromFile(new File(fileName)).toString();
          Log.e("GGG","uricalssi:1 "+new File(fileName).toURI().toString()+"\n"+Uri.fromFile(new File(fileName))+"\n"+new File(fileName).getAbsolutePath()+"\n"+new File(fileName).getPath());
          finalData.add(dat);
          broadcast.putExtra("mediaarray", finalData);
          getContext().sendBroadcast(broadcast);
        }
        else
        {
          if(fileName!=null)
          {
            File file=new File(fileName);
            file.delete();
          }
        }
        fileName=null;
      }

      @Override
      public void onRecordCanceled(float x) {
        vibrateInMilliSeconds(getContext(),50);
        stopRecording();
        updateRecordingstatus(false);
        ((Activity)getContext()).getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if(fileName!=null)
        {
          File file=new File(fileName);
          file.delete();
        }
        //lastRecord=null;
        fileName=null;
      }


      private void startRecording() {
        fileName = getDefaultAudioDirectory(getContext());
        //lastRecord=randomStringGenerator();
        fileName += "/"+ StringUtils.randomStringGenerator()+".mp3";
        Log.e("GGG","audiooo file ocation "+fileName);

        mMediaRecorder=new MediaRecorder();
        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mMediaRecorder.setOutputFile(fileName);
        mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);

        try {
          mMediaRecorder.prepare();
        } catch (IOException e) {
          //Log.e(LOG_TAG, "prepare() failed");
        }

        mMediaRecorder.start();

      }

      void stopRecording()
      {
        if(mMediaRecorder!=null) {
          try {
            mMediaRecorder.stop();
            mMediaRecorder.release();
          }
          catch (Exception s)
          {          }
          finally {
            mMediaRecorder = null;
          }

        }
      }

      @Override
      public void onRecordMoved(float x, float absoluteX) {

      }

      @Override
      public void onRecordPermissionRequired() {

      }
    });
    chatToggleButton.setUpRecordListner();

  }
  void showExtraWindow()
  {
    ViewFunctions.fadeIn(chatExtraSection);
    ViewFunctions.fadeOut(chatSection);

    ViewFunctions.animateButtonBurstIn(scheduledMessage,50,false);
    ViewFunctions.animateButtonBurstIn(destroyTimer,100,false);
    //ViewFunctions.animateButtonBurstIn(reminderSetup,150,false);

  }
  void hideExtraWindow()
  {
    ViewFunctions.fadeOut(chatExtraSection);
    ViewFunctions.fadeIn(chatSection);
  }

  void updateRecordingstatus(boolean isRecording)
  {
    if(isRecording)
    {
      Log.e("MGG","startingggggg NOW");
      ViewFunctions.fadeIn(audioRecordingStatus);
      //ViewFunctions.fadeOut(chatSection);
      //ViewFunctions.scaleView(chatToggleButton,1,2);

    }
    else
    {    recordTimer.hide();
      //recordMsgButton.setOnTouchListener(null);
      Log.e("MGG","stoppin FNOW");
      ViewFunctions.fadeOut(audioRecordingStatus);
      //ViewFunctions.fadeIn(chatSection);
    }
  }
  void updateToggleButton()
  {
    Log.e("GGG","TEXT CHANGE FNOW");
   // SpannableStringBuilder temp=StringUtils.makeHashTagText(getContext(),chat_message.getText().toString(),false);
    //lastMSG=temp.toString().length();
   // chat_message.setText(temp);

    //xxx
    String chatText=chat_message.getText().toString();
    if(chatText.trim().length()==0)
    {
      chatToggleButton.display(recordMsgButton);
      Log.e("GGG","maxline1 chat ");
      chat_message.setMaxLines(1);

    }
    else
    {
      Log.e("GGG","maxline4 chat ");
      chat_message.setMaxLines(4);
      chatToggleButton.display(sendMsgButton);
      //chat_message.setSingleLine(false);
    }
  }

  public static class RecordTimer implements Runnable {

    private TextView recordTimeView=null;
    private final AtomicLong startTime = new AtomicLong(0);
    String combiner;
    TimerListner listner;
    Long limit=null,intervel=null;
    boolean stop=false;
    boolean reverse=false;

    public RecordTimer(TextView recordTimeView) {
      this.recordTimeView = recordTimeView;
    }
    public RecordTimer(TextView recordTimeView,String combineText) {
      this.recordTimeView = recordTimeView;
      this.combiner=combineText;
    }
    public RecordTimer() {
    }

    public void display() {
      stop=false;
      this.startTime.set(System.currentTimeMillis());
      if(recordTimeView!=null)
        this.recordTimeView.setText(DateUtils.formatElapsedTime(0));

      //.runOnMainDelayed(this, TimeUnit.SECONDS.toMillis(1));
      new Handler(Looper.getMainLooper()).postDelayed(this, 5);
    }

    public long hide() {
      long elapsedtime = System.currentTimeMillis() - startTime.get();
      this.startTime.set(0);
      stop=true;
      return elapsedtime;
    }

    public void setTimerListner(@Nullable Long limit, @Nullable Long intervel, TimerListner listner)
    {
      this.listner=listner;
      this.limit=limit;
    }

    public void setReverse(boolean reverse)
    {
      this.reverse=reverse;
    }
    void setCombiber(String comb)
    {
      this.combiner=comb;
    }

    String getformatedStringOutput()
    {
      String time;
      if(reverse)
        time=DateUtils.formatElapsedTime(TimeUnit.MILLISECONDS.toSeconds(limit-getElapseTime()));
      else
        time=DateUtils.formatElapsedTime(TimeUnit.MILLISECONDS.toSeconds(getElapseTime()));

      if(combiner!=null)
        return String.format(combiner,time);
      else
        return time;
    }

    long getElapseTime()
    {
      return System.currentTimeMillis() - startTime.get();
    }

    long getRemainningTime()
    {
      long localStartTime = startTime.get();
      if(reverse)
        return limit-(System.currentTimeMillis() - localStartTime);
      else
        return System.currentTimeMillis() - localStartTime;
    }

    @Override
    public void run() {
      long localStartTime = startTime.get();
      if (localStartTime > 0) {
        long elapsedTime =System.currentTimeMillis() - localStartTime;

        if(recordTimeView!=null)
        recordTimeView.setText(getformatedStringOutput());
        if(listner!=null)
          listner.onSpecifiedIntervel(getRemainningTime(),getformatedStringOutput());
        if(limit!=null)
        {
          if(elapsedTime>limit)
          {
            if(listner!=null)
            {
              listner.onTimerEnd(getRemainningTime(),getformatedStringOutput());
            }
            stop=true;
          }
        }
        if(!stop)
        new Handler(Looper.getMainLooper()).postDelayed(this, 5);
      }
    }
  }

}
