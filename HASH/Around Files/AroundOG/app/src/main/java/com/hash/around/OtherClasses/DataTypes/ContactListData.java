package com.hash.around.OtherClasses.DataTypes;

/**
 * Created by Febin on 26-08-2017.
 */
import java.io.Serializable;

public class ContactListData implements  Serializable {

    public String user_id;
    public String user_name;
    public String number;
    public int type;
    public String location; //NEW ENTRY
    public double latitude; //NEW ENTRY
    public double longitude; //NEW ENTRY
    public String status;
    public long status_timestamp;
    public String img_url;
    public String music_url="null";
    public long music_timestamp;

    public ContactListData()
    {

    }
    public ContactListData(ContactListData data)
    {
        this.user_id=data.user_id;
        this.user_name=data.user_name;
                this.number=data.number;
                this.type=data.type;
                this.location=data.location;
                this.latitude=data.latitude;
                this.longitude=data.longitude;
                this.status=data.status;
                this.img_url=data.img_url;
                this.music_url=data.music_url;
                this.status_timestamp=data.status_timestamp;
        this.music_timestamp=data.music_timestamp;
    }

}



