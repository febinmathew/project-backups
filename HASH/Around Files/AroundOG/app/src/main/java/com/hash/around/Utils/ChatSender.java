package com.hash.around.Utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.hash.around.Broadcasts.AroundBroadcastReceiver;
import com.hash.around.ChatWindow;
import com.hash.around.OtherClasses.DataTypes.ChatData;
import com.hash.around.OtherClasses.DataTypes.ChatReplayData;
import com.hash.around.OtherClasses.DataTypes.FeedData;
import com.hash.around.OtherClasses.DataTypes.GroupChatData;
import com.hash.around.OtherClasses.DataTypes.GroupFeedData;
import com.hash.around.OtherClasses.DataTypes.MediaData;
import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.hash.around.OtherClasses.Interfaces.ChatSentListner;
import com.hash.around.OtherClasses.Interfaces.FileUploadListner;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.R;

import java.util.ArrayList;

import static android.content.Context.ALARM_SERVICE;
import static com.hash.around.TabbedActivity.mTabbedContext;
import static com.hash.around.TabbedActivity.userdetail;
import static com.hash.around.Utils.MediaManager.uploadResourceAndReturnUrls;

/**
 * Created by Febin on 6/12/2018.
 */

/*
* features
* sent text messsages
*/
public class ChatSender {

    static DatabaseReference chatDatabaseRef=null;
    static DatabaseReference groupChatDatabaseRef=null;
    static StorageReference chatStorageRef=null;


    static public DatabaseReference getChatDatabaseRef() {
        if(chatDatabaseRef==null)
            chatDatabaseRef = FirebaseDatabase.getInstance().getReference().child("chat_data");
        return chatDatabaseRef;
    }



    public static DatabaseReference getGroupChatDatabaseRef() {
        if(groupChatDatabaseRef==null)
            groupChatDatabaseRef=FirebaseDatabase.getInstance().getReference().child("chat_group");//.child(uId).child("chats")
        return groupChatDatabaseRef;
    }

    public static StorageReference getChatStorageRef() {
        if(chatStorageRef==null)
            chatStorageRef= FirebaseStorage.getInstance().getReference().child("chat_datas").child(FirebaseAuth.getInstance().getUid());
        return chatStorageRef;
    }
    public static void newMessageSchedule(Context context,long timestamp,long chatRow,String userID,int usertype)
    {
        PendingIntent pendingIntent;
        Intent myIntent = new Intent(context, AroundBroadcastReceiver.class);
        myIntent.setAction("com.hash.around.MESSAGE_SCHEDULE");
        myIntent.putExtra("chatrow",chatRow);
        myIntent.putExtra("usertype",usertype);
        myIntent.putExtra("userid",userID);
        pendingIntent = PendingIntent.getBroadcast(context, (int)System.currentTimeMillis(), myIntent,PendingIntent.FLAG_ONE_SHOT);

        AlarmManager alarmManager = (AlarmManager)context.getSystemService(ALARM_SERVICE);

        if (Build.VERSION.SDK_INT >= 23) {
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP,
                    timestamp, pendingIntent);
        } else if (Build.VERSION.SDK_INT >= 19) {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, timestamp, pendingIntent);
        } else {
            alarmManager.set(AlarmManager.RTC_WAKEUP, timestamp, pendingIntent);
        }

        //alarmManager.set(AlarmManager.RTC, timestamp, pendingIntent);
        //Log.e("MGG","BroadCAST SET "+(timestamp-System.currentTimeMillis()));
    }
    public static void newMessageDeletionSchedule(Context context,long timestamp,long chatRow,String userID)
    {
        PendingIntent pendingIntent;
        Intent myIntent = new Intent(context, AroundBroadcastReceiver.class);
        myIntent.setAction("com.hash.around.MESSAGE_SCHEDULE");
        myIntent.putExtra("chatrow",chatRow);
        myIntent.putExtra("usertype",2); //temp value not used
        myIntent.putExtra("userid",userID);
        myIntent.putExtra("intent_type",1);
        pendingIntent = PendingIntent.getBroadcast(context, (int)timestamp, myIntent,PendingIntent.FLAG_ONE_SHOT);

        AlarmManager alarmManager = (AlarmManager)context.getSystemService(ALARM_SERVICE);

        if (Build.VERSION.SDK_INT >= 23) {
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP,
                    timestamp, pendingIntent);
        } else if (Build.VERSION.SDK_INT >= 19) {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, timestamp, pendingIntent);
        } else {
            alarmManager.set(AlarmManager.RTC_WAKEUP, timestamp, pendingIntent);
        }

        //alarmManager.set(AlarmManager.RTC, timestamp, pendingIntent);
        ////Log.e("GGG","BroadCAST SET "+(timestamp-System.currentTimeMillis()));
    }

    public static void updateChatWindow(Context context,int type,long rowID,String userID,Integer chatData)
    {
        //////Log.e("GGG","gotttttt the pos : "+pos);
        if(ChatWindow.uId!=null)
        {////Log.e("GGG","uid OK ");
            if(ChatWindow.uId.equals(userID))
            {////Log.e("GGG","userID OK ");

                Integer pos;
                switch (type)
                {
                    case 0://seen status updation
                         pos=updateChatWindowExtention_getPositionInFinalData(ChatWindow.finaldata,rowID);
                       Log.e("GGG","gotttttt the pos : "+pos);
                        if(pos!=null)
                        {
                            ChatWindow.finaldata.get(pos).msgseen=chatData;
                            ChatWindow.adap.notifyItemChanged(pos);
                            //ChatWindow.adap.notifyDataSetChanged();
                        }
                        break;
                    case 1:
                        Log.e("GGG","notifi=ying dataset chaged the pos : "+ChatWindow.finaldata.size());
                        ChatWindow.adap.notifyDataSetChanged();
                        Log.e("GGG","notifi=ying dataset chaged the pos : "+ChatWindow.finaldata.size());
                        break;
                    case 2://deletion

                        pos=updateChatWindowExtention_getPositionInFinalData(ChatWindow.finaldata,rowID);
                        //Log.e("GGG","removing "+pos);
                        if(pos!=null)
                        {
                            //Log.e("GGG","final datasize "+ChatWindow.finaldata.size());
                            ChatWindow.finaldata.remove((int)pos);
                            ChatWindow.adap.notifyItemRemoved(pos);
                            //ChatWindow.adap.notifyDataSetChanged();
                            //Log.e("GGG","final datasize "+ChatWindow.finaldata.size());

                        }

                        break;

                }




            }
        }
    }

    public static void updateChatWindowHEAVY(String userID)
    {
        if(ChatWindow.uId!=null)
        {
            if(ChatWindow.uId.equals(userID))
            {

                NotifyTimer.removeAllListner();
                ChatWindow.instance.getcontentfromdatabase();   //heavy code Do Somethings
                ChatWindow.adap.notifyDataSetChanged();      //


            }
        }
    }

    public static void updateChatLastSeen(String userID,Long time)
    {
        if(ChatWindow.uId!=null)
        {
            if(ChatWindow.uId.equals(userID))
            {

                ChatWindow.instance.setLastSeenTimestamp(time!=null?time:0);//System.currentTimeMillis()+2);   //heavy code Do Somethings

            }
        }
    }

   static Integer updateChatWindowExtention_getPositionInFinalData(ArrayList<GroupChatData> data,long rowID)
    {
        for(int i=data.size()-1;i>=0;i--)
        {
            //Log.e("GGG","checking "+data.get(i).chatRow+" with "+rowID);
            if(data.get(i).chatRow==rowID)
                return i;
        }
        return null;
    }
    static long millischange=0;
    public static void sentTextMessage(Context context, int type, String message, ChatReplayData replayData, final UserWholeData userData, final ChatSentListner listener, final boolean byPassDB)
    {

        final GroupChatData x=new GroupChatData();
        x.replay_id=1;
        x.chatSchedule=userData.chat_scheduletime;
        x.chatExpire=userData.chat_expiretime;
        x.userid=userData.user_id;
        x.sender=0;
        x.type=type;
        x.timestamp= System.currentTimeMillis();
        updateChatLastSeen(userData.user_id,0L);
        x.msg=message;
        x.msgseen=0;
        x.temp_url="null";
        x.final_url="null";
        x.media_id=1;

        x.chatReplay=replayData;

        if(x.chatReplay!=null) {
            x.replay_id = DatabaseManager.insertChatReplaysToDatabase(x.chatReplay);
            Log.e("GGG","sending repkay is "+replayData.userName+" / "+replayData.description+" / "+
                    replayData.type+" / "+replayData.contentUri);
        }
        else
        {
            Log.e("GGG","sending repkay is NULL");
        }
if (!byPassDB) {

    if (userData.type == 15 || userData.type == 16) {
        x.chatRow = x.sl = DatabaseManager.saveChatToDatabase(context, x, userdetail.getUid());
        x.name = context.getString(R.string.chat_you);
    } else {
        x.chatRow = x.sl = DatabaseManager.saveChatToDatabase(context, x, null);
    }
    Log.e("GGG", "row is " + x.sl);
}
else{
    x.chatRow=0;
    userData.chat_scheduletime=0;
    x.chatReplay=null;
}

        if(listener!=null)
            listener.onChatAdded(x);

        if(userData.chat_scheduletime>System.currentTimeMillis())
        {
            newMessageSchedule(mContext,userData.chat_scheduletime,x.sl,userData.user_id,userData.type);
            ////Log.e("MSG","updated x time is 1 "+x.timestamp);
            return;
        }

        uploadReplay(x.chatReplay,new JobCompletionListner() {
            @Override
            public void onJobCompletion(String txt1) {
                x.replay_url=txt1;
                uploadTextData(x,userData.user_id,userData.type,listener,byPassDB);
            }
        });
    }


    public static void uploadReplay( ChatReplayData replay,final JobCompletionListner listner)
    {

        if(replay==null)
        {
            if(listner!=null)
                listner.onJobCompletion("null");

            return;
        }
        byte[] arr=ByteUtils.serialize(replay);
        Log.e("GGG","chatreplay upload is is>> "+replay.contentUri);
        //ChatReplayData newreplay;//=new ChatReplayData();
        //newreplay= (ChatReplayData)ByteUtils.deserialize(arr);
        //Log.e("GGG","decerialized is "+newreplay.description);
        if(arr!=null)
            MediaManager.uploadByteFileAndReturnUrl(mContext, arr,getChatStorageRef() , new FileUploadListner() {
                @Override
                public void onFileUploaded(String global, String globalLite, String liteLocal) {
                    //Log.e("GGG","replay upload complete");
                    if (listner!=null)
                    listner.onJobCompletion(global);
                }

                @Override
                public void onProgressListener(int percent) {

                }

                @Override
                public void onFailureListener(int percent) {

                }

                @Override
                public void onPausedListener(int percent) {

                }
            });
    }

    public static void uploadTextData(final GroupChatData x, final String userID, final int userType, final ChatSentListner listener, final boolean byPassDB)
    {
        //Log.e("MSG","updated x.chatSchedule 1 "+x.chatSchedule);


        if(x.chatSchedule>0)
            x.chatSchedule=x.timestamp;
        else
            x.chatSchedule=0;



        final String fUser=UserManager.getUserInfo().getUid();
        x.userid=fUser;
        DatabaseReference singeChatRef;
        if(userType==15||userType==16) {
            singeChatRef = getGroupChatDatabaseRef().child(userID).child("chats").push();
        }
        else {
            singeChatRef = getChatDatabaseRef().child(userID).push();
        }
        final long val=x.chatRow;

        //Log.e("MGG","rowrow "+x.sl+" / "+x.chatRow);
        //String key= chatRef.getKey();
        singeChatRef.setValue(new ChatData(x)).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid)
            {
                //x.sl;
                DatabaseManager.updateSelfChatstatus(val,1);
                if(userType==15||userType==16) {

                    DatabaseReference databaseRef = getGroupChatDatabaseRef()
                            .child(userID).child("chat_status");
                    databaseRef.child(fUser).child("last_rece_time").setValue(x.timestamp);
                    databaseRef.child(fUser).child("last_seen_time").setValue(x.timestamp);


                    //getGroupChatsFromDatabase();
                }
                else
                {
                    //getUserChatsFromDatabase();
                }

                if(listener!=null)
                    listener.onChatUploaded(x);

                if(!byPassDB) {
                    //setUpMediaPlayer(mContext);
                    if(mp!=null && PrefManager.getBooleanPreference(mContext,"pref_chat_sent_sound",true))
                     mp.start();
                    updateChatWindow(null, 0, val, userID, 1);
                }

            }
        });
    }

   static void setUpMediaPlayer(Context context)
    {
        if(mp==null)
            mp= MediaPlayer.create(context, R.raw.msgsent6);
    }
    /*public void uploadChat(ChatData x, final @Nullable ProcessCompletedListner listner)
    {
        databaseRef.push().setValue(x).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {

                    if(listner!=null)
                        listner.processCompleteAction();
                }
            }
        });
    }*/

    static Context mContext;
    static MediaPlayer mp =null;
    public static void AIMessageSender(Context context,int type,String fileUri,String message,ChatReplayData replayData, final UserWholeData userData, final ChatSentListner listener,boolean byPassDB)
    {
       // Log.e("GGG","mediasize AI starting "+type+" / "+fileUri+" / "+message+" / "+userData.user_id);



        setUpMediaPlayer(context);

        if(userData.chat_scheduletime<System.currentTimeMillis())
            userData.chat_scheduletime=0;
       // if(userData.chat_expiretime>0)
            //userData.chat_expiretime=0;
        mContext=context;
        if(fileUri==null)
        {
            sentTextMessage(context,type,message,replayData,userData,listener,byPassDB);
        }
        else
        {
            Log.e("GGG","intent new image sending \n"+type+"\n"+fileUri+"\n"+message+"\n"+userData.user_id+"\n");
            sentMediaMessage(context,type,fileUri,message,userData,listener);
        }
    }

    /*public static void AIMessageForwardSender(Context context,ArrayList<ChatData> data,ArrayList<ChatReplayData> replayData, final ArrayList<String> userID, final ChatSentListner listener)
    {
       for (int i = 0;i<userID.size(); i ++) {



        for (int k = 0; k < data.size(); k++) {

            ChatData x = new ChatData();
            x.type = data.get(k).type;
            x.timestamp = System.currentTimeMillis();
            x.msg = data.get(k).msg;
            x.temp_url = data.get(k).temp_url;
            x.final_url = data.get(k).final_url;

            x.userid = userdetail.getUid();

            final long val = x.sl;

            ChatSender.AIMessageForwardSender(context,x,replayData.get(k),userID.get(i),null);
        }
    }

    }*/



    public static void AIMessageForwardSender(Context context,GroupChatData data,ChatReplayData replayData, final String userID, final ChatSentListner listener)
    {       Log.e("GGG","forward sent initiated");

    if(!data.msg.equals(""))
        replayData=null;

        UserWholeData userData=ChatWindow.returnSingleUserDetails(context,userID);
        sentForwardMessage(context,data,replayData,userData,null);
    }


    public static void AIMessageForwardSender(Context context, GroupFeedData data, final String userID, final ChatSentListner listener,boolean byPassDB)
    {
        UserWholeData userData=ChatWindow.returnSingleUserDetails(context,userID);
        ChatReplayData repl=ChatReplayData.getReplayDataFromFeedData(data);
        AIMessageSender(context,0,null,"",repl,userData,null,byPassDB);
    }

    public static void sentForwardMessage(Context context,final GroupChatData x, ChatReplayData replayData,final UserWholeData userData, final ChatSentListner listener)
    {
        //final GroupChatData x=new GroupChatData();
        x.msgseen=0;
        x.replay_id=1;
        //x.chatSchedule=userData.chat_scheduletime;
        //x.chatExpire=userData.chat_expiretime;
        x.userid=userData.user_id;
        x.sender=0;
        //x.type=type;
        x.timestamp= System.currentTimeMillis();
        updateChatLastSeen(userData.user_id,0L);
        //millischange+=millischange;
        //x.msg=message;
        x.msgseen=0;
        x.temp_url="null";
        x.final_url="null";
        //x.type=0;

        //x.media_id=1;

        Log.e("GGG","row forward media id is "+x.media_id);
        if(x.media_id==1){

        }
        else
        {
            MediaData mData=MediaManager.returnMediaFromRow(context,x.media_id);
            if(mData!=null){
            x.temp_url=mData.globalLiteUri;
            x.final_url=mData.globalUri;
            }
            else
                x.media_id=1;
        }
        Log.e("GGG","row forward media id NOW IS  "+x.media_id);

        x.chatReplay=replayData;

        if(x.chatReplay!=null) {
            x.replay_id = DatabaseManager.insertChatReplaysToDatabase(x.chatReplay);
            Log.e("GGG","sending repkay is "+replayData.userName+" / "+replayData.description+" / "+
                    replayData.type+" / "+replayData.contentUri);
        }
        else
        {
            Log.e("GGG","sending repkay is NULL");
        }



        if(userData.type==15||userData.type==16)
        {
            //x.userid = userdetail.getUid();
            x.chatRow=x.sl = DatabaseManager.saveChatToDatabase(context,x,userdetail.getUid());
        }
        else
        {
            x.chatRow=x.sl=DatabaseManager.saveChatToDatabase(context,x,null);
        }
        Log.e("GGG","row is " +x.sl);




        if(listener!=null)
            listener.onChatAdded(x);

        if(userData.chat_scheduletime>System.currentTimeMillis())
        {
            newMessageSchedule(mContext,userData.chat_scheduletime,x.sl,userData.user_id,userData.type);
            ////Log.e("MSG","updated x time is 1 "+x.timestamp);
            return;
        }

        uploadReplay(x.chatReplay,new JobCompletionListner() {
            @Override
            public void onJobCompletion(String txt1) {
                x.replay_url=txt1;
                uploadTextData(x,userData.user_id,userData.type,listener,false);
            }
        });
    }


    public static void sentMediaMessage(Context context,int type,final String fileUri,String message, final UserWholeData userData, final ChatSentListner listener)
    {
        final GroupChatData x=new GroupChatData();

        Log.e("GGG","mediasize mediapost starting ");
        x.replay_id=1;
        x.userid=userData.user_id;
        x.chatSchedule=userData.chat_scheduletime;
        x.chatExpire=userData.chat_expiretime;
        x.sender=0;
        x.type=type;
        x.timestamp= System.currentTimeMillis();
        updateChatLastSeen(userData.user_id,0L);

        x.msg=message;
        x.msgseen=0;
        try {
            x.temp_url=MediaManager.getLiteLocalMediaFromOrginalMedia(context,fileUri,type);
            Log.e("GGG","chatlistimage upload image is "+x.temp_url);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "Media file not supported", Toast.LENGTH_LONG).show();
            Log.e("GGG","received file : "+e.getMessage()+" "+e.toString());
            return;
        }
        String globalUri="null"+StringUtils.randomStringGenerator();
        if(type==1||type==3||type==5)
        x.final_url=fileUri;//globalUri;
        else
        x.final_url=fileUri;
        try {
            Log.e("GGG","mediasize start");
            x.mediaSize=MediaManager.getMediaSize(context,Uri.parse(fileUri));
            Log.e("GGG","mediasize is : "+x.mediaSize);
        } catch (Exception e) {
            Log.e("GGG","mediasize error : "+Uri.parse(fileUri)+" /\n "+e.toString());
            e.printStackTrace();
        }

        ////Log.e("MGG","added CC : "+fileUri);

        final long mediaPosition=DatabaseManager.insertMediaToDatabase(context,globalUri,fileUri,globalUri, x.temp_url, x.mediaSize);

        Log.e("MGG","inserted media at : "+mediaPosition);

        x.media_id=mediaPosition;

        if(x.chatReplay!=null) {
            x.replay_id = DatabaseManager.insertChatReplaysToDatabase(x.chatReplay);
           }

        if(userData.type==15||userData.type==16)
        {
            x.chatRow=x.sl = DatabaseManager.saveChatToDatabase(context,x,userdetail.getUid());
            x.name=context.getString(R.string.chat_you);
        }
        else {
            x.chatRow=x.sl = DatabaseManager.saveChatToDatabase(context, x, null);
        }
        Log.e("GGG","mediapost inserted at "+x.sl);
       // Log.e("GGG","chatimage uploadComplete "+ x.temp_url+"\n"+ x.final_url);
        if (listener!=null)
            listener.onChatAdded(x);

        ////Log.e("MGG","PPreeeeee2 final url is "+Uri.parse(x.final_url));
        if(userData.chat_scheduletime>System.currentTimeMillis())
        {
            newMessageSchedule(mContext,userData.chat_scheduletime,x.sl,userData.user_id,userData.type);
            return;
        }
        Log.e("GGG","mediapost "+x.temp_url+"\n"+x.final_url);
        uploadReplay(x.chatReplay,new JobCompletionListner() {
            @Override
            public void onJobCompletion(String txt1) {
                x.replay_url=txt1;
                Log.e("GGG","mediapost starting upload");
                uploadMediaData(fileUri,new GroupChatData(x),userData,listener,null);
                //uploadTextData(x,userData.user_id,userData.type,listener);
            }
        });


    }

    public static void uploadMediaData( final String fileUri, final GroupChatData x, final UserWholeData userDat,final ChatSentListner listener,final FileUploadListner fileUploadListner)
    {

        if(x.chatSchedule>0)
            x.chatSchedule=x.timestamp;

        //Log.e("GGG","chatimage uploadComplete "+ x.temp_url);
        //Log.e("MGG","PPreeeeee5 final url is "+Uri.parse(x.final_url));
        uploadResourceAndReturnUrls(mContext,x.temp_url,x.type, fileUri,getChatStorageRef(),new FileUploadListner() {
            @Override
            public void onFileUploaded(String global, String globalLite,final String liteLocal) {

                if(fileUploadListner!=null)
                    fileUploadListner.onFileUploaded(global, globalLite, liteLocal);

                DatabaseManager.updateMediaInDatabase(x.media_id,global,fileUri,globalLite,liteLocal);

                //ChatData temp=;
                // temp= data.get(pos);
                final String outFinalData=x.temp_url;
                final String target=x.userid;
                x.userid=UserManager.getUserInfo().getUid();
                x.final_url=global;
                x.temp_url=globalLite;
                final long val=x.chatRow;
                //Log.e("MGG","PPreeeeee6 final url is "+Uri.parse(x.final_url));
                Log.e("GGG","mediapost Sucess upload media");
                UserWholeData targetData;
                if(userDat==null)
                    targetData=ChatWindow.returnSingleUserDetails(mContext,x.userid);
                else
                    targetData=userDat;

                final int targetType=targetData.type;

                uploadChatFromAnyWhere(targetData.user_id, targetData.type,new ChatData(x), new ChatSentListner() {
                    @Override
                    public void onChatAdded(GroupChatData data) {}

                    @Override
                    public void onChatUploaded(GroupChatData data) {

                        //Log.e("MGG","pre final url is "+Uri.parse(x.final_url));
                        DatabaseManager.updateSelfChatstatus(x.sl,1);

                        if(targetType==15||targetType==16) {

                            DatabaseReference databaseRef = getGroupChatDatabaseRef()
                                    .child(target).child("chat_status");
                            databaseRef.child(UserManager.getUserInfo().getUid()).child("last_rece_time").setValue(x.timestamp);
                            databaseRef.child(UserManager.getUserInfo().getUid()).child("last_seen_time").setValue(x.timestamp);


                            //getGroupChatsFromDatabase();
                        }

                        if(listener!=null) {
                            //x.final_url
                            ////Log.e("MGG","PPreeeeee4 final url is "+Uri.parse(x.final_url));

                            x.temp_url=liteLocal;
                            x.final_url=fileUri;
                           // Log.e("GGG","chatimage uploadComplete "+ x.temp_url);
                            listener.onChatUploaded(x);


                        }
                        if(mp!=null && PrefManager.getBooleanPreference(mContext,"pref_chat_sent_sound",true))
                            mp.start();
                        updateChatWindow(null,0,val,target,1);
                    }});
            }

            @Override
            public void onProgressListener(int percent) {
                if(fileUploadListner!=null)
                    fileUploadListner.onProgressListener(percent);
            }

            @Override
            public void onFailureListener(int percent) {
                if(fileUploadListner!=null)
                    fileUploadListner.onFailureListener(percent);

            }

            @Override
            public void onPausedListener(int percent) {
                if(fileUploadListner!=null)
                    fileUploadListner.onPausedListener(percent);
            }
        });
    }

    public static void uploadChatFromAnyWhere(String userID,int type,final ChatData data, final @Nullable ChatSentListner listner)
    {
        DatabaseReference singeChatRef;
        if(type==15||type==16) {
            singeChatRef = getGroupChatDatabaseRef().child(userID).child("chats").push();
        }
        else {
            singeChatRef = getChatDatabaseRef().child(userID).push();
        }

        singeChatRef.setValue(data).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {

                    if(listner!=null)
                        listner.onChatUploaded(null);
                }
            }
        });

    }


}