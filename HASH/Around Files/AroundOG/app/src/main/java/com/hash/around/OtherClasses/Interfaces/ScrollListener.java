package com.hash.around.OtherClasses.Interfaces;

import com.hash.around.OtherClasses.DataTypes.UserPrivacyData;

public interface ScrollListener {
    void onUserStatusReceieved(int pos,int scrollX, int scrollY, int oldScrollX, int oldScrollY);
}


