package com.hash.around;

import android.Manifest;
import android.app.Activity;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.hash.around.OtherClasses.Adapter_Contacts;
import com.hash.around.OtherClasses.DataTypes.ChatData;
import com.hash.around.OtherClasses.DataTypes.ContactListData;
import com.hash.around.OtherClasses.DataTypes.GroupFeedData;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.OtherClasses.DataTypes.GroupChatData;
import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.hash.around.Utils.AppOnlineStatusManager;
import com.hash.around.Utils.ChatSender;
import com.hash.around.Utils.DatabaseManager;
import com.hash.around.Utils.NetworkUtils;
import com.hash.around.Utils.PermissionManager;
import com.hash.around.Utils.UserManager;
import com.hash.around.Views.ViewFunctions;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;



import static com.hash.around.TabbedActivity.fDatabase;

import static com.hash.around.TabbedActivity.saveContactToDatabase;
import static com.hash.around.TabbedActivity.userdetail;

public class AllUsersList extends AppCompatActivity {


    DatabaseReference newPostRef;
    ChildEventListener childEventListener;
    public ArrayList<ContactListData> generateDummy()
    {
        ArrayList<ContactListData> list=new ArrayList<ContactListData>();


        return list;
    }

    RecyclerView rView;
    Adapter_Contacts adap;
    ArrayList<UserWholeData> finaldata;
    private LinearLayoutManager mLayoutManager;

    ArrayList<GroupChatData> forwardchatData;

    boolean chatforwardActivity=false;
    boolean dataBackwardActivity=false;
    FloatingActionButton contactFloatButton;
    LinearLayout newGroup,newChannel;
    TextView toolbarStatus,extraTag;
    Spinner userTypeSpinner;
    ArrayList<String> alredySelected;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.all_users_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.findViewById(R.id.up_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AllUsersList.super.onBackPressed();
            }
        });
        newGroup=findViewById(R.id.newGroup);
        newGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),CreateNewGroup.class);
                intent.putExtra("type",15);
                startActivity(intent);
            }
        });
        newChannel=findViewById(R.id.newChannel);
        newChannel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),CreateNewGroup.class);
                intent.putExtra("type",16);
                startActivity(intent);
            }
        });
        toolbarStatus=findViewById(R.id.toolbarStatus);

        extraTag=findViewById(R.id.extraTag);
        userTypeSpinner=(Spinner)findViewById(R.id.userTypeSpinner);
        userTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                /*   String result = getResources().getStringArray(R.array.user_types_values)[position];
                   int value=Integer.parseInt(result);
                   getuserListFromDB(getApplicationContext(),value);*/
                fetchFromDataBaseAi();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




        Bundle args = getIntent().getBundleExtra("BUNDLE");
        if(args!=null) {
            int forwardType;
            //ArrayList<GroupFeedData>
            chatforwardActivity=true;
            /*forwardType=getIntent().getIntExtra("forwardType",0);
            if(forwardType==1)
            {*/
                forwardchatData = (ArrayList<GroupChatData>) args.getSerializable("ARRAYLIST");
           /* }
            else if (forwardType==2)
            {
                forwardchatData = (ArrayList<GroupChatData>) args.getSerializable("ARRAYLIST");
            }
            else*/
                //chatforwardActivity=false;


        }

        else if(getIntent().getBooleanExtra("returnData",false))
        {
            alredySelected=(ArrayList<String>)getIntent().getSerializableExtra("selectedpeople");
            dataBackwardActivity=true;
        }

        contactFloatButton=(FloatingActionButton) findViewById(R.id.contactFloatButton);

        if(chatforwardActivity) {

            contactFloatButton.setImageResource(R.drawable.ic_send_white);
            contactFloatButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (int i = adap.selectedUsers.size() - 1; i >= 0; i--) {

                        //DatabaseReference chatRef = FirebaseDatabase.getInstance().getReference().child("chat_data").child(finaldata.get(adap.selectedUsers.keyAt(i)).user_id).push();

                        for (int k = 0; k < forwardchatData.size(); k++) {

                            /*if(forwardchatData.get(k).numberType==0)
                            {
                                ChatSender.sentTextMessage(forwardchatData.get(k).chatMsg,)
                            }
                            else
                            {

                            }*/
                            //x.userid = finaldata.get(adap.selectedUsers.keyAt(i)).user_id;
                            /*GroupChatData x = new GroupChatData();

                            x.sender = 0;
                            x.msgseen = 0;
                            x.type = forwardchatData.get(k).type;
                            x.timestamp = System.currentTimeMillis();
                            x.msg = forwardchatData.get(k).msg;
                            x.temp_url = forwardchatData.get(k).temp_url;
                            x.final_url = forwardchatData.get(k).final_url;
                            //x.sl =  MainServiceThread.mainService.saveChatToDatabase(x,null);

                            x.userid = userdetail.getUid();

                            //final long val = x.sl;*/

                            ChatSender.AIMessageForwardSender(getApplicationContext(),forwardchatData.get(k),forwardchatData.get(k).chatReplay,finaldata.get(adap.selectedUsers.keyAt(i)).user_id,null);
                            //String key= chatRef.getKey();
                            /*chatRef.setValue(x).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    DatabaseManager.updateSelfChatstatus(val, 1);

                                }
                            });*/
                        }
                    }

                    adap.selectedUsers.clear();
                    //adap.notifyDataSetChanged();
                    finish();
                }
            });
        }
        else if(dataBackwardActivity)
        {
            newChannel.setVisibility(View.GONE);
            newGroup.setVisibility(View.GONE);
            userTypeSpinner.setVisibility(View.GONE);

            contactFloatButton.setImageResource(R.drawable.ic_exit_white);
            contactFloatButton.getDrawable().setColorFilter(ContextCompat.getColor(getApplicationContext(),R.color.white), PorterDuff.Mode.SRC_IN);
            contactFloatButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ArrayList<String> grpuser=new ArrayList<String>();
                    for (int i = adap.selectedUsers.size() - 1; i >= 0; i--) {
                        grpuser.add(finaldata.get(adap.selectedUsers.keyAt(i)).user_id);
                    }

                    adap.selectedUsers.clear();


                    Intent intent=new Intent();
                    Bundle args = new Bundle();
                    args.putSerializable("ARRAYLIST",(Serializable)grpuser);
                    intent.putExtra("BUNDLE",args);

                    setResult(Activity.RESULT_OK,intent);
                    finish();
                }
            });
        }
        else
        {
            contactFloatButton.setImageResource(R.drawable.fab_add);
            contactFloatButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
                    intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
                    startActivity(intent);
                }
            });
        }

        rView=(RecyclerView)findViewById(R.id.rView);
        rView.setNestedScrollingEnabled(false);
       // rView.setHasFixedSize(true);
        //cView=(CardView)rootView.findViewById(R.id.cView);
        mLayoutManager=new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);
        //  mLayoutManager=new GridLayoutManager(this,2,LinearLayoutManager.HORIZONTAL,false);
        // mLayoutManager=new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);
        //mLayoutManager.setStackFromEnd(true);
        rView.setLayoutManager(mLayoutManager);

         finaldata=new ArrayList<UserWholeData>();



        adap=new Adapter_Contacts(AllUsersList.this, finaldata, false,chatforwardActivity || dataBackwardActivity, new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                setFloatButtonVisibility();
            }
        });
        rView.setAdapter(adap);

        //getuserListFromDB(getApplicationContext(),0);
        fetchFromDataBaseAi();
        updateContent();
        setFloatButtonVisibility();


        IntentFilter intentFilter = new IntentFilter("CONTACTS_UPDATED");
        registerReceiver(contactUpdateReceiver, intentFilter);
    }
    @Override
    protected void onStart() {
        super.onStart();
        AppOnlineStatusManager.OnlineStatus.setActive(true);

    }
    @Override
    public void onStop() {
        AppOnlineStatusManager.OnlineStatus.setActive(false);
        super.onStop();
    }
    private BroadcastReceiver contactUpdateReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            fetchFromDataBaseAi();
        }
    };


    void fetchFromDataBaseAi()
    {
        String result = getResources().getStringArray(R.array.user_types_values)[userTypeSpinner.getSelectedItemPosition()];
        int value=Integer.parseInt(result);

        getuserListFromDB(getApplicationContext(),value);

        int user=R.plurals.contact_amount;
        switch (value)
        {
            case 2:
                user=R.plurals.contact_amount;
                break;
            case 3:
                user=R.plurals.following_amount;
                break;
            case 11:
                user=R.plurals.blocked_amount;
                break;
            case 15:
                user=R.plurals.groups_amount;
                break;
            case 16:
                user=R.plurals.channels_amount;
                break;
        }
        extraTag.setText(getApplicationContext().getResources().getQuantityString(user, finaldata.size(),  finaldata.size()));
    }

    void setFloatButtonVisibility()
    {
        if(dataBackwardActivity || chatforwardActivity) {
           // getSupportActionBar().setDisplayShowTitleEnabled(true);
            toolbarStatus.setVisibility(View.VISIBLE);
            if(adap.selectedUsers.size()>0){
               // getSupportActionBar().setTitle(adap.selectedUsers.size()+" selected");
                toolbarStatus.setText(adap.selectedUsers.size()+" "+getApplicationContext().getString(R.string.selected_tag));
            if(contactFloatButton.getVisibility()==View.GONE)
                ViewFunctions.independantAnimateIn(contactFloatButton,getApplicationContext());
            }
                else{
                //getSupportActionBar().setTitle("Select people");
                toolbarStatus.setText(R.string.select_people);
                if(contactFloatButton.getVisibility()==View.VISIBLE)
                    ViewFunctions.independantAnimateOut(contactFloatButton,getApplicationContext());
            }


        }
         else {
            if(contactFloatButton.getVisibility()==View.GONE)
           ViewFunctions.independantAnimateIn(contactFloatButton,getApplicationContext());
        }
    }


    void getuserListFromDB(Context context, int num)
    {


        finaldata.clear();
        SQLiteDatabase db = DatabaseManager.getDatabase(context).getReadableDatabase();

        Cursor cursor = null;

        if(num==3) {
            Log.e("GGG","followfollow22 "+num+" / / "+ "1");
            cursor = db.query(DatabaseAccesser.user_table,
                   null,
                    DatabaseAccesser.user_user_follow_type+" =?",
                    new String[]{ "1"},
                    null, null, null, null);
        }
        else  if(num==11) {
            cursor = db.query(DatabaseAccesser.user_table,
                    null,
                    DatabaseAccesser.user_user_follow_type+" =?",
                    new String[]{"11"},
                    null, null, null, null);
        }
        else
        {
            cursor = db.query(DatabaseAccesser.user_table,
                    null,
                    DatabaseAccesser.user_user_type + " =?",
                    new String[]{""+num},
                    null, null, null, null);
        }
int count=0;
        while (cursor.moveToNext()) {


            UserWholeData temp = new UserWholeData();

            temp.user_id = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_user_id));
            if(dataBackwardActivity)
            {
                if(alredySelected.contains(temp.user_id))
                {

                    adap.selectedUsers.put(count,true);
                }
            }
            temp.user_name = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_name));
            temp.type=cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_user_type));
            Log.e("GGG","alluserfollow "+temp.type);
            temp.status = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_status));
            temp.img_url = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_localUrl));
            temp.priv_profile_picture = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_priv_profile_picture_status));
            temp.remoteContact = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_remote_contact));

            temp.followType = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_user_follow_type));
            temp.remoteBlock = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_remote_block));
            temp.blockType = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_user_block_type));
            Log.e("GGG","followfollow "+num+" / / "+ temp.followType);

            Log.e("MSGG","retrivrd with prof status : "+ temp.priv_profile_picture);

            //Toast.makeText(getActivity(), temp.uri, Toast.LENGTH_SHORT).show();
            // long itemId =
            finaldata.add(temp);
            count++;
        }
        cursor.close();

        adap.notifyDataSetChanged();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==PermissionManager.CONTACTS_PERMISSION_CODE && resultCode==RESULT_OK)
        {
            updateContent();
        }
    }


    void updateContent() {
        if (!PermissionManager.checkContactsPermission(this)) {

                return;

        }
       /* Intent intent = new Intent("GET_CONTACTS_UPDATED");
        sendBroadcast(intent);*/
        Log.e("MSGG","=starting ");
        /*fetchUserDetailsbyIDOrPhone(phoneNumberValidation("91(974)4 02-09-58"), false, new ProcessCompletedListner() {
            @Override
            public void processCompleteAction() {

            }

            @Override
            public void newUserAdded(ContactListData data) {
                Log.e("MSGG","=utimate "+data);
            }
        });*/
    }


    String phoneNumberValidation(String num)
    {
        return num.replaceAll("[()\\s-]+","");
    }

   /* @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 15:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //locationok();

                }
                break;
        }
    }*/


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(contactUpdateReceiver);
//        newPostRef.removeEventListener(childEventListener);
    }


    void upadte_content() {


         newPostRef=fDatabase.child("user_profile");

         childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                final ContactListData post = dataSnapshot.getValue(ContactListData.class);
                SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();

                Cursor cursor = db.rawQuery(" select " + DatabaseAccesser.user_user_id + " from " + DatabaseAccesser.user_table + " where " +
                        DatabaseAccesser.user_user_id + " =? ", new String[]{post.user_id + ""});

                if (cursor.getCount() == 0) {

                    StorageReference httpsReference = FirebaseStorage.getInstance().getReferenceFromUrl(post.img_url);
                    httpsReference.getBytes(1024 * 1024).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                        @Override
                        public void onSuccess(byte[] bytes) {
                            // Data for "images/island.jpg" is returns, use this as needed

                            //String output = ;

                            try {

                                File file = new File(Environment.getExternalStorageDirectory().getPath(), "Around Files/Profile Pictures");
                                if (!file.exists()) {
                                    file.mkdirs();
                                }
                                //File tempFile = new File(file.getAbsolutePath(),"contact.vcf");

                                String name = post.user_name + ".png";
                                FileOutputStream fos = new FileOutputStream(new File(file.getAbsolutePath(), name));
                                fos.write(bytes);
                                fos.close();

                                //Toast.makeText(getActivity(),  , Toast.LENGTH_SHORT).show();

                                String globalUrl= post.img_url;
                                post.img_url = file.toURI().toString() + name;


                                finaldata.add(0,(UserWholeData) post);
                                adap.notifyItemInserted(0);

                                // adap.notifyItemChanged(finaldata.size() - 1);
                                // rView.scrollToPosition(finaldata.size() - 1);

                                saveContactToDatabase((UserWholeData) post,globalUrl,true);


                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            // Handle any errors
                        }
                    });
                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };


//                      Toast.makeText(getActivity(), "hurray", Toast.LENGTH_SHORT).show();
                // ...

      //  newPostRef.addChildEventListener(childEventListener);

    }


    /*void saveContactToDatabase(ContactListData post,String globalUrl)
    {
        SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();


        ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.user_user_id, post.user_id);
        values.put(DatabaseAccesser.user_name, post.user_name);
        values.put(DatabaseAccesser.user_status, post.status);
        if(post.user_id.equals(userdetail.getUid()))
        {
            values.put(DatabaseAccesser.user_user_type, 0);
        }
        else
        {
            values.put(DatabaseAccesser.user_user_type, post.numberType);
        }
        values.put(DatabaseAccesser.user_globalUrl,globalUrl);
        values.put(DatabaseAccesser.user_localUrl, post.img_url);



        long newRowId = db.insert(DatabaseAccesser.user_table, null, values);
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.all_user_activity, menu);
        MenuItem sItem=menu.findItem(R.id.action_search_alluser);
        SearchView searchView =
                (SearchView) sItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return true;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                searchOnList(newText);
                return false;
            }
        });

        if(dataBackwardActivity||chatforwardActivity)
        {
            menu.findItem(R.id.action_new_group).setVisible(false);
            menu.findItem(R.id.action_new_channel).setVisible(false);
            menu.findItem(R.id.action_add_new_contact).setVisible(false);
        }

        return super.onCreateOptionsMenu(menu);

    }

    void searchOnList(String searchTerm)
    {
        if(searchTerm.equals(""))
        {
            adap.updateDataList(finaldata);
            adap.notifyDataSetChanged();
        return;
        }

        ArrayList<UserWholeData> temp=new ArrayList<>();
        for(int i=finaldata.size()-1;i>=0;i--)
        {
            if(finaldata.get(i).user_name.toLowerCase().contains(searchTerm.toLowerCase()))
            {
                temp.add(finaldata.get(i));
            } else
            {
            }
        }
        adap.updateDataList(temp);
        adap.notifyDataSetChanged();
    }
    @Override
    protected void onNewIntent(Intent intent) {

        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            //Toast.makeText(this, query +" -dont know", Toast.LENGTH_SHORT).show();
            //use the query to search your data somehow
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_new_group)
        {
           newGroup.callOnClick();
        }
        else if (id == R.id.action_new_channel)
        {
            newChannel.callOnClick();
        }
        else if(id==R.id.action_add_new_contact)
        {
            contactFloatButton.callOnClick();
        }
        else if(id==R.id.action_refresh)
        {
            if(NetworkUtils.networkIsAvailable(getApplicationContext())) {
                Toast.makeText(this, R.string.refreshing, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent("GET_CONTACTS_UPDATED");
                sendBroadcast(intent);
            }
            else
            {
                Toast.makeText(this, R.string.no_network_connection, Toast.LENGTH_SHORT).show();
            }
        }

        return super.onOptionsItemSelected(item);
    }
}
