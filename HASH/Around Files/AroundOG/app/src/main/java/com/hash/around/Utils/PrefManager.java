package com.hash.around.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.ArrayRes;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Febin on 6/12/2018.
 */

public class PrefManager {

    public static String PREF_CHAT_MUTE="pref_chat_mute";
    public static String PREF_CHAT_NOTIFICATION_SOUND="pref_chat_notification_sound";
    public static String PREF_CHAT_VIBRATION="pref_chat_vibration";
    public static String PREF_CHAT_NOTIFICATION_LIGHT=" pref_chat_notification_light";

    public static String PREF_FEED_MUTE="pref_feed_mute";
    public static String PREF_FEED_NOTIFICATION_SOUND="pref_feed_notification_sound";
    public static String PREF_FEED_VIBRATION=" pref_feed_vibration";
    public static String PREF_FEED_NOTIFICATION_LIGHT=" pref_feed_notification_light";

    public static String PREF_CHAT_READ_RECEIPT="pref_chat_read_receipt";
    public static String PREF_CHAT_RECEIVED_RECEIPT="pref_chat_received_receipt";

    public static String PREF_CHAT_AUTO_DELETE="pref_chat_auto_delete";
    public static String PREF_CHAT_AUTO_DELETE_SIZE="pref_chat_auto_delete_size";

    public static String PREF_FEED_AUTO_DELETE="pref_feed_auto_delete";
    public static String PREF_FEED_AUTO_DELETE_SIZE="pref_feed_auto_delete_size";

    public static String PREF_LANGUAGE="pref_language";

    public static String LAST_CONTACT_SIZE="countactsize";
    public static String LAST_CONTACT_TIMESTAMP="countactlasttimestamp";



    public static void setBooleanPreference(Context context, String key, boolean value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(key, value).apply();
    }

    public static boolean getBooleanPreference(Context context, String key, boolean defaultValue) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(key, defaultValue);
    }

    public static void setStringPreference(Context context, String key, String value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(key, value).apply();
    }

    public static String getStringPreference(Context context, String key, String defaultValue) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(key, defaultValue);
    }
    private static int getIntegerPreference(Context context, String key, int defaultValue) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(key, defaultValue);
    }

    private static void setIntegerPrefrence(Context context, String key, int value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(key, value).apply();
    }
    private static Set<String> getStringSetPreference(Context context, String key, Set<String> defaultValues) {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        if (prefs.contains(key)) {
            return prefs.getStringSet(key, Collections.<String>emptySet());
        } else {
            return defaultValues;
        }
    }

//new HashSet<>(Arrays.asList(context.getResources().getStringArray(defaultValuesRes)))

    public static boolean checkSearchMultiSelectPref(Context context,String key,int val,@ArrayRes int defaultValuesRes)
    {
        Set<String> defaultValues= new HashSet<>(Arrays.asList(context.getResources().getStringArray(defaultValuesRes)));
        SharedPreferences pref=PreferenceManager.getDefaultSharedPreferences(context);
        Set<String> selections =pref.getStringSet(key,defaultValues);
        if(selections!=null) {
            if (selections.contains(val + "")) {
                return true;
            }
        }
        return false;
    }
   /* public static boolean checkSearchListPref(Context context,String key,int val)
    {
        SharedPreferences pref=PreferenceManager.getDefaultSharedPreferences(context);
        String selections =pref.getString(key,null);
        if(selections!=null) {
            if (selections.contains(val + "")) {
                return true;
            }
        }
        return false;
    }*/

}
