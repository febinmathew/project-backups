package com.hash.around;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.LinearLayout;

import com.github.clans.fab.FloatingActionButton;
import com.hash.around.OtherClasses.Adapter_LocationData;
import com.hash.around.OtherClasses.DataTypes.LocationData;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.OtherClasses.RecyclerViewTouchListner;
import com.hash.around.TabFragments.MapsFragment;
import com.hash.around.TabFragments.TabFragment3;
import com.hash.around.Utils.AppOnlineStatusManager;
import com.hash.around.Views.ViewFunctions;

import java.util.ArrayList;

//import com.hash.around.TabFragments.TabFragment2.get;


public class LocationsWindow extends AppCompatActivity {

    FloatingActionButton mapExpandMinimizeFloating;

    //public TabFragment3 tabFragment3;

    public RecyclerView rView;
    private LinearLayoutManager mLayoutManager;
    FloatingActionButton floatButton;
    ArrayList<LocationData> finaldata;
    Adapter_LocationData adap;
    LinearLayout noHotspotsAvailable;
NestedScrollView locationNestedScroll;
boolean selectionActivity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.locations_window);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        //getSupportActionBar().setDisplayShowHomeEnabled(false);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        //getSupportActionBar().setLogo(R.drawable.ic_icon_chat_bubble_green);

        ActionBar ab=getSupportActionBar();

        if (ab != null) {
           // ab.setLogo(R.mipmap.ic_launcher);

            //ab.setDisplayUseLogoEnabled(true);
            ab.setTitle(null);
            ab.setDisplayShowTitleEnabled(true);
            ab.setTitle(R.string.hotspots_tag);
            //ab.setDisplayShowCustomEnabled(false);
            ab.setDisplayHomeAsUpEnabled(true);
            //ab.setLogo(R.mipmap.logo1);

           // ab.setDisplayHomeAsUpEnabled(false);
            //ab.setDisplayShowHomeEnabled(true);
        }

        //mapFragmentAnchoredExpandFloating=(FloatingActionButton)findViewById(R.id.mapFragmentAnchoredExpandFloating);
        FragmentManager fragmentManager= getFragmentManager();

        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
        //final FragmentTransaction fragmentTransaction2=fragmentManager.beginTransaction();

        /*tabFragment3=new TabFragment3();
        fragmentTransaction
                .replace(R.id.fragmnetLocationsListSection, tabFragment3)
                .commit();*/

        selectionActivity=getIntent().getBooleanExtra("selection",false);


        mapExpandMinimizeFloating =(FloatingActionButton)findViewById(R.id.mapExpandMinimizeFloating);
        setExpandFabClickListner();
        //mapFragmentExpand=(FloatingActionButton)findViewById(R.id.mapFragmentExpand);
        locationNestedScroll=findViewById(R.id.locationNestedScroll);
        rView=(RecyclerView)findViewById(R.id.rView);
        noHotspotsAvailable=(LinearLayout) findViewById(R.id.noFeedsAvailable);
        mLayoutManager=new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,true);
        mLayoutManager.setStackFromEnd(true);
        rView.setLayoutManager(mLayoutManager);
        rView.setNestedScrollingEnabled(false);
        finaldata=new ArrayList<LocationData>();
        getLocationData();
        adap=new Adapter_LocationData(this,finaldata,selectionActivity);
        rView.setAdapter(adap);
        //setHasOptionsMenu(true);
        floatButton=(FloatingActionButton)findViewById(R.id.floatButton);
        floatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mapIntent=new Intent(getApplicationContext(),MapsActivity.class);
                mapIntent.putExtra("selection",selectionActivity);
                //  uploadIntent.putExtra("numberType",5);
                //uploadIntent.putExtra("uri",directory.toString());
                startActivityForResult(mapIntent,20);
            }
        });

        setupRecyclerlistner();

        rView.addOnItemTouchListener(new RecyclerViewTouchListner(this, rView, new RecyclerViewTouchListner.RecyclerClick_Listener()
        {
            @Override
            public void onClick(View view, int position) {
                if(selectionActivity)
                    returnActivityResult(finaldata.get(position));
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        IntentFilter intentFilter = new IntentFilter("HOTSPOTS_UPDATED");
        registerReceiver(locationUpdateReceiver, intentFilter);
    }
    @Override
    protected void onStart() {
        super.onStart();
        AppOnlineStatusManager.OnlineStatus.setActive(true);

    }
    @Override
    public void onStop() {
        AppOnlineStatusManager.OnlineStatus.setActive(false);
        super.onStop();
    }
    private BroadcastReceiver locationUpdateReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            getLocationData();
            adap.notifyDataSetChanged();
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK&& requestCode==20)
        {
            if(selectionActivity)
            {
                Log.e("GGG","locdelete selectionact act "+selectionActivity);
                LocationData dat=(LocationData) data.getSerializableExtra("location");
                returnActivityResult(dat);
            }
            else
            {
                finaldata.clear();
                getLocationData();
                adap.notifyDataSetChanged();
            }
        }
    }

    void returnActivityResult(LocationData temp)
    {
        Intent intent = new Intent();
        intent.putExtra("location",temp);
        setResult(RESULT_OK, intent);
        finish();
    }

    void getLocationData()
    {
        finaldata.clear();
        SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();

        Cursor cursor = db.query(DatabaseAccesser.location_table, new String[]{DatabaseAccesser.loc_col_location_name,
                        DatabaseAccesser.loc_col_status, DatabaseAccesser.loc_col_latitude, DatabaseAccesser.loc_col_longitude},
                null,
                null,
                null, null, null,null);

        while (cursor.moveToNext()) {


            LocationData temp = new LocationData();

            temp.location_name = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.loc_col_location_name));
            temp.status = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.loc_col_status));
            temp.latitude = cursor.getDouble(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.loc_col_latitude));
            temp.longitude = cursor.getDouble(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.loc_col_longitude));

            finaldata.add(temp);
        }
        cursor.close();

        if(finaldata.size()==0)
        {
            noHotspotsAvailable.setVisibility(View.VISIBLE);
        }
        else
            noHotspotsAvailable.setVisibility(View.GONE);
    }


public void setExpandFabClickListner()
{
    mapExpandMinimizeFloating.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if(getFragmentManager().findFragmentByTag("MAP_FRAGMENT")==null)
            {
                getFragmentManager().beginTransaction()
                        .replace(R.id.fragmnetMapSection, new MapsFragment(new MapsFragment.OnTouchListener() {
                            @Override
                            public void onTouch() {
                                Log.e("GGG","intercept");
                                locationNestedScroll.requestDisallowInterceptTouchEvent(true);
                            }
                        }), "MAP_FRAGMENT")
                        .commit();
//                    mapFragmentExpand.setVisibility(View.VISIBLE);
                ViewFunctions.rotateAnimation(getApplicationContext(),mapExpandMinimizeFloating,0,180,500);
                //  mapExpandMinimizeFloating.setImageResource(R.drawable.ic_arrow_up_white);
            }
            else
            {
                getFragmentManager().beginTransaction().remove(getFragmentManager().findFragmentByTag("MAP_FRAGMENT")).commit();
                // mapFragmentExpand.setVisibility(View.GONE);
                ViewFunctions.rotateAnimation(getApplicationContext(),mapExpandMinimizeFloating,180,0,500);

                // mapExpandMinimizeFloating.setImageResource(R.drawable.ic_arrow_down_white);

            }
        }
    });
}


public void setupRecyclerlistner ()
{
    rView.addOnScrollListener(new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            Log.e("MGG","position is "+dy);
            if(finaldata.size()>0 && (mLayoutManager.findFirstCompletelyVisibleItemPosition()!=0 && mapExpandMinimizeFloating.getVisibility()!=View.GONE))
            {
                ViewFunctions.independantAnimateOut(mapExpandMinimizeFloating,getApplicationContext());
            }
            else if(mapExpandMinimizeFloating.getVisibility()!=View.VISIBLE)
            {
                ViewFunctions.independantAnimateIn(mapExpandMinimizeFloating,getApplicationContext());
            }
            /*if(dy>5 && mapFragmentExpand.getVisibility()==View.GONE)
            {
                mapExpandMinimizeFloating.setVisibility(View.GONE);
            }
            else
            {
                mapExpandMinimizeFloating.setVisibility(View.VISIBLE);
            }*/
        }
    });
}


    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.location_activity, menu);
        //mapExpandMinimizeFloating=menu.findItem(R.id.menu_expand_action).getActionView().findViewById(R.id.floatMenuItemView);
        ///setExpandFabClickListner();
        /*menu.findItem(R.id.menu_expand_action).getActionView().setOnClickListener(new View.OnClickListener() {
        });*/

 this.chatMenu=menu;




       /* MenuItem sItem=menu.findItem(R.id.action_search_chat);
        SearchView searchView =
                (SearchView) sItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // return false;
                // Toast.makeText(AllUsersList.this, "Submit- "+query, Toast.LENGTH_SHORT).show();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                Log.e("MGG","search id: "+newText);
                searchOnList(newText);
                return false;
            }
        });*/

        //searchView.setIconified(true);
        //searchView.setIconifiedByDefault(false);


        return true;
    }
    //SparseBooleanArray selectedChats=new SparseBooleanArray();
    int currentVisibleSearch;
    Menu chatMenu;
    void searchOnList(String searchTerm)
    {
        /*adap.selectedItems.clear();
        adap.notifyDataSetChanged();

        if(searchTerm.equals(""))
        {

            //return;
        }
        else {
            for (int i = 0; i < finaldata.size(); i++) {

                if (finaldata.get(i).msg.toLowerCase().contains(searchTerm.toLowerCase())) {
                    adap.selectedItems.put(i, true);
                    adap.notifyItemChanged(i);
                }
            }
        }



        if(adap.selectedItems.size()>0)
        {
            currentVisibleSearch=adap.selectedItems.size()-1;
            chatView.scrollToPosition(adap.selectedItems.keyAt(currentVisibleSearch));
            chatMenu.findItem(R.id.action_search_chat_moveup).setVisible(true);
            chatMenu.findItem(R.id.action_search_chat_movedown).setVisible(true);

        }
        else
        {
            chatMenu.findItem(R.id.action_search_chat_moveup).setVisible(false);
            chatMenu.findItem(R.id.action_search_chat_movedown).setVisible(false);
        }*/
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

       // switch (item.getItemId()) {


              //  return true;


        return super.onOptionsItemSelected(item);
    }


}
