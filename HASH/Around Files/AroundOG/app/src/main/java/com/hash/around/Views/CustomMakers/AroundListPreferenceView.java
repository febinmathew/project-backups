package com.hash.around.Views.CustomMakers;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.v4.content.ContextCompat;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceViewHolder;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.hash.around.R;

/**
 * Created by Febin on 6/8/2018.
 */

public class AroundListPreferenceView extends ListPreference {

    private TextView rightSummary;
    private CharSequence summary;
    //private ImageView preferenceIcon;
    public AroundListPreferenceView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initialize();

    }

    public AroundListPreferenceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    public AroundListPreferenceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public AroundListPreferenceView(Context context) {
        super(context);
        initialize();
    }

    private void initialize() {
        Log.e("MGG","executingh");
        setWidgetLayoutResource(R.layout.preference_rightsummary);

    }



    @Override
    public void onBindViewHolder(PreferenceViewHolder view) {
        super.onBindViewHolder(view);

        //this.preferenceIcon= (ImageView) view.findViewById(R.id.preferenceIcon);
        this.rightSummary = (TextView)view.findViewById(R.id.right_summary);
        setSummary(this.summary);
    }

    @Override
    public void setIcon(int iconResId) {
        super.setIcon(iconResId);
        getIcon().setColorFilter(ContextCompat.getColor(getContext(), R.color.colorPrimary), android.graphics.PorterDuff.Mode.SRC_IN);

    }

    @Override
    public void setIcon(Drawable icon) {
        super.setIcon(icon);
        getIcon().setColorFilter(ContextCompat.getColor(getContext(), R.color.colorPrimary), android.graphics.PorterDuff.Mode.SRC_IN);
    }

    @Override
    public void setSummary(CharSequence summary) {
        super.setSummary(null);

        this.summary = summary;

        if (this.rightSummary != null) {
           this.rightSummary.setText(summary);
        }
    }
}
