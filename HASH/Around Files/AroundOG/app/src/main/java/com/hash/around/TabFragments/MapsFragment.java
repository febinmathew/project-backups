package com.hash.around.TabFragments;

import android.Manifest;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hash.around.OtherClasses.GeofenceTrasitionService;
import com.hash.around.R;

import java.io.IOException;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class MapsFragment extends Fragment implements OnMapReadyCallback,GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener, ResultCallback<Status> {

    MapView mMapView;
    private GoogleMap mMap;
    Marker marker;
    private String mLastQuery = "";
    FrameLayout defaultFrame;
    String locName;
    LatLng ltlg;

    /*public MapsFragment()
    {
        super();
    }*/
    public MapsFragment(OnTouchListener listener)
    {
        super();
        mListener = listener;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.maps_fragment, container, false);

        DisplayMetrics dm=new DisplayMetrics();

        /*rootView.getMetrics(dm);

        //widnowsize=dm.widthPixels-(int)(dm.widthPixels*0.9);

        rootView.getwin.setLayout((int)(dm.widthPixels*0.9),(int)(dm.heightPixels*0.8));*/



        createGoogleApi();
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.maps_activity);

        defaultFrame = (FrameLayout) rootView.findViewById(R.id.defaultFrame);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume(); // needed to get the map to display immediately



        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(this);




        TouchableWrapper frameLayout =new TouchableWrapper(getActivity());
        ((ViewGroup) rootView).addView(frameLayout,
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        // mMapView.onInterceptTouchEvent( final MotionEvent event){})
        ViewGroup.LayoutParams lp = mMapView.getLayoutParams();
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        lp.height = (int)(metrics.widthPixels*((float)3.3/4));
        mMapView.setLayoutParams(lp);

        return  rootView;

    }
    public class TouchableWrapper extends FrameLayout {

        public TouchableWrapper(Context context) {
            super(context);
        }

        @Override
        public boolean dispatchTouchEvent(MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    mListener.onTouch();
                    Log.e("GGG","touch2 ");
                    break;
                case MotionEvent.ACTION_UP:
                    mListener.onTouch();
                    break;
            }
            return super.dispatchTouchEvent(event);
        }
    }
    private OnTouchListener mListener;
    public void setListener(OnTouchListener listener) {
        mListener = listener;
    }

    public interface OnTouchListener {
        public abstract void onTouch();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        //mMap.listene
      /*  mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                Log.e("GGG","touch2 ");
            }
        });*/

        // Add a marker in Sydney and move the camera
        //LatLng sydney = new LatLng(-34, 151);
       //marker= mMap.addMarker(new MarkerOptions().position(sydney).title("Sydney"));
        //Toast.makeText(this, "Long press areas on map to select location.", Toast.LENGTH_SHORT).show();
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        //xxx
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},10);
            }

        }
        else
            {
                mMap.setMyLocationEnabled(true);

                mMap.getUiSettings().setMyLocationButtonEnabled(true);

                mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                    @Override
                    public void onMapLongClick(LatLng point) {
                        searchLocation(point);
                    }
                });
                /*
                mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng point) {

                        //Log.d("DEBUG","Map clicked [" + point.latitude + " / " + point.longitude +

                        //Do your stuff with LatLng here
                        //Then pass LatLng to other activity
                    }
                });*/

        }
    }




    public void searchLocation(LatLng point)
    {
        Geocoder gcd = new Geocoder(getActivity());

        List<Address> addresses = null;
        try {
            addresses = gcd.getFromLocation(
                    point.latitude,
                    point.longitude,
                    // In this sample, get just a single address.
                    1);
        } catch (IOException ioException) {
            // Catch network or other I/O problems.
            //Toast.makeText(SetProfile.this, "Service no avaialble", Toast.LENGTH_SHORT).show();
        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
            //Toast.makeText(SetProfile.this, "invalid latlong", Toast.LENGTH_SHORT).show();
        }
        //try {
            if (addresses.size() >0) {
                Address address = addresses.get(0);
                if (addresses.get(0).getLocality() == null) {
                    //Toast.makeText(this, "Could not locate the place. Try again!", Toast.LENGTH_SHORT).show();
                } else {
                    if (marker != null) {
                        //marker.remove();
                    }
                    ltlg = new LatLng(address.getLatitude(), address.getLongitude());
                    marker = mMap.addMarker(new MarkerOptions().position(ltlg).title(address.getLocality() + ", " + address.getCountryName()));
                    mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                        @Override
                        public boolean onMarkerClick(Marker marker) {

                            success_proceed();
                            return false;


                            // Toast.makeText(MapsActivity.this,  marker.getTitle()+" "+marker.getPosition().latitude+" / "+marker.getPosition().longitude, Toast.LENGTH_SHORT).show();

                        }
                    });
                    // Toast.makeText(this, "starting", Toast.LENGTH_SHORT).show();
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(ltlg,12.0f));
                    //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 12.0f));
                    startGeofence(ltlg);
                    // mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                    // locationtxt.setText(addresses.get(0).getLocality() + ", " + addresses.get(0).getCountryName());
                }
            }
        //}
        //catch (NullPointerException e)
        //{
            //change here

       // }

    }
    

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        googleApiClient.connect();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        googleApiClient.disconnect();
    }

    /***** CODE FROM https://code.tutsplus.com/tutorials/how-to-work-with-geofences-on-android--cms-26639 */
    private static final long GEO_DURATION = 60 * 60 * 1000;
    private static final String GEOFENCE_REQ_ID = "My Geofence";
    private static final String TAG="MSG";
    private static final float GEOFENCE_RADIUS = 2000.0f;
    private PendingIntent geoFencePendingIntent;
    private final int GEOFENCE_REQ_CODE = 0;
    private GoogleApiClient googleApiClient;
    private Circle geoFenceLimits;
    LatLng geofencePos;

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(@NonNull Status status) {
        Log.d(TAG, "RESULTS OBTAINED");
        drawGeofence();

    }

    private void drawGeofence() {
        Log.d(TAG, "drawGeofence()");

        if ( geoFenceLimits != null )
            geoFenceLimits.remove();

        CircleOptions circleOptions = new CircleOptions()
                .center( geofencePos)
                .strokeColor(Color.argb(150,244, 65, 65))
                .fillColor( Color.argb(100,65, 190, 244) )
                .radius( GEOFENCE_RADIUS );
        geoFenceLimits = mMap.addCircle( circleOptions );
    }
    private void createGoogleApi() {
        Log.d(TAG, "createGoogleApi()");
        if ( googleApiClient == null ) {
            googleApiClient = new GoogleApiClient.Builder( getActivity() )
                    .addConnectionCallbacks( this )
                    .addOnConnectionFailedListener( this )
                    .addApi( LocationServices.API )
                    .build();
        }
    }

    private Geofence createGeofence(LatLng latLng, float radius ) {
        Log.d(TAG, "createGeofence");
        return new Geofence.Builder()
                .setRequestId(GEOFENCE_REQ_ID)
                .setCircularRegion( latLng.latitude, latLng.longitude, radius)
                .setExpirationDuration( GEO_DURATION )
                .setTransitionTypes( Geofence.GEOFENCE_TRANSITION_ENTER
                        | Geofence.GEOFENCE_TRANSITION_EXIT )
                .build();
    }
    private GeofencingRequest createGeofenceRequest(Geofence geofence ) {
        Log.d(TAG, "createGeofenceRequest");
        return new GeofencingRequest.Builder()
                .setInitialTrigger( GeofencingRequest.INITIAL_TRIGGER_ENTER )
                .addGeofence( geofence )
                .build();
    }

    private void startGeofence(LatLng pos) {
        Log.i(TAG, "startGeofence()");
geofencePos=pos;
        Geofence geofence = createGeofence(pos, GEOFENCE_RADIUS);
        GeofencingRequest geofenceRequest = createGeofenceRequest(geofence);
        addGeofence(geofenceRequest);
    }
    private void addGeofence(GeofencingRequest request) {
        Log.d(TAG, "addGeofence");
        if (checkPermission())
            LocationServices.GeofencingApi.addGeofences(
                    googleApiClient,
                    request,
                    createGeofencePendingIntent()
            ).setResultCallback(this);
    }
    private PendingIntent createGeofencePendingIntent()
    {
        Log.d(TAG, "createGeofencePendingIntent");
        if ( geoFencePendingIntent != null )
            return geoFencePendingIntent;

        Intent intent = new Intent( getActivity(), GeofenceTrasitionService.class);
        return PendingIntent.getService(
                getActivity(), GEOFENCE_REQ_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT );
    }

    private boolean checkPermission() {
        Log.d(TAG, "checkPermission()");
        // Ask for permission if it wasn't granted yet
        return (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED );
    }


    public void success_proceed()
    {
        Snackbar sBar=Snackbar.make(defaultFrame,marker.getTitle(),Snackbar.LENGTH_INDEFINITE);
        locName=marker.getTitle();
        sBar.setAction("PROCEED",  new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(MapsActivity.this, "Data Taken", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent();
                intent.putExtra("locname",locName);
                intent.putExtra("lat",ltlg.latitude);
                intent.putExtra("lng",ltlg.longitude);
                getActivity().setResult(RESULT_OK, intent);
                getActivity().finish();

            }
        });

        View sbView = sBar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        sBar.show();




    }



}
