package com.hash.around;

import android.app.ActivityOptions;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.hash.around.OtherClasses.DataTypes.ChatData;
import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.Utils.AppOnlineStatusManager;
import com.hash.around.Utils.LanguageHelper;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.HashMap;

import static android.view.View.GONE;

public class ProfileSettings extends AppCompatActivity {

    String userId;
    CircularImageView profileimage;
    TextView profile_username,profile_status;
    LanguageHelper languageHelper=new LanguageHelper();

    @Override
    protected void onResume() {
        super.onResume();
        languageHelper.onReset(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languageHelper.onStart(this);
        setContentView(R.layout.profile_settings_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        profileimage=(CircularImageView)findViewById(R.id.profileimage);
        profile_username=(TextView)findViewById(R.id.profile_username);
        profile_status=(TextView)findViewById(R.id.profile_status);

        userId=getIntent().getStringExtra("userid");
        accessDatabseForContent(userId);
        //getActionBar().setDisplayHomeAsUpEnabled(true);

    }


    @Override
    protected void onStart() {
        super.onStart();
        AppOnlineStatusManager.OnlineStatus.setActive(true);

    }
    @Override
    public void onStop() {
        AppOnlineStatusManager.OnlineStatus.setActive(false);
        super.onStop();
    }

    void accessDatabseForContent(String uID)
    {

        UserWholeData userWholeData= ChatWindow.returnSingleUserDetails(getApplicationContext(),uID);
        profile_username.setText(userWholeData.user_name);

        if(userWholeData.status.equals("null"))
            profile_status.setText("Wanna know an App called SpotIt?");
        else
         profile_status.setText(userWholeData.status);

        Log.e("GGG","userprofile "+userWholeData.img_url);
        if(userWholeData.img_url.equals("null") ||userWholeData.img_url==null)
        {
            profileimage.setImageResource(R.drawable.default_user);
        }
        else
        {
            profileimage.setImageURI(Uri.parse(userWholeData.img_url));
        }

    }





    public void onclickprofile(View view)
    {
        Intent indi=new Intent(getApplicationContext(),Profile_Activity.class);
        indi.putExtra("userid",userId);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // Apply activity transition

            Pair contact_image = Pair.create(profileimage, "contact_image");
            Pair contact_name = Pair.create(profile_username, "contact_name");

            ActivityOptions compat=ActivityOptions.makeSceneTransitionAnimation(
                    this,contact_image,contact_name);
            startActivity(indi,compat.toBundle());
        }
        else
        {

            startActivity(indi);
        }

    }

    public void notificationSettings(View view)
    {
        Intent intent=new Intent(this,SettingsActivity.class);
        intent.putExtra("stat",0);
        startActivity(intent);
    }

    public void privacySettings(View view)
    {
        Intent intent=new Intent(this,SettingsActivity.class);
        intent.putExtra("stat",1);
        startActivity(intent);
    }

    public void dataUsageSettings(View view)
    {
        Intent intent=new Intent(this,SettingsActivity.class);
        intent.putExtra("stat",2);
        startActivity(intent);
    }
    public void feedSettings(View view)
    {
        Intent intent=new Intent(this,SettingsActivity.class);
        intent.putExtra("stat",3);
        startActivity(intent);
    }
    public void chatSettings(View view)
    {
        Intent intent=new Intent(this,SettingsActivity.class);
        intent.putExtra("stat",4);
        startActivity(intent);
    }
    /*public void testSettings(View view)
    {
        Intent intent=new Intent(this,SettingsActivity.class);
        intent.putExtra("stat",2);
        startActivity(intent);
    }*/


}
