package com.hash.around;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PointF;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.hash.around.ImageCropActivity;
import com.hash.around.OtherClasses.Interfaces.BitmapRenderListener;
import com.hash.around.R;
import com.hash.around.TabFragments.MapsFragment;
import com.hash.around.UploadActivity;
import com.hash.around.Utils.MediaManager;
import com.hash.around.Utils.StringUtils;
import com.hash.around.Views.Glide.GlideApp;
import com.hash.around.Views.scribbles.StickerSelectActivity;
import com.hash.around.Views.scribbles.viewmodel.Font;
import com.hash.around.Views.scribbles.viewmodel.Layer;
import com.hash.around.Views.scribbles.viewmodel.TextLayer;
import com.hash.around.Views.scribbles.widget.ImageEditControlsPanel;
import com.hash.around.Views.scribbles.widget.MotionView;
import com.hash.around.Views.scribbles.widget.ScribbleView;
import com.hash.around.Views.scribbles.widget.VerticalSlideColorPicker;
import com.hash.around.Views.scribbles.widget.entity.ImageEntity;
import com.hash.around.Views.scribbles.widget.entity.MotionEntity;
import com.hash.around.Views.scribbles.widget.entity.TextEntity;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

import static android.view.View.GONE;

public class MediaEditActivity extends AppCompatActivity implements  VerticalSlideColorPicker.OnColorChangeListener,ImageEditControlsPanel.ScribbleToolbarListener{
    private VerticalSlideColorPicker colorPicker;
    ImageEditControlsPanel imageEditControlsPanel;
    public ScribbleView scribble_view;
    String image_Uri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.media_edit_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.findViewById(R.id.up_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaEditActivity.super.onBackPressed();
            }
        });
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        image_Uri=getIntent().getStringExtra("uri");
        this.colorPicker   = findViewById(R.id.scribble_color_picker);
        this.colorPicker.setVisibility(GONE);
        colorPicker.setOnColorChangeListener(this);



        imageEditControlsPanel= findViewById(R.id.imageEditControlsPanel);
        imageEditControlsPanel.setListener(this);
        imageEditControlsPanel.setToolColor(Color.RED);

        scribble_view=findViewById(R.id.scribble_view);

        scribble_view.setImage(Uri.parse(image_Uri));

       /* scribble_view.setTouchListener(new MapsFragment.OnTouchListener() {
            @Override
            public void onTouch() {
              viewPager.requestDisallowInterceptTouchEvent(true);
            }
        });*/
        scribble_view.setMotionViewCallback(motionViewCallback);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1 && resultCode == RESULT_OK)
        {

                //uploadImg.setImageURI(null);
                Uri uricropped=Uri.parse(data.getStringExtra("uri"));
                image_Uri=data.getStringExtra("uri");
            Log.e("GGG","bitbit5 CROPED URI "+uricropped);
                scribble_view.setImage(uricropped);
                scribble_view.invalidate();
        }
        else if (requestCode == 10&& resultCode == RESULT_OK) {
            if (data != null) {

                final String stickerFile = data.getStringExtra(StickerSelectActivity.EXTRA_STICKER_FILE);

                new AsyncTask<Void, Void, Bitmap>() {
                    @Override
                    protected @Nullable
                    Bitmap doInBackground(Void... params) {
                        try {
                            return BitmapFactory.decodeStream(getAssets().open(stickerFile));
                        } catch (IOException e) {
                            //Log.w(TAG, e);
                            return null;
                        }
                    }

                    @Override
                    protected void onPostExecute(@Nullable Bitmap bitmap) {
                        addSticker(bitmap);
                    }
                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        }
    }

    @Override
    public void onColorChange(int color) {
        if (color == 0) color = Color.RED;
        // background.setBackgroundColor(color);

        // toolbar.setToolColor(color);
        scribble_view.setDrawingBrushColor(color);
        imageEditControlsPanel.setToolColor(color);

        TextEntity textEntity = scribble_view.currentTextEntity();

        if (textEntity == null) {
            return;
        }

        textEntity.getLayer().getFont().setColor(color);
        textEntity.updateEntity();
        scribble_view.invalidate();
        //changeTextEntityColor(color);
    }
    private void addSticker(final Bitmap pica) {
        //Util.runOnMain(() -> {

        Layer layer  = new Layer();
        ImageEntity entity = new ImageEntity(layer, pica, scribble_view.getWidth(), scribble_view.getHeight());

        scribble_view.addEntityAndPosition(entity);
        //});
    }



    @Override
    public void onStickerSelected(boolean enabled) {
        colorPicker.setVisibility(View.GONE);

        if (!enabled) {
            //scribbleView.clearSelection();
        } else {
            scribble_view.setDrawingMode(false);
            Intent intent = new Intent(this, StickerSelectActivity.class);
            startActivityForResult(intent, 10);
        }
    }

    @Override
    public void onTextSelected(boolean enabled) {

        if (enabled) {
            addTextSticker();
            scribble_view.setDrawingMode(false);
            colorPicker.setVisibility(View.VISIBLE);
        } else {
            scribble_view.clearSelection();
            colorPicker.setVisibility(View.GONE);
        }
    }

    protected void addTextSticker() {
        TextLayer textLayer  = createTextLayer();

        TextEntity textEntity = new TextEntity(textLayer, scribble_view.getWidth(), scribble_view.getHeight());
        scribble_view.addEntityAndPosition(textEntity);

        // move text sticker up so that its not hidden under keyboard
        PointF center = textEntity.absoluteCenter();
        center.y = center.y * 0.5F;
        textEntity.moveCenterTo(center);

        // redraw
        scribble_view.invalidate();

        scribble_view.startEditing(textEntity);
        //((TextEntity) scribbleView.getSelectedEntity().
        //startTextEntityEditing();
        textEntity.getLayer().getFont().setColor(imageEditControlsPanel.getToolColor());
        textEntity.updateEntity();
        scribble_view.invalidate();
        // changeTextEntityColor(colorPicker.getSolidColor());
    }

    private TextLayer createTextLayer() {
        TextLayer textLayer = new TextLayer();
        Font font = new Font();

        font.setColor(TextLayer.Limits.INITIAL_FONT_COLOR);
        font.setSize(TextLayer.Limits.INITIAL_FONT_SIZE);

        textLayer.setFont(font);

        return textLayer;
    }

    @Override
    public void onBrushSelected(boolean enabled) {
        scribble_view.setDrawingMode(enabled);
        colorPicker.setVisibility(enabled ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onPaintUndo() {

        scribble_view.undoDrawing();
    }

    @Override
    public void onDeleteSelected() {

        scribble_view.deleteSelected();


        colorPicker.setVisibility(View.GONE);
    }

    void updateDeleteView()
    {
        //scribble_view

    }

    @Override
    public void onSave() {
        scribble_view.getRenderedImage(this,new BitmapRenderListener() {
            @Override
            public void onSuccess(Bitmap bitmap) {

                if(bitmap==null)
                    return;
                ByteArrayOutputStream baos     = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);

                byte[] data = baos.toByteArray();
                baos   = null;
                bitmap = null;

                Uri    uri    = MediaManager.createNewFile(getApplicationContext(),new ByteArrayInputStream(data),System.currentTimeMillis(),"image/png", StringUtils.randomStringGenerator(),0L);
                Log.e("GGG","bitbit5 success "+uri);
               scribble_view.setImage(uri);
                Intent intent = new Intent();
                intent.setData(uri);
                setResult(RESULT_OK, intent);
                finish();
            }

            @Override
            public void onFailure(Exception e) {

            }
        });


    }

    @Override
    public void onCropSelected() {
        Intent cropIntent=new Intent(this,ImageCropActivity.class);
        cropIntent.putExtra("uri",image_Uri);
        startActivityForResult(cropIntent,1);
    }

    private final MotionView.MotionViewCallback motionViewCallback = new MotionView.MotionViewCallback() {
        @Override
        public void onEntitySelected(@Nullable MotionEntity entity) {
            if (entity == null) {
                imageEditControlsPanel.setNoneSelected();
                colorPicker.setVisibility(View.GONE);
            } else if (entity instanceof TextEntity) {
                imageEditControlsPanel.setTextSelected(true);
                colorPicker.setVisibility(View.VISIBLE);
            } else {
                imageEditControlsPanel.setStickerSelected(true);
                colorPicker.setVisibility(View.GONE);
            }
        }
        @Override
        public void onEntityDoubleTap(@NonNull MotionEntity entity) {
            startTextEntityEditing();
        }

    };
    private void startTextEntityEditing() {

        TextEntity textEntity = scribble_view.currentTextEntity();
        if (textEntity != null) {
            scribble_view.startEditing(textEntity);
        }
    }
}
