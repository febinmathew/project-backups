package com.hash.around.OtherClasses;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;

import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.hash.around.AllUsersList;
import com.hash.around.ChatForwardActivity;
import com.hash.around.OtherClasses.DataTypes.ChatData;
import com.hash.around.OtherClasses.DataTypes.GroupChatData;
import com.hash.around.OtherClasses.DataTypes.LocationData;
import com.hash.around.R;
import com.hash.around.TabbedActivity;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.io.Serializable;
import java.util.ArrayList;

import static com.hash.around.TabbedActivity.editor;

/**
 * Created by Febin on 26-08-2017.
 */

public class Adapter_LocationData extends RecyclerView.Adapter<Adapter_LocationData.ViewHolder> {

    Context mContext;
    ArrayList<LocationData> data;
boolean selectionActivity;
    public Adapter_LocationData(Context mcont, ArrayList<LocationData> list,boolean selectionActivity)
    {
        mContext=mcont;
        data=list;
        this.selectionActivity=selectionActivity;
        //selectedUsers=new SparseBooleanArray();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v =  LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_location, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }
   /* public SparseBooleanArray selectedUsers;
    Snackbar snack;*/
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        /*final Drawable dr = holder.temp;

        if (selectedUsers.get(position))
        {
            holder.itemView.setBackgroundColor(Color.parseColor("#10000000"));
            holder.imageSelectionTick.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.itemView.setBackground(dr);
            holder.imageSelectionTick.setVisibility(View.GONE);
        }*/
        //final SwitchCompat compatSwitch=holder.switchLoc;
        holder.switchLoc.setChecked(data.get(position).status==1);
        setCompatSwitch(holder.switchLoc,data.get(position).status==1);
       /* compatSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {

                }
                else
                    setCompatSwitch(compatSwitch,false);

            }
        });*/


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SwitchCompat tempswitch=  holder.switchLoc;

                Log.e("GGG","locdelete selectionact adap "+selectionActivity);
                if(selectionActivity)
                {
                   /* if (selectedUsers.get(position)) {
                        selectedUsers.delete(position);
                        //rView.setBackground(dr);
                        notifyItemChanged(position);
                    }
                    else {
                        selectedUsers.put(position,true);
                        notifyItemChanged(position);
                        //rView.setBackgroundColor(Color.parseColor("#20000000"));
                    }

                    if(selectedUsers.size()>0)
                    {
                        String snackText="";
                        for (int i = selectedUsers.size()-1; i>=0; i--) {

                            if(i==selectedUsers.size()-1)
                                snackText+=data.get(selectedUsers.keyAt(i)).location_name;
                            else
                                snackText+=", "+data.get(selectedUsers.keyAt(i)).location_name;
                        }

                        if(snack==null) {
                            snack = Snackbar.make(v, snackText, Snackbar.LENGTH_INDEFINITE);
                            snack.show();

                        }
                        else
                        {
                            snack.setText(snackText);
                        }
                    }
                    else {
                        snack.dismiss();
                        snack=null;
                        //remove snackbar
                    }*/
                }
                else {

                    tempswitch.setChecked(! tempswitch.isChecked());
                }
            }
        });

        /*holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


               // editor.putString("location_name", data.get(position).location_name);
               // editor.putString("location_latitude", String.valueOf(data.get(position).latitude));
               // editor.putString("location_longitude", String.valueOf(data.get(position).longitude));
               // editor.commit();
                //Toast.makeText(mContext, "Clickeyeeee", Toast.LENGTH_SHORT).show();
            }
        });*/



        String temp;
        holder.cLocationText.setText(data.get(position).location_name);
        temp=data.get(position).latitude+", "+data.get(position).longitude;
        holder.cLatitudeText.setText(temp);



        /*if(data.get(position).status==1)
        {
            holder.switchLoc.setChecked(true);
        }
        else
        {
            holder.switchLoc.setChecked(false);
        }*/

        holder.switchLoc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                SQLiteDatabase db= TabbedActivity.db.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(DatabaseAccesser.loc_col_status, isChecked?1:0);
                int rowln=db.update(DatabaseAccesser.location_table,
                        values,
                        DatabaseAccesser.loc_col_latitude + " = ? AND " + DatabaseAccesser.loc_col_longitude + " = ?",
                        new String[]{data.get(position).latitude+"",data.get(position).longitude+""});

                if(rowln !=1)
                {
                    Toast.makeText(mContext, "not updated Error", Toast.LENGTH_SHORT).show();
                }

                setCompatSwitch(holder.switchLoc,isChecked);

            }
        });

        holder.deleteLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteLocation(data.get(position).location_name+"",
                        data.get(position).latitude+"",
                        data.get(position).longitude+"",position);

            }
        });




holder.shareLoc.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        shareLocation(data.get(position));
    }
});
        //temp="Lng: "+data.get(position).longitude;
      //  holder.cLongitudeText.setText(temp);
     /* final int pos=position;

       if(data.get(position).userid==1)
            holder.proimg.setImageResource(R.drawable.nick);
        else
            holder.proimg.setImageResource(R.drawable.profile);
switch(data.get(position).id) {
    case 1:
    holder.img.setImageResource(R.drawable.danial);
        break;
    case 2:
        holder.img.setImageResource(R.drawable.profile);
        break;
    case 3:
        holder.img.setImageResource(R.drawable.blue);
        break;
    case 4:
        holder.img.setImageResource(R.drawable.acce);
        break;
}
        holder.tx.setText(data.get(position).text);

        holder.proimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Toast.makeText(mContext,data.get(pos).text, Toast.LENGTH_SHORT).show();
                //Intent in=new Intent(mContext, Profile_Activity.class);

               // mContext.startActivity(in);
            }
        });*/
    }
    void setCompatSwitch(SwitchCompat compatSwitch,boolean enabled)
    {
        compatSwitch.getCompoundDrawables()[0].setColorFilter(mContext.getResources().getColor(enabled?R.color.colorAccent:R.color.semiblack_drawable), PorterDuff.Mode.SRC_IN);
    }
    void shareLocation(LocationData value)
    {

        Intent intent = new Intent(mContext, ChatForwardActivity.class);
        ArrayList<GroupChatData> chatsArray=new ArrayList<>();
        GroupChatData temp=new GroupChatData();
        temp.type=4;
        temp.msg=value.location_name+TabbedActivity.TEXT_SEPARATOR+value.latitude+TabbedActivity.TEXT_SEPARATOR+value.longitude;
        temp.temp_url = "null";
        temp.final_url ="null";

        chatsArray.add(temp);
        Bundle args = new Bundle();
        args.putSerializable("ARRAYLIST",(Serializable)chatsArray);
        intent.putExtra("BUNDLE",args);
        mContext.startActivity(intent);

    }

    void deleteLocation(String locationName, final String lat, final String lng, final int pos) {

        // Context tempContext=getActivity().getApplicationContext();

        Log.e("GGG","locdelete ");
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Remove "+locationName+"?");

        builder.setCancelable(true);


            builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Log.e("GGG","locdelete delete");
                    SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();
                    int rowln=db.delete(DatabaseAccesser.location_table,
                            DatabaseAccesser.loc_col_latitude+" = ? AND " + DatabaseAccesser.loc_col_longitude + " = ?",
                            new String[]{lat,lng});
                    Intent intent = new Intent("HOTSPOTS_UPDATED");
                    mContext.sendBroadcast(intent);
                   /* int rowln=db.delete(DatabaseAccesser.location_table,
                            DatabaseAccesser.loc_col_latitude+" = ? OR ''='' OR " + DatabaseAccesser.loc_col_longitude + " = ?",
                            new String[]{lat,lng});*/

                    if(rowln !=1)
                    {
                        Toast.makeText(mContext, "Error: Unable to delete location!", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        data.remove(pos);

                    }

                    notifyItemRemoved(pos);
                    dialog.dismiss();
                }
            });

        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.e("GGG","locdelete dismiss");
               dialog.dismiss();
                   }
        });

        Log.e("GGG","locdelete show");
        AlertDialog alert = builder.create();
        alert.show();
        Log.e("GGG","locdelete show1");
    }

    @Override
    public int getItemCount() {
        if(data!=null) {
            return data.size();
        }
        return 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
      /*  public CardView cv;
        public TextView tx;
        public ImageView img;
        public ImageView proimg;*/

        public TextView cLocationText;
        public TextView cLatitudeText;
        View rootView;
        SwitchCompat switchLoc;

        ImageButton deleteLoc,shareLoc;
        public Drawable temp=null;
      //  public TextView cLongitudeText;
      public CircularImageView imageSelectionTick;
        public ViewHolder(View item) {
            super(item);
            /*cv=(CardView)item.findViewById(R.id.cView);

            img=(ImageView)item.findViewById(R.id.ccImage);
            proimg=(ImageView)item.findViewById(R.id.cProfileImage);*/
            cLocationText=(TextView)item.findViewById(R.id.cLocationText);
            cLatitudeText=(TextView)item.findViewById(R.id.cLatitudeText);
            rootView=item;
            switchLoc=(SwitchCompat) item.findViewById(R.id.switchLoc);

            deleteLoc=(ImageButton) item.findViewById(R.id.deleteLoc);
            shareLoc=(ImageButton) item.findViewById(R.id.shareLoc);
            //item.setOnClickListener();
                   // cLongitudeText=(TextView)item.findViewById(R.id.cLongitudeText);

            /*if(temp==null) {
                temp = rootView.getBackground();
            }
            imageSelectionTick=(CircularImageView) item.findViewById(R.id.imageSelectionTick);*/

        }
    }

}


