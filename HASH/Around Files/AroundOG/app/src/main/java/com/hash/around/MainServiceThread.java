package com.hash.around;

import android.*;
import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnPausedListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.hash.around.Broadcasts.AroundBroadcastReceiver;
import com.hash.around.Broadcasts.ContactChangeObserver;
import com.hash.around.OtherClasses.DataTypes.ChatData;
import com.hash.around.OtherClasses.DataTypes.ChatReplayData;
import com.hash.around.OtherClasses.DataTypes.ContactListData;
import com.hash.around.OtherClasses.DataTypes.DownloadData;
import com.hash.around.OtherClasses.DataTypes.DownloadListData;
import com.hash.around.OtherClasses.DataTypes.GroupFeedData;
import com.hash.around.OtherClasses.DataTypes.LocationData;
import com.hash.around.OtherClasses.DataTypes.MediaData;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.OtherClasses.DataTypes.GroupChatData;
import com.hash.around.OtherClasses.DataTypes.GroupMemberData;
import com.hash.around.OtherClasses.Interfaces.ChatReplayReceiver;
import com.hash.around.OtherClasses.Interfaces.ChatSentListner;
import com.hash.around.OtherClasses.Interfaces.FeedDownloadListner;
import com.hash.around.OtherClasses.Interfaces.FileDownloadListner;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.OtherClasses.DataTypes.NotificationData;
import com.hash.around.OtherClasses.Interfaces.JobCompletionWithFailureListner;
import com.hash.around.OtherClasses.Interfaces.ProcessCompletedListner;
import com.hash.around.OtherClasses.DataTypes.UserPrivacyData;
import com.hash.around.OtherClasses.Interfaces.RemoteStatusFetchListener;
import com.hash.around.OtherClasses.Interfaces.UserPrivacyStatusListner;
import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.hash.around.Utils.AroundStaticInfo;
import com.hash.around.Utils.ByteUtils;
import com.hash.around.Utils.ChatSender;
import com.hash.around.Utils.DatabaseManager;
import com.hash.around.Utils.LocalNotificationManager;
import com.hash.around.Utils.LocationUtils;
import com.hash.around.Utils.MediaManager;
import com.hash.around.Utils.NetworkUtils;
import com.hash.around.Utils.PermissionManager;
import com.hash.around.Utils.PrefManager;
import com.hash.around.Utils.StringUtils;
import com.hash.around.Utils.UserManager;
import com.hash.around.Views.CustomMakers.NotificationTag;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;


import static com.hash.around.Profile_Activity.sentCustomNotificationToUser;
import static com.hash.around.Profile_Activity.fecthUserPrivacyStatus;
import static com.hash.around.TabbedActivity.AROUND_FEED_RADIUS;
import static com.hash.around.TabbedActivity.checkForMediaAvailablility;
import static com.hash.around.TabbedActivity.fDatabase;
import static com.hash.around.TabbedActivity.userdetail;
import static com.hash.around.Utils.PrefManager.PREF_CHAT_READ_RECEIPT;


public class MainServiceThread extends Service{
    ChildEventListener childEventListener,chatReceiveEventListener,childEventListenerForNotification;
    DatabaseReference newPostRef,chatReceiveRef,notificationReceiveRef,groupsRef;
    static String x=null;
    GeoFire geoFire;
    Double lat=0.0;
    Double lng=0.0;
    //TextView locationText;

    GeoQuery geoQuery;
    ValueEventListener postListener;
    //DatabaseAccesser dbAccess;
    private BroadcastReceiver getFeedReciever,phoneContactsUpdateReceiver;
    BroadcastReceiver pendingReciever=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            handlePendingProcess();
        }
    };

    SQLiteDatabase writableDB,readableDB;

    static FirebaseUser userdetail;

    public static int PEOPLE_NEAR_FILTER=0;
    public static int FOLLOWING_PEOPLE_FILTER=1;
    public static int SELECTED_LOCATION_FILTER=2;

    public static MainServiceThread mainService;
    @Override
    public void onCreate() {
        super.onCreate();


        final IntentFilter theFilter = new IntentFilter("GET_FEED_UPDATED");
        getFeedReciever = new BroadcastReceiver()
        {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.e("GGG","total active RECE ");
                Double x;
                Double y;
                try {
                    x = Double.parseDouble(intent.getStringExtra("lat"));
                    y = Double.parseDouble(intent.getStringExtra("lng"));
                }
                catch(Exception e)
                {
                    // Toast.makeText(context, "noo error", Toast.LENGTH_SHORT).show();
                    Log.e("GGG","KATTA ERROR");
                    return;
                }

                SharedPreferences pref= PreferenceManager.getDefaultSharedPreferences(context);
                Set<String>  selections =pref.getStringSet("filter_feed", null);
                if(selections==null)return;

                Log.e("GGG","SEARCHING new feedRADIUS 3 ");
                if( !selections.contains(PEOPLE_NEAR_FILTER+""))
                {
                    x=y=0.0;
                    Log.e("GGG","people nearby removed");
                }
                if(selections.contains(FOLLOWING_PEOPLE_FILTER+""))
                {
                    Log.e("GGG","people following enabled");
                    update_feed2();
                }

                if(selections.contains(SELECTED_LOCATION_FILTER+""))
                {
                    //Log.e("GGG","people location enabled");
                    Log.e("GGG","SEARCHING new feedRADIUS 1 ");
                    update_feed(x,y,false);
                }
                else if(selections.contains(PEOPLE_NEAR_FILTER+"")){
                    Log.e("GGG","SEARCHING new feedRADIUS 2 ");
                update_feed(x,y,true);}


                    //


            }
        };
        // Registers the receiver so that your service will listen for
        // broadcasts

        registerReceiver(getFeedReciever, theFilter);

        final IntentFilter pendingFilter = new IntentFilter("PENDING_UPDATED");
        registerReceiver(pendingReciever, pendingFilter);

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("feeds_location");
        geoFire = new GeoFire(ref);

        x=null;



        final IntentFilter contactFilter = new IntentFilter("GET_CONTACTS_UPDATED");
        phoneContactsUpdateReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {


                Log.e("GGG","change contactupdate called "+ContactsContract.Contacts.CONTACT_LAST_UPDATED_TIMESTAMP);

               // Log.e("MSGG","=broad cast recieved");
                final String[] PROJECTION =
                        {
                                Contacts._ID,
                                Contacts.LOOKUP_KEY,
                                Build.VERSION.SDK_INT
                                        >= Build.VERSION_CODES.HONEYCOMB ?
                                        Contacts.DISPLAY_NAME_PRIMARY :
                                        Contacts.DISPLAY_NAME,
                                Contacts.HAS_PHONE_NUMBER,
                                Contacts._ID

                        };

                final String SELECTION = ContactsContract.CommonDataKinds.Phone.HAS_PHONE_NUMBER+ " >?";
                final String[] mSelectionArgs = { "0" };

                String prevNumber="9999999999";
                String thisUserNumber=userdetail.getPhoneNumber();
                int count=0;
                ArrayList<ContactListData> phoneContacts=new ArrayList<>();

                Cursor cur=getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null,SELECTION,mSelectionArgs,null);
                Log.e("GGG","change contact size is "+cur.getCount());
                while(cur.moveToNext())
                {

                    String contactNumber = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    String contactName = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));


                    if(!(PhoneNumberUtils.compare(contactNumber, prevNumber)||PhoneNumberUtils.compare(contactNumber, thisUserNumber)))
                    {
                        prevNumber=contactNumber;

                        count++;
                        ContactListData data=new ContactListData();
                        data.user_name=contactName;
                        data.number=phoneNumberValidation(contactNumber);
                        phoneContacts.add(data);
                    }
                    else
                    {
                        // Log.e("MSGG","==REMOVED "+contactName+" numner : "+contactNumber+" status : "+!PhoneNumberUtils.compare(contactNumber, contactNumber));

                    }

                }


                nativeUpdateContacts(context,phoneContacts,false);

                syncPhoneContactsWithAppContacts(phoneContacts);

            }
        };
        registerReceiver(phoneContactsUpdateReceiver, contactFilter);
        /*******broadcast for conatct update*******/


        ContactChangeObserver contactObserver=new ContactChangeObserver(new Handler(),getApplicationContext());
        getContentResolver().registerContentObserver(Contacts.CONTENT_URI,true,contactObserver);

       /* Log.e("GGG","change checking settings  "+PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getLong(PrefManager.LAST_CONTACT_SIZE,0)
        +" / / "+getcurrentContactSize()+" /// "+NetworkUtils.networkIsAvailable(getApplicationContext())
        +" / / "+PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getLong(PrefManager.LAST_CONTACT_TIMESTAMP,0)
        +" //// "+getLastContactUpdateTime());*/
        if((PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getLong(PrefManager.LAST_CONTACT_SIZE,0)!=getcurrentContactSize()||
                PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getLong(PrefManager.LAST_CONTACT_TIMESTAMP,0)<getLastContactUpdateTime())
                && NetworkUtils.networkIsAvailable(getApplicationContext()))
        {
            Intent intent = new Intent("GET_CONTACTS_UPDATED");
            sendBroadcast(intent);
        }

    }

    long getcurrentContactSize()
    {
        if(ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED)
          return 0;
        Cursor cur=getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
         null,null,null,null);
        return cur.getCount();
    }
    long getLastContactUpdateTime()
    {
        if(ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED)
            return 0;
        long timestamp=0;
        Cursor cur=getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                new String[]{ContactsContract.CommonDataKinds.Phone.CONTACT_LAST_UPDATED_TIMESTAMP},
                null,null,ContactsContract.CommonDataKinds.Phone.CONTACT_LAST_UPDATED_TIMESTAMP+" DESC ");
        if(cur.moveToNext())
            timestamp=cur.getLong(0);

        return timestamp;
    }

    String phoneNumberValidation(String num)
    {
        if(!num.contains("+"))
        {
            String simCountryIso = ((TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE)).getSimCountryIso();
            if(simCountryIso == null ) {
                simCountryIso = ((TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE)).getNetworkCountryIso();
            }
            if(simCountryIso != null ) {
                simCountryIso = simCountryIso.toUpperCase();
                PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
                try {
                    Phonenumber.PhoneNumber swissNumberProto = phoneUtil.parse(num, simCountryIso);
                    //if(num.contains("009"))
                        //Log.e("GGG","name phonevery "+phoneUtil.format(swissNumberProto, PhoneNumberUtil.PhoneNumberFormat.E164));

                    return  phoneUtil.format(swissNumberProto, PhoneNumberUtil.PhoneNumberFormat.E164);
                } catch (NumberParseException e) {
                    System.err.println("NumberParseException was thrown: " + e.toString());
                }
            }
        }


        return num.replaceAll("[()\\s-]","");
    }

    /*private class ContactsContentObserver extends ContentObserver {
        public ContactsContentObserver() {
            super(null);
        }

        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            Log.e("MSGG", "voooo changed");


        }
    }*/

    void syncPhoneContactsWithAppContacts(ArrayList<ContactListData> data)
    {
        // ArrayList<String> data1=new ArrayList<>();
        Log.e("GGG","change sync "+data.size());
        ArrayList<String> data2=new ArrayList<>();
        for(int i=0;i<data.size();i++) {
            data2.add(data.get(i).number);
        }

        ArrayList<String> appContacts1=new ArrayList<String>();
        ArrayList<String> appContacts2=new ArrayList<String>();

        /*data1.add("8138908161");
        data1.add("8606543912");
        data1.add("9731592516");
        data1.add("09443085174");


        for(int i=0;i<data1.size();i++) {
            Log.e("MSGG","num : "+data1.get(i)+" Formated: "+ PhoneNumberUtils.formatNumber(data1.get(i),"IN"));
        }*/


        SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();


        Cursor cursor = db.query(DatabaseAccesser.user_table,
                new String[]{

                        DatabaseAccesser.user_user_id,
                        DatabaseAccesser.user_name,
                        DatabaseAccesser.user_number,
                        DatabaseAccesser.user_localUrl,
                        DatabaseAccesser.user_globalUrl},
                DatabaseAccesser.user_user_type + " =?",
                new String[]{"2"},
                null, null, null,null);

        while (cursor.moveToNext()) {

            ContactListData tempContact=new ContactListData();
            tempContact.number=cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_number));
            tempContact.user_id=cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_user_id));

            appContacts1.add(tempContact.user_id);
            appContacts2.add(tempContact.number);

        }
        cursor.close();

        for(int i=0;i<appContacts2.size();i++)
        {
            if(!data2.contains(appContacts2.get(i)))
            {
                //Log.e("MSGG","removed"+ appContacts2.get(i));
                FirebaseDatabase.getInstance().getReference().child("user_profile").child(userdetail.getUid()).child("contacts").child(appContacts1.get(i)).child("contact").removeValue();
                removeContactBasedOnPhoneNumber(appContacts1.get(i),appContacts2.get(i));
            }
        }

        for(int i=0;i<data2.size();i++)
        {
            if(!appContacts2.contains(data2.get(i)))
            {
                //Log.e("MSGG","add "+ appContacts.get(i).number);
                // CODDE TO FETCH NEW CONTACTS THAT ARE NOT INSIDE THE APP
            }

        }
        Log.e("GGG","change saving size  "+getcurrentContactSize());
        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putLong(PrefManager.LAST_CONTACT_SIZE,getcurrentContactSize()).commit();
        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putLong(PrefManager.LAST_CONTACT_TIMESTAMP,getLastContactUpdateTime()).commit();
    }

    void removeContactBasedOnPhoneNumber(String userid,String phoneNumber)
    {
        SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put(DatabaseAccesser.user_user_type,1);
        db.update(DatabaseAccesser.user_table,values,DatabaseAccesser.user_user_id + "=?",
                new String[]{userid});
        //db.delete(DatabaseAccesser.user_table, DatabaseAccesser.user_number + "=?",
                //new String[]{phoneNumber});
    }

    void nativeUpdateContacts(final Context context,final ArrayList<ContactListData> phonecontacts,boolean basedOnID)
    {

       // Log.e("GGG","change native contact update "+phonecontacts.size());
        for(int i=0;i<phonecontacts.size();i++)
        {
            //if(!phonecontacts.get(i).number.contains("7012153009"))
            //continue;

            Log.e("GGG","contact2 checking  /"+phonecontacts.get(i).number+" / "+phonecontacts.get(i).user_name);
            final int k=i;
            fetchUserDetailsbyIDOrPhone(phonecontacts.get(i).number,false, true,new ProcessCompletedListner() {
                @Override
                public void processCompleteAction() {}

                @Override
                public void newUserAdded(UserWholeData data) {
                    Log.e("GGG","statustime is2 "+data.status_timestamp+" / "+data.user_name);
                    data.user_name=phonecontacts.get(k).user_name;
                    data.type=2;
                    updateOrInsertUserIfNone(context,data, new JobCompletionListner() {
                        @Override
                        public void onJobCompletion(String txt1) {

                            Intent intent = new Intent("CONTACTS_UPDATED");
                            sendBroadcast(intent);
                            //if(AllUsersList.)
                            //Log.e("MSG","=new user added");
                        }
                    });


                }
            });
        }
    }

    public static void updateOrInsertUserIfNone(Context context,final UserWholeData contact, final JobCompletionListner listn)
    {

        SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();

        Cursor cursor = db.query(DatabaseAccesser.user_table,
                new String[]{
                        DatabaseAccesser.user_user_id,
                        DatabaseAccesser.user_name,
                        DatabaseAccesser.user_number,
                        DatabaseAccesser.user_localUrl,
                        DatabaseAccesser.user_globalUrl,
                        DatabaseAccesser.user_music_globalUrl},
                DatabaseAccesser.user_user_id + " =?",
                new String[]{contact.user_id},
                null, null, null,null);

        if (cursor.getCount() == 1)
        {

            cursor.moveToNext();
            //Log.e("MSGG","=update "+cursor.getString(cursor.getColumnIndexOrThrow(DatabaseAccesser.user_number)));
            if(!contact.img_url.equals(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseAccesser.user_globalUrl))))
            {

                //Log.e("MSGG","=url not same");
                MainServiceThread.downloadFileAndReturnUriString(context,contact.img_url, false,2,null,new FileDownloadListner() {
                    @Override
                    public void onProgressListener(int percent) {

                    }

                    @Override
                    public void onSuccessListener(String localFilename) {
                        String globalUrl= contact.img_url;
                        contact.img_url = localFilename;
                        TabbedActivity.saveContactToDatabase(contact,globalUrl,false);
                        if(listn!=null)
                            listn.onJobCompletion("true");
                    }

                    @Override
                    public void onFailureListener(int percent) {

                    }

                    @Override
                    public void onPausedListener(int percent) {

                    }
                });
            }
            else
            {
                TabbedActivity.updateContactStatusAndPrivacy(contact);
                if(listn!=null)
                    listn.onJobCompletion("true");
              //+contact.img_url+"\n"+ cursor.getString(cursor.getColumnIndexOrThrow(DatabaseAccesser.user_globalUrl)));
            }

            //Log.e("MGG","media is "+ contact.music_url );
            //Log.e("MGG","media is null for "+ contact.user_name);
            //Log.e("MGG","media is "+ cursor.getString(cursor.getColumnIndexOrThrow(DatabaseAccesser.user_music_globalUrl)) );

            if(!contact.music_url.equals(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseAccesser.user_music_globalUrl))))
            {
                TabbedActivity.saveContactMusicToDatabase(contact.user_id,contact.music_url,null,contact.status_timestamp);
            }


        }
        else
        {
            //Log.e("MSGG","=insert");
            MainServiceThread.downloadFileAndReturnUriString(context,contact.img_url,false,2,null, new FileDownloadListner() {
                @Override
                public void onProgressListener(int percent) {

                }

                @Override
                public void onSuccessListener(String localFilename) {
                    FirebaseDatabase.getInstance().getReference().child("user_profile").child(userdetail.getUid()).child("contacts").child(contact.user_id).child("contact").setValue("1");

                    String globalUrl= contact.img_url;
                    contact.img_url = localFilename;
                    TabbedActivity.saveContactToDatabase(contact,globalUrl,true);
                    TabbedActivity.saveContactMusicToDatabase(contact.user_id,contact.music_url,null,contact.status_timestamp);  // CODE 10 can merge this fn with "saveContactToDatabase". i did this to save time
                    if(listn!=null)
                        listn.onJobCompletion("true");
                }

                @Override
                public void onFailureListener(int percent) {

                }

                @Override
                public void onPausedListener(int percent) {

                }
            });

        }
    }


    static void fetchRemoteUserStatusOfThisUser(final String tragetID,final RemoteStatusFetchListener listner)
    {
        Log.e("GGG","contact5 GO FETCH "+tragetID+" / "+FirebaseAuth.getInstance().getUid());
        FirebaseDatabase.getInstance().getReference().child("user_profile").child(tragetID).child("contacts").
                child(FirebaseAuth.getInstance().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                HashMap<String,Integer> map=new HashMap<>();
                Log.e("GGG","contact5 FETCH FOUND"+tragetID);
                if(dataSnapshot.child("contact").getValue()==null)
                    //listner.onJobCompletion("0");
                    map.put("contact",0);
                else
                    //listner.onJobCompletion("1");
                    map.put("contact",1);

                if(dataSnapshot.child("block").getValue()==null)
                    map.put("block",0);
                else
                    map.put("block",1);

                listner.onRemoteStatusfetched(map);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("GGG","contact5 FETCH CANCELS "+tragetID);
            }
        });
    }



//simply fetches a user detail

    public static void fetchUserDetailsbyIDOrPhone(final String userData,final boolean byUserName,final boolean bulk, final ProcessCompletedListner listn)
    {
        if(!byUserName)
        {
            //Log.e("GGG","contact 1 "+userData);
            FirebaseDatabase.getInstance().getReference().child("user_profile").orderByChild("number").equalTo(userData).limitToFirst(1)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            //
                            //if(userData.equals("+917012153008"))
                                //Log.e("GGG","contact5  got data "+userData+" / ");
                            for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {

                                ContactListData dat=postSnapshot.getValue(ContactListData.class);
                                dat.music_url=postSnapshot.child("music_url").getValue().toString();

                                dat.type=2;

                                UserPrivacyData privacy =getPrivacyFromuserSnapshot(postSnapshot);
                                final UserWholeData data=new UserWholeData(dat,privacy);
                               // if(userData.equals("+917012153008"))
                                    Log.e("GGG","contact5  INDI data "+userData+" / "+data.user_id);
                                    if(!bulk) {
                                        fetchRemoteUserStatusOfThisUser(data.user_id, new RemoteStatusFetchListener() {
                                            @Override
                                            public void onRemoteStatusfetched(HashMap<String, Integer> txt1) {
                                                Log.e("GGG", "contact5  FETCHI data " + userData + " / ");
                                                data.remoteContact = txt1.get("contact");
                                                data.remoteBlock = txt1.get("block");
                                                listn.newUserAdded(data);
                                            }
                                        });
                                    }
                                    else{
                                //data.remoteContact=Integer.parseInt("1");
                                //data.remoteBlock=Integer.parseInt("0");
                                listn.newUserAdded(data);}


                            }

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                            listn.processCompleteAction();

                        }
                    });
        }
        else
        {
            FirebaseDatabase.getInstance().getReference().child("user_profile").child(userData)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            ContactListData dat=dataSnapshot.getValue(ContactListData.class);
                            if(dat==null){
                                listn.processCompleteAction();
                                return;
                            }
                            UserPrivacyData privacy =getPrivacyFromuserSnapshot(dataSnapshot);
                            UserWholeData data=new UserWholeData(dat,privacy);
                            listn.newUserAdded(data);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                            listn.processCompleteAction();

                        }
                    });
        }
    }

    public static UserPrivacyData getPrivacyFromuserSnapshot(DataSnapshot dataSnapshot)
    {
        DataSnapshot temp=dataSnapshot.child("user_privacy");
        if(temp==null)
            return null;

        final UserPrivacyData postPrivacy=new UserPrivacyData();
        postPrivacy.activity_status=Integer.parseInt(temp.child("activity_status").getValue().toString());
        postPrivacy.profile_picture=Integer.parseInt(temp.child("profile_picture").getValue().toString());
        postPrivacy.profile_status=Integer.parseInt(temp.child("profile_status").getValue().toString());
        postPrivacy.number=Integer.parseInt(temp.child("number").getValue().toString());
        postPrivacy.location=Integer.parseInt(temp.child("location").getValue().toString());
        postPrivacy.anyonefollow=Integer.parseInt(temp.child("anyonefollow").getValue().toString());
        postPrivacy.anyonemessage=Integer.parseInt(temp.child("anyonemessage").getValue().toString());

        return postPrivacy;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        mainService=this;
        if(x==null)
        {
            writableDB = new DatabaseAccesser(getApplicationContext()).getWritableDatabase();
            readableDB=new DatabaseAccesser(getApplicationContext()).getReadableDatabase();

           // mainService=this;


            userdetail= FirebaseAuth.getInstance().getCurrentUser();
            if(userdetail==null)
            {
                x=null;
                Toast.makeText(this, "No creta", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(this, "creeeeeta", Toast.LENGTH_SHORT).show();
                x=userdetail.getUid();
                newPostRef = FirebaseDatabase.getInstance().getReference().child("chat_data").child(x);
                chatReceiveRef = FirebaseDatabase.getInstance().getReference().child("chat_status").child(x);

                notificationReceiveRef = FirebaseDatabase.getInstance().getReference().child("notifi_data").child(x);

                groupsRef = FirebaseDatabase.getInstance().getReference().child("chat_group");
                updateFeedPersistanceAndChatPersistance(null,null);
                update_content();
                handlePendingProcess();
                listenToGroupChats();
                update_chat_status();
                update_notifications();
                sendUnsentMessages(getApplicationContext());
            }

            /*SQLiteDatabase db = new DatabaseAccesser(getApplicationContext()).getReadableDatabase();

            Cursor cursor = db.query(DatabaseAccesser.user_table,
                    new String[]{

                            DatabaseAccesser.user_user_id,DatabaseAccesser.user_userme},
                    DatabaseAccesser.user_userme + " =?",
                    new String[]{"1"},
                    null, null, null,null);

            while (cursor.moveToNext()) {

                x = cursor.getString(
                        cursor.getColumnIndexOrThrow(DatabaseAccesser.user_user_id));

            }
            cursor.close();*/
            if(x!=null) {

            }

        }
       // return super.onStartCommand(intent, flags, startId);
        return START_STICKY;

    }

public static void sendUnsentMessages(Context context)
{
    SQLiteDatabase db = DatabaseManager.getDatabase(context).getReadableDatabase();
    //String userSelection;
    Cursor cursor;

    cursor = db.rawQuery(
            "SELECT *,"+DatabaseAccesser.chat_table+".rowid AS rowID"+" FROM " + DatabaseAccesser.chat_table +
                    " INNER JOIN " + DatabaseAccesser.user_table + " ON " +
                    DatabaseAccesser.chat_table + "." + DatabaseAccesser.chat_userid + " = " + DatabaseAccesser.user_table + "." + DatabaseAccesser.user_user_id +
                    " INNER JOIN "+DatabaseAccesser.media_table+" ON "+
                    DatabaseAccesser.chat_table+"."+DatabaseAccesser.chat_col_media_id+" = "+ DatabaseAccesser.media_table+".rowid"+
                    " INNER JOIN "+DatabaseAccesser.replay_table+" ON "+
                    DatabaseAccesser.chat_table+"."+DatabaseAccesser.chat_col_replay_id+" = "+ DatabaseAccesser.replay_table+".rowid"+
                   // " WHERE "+DatabaseAccesser.chatList_table + "."+DatabaseAccesser.chatList_chat_hidden +" =? "+
                    " WHERE "+DatabaseAccesser.chat_table + "." + DatabaseAccesser.chat_ifseen+" =? AND "
                    +DatabaseAccesser.chat_table + "." + DatabaseAccesser.chat_sender+" =? AND "+
                    DatabaseAccesser.chat_table + "." + DatabaseAccesser.chat_schedule+" <=?"+
                    " ORDER BY " + DatabaseAccesser.chat_table + "." + DatabaseAccesser.chat_timestamp +" ASC ",
            new String[]{"0","0",String.valueOf(System.currentTimeMillis())});

    Log.e("GGG","unsendMessages vount "+cursor.getCount());
    while (cursor.moveToNext()) {

        //GroupChatData temp = new GroupChatData();
        Intent myIntent = new Intent(context, AroundBroadcastReceiver.class);
        myIntent.setAction("com.hash.around.MESSAGE_SCHEDULE");
        myIntent.putExtra("chatrow",cursor.getLong(
                cursor.getColumnIndexOrThrow("rowID")));
        myIntent.putExtra("usertype",cursor.getInt(
                cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_type)));
        myIntent.putExtra("userid",cursor.getString(
                cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_userid)));
        myIntent.putExtra("intent_type",0);
        //Intent new_intent = new Intent("CHAT_UPDATED");
        context.sendBroadcast(myIntent);

    }
}
    void updateFeedPersistanceAndChatPersistance(Integer listenerTager,JobCompletionListner listener)
    {
        int feedcount=0,chatCount=0;
        SQLiteDatabase db=DatabaseManager.getDatabase(getApplicationContext()).getWritableDatabase();
        if(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getBoolean(PrefManager.PREF_FEED_AUTO_DELETE,false))
        {
            Log.e("GGG","feeddelete is persitance");
            int feedDeleteSize=Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(PrefManager.PREF_FEED_AUTO_DELETE_SIZE,"50"));
            Cursor cursor=db.query(DatabaseAccesser.feed_table,null,DatabaseAccesser.feed_col_user_id+"!=?",new String[]{FirebaseAuth.getInstance().getUid()},null,null,null);
            Log.e("GGG","feeddelete counts "+cursor.getCount()+" / "+feedDeleteSize);

            if(cursor.getCount()>feedDeleteSize)
            {
                int deleteAmount=cursor.getCount()-feedDeleteSize;
                Log.e("GGG","feeddelete DELET first "+deleteAmount);
                Cursor cursor1=db.query(DatabaseAccesser.feed_table,new String[]{"rowid as rowID","*"},DatabaseAccesser.feed_col_user_id+" !=?",new String[]{FirebaseAuth.getInstance().getUid()},null,null,DatabaseAccesser.feed_col_timestamp+" ASC ",deleteAmount+"");
                Log.e("GGG","feeddelete found "+cursor1.getCount());
                while (cursor1.moveToNext())
                {
                    feedcount+=db.delete(DatabaseAccesser.feed_table,"rowid =?",new String[]{String.valueOf(cursor1.getLong(cursor1.getColumnIndexOrThrow("rowID")))});
                }
                //db.rawQuery("DELETE FROM "+DatabaseAccesser.feed_table+" WHERE "+DatabaseAccesser.feed_col_feed_id+" IN(+")",null);

               /* DELETE FROM ranking WHERE id NOT IN (
                    SELECT id FROM ranking ORDER BY score DESC LIMIT 100);*/

            }
            db.delete(DatabaseAccesser.feed_table,DatabaseAccesser.feed_col_timestamp+"<? AND "+DatabaseAccesser.feed_col_user_id+"=?",new String[]{String.valueOf(System.currentTimeMillis()- TimeUnit.DAYS.toMillis(1)),FirebaseAuth.getInstance().getUid()});

        }
        else
        {

            long rowln=db.delete(DatabaseAccesser.feed_table,DatabaseAccesser.feed_col_timestamp+"<?",new String[]{String.valueOf(System.currentTimeMillis()- TimeUnit.DAYS.toMillis(1))});
            Log.e("GGG","feeddelete is NOT persitance "+rowln);
        }

        if(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getBoolean(PrefManager.PREF_CHAT_AUTO_DELETE,false))
        {
            int chatDeleteSize=Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(PrefManager.PREF_CHAT_AUTO_DELETE_SIZE,"300"));

            Cursor cursor2 = db.rawQuery(
                    "SELECT count(*) as chatcount,* FROM " + DatabaseAccesser.chat_table +
                            " GROUP BY "+DatabaseAccesser.chat_userid,
                    null);

            while(cursor2.moveToNext())
            {
                int userChatSize=cursor2.getInt(cursor2.getColumnIndexOrThrow("chatcount"));
                String userChatUserID=cursor2.getString(cursor2.getColumnIndexOrThrow(DatabaseAccesser.chat_userid));
                Log.e("GGG","chatdelete list "+userChatUserID+" counts: "+userChatSize+" limit: "+chatDeleteSize);
                if(userChatSize>chatDeleteSize)
                {
                    int deleteAmount=userChatSize-chatDeleteSize;
                    Log.e("GGG","chatdelete DELET first "+deleteAmount);
                    Cursor cursor3=db.query(DatabaseAccesser.chat_table,new String[]{"rowid as rowID","*"},DatabaseAccesser.chat_userid+" =?",new String[]{userChatUserID},null,null,DatabaseAccesser.chat_timestamp+" ASC ",deleteAmount+"");
                    Log.e("GGG","chatdelete found "+cursor3.getCount());
                    while (cursor3.moveToNext())
                    {
                        chatCount+=db.delete(DatabaseAccesser.chat_table,"rowid =?",new String[]{String.valueOf(cursor3.getLong(cursor3.getColumnIndexOrThrow("rowID")))});
                    }
                }
            }
        }

        if(listenerTager!=null)
        {
            if (listener!=null)
            {
                if(listenerTager==0)
                    listener.onJobCompletion(getApplicationContext().getResources().getQuantityString(R.plurals.x_feeds_removed, feedcount, feedcount) +"");
                else if(listenerTager==1)
                    listener.onJobCompletion(getApplicationContext().getResources().getQuantityString(R.plurals.x_chats_removed, chatCount, chatCount) +"");

            }
        }


    }

    void update_notifications()
    {


        //final Context p=getActivity();
        // if(childEventListener==null)


        if(childEventListenerForNotification==null) {

            childEventListenerForNotification = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                    final NotificationData post = dataSnapshot.getValue(NotificationData.class);
                    //post.sender = 1;
                    //finaldata.add(post);
                    switch(post.type)
                    {
                        case 0:
                            saveNotificationToDatabase(post);

                            break;
                        case 1:
                            saveNotificationToDatabase(post);
                            break;
                        case 2:

                            StorageReference httpsReference = FirebaseStorage.getInstance().getReferenceFromUrl(post.image);
                            File localFile = null;
                            try {
                                localFile = File.createTempFile("images", "png");
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            httpsReference.getBytes(1024 * 1024).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                                @Override
                                public void onSuccess(byte[] bytes) {
                                    try {

                                        File file = new File(Environment.getExternalStorageDirectory().getPath(), "Around Files/Media");
                                        if (!file.exists()) {
                                            file.mkdirs();
                                        }
                                        String name = StringUtils.randomStringGenerator() + ".png";
                                        FileOutputStream fos = new FileOutputStream(new File(file.getAbsolutePath(), name));
                                        fos.write(bytes);
                                        fos.close();
                                        post.image = file.toURI().toString() + name;
                                        saveNotificationToDatabase(post);

                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    // Handle any errors
                                }
                            });

                            break;

                        case 3:
                            UserFollowApproved(getApplicationContext(),post.userid);
                            saveNotificationToDatabase(post);

                            //follow accepted
                            break;
                        case 4:
                            UserChatApproved(getApplicationContext(),post.userid);
                            saveNotificationToDatabase(post);
                            //chat accepted
                            break;
                        case 26:  //follow rejected


                            break;
                        case 27:  //chat rejected


                            break;

                        case 11:
                           // Log.e("MGG","new follow REQUEST");
                            //CODE UNKNOWN showin deffault notification if folly numberType is not private
                            saveNotificationToDatabase(post);
                            //follow request
                            break;
                        case 12:
                           // Log.e("MGG","new chat REQUEST");
                            saveNotificationToDatabase(post);
                            //chat request
                            break;
                        case 15:
                           //Log.e("GGG","new group");
                            Log.e("GGG","groupmultiple2 new group");
                            addIncommingNewGroup(post.text,post.timestamp);
                            break;

                        case 21: //block

                            addNewIncommingBlockAndUnblock(post.userid,true);

                            break;

                        case 22:  //unblock
                            addNewIncommingBlockAndUnblock(post.userid,false);

                            break;
                        case 100:

                           DatabaseManager. saveFeedSeenStatus(getApplicationContext(),post);
                        Log.e("GGG","notification received 100");
                            break;


                    }
                    dataSnapshot.getRef().removeValue();


                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            };


//                      Toast.makeText(getActivity(), "hurray", Toast.LENGTH_SHORT).show();
            // ...
            //newPostRef.goOffline();
            //newPostRef.removeEventListener(childEventListener);

            // Toast.makeText(this, "creeeeeta", Toast.LENGTH_SHORT).show();
            notificationReceiveRef.addChildEventListener(childEventListenerForNotification);

        }

    }


    static void fecthSingleUserPrivacyStatus(String user,final UserPrivacyStatusListner lstn) {
        FirebaseDatabase.getInstance().getReference().child("user_profile").child(user).child("user_privacy")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        UserPrivacyData post=new UserPrivacyData();

                        post.profile_picture=Integer.parseInt(dataSnapshot.child("profile_picture").getValue().toString());
                        post.profile_status=Integer.parseInt(dataSnapshot.child("profile_status").getValue().toString());
                        post.number=Integer.parseInt(dataSnapshot.child("number").getValue().toString());
                        post.activity_status=Integer.parseInt(dataSnapshot.child("activity_status").getValue().toString());
                        post.location=Integer.parseInt(dataSnapshot.child("location").getValue().toString());
                        post.anyonefollow=Integer.parseInt(dataSnapshot.child("anyonefollow").getValue().toString());
                        post.anyonemessage=Integer.parseInt(dataSnapshot.child("anyonemessage").getValue().toString());



                        if(lstn!=null)
                            lstn.onUserStatusReceieved(post);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public static void newUserFollowRequestWithDoubleCheck(final Activity context,final String uID, final JobCompletionListner listner) {

        //context.findViewById()

        fecthUserPrivacyStatus(uID, new UserPrivacyStatusListner() {
            @Override
            public void onUserStatusReceieved(UserPrivacyData privacy) {
                int aa = privacy.anyonefollow;
                if (aa == 2) {

                    Snackbar snack =Snackbar.make(context.findViewById(android.R.id.content), R.string.this_user_is_having_private_account, Snackbar.LENGTH_LONG)
                            .setAction(R.string.send_request, new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    AroundAlertBoxManager.showProgress(context,null,R.string.sending_follow_request,true);
                                    sentCustomNotificationToUser(context,uID, 11, "", new JobCompletionListner() {
                                        @Override
                                        public void onJobCompletion(String txt1) {
                                            AroundAlertBoxManager.cancelProgress();
                                            updateUserFollowStatus(context,uID,3);
                                            if (listner != null)
                                                listner.onJobCompletion("3");
                                        }
                                    });
                                }
                            });
                    ((TextView) snack.getView().findViewById(android.support.design.R.id.snackbar_text)).setMaxLines(5);
                    ViewGroup.LayoutParams params=snack.getView().getLayoutParams();
                    /*params.setMargins(params.leftMargin + sideMargin,
                            params.topMargin,
                            params.rightMargin + sideMargin,
                            params.bottomMargin + marginBottom);*/
                    snack.show();

                } else {
                    UserFollowApproved(context,uID);

                    if (listner != null)
                        listner.onJobCompletion("1");
                }
            }
        });
    }


    static void UserFollowApproved(Context context,String userID)
    {
        updateUserFollowStatus(context,userID,1);
    }

    public static void updateuserFollowStatusWithRequestedCheck(final Activity activity,int followType,final String uID, final JobCompletionListner listner)
    {
        if(followType==3) {
            Snackbar snack = Snackbar.make(activity.findViewById(android.R.id.content), R.string.follow_request_still_pending_with_unfollow, Snackbar.LENGTH_LONG)
                    .setAction(R.string.unfollow, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            updateUserFollowStatus(activity,uID, 0);
                            //userData.followType=0;
                            // updateFollowDetails();
                            listner.onJobCompletion("0");
                        }
                    });
            ((TextView) snack.getView().findViewById(android.support.design.R.id.snackbar_text)).setMaxLines(5);
            snack.show();
        }
        else
        {
            updateUserFollowStatus(activity,uID, 0);
            listner.onJobCompletion("0");
        }

    }

    public  static void updateUserFollowStatus(Context context,final String uID,int status)
    {
        final SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();
        final ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.user_user_follow_type, status);

        long newRowId = db.update(DatabaseAccesser.user_table, values, DatabaseAccesser.user_user_id + " =? ", new String[]{uID});

        if (newRowId == 0) {
            lightningEasyUserUpdateListner(context,uID, null, new JobCompletionListner() {
                @Override
                public void onJobCompletion(String txt1) {
                    long newRowId= db.update(DatabaseAccesser.user_table, values, DatabaseAccesser.user_user_id + " =? ", new String[]{uID});
                }
            });
        }
    }

    public static void newUserChatRequestWithDoubleCheck(final Context context,final String uID, final JobCompletionListner listner) {

        fecthUserPrivacyStatus(uID, new UserPrivacyStatusListner() {
            @Override
            public void onUserStatusReceieved(UserPrivacyData privacy) {
                int aa = privacy.anyonemessage;
                if (aa == 2) {
                    sentCustomNotificationToUser(context,uID, 11, "", new JobCompletionListner() {
                        @Override
                        public void onJobCompletion(String txt1) {
                            updateUserChatStatus(context,uID,3);
                            if (listner != null)
                                listner.onJobCompletion("Request sent");
                        }
                    });
                } else {

                    UserChatApproved(context,uID);
                    if (listner != null)
                        listner.onJobCompletion("Chat Now");
                }
            }
        });
    }


    static  void UserChatApproved(Context context,String userID)
    {
        updateUserChatStatus(context,userID,1);
    }

    public static void updateUserChatStatus(Context context,final String userID,int type)
    {
        final SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();
        final ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.user_user_chat_type, type);

        long newRowId = db.update(DatabaseAccesser.user_table, values, DatabaseAccesser.user_user_id + " =? ", new String[]{userID});

        if (newRowId == 0) {
            lightningEasyUserUpdateListner(context,userID, null, new JobCompletionListner() {
                @Override
                public void onJobCompletion(String txt1) {
                    long newRowId = db.update(DatabaseAccesser.user_table, values, DatabaseAccesser.user_user_id + " =? ", new String[]{userID});
                }
            });
        }
    }

    void addNewIncommingBlockAndUnblock(final String userID, boolean block)
    {
        final SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();

        final ContentValues values = new ContentValues();


        /*if(block)
            values.put(DatabaseAccesser.user_user_follow_type, 5);
        else
            values.put(DatabaseAccesser.user_user_follow_type, 1);*/

       /* lightningEasyUserUpdateListner(userID, block?12:0,new JobCompletionListner() {
            @Override
            public void onJobCompletion(String txt1) {
                long newRowId = db.update(DatabaseAccesser.user_table, values, DatabaseAccesser.user_user_id + " =? ", new String[]{userID});
            }
        });*/



    }

    public static void lightningEasyUserUpdateListner(final Context context,String userID,final Integer follow_type,final JobCompletionListner allComplete)
    {
        fetchUserDetailsbyIDOrPhone(userID, true,false, new ProcessCompletedListner() {
            @Override
            public void processCompleteAction() {}

            @Override
            public void newUserAdded(UserWholeData data) {

                if(follow_type!=null)
                {
                    data.followType=follow_type;
                }
                updateOrInsertUserIfNone(context,data, new JobCompletionListner() {
                    @Override
                    public void onJobCompletion(String txt1) {
                        allComplete.onJobCompletion("done");
                    }
                });

            }
        });
    }

    void addIncommingNewGroup(final String groupID,final long addTime)
    {
        //Log.e("MSGG","inclimming group");
        Log.e("GGG","groupmultiple2 inclimming");
        FirebaseDatabase.getInstance().getReference().child("user_profile").child(groupID)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //Log.e("GGG","new group got data");
                        //final ContactListData post= dataSnapshot.getValue(ContactListData.class);
                        final ContactListData contact2 = new ContactListData();
                        //Log.e("MSG",""+groupID);
                        contact2.img_url= dataSnapshot.child("img_url").getValue().toString();
                        contact2.type= Integer.parseInt(dataSnapshot.child("type").getValue().toString());
                        contact2.user_id= dataSnapshot.child("user_id").getValue().toString();
                        contact2.user_name= dataSnapshot.child("user_name").getValue().toString();
                        contact2.status=dataSnapshot.child("status").getValue().toString();
                        contact2.music_url=dataSnapshot.child("music_url").getValue().toString();
                        contact2.location=dataSnapshot.child("location").getValue().toString();

                        Log.e("GGG","groupmultiple2 group saved");
                        //Log.e("MSG",contact2.img_url);
                        //Log.e("GGG","new group starting downloadkolakola");
                        downloadFileAndReturnUriString(getApplicationContext(),contact2.img_url,false,2,null, new FileDownloadListner() {
                            @Override
                            public void onProgressListener(int percent) {

                            }

                            @Override
                            public void onSuccessListener(String localFilename) {
                                String globalUrl = contact2.img_url;
                                contact2.img_url = localFilename;
                                Log.e("GGG","groupmultiple1 image saved");

                                removeAllMembersFromAGroup(getApplicationContext(),groupID);

                                FirebaseDatabase.getInstance().getReference().
                                        child("chat_group").child(groupID).child("group_members").addChildEventListener(new ChildEventListener() {
                                    @Override
                                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                        GroupMemberData memberData= dataSnapshot.getValue(GroupMemberData.class);
                                        updateGroupMemberInDatabase(memberData,groupID);
                                        //Log.e("GGG","group new member member :" +memberData.userid);

                                        Log.e("GGG","groupmultiple1 child "+memberData.userid+" / "+FirebaseAuth.getInstance().getUid());
                                        if(memberData.userid.equals(FirebaseAuth.getInstance().getUid()))
                                        {
                                            Log.e("GGG","groupmultiple1 listentoSingleGroupChats");
                                            listentoSingleGroupChats(groupID, addTime);
                                        }
                                    }

                                    @Override
                                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                                    }

                                    @Override
                                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                                    }

                                    @Override
                                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });
                                // saveGroupToDatabase(contact2, globalUrl);
                                TabbedActivity.saveContactToDatabase(new UserWholeData(contact2,new UserPrivacyData().setFullPublicPrivacy()), globalUrl,true);


                            }

                            @Override
                            public void onFailureListener(int percent) {

                            }

                            @Override
                            public void onPausedListener(int percent) {

                            }
                        });
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

        // updateChatlistWithGroup

    }

    void removeAllMembersFromAGroup(Context context,String groupID)
    {
        long  newRowId = writableDB.delete(DatabaseAccesser.groupsmembers_table, DatabaseAccesser.groupsmembers_group_id+"=?", new String[]{groupID});
    }

    void updateGroupMemberInDatabase(GroupMemberData data,String grp)
    {

        ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.groupsmembers_group_id,grp);
        values.put(DatabaseAccesser.groupsmembers_user_id, data.userid);
        values.put(DatabaseAccesser.groupsmembers_member_type, data.member_type);

        long  newRowId = writableDB.insert(DatabaseAccesser.groupsmembers_table, null, values);
        Log.e("GGG","group new member member inserted at "+newRowId+" / group "+grp+" / "+data.userid);

    }


    void updateChatlistWithGroup(ContactListData contact,String groupID)
    {
        //SQLiteDatabase db = dbAccess.getWritableDatabase();
        ContentValues values2 = new ContentValues();
        values2.put(DatabaseAccesser.chatList_lastmsg, "You were added!");
        values2.put(DatabaseAccesser.chatList_lastmsg_type, 0);
        values2.put(DatabaseAccesser.chatList_lastmsgbyme, 0);
        values2.put(DatabaseAccesser.chatList_msgqueue, 1);
        values2.put(DatabaseAccesser.chatList_lastmsgRowId, 0);
        values2.put(DatabaseAccesser.chatList_last_msgseen, 0);
        values2.put(DatabaseAccesser.chatList_name, contact.user_name);
        values2.put(DatabaseAccesser.chatList_userid, groupID);
        values2.put(DatabaseAccesser.chatList_timestamp, System.currentTimeMillis());
        values2.put(DatabaseAccesser.chatList_url, contact.img_url);

        int rowln=writableDB.update(DatabaseAccesser.chatList_table,values2, DatabaseAccesser.chatList_userid+" =?",new String[]{groupID});
        long srowln=0;
        if(rowln==0) {
            // Log.e("MSGG","=== insert");
            srowln = writableDB.insert(DatabaseAccesser.chatList_table, null, values2);

        }

        //Log.e("MSGG",rowln+ " :@: "+srowln);

        Intent new_intent = new Intent();
        new_intent.setAction("CHAT_UPDATED");
        sendBroadcast(new_intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        newPostRef.removeEventListener(childEventListener);
        chatReceiveRef.removeEventListener(chatReceiveEventListener);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    void update_group_chat_status(final String grpID)
    {
        Log.e("GGG","updaing seeeeen "+grpID);
        DatabaseReference databaseRef = groupsRef.child(grpID).child("chat_status");
        databaseRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                //Log.e("MSG","keeee : "+dataSnapshot.getKey());
                update_GroupChatSeenOnDatabase(dataSnapshot.getKey(),
                        dataSnapshot.child("last_rece_time").getValue().toString(),true,grpID);
                if(dataSnapshot.child("last_seen_time").getValue()!=null){
                update_GroupChatSeenOnDatabase(dataSnapshot.getKey(),
                       dataSnapshot.child("last_seen_time").getValue().toString(),false,grpID);}

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    void update_chat_status()
    {//xxx code 1
        if(chatReceiveEventListener==null) {
            chatReceiveEventListener=new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                    onChildChanged(dataSnapshot,s);
                    //Toast.makeText(MainServiceThread.this, "aaa added01", Toast.LENGTH_SHORT).show();
                    Log.e("GGG","child added ");

                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {


                    for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                        Log.e("GGG","lastFinalData child change ");
                        //Log.e("GGG","datasnapshot " +postSnapshot.child("last_msg").getValue());

                        int type=Integer.parseInt(postSnapshot.child("numberType").getValue().toString());
                        String timestamp=postSnapshot.child("timestamp").getValue().toString();
                        String lastsl=postSnapshot.child("last_msg").getValue().toString();

                        update_chatSeenOnDatabase(getApplicationContext(),dataSnapshot.getKey(),lastsl,type,timestamp);

                        postSnapshot.getRef().removeValue();
                    }

                    //update_chatSeenOnDatabase(dataSnapshot.getKey(),dataSnapshot.child("last_rece_msg").getValue().toString(),2);
                    //update_chatSeenOnDatabase(dataSnapshot.getKey(),dataSnapshot.child("last_seen_msg").getValue().toString(),3);

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            };

            chatReceiveRef.addChildEventListener(chatReceiveEventListener);
        }

    }

    void update_GroupChatSeenOnDatabase(String userID,String timeValue,boolean isreceive,final String grp)
    {

        //SQLiteDatabase db= TabbedActivity.db.getWritableDatabase();
        ContentValues values = new ContentValues();
        int status=0;
        if(isreceive) {
            values.put(DatabaseAccesser.groupsmembers_last_rece_time, timeValue);

            status=2;
        }
        else {
            values.put(DatabaseAccesser.groupsmembers_last_seen_time, timeValue);
            status = 3;
        }



        int rowln=writableDB.update(DatabaseAccesser.groupsmembers_table,values,
                DatabaseAccesser.groupsmembers_group_id+" =? AND "+
                        DatabaseAccesser.groupsmembers_user_id+" =?",new String[]{grp,userID});


        ContentValues values2 = new ContentValues();

        long timeData=getLeastTimeAlluserSeenChatInGroup(grp,status);
        timeData++;

        if(status==2)
            Log.e("MSGG","least time : "+timeData);

        values2.put(DatabaseAccesser.chat_ifseen, status);

        int rowln2=writableDB.update(DatabaseAccesser.chat_table,values2,
                DatabaseAccesser.chat_groupid+" =? AND "+
                        DatabaseAccesser.chat_timestamp+" <=? AND "+
                        DatabaseAccesser.chat_ifseen+" <?",new String[]{grp,timeData+"",status+""});

        //Log.e("MSGG","total chat update /: " + rowln2+" status :"+ status);

        ChatSender.updateChatWindowHEAVY(grp);
        /*if(ChatWindow.uId!=null)
        {
            if(ChatWindow.uId.equals(grp))
            {

                ChatWindow.instance.getcontentfromdatabase();   //heavy code Do Somethings
                ChatWindow.adap.notifyDataSetChanged();      //


            }
        }*/

        /*ContentValues values2 = new ContentValues();
        values2.put(DatabaseAccesser.chatList_last_msgseen, status);
        int rowln2=writableDB.update(DatabaseAccesser.chatList_table,values2,
        DatabaseAccesser.chatList_lastmsgRowId+" <=? and "+DatabaseAccesser.chatList_userid+" =?",
        new String[]{lastRow,userID});*/


        Intent new_intent = new Intent();
        new_intent.setAction("CHAT_UPDATED");
        sendBroadcast(new_intent);

    }


    long getLeastTimeAlluserSeenChatInGroup(String gpID,int status)
    {

        Cursor cursor = readableDB.query(DatabaseAccesser.groupsmembers_table,
                new String[]{
                        DatabaseAccesser.groupsmembers_last_rece_time,
                        DatabaseAccesser.groupsmembers_last_seen_time},
                DatabaseAccesser.groupsmembers_group_id + " =?",
                new String[]{gpID},
                null, null, null,null);

        long lastRece,lastSeen,lastSeenTemp,lastReceTemp;
        lastRece=lastSeen=System.currentTimeMillis();
        while (cursor.moveToNext()) {


            lastReceTemp = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.groupsmembers_last_rece_time));
            lastSeenTemp = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.groupsmembers_last_seen_time));

            lastRece= lastRece>lastReceTemp?lastReceTemp:lastRece;
            lastSeen= lastSeen>lastSeenTemp?lastSeenTemp:lastSeen;
        }

        if(status==2)
            return lastRece;
        else if(status==3)
            return lastSeen;
        else
            return 0;


    }

    void update_chatSeenOnDatabase(Context context,String userID,String lastRow,int status,String timestamp)
    {
        if(status==1 && !PrefManager.getBooleanPreference(context,PREF_CHAT_READ_RECEIPT,true))
        {
            /*if (listener!=null)
                listener.onJobCompletion("");*/
            return;
        }
        else if(status==0 && !PrefManager.getBooleanPreference(context,PrefManager.PREF_CHAT_RECEIVED_RECEIPT,true))
        {
            /*if (listener!=null)
                listener.onJobCompletion("");
                */
            return;
        }

        status=status==0?2:3;
        ContentValues values = new ContentValues();
        values.put(DatabaseAccesser.chat_ifseen, status);
        Log.e("GGG","last time "+timestamp);
        if(status==2)
        {
            Log.e("GGG","got last rece time "+timestamp);
            values.put(DatabaseAccesser.chat_rece_timestamp, timestamp);
        }
        else if(status==3)
        {
            values.put(DatabaseAccesser.chat_seen_timestamp, timestamp);
        }
        int rowln=writableDB.update(DatabaseAccesser.chat_table,values,"rowid"+" =? AND "+
                DatabaseAccesser.chat_userid+" =? AND "+
                DatabaseAccesser.chat_ifseen+" <?",new String[]{lastRow,userID,status+""});

        Log.e("MSGG"," updateddd " +rowln+" / "+status+" / "+lastRow);

        //ChatSender.updateChatWindow(getApplicationContext(),0,Long.parseLong(lastRow),userID,status);
        ChatSender.updateChatWindowHEAVY(userID);
       /* if(ChatWindow.uId!=null)
        {
            if(ChatWindow.uId.equals(userID))
            {

                ChatWindow.getcontentfromdatabase();   //heavy code Do Somethings
                ChatWindow.adap.notifyDataSetChanged();      //


            }
        }*/

        ContentValues values2 = new ContentValues();
        values2.put(DatabaseAccesser.chatList_last_msgseen, status);
        int rowln2=writableDB.update(DatabaseAccesser.chatList_table,values2,DatabaseAccesser.chatList_lastmsgRowId+" <=? and "+DatabaseAccesser.chatList_userid+" =?",new String[]{lastRow,userID});
        Intent new_intent = new Intent();
        new_intent.setAction("CHAT_UPDATED");
        sendBroadcast(new_intent);

    }

    void listenToGroupChats()
    {
        removeAllGroupListeners();
        //SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();

        Cursor cursor;/* = readableDB.query(DatabaseAccesser.user_table,
                new String[]{DatabaseAccesser.user_user_id},
                DatabaseAccesser.user_user_type + " >=?",
                new String[]{"15"},
                null, null, null,null);*/

        cursor= readableDB.rawQuery(
                "SELECT "+DatabaseAccesser.user_table +"."+DatabaseAccesser.user_user_id+" FROM "+DatabaseAccesser.user_table +
                        " INNER JOIN "+DatabaseAccesser.groupsmembers_table+" ON "+
                        DatabaseAccesser.user_table+"."+DatabaseAccesser.user_user_id+" = "+ DatabaseAccesser.groupsmembers_table+"."+DatabaseAccesser.groupsmembers_group_id+
                        " WHERE " +
                        DatabaseAccesser.user_table+"."+DatabaseAccesser.user_user_type + " >=? AND "+
                        DatabaseAccesser.groupsmembers_table+"."+DatabaseAccesser.groupsmembers_user_id+"=? AND "+
                        DatabaseAccesser.user_table +"."+DatabaseAccesser.user_remote_block+"!=?",
                new String[]{"15",FirebaseAuth.getInstance().getUid(),"1"});



        while (cursor.moveToNext()) {


            final String groupID = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_table+"."+DatabaseAccesser.user_user_id));


            final  long lastReceived= retrieveLastReceivedTime(groupID);
            Log.e("GGG","groupcount "+cursor.getCount()+" group "+groupID+" lastre "+lastReceived);
            listentoSingleGroupChats(groupID,lastReceived);



        }}


    private HashMap<DatabaseReference, ChildEventListener> mListenerMap=new HashMap<DatabaseReference, ChildEventListener>();
    static ArrayList<DatabaseReference> refs=new ArrayList<>();
    static ArrayList<ChildEventListener> listeners=new ArrayList<>();
    public void listentoSingleGroupChats(final String groupIDFinal,long lastReceived)
    {
        ChildEventListener groupChatListener;
        groupChatListener=new ChildEventListener() {
            @Override
            public void onChildAdded(final DataSnapshot dataSnapshot, String s) {

                final GroupChatData post = dataSnapshot.getValue(GroupChatData.class);

                String userGroupID=post.userid;
                post.userid=groupIDFinal;


                updateLastReceivedTime(post.userid,post.timestamp);

                if(userGroupID.equals(userdetail.getUid()))
                    return;

                saveIncommingChat(post, userGroupID, new JobCompletionListner() {
                    @Override
                    public void onJobCompletion(String txt1) {
                        if(post.type==20)
                            dataSnapshot.getRef().removeValue();
                        DatabaseReference databaseRef = groupsRef.child(post.userid).child("chat_status");
                        databaseRef.child(userdetail.getUid()).child("last_rece_time").setValue(post.timestamp);

                        updateGroupWithLastReceiveMessageTime(post.timestamp,post.userid);
                    }
                });


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        //Log.e("GGG","groupmultiple listentoSingleGroupChats");
        refs.add(groupsRef.child(groupIDFinal).getRef().child("chats"));
        listeners.add(groupChatListener);
       // mListenerMap.put(groupsRef.child(groupIDFinal)/*.child("chats").orderByChild("timestamp").startAt(lastReceived)*/.getRef(),groupChatListener);
       // groupsRef.child(groupIDFinal).child("chats").orderByChild("timestamp").startAt(lastReceived).removeEventListener(groupChatListener);

        Log.e("GGG","groupmultiple listing ADDING1 PRO "+refs.size());
       //mListenerMap.get(groupsRef.child(groupIDFinal).getRef());

        groupsRef.child(groupIDFinal).child("chats").orderByChild("timestamp").startAt(lastReceived).addChildEventListener(groupChatListener);
        update_group_chat_status(groupIDFinal);

        // groupsRef.child(groupID).child("chats").keepSynced(true);


    }
    public void removeAllGroupListeners()
    {
        /*if(mListenerMap==null)
            return;
        Log.e("GGG","started listing REMOVING2 PRO "+mListenerMap.entrySet().size());
        for (Map.Entry<DatabaseReference, ChildEventListener> entry : mListenerMap.entrySet()) {
            Log.e("GGG","started listing REMOVING LISTENER");
            DatabaseReference ref = entry.getKey();
            ChildEventListener listener = entry.getValue();
            ref.removeEventListener(listener);
        }*/
        Log.e("GGG","groupmultiple listing REMOVING2 PRO "+refs.size());

        for (int i=refs.size()-1;i>=0;i--) {
            Log.e("GGG", "groupmultiple listing REMOVING LISTENER");
            //DatabaseReference ref = entry.getKey();
            //ChildEventListener listener = entry.getValue();
            refs.get(i).removeEventListener(listeners.get(i));
            refs.remove(i);
            listeners.remove(i);
        }
        }


    public static void removeSingleUserFromGroup(final Context context,final String userId,final String grpId,boolean serverAlso)
    {
        if(serverAlso)
        {
            DatabaseReference groupChatRef = FirebaseDatabase.getInstance().getReference().child("chat_group").child(grpId).child("group_members");
            groupChatRef.child(userId).removeValue();

            //GroupChatData removeChat=new GroupChatData();
            //removeChat.type=25;
            //removeChat.msg=userId;
            //removeChat.final_url=null;

            UserWholeData dat=ChatWindow.returnSingleUserDetails(context,grpId);
            dat.chat_scheduletime=0;
            dat.chat_expiretime=0;
            ChatSender.AIMessageSender(context, 22, null, userId, null, dat, new ChatSentListner() {

                @Override
                public void onChatAdded(GroupChatData data) {}

                @Override
                public void onChatUploaded(GroupChatData data) {
                    removeMemberFromGroupLocal(context,userId,grpId);
                }
            },false);
        }
        else
        {
            removeMemberFromGroupLocal(context,userId,grpId);
        }

    }


    public static void removeMemberFromGroupLocal(Context context,String userId,String grpId)
    {

        DatabaseManager.getDatabase(context).getWritableDatabase().delete(DatabaseAccesser.groupsmembers_table,DatabaseAccesser.groupsmembers_group_id+" =? AND "+
                DatabaseAccesser.groupsmembers_user_id+" =?",new String[]{grpId,userId});
        if (userId.equals(FirebaseAuth.getInstance().getUid()))
        {
            //Toast.makeText(context, "remove", Toast.LENGTH_SHORT).show();
            //TabbedActivity.instance.restartService();
            MainServiceThread.mainService.listenToGroupChats();
           /* Intent intent3 = new Intent(context, TabbedActivity.class);
            intent3.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            context. startActivity(intent3);*/

        }
        Intent intent = new Intent("GROUP_MEMBER_UPDATED");
        context.sendBroadcast(intent);
    }

    public static void revokeAdminStatusServer(final Context context,final String groupID,final String userId,final int newStatus)
    {
        Log.e("GGG","adminstat senting "+userId + " / "+newStatus);
        DatabaseReference groupChatRef = FirebaseDatabase.getInstance().getReference().child("chat_group").child(groupID).child("group_members");
        groupChatRef.child(userId).child("member_type").setValue(newStatus);
        UserWholeData dat=ChatWindow.returnSingleUserDetails(context,groupID);
        dat.chat_scheduletime=0;
        dat.chat_expiretime=0;
        Log.e("GGG","adminstat starting "+userId + " / "+newStatus);
        ChatSender.AIMessageSender(context, newStatus==1?23:24, null, userId, null, dat, new ChatSentListner() {
            @Override
            public void onChatAdded(GroupChatData data) {
                Log.e("GGG","adminstat chatupload "+userId + " / "+newStatus);
            }

            @Override
            public void onChatUploaded(GroupChatData data) {
                Log.e("GGG","adminstat dispatched "+userId + " / "+newStatus);
            }
        },true);

        revokeAdminStatusOfMemberLocal(context,groupID,userId,newStatus);
    }

    public static void revokeAdminStatusOfMemberLocal(Context context,String groupID,String userId,int newStatus)
    {
        SQLiteDatabase db=DatabaseManager.getDatabase(context).getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put(DatabaseAccesser.groupsmembers_member_type,newStatus);
        Log.e("GGG","adminstat DELET COMMAND "+groupID+" \n "+userId);
       int rowln= db.update(DatabaseAccesser.groupsmembers_table,values,DatabaseAccesser.groupsmembers_group_id+"=? AND "+
        DatabaseAccesser.groupsmembers_user_id+"=? ",new String[]{groupID,userId});

        Log.e("GGG","adminstat rowcount is  "+rowln);
        Intent intent = new Intent("GROUP_MEMBER_UPDATED");
        context.sendBroadcast(intent);
    }

    public static void addNewUserToGroup(final Context context,final String userId,final String grpId)
    {
        DatabaseReference groupChatRef = FirebaseDatabase.getInstance().getReference().child("chat_group").child(grpId).child("group_members");

        final SQLiteDatabase db = DatabaseManager.getDatabase(context).getWritableDatabase();

        GroupMemberData memberData= new GroupMemberData();
        memberData.userid=userId;

        final ContentValues values = new ContentValues();
        values.put(DatabaseAccesser.groupsmembers_group_id,grpId);
        values.put(DatabaseAccesser.groupsmembers_user_id, userId);

        if(FirebaseAuth.getInstance().getUid().equals(userId)) {
            values.put(DatabaseAccesser.groupsmembers_member_type, 1);
            memberData.member_type=1;
            if( MainServiceThread.mainService!=null)
                MainServiceThread.mainService.listentoSingleGroupChats(grpId, System.currentTimeMillis());
        }
        else {
            values.put(DatabaseAccesser.groupsmembers_member_type, 0);
            memberData.member_type=0;
            sentCustomNotificationToUser(context,userId, 15, grpId, new JobCompletionListner() {
                @Override
                public void onJobCompletion(String txt1) {
                    //Log.e("GGG","groupchatCREATE sender user: "+userId+" group is : "+grpId+" sender is "+FirebaseAuth.getInstance().getUid());
                    ChatSender.AIMessageSender(context,21,null,userId,null,ChatWindow.returnSingleUserDetails(context,grpId),null,false);
                }
            });

        }
        groupChatRef.child(userId).setValue(memberData).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                long  newRowId = db.insert(DatabaseAccesser.groupsmembers_table, null, values);

            }
        });


    }

    /*public static void addSingleUserFromGroup(final Context context,final String userId,final String grpId)
    {


        sentCustomNotificationToUser(userId, 15, grpId, new JobCompletionListner() {
            @Override
            public void onJobCompletion(String txt1) {
                Log.e("GGG","groupadd senting msg");
                UserWholeData dat=ChatWindow.returnSingleUserDetails(context,grpId);
                ChatSender.AIMessageSender(context, 21, null, userId, null, dat, new ChatSentListner() {
                    @Override
                    public void onChatAdded(GroupChatData data) {

                    }

                    @Override
                    public void onChatUploaded(GroupChatData data) {
                        GroupMemberData memberData= new GroupMemberData();
                        ContentValues values = new ContentValues();
                        values.put(DatabaseAccesser.groupsmembers_group_id,grpId);
                        values.put(DatabaseAccesser.groupsmembers_user_id, userId);
                        values.put(DatabaseAccesser.groupsmembers_member_type, 0);
                        long  newRowId =  DatabaseManager.getDatabase(context).getWritableDatabase()
                                .insert(DatabaseAccesser.groupsmembers_table, null, values);
                    }
                });




            }
        });


    }*/

    void updateLastReceivedTime(String grp,long timeData)
    {


        ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.user_timestamp, timeData);

        //long newRowId = writableDB.insert(DatabaseAccesser.user_table, null, values);
        int rowln=writableDB.update(DatabaseAccesser.user_table,values, DatabaseAccesser.user_user_id+" =?",new String[]{grp});
        Log.e("MSGG","last update  :"+timeData+" row : "+rowln);

    }

    long retrieveLastReceivedTime(String grp)
    {

        //SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();

        Cursor cursor = readableDB.query(DatabaseAccesser.user_table,
                new String[]{
                        DatabaseAccesser.user_timestamp},
                DatabaseAccesser.user_user_id + " =?",
                new String[]{grp},
                null, null, null,null);

        cursor.moveToNext();

        //cursor.getInt(
        //cursor.getColumnIndexOrThrow(DatabaseAccesser.user_timestamp));
        long time= cursor.getLong(
                cursor.getColumnIndexOrThrow(DatabaseAccesser.user_timestamp));
       // Log.e("MSGG","last rec :"+time);
        time++;
        return time;
    }


    /*void saveGroupChatToDatabase(final GroupChatData post, String userGroupID)
    {

        ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.chat_userid, post.userid);
        values.put(DatabaseAccesser.chat_sender, post.sender);
        values.put(DatabaseAccesser.chat_type, post.type);
        values.put(DatabaseAccesser.chat_timestamp, System.currentTimeMillis()); //chattimestamp changes made here deafult was system time
        values.put(DatabaseAccesser.chat_text, post.msg);
        //values.put(DatabaseAccesser.chat_temp_url, post.temp_url);
        values.put(DatabaseAccesser.chat_col_media_id, post.final_url);
        values.put(DatabaseAccesser.chat_groupid, userGroupID);

        final long newRowId = writableDB.insert(DatabaseAccesser.chat_table, null, values);

        ContentValues values2 = new ContentValues();

        values2.put(DatabaseAccesser.chatList_lastmsg, post.msg);
        values2.put(DatabaseAccesser.chatList_timestamp, System.currentTimeMillis());
        values2.put(DatabaseAccesser.chatList_lastmsg_type, post.type);
        values2.put(DatabaseAccesser.chatList_lastmsgbyme, 0);
        values2.put(DatabaseAccesser.chatList_msgqueue, 1);
        values2.put(DatabaseAccesser.chatList_lastmsgRowId, newRowId);
        //values2.put(DatabaseAccesser.chatList_last_msgseen, post.msgseen);


        //int rowln=writableDB.update(DatabaseAccesser.chatList_table,values2, DatabaseAccesser.chatList_userid+" =?",new String[]{groupID});
        writableDB.rawQuery("UPDATE "+DatabaseAccesser.chatList_table+
                " SET "+DatabaseAccesser.chatList_msgqueue+" = "+DatabaseAccesser.chatList_msgqueue+"+1"+ " WHERE "+ DatabaseAccesser.chatList_userid+" =?",new String[]{post.userid});
        if(ChatWindow.uId!=null)
        {
            if(ChatWindow.uId.equals(post.userid))
            {

                ChatWindow.finaldata.add(post);
                ChatWindow.adap.notifyItemInserted(ChatWindow.finaldata.size()-1);
                ChatWindow.chatView.scrollToPosition(ChatWindow.finaldata.size() - 1);

                DatabaseReference databaseRef = groupsRef.child(post.userid).child("chat_status");


                databaseRef.child(userdetail.getUid()).child("last_seen_time").setValue(post.timestamp);
            }
        }

        Intent new_intent = new Intent();
        new_intent.setAction("CHAT_UPDATED");
        sendBroadcast(new_intent);


    }*/

    static void uploadFileAndReturnDownloadUrl(StorageReference hRef, Uri url, final JobCompletionListner listner)
    {
      /* hRef.putFile(url).add
               .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                   @Override
                   public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                   }

               }).;*/
    }

    public static void removeDownload(DownloadData downloadData)
    {
        for(int i=finaldownloadList.size()-1;i>=0;i--) {
            // Log.e("GGG","\n downloadme checking equality "+"\n"+downloadList.get(i).downloadUrl+" \n "+downloadData.downloadUrl+"\n"+(downloadList.get(i).downloadUrl.equals(downloadData.downloadUrl))+"\n");
            if ((finaldownloadList.get(i).downloadUrl.equals(downloadData.downloadUrl))) {
                finaldownloadList.remove(i);
            }
        }
    }
   public static boolean addDownload(DownloadData downloadData)
   {
       for(int i=finaldownloadList.size()-1;i>=0;i--)
       {
          // Log.e("GGG","\n downloadme checking equality "+"\n"+downloadList.get(i).downloadUrl+" \n "+downloadData.downloadUrl+"\n"+(downloadList.get(i).downloadUrl.equals(downloadData.downloadUrl))+"\n");
           if((finaldownloadList.get(i).downloadUrl.equals(downloadData.downloadUrl)))
           {

                  // Log.e("GGG","downloadme adding new listener "+finaldownloadList.get(i).downloadUrl+"\n"+downloadData.downloadUrl);
                   //downloadList.get(i).listener.add(downloadData.listener);
                   //Log.e("GGG","downloadme adding NOT unique complete "+downloadData.downloadUrl);
                   finaldownloadList.add(downloadData);
                   return true;


           }

       }
       Log.e("GGG","downloadme NEW URL "+downloadData.downloadUrl);
       /*DownloadListData ddd=new DownloadListData();
       ddd.listener.add(downloadData.listener);
       ddd.downloadUrl=downloadData.downloadUrl;*/

       finaldownloadList.add(downloadData);
       return false;
   }

   static DownloadListData getDownloadListItemArrayFromUri(String uri)
   {
       for(int i=0;i<downloadList.size();i++)
       {
           if(downloadList.get(i).downloadUrl.equals(uri));
           return downloadList.get(i);
       }
       return null;
   }
  final static  ArrayList<DownloadListData> downloadList=new ArrayList<>();
    final static  ArrayList<DownloadData> finaldownloadList=new ArrayList<>();
//objectType = 0->chats 1->feeds 2->profile images 3-> chatreplays
    public static void downloadFileAndReturnUriString(Context context,final String url,boolean cache,int objectType,Integer mediaType,final FileDownloadListner listner) {
        if (url.equals("null") || !url.contains("http")) {
            listner.onSuccessListener("null");
            return;
        }
        Log.e("GGG","downloadme INCOMMING "+url);
        StorageReference httpsReference = FirebaseStorage.getInstance().getReferenceFromUrl(url);
       /* File localFile = null;
        try {
            localFile = File.createTempFile("images", "png");
        } catch (IOException e) {
            e.printStackTrace();
        }*/


        final DownloadData dat=new DownloadData();
        dat.listener=listner;
        dat.downloadUrl=url;
        //finaldownloadList.add(dat);

        /*final DownloadListData dat2=new DownloadListData();
        dat2.listener=new ArrayList<>();
        dat2.listener.add(listner);
        dat2.downloadUrl=url;
        downloadList.add(dat2);*/
       // Log.e("GGG","downloadme new download "+url);

        if(addDownload(dat))
            return;

        //Log.e("GGG","downloadme unique "+url);
        //downloadList.add(dat);

        final String name = StringUtils.randomStringGenerator() + "." + httpsReference.getName().split("\\.")[1];

       /* final File file = new File(Environment.getExternalStorageDirectory().getPath(), "Around Files/Media");
        if (!file.exists()) {
            file.mkdirs();
        }*/
        File file=null;
        Log.e("GGG","downloaddir 0 ");
       if(cache)
       {
           file=new File(AroundStaticInfo.getDefaultCacheDirectory(context));
       }
       else if(objectType==0)
       {
           Log.e("GGG","downloaddir 0 "+AroundStaticInfo.getDefaultChatDirectory(context));
           if(mediaType==6)
                file=new File(AroundStaticInfo.getDefaultDocumentsDirectory(context));
           else
                file=new File(AroundStaticInfo.getDefaultChatDirectory(context));
       }
       else if(objectType==1)
       {
           file=new File(AroundStaticInfo.getDefaultFeedDirectory(context));
       }
       else if(objectType==2)
       {
           file=new File(AroundStaticInfo.getDefaultProfileDirectory(context));
       }
       else if(objectType==3)
       {
           file=new File(AroundStaticInfo.getDefaultOthersDirectory(context));
       }

        File localFile=null;
        try {
             localFile = new File(file.getAbsolutePath(), name);
        } catch (Exception e) {
            e.printStackTrace();
        }
        final File temp=localFile;
        //Log.e("GGG", "abc getting file");
        //Log.e("GGG","new group got data2");
        httpsReference.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {

                Log.e("GGG", "abc download success");
                /*FileOutputStream fos = new FileOutputStream(new File(file.getAbsolutePath(), name));
                fos.write(bytes);
                fos.close();*/
                //temp.
                //temp.
                DownloadListData currentItem=getDownloadListItemArrayFromUri(url);
                //Log.e("GGG","downloadme TO COMPLETE");
                //addDownloadDataIfNotOrRemove(dat,true);
                //Log.e("GGG","downloadme DOWN COMPLETE 1 "+url+"\n"+Uri.fromFile(temp).toString());
                //listner.onSuccessListener(Uri.fromFile(temp).toString());

                Log.e("GGG","downloadme OUTGOING ORDINARY "+url+"\n"+Uri.fromFile(temp).toString());
               /* for(FileDownloadListner list:currentItem.listener) {
                    if (list != null)
                        Log.e("GGG","downloadme OUTGOING "+url+"\n"+Uri.fromFile(temp).toString());
                        list.onSuccessListener(Uri.fromFile(temp).toString());
                }*/

                for(int i=0;i<finaldownloadList.size();i++)
                {
                    if(finaldownloadList.get(i).downloadUrl.equals(url))
                    {
                        Log.e("GGG","downloadme OUTGOING "+url+"\n"+Uri.fromFile(temp).toString());
                        finaldownloadList.get(i).listener.onSuccessListener(Uri.fromFile(temp).toString());
                    }
                }
                Log.e("GGG","downloadme size is  "+finaldownloadList.size());
                removeDownload(dat);
                Log.e("GGG","downloadme size is  "+finaldownloadList.size());


            }
        }).addOnProgressListener(new OnProgressListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onProgress(FileDownloadTask.TaskSnapshot taskSnapshot) {
                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                //float percent = ((float) taskSnapshot.getBytesTransferred() / (float) taskSnapshot.getTotalByteCount()) * 100;
                /*DownloadListData currentItem=getDownloadListItemArrayFromUri(url);
                for(FileDownloadListner list:currentItem.listener)
                    if(list!=null) list.onProgressListener((int) progress);*/
            }
        }).addOnFailureListener(new OnFailureListener() {

            @Override
            public void onFailure(@NonNull Exception e) {
                /*DownloadListData currentItem=getDownloadListItemArrayFromUri(url);
                //addDownloadDataIfNotOrRemove(dat,true);
                for(FileDownloadListner list:currentItem.listener)
                    if(list!=null)  list.onFailureListener(0);*/


            }
        }).addOnPausedListener(new OnPausedListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onPaused(FileDownloadTask.TaskSnapshot taskSnapshot) {

                DownloadListData currentItem=getDownloadListItemArrayFromUri(url);
               // addDownloadDataIfNotOrRemove(dat,true);
              /*  for(FileDownloadListner list:currentItem.listener)
                    if(list!=null)list.onPausedListener(0);*/
                //listner.onPausedListener(0);

            }
        });
    }

    void downloadReplay(String uri, final ChatReplayReceiver listener)
    {
        if(uri==null||uri.equals("null"))
        {
            listener.onChatReplayReceived(null);
        }
        else
        {
            downloadFileAndReturnUriString(getApplicationContext(),uri,false,3,null, new FileDownloadListner() {
                @Override
                public void onProgressListener(int percent) {

                }

                @Override
                public void onSuccessListener(String localFilename) {
                    ChatReplayData dat=(ChatReplayData)ByteUtils.deserializeFile(Uri.parse(localFilename));
                    Log.e("GGG","downloadme chat replay received file is "+localFilename+" / "+dat);
                    if (listener!=null)
                        listener.onChatReplayReceived(dat);
                }

                @Override
                public void onFailureListener(int percent) {

                }

                @Override
                public void onPausedListener(int percent) {

                }
            });
        }
    }

    synchronized void saveIncommingChat(final GroupChatData post,final String groupUserID,final JobCompletionListner listener)
    {
        post.sender = 1;
        post.msgseen =2;

        downloadReplay(post.replay_url, new ChatReplayReceiver() {
            @Override
            public void onChatReplayReceived(ChatReplayData data) {
                Log.e("GGG","chatreplay recive is is>>2 "+data);
                if(data==null)
                    post.replay_id=1;
                else{
                    post.chatReplay=data;
                    post.replay_id=DatabaseManager.insertChatReplaysToDatabase(data);
                    Log.e("GGG","chatreplay recive is is>> "+post.chatReplay.contentUri+" uri us "+post.replay_id);
                }


                // String urlToDownload = "null";
                boolean autodownload=false;
                switch(post.type)
                {
                    case 0:
                        post.media_id=1;
                        saveChatToDatabase(post,groupUserID);

                        break;

                    case 1:
                        Log.e("GGG","mediapost received image");
                        if (PrefManager.checkSearchMultiSelectPref(getApplicationContext(),"data_usage_chat", TabbedActivity.PREF_IMAGE,R.array.data_usage_chat_default_values)) {
                            autodownload = true;}
                        downloadChatMediaAndSave(post,autodownload,groupUserID);
                        break;
                    case 2:
                        if (PrefManager.checkSearchMultiSelectPref(getApplicationContext(),"data_usage_chat", TabbedActivity.PREF_AUDIO,R.array.data_usage_chat_default_values))//vvvv
                        {autodownload = true;}
                        //Log.e("GGG", "abc autodownload "+autodownload+" / "+(checkSearchMultiSelectPref(getApplicationContext(),"data_usage_chat", TabbedActivity.PREF_AUDIO)));
                        downloadChatMediaAndSave(post,autodownload,groupUserID);
                        break;

                    case 3:
                        if (PrefManager.checkSearchMultiSelectPref(getApplicationContext(),"data_usage_chat", TabbedActivity.PREF_VIDEO,R.array.data_usage_chat_default_values))//vvvv
                        {autodownload = true;}
                        Log.e("MGG", "abc Video Found ");
                        downloadChatMediaAndSave(post,autodownload,groupUserID);
                        break;
                    case 5:
                        if (PrefManager.checkSearchMultiSelectPref(getApplicationContext(),"data_usage_chat", TabbedActivity.PREF_GIF,R.array.data_usage_chat_default_values))//vvvv
                        {autodownload = true;}
                        downloadChatMediaAndSave(post,autodownload,groupUserID);
                        break;
                    case 6:
                        if (PrefManager.checkSearchMultiSelectPref(getApplicationContext(),"data_usage_chat", TabbedActivity.PREF_FIlE,R.array.data_usage_chat_default_values))//vvvv
                        {autodownload = true;}
                        downloadChatMediaAndSave(post,autodownload,groupUserID);
                        break;
                    case 7:
                        post.media_id=1;
                        saveChatToDatabase(post,groupUserID);
                        break;
                    case 4:
                        post.media_id=1;

                       // LocationData loc;
                        //loc=(LocationData)ByteUtils.deserialize(ByteUtils.stringToByte(post.msg));

                        //post.msg=loc.location_name+TabbedActivity.TEXT_SEPARATOR+
                                //loc.latitude+TabbedActivity.TEXT_SEPARATOR+loc.longitude;


                        saveChatToDatabase(post,groupUserID);
                        break;

                    case 20:
                        // Log.e("MSGG","poke recievedx");
                        if(post.msg.equals(userdetail.getUid())) {
                            Toast.makeText(MainServiceThread.this, "Poke from "+post.userid, Toast.LENGTH_SHORT).show();
                            if(ChatWindow.uId!=null)
                            {
                                if(ChatWindow.uId.equals(post.userid))
                                {

                                    Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                    // Vibrate for 500 milliseconds
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                        v.vibrate(VibrationEffect.createOneShot(500,VibrationEffect.DEFAULT_AMPLITUDE));
                                    }else{
                                        //deprecated in API 26
                                        v.vibrate(500);
                                    }
                                }
                            }

                        }
                        break;


                    case 21:
                        post.media_id=1;
                        Toast.makeText(MainServiceThread.this, "added "+post.msg, Toast.LENGTH_SHORT).show();
                        post.sender=1;
                        GroupMemberData vvv=new GroupMemberData();
                        vvv.userid=post.msg;
                        vvv.member_type=0;
                        Log.e("GGG","groupchatCREATE rece user: "+post.msg+" group is : "+post.userid+" sender is "+groupUserID);
                        updateGroupMemberInDatabase(vvv,post.userid);
                        //addSingleUserFromGroup(getApplicationContext(),post.msg,post.userid);
                        //saveGroupChatToDatabase(post, groupUserID);

                        saveChatToDatabase(post,groupUserID);
                        break;
                    case 22:

                        post.media_id=1;
                        Log.e("GGG","groupchatCREATE rece user: "+post.msg+" group is : "+post.userid+" sender is "+groupUserID);
                        post.sender=1;
                        removeMemberFromGroupLocal(getApplicationContext(),post.msg,post.userid);
                        //removeSingleUserFromGroup(getApplicationContext(),post.msg,post.userid,false);
                        //saveGroupChatToDatabase(post, groupUserID);  this might be the correct...uncomment the fn
                        saveChatToDatabase(post,groupUserID);

                        break;
                    case 23:
                        Log.e("GGG","adminstat new admin "+post.msg);
                        post.media_id=1;
                       post.sender=1;
                        //removeMemberFromGroupLocal(getApplicationContext(),post.msg,post.userid);
                        //removeSingleUserFromGroup(getApplicationContext(),post.msg,post.userid,false);
                        //saveGroupChatToDatabase(post, groupUserID);  this might be the correct...uncomment the fn
                        //saveChatToDatabase(post,groupUserID);
                        revokeAdminStatusOfMemberLocal(getApplicationContext(),post.userid,post.msg,1);
                        break;
                    case 24:

                        post.media_id=1;
                        Log.e("GGG","adminstat removed "+post.msg);
                        post.sender=1;
                        //removeMemberFromGroupLocal(getApplicationContext(),post.msg,post.userid);
                        revokeAdminStatusOfMemberLocal(getApplicationContext(),post.userid,post.msg,0);
                        //saveChatToDatabase(post,groupUserID);

                        break;
                }

            }
        });

        listener.onJobCompletion("");
    }
    void handlePendingProcess()
    {
        //new DatabaseAccesser(getApplicationContext()).resetDatabase();
        Cursor cursor=DatabaseManager.getDatabase(getApplicationContext()).getReadableDatabase().query(DatabaseAccesser.pending_table,new String[]{"rowid","*"},null,null,null,null,null);

        Log.e("GGG","pendingsize "+cursor.getCount());
        while(cursor.moveToNext())
        {
            int type=cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.pending_type));
            final long row=cursor.getLong(
                    cursor.getColumnIndexOrThrow("rowid"));
           // long itemRow=cursor.getLong(
                   // cursor.getColumnIndexOrThrow(DatabaseAccesser.pending_item_row));
            long timeData=cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.pending_timestamp));

            long longExtra=cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.pending_long_extra));
            String stringExtra=cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.pending_string_extra));

            if(type==0)
            {
                UserManager.updateLastReceiveTime(getApplicationContext(),1, longExtra, FirebaseDatabase.getInstance().getReference().child("chat_status").child(stringExtra).child(FirebaseAuth.getInstance().getUid()), new JobCompletionWithFailureListner() {
                    @Override
                    public void onJobFailed(String txt1) {

                    }

                    @Override
                    public void onJobCompletion(String txt1) {
                        DatabaseManager.removePendingProcess(getApplicationContext(),row);

                    }
                });
            }
        }
    }
    void update_content()
    {
        if(childEventListener==null) {

            childEventListener = new ChildEventListener() {
                @Override
                public void onChildAdded(final DataSnapshot dataSnapshot, String s) {

                    final GroupChatData post = dataSnapshot.getValue(GroupChatData.class);

                    saveIncommingChat(post,null,new JobCompletionListner(){
                        @Override
                        public void onJobCompletion(String txt1) {
                            UserManager.updateLastReceiveTime(getApplicationContext(),0,post.sl,FirebaseDatabase.getInstance().getReference().child("chat_status").child(post.userid).child(userdetail.getUid()), new JobCompletionWithFailureListner() {
                                @Override
                                public void onJobFailed(String txt1) {  }

                                @Override
                                public void onJobCompletion(String txt1) {
                                    //yyyy
                                    dataSnapshot.getRef().removeValue();

                                    updateUserWithLastReceiveMessageSl(post.sl,post.userid);
                                }
                            });
                        }
                    });
                    //DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference().child("chat_status").child(post.userid);
                    //databaseRef.child(userdetail.getUid()).child("last_rece_msg").setValue(post.sl);



                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            };


//                      Toast.makeText(getActivity(), "hurray", Toast.LENGTH_SHORT).show();
            // ...
            //newPostRef.goOffline();
            //newPostRef.removeEventListener(childEventListener);

            Toast.makeText(this, "creeeeeta", Toast.LENGTH_SHORT).show();
            newPostRef.addChildEventListener(childEventListener);

        }

    }
    private class FileDownloader
    {
        String local;
        String litelocal;
        GroupChatData post;boolean autodownload; String groupID;
        public FileDownloader(GroupChatData post,boolean autodownload, String groupID) {
            this.post=new GroupChatData(post);
            this. autodownload=autodownload;
            this.groupID=groupID;

        }
        void startDownload(){
            Log.e("GGG", "bulkdownload1 TEMP START 1  "+post.chatReplay);
            downloadFileAndReturnUriString(getApplicationContext(), post.temp_url,true,0,post.type, new FileDownloadListner() {
                @Override
                public void onProgressListener(int percent) {

                }

                @Override
                public void onSuccessListener(String localFilename) {
                    litelocal = localFilename;
                    Log.e("GGG","bulkdownload1 TEMP DONE 1 "+post.temp_url+"\n"+localFilename);

                    if (autodownload/*false*/) {
                        Log.e("GGG", "bulkdownload1 FINAL START 1  \n"+post.temp_url+"\nFINAL : "+post.final_url);
                        final GroupChatData tempooo=new GroupChatData(post);
                        downloadFileAndReturnUriString(getApplicationContext(),post.final_url,false,0,post.type, new FileDownloadListner() {
                            @Override
                            public void onProgressListener(int percent) {

                            }

                            @Override
                            public void onSuccessListener(String localFilename) {
                                Log.e("GGG","bulkdownload1 FINAL DONE 2 \n"+tempooo.final_url+"\n media "+tempooo.media_id+"\n local : "+localFilename);
                                local = localFilename;
                                GroupChatData kemppppp=new GroupChatData(tempooo);

                                //post.media_id = DatabaseManager.insertMediaToDatabase(post.final_url, local, post.temp_url, litelocal,post.mediaSize);
                                //if(post.media_id==0)
                                kemppppp.media_id = DatabaseManager.insertMediaToDatabase(getApplicationContext(),kemppppp.final_url, local, kemppppp.temp_url, litelocal,kemppppp.mediaSize);
                                //else
                                //DatabaseManager.updateMediaInDatabase(post.media_id,null,local,null,litelocal) ;


                                kemppppp.temp_url=litelocal;
                                kemppppp.final_url=local;
                                Log.e("GGG","bulkdownload1 FINAL DB SAVE 2 \n"+kemppppp.final_url+"\n media "+kemppppp.media_id+"\n local : "+kemppppp.temp_url);

                                saveChatToDatabase(kemppppp,groupID);

                            }

                            @Override
                            public void onFailureListener(int percent) {

                            }

                            @Override
                            public void onPausedListener(int percent) {

                            }
                        });
                    } else
                    {
                        ///if(post.media_id==0)
                        final GroupChatData tempoooeeee=new GroupChatData(post);
                        tempoooeeee.media_id = DatabaseManager.insertMediaToDatabase(getApplicationContext(),tempoooeeee.final_url, "null", tempoooeeee.temp_url, litelocal,tempoooeeee.mediaSize);
                        //else
                            //DatabaseManager.updateMediaInDatabase(post.media_id,null,"null",null,litelocal) ;
                        tempoooeeee.temp_url=litelocal;
                        // if(groupID==null)
                        Log.e("GGG","bulkdownload1 FINAL DB SAVE 2 \n"+tempoooeeee.final_url+"\n media "+tempoooeeee.media_id+"\n local : "+tempoooeeee.temp_url);

                        saveChatToDatabase(tempoooeeee,groupID);

                    }

                }

                @Override
                public void onFailureListener(int percent) {

                }

                @Override
                public void onPausedListener(int percent) {

                }
            });
        }
    }

    synchronized void downloadChatMediaAndSave(final GroupChatData post,final boolean autodownload,final @Nullable String groupID)
    {
       FileDownloader ve= new FileDownloader( new GroupChatData(post),autodownload,  groupID);
        ve.startDownload();
        if(true)
            return;


        //Log.e("GGG","ddownloadme CHAT INCOMMING 1 ");
        long temp = checkForMediaAvailablility(post.final_url);
        /*if (temp != 0) {
            post.media_id = temp;
            MediaData mDat= MediaManager.returnMediaFromRow(getApplicationContext(),temp);

            post.temp_url=mDat.localLiteUri;
            post.final_url=mDat.localUri;
            if(post.final_url.equals("null"))
            {
                post.final_url=mDat.globalUri;
            }
            else
            {
                saveChatToDatabase(post,groupID);
                return;
            }

           // else
              //  saveGroupChatToDatabase(post,groupID);

            //return post;
        }*/




       /* Thread xxx=new Thread(new Runnable() {
            @Override
            public void run() {


        synchronized(post)
        {*/
        Log.e("GGG", "bulkdownload TEMP START 1  \n"+post.temp_url+"\nFINAL : "+post.final_url);
            downloadFileAndReturnUriString(getApplicationContext(),post.temp_url,true,0,post.type, new FileDownloadListner() {
            @Override
            public void onProgressListener(int percent) {

            }

            @Override
            public void onSuccessListener(String localFilename) {
                final String litelocal = localFilename;
                Log.e("GGG", "abc first complete ");
                Log.e("GGG","bulkdownload TEMP DONE 1 \n"+post.temp_url+"\n"+localFilename);
                if (autodownload) {
                    Log.e("GGG", "bulkdownload FINAL START 1  \n"+post.temp_url+"\nFINAL : "+post.final_url);
                    downloadFileAndReturnUriString(getApplicationContext(),post.final_url,false,0,post.type, new FileDownloadListner() {
                        @Override
                        public void onProgressListener(int percent) {

                        }

                        @Override
                        public void onSuccessListener(String localFilename) {
                            Log.e("GGG","bulkdownload FINAL DONE 2 \n"+post.final_url+"\n media "+post.media_id+"\n local : "+localFilename);

                            //GroupChatData temp=new GroupChatData(post);
                            final String local = localFilename;
                            //post.media_id = DatabaseManager.insertMediaToDatabase(post.final_url, local, post.temp_url, litelocal,post.mediaSize);
                            //if(post.media_id==0)
                                post.media_id = DatabaseManager.insertMediaToDatabase(getApplicationContext(),post.final_url, local, post.temp_url, litelocal,post.mediaSize);
                            //else
                                //DatabaseManager.updateMediaInDatabase(post.media_id,null,local,null,litelocal) ;


                            post.temp_url=litelocal;
                            post.final_url=local;

                            Log.e("GGG","bulkdownload FINAL DB SAVE 2 \n"+post.final_url+"\n media "+post.media_id+"\n local : "+post.temp_url);

                            //if(groupID==null)
                            saveChatToDatabase(post,groupID);
                            /*else
                                saveGroupChatToDatabase(post,groupID);*/
                        }

                        @Override
                        public void onFailureListener(int percent) {

                        }

                        @Override
                        public void onPausedListener(int percent) {

                        }
                    });
                } else
                    {
                    if(post.media_id==0)
                        post.media_id = DatabaseManager.insertMediaToDatabase(getApplicationContext(),post.final_url, "null", post.temp_url, litelocal,post.mediaSize);
                    else
                        DatabaseManager.updateMediaInDatabase(post.media_id,null,"null",null,litelocal) ;
                    post.temp_url=litelocal;
                    // if(groupID==null)
                    saveChatToDatabase(post,groupID);
                    /*else
                        saveGroupChatToDatabase(post,groupID);*/
                }

            }

            @Override
            public void onFailureListener(int percent) {

            }

            @Override
            public void onPausedListener(int percent) {

            }
        });/*}

            }
        });
        xxx.start();*/
    }

    void updateUserWithLastReceiveMessageSl(long serialID,String user)
    {
        ContentValues values2 = new ContentValues();

        values2.put(DatabaseAccesser.user_lastRec_sl, serialID);

        int rowln=writableDB.update(DatabaseAccesser.user_table,values2, DatabaseAccesser.user_user_id+" =?",new String[]{user});
    }
    void updateGroupWithLastReceiveMessageTime(long time,String gpID)
    {

        ContentValues values2 = new ContentValues();

        values2.put(DatabaseAccesser.groupsmembers_last_rece_time, time);

        int rowln=writableDB.update(DatabaseAccesser.groupsmembers_table,values2, DatabaseAccesser.groupsmembers_group_id + " =? AND "+DatabaseAccesser.groupsmembers_user_id+ " =?",
                new String[]{gpID,userdetail.getUid()});

    }

    void saveNotificationToDatabase(final NotificationData post)
    {
        //SQLiteDatabase db = dbAccess.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.notification_user_id, post.userid);
        values.put(DatabaseAccesser.notification_user_name, post.username);
        values.put(DatabaseAccesser.notification_timestamp, System.currentTimeMillis());
        values.put(DatabaseAccesser.notification_status, 0);
        values.put(DatabaseAccesser.notification_text, post.text);
        values.put(DatabaseAccesser.notification_seen_status, 2);
        values.put(DatabaseAccesser.notification_image_url, post.image);
        values.put(DatabaseAccesser.notification_link, post.link);
        values.put(DatabaseAccesser.notification_type, post.type);

        if( NotificationTag.instance!=null)
        {
            NotificationTag.instance.incrementAINotification(1,post.type);
        }

        if(post.type>10) {

           int rowln= writableDB.update(DatabaseAccesser.notification_table,values,
                   DatabaseAccesser.notification_user_id+" =? AND "+
                           DatabaseAccesser.notification_type+" =? ",new String[]{post.userid,post.type+""});
           Log.e("GGG","requestNOT no "+rowln);
           if(rowln>0)
               return;
        }

        final long newRowId = writableDB.insert(DatabaseAccesser.notification_table, null, values);

        //Log.e("GGG","notificationtext Size of row :"+newRowId);

        Intent new_intent = new Intent("NOTIFICATION_UPDATED");
        new_intent.putExtra("type",post.type<10?0:1);
        sendBroadcast(new_intent);

    }

    public long saveChatToDatabase(final GroupChatData post,@Nullable String groupID)
    {
        /*ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.chat_userid, post.userid);
        values.put(DatabaseAccesser.chat_sender, post.sender);
        values.put(DatabaseAccesser.chat_type, post.numberType);
        values.put(DatabaseAccesser.chat_timestamp, System.currentTimeMillis());
        values.put(DatabaseAccesser.chat_text, post.chatMsg);
        //values.put(DatabaseAccesser.chat_temp_url, post.temp_url);
        values.put(DatabaseAccesser.chat_col_media_id, post.media_id);
        if (groupID!=null)
            values.put(DatabaseAccesser.chat_groupid, groupID);

        final long newRowId = writableDB.insert(DatabaseAccesser.chat_table, null, values);

        updateOrInsertIntoChatlist(post,newRowId,System.currentTimeMillis());*/

        final long newRowId = DatabaseManager.saveChatToDatabase(getApplicationContext(),post,groupID);
        post.chatRow=newRowId;

        if(ChatWindow.uId!=null)
        {
            if(ChatWindow.uId.equals(post.userid))
            {

                ChatWindow.finaldata.add(post);
                //ChatWindow.adap.notifyItemInserted(ChatWindow.finaldata.size()-1);
                ChatWindow.adap.notifyDataSetChanged();
                ChatWindow.chatView.scrollToPosition(ChatWindow.finaldata.size() - 1);

                //UserManager.updateLastReceiveTime(1,post.sl,FirebaseDatabase.getInstance().getReference().child("chat_status").child(post.userid).child(userdetail.getUid()), null);
                //DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference().child("chat_status").child(post.userid);
                //databaseRef.child(userdetail.getUid()).child("last_seen_msg").setValue(post.sl);

            }
        }
        else
        {
            //NotificationC

            LocalNotificationManager.sentNewChatNotitication(getApplicationContext(),post.userid,post.msg);
            /*LocalNotificationManager mNotificationManager =
                    (LocalNotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            Intent intenttest = new Intent(getApplicationContext(), TabbedActivity.class);
            PendingIntent pIntent = PendingIntent.getActivity(getApplicationContext(), (int) System.currentTimeMillis(), intenttest, 0);

            Notification.InboxStyle inboxStyle = new Notification.InboxStyle();
            inboxStyle.setBigContentTitle("Messages:");
            inboxStyle.setSummaryText(" New chats");
            inboxStyle.addLine(post.chatMsg);



            Notification n  =  new Notification.Builder(getApplicationContext())
                    .setContentTitle("SpotIt")
                    .setContentText(" New chats")
                    .setSmallIcon(R.drawable.nick)
                    .setContentIntent(pIntent)
                    .setStyle(inboxStyle)
                    .setPriority(Notification.PRIORITY_MAX)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true).build();

            mNotificationManager.notify(0,n);*/

            Intent new_intent = new Intent();
            new_intent.setAction("CHAT_UPDATED");
            sendBroadcast(new_intent);


        }

        return newRowId;
    }

    public static void updateOrInsertIntoChatlist(final Context context,final GroupChatData post,final long  newRowId,long timedata)
    {

          final  SQLiteDatabase db=DatabaseManager.getDatabase(context).getWritableDatabase();
        //values2.put(DatabaseAccesser.chatList_last_msgseen, post.msgseen);

        final ContentValues values2 = new ContentValues();

        values2.put(DatabaseAccesser.chatList_lastmsg, post.msg);
        values2.put(DatabaseAccesser.chatList_timestamp,timedata);
        values2.put(DatabaseAccesser.chatList_lastmsg_type, post.type);
        values2.put(DatabaseAccesser.chatList_lastmsgRowId, newRowId);
        if(post.sender==0)
        {
            values2.put(DatabaseAccesser.chatList_lastmsgbyme, 1);
            values2.put(DatabaseAccesser.chatList_last_msgseen, post.msgseen);
        }
        else
        {
            values2.put(DatabaseAccesser.chatList_lastmsgbyme, 0);
        }



        int rowln=db.update(DatabaseAccesser.chatList_table,values2, DatabaseAccesser.chatList_userid+" =?",new String[]{post.userid});

        if(post.sender==1) {
            db.execSQL("UPDATE " + DatabaseAccesser.chatList_table +
                    " SET " + DatabaseAccesser.chatList_msgqueue + " =" + DatabaseAccesser.chatList_msgqueue + "+1" + " WHERE " + DatabaseAccesser.chatList_userid + " ='" + post.userid + "'");
            // Log.e("MSGG","kolAA==update : "+rowln+" cur time "+timedata);
        }


        if(rowln<1)
        {


            final String msg=post.msg;
            final String UserId=post.userid;


            fetchUserDetailsbyIDOrPhone(post.userid, true, false,new ProcessCompletedListner() {
                @Override
                public void processCompleteAction() {

                }

                @Override
                public void newUserAdded(UserWholeData data) {
                    final ContactListData contact=data;
                    updateOrInsertUserIfNone(context,data, new JobCompletionListner() {
                        @Override
                        public void onJobCompletion(String txt1) {
                            //ContentValues values3 = new ContentValues();
                            values2.put(DatabaseAccesser.chatList_userid, UserId);
                            values2.put(DatabaseAccesser.chatList_name, contact.user_name);   //not needed - name already in user data
                            //values3.put(DatabaseAccesser.chatList_lastmsgbyme, 0);
                            //values3.put(DatabaseAccesser.chatList_lastmsg, chatMsg);
                            //values3.put(DatabaseAccesser.chatList_lastmsgRowId, newRowId);
                            //values3.put(DatabaseAccesser.chatList_lastmsg_type, post.numberType);
                            //values3.put(DatabaseAccesser.chatList_timestamp,System.currentTimeMillis());
                            if(post.sender==1)
                            values2.put(DatabaseAccesser.chatList_msgqueue, 1);
                            values2.put(DatabaseAccesser.chatList_url, contact.img_url);   //very much needed needed - url already in user data

                            long rowln=db.insert(DatabaseAccesser.chatList_table, null, values2);
                            Intent new_intent = new Intent();
                            new_intent.setAction("CHAT_UPDATED");
                            context.sendBroadcast(new_intent);
                        }
                    });
                }
            });

        }
    }

    /*public static boolean checkSearchMultiSelectPref(Context context,String key,int val)
    {

        SharedPreferences pref=PreferenceManager.getDefaultSharedPreferences(context);
        Set<String>  selections =pref.getStringSet(key,null);

        if(selections!=null) {
            if (selections.contains(val + "")) {
                return true;
            }
        }
        return false;
    }*/

    ArrayList<LocationData> currentLocations;
    GeoQueryEventListener feedFromLocationListner=new GeoQueryEventListener() {
        @Override
        public void onKeyEntered(String key, final GeoLocation location) {

            //Log.e("MSG",location.latitude+" "+location.longitude);
            Log.e("GGG","dataSnapshot is  user "+key);
            DatabaseReference newPostRef = fDatabase.child("feed_stack").child(key);

            postListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    handleInclommingFeeds(getApplicationContext(),dataSnapshot,location,null);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    // Getting Post failed, log a message
                    // Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
                    // ...
                }
            };

            newPostRef.addListenerForSingleValueEvent(postListener);


            //System.out.println(String.format("Key %s entered the search area at [%f,%f]", key, location.latitude, location.longitude));
        }

        @Override
        public void onKeyExited(String key) {
            //System.out.println(String.format("Key %s is no longer in the search area", key));
        }

        @Override
        public void onKeyMoved(String key, GeoLocation location) {
            //System.out.println(String.format("Key %s moved within the search area to [%f,%f]", key, location.latitude, location.longitude));
        }

        @Override
        public void onGeoQueryReady() {
            //System.out.println("All initial data has been loaded and events have been fired!");
        }

        @Override
        public void onGeoQueryError(DatabaseError error) {
            //System.err.println("There was an error with this query: " + error);
        }
    };
    void update_feed(Double lat,Double lng,boolean nearbyFeedsSearchOnly) {

        ArrayList<LocationData> dat=new ArrayList<>();
        if (!nearbyFeedsSearchOnly){
             dat.addAll(LocationUtils.getLocationAsList(getApplicationContext(), true));
            }

        if(lat!=0 &&lng!=0)
        dat.add(new LocationData(lat,lng,"current"));

       /* try {
            Log.e("GGG", "dataSnapshot total active locations " + dat.size() + " eqiality " + (checkLocationListSameAsCurrent(dat)) + " /// ");
        }
        catch (Exception e){}*/

        if(checkLocationListSameAsCurrent(dat)) {
            return;}

        if(geoQuery!=null)
            geoQuery.removeAllListeners();


        currentLocations=dat;
        //Log.e("GGG","dataSnapshot total active location "+dat.size()+" / "+lat+" / "+lng);
        for(int i=0;i<dat.size();i++)
        {
            updateFeedFromLocation(dat.get(i).latitude,dat.get(i).longitude);
        }

    }

    void update_feed2()
    {
        ArrayList<UserWholeData> dat=UserManager.getUsersList(getApplicationContext(),1);

        //CODE 47 : always returning false(need to create a fucntion to compare two Arraylist of objects)
        // here done some shit codes////make universal code
        if(checkUserListSameAsCurrent(dat)) {
            return;}
        currentUsers=dat;
        for(int i=0;i<dat.size();i++)
        {
            updateFeedFromUserID(dat.get(i).user_id);
        }
    }
    boolean checkUserListSameAsCurrent( ArrayList<UserWholeData> dat)
    {
        if(currentUsers==null)
            return false;
        else if(dat.size()!=currentUsers.size())
            return false;
        else
        {
            for(int i=0;i<currentUsers.size();i++)
            {
                if(!currentUsers.get(i).user_id.equals(dat.get(i).user_id))
                    return false;
            }
            /*Log.e("GGG", "dataSnapshot checking "+currentUsers.containsAll(dat)+" / "+currentUsers.get(0).user_id+
            " / "+dat.get(0).user_id);
            if(!currentUsers.containsAll(dat))
                return false;*/
        }
        return true;
    }
    boolean checkLocationListSameAsCurrent( ArrayList<LocationData> dat)
    {
        if(currentLocations==null)
            return false;
        else if(dat.size()!=currentLocations.size())
            return false;
        else
        {
            for(int i=0;i<currentLocations.size();i++)
            {
                if(!currentLocations.get(i).latitude.equals(dat.get(i).latitude) &&
                        !currentLocations.get(i).longitude.equals(dat.get(i).longitude) )
                {
                    Log.e("GGG", "dataSnapshot checking "+currentLocations.get(i).latitude+" / "+dat.get(i).latitude+"   //// "+currentLocations.get(i).longitude+" / "+dat.get(i).longitude);
                    return false;}
            }
            /*+
            " / "+dat.get(0).user_id);
            if(!currentUsers.containsAll(dat))
                return false;*/
        }
        return true;
    }
    ArrayList<UserWholeData> currentUsers;
    ChildEventListener feedEventListnerFromUserID=new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            onChildChanged(dataSnapshot, s);
        }

        @Override
        public void onChildChanged(final DataSnapshot dataSnapshot, String s) {


            fDatabase.child("feeds_location").child(dataSnapshot.getKey()).addListenerForSingleValueEvent(
                    new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot2) {
                            Double latt=Double.parseDouble(dataSnapshot2.child("l").child("0").getValue().toString());
                            Double lngg=Double.parseDouble(dataSnapshot2.child("l").child("1").getValue().toString());
                            Log.e("GGG","lat long is "+ latt+" / "+lngg);
                            handleInclommingFeeds(getApplicationContext(),dataSnapshot,new GeoLocation(latt, lngg),null);
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {}
                    });}
        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {}
        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {}
        @Override
        public void onCancelled(DatabaseError databaseError) {}};

    void updateFeedFromUserID(String userID)
    {
        //if(feedEventListnerFromUserID!=null)
      //  {
            fDatabase.child("feed_stack").orderByChild("userid").startAt(userID).endAt(userID).removeEventListener(feedEventListnerFromUserID);
            //feedEventListnerFromUserID=null;
        //}

        fDatabase.child("feed_stack").orderByChild("userid").startAt(userID).endAt(userID)
                .addChildEventListener(feedEventListnerFromUserID);

    }
    void updateFeedFromLocation(Double lat,Double lng)
    {

    Log.e("GGG","SEARCHING new feedRADIUShiygu "+ Double.parseDouble(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("pref_feed_radius","3")));
        geoQuery = geoFire.queryAtLocation(new GeoLocation(lat, lng),Double.parseDouble(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("pref_feed_radius","3")));
        geoQuery.addGeoQueryEventListener(feedFromLocationListner);

    }

   public static void handleInclommingFeeds(final Context context, DataSnapshot dataSnapshot, final GeoLocation location,final FeedDownloadListner feedDownloadListner)
   {
       // Get Post object and use the values to update the UI
       final GroupFeedData post= dataSnapshot.getValue(GroupFeedData.class);
       if(post==null)
           return;
       post.feedID=dataSnapshot.getKey();
       //Log.e("GGG","feedtext receved is1 "+post.text);

//CODEFEED10
       post.ifseen=2;

                       /* post.userid;
                        post.username;
                        post.numberType;
                        post.timestamp;
                        post.text;
                        post.semi_uri;
                        post.uri;*/


       Cursor cursor = DatabaseManager.getDatabase(context).getReadableDatabase().rawQuery(" select " + DatabaseAccesser.feed_col_user_id + " " +
               DatabaseAccesser.feed_col_timestamp + " from " + DatabaseAccesser.feed_table + " where " +
               DatabaseAccesser.feed_col_user_id + " = ? AND " +
               DatabaseAccesser.feed_col_timestamp + " = ?", new String[]{post.userid , post.timestamp + ""});


       if (cursor.getCount() == 0) {
           Log.e("GGG","NEW FEED "+ post.type+" / "+post.userid+" / "+post.text);
           // Log.e("MSGG","====new post");

           if (post.type == 0) {
               // finaldata.add(post);
               // adap.notifyItemInserted(finaldata.size() - 1);

               // adap.notifyItemChanged(finaldata.size() - 1);
               // rView.scrollToPosition(finaldata.size() - 1);
               //post.uri="1";
               post.media_id=1;

               saveFeedToDatabase(context,post,location);
               if(feedDownloadListner!=null)
                   feedDownloadListner.onFeedDownloaded(post);
           }
           else{
               long temp=checkForMediaAvailablility(post.uri);
               if(temp!=0)
               {
                   post.media_id=temp;
                   saveFeedToDatabase(context,post, location);
                   if(feedDownloadListner!=null)
                       feedDownloadListner.onFeedDownloaded(post);
                   cursor.close();
                   return;
               }

               String urlToDownload="null";
               final boolean autodownload;
               if(post.type==1) {
                   if (PrefManager.checkSearchMultiSelectPref(context,"data_usage_feed", TabbedActivity.PREF_IMAGE,R.array.data_usage_feed_default_values))
                   {
                       urlToDownload = post.uri;
                       autodownload = true;
                   } else {
                       urlToDownload = post.semi_uri;
                       autodownload = false;
                   }
               }
               else if(post.type==2)
               {
                   if (PrefManager.checkSearchMultiSelectPref(context,"data_usage_feed", TabbedActivity.PREF_AUDIO,R.array.data_usage_feed_default_values))//vvvv
                   {
                       urlToDownload = post.uri;
                       autodownload = true;
                   } else {
                       urlToDownload = post.semi_uri;
                       autodownload = false;
                   }
               }
               else if(post.type==3)
               {
                   if (PrefManager.checkSearchMultiSelectPref(context,"data_usage_feed", TabbedActivity.PREF_VIDEO,R.array.data_usage_feed_default_values))//vvvv
                   {
                       urlToDownload = post.uri;
                       autodownload = true;
                   } else {
                       urlToDownload = post.semi_uri;
                       autodownload = false;
                   }
               }
               else
               {
                   autodownload=false;
               }
               // final String local,litelocal;
               // Log.e("MSGG","1");
               ///xxxx
               MainServiceThread.downloadFileAndReturnUriString(context,post.semi_uri,true,1,post.type, new FileDownloadListner() {
                   @Override
                   public void onProgressListener(int percent) {

                   }

                   @Override
                   public void onSuccessListener(String localFilename) {
                       final String litelocal=localFilename;
                       //CODE 99 issue if  autodownlaod enabled, the onfeedreceived is not called...onFeedDownloaded isonly called
                       if(autodownload) {
                           MainServiceThread.downloadFileAndReturnUriString(context,post.uri,false,1,post.type, new FileDownloadListner() {
                               @Override
                               public void onProgressListener(int percent) {

                               }

                               @Override
                               public void onSuccessListener(String localFilename) {
                                   final String local = localFilename;
                                   //Log.e("MSGG","2");
                                   post.media_id= DatabaseManager.insertMediaToDatabase(context,post.uri, local, post.semi_uri, litelocal,post.mediaSize);
                                   Log.e("GGG","SAVING IMAGE TO FB "+ post.media_id+" / "+local+" / "+litelocal);
                                   saveFeedToDatabase(context,post, location);
                                   if(feedDownloadListner!=null)
                                       feedDownloadListner.onFeedDownloaded(post);
                               }

                               @Override
                               public void onFailureListener(int percent) {

                               }

                               @Override
                               public void onPausedListener(int percent) {

                               }
                           });
                       }
                       else
                       {
                           //post.semi_uri +=">>"+ file.toURI().toString() + name;
                           //Log.e("MSGG","3");

                           post.media_id=DatabaseManager.insertMediaToDatabase(context,post.uri, "null", post.semi_uri, litelocal,post.mediaSize);
                           Log.e("GGG","SAVING IMAGE TO FB "+ post.media_id+" / "+"null"+" / "+litelocal);
                           saveFeedToDatabase(context,post, location);
                           if(feedDownloadListner!=null)
                               feedDownloadListner.onFeedReceived(post,autodownload);
                           //post.uri="null";
                       }
                   }

                   @Override
                   public void onFailureListener(int percent) {

                   }

                   @Override
                   public void onPausedListener(int percent) {

                   }
               });


           }
       }


       cursor.close();
   }


    /*void saveContactToDatabase(ContactListData post,String globalUrl)
    {
        //SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();


        ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.user_user_id, post.user_id);
        values.put(DatabaseAccesser.user_name, post.user_name);
        values.put(DatabaseAccesser.user_status, post.status);
        if(post.user_id.equals(userdetail.getUid()))
        {
            values.put(DatabaseAccesser.user_user_type, 0);
        } else
        {
            values.put(DatabaseAccesser.user_user_type, post.type);
        }
        values.put(DatabaseAccesser.user_globalUrl,globalUrl);
        values.put(DatabaseAccesser.user_localUrl, post.img_url);
        values.put(DatabaseAccesser.user_timestamp, System.currentTimeMillis());



        long newRowId = writableDB.insert(DatabaseAccesser.user_table, null, values);
    }*/

    static void saveFeedToDatabase(Context context,GroupFeedData post, GeoLocation location)
    {
        // SQLiteDatabase db = dbAccess.getWritableDatabase();

        //  Toast.makeText(getActivity(), "Now saving to database", Toast.LENGTH_SHORT).show();
        ContentValues values = new ContentValues();
        values.put(DatabaseAccesser.feed_col_sl_no, post.sl);
        values.put(DatabaseAccesser.feed_col_user_id, post.userid);
        values.put(DatabaseAccesser.feed_col_feed_id, post.feedID);
        values.put(DatabaseAccesser.feed_col_username, post.username);
        values.put(DatabaseAccesser.feed_col_timestamp, post.timestamp);
        values.put(DatabaseAccesser.feed_col_type, post.type);
        values.put(DatabaseAccesser.feed_col_allowdownload, post.allowdownload);
        values.put(DatabaseAccesser.feed_col_seen, post.ifseen);
        values.put(DatabaseAccesser.feed_col_text, post.text);
        values.put(DatabaseAccesser.feed_col_media_id, post.media_id);
        //values.put(DatabaseAccesser.feed_col_url, post.uri);
        values.put(DatabaseAccesser.feed_col_latitude, location.latitude);
        values.put(DatabaseAccesser.feed_col_longitude, location.longitude);


        long newRowId = DatabaseManager.getDatabase(context).getWritableDatabase().insert(DatabaseAccesser.feed_table, null, values);
        Log.e("GGG"," inseted row "+ newRowId);

        Intent new_intent = new Intent();
        new_intent.setAction("FEED_UPDATED");
        context.sendBroadcast(new_intent);
    }


}
