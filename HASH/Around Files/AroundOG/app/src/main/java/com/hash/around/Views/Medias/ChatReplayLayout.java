package com.hash.around.Views.Medias;

import android.app.NotificationManager;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.Shader;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.util.IOUtils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.hash.around.OtherClasses.DataTypes.ChatData;
import com.hash.around.OtherClasses.DataTypes.ChatReplayData;
import com.hash.around.OtherClasses.DataTypes.FeedData;
import com.hash.around.OtherClasses.DataTypes.GroupChatData;
import com.hash.around.OtherClasses.DataTypes.GroupFeedData;
import com.hash.around.OtherClasses.DataTypes.MediaData;
import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.hash.around.OtherClasses.DataTypes.VCardData;
import com.hash.around.R;
import com.hash.around.TabbedActivity;
import com.hash.around.Utils.ByteUtils;
import com.hash.around.Utils.LocalNotificationManager;
import com.hash.around.Utils.StringUtils;
import com.hash.around.Views.ViewFunctions;
import com.vanniktech.emoji.EmojiTextView;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


public class ChatReplayLayout extends RelativeLayout {

    ImageView thumbline,replayColorImage,quote_dismiss;
    TextView heading;
    EmojiTextView description;
    RelativeLayout icon_container;

    public ChatReplayLayout(Context context) {
        this(context, null);
    }

    public ChatReplayLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ChatReplayLayout(final Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        inflate(context, R.layout.chat_replay_viewsection, this);
        thumbline=findViewById(R.id.thumbline);
        replayColorImage=findViewById(R.id.replayColorImage);
        heading=findViewById(R.id.heading);
        description=findViewById(R.id.description);
        quote_dismiss=findViewById(R.id.quote_dismiss);
        icon_container=findViewById(R.id.icon_container);

        if (attrs != null) {
            /*TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CircularRelativeLayout, 0, 0);
            this.radius   = typedArray.getInt(R.styleable.CircularRelativeLayout_radius, 4);
            typedArray.recycle();*/
        }


    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        initViews();
    }

    public void initViews()
    {
        quote_dismiss.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
               hide();
            }
        });
    }

    public void setImageUri(Uri uri)
    {
        thumbline.setImageURI(uri);
    }
    public void setImageBitmap(Bitmap uri)
    {
        thumbline.setImageBitmap(uri);
    }
    public void hideImageThumbnail()
    {
        icon_container.setVisibility(GONE);
    }
    public void showImageThumbnail()
    {
        icon_container.setVisibility(VISIBLE);
    }
    /*public void setHeadingAndDescription(String head,String descrip)
    {
        heading.setText(head);
        description.setText(descrip);
    }*/

    ChatReplayData finalData=null;
   /* public ChatReplayData getReplayData()
    {
        return finalData;
    }*/
    public void setReplayDataFromFeed(GroupFeedData feed)
    {
        /*ChatReplayData dat=new ChatReplayData();

        dat.userName=feed.username;
        dat.userID=feed.userid;
        dat.type=2;


        String type=StringUtils.convertMediaTypeToString(feed.type);
        dat.description=(!type.equals("")?"("+type+") ":"")+feed.text;
        //code 120

        dat.contentUri=feed.semi_uri;*/
        //finalData=dat;
        finalData=ChatReplayData.getReplayDataFromFeedData(feed);
        setReplayData(finalData,true);
    }
    public void setReplayDataFromChat(GroupChatData chat, UserWholeData userdat)
    {
        Log.e("GGG","local url iss "+chat.temp_url);
        ChatReplayData dat=new ChatReplayData();

        dat.userName=userdat.user_name;
        if(chat.sender==0)
            dat.userID= FirebaseAuth.getInstance().getCurrentUser().getUid();
        else
            dat.userID=chat.userid;

        Log.e("GGG","row repay : "+dat.userID);
       // if(chat.sender==0)
           // dat.userID=TabbedActivity.userdetail.getUid();
        dat.type=1;
        dat.content_sl=chat.sl;

        String type=StringUtils.convertMediaTypeToString(chat.type);
        dat.description=(!type.equals("")?"("+type+") ":"")+chat.msg;
        //code 120

        if(chat.temp_url==null||chat.temp_url.contains("http") || chat.temp_url.length()<5)
        {   dat.contentUri="null";
            Log.e("GGG","local content uri NULL "+dat.contentUri);}
        else
        {  dat.contentUri= ByteUtils.byteToString(ByteUtils.bitmapToByteArray(ByteUtils.getBitmapFromuri(chat.temp_url),20));
            Log.e("GGG","local content uri NOT "+dat.contentUri);}


        finalData=dat;
        setReplayData(dat,true);
    }

    public void setReplayData(ChatReplayData dat,boolean smoothScroll)  //(for chat data adapter)
    {

        heading.setText((dat.userID.equals(FirebaseAuth.getInstance().getUid())?"You":dat.userName)+(dat.type==1?"":" | Feed"));
        description.setText(StringUtils.makeHashTagText(getContext(),dat.description,true));
        if(dat.contentUri==null||dat.contentUri.contains("null"))
            hideImageThumbnail();
        else
        { showImageThumbnail();
            setImageBitmap(ByteUtils.byteArrayToPhoto(ByteUtils.stringToByte(dat.contentUri)));}

            //thumbline.setImageBitmap(ByteUtils.byteArrayToPhoto(ByteUtils.stringToByte(dat.contentUri)));

        if(getVisibility()==GONE) {
            if(smoothScroll)
                show();
            else
                setVisibility(VISIBLE);
        }
    }
    public ChatReplayData getReplayData()  //(for chat data adapter)
    {
        return finalData;
    }
    public void disableCancelButton()
    {
        quote_dismiss.setVisibility(GONE);
    }

    public void show()
    {
        Log.e("GGG","swipeview show ");
        //ViewFunctions.animateButtonBurstIn(this,0,false);
        ViewFunctions.scaleView(this,0,1);
        ViewFunctions.AIanimation(this,getContext(),R.anim.toggle_animation_in_fromzero,200,0,VISIBLE);
    }
    public void hide()
    {
        //ViewFunctions.animateButtonBurstOut(this,0,false);
        //ViewFunctions.scaleView(this,1,0);
        ViewFunctions.AIanimation(this,getContext(),R.anim.toggle_animation_out_tozero,200,0,GONE);
        finalData=null;
    }


    private final RectF drawRect = new RectF();
    private final Path  clipPath = new Path();
    int dimen=getResources().getDimensionPixelSize(R.dimen.default_corner_radius);
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        drawRect.left = 0;
        drawRect.top = 0;
        drawRect.right = getWidth();
        drawRect.bottom = getHeight();
        clipPath.reset();
        clipPath.addRoundRect(drawRect, dimen, dimen, Path.Direction.CCW);
        clipPath.close();
        }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        int save = canvas.save();
        canvas.clipPath(clipPath);
        super.dispatchDraw(canvas);
        canvas.restoreToCount(save);
    }
}