package com.hash.around.OtherClasses.Interfaces;

public interface FileUploadListner {
    void onFileUploaded(String global,String globalLite,String liteLocal);

    void onProgressListener(int percent);
    void onFailureListener(int percent);
    void onPausedListener(int percent);
}


