package com.hash.around.OtherClasses;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.text.format.DateUtils;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hash.around.ChatWindow;
import com.hash.around.MainServiceThread;
import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.Profile_Activity;
import com.hash.around.R;
import com.hash.around.SoloImageViewActivity;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;

import static com.hash.around.MainServiceThread.newUserFollowRequestWithDoubleCheck;
import static com.hash.around.MainServiceThread.updateUserFollowStatus;

public class Adapter_Contacts extends RecyclerView.Adapter<Adapter_Contacts.ViewHolder> {

    Activity mContext;
    ArrayList<UserWholeData> data;

boolean simpleVersion=true;
    public SparseBooleanArray selectedUsers;

     boolean slectionActivity=false;
    Snackbar snack;

   static  Drawable temp=null;
   AdapterView.OnItemClickListener itemClickListener;
    public Adapter_Contacts(Activity mcont, ArrayList<UserWholeData> list,boolean simpleVer,boolean selection,AdapterView.OnItemClickListener itemClickListener)
    {
        mContext=mcont;
        data=list;
        selectedUsers=new SparseBooleanArray();
        slectionActivity=selection;
        this.itemClickListener=itemClickListener;
        this.simpleVersion=simpleVer;

    }

    public void updateDataList(ArrayList<UserWholeData> list)
    {
        data=list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        /*FUUUUUUULL CHANGE*/
        switch(viewType) {
            case 0:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.single_contact_view, parent, false);
                // set the view's size, margins, paddings and layout parameters

                ViewHolder vh = new ViewHolder(v);
                return vh;
            case 1:
             v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.single_contact_view, parent, false);
            // set the view's size, margins, paddings and layout parameters

           /* ViewHolder2 vh2 = new ViewHolder(v);
            return vh2;*/
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        Log.e("GGG","username is "+data.get(position).user_name);
        final int pos = position;
        final RelativeLayout rView = holder.wholeView;
        final Drawable dr = holder.temp;

        if (selectedUsers.get(position))
        {
            rView.setBackgroundColor(Color.parseColor("#20000000"));
            holder.imageSelectionTick.setVisibility(View.VISIBLE);
        }
        else
        {
            rView.setBackground(dr);
            holder.imageSelectionTick.setVisibility(View.GONE);
        }



        holder.name.setText(data.get(position).user_name);

        if (checkIfPrivacyShouldBeActivie(data.get(pos).priv_profile_status,data.get(pos).remoteContact)) {
            String status = data.get(position).status;
            if (status.equals("null"))
                status = "Wanna know an App called Around?";

            holder.status.setText(status);
        }
        else
        {
            holder.status.setText("");
        }
       final  String dir=data.get(position).img_url;

        if (checkIfPrivacyShouldBeActivie(data.get(pos).priv_profile_picture,data.get(pos).remoteContact) && !dir.equals("null"))
        {
                holder.proimg.setImageURI(Uri.parse(dir));
                holder.proimg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent solophoto=new Intent(mContext,SoloImageViewActivity.class);
                        solophoto.setData(Uri.parse(dir));
                        mContext.startActivity(solophoto);
                    }
                });
        }
        else {
            holder.proimg.setImageResource(R.drawable.default_user);
            holder.proimg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent solophoto=new Intent(mContext,SoloImageViewActivity.class);
                    mContext.startActivity(solophoto);
                }
            });
        }





        final SwitchCompat compatSwitch=holder.switchFollow;
        compatSwitch.setChecked(data.get(pos).followType==1||data.get(pos).followType==3);
        setCompatSwitch(mContext,compatSwitch,data.get(pos).followType==1);
        Log.e("GGG","followfolloww "+data.get(pos).user_name+" "+data.get(pos).followType);
        if(data.get(pos).followType==3){
           // compatSwitch.setChecked(true);
            setCompatSwitchFollowRequested(mContext,compatSwitch,true);
            //compatSwitch.setEnabled(false);
        }

        compatSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(compatSwitch.isChecked()){
                    compatSwitch.setChecked(!compatSwitch.isChecked());
                    Log.e("GGG","followfolloww change 1 ");
                    newUserFollowRequestWithDoubleCheck((Activity) mContext,data.get(pos).user_id, new JobCompletionListner() {
                        @Override
                        public void onJobCompletion(String txt1) {
                            data.get(pos).followType=Integer.parseInt(txt1);
                            if(data.get(pos).followType==1)
                            {
                                //Log.e("GGG","dfghfgh "+data.get(pos).followType);
                                compatSwitch.setChecked(true);
                                //compatSwitch.setEnabled(true);
                                setCompatSwitch(mContext,compatSwitch,true);}
                            else if(data.get(pos).followType==3)
                            {
                                compatSwitch.setChecked(true);
                                setCompatSwitchFollowRequested(mContext,compatSwitch,true);
                            }
                            else
                            { compatSwitch.setChecked(false);
                                setCompatSwitch(mContext,compatSwitch,false);
                            }
                            //notifyItemChanged(pos);

                        }
                    });}
                else {
                    compatSwitch.setChecked(!compatSwitch.isChecked());
                    MainServiceThread.updateuserFollowStatusWithRequestedCheck(mContext, data.get(pos).followType, data.get(pos).user_id, new JobCompletionListner() {
                        @Override
                        public void onJobCompletion(String txt1) {
                            data.get(pos).followType=Integer.parseInt(txt1);
                            compatSwitch.setChecked(false);
                            setCompatSwitch(mContext,compatSwitch,false);
                        }
                    });
                    //updateUserFollowStatus(data.get(pos).user_id, 0);

                }

            }
        });
       /* compatSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.e("MSGG","changed called");
                compatSwitch.setChecked(!isChecked);
                if(isChecked){
                    Log.e("GGG","followfolloww change 1 ");
                    newUserFollowRequestWithDoubleCheck((Activity) mContext,data.get(pos).user_id, new JobCompletionListner() {
                        @Override
                        public void onJobCompletion(String txt1) {
                            data.get(pos).followType=Integer.parseInt(txt1);
                            if(data.get(pos).followType==1)
                            {
                                Log.e("GGG","dfghfgh "+data.get(pos).followType);
                                compatSwitch.setChecked(true);
                                compatSwitch.setEnabled(true);
                                setCompatSwitch(mContext,compatSwitch,true);}
                            else if(data.get(pos).followType==3)
                            {
                                compatSwitch.setChecked(true);
                                setCompatSwitchFollowRequested(mContext,compatSwitch,true);
                            }
                            else
                            { compatSwitch.setChecked(false);
                                setCompatSwitch(mContext,compatSwitch,false);
                            }
                            //notifyItemChanged(pos);

                        }
                    });}
                else {

                    MainServiceThread.updateuserFollowStatusWithRequestedCheck(mContext, data.get(pos).followType, data.get(pos).user_id, new JobCompletionListner() {
                        @Override
                        public void onJobCompletion(String txt1) {
                            data.get(pos).followType=0;
                            compatSwitch.setChecked(false);
                            setCompatSwitch(mContext,compatSwitch,false);
                        }
                    });
                    //updateUserFollowStatus(data.get(pos).user_id, 0);

                }
            }
        });*/


        holder.wholeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(slectionActivity)
                {
                    if (selectedUsers.get(pos)) {
                        selectedUsers.delete(pos);
                        //rView.setBackground(dr);
                        notifyItemChanged(pos);
                    }
                    else {
                        selectedUsers.put(pos,true);
                        notifyItemChanged(pos);
                        //rView.setBackgroundColor(Color.parseColor("#20000000"));
                    }

                    if(selectedUsers.size()>0)
                    {
                        String snackText="";
                        for (int i = selectedUsers.size()-1; i>=0; i--) {

                            if(i==selectedUsers.size()-1)
                                 snackText+=data.get(selectedUsers.keyAt(i)).user_name;
                            else
                                snackText+=", "+data.get(selectedUsers.keyAt(i)).user_name;
                        }

                        if(snack==null) {
                            snack = Snackbar.make(v, snackText, Snackbar.LENGTH_INDEFINITE);
                            snack.show();

                        }
                        else
                        {
                            snack.setText(snackText);
                        }
                    }
                    else {
                        snack.dismiss();
                        snack=null;
                        //remove snackbar
                    }
                }
                else {
                    Context cont = mContext;
                    Intent indi = new Intent(cont, ChatWindow.class);
                    indi.putExtra("userid", data.get(pos).user_id);
                    indi.putExtra("username", data.get(pos).user_name);
                    ((AppCompatActivity) mContext).finish();
                    cont.startActivity(indi);
                }
                if(itemClickListener!=null)
                itemClickListener.onItemClick(null,holder.wholeView,pos,0);
            }
        });

        ///if(data.get(position).) checkfor privacy  CODE-01
        Log.e("GGG","user type is "+data.get(position).type);
        if(data.get(position).type<14)
        checkSingleOnlineStatus(data.get(position).user_id,holder.userOnlineStatus);
        else
        {
            holder.userOnlineStatus.setVisibility(View.VISIBLE);
            holder.userOnlineStatus.setCompoundDrawables(null,null,null,null);
            holder.userOnlineStatus.setTextColor(mContext.getResources().getColor(R.color.blue_500));
            if(data.get(position).type==15)
                holder.userOnlineStatus.setText("GROUP");
            else if(data.get(position).type==16)
                holder.userOnlineStatus.setText("CHANNEL");
        }

       if( simpleVersion || data.get(pos).type==15 || data.get(pos).type==16)
       {
           holder.switchFollow.setVisibility(View.GONE);
       }
       else
       {
           holder.switchFollow.setVisibility(View.VISIBLE);
       }

    }

    void setCompatSwitch(Context context,SwitchCompat compatSwitch,boolean enabled)
    {
        /*if (enabled)
            compatSwitch.setCompoundDrawables(context.getResources().getDrawable(R.drawable.ic_notifications_following),null,null,null);
        else
            compatSwitch.setCompoundDrawables(context.getResources().getDrawable(R.drawable.ic_notifications_white),null,null,null);
        */

        compatSwitch.getCompoundDrawables()[0].setColorFilter(mContext.getResources().getColor(enabled?R.color.colorAccent:R.color.semiblack_drawable), PorterDuff.Mode.SRC_IN);
        compatSwitch.getCompoundDrawables()[0].mutate();
    }
    void setCompatSwitchFollowRequested(Context context,SwitchCompat compatSwitch,boolean enabled)
    {   /*if (enabled)
        compatSwitch.setCompoundDrawables(context.getResources().getDrawable(R.drawable.ic_notifications_requested),null,null,null);
        else
        compatSwitch.setCompoundDrawables(context.getResources().getDrawable(R.drawable.ic_notifications_white),null,null,null);
*/
        compatSwitch.getCompoundDrawables()[0].mutate();
        compatSwitch.getCompoundDrawables()[0].setColorFilter(mContext.getResources().getColor(enabled?R.color.red_black:R.color.semiblack_drawable), PorterDuff.Mode.SRC_IN);
    }
    public static boolean checkIfPrivacyShouldBeActivie(int privacy,int remoteContact)
    {
        if(privacy<2)
        {
            return true;
        }
        else if (privacy==2)
        {
            if(remoteContact==1)
            { Log.e("MGG","USER PRIVACY SET TO CONTACTS ONLY....SINCE THE USER IS REMOTE CONTACT");
                return true;}
            else {
                Log.e("MGG","USER PRIVACY SET TO CONTACTS ONLY..");
                return false;
            }

        }
        else
        {
            return false;
        }
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        // write code to remove listners to the online status CODE-02
    }

    void checkSingleOnlineStatus(String userId, final TextView updateText)
    {
        final long currentTimestamp=System.currentTimeMillis();
        FirebaseDatabase.getInstance().getReference().child("user_profile").child(userId).
                child("online_status").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot==null)
                {
                    Log.e("GGG","snapshot NULLLLLL");
                    updateText.setVisibility(View.GONE);
                    return;
                }
                String status=dataSnapshot.child("status").getValue().toString();
                long timeData=Long.parseLong(dataSnapshot.child("timestamp").getValue().toString());
                if(status.equals("online") && timeData+60000<currentTimestamp)
                {
                    updateText.setVisibility(View.VISIBLE);
                    Log.e("GGG","snapshot ONLINE");
                }
                else
                {

                }
                if((timeData+15000)>System.currentTimeMillis() && status.equals("online"))
                {
                    updateText.setVisibility(View.VISIBLE);
                    updateText.setTextColor(mContext.getResources().getColor(R.color.green_dark));
                }
                else
                {
                    updateText.setText(DateUtils.getRelativeTimeSpanString(timeData));
                    updateText.setTextColor(mContext.getResources().getColor(R.color.semiblack_drawable));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }



    @Override
    public int getItemCount() {
        if(data!=null) {
            return data.size();
        }
        return 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name;
        RelativeLayout wholeView;

        public TextView status;
        public ImageView proimg;
        public Drawable temp=null;

        public CircularImageView imageSelectionTick;
        SwitchCompat switchFollow;

        TextView userOnlineStatus;
        public ViewHolder(View item)
        {
            super(item);
            name=(TextView)item.findViewById(R.id.contactName);
            status=(TextView)item.findViewById(R.id.contactStatus);
            proimg=(ImageView) item.findViewById(R.id.contactImg);

            imageSelectionTick=(CircularImageView) item.findViewById(R.id.imageSelectionTick);
            wholeView=(RelativeLayout)item.findViewById(R.id.singleContact);

            userOnlineStatus=(TextView)item.findViewById(R.id.userOnlineStatus);
            switchFollow=(SwitchCompat) item.findViewById(R.id. switchFollow);
            if(temp==null) {
                temp = wholeView.getBackground();
            }

        }
    }

}


