package com.hash.around.OtherClasses.Interfaces;

public interface FileDownloadListner {
    void onProgressListener(int percent);
    void onSuccessListener(String  localFilename);
    void onFailureListener(int percent);
    void onPausedListener(int percent);
}


