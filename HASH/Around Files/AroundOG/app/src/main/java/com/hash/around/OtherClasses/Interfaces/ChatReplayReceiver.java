package com.hash.around.OtherClasses.Interfaces;

import com.hash.around.OtherClasses.DataTypes.ChatReplayData;


public interface ChatReplayReceiver {
    void onChatReplayReceived(ChatReplayData data);
    //void onChatUploaded(GroupChatData data);
}


