package com.hash.around.Views.CustomMakers;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Build;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.hash.around.ChatForwardActivity;
import com.hash.around.OtherClasses.ChatDataAdapter;
import com.hash.around.R;
import com.hash.around.Views.ViewFunctions;

public class ChatForward_UserTypeHeader extends  RecyclerView.ItemDecoration{
    Context mContext;
    //ChatDataAdapter adapter;
    ChatForwardActivity activity;
    public ChatForward_UserTypeHeader(Context context, ChatForwardActivity mainClass)
    {
        // mDivider = ContextCompat.getDrawable(context, R.drawable.ic_fingerprint_color_primary);
        mContext=context;
        this.activity=mainClass;
       // this.adapter=adap;
    }

    public boolean hasHeader(int adapPos,RecyclerView par)
    {
        /*if(this.lastSeenTimestamp==0)
            return false;*/
        //Log.e("MSG","this.lastSeenTimestamp "+this.lastSeenTimestamp);
        return activity.checkForHeader(adapPos,isReverseLayout(par));//adapter.hasUnseenHeader(adapPos,isReverseLayout(par),this.lastSeenTimestamp);
    }
    public boolean isReverseLayout(final RecyclerView parent) {
        return (parent.getLayoutManager() instanceof LinearLayoutManager) &&
                ((LinearLayoutManager)parent.getLayoutManager()).getReverseLayout();
    }
    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        final int count = parent.getChildCount();

        for (int layoutPos = 0; layoutPos < count; layoutPos++) {
            final View child = parent.getChildAt(translatedChildPosition(parent, layoutPos));

            final int adapterPos = parent.getChildAdapterPosition(child);

            if (adapterPos != RecyclerView.NO_POSITION && hasHeader(adapterPos,parent)) {
                View header = getHeader(parent, mContext,adapterPos,false).itemView;

                c.save();
                final int left = child.getLeft();
                final int top = getHeaderTop(parent, child, header, adapterPos, layoutPos);
                c.translate(left, top);
                header.draw(c);
                c.restore();


            }
        }
    }

    /*public  boolean hasHeader(int adapPos,RecyclerView par)
    {
        return false;
    }*/
    public int translatedChildPosition(RecyclerView parent, int position) {
        return isReverseLayout(parent) ? parent.getChildCount() - 1 - position : position;
    }

    public int getChildY(RecyclerView parent, View child) {
        if (Build.VERSION.SDK_INT < 11) {
            Rect rect = new Rect();
            parent.getChildVisibleRect(child, rect, null);
            return rect.top;
        } else {
            return (int) ViewCompat.getY(child);
        }
    }

    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        int position     = parent.getChildAdapterPosition(view);
        int headerHeight = 0;

        if (position != RecyclerView.NO_POSITION && hasHeader(position,parent)) {
            View header = getHeader(parent, mContext, position,true).itemView;
            headerHeight = header.getHeight();
        }
        outRect.set(0, headerHeight, 0, 0);
    }


    protected int getHeaderTop(RecyclerView parent, View child, View header, int adapterPos,
                               int layoutPos)
    {
        int headerHeight = header.getHeight();
        int top = getChildY(parent, child) - headerHeight;


        return parent.getLayoutManager().getDecoratedTop(child);
    }

    RecyclerView.ViewHolder getHeader(RecyclerView parent, Context context, int position,boolean forHeight)
    {
       /* final RecyclerView.ViewHolder holder = new ChatView_DateHeader.HeaderViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chatforward_tag_usertype, parent, false));
        final View header = holder.itemView;*/

        RecyclerView.ViewHolder  viewHolder= new ChatView_DateHeader.HeaderViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chatforward_tag_usertype, parent, false));
        this.activity.bindHeader(viewHolder,position);

        int widthSpec  = View.MeasureSpec.makeMeasureSpec(parent.getWidth(), View.MeasureSpec.EXACTLY);
        int heightSpec = View.MeasureSpec.makeMeasureSpec(parent.getHeight(), View.MeasureSpec.UNSPECIFIED);

        int childWidth  = ViewGroup.getChildMeasureSpec(widthSpec, parent.getPaddingLeft() + parent.getPaddingRight(), viewHolder.itemView.getLayoutParams().width);
        int childHeight = ViewGroup.getChildMeasureSpec(heightSpec, parent.getPaddingTop() + parent.getPaddingBottom(), viewHolder.itemView.getLayoutParams().height);

        viewHolder.itemView.measure(childWidth, childHeight);
        viewHolder.itemView.layout(0, 0, viewHolder.itemView.getMeasuredWidth(), viewHolder.itemView.getMeasuredHeight());


        //headerCache.put(key, holder);

        return viewHolder;

    }



}

