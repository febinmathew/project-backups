package com.hash.around;

import android.Manifest;
import android.app.Activity;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.hash.around.OtherClasses.Adapter_Contacts;
import com.hash.around.OtherClasses.DataTypes.ChatReplayData;
import com.hash.around.OtherClasses.DataTypes.ContactListData;
import com.hash.around.OtherClasses.DataTypes.GroupChatData;
import com.hash.around.OtherClasses.DataTypes.MediaData;
import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.Utils.AppOnlineStatusManager;
import com.hash.around.Utils.ChatSender;
import com.hash.around.Utils.DatabaseManager;
import com.hash.around.Utils.MediaManager;
import com.hash.around.Utils.NetworkUtils;
import com.hash.around.Utils.UserManager;
import com.hash.around.Views.CustomMakers.ChatForward_UserTypeHeader;
import com.hash.around.Views.ViewFunctions;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import static android.view.View.GONE;
import static com.hash.around.TabbedActivity.fDatabase;
import static com.hash.around.TabbedActivity.saveContactToDatabase;

public class ChatForwardActivity extends AppCompatActivity {


    DatabaseReference newPostRef;
    ChildEventListener childEventListener;
    public ArrayList<ContactListData> generateDummy()
    {
        ArrayList<ContactListData> list=new ArrayList<ContactListData>();



        return list;
    }

    RecyclerView rView;
    Adapter_Contacts adap;
    ArrayList<UserWholeData> finaldata;
    private LinearLayoutManager mLayoutManager;

    ArrayList<GroupChatData> forwardchatData;

    boolean chatforwardActivity=false;
    boolean externalAppSharingMedia=false;
    boolean dataBackwardActivity=false;
    LinearLayout newFeed;

    void sentChatAsFeed(ArrayList<GroupChatData> chats)
    {
        ArrayList<MediaData> mediaData=new ArrayList<MediaData>();
        for (GroupChatData indi:chats) {
                MediaData temp=indi.getMediaData();
                if(temp!=null)
                    mediaData.add(temp);

        }
        if(mediaData.size()>0){
        //UploadActivity.receiverAcitvity=2;
        Intent uploadIntent = new Intent(this, UploadActivity.class);
        //uploadIntent.putExtra("numberType", 6);
        //uploadIntent.putExtra("uri", url.uri);
        uploadIntent.putExtra("mediaarray", mediaData);
        uploadIntent.putExtra("forChat", 2);
        finish();
        uploadIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(uploadIntent);
        }
        else
        {
            Toast.makeText(this, "Can't post as feed", Toast.LENGTH_SHORT).show();
        }
    }
    FloatingActionButton contactFloatButton;
    //TextView toolbarStatus;

    void handleActionSENDIntent(Bundle savedInstanceState)
    {
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            chatforwardActivity=true;
            externalAppSharingMedia=true;
            forwardchatData=new ArrayList<>();
            String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
            Uri imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);

            addMediaAsChat(imageUri,sharedText,type);

        } else if (Intent.ACTION_SEND_MULTIPLE.equals(action) && type != null) {
            chatforwardActivity=true;
            externalAppSharingMedia=true;
            forwardchatData=new ArrayList<>();
            handleSendMultipleImages(intent);
        } else {
            // Handle other intents, such as being started from the home screen
        }
    }
    /*void handleSendText(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        //Uri imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
        if (sharedText != null) {
            GroupChatData temp=new GroupChatData();
            temp.type=0;
            temp.final_url=null;

            temp.msg=sharedText;

            forwardchatData.add(temp);
        }
    }*/
    /*void handleSendImage(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        Uri imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);

    }*/

    void handleSendMultipleImages(Intent intent) {
        ArrayList<Uri> imageUris = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
        //ArrayList<String> text = intent.getParcelableArrayListExtra(Intent.EXTRA_TEXT);
        String type = intent.getType();
        if (imageUris != null) {

            for (Uri singleFile:imageUris) {

                if(type.length()>3)
                {
                }
                else
                type=MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(singleFile.toString()));

                addMediaAsChat(singleFile,null,type);
            }
        }
    }

    void addMediaAsChat(Uri uri,String text,String mimeType)
    {
        /*if ("text/plain".equals(mimeType)) {
            handleSendText(intent); // Handle text being sent
        }
        else if (mimeType.startsWith("image/")||mimeType.startsWith("video/")||mimeType.startsWith("audio/")) {*/

                //Log.e("GGG","intent image 55 "+
                //MediaManager.getMediaOgUriFromTemp4(getApplicationContext(),imageUri,intent.getType()));
                GroupChatData temp = new GroupChatData();
        if ("text/plain".equals(mimeType)) {
            temp.type = 0;
            if(text==null)
                temp.type =6;
            Log.e("GGG", "forwardmedia TEXT IS " + text);
        }
        else if (mimeType.startsWith("image/"))
        {
            temp.type = 1;
        }
        else if (mimeType.startsWith("audio/")) {
            temp.type = 2;
        }
        else if (mimeType.equals("video/gif")) {
            temp.type = 5;
        }
        else if (mimeType.startsWith("video/")) {
            temp.type = 4;
        }
        else
        {
            temp.type = 6;
        }


                if (text != null)
                    temp.msg = text;
                else
                {
                    temp.msg = "";
                }


                if(uri!=null) {
                    Uri ddd = MediaManager.getMediaOgUriFromTemp2(getApplicationContext(), uri);
                    if (ddd != null)
                        temp.final_url = Uri.fromFile(new File(ddd.toString())).toString();
                    if (temp.final_url == null) {

                        temp.final_url = MediaManager.getMediaUriAfterCopyingtoCache(getApplicationContext(), uri, mimeType).toString();
                        Log.e("GGG", "forwardmedia image COPY must " + temp.final_url);
                    } else
                        Log.e("GGG", "forwardmedia image FIRST ONE NO COPY" + temp.final_url);
                    //temp.final_url=Uri.fromFile(new File(MediaManager.getMediaOgUriFromTemp3(getApplicationContext(),imageUri).toString())).toString();
                    if (temp.final_url == null)
                        return;
                }
                else
                {
                    temp.final_url=null;
                }
                Log.e("GGG","forwardmedia added "+mimeType+" code "+temp.type+" "+temp.final_url+" "+temp.msg);
                forwardchatData.add(temp);



       /* else if (mimeType.startsWith("audio/")) {
            Log.e("GGG","intent audio");
        }
        else if (mimeType.startsWith("video/")) {
            Log.e("GGG","intent video");
        }*/
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_forward_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Forward to...");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        newFeed=findViewById(R.id.newFeed);
        Bundle args = getIntent().getBundleExtra("BUNDLE");
        if(args!=null) {
            chatforwardActivity=true;
                forwardchatData = (ArrayList<GroupChatData>) args.getSerializable("ARRAYLIST");

        }
        else if(getIntent().getBooleanExtra("returnData",false))
        {
            dataBackwardActivity=true;
        }

        handleActionSENDIntent(savedInstanceState);

        if(chatforwardActivity)
        {
            newFeed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sentChatAsFeed(forwardchatData);
                }
            });
            for(GroupChatData test:forwardchatData)
            {
                if(test.type==4||test.type>=6) {
                    newFeed.setVisibility(GONE);
                    break;
                }
            }
        }


         contactFloatButton=(FloatingActionButton) findViewById(R.id.contactFloatButton);

        if(chatforwardActivity) {

            contactFloatButton.setImageResource(R.drawable.ic_send_white);
            contactFloatButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!externalAppSharingMedia){
                    for (int i = adap.selectedUsers.size() - 1; i >= 0; i--) {

                        for (int k = 0; k < forwardchatData.size(); k++) {
                            Log.e("GGG","intent new chat FORWARD");
                           // Log.e("GGG","row forward media id EARLIER "+forwardchatData.get(k).media_id);
                            ChatSender.AIMessageForwardSender(getApplicationContext(),forwardchatData.get(k),forwardchatData.get(k).chatReplay,finaldata.get(adap.selectedUsers.keyAt(i)).user_id,null);
                        }
                        finish();
                    }}
                    else
                    {
                        for (int i = adap.selectedUsers.size() - 1; i >= 0; i--) {

                            for (int k = 0; k < forwardchatData.size(); k++) {

                                //receiverData=(MediaData)intent.getSerializableExtra("mediaarray");
                                //replayData=(ChatReplayData)intent.getSerializableExtra("replayData");

                                //userData=(UserWholeData) intent.getSerializableExtra("userData");


                                ChatSender.AIMessageSender(getApplicationContext(),forwardchatData.get(k).type,forwardchatData.get(k).final_url,forwardchatData.get(k).msg,null,finaldata.get(adap.selectedUsers.keyAt(i)),null,false);

                            }
                        }
                    }

                    adap.selectedUsers.clear();
                    //adap.notifyDataSetChanged();
                   // finish();
                }
            });
        }
        else if(dataBackwardActivity)
        {
            contactFloatButton.setImageResource(R.drawable.ic_send_white);
            contactFloatButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ArrayList<String> grpuser=new ArrayList<String>();
                    for (int i = adap.selectedUsers.size() - 1; i >= 0; i--) {

                        grpuser.add(finaldata.get(adap.selectedUsers.keyAt(i)).user_id);

                    }

                    adap.selectedUsers.clear();


                    Intent intent=new Intent();
                    Bundle args = new Bundle();
                    args.putSerializable("ARRAYLIST",(Serializable)grpuser);
                    intent.putExtra("BUNDLE",args);

                    setResult(Activity.RESULT_OK,intent);
                    finish();
                }
            });
        }
        else
        {
            contactFloatButton.setImageResource(R.drawable.fab_add);
            contactFloatButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
                    intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
                    startActivity(intent);
                }
            });
        }
        //toolbarStatus=findViewById(R.id.toolbarStatus);
        rView=(RecyclerView)findViewById(R.id.rView);
        //rView.setHasFixedSize(true);
        rView.setNestedScrollingEnabled(false);
        //cView=(CardView)rootView.findViewById(R.id.cView);
        mLayoutManager=new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);
        //mLayoutManager=new GridLayoutManager(this,2,LinearLayoutManager.HORIZONTAL,false);
        //mLayoutManager=new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);
        //mLayoutManager.setStackFromEnd(true);
        rView.setLayoutManager(mLayoutManager);

        finaldata=new ArrayList<UserWholeData>();



        //


        adap=new Adapter_Contacts(ChatForwardActivity.this, finaldata,true, chatforwardActivity || dataBackwardActivity, new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //fill codes to update UI
                setFloatButtonVisibility();
            }
        });
        rView.setAdapter(adap);
        //getuserListFromDB(getApplicationContext());
        ChatForward_UserTypeHeader header=new ChatForward_UserTypeHeader(this,this);
        rView.addItemDecoration(header);
        getchatListFromDatabase(getApplicationContext());
        //updateContent();
        setFloatButtonVisibility();

        IntentFilter intentFilter = new IntentFilter("CONTACTS_UPDATED");
        registerReceiver(contactUpdateReceiver, intentFilter);
    }
    @Override
    protected void onStart() {
        super.onStart();
        AppOnlineStatusManager.OnlineStatus.setActive(true);

    }
    @Override
    public void onStop() {
        AppOnlineStatusManager.OnlineStatus.setActive(false);
        super.onStop();
    }
    private BroadcastReceiver contactUpdateReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            getchatListFromDatabase(getApplicationContext());
        }
    };

    void setFloatButtonVisibility()
    {
        if(chatforwardActivity) {
            // getSupportActionBar().setDisplayShowTitleEnabled(true);
           // toolbarStatus.setVisibility(View.VISIBLE);
            if(adap.selectedUsers.size()>0){
                // getSupportActionBar().setTitle(adap.selectedUsers.size()+" selected");
                getSupportActionBar().setTitle(adap.selectedUsers.size()+" selected");
                if(contactFloatButton.getVisibility()==View.GONE)
                    ViewFunctions.independantAnimateIn(contactFloatButton,getApplicationContext());
            }
            else{
                //getSupportActionBar().setTitle("Select people");
                getSupportActionBar().setTitle(R.string.select_chats);
                if(contactFloatButton.getVisibility()==View.VISIBLE)
                    ViewFunctions.independantAnimateOut(contactFloatButton,getApplicationContext());
            }


        }
        else {
            if(contactFloatButton.getVisibility()==View.GONE)
                ViewFunctions.independantAnimateIn(contactFloatButton,getApplicationContext());
        }
    }

    public boolean checkForHeader(int pos,boolean reverseLayout)
    {
        Log.e("GGG","header "+reverseLayout+" /pos: "+pos+" = "+chatUserStart+" = "+contactUserStart);
        return (pos==chatUserStart||pos==contactUserStart);
    }
    public void bindHeader(RecyclerView.ViewHolder  viewHolder,int pos)
    {
        TextView txt=viewHolder.itemView.findViewById(R.id.notificationExtraText);
        if(pos==chatUserStart)
            txt.setText(R.string.recents_chats);
        else if(pos==contactUserStart)
            txt.setText(R.string.other_users_and_groups);
    }
    int chatUserStart=-1;
    int contactUserStart=-1;

    void getchatListFromDatabase(final Context context)
    {
        finaldata.clear();
        SQLiteDatabase db = DatabaseManager.getDatabase(context).getReadableDatabase();

        Cursor cursor = null;

        cursor= db.rawQuery(
                "SELECT *,"+DatabaseAccesser.chat_table+".rowid AS rowID"+" FROM "+DatabaseAccesser.chat_table +
                        " INNER JOIN "+DatabaseAccesser.user_table+" ON "+
                        DatabaseAccesser.chat_table+"."+DatabaseAccesser.chat_userid+" = "+ DatabaseAccesser.user_table+"."+DatabaseAccesser.user_user_id+
                        " GROUP BY " +
                        DatabaseAccesser.chat_userid  + " ORDER BY "+DatabaseAccesser.chat_timestamp+" DESC",null);
        while (cursor.moveToNext()) {



            UserWholeData temp = new UserWholeData();

            temp.user_id = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_user_id));
            temp.type = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_user_type));
            temp.user_name = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_name));
            //Log.e("GGG","username is "+temp.user_name);
            temp.status = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_status));
            temp.img_url = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_localUrl));
            temp.priv_profile_picture = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_priv_profile_picture_status));
            temp.remoteContact = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_remote_contact));
            temp.followType = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_user_follow_type));

            temp.remoteBlock = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_remote_block));
            temp.blockType = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_user_block_type));

            finaldata.add(temp);
            if(chatUserStart==-1)
                chatUserStart=finaldata.size()-1;

        }
        cursor.close();

        Thread cc=new Thread(new Runnable() {
            @Override
            public void run() {
                ArrayList<UserWholeData> allUsers=UserManager.getUsersList(context,null);
                for(UserWholeData temp:allUsers) {
                    if((temp.type==2 || temp.type==15||temp.type==16)&&!UserManager.checkIfUserIdInList(finaldata,temp.user_id)) {
                        finaldata.add(temp);
                        if(contactUserStart==-1)
                            contactUserStart=finaldata.size()-1;
                    }
                }
                adap.notifyDataSetChanged();
            }
        });
        cc.start();
        adap.notifyDataSetChanged();
    }

    void getuserListFromDB(Context context)
    {

        SQLiteDatabase db = DatabaseManager.getDatabase(context).getReadableDatabase();

        Cursor cursor = null;


            cursor = db.query(DatabaseAccesser.user_table,
                    null,
                    DatabaseAccesser.user_user_type + " BETWEEN ? AND ?",
                    new String[]{"1", "4"},
                    null, null, DatabaseAccesser.user_name, null);


        while (cursor.moveToNext()) {


            UserWholeData temp = new UserWholeData();

            temp.user_id = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_user_id));
            temp.type = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_user_type));
            temp.user_name = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_name));
            temp.status = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_status));
            temp.img_url = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_localUrl));
            temp.priv_profile_picture = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_priv_profile_picture_status));
            temp.remoteContact = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_remote_contact));
            temp.followType = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_user_follow_type));


            if(!UserManager.checkIfUserIdInList(finaldata,temp.user_id))
            finaldata.add(temp);

        }
        cursor.close();

        adap.notifyDataSetChanged();
    }











    String phoneNumberValidation(String num)
    {
        return num.replaceAll("[()\\s-]+","");
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
//        newPostRef.removeEventListener(childEventListener);
    }


    void upadte_content() {


         newPostRef=fDatabase.child("user_profile");

         childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                final ContactListData post = dataSnapshot.getValue(ContactListData.class);
                SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();

                Cursor cursor = db.rawQuery(" select " + DatabaseAccesser.user_user_id + " from " + DatabaseAccesser.user_table + " where " +
                        DatabaseAccesser.user_user_id + " = ? ", new String[]{post.user_id + ""});

                if (cursor.getCount() == 0) {

                    StorageReference httpsReference = FirebaseStorage.getInstance().getReferenceFromUrl(post.img_url);
                    httpsReference.getBytes(1024 * 1024).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                        @Override
                        public void onSuccess(byte[] bytes) {
                            // Data for "images/island.jpg" is returns, use this as needed

                            //String output = ;

                            try {

                                File file = new File(Environment.getExternalStorageDirectory().getPath(), "Around Files/Profile Pictures");
                                if (!file.exists()) {
                                    file.mkdirs();
                                }
                                //File tempFile = new File(file.getAbsolutePath(),"contact.vcf");

                                String name = post.user_name + ".png";
                                FileOutputStream fos = new FileOutputStream(new File(file.getAbsolutePath(), name));
                                fos.write(bytes);
                                fos.close();

                                //Toast.makeText(getActivity(),  , Toast.LENGTH_SHORT).show();

                                String globalUrl= post.img_url;
                                post.img_url = file.toURI().toString() + name;


                                finaldata.add(0,(UserWholeData) post);
                                adap.notifyItemInserted(0);

                                // adap.notifyItemChanged(finaldata.size() - 1);
                                // rView.scrollToPosition(finaldata.size() - 1);

                                saveContactToDatabase((UserWholeData) post,globalUrl,true);


                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            // Handle any errors
                        }
                    });
                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };


//                      Toast.makeText(getActivity(), "hurray", Toast.LENGTH_SHORT).show();
                // ...

      //  newPostRef.addChildEventListener(childEventListener);

    }


    /*void saveContactToDatabase(ContactListData post,String globalUrl)
    {
        SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();


        ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.user_user_id, post.user_id);
        values.put(DatabaseAccesser.user_name, post.user_name);
        values.put(DatabaseAccesser.user_status, post.status);
        if(post.user_id.equals(userdetail.getUid()))
        {
            values.put(DatabaseAccesser.user_user_type, 0);
        }
        else
        {
            values.put(DatabaseAccesser.user_user_type, post.numberType);
        }
        values.put(DatabaseAccesser.user_globalUrl,globalUrl);
        values.put(DatabaseAccesser.user_localUrl, post.img_url);



        long newRowId = db.insert(DatabaseAccesser.user_table, null, values);
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.chat_forward_activity, menu);
        MenuItem sItem=menu.findItem(R.id.action_search_alluser);
        SearchView searchView =
                (SearchView) sItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // return false;
               // Toast.makeText(AllUsersList.this, "Submit- "+query, Toast.LENGTH_SHORT).show();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //Toast.makeText(AllUsersList.this, "Change- "+newText, Toast.LENGTH_SHORT).show();
                searchOnList(newText);
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);

    }

    void searchOnList(String searchTerm)
    {
        if(searchTerm.equals(""))
        {
            adap.updateDataList(finaldata);
            adap.notifyDataSetChanged();
        return;
        }

        ArrayList<UserWholeData> temp=new ArrayList<>();
        for(int i=finaldata.size()-1;i>=0;i--)
        {
            if(finaldata.get(i).user_name.toLowerCase().contains(searchTerm.toLowerCase()))
            {
                temp.add(finaldata.get(i));
            }
            else
            {

            }
        }
        adap.updateDataList(temp);
        adap.notifyDataSetChanged();
    }
    @Override
    protected void onNewIntent(Intent intent) {

        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            //Toast.makeText(this, query +" -dont know", Toast.LENGTH_SHORT).show();
            //use the query to search your data somehow
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_new_group)
        {
            Intent intent=new Intent(this,CreateNewGroup.class);
            intent.putExtra("type",15);
            startActivity(intent);
        }
        else if(id==R.id.action_refresh)
        {
            if(NetworkUtils.networkIsAvailable(getApplicationContext())) {
                Toast.makeText(this, R.string.refreshing, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent("GET_CONTACTS_UPDATED");
                sendBroadcast(intent);
            }
            else
            {
                Toast.makeText(this, R.string.no_network_connection, Toast.LENGTH_SHORT).show();
            }
        }

        return super.onOptionsItemSelected(item);
    }
}
