package com.hash.around;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.hash.around.OtherClasses.DataTypes.ContactListData;
import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.OtherClasses.DataTypes.UserPrivacyData;
import com.hash.around.OtherClasses.Interfaces.FileDownloadListner;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.OtherClasses.Interfaces.LocationReceiver;
import com.hash.around.OtherClasses.Interfaces.ProcessCompletedListner;
import com.hash.around.Utils.LocationUtils;
import com.hash.around.Utils.MediaManager;
import com.hash.around.Utils.NetworkUtils;
import com.hash.around.Utils.PermissionManager;
import com.hash.around.Views.EmojiIconImage;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.vanniktech.emoji.EmojiEditText;
import com.vanniktech.emoji.EmojiManager;
import com.vanniktech.emoji.ios.IosEmojiProvider;


import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import static com.hash.around.MainServiceThread.fetchUserDetailsbyIDOrPhone;
import static com.hash.around.MainServiceThread.updateOrInsertUserIfNone;


public class SetProfile extends AppCompatActivity {
    UserPrivacyData privacyInfo=null;
    Toolbar toolbar;
    TextView locationtxt;

    EmojiEditText name;
    EmojiIconImage emoticon;
    ImageButton locationCheck;
    ProgressBar profileloading;

    CircularImageView circleProfileImage;
    ImageButton chooseButton;
    Button completeButton;
    boolean locationError;
    Uri directory = null;
    boolean locationFecthSuccess=false;
    Uri outputUri;
    //LocationManager locationManager;
    LocationListener listner;
    private StorageReference mStorageRef;
    FirebaseUser userdetail;
    DatabaseReference databaseRef;
    public SharedPreferences prefs;
    public SharedPreferences.Editor editor;
    double latitute;
    double longitude;
    String locationName;
    StorageReference riversRef;
    String provider;
    private LocationCallback mLocationCallback;
    private FusedLocationProviderClient mFusedLocationClient;
    LocationRequest mLocationRequest;


    void savePostToDatabase(ContactListData post,String globalUrl)
    {
        SQLiteDatabase db = tempDb.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.user_user_id, post.user_id);
        values.put(DatabaseAccesser.user_name, post.user_name);

        values.put(DatabaseAccesser.user_status, post.status);
        values.put(DatabaseAccesser.user_status_timestamp, post.status_timestamp);
       // values.put(DatabaseAccesser.user_music_globalUrl, post.music_url);
       // values.put(DatabaseAccesser.user_status, post.music_timestamp);

        values.put(DatabaseAccesser.user_number, post.number);
        if(post.user_id.equals(userdetail.getUid()))
        {
            values.put(DatabaseAccesser.user_user_type, 0);
        }
        else
        {
            values.put(DatabaseAccesser.user_user_type, post.type);
        }
        values.put(DatabaseAccesser.user_globalUrl,globalUrl);
        values.put(DatabaseAccesser.user_localUrl, post.img_url);


        long newRowId = db.insert(DatabaseAccesser.user_table, null, values);
        db.close();

    }
    public DatabaseAccesser tempDb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        EmojiManager.install(new IosEmojiProvider());
        setContentView(R.layout.set_profile_activity);

        tempDb = new DatabaseAccesser(getApplicationContext());

        locationError = true;
        toolbar = (Toolbar) findViewById(R.id.commontoolbar);
        prefs = getSharedPreferences("user_preferences", MODE_PRIVATE);
        editor = prefs.edit();

        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(getString(R.string.profile_setup));
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        locationtxt = (TextView) findViewById(R.id.locationEdit);
        circleProfileImage = (CircularImageView) findViewById(R.id.circular_image);
        //root = (RelativeLayout) findViewById(R.id.rootrelative_profilesetup);
        emoticon = findViewById(R.id.emotimg);
        name = (EmojiEditText) findViewById(R.id.profilenametext);
        emoticon.setTargetText(name,findViewById(android.R.id.content));
        /*actions = new EmojIconActions(getApplicationContext(), root, name, emoticon);
        actions.ShowEmojIcon();*/
        //emoticon.setImageResource(R.drawable.ic_emoticon_semiblack);
        userdetail = FirebaseAuth.getInstance().getCurrentUser();
        databaseRef = FirebaseDatabase.getInstance().getReference();
        locationCheck=findViewById(R.id.locationCheck);
        profileloading=findViewById(R.id.profileloading);
        Log.e("GGG","intentintent REAL START");
        if (userdetail == null)
        {
            Intent intent = new Intent(this, RegisterActivity.class);
            finish();
            Log.e("GGG","intentintent 1 finish");
            startActivity(intent);
        } else {
            Toast.makeText(this, userdetail.getUid(), Toast.LENGTH_SHORT).show();
        }
        mStorageRef = FirebaseStorage.getInstance().getReference();
        riversRef = mStorageRef.child("profile_images").child(randomStringGenerator() + ".png");
        chooseButton = (ImageButton) findViewById(R.id.imageButton1);

        completeButton = (Button) findViewById(R.id.completeButton);
        completeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(!verifyData())
                    return;


                if (directory != null) {
                    //Log.e("GGG","directory is "+directory);
                    riversRef.putFile(directory)
                            .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    // Get a URL to the uploaded content
                                    final Uri downloadUrl = taskSnapshot.getDownloadUrl();
                                    uploadDataToServer(downloadUrl.toString());
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    Toast.makeText(SetProfile.this, exception.getMessage() + exception.toString(), Toast.LENGTH_LONG).show();

                                    AroundAlertBoxManager.cancelProgress();
                                    // ...
                                }
                            });


                } else {
                    uploadDataToServer("null");
                }

            }
        });

        chooseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(PermissionManager.checkExternalStoragePermission(SetProfile.this))
                pickFromGallery();

            }
        });

        /*actions.setKeyboardListener(new EmojIconActions.KeyboardListener() {
            @Override
            public void onKeyboardOpen() {
               /// Log.e("Keyboard","open");
            }

            @Override
            public void onKeyboardClose() {
               // Log.e("Keyboard","close");
            }
        });*/

       // locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
       // final Criteria criteria = new Criteria();
       // provider = locationManager.getBestProvider(criteria, false);

        //Location location = locationManager.getLastKnownLocation(provider);

        //Toast.makeText(SetProfile.this, "Default :"+location.getLatitude() + "/" + location.getLongitude(), Toast.LENGTH_SHORT).show();

        /*******Playyyyyyyservices*/

       /* mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            Toast.makeText(SetProfile.this, "Default :"+location.getLatitude() + "/" + location.getLongitude(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });*/


        /*******Playyyyyyyservices*/
        listner = new LocationListener() {
            public void onStatusChanged(String provider, int status, Bundle extras) {
                Toast.makeText(SetProfile.this, "Enthokkeyo", Toast.LENGTH_SHORT).show();
                // called when the location provider status changes. Possible status: OUT_OF_SERVICE, TEMPORARILY_UNAVAILABLE or AVAILABLE.
            }

            public void onProviderEnabled(String provider) {
                // called when the location provider is enabled by the user

                //Toast.makeText(SetProfile.this, "Enabled", Toast.LENGTH_SHORT).show();
            }

            public void onProviderDisabled(String provider) {
                //Toast.makeText(SetProfile.this, "Disabled", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
                // called when the location provider is disabled by the user. If it is already disabled, it's called immediately after requestLocationUpdates
            }

            public double round(double value, int places) {
                if (places < 0) throw new IllegalArgumentException();

                long factor = (long) Math.pow(10, places);
                value = value * factor;
                long tmp = Math.round(value);
                return (double) tmp / factor;
            }

            public void onLocationChanged(Location location) {
                latitute = location.getLatitude();
                longitude = location.getLongitude();

                editor.putString("location_latitude", String.valueOf(latitute));
                editor.putString("location_longitude", String.valueOf(longitude));

                Toast.makeText(SetProfile.this, latitute + "/" + longitude, Toast.LENGTH_SHORT).show();

                locationtxt.setText(latitute + "/" + longitude);

            }
        };
        locationCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationfinder(v);
            }
        });



        //locationfinder(null);

    }

    void uploadDataToServer(final String downloadUrl)
    {
        if(!NetworkUtils.networkIsAvailable(getApplicationContext()))
        {
            Toast.makeText(this, R.string.no_network_connection, Toast.LENGTH_SHORT).show();
            return;
        }
        AroundAlertBoxManager.showProgress(this,null,R.string.updating_profile,true);

        final ContactListData map = new ContactListData();
        map.user_name = name.getText().toString();
        map.user_id = userdetail.getUid();
        map.type = 1;
        map.img_url = downloadUrl;
        map.status = "null";
        map.status_timestamp=System.currentTimeMillis();
        map.location=locationName;
        map.number=userdetail.getPhoneNumber();
        map.music_url="null";
        map.music_timestamp=System.currentTimeMillis();


        //Toast.makeText(SetProfile.this, locationName, Toast.LENGTH_SHORT).show();


        databaseRef.child("user_profile").child(userdetail.getUid()).setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful())
                {

                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference("user_location");
                    GeoFire geoFire = new GeoFire(ref);
                    geoFire.setLocation(userdetail.getUid(), new GeoLocation(latitute, longitude), new GeoFire.CompletionListener() {
                        @Override
                        public void onComplete(String key, DatabaseError error) {
                            if (error != null) {
                                Toast.makeText(getApplicationContext(), "Error writing location", Toast.LENGTH_SHORT).show();
                            } else {
                                //Toast.makeText(getApplicationContext(), "Success location", Toast.LENGTH_SHORT).show();

                                if(directory==null)
                                    map.img_url="null";
                                else
                                    map.img_url=directory.toString();
                                //directory =
                                map.type=0;
                                savePostToDatabase(map,downloadUrl);
                                TabbedActivity.saveContactMusicToDatabase(map.user_id,"null", map.music_url,System.currentTimeMillis());
                                movetotabbedactivity();
                                //map.numberType=1;


                                editor.putString("location_name", locationName);
                                editor.putString("location_latitude", String.valueOf(latitute));
                                editor.putString("location_longitude", String.valueOf(longitude));
                            }
                        }
                    });


                                                /*HashMap privacymap = new HashMap();
                                                privacymap.put("profile_picture",0);
                                                privacymap.put("profile_status",0);
                                                privacymap.put("number",0);
                                                privacymap.put("activity_status",0);
                                                privacymap.put("location",1);
                                                privacymap.put("anyonefollow",0);
                                                privacymap.put("anyonemessage",0);*/


                    if(privacyInfo==null) {
                        privacyInfo = new UserPrivacyData();
                        privacyInfo.profile_picture = 0;
                        privacyInfo.profile_status = 0;
                        privacyInfo.number = 0;
                        privacyInfo.activity_status = 0;
                        privacyInfo.location = 1;
                        privacyInfo.anyonefollow = 0;
                        privacyInfo.anyonemessage = 0;
                        privacyInfo.profile_music = 0;
                    }
                    DatabaseReference databaseRef;
                    databaseRef = FirebaseDatabase.getInstance().getReference();

                    databaseRef.child("user_profile").child(userdetail.getUid()).child("user_privacy").setValue(privacyInfo).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                            }
                        }
                    });
                }
            }
        });
    }

    boolean verifyData()
    {
        if(name.getText().toString().trim().equals(""))
        {
            Toast.makeText(this, R.string.name_cannot_beempty_error, Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(locationFecthSuccess==false)
        {
            Toast.makeText(this, R.string.couldnot_identify_your_location, Toast.LENGTH_SHORT).show();
            locationfinder(null);
            return false;
        }
        //yyy
        return true;
    }

    void preFetchDataFromDatabaseIfAvailable()
    {
        Log.e("GGG","prefetch starts "+userdetail.getUid());

        if ( PermissionManager.checkExternalStoragePermission(this)){

        try {

            fetchUserDetailsbyIDOrPhone(userdetail.getUid(), true, false, new ProcessCompletedListner() {
                @Override
                public void processCompleteAction() {
                }

                @Override
                public void newUserAdded(UserWholeData data) {

                    Log.e("GGG","prefetch newuser");

                    name.setText(data.user_name);
                    privacyInfo = data.getUserPrivacyData();
                    installUserPrivacyToSettings(privacyInfo);

                    if (data.img_url.equals("null")) {
                        Log.e("GGG","prefetch nofetch");
                    }
                    else if(data.img_url.contains("http")){
                        profileloading.setVisibility(View.VISIBLE);
                        MainServiceThread.downloadFileAndReturnUriString(getApplicationContext(),data.img_url,false,2,null, new FileDownloadListner() {
                        @Override
                        public void onProgressListener(int percent) {

                        }

                        @Override
                        public void onSuccessListener(String localFilename) {
                            directory = Uri.parse(localFilename);
                            circleProfileImage.setImageURI(directory);
                            profileloading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailureListener(int percent) {

                        }

                        @Override
                        public void onPausedListener(int percent) {

                        }
                    });}

                }
            });
        }
        catch (Exception e)
        {

        }
        }
    }

    void installUserPrivacyToSettings(UserPrivacyData privacyData)
    {
        //CODE 47 create codes to save user preference from web
    }
    public void fetchAnotherName(Location location)
    {
        Address address= LocationUtils.getAddressFromLocation(this,location);
        if(address!=null){

        String tempText=address.getLocality();
        if(tempText==null)
            tempText=address.getFeatureName();
        if(tempText==null)
        {
           fetchLocation();
           return;
        }
            locationtxt.setText(tempText+", " +address.getCountryName());
            Log.e("GGG","Location name: "+address.getLocality() + ", " + address.getAddressLine(0)+", "+address.getCountryName());
            LocationUtils.setUserLocation(this,location,address);
            locationFecthSuccess=true;
        }
        else
        {
            fetchLocation();
        }

    }


    public void movetotabbedactivity() {
        AroundAlertBoxManager.cancelProgress();
        Intent intent = new Intent(this, TabbedActivity.class);
        finish();
        Log.e("GGG","intentintent 2 finish");
        startActivity(intent);


    }

    public String randomStringGenerator() {
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (
                int i = 0;
                i < 20; i++)

        {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == RESULT_OK) {

            if (data != null && data.getBooleanExtra("delete", false)) {
                // avatarBytes = null;
                circleProfileImage.setImageDrawable(getApplicationContext().getResources().getDrawable(R.drawable.default_user));
                directory=null;
            } else {

                Uri inputFile  = (data != null ? data.getData() : null);
                if (inputFile == null && photoCameraURI != null) {
                    inputFile = photoCameraURI;
                }

                Log.e("GGG","filefile is "+inputFile);

                //directory = MediaManager.getMediaOgUriFromTemp2(getApplicationContext(),inputFile);
                Log.e("GGG","filefile2 is "+directory);
            /*if(directory==null)
            {
                Log.e("GGG","filefile NOTE FOUND IN TABLE "+inputFile);
                directory= MediaManager.getMediaUriAfterCopyingtoCache(getApplicationContext(), inputFile, null);
            }*/
                //circleProfileImage.setImageURI(inputFile);

                Intent cropIntent = new Intent(this, ImageCropActivity.class);
                cropIntent.putExtra("uri", inputFile.toString());
                cropIntent.putExtra("mode", 1);
                startActivityForResult(cropIntent, 5);

                // new Crop(inputFile).output(outputFile).asSquare().start(this);
            }
        }
        else if (requestCode == 5 && resultCode == RESULT_OK) {
            try {

                directory=data.getData();

                Log.e("GGG","filefile2 after CROP is "+directory);
                //directory=Uri.parse(data.getStringExtra("uri"));


                //MediaManager.copyFileFromUri(getApplicationContext(),directory,"sdfsdfdsfdf.png");
                //Log.e("GGG","uriuri got data: "+data.getData()+" / convertes  is "+directory);
                circleProfileImage.setImageURI(directory);

            } catch (Exception e) {
                Toast.makeText(this, e.getMessage() + e.toString(), Toast.LENGTH_LONG).show();
            }

        }
        else if (requestCode == PermissionManager.LOCATION_ACCESS_PERMISSION_CODE && resultCode == RESULT_OK)
        {
            locationfinder(null);
        }
        else if (requestCode == PermissionManager.EXTERNAL_STORAGE_READ_WRITE_PERMISSION_CODE && resultCode == RESULT_OK)
        {
            if(directory==null)
            {
                preFetchDataFromDatabaseIfAvailable();
            }
        }



    }
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        if(photoCameraURI!=null)
            savedInstanceState.putString("contentUri", photoCameraURI.toString());

    }
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        String mCurrentPhotoPath=savedInstanceState.getString("contentUri");
        if(mCurrentPhotoPath!=null)
            photoCameraURI=Uri.parse(mCurrentPhotoPath);
    }
    Uri photoCameraURI;

    private void pickFromGallery() {
        if (!PermissionManager.checkExternalStoragePermission(this))
        {
            return;
        }
        boolean cameraPermission=PermissionManager.checkHasPermission(this);
        File photoFile=null;
        try {
            photoFile= MediaManager.createImageFile(getApplicationContext());
        } catch (IOException e) {
            e.printStackTrace();
        }
        photoCameraURI = FileProvider.getUriForFile(getApplicationContext(),
                "com.hash.around.fileprovider",
                photoFile);
        Intent chooserIntent = CreateNewGroup.createAvatarSelectionIntent(SetProfile.this,photoCameraURI, directory!=null, cameraPermission);
        startActivityForResult(chooserIntent, 1);


        // Crop.pickImage(SetProfile.this);

    }

    /*@Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 10:
                if (grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //locationok();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                            // locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0, listner);
                            locationfinder(null);
                        }
                    }
                }
                break;
            case 11:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //locationok();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                                checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                                checkSelfPermission(Manifest.permission.INTERNET) == PackageManager.PERMISSION_GRANTED) {


                        }
                    }
                }
                break;

        }
    }*/

    public void locationfinder(View v)
    {
       if(PermissionManager.checkLocationPermission(this))
       {
           //yyy
           Log.e("GGG","locationsloo calling with pr");
        fetchLocation();
       }
    }

    @Override
    protected void onResume() {
        super.onResume();
        preFetchDataFromDatabaseIfAvailable();
        locationfinder(null);

    }



    public void fetchLocation() {

        /*mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {


                Location location=locationResult.getLastLocation();


                mFusedLocationClient.removeLocationUpdates(mLocationCallback);

                fetchAnotherName(location);



            }
        };

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (android.support.v4.app.ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && android.support.v4.app.ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                mLocationCallback,
                null );*/
        /*TabbedActivity.fetchFusedLocation(new LocationReceiver() {
            @Override
            public void onLocationReceived(Location data) {
                //Location location=locationResult.getLastLocation();
               // Log.e("GGG","locationcheckf fused "+data.getLatitude()+" / "+data.getLongitude());

               // mFusedLocationClient.removeLocationUpdates(mLocationCallback);



            }
        },this);*/

        if (TabbedActivity.isLocationEnabled(this)) {

        TabbedActivity.fetchNativeLocation(new LocationReceiver() {
            @Override
            public void onLocationReceived(Location data) {

                fetchAnotherName(data);
            }
        },this,false);
        } else
            {
            showGPSOffAlert(this);
        }
    }



    public static void showGPSOffAlert(final Activity act) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(act);
        dialog.setTitle(R.string.enable_location)
                .setMessage(R.string.gps_location_off_details)
                .setPositiveButton(act.getString(R.string.open_settings), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        paramDialogInterface.dismiss();
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        act.startActivity(myIntent);

                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        paramDialogInterface.dismiss();
                    }
                });
            dialog.show();
    }

}