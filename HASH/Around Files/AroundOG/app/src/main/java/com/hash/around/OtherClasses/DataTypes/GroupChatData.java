package com.hash.around.OtherClasses.DataTypes;

import java.io.Serializable;

public class GroupChatData extends ChatData implements Serializable{

   public long chat_rece_time=0;
   public long chat_seen_time=0;
   public long chatRow;
   public String name;
   public int sender;
   public int msgseen=0;
   public long media_id=0;
   public long replay_id;

   public ChatReplayData chatReplay;


   public GroupChatData()
   {}
   public GroupChatData(GroupChatData dat)
   {
      super(dat);
      this.name=dat.name;
      this.sender=dat.sender;
      this.msgseen=dat.msgseen;
      this.media_id=dat.media_id;

      this.chatRow=dat.chatRow;
      this.chatReplay=dat.chatReplay;


      this.chat_rece_time=dat.chat_rece_time;
      this.chat_seen_time=dat.chat_seen_time;


      this.replay_id=dat.replay_id;


   }
   public GroupChatData(int sender, int type, String msg, String temp_url, String final_url, int msgseen, long timestamp)
   {
      super(sender, type, msg, temp_url, final_url, msgseen, timestamp);

   }

   public MediaData getMediaData()
   {
      MediaData mDat=new MediaData();
      mDat.type=this.type;
      mDat.text=this.msg;
      mDat.uri=this.final_url;
      return mDat;
   }

}
