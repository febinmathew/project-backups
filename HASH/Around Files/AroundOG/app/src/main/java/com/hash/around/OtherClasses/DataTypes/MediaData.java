package com.hash.around.OtherClasses.DataTypes;

/**
 * Created by Febin on 26-08-2017.
 */
import android.net.Uri;

import java.io.Serializable;

public class MediaData implements Serializable {

    public String folderName;
    public int ID;
    public String mimeType;
    public int type;
    public String uri;
    public String text="";
    public String location="null";
    public int privacy=0;
    public long mediaSize;

    public String globalLiteUri;
    public String globalUri;
    public String localLiteUri;
    public String localUri;

}


