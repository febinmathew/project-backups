package com.hash.around.Views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hash.around.OtherClasses.DataTypes.MediaData;
import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.hash.around.OtherClasses.Interfaces.TimerListner;
import com.hash.around.R;
import com.hash.around.TabFragments.MapsFragment;
import com.hash.around.UploadActivity;
import com.hash.around.Utils.LocationUtils;
import com.hash.around.Utils.StringUtils;
import com.hash.around.Views.Medias.AroundCircularImageView;
import com.hash.around.Views.Medias.ChatReplayLayout;
import com.vanniktech.emoji.EmojiEditText;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import static com.hash.around.Utils.AroundStaticInfo.getDefaultAudioDirectory;
import static com.hash.around.Utils.AroundStaticInfo.vibrateInMilliSeconds;


public class AroundFeedInputSection extends LinearLayout {

  public AroundFeedInputSection(Context context) {
    this(context, null);
  }


  public AroundFeedInputSection(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  TextView textAmount,feedLocation;
  LinearLayout textFeedStatus;
  ImageButton expandButton;
  AroundCircularImageView feedUserImage;
  ImageButton sendMsgButton,recordMsgButton;
  AroundToggleViewFrameLayout chatToggleButton;
  ImageView attachments;
  EmojiIconImage emoticon;
  EmojiEditText chat_message;
  LinearLayout chatSection,audioRecordingStatus,chatExtraSection;

  RecordTimer recordTimer;

  String fileName;
  MediaRecorder mMediaRecorder;

  Context mContext;

  //ImageView extraButton,extraSectionClose;//,reminderSetup;
  com.github.clans.fab.FloatingActionButton upload_text,upload_gif,upload_audio,upload_image,upload_video;

  public ChatReplayLayout replaySection;

  public AroundFeedInputSection(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);

    mContext=context;
    if (attrs != null) {
     /* TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.SquareFrameLayout, 0, 0);

      typedArray.recycle();*/
    }
    inflate(context, R.layout.around_feed_inputarea, this);
    upload_text=findViewById(R.id.upload_text);
    upload_gif=findViewById(R.id.upload_gif);
    upload_audio=findViewById(R.id.upload_audio);
    upload_image=findViewById(R.id.upload_image);
    upload_video=findViewById(R.id.upload_video);
    expandButton=findViewById(R.id.expandButton);
    chatToggleButton=(AroundToggleViewFrameLayout)findViewById(R.id.chatToggleButton);
    audioRecordingStatus=(LinearLayout)findViewById(R.id.audioRecordingStatus);
    emoticon = findViewById(R.id.emotimg);
    attachments = (ImageView) findViewById(R.id.attachments);
    //attachments.setVisibility(GONE);
    //emoticon.setImageResource(R.drawable.ic_emoticon_semiblack);
    sendMsgButton=(ImageButton)findViewById(R.id.sendMsgButton) ;
    recordMsgButton=(ImageButton)findViewById(R.id.recordMsgButton) ;
    chat_message=(EmojiEditText)findViewById(R.id.chat_message);
    //extraButton=findViewById(R.id.addExtras);
    chatExtraSection=findViewById(R.id.chatExtraSection);
    chatSection=findViewById(R.id.chatSection);
    // extraSectionClose=findViewById(R.id.extraSectionClose);
    replaySection=findViewById(R.id.replaySection);
    feedUserImage=findViewById(R.id.feedUserImage);
    textFeedStatus=findViewById(R.id.textFeedStatus);
    textAmount=findViewById(R.id.textAmount);
    feedLocation=findViewById(R.id.feedLocation);
    //reminderSetup=findViewById(R.id.reminderSetup);
    updateToggleButton();

    expandButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        //UploadActivity.receiverAcitvity=2;
       // uploadIntent.putExtra("forChat", 1);
        Intent uploadIntent = new Intent(getContext().getApplicationContext(), UploadActivity.class);
        uploadIntent.putExtra("textSequence",chat_message.getText().toString());
        uploadIntent.putExtra("forChat", 2);
        getContext().startActivity(uploadIntent);
      }
    });
  }

  public ImageView getAtachmentView()
  {
    return attachments;
  }
  public EmojiEditText getEditTextView()
  {
    return chat_message;
  }
  public ImageButton getSendMsgButton()
  {
    return sendMsgButton;
  }

  @Override
  protected void onFinishInflate() {
    super.onFinishInflate();
    initViews();
  }

  public UserWholeData userData;


  public void setUserData(UserWholeData userDataArg)
  {
    this.userData=userDataArg;
    if(userData.img_url!=null && !userData.equals("null"))
      feedUserImage.setImageURI(Uri.parse(userData.img_url));
    else
      feedUserImage.setImageResource(R.drawable.default_user);
  }
/*public void setReplayLayoutData(String heading,String description,Uri uri)
{
  if(uri==null)
     replaySection.hideImageThumbnail();
  else
    replaySection.setImageUri(uri);

  replaySection.setHeadingAndDescription(heading,description);
  replaySection.show();
}*/
private MapsFragment.OnTouchListener touchListener=null;
  public void setListener(MapsFragment.OnTouchListener listener) {
    touchListener = listener;
  }

  public void initViews()
  {
    //userData=ChatWindow.instance.userData;
    final View root=getRootView(); //changes here final View root=this.findViewById(android.R.id.content).getRootView();
    /*final EmojiPopup emojiPopup = EmojiPopup.Builder.fromRootView(root).setOnEmojiPopupDismissListener(new OnEmojiPopupDismissListener() {
      @Override
      public void onEmojiPopupDismiss() {
        emoticon.setImageResource(R.drawable.ic_emoticon_semiblack);
      }
    }).build(chat_message);*/

    emoticon.setTargetText(chat_message,root);

    /*sendMsgButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        for(int i=0;i<finalData.size();i++)
        {
          finalData.get(i).location=locationName;
          finalData.get(i).privacy=visibilityMode;
        }
        TabFragment2.latitude=latitute;
        TabFragment2.longitude=longitude;

        Intent broadcast = new Intent("around.NEWFEED");
        broadcast.putExtra("mediaarray", finalData);
        sendBroadcast(broadcast);

        Intent intent3 = new Intent(this, TabbedActivity.class);
        intent3.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP );
        startActivity(intent3);
      }
    });*/

    chat_message.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

        /*if(s.length() == 0) {
          chat_message.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        }
        else
        {//yyy
          chat_message.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        }*/
        //chat_message.postDelayed(ChatWindow.this::updateToogleButton, 50);
        updateToggleButton();
        StringUtils.makeHashTagTextForEditText(getContext(),chat_message.getText(),false);
      }

      @Override
      public void afterTextChanged(Editable s) {

      }
    });

    /*emoticon.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        // emoticon.setImageResource(R.drawable.ic_keyboard_semiblack);
        Log.e("MSGG","show status : "+emojiPopup.isShowing());

        if(emojiPopup.isShowing())
        {
          emoticon.setImageResource(R.drawable.ic_emoticon_semiblack);
          Log.e("MSGG","chnaged emoji: "+emojiPopup.isShowing());
        }
        else {
          emoticon.setImageResource(R.drawable.ic_keyboard_semiblack);
          Log.e("MSGG","chnaged keyboard: "+emojiPopup.isShowing());
        }

        emojiPopup.toggle(); // Toggles visibility of the Popup.
        // emojiPopup.dismiss(); // Dismisses the Popup.

      }
    });*/

    /*attachments.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        Log.e("MGG","extraButton");
        if(chatExtraSection.getVisibility()==GONE)
          showExtraWindow();
        else
          hideExtraWindow();

      }
    });*/




    recordTimer=new RecordTimer((TextView) findViewById(R.id.record_time));
    chatToggleButton.setListner(new AroundToggleViewFrameLayout.AudioRecordistener() {
      @Override
      public void onRecordPressed(float x) {
        if(touchListener!=null)
          touchListener.onTouch();

        vibrateInMilliSeconds(mContext,40);

        //if(requestPermissionsOneByOne((Activity)getContext(),new String[]{Manifest.permission.RECORD_AUDIO},15)) {
        //((Activity)getContext()).getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        updateRecordingstatus(true);
        recordTimer.display();
        startRecording();

          /*if (getContext() instanceof Activity) {
           // Window window = ;*/

        //}
      }



      @Override
      public void onRecordReleased(float x) {
        stopRecording();
        long timelapse=recordTimer.hide();
        Log.e("MGG","timelpase "+timelapse);
        updateRecordingstatus(false);

        ((Activity)getContext()).getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if(timelapse>5000) {

          Intent broadcast = new Intent("around.NEWFEED");
          ArrayList<MediaData> finalData = new ArrayList<>();
          MediaData dat = new MediaData();
          dat.type = 2;
          dat.uri = Uri.fromFile(new File(fileName)).toString();
          finalData.add(dat);
          broadcast.putExtra("mediaarray", finalData);
          getContext().sendBroadcast(broadcast);

          /*Intent broadcast = new Intent("around.NEWCHAT");
          ArrayList<MediaData> finalData = new ArrayList<>();
          MediaData dat = new MediaData();
          dat.type = 2;
          dat.uri = new File(fileName).toURI().toString();
          finalData.add(dat);
          broadcast.putExtra("mediaarray", finalData);
          v.sendBroadcast(broadcast);*/
        }
        else
        {
          if(fileName!=null)
          {
            File file=new File(fileName);
            file.delete();
          }
        }
        fileName=null;
      }

      @Override
      public void onRecordCanceled(float x) {
        Log.e("MGG","Vibrate 40");
        vibrateInMilliSeconds(getContext(),50);
        stopRecording();
        updateRecordingstatus(false);
        ((Activity)getContext()).getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if(fileName!=null)
        {
          File file=new File(fileName);
          file.delete();
        }
        //lastRecord=null;
        fileName=null;
      }


      private void startRecording() {
        fileName = getDefaultAudioDirectory(getContext());
        //lastRecord=randomStringGenerator();
        fileName += "/"+ StringUtils.randomStringGenerator()+".mp3";
        Log.e("MGG","file ocation "+fileName);

        mMediaRecorder=new MediaRecorder();
        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mMediaRecorder.setOutputFile(fileName);
        mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);

        try {
          mMediaRecorder.prepare();
        } catch (IOException e) {
          //Log.e(LOG_TAG, "prepare() failed");
        }

        mMediaRecorder.start();

      }

      void stopRecording()
      {
        if(mMediaRecorder!=null) {
          try {
            mMediaRecorder.stop();
            mMediaRecorder.release();
          }
          catch (Exception s)
          {          }
          finally {
            mMediaRecorder = null;
          }

        }
      }

      @Override
      public void onRecordMoved(float x, float absoluteX) {

      }

      @Override
      public void onRecordPermissionRequired() {

      }
    });
    chatToggleButton.setUpRecordListner();


    setLocationToFeedLocation();
  }

  void setLocationToFeedLocation()
  {
    Location dat=LocationUtils.getUserLocation(getContext());
    feedLocation.setText(dat.getProvider());
  }

  void showExtraWindow()
  {
    ViewFunctions.independantAnimateIn(chatExtraSection,getContext());
    ViewFunctions.animateButtonBurstIn(upload_text,50,true);
    ViewFunctions.animateButtonBurstIn(upload_image,100,true);
    ViewFunctions.animateButtonBurstIn(upload_audio,150,true);
    ViewFunctions.animateButtonBurstIn(upload_video,200,true);
    ViewFunctions.animateButtonBurstIn(upload_gif,250,true);

  }
  void hideExtraWindow()
  {
    ViewFunctions.independantAnimateOut(chatExtraSection,getContext());
  }

  void updateRecordingstatus(boolean isRecording)
  {
    if(isRecording)
    {
      Log.e("MGG","startingggggg NOW");
      ViewFunctions.fadeIn(audioRecordingStatus);
      ViewFunctions.fadeOut(chatSection);
      //ViewFunctions.scaleView(chatToggleButton,1,2);

    }
    else
    {    recordTimer.hide();
      //recordMsgButton.setOnTouchListener(null);
      Log.e("MGG","stoppin FNOW");
      ViewFunctions.fadeOut(audioRecordingStatus);
      ViewFunctions.fadeIn(chatSection);
    }
  }
  void updateToggleButton()
  {
    //xxx
    String chatText=chat_message.getText().toString();
    if(chatText.trim().length()==0)
    {
      chatToggleButton.display(recordMsgButton);
      Log.e("GGG","maxline1 feed");
      chat_message.setMaxLines(1);
      expandButton.setVisibility(GONE);
      attachments.setVisibility(VISIBLE);
      textFeedStatus.setVisibility(GONE);
      //ViewFunctions.animateViewScale(feedUserImage,1,null);
      /*if(feedUserImage.getScaleX()<1) {
        Animation a = new Animation() {

          @Override
          protected void applyTransformation(float interpolatedTime, Transformation t) {
            LayoutParams params = (LayoutParams) feedUserImage.getLayoutParams();
            params.rightMargin = params.rightMargin + (int) (3f * interpolatedTime);
            params.leftMargin = params.leftMargin + (int) (3f * interpolatedTime);
            feedUserImage.setLayoutParams(params);
          }
        };
        a.setDuration(500); // in ms
        feedUserImage.startAnimation(a);
      }*/

    }
    else
    {
      Log.e("GGG","maxline2 feed");
      chatToggleButton.display(sendMsgButton);
      //chat_message.setSingleLine(false);
      chat_message.setMaxLines(5);
      expandButton.setVisibility(VISIBLE);
      attachments.setVisibility(GONE);
      textFeedStatus.setVisibility(VISIBLE);
      textAmount.setText(chatText.length()+"");

      if(chatExtraSection.getVisibility()==VISIBLE)
      {hideExtraWindow();
      setLocationToFeedLocation();}
      /*if(feedUserImage.getScaleX()>=1) {
        Animation a = new Animation() {

          @Override
          protected void applyTransformation(float interpolatedTime, Transformation t) {
            LayoutParams params = (LayoutParams) feedUserImage.getLayoutParams();
            params.rightMargin = params.rightMargin - (int) (3f * interpolatedTime);
            params.leftMargin = params.leftMargin - (int) (3f * interpolatedTime);
            feedUserImage.setLayoutParams(params);
          }
        };
        a.setDuration(500); // in ms
        feedUserImage.startAnimation(a);
      }*/
     // ViewFunctions.animateViewScale(feedUserImage,0.5f,null);
      //chat_message.setEllipsize(null);
      //chat_message.lin
    }
  }

  public static class RecordTimer implements Runnable {

    private TextView recordTimeView=null;
    private final AtomicLong startTime = new AtomicLong(0);
    String combiner;
    TimerListner listner;
    Long limit=null,intervel=null;
    boolean stop=false;
    boolean reverse=false;

    public RecordTimer(TextView recordTimeView) {
      this.recordTimeView = recordTimeView;
    }
    public RecordTimer(TextView recordTimeView,String combineText) {
      this.recordTimeView = recordTimeView;
      this.combiner=combineText;
    }
    public RecordTimer() {
    }

    public void display() {
      stop=false;
      this.startTime.set(System.currentTimeMillis());
      if(recordTimeView!=null)
        this.recordTimeView.setText(DateUtils.formatElapsedTime(0));

      //.runOnMainDelayed(this, TimeUnit.SECONDS.toMillis(1));
      new Handler(Looper.getMainLooper()).postDelayed(this, 5);
    }

    public long hide() {
      long elapsedtime = System.currentTimeMillis() - startTime.get();
      this.startTime.set(0);
      stop=true;
      return elapsedtime;
    }

    public void setTimerListner(@Nullable Long limit, @Nullable Long intervel, TimerListner listner)
    {
      this.listner=listner;
      this.limit=limit;
    }

    public void setReverse(boolean reverse)
    {
      this.reverse=reverse;
    }
    void setCombiber(String comb)
    {
      this.combiner=comb;
    }

    String getformatedStringOutput()
    {
      String time;
      if(reverse)
        time=DateUtils.formatElapsedTime(TimeUnit.MILLISECONDS.toSeconds(limit-getElapseTime()));
      else
        time=DateUtils.formatElapsedTime(TimeUnit.MILLISECONDS.toSeconds(getElapseTime()));

      if(combiner!=null)
        return String.format(combiner,time);
      else
        return time;
    }

    long getElapseTime()
    {
      return System.currentTimeMillis() - startTime.get();
    }

    long getRemainningTime()
    {
      long localStartTime = startTime.get();
      if(reverse)
        return limit-(System.currentTimeMillis() - localStartTime);
      else
        return System.currentTimeMillis() - localStartTime;
    }

    @Override
    public void run() {
      long localStartTime = startTime.get();
      if (localStartTime > 0) {
        long elapsedTime =System.currentTimeMillis() - localStartTime;

        if(recordTimeView!=null)
          recordTimeView.setText(getformatedStringOutput());
        if(listner!=null)
          listner.onSpecifiedIntervel(getRemainningTime(),getformatedStringOutput());
        if(limit!=null)
        {
          if(elapsedTime>limit)
          {
            if(listner!=null)
            {
              listner.onTimerEnd(getRemainningTime(),getformatedStringOutput());
            }
            stop=true;
          }
        }
        if(!stop)
          new Handler(Looper.getMainLooper()).postDelayed(this, 5);
      }
    }
  }

}
