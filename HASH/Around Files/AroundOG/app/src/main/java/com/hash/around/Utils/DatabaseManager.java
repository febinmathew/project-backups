package com.hash.around.Utils;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.hash.around.MainServiceThread;
import com.hash.around.OtherClasses.DataTypes.ChatListData;
import com.hash.around.OtherClasses.DataTypes.ChatReplayData;
import com.hash.around.OtherClasses.DataTypes.GroupChatData;
import com.hash.around.OtherClasses.DataTypes.NotificationData;
import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.TabbedActivity;

/**
 * Created by Febin on 6/13/2018.
 */

/*
* fetures
* save chat to database
* update the status of th chat
* */
public class DatabaseManager {

    public  static DatabaseAccesser dbLink;

    public static DatabaseAccesser getDatabase()
    {
        if(dbLink==null)
        {
            dbLink=TabbedActivity.db;
            //if(dbLink==null)
                //dbLink=new DatabaseAccesser();
        }
        TabbedActivity.db=dbLink;
        return dbLink;
    }
    public static DatabaseAccesser getDatabase(Context context)
    {
        if(dbLink==null)
        {
            dbLink=new DatabaseAccesser(context);
        }
        return dbLink;
    }

    //to be removed
    public static long  saveGroupChatToDatabaseXX(final GroupChatData post, String groupID, long mediaposition)
    {
        SQLiteDatabase db =getDatabase().getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.chat_userid, post.userid);
        values.put(DatabaseAccesser.chat_sender, post.sender);
        values.put(DatabaseAccesser.chat_type, post.type);
        values.put(DatabaseAccesser.chat_timestamp, System.currentTimeMillis());
        values.put(DatabaseAccesser.chat_text, post.msg);
        //values.put(DatabaseAccesser.chat_temp_url, post.temp_url);
        values.put(DatabaseAccesser.chat_col_media_id, mediaposition);
        values.put(DatabaseAccesser.chat_groupid, groupID);

        final long newRowId = db.insert(DatabaseAccesser.chat_table, null, values);

        ContentValues values2 = new ContentValues();

        values2.put(DatabaseAccesser.chatList_lastmsg, post.msg);
        values2.put(DatabaseAccesser.chatList_lastmsg_type, post.type);
        values2.put(DatabaseAccesser.chatList_lastmsgbyme, 0);
        values2.put(DatabaseAccesser.chatList_msgqueue, 1);
        values2.put(DatabaseAccesser.chatList_lastmsgRowId, newRowId);
        values2.put(DatabaseAccesser.chatList_last_msgseen, post.msgseen);
        values2.put(DatabaseAccesser.chatList_timestamp, System.currentTimeMillis());

        int rowln=db.update(DatabaseAccesser.chatList_table,values2, DatabaseAccesser.chatList_userid+" =?",new String[]{groupID});

        return newRowId;

    }

    public static long saveChatToDatabase(Context context,GroupChatData post, @Nullable String groupID)
    {

        SQLiteDatabase db = getDatabase().getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.chat_sl_no, post.sl);
        values.put(DatabaseAccesser.chat_userid, post.userid);
        values.put(DatabaseAccesser.chat_sender, post.sender);
        values.put(DatabaseAccesser.chat_type, post.type);
        values.put(DatabaseAccesser.chat_timestamp, System.currentTimeMillis());
        values.put(DatabaseAccesser.chat_text, post.msg);
        values.put(DatabaseAccesser.chat_expire, post.chatExpire);
        values.put(DatabaseAccesser.chat_schedule, post.chatSchedule);
        if(post.sender==1){
        values.put(DatabaseAccesser.chat_ifseen, post.msgseen);
        values.put(DatabaseAccesser.chat_rece_timestamp, post.timestamp);
        }
        //values.put(DatabaseAccesser.chat_temp_url, post.temp_url);
        values.put(DatabaseAccesser.chat_col_media_id, post.media_id);
        values.put(DatabaseAccesser.chat_col_replay_id, post.replay_id);
        if(groupID!=null)
        {
            values.put(DatabaseAccesser.chat_groupid, groupID);}
        /*else
        {
            values.put(DatabaseAccesser.chat_groupid, "null");
        }*/


        long  newRowId = db.insert(DatabaseAccesser.chat_table, null, values);
        Log.e("GGG","groupchatCREATE at "+ newRowId+" userid "+ post.userid+" msg : "+post.msg+" grp "+groupID);
        //Toast.makeText(getApplicationContext(),newRowId+ " row inserter", Toast.LENGTH_SHORT).show();


        MainServiceThread.updateOrInsertIntoChatlist(context,post,newRowId,System.currentTimeMillis());

        /*ContentValues values2 = new ContentValues();

        values2.put(DatabaseAccesser.chatList_lastmsg, post.chatMsg);
        values2.put(DatabaseAccesser.chatList_lastmsgRowId, newRowId);
        values2.put(DatabaseAccesser.chatList_lastmsg_type, post.numberType);
        values2.put(DatabaseAccesser.chatList_lastmsgbyme, 1);
        values2.put(DatabaseAccesser.chatList_msgqueue, 0);
        values2.put(DatabaseAccesser.chatList_last_msgseen, post.msgseen);
        values2.put(DatabaseAccesser.chatList_timestamp, System.currentTimeMillis());

        int rowln=db.update(DatabaseAccesser.chatList_table,values2, DatabaseAccesser.chatList_userid+" =?",new String[]{post.userid});


        if(rowln<1)
        {


            db = getDatabase().getReadableDatabase();


            Cursor cursor = db.query(DatabaseAccesser.user_table,
                    new String[]{DatabaseAccesser.user_user_id,
                            DatabaseAccesser.user_name,
                            DatabaseAccesser.user_status,
                            DatabaseAccesser.user_localUrl,
                            DatabaseAccesser.user_globalUrl},
                    DatabaseAccesser.user_user_id + " =?",
                    new String[]{post.userid},
                    null, null, null,null);
            ChatListData temp = new ChatListData();
            while (cursor.moveToNext()) {





                temp.name = cursor.getString(
                        cursor.getColumnIndexOrThrow(DatabaseAccesser.user_name));

                temp.url = cursor.getString(
                        cursor.getColumnIndexOrThrow(DatabaseAccesser.user_localUrl));



                //Toast.makeText(getActivity(), temp.uri, Toast.LENGTH_SHORT).show();
                // long itemId =
                //  finaldata.add(temp);

            }
            cursor.close();


            ContentValues values3 = new ContentValues();
            values3.put(DatabaseAccesser.chatList_userid, post.userid);
            values3.put(DatabaseAccesser.chatList_name, temp.name);
            values3.put(DatabaseAccesser.chatList_lastmsgbyme, 1);
            values3.put(DatabaseAccesser.chatList_lastmsg, post.chatMsg);
            values3.put(DatabaseAccesser.chatList_lastmsgRowId, newRowId);
            values2.put(DatabaseAccesser.chatList_last_msgseen, post.msgseen);
            values3.put(DatabaseAccesser.chatList_lastmsg_type, post.numberType);
            values3.put(DatabaseAccesser.chatList_timestamp,System.currentTimeMillis());
            values3.put(DatabaseAccesser.chatList_msgqueue, 0);

            values3.put(DatabaseAccesser.chatList_url, temp.url);

            db = TabbedActivity.db.getWritableDatabase();

            db.insert(DatabaseAccesser.chatList_table, null, values3);

        }*/

        return newRowId;

    }
    public static GroupChatData getSingleChatFromRowID(Context context,long rowID)
    {
        Cursor cursor;
       /* cursor= getDatabase(context).getReadableDatabase().query(DatabaseAccesser.chat_table,
                null,"rowid=?",new String[]{rowID+""},null,null,null);*/

        cursor= getDatabase(context).getReadableDatabase().rawQuery(
                "SELECT *,"+DatabaseAccesser.chat_table+".rowid AS rowID FROM "+DatabaseAccesser.chat_table +
                        " INNER JOIN "+DatabaseAccesser.media_table+" ON "+
                        DatabaseAccesser.chat_table+"."+DatabaseAccesser.chat_col_media_id+" = "+ DatabaseAccesser.media_table+".rowid"+
                        " INNER JOIN "+DatabaseAccesser.replay_table+" ON "+
                        DatabaseAccesser.chat_table+"."+DatabaseAccesser.chat_col_replay_id+" = "+ DatabaseAccesser.replay_table+".rowid"+
                        " WHERE " +
                        DatabaseAccesser.chat_table +".rowid"+ " = ?", new String[]{rowID+""});
        GroupChatData temp = new GroupChatData();
        if (cursor.moveToNext()) {


            temp.chatRow=cursor.getLong(
                    cursor.getColumnIndexOrThrow("rowID"));
            temp.sl = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_sl_no));
            temp.userid = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_userid));
            temp.sender = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_sender));
            if(temp.sender==0)
                temp.sl=temp.chatRow;

            temp.type = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_type));
            temp.timestamp = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_timestamp));
            temp.msg = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_text));

            temp.msgseen = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_ifseen));

            temp.mediaSize = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.media_size));

            temp.temp_url = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.media_local_uri));

            if(temp.temp_url.equals("null"))
            {
                temp.temp_url = cursor.getString(
                        cursor.getColumnIndexOrThrow(DatabaseAccesser.media_global_uri));
            }

            temp.final_url = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.media_lite_local_uri));


            temp.chatSchedule = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_schedule));
            temp.chatExpire = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_expire));

            temp.replay_id = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chat_col_replay_id));


            if(temp.replay_id!=1) {
                temp.chatReplay = new ChatReplayData();

                temp.chatReplay.type=cursor.getInt(
                        cursor.getColumnIndexOrThrow(DatabaseAccesser.replay_type));
                temp.chatReplay.description=cursor.getString(
                        cursor.getColumnIndexOrThrow(DatabaseAccesser.replay_description));
                temp.chatReplay.userName=cursor.getString(
                        cursor.getColumnIndexOrThrow(DatabaseAccesser.replay_username));
                temp.chatReplay.userID=cursor.getString(
                        cursor.getColumnIndexOrThrow(DatabaseAccesser.replay_userid));
                temp.chatReplay.content_ID=cursor.getString(
                        cursor.getColumnIndexOrThrow(DatabaseAccesser.replay_content_id));
                temp.chatReplay.content_sl=cursor.getLong(
                        cursor.getColumnIndexOrThrow(DatabaseAccesser.replay_content_sl));
                temp.chatReplay.contentUri=cursor.getString(
                        cursor.getColumnIndexOrThrow(DatabaseAccesser.replay_content_uri));
                // Log.e("GGG","username from db is "+ temp.chatReplay.userName);
            }
            else
                temp.chatReplay=null;


        }
        cursor.close();
        return temp;
    }
    public static long removeSingleChatFromRowID(Context context,long rowID) {
        long rowsDeleted=getDatabase(context).getWritableDatabase().delete(DatabaseAccesser.chat_table,"rowid=?",new String[]{rowID+""});
        return rowsDeleted;
    }

    static public long addPendingProcess(Context context,long rowId, int type,long extraInt,String extraString)
    {
        SQLiteDatabase db= getDatabase(context).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseAccesser.pending_item_row, rowId);
        values.put(DatabaseAccesser.pending_type, type);
        values.put(DatabaseAccesser.pending_timestamp,System.currentTimeMillis());
        values.put(DatabaseAccesser.pending_long_extra,extraInt);
        values.put(DatabaseAccesser.pending_string_extra,extraString);


        long rowln2= db.insert(DatabaseAccesser.pending_table, null, values);
        return  rowln2;
        //db.update(DatabaseAccesser.chatList_table,values2,DatabaseAccesser.chatList_lastmsgRowId+" =?",new String[]{String.valueOf(rowId)});

    }
    static public int removePendingProcess(Context context,long row)
    {
        SQLiteDatabase db= getDatabase(context).getWritableDatabase();
        int rowln2= db.delete(DatabaseAccesser.pending_table, DatabaseAccesser.pending_table+".rowid =? ", new String[]{String.valueOf(row)});
        return  rowln2;
    }
    static public void updateSelfChatstatus(long rowId, int status)
    {
        SQLiteDatabase db= getDatabase().getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseAccesser.chat_ifseen, status);
        if(status==2)
            values.put(DatabaseAccesser.chat_rece_timestamp, System.currentTimeMillis());
        else if(status==3)
            values.put(DatabaseAccesser.chat_seen_timestamp, System.currentTimeMillis());

        Log.e("MGG","rowrow "+rowId);
        int rowln=db.update(DatabaseAccesser.chat_table,values,"rowid =? ",new String[]{rowId+""});

        Log.e("MGG","audio updated row is "+rowln);

        ContentValues values2 = new ContentValues();
        values2.put(DatabaseAccesser.chatList_last_msgseen, 1);
        int rowln2=db.update(DatabaseAccesser.chatList_table,values2,DatabaseAccesser.chatList_lastmsgRowId+" =?",new String[]{String.valueOf(rowId)});

    }

    static public void updateSelfFeedstatus(Context context,long rowId, int status)
    {
        SQLiteDatabase db= getDatabase(context).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseAccesser.feed_col_seen, status);
       /* if(status==2)
            values.put(DatabaseAccesser.chat_rece_timestamp, System.currentTimeMillis());
        else if(status==3)
            values.put(DatabaseAccesser.chat_seen_timestamp, System.currentTimeMillis());*/

        //Log.e("MGG","rowrow "+rowId);
        int rowln=db.update(DatabaseAccesser.feed_table,values,"rowid =? ",new String[]{rowId+""});

        //Log.e("MGG","audio updated row is "+rowln);

        //ContentValues values2 = new ContentValues();
        //values2.put(DatabaseAccesser.chatList_last_msgseen, 1);
        //int rowln2=db.update(DatabaseAccesser.chatList_table,values2,DatabaseAccesser.chatList_lastmsgRowId+" =?",new String[]{String.valueOf(rowId)});

    }

    public static synchronized long insertMediaToDatabase(Context context,String global,String local,String lite_global,String lite_local,long size)
    {
        SQLiteDatabase db = getDatabase(context).getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.media_global_uri, global);
        values.put(DatabaseAccesser.media_local_uri, local);
        values.put(DatabaseAccesser.media_lite_global_uri, lite_global);
        values.put(DatabaseAccesser.media_lite_local_uri, lite_local);
        values.put(DatabaseAccesser.media_timestamp, System.currentTimeMillis());
        values.put(DatabaseAccesser.media_size, size);

        final long newRowId = db.insert(DatabaseAccesser.media_table, null, values);

        Log.e("MSGG","count "+newRowId+" "+global+"\n"+local+"\n"+lite_global+"\n"+lite_local+"\n");
        return newRowId;


    }
    public static long insertChatReplaysToDatabase(ChatReplayData dat)
    {
        SQLiteDatabase db = getDatabase().getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.replay_type, dat.type);
        values.put(DatabaseAccesser.replay_content_id, dat.content_ID);
        values.put(DatabaseAccesser.replay_content_sl, dat.content_sl);
        values.put(DatabaseAccesser.replay_content_uri, dat.contentUri);
        values.put(DatabaseAccesser.replay_description, dat.description);
        values.put(DatabaseAccesser.replay_userid, dat.userID);
        values.put(DatabaseAccesser.replay_username, dat.userName);

        final long newRowId = db.insert(DatabaseAccesser.replay_table, null, values);

        Log.e("GGG"," replay insert at count "+newRowId);
        if(newRowId<0)
            return  1;

        return newRowId;


    }
    public static long updateMediaInDatabase(long mediaPosition,@Nullable String global,@Nullable String local,@Nullable String lite_global,@Nullable String lite_local)
    {
        SQLiteDatabase db = getDatabase().getWritableDatabase();

        ContentValues values = new ContentValues();

        if(global!=null)
            values.put(DatabaseAccesser.media_global_uri, global);
        if(local!=null)
            values.put(DatabaseAccesser.media_local_uri, local);
        if(lite_global!=null)
            values.put(DatabaseAccesser.media_lite_global_uri, lite_global);
        if(lite_local!=null)
            values.put(DatabaseAccesser.media_lite_local_uri, lite_local);

        final long newRowId = db.update(DatabaseAccesser.media_table, values," rowid =? ",new String[]{mediaPosition+""});

        Log.e("MSGG","updated media count "+newRowId);
        return newRowId;


    }

    public static void clearMessageQueue(Context context,String user)
    {
        ContentValues values2 = new ContentValues();

        values2.put(DatabaseAccesser.chatList_msgqueue, 0);

        int rowln=getDatabase().getWritableDatabase().update(DatabaseAccesser.chatList_table,values2, DatabaseAccesser.chatList_userid+" =?",new String[]{user});

        Intent new_intent = new Intent();
        new_intent.setAction("CHAT_UPDATED");
        context.sendBroadcast(new_intent);

    }

    /*public static void updateSingleUserSeenSl(String targetUserID)
    {
        SQLiteDatabase db = getDatabase().getReadableDatabase();

        Cursor cursor = db.query(DatabaseAccesser.user_table,
                new String[]{
                        DatabaseAccesser.user_lastRec_sl},
                DatabaseAccesser.user_user_id + " =?",
                new String[]{targetUserID},
                null, null, null,null);
        if(cursor.moveToNext())
        {
            long serialID=cursor.getLong(cursor.getColumnIndexOrThrow(DatabaseAccesser.user_lastRec_sl));

            DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference().child("chat_status").child(targetUserID);
            databaseRef.child(UserManager.getUserInfo().getUid()).child("last_seen_msg").setValue(serialID);
        }
    }*/

    public static void updateMediaTableFromGlobalUrl(String global,String local)
    {
        SQLiteDatabase db= getDatabase().getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseAccesser.media_local_uri,local);
        int rowln=db.update(DatabaseAccesser.media_table,
                values,
                DatabaseAccesser.media_global_uri + " = ?",
                new String[]{global});
    }

    public static long saveFeedSeenStatus(Context context,NotificationData dat)
    {
        SQLiteDatabase db = getDatabase(context).getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DatabaseAccesser.feed_seen_timestamp, dat.timestamp);
        values.put(DatabaseAccesser.feed_seen_user_id, dat.userid);
        values.put(DatabaseAccesser.feed_seen_username, dat.text);
        values.put(DatabaseAccesser.feed_seen_feedrow, dat.rowId);


        final long newRowId = db.insert(DatabaseAccesser.feed_seen_table, null, values);

        Log.e("GGG"," feed seeeeen insert at count "+newRowId);
       /* if(newRowId<0)
            return  1;*/

        return newRowId;
    }
}
