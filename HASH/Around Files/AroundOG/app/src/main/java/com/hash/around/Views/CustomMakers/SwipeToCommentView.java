package com.hash.around.Views.CustomMakers;

/**
 * Created by Febin on 5/25/2018.
 */


import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Scroller;
import android.widget.TextView;

import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.R;

import java.lang.reflect.Field;

/**
 * Create On 16/10/2016
 * @author wayne
 */
public class SwipeToCommentView extends FrameLayout implements View.OnTouchListener {

    public SwipeToCommentView(Context context) {
        super(context);
    }

    public SwipeToCommentView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }
    DisplayMetrics dm;
    float heightLimit;
    public SwipeToCommentView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        dm=new DisplayMetrics();

        ((Activity)context). getWindowManager() .getDefaultDisplay().getMetrics(dm);
        heightLimit=((float)dm.heightPixels/100)*30;
    }
    TextView swipeText;

    boolean selfUser=false;
    public void setAsSelfUser(int num)
    {
        if(num>0) {
            swipeText.setText(getContext().getString(R.string.seen_by,num+""));
            selfUser = true;
            swipeText.setCompoundDrawablesWithIntrinsicBounds(getContext().getResources().getDrawable(R.drawable.ic_menu_seen), null, null, null);
        }
        else{
        setVisibility(GONE);}
    }
    @Override
    public void onFinishInflate() {
        super.onFinishInflate();

        swipeText = findViewById( R.id.swipeText);
        //this.floatingRecordButton = new FloatingRecordButton(getContext(), recordButtonFab);

        //View recordButton = findViewById( R.id.quick_audio_fab);
        setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(View v, final MotionEvent event) {

        if(selfUser)
            return  false;

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:


                this.actionInProgress = true;
                display(event.getY());


                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                if (this.actionInProgress) {
                    Log.e("GGG","touch up");
                    this.actionInProgress = false;
                    hide(event.getY());
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if (this.actionInProgress) {
                Log.e("GGG","touch move "+getOffset(swipeText,event.getY())+" / "+heightLimit);
                if (getOffset(swipeText,event.getY()) < -350) {
                    if (joblistner!=null)
                    joblistner.onJobCompletion("");
                    this.actionInProgress=false;
                    hide(event.getY());
                }
                else
                this.moveTo(swipeText,event.getY());
                }
                break;
        }
        return true;
    }
    boolean actionInProgress;
    private float startPositionX;
    private float lastPositionY;
    JobCompletionListner joblistner=null;
    public void setJobListener(JobCompletionListner list)
    {
        joblistner=list;
    }
    public void moveTo(View view, float x) {
        this.lastPositionY = x;

        float offset          = getOffset(view,x)/1.4f;
        int   widthAdjustment = getWidthAdjustment(view);

        Animation translateAnimation = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0f,
                Animation.RELATIVE_TO_SELF, 0f,
                Animation.ABSOLUTE, widthAdjustment + offset,
                Animation.ABSOLUTE, widthAdjustment + offset);

        translateAnimation.setDuration(0);
        translateAnimation.setFillAfter(true);
        translateAnimation.setFillBefore(true);

        view.startAnimation(translateAnimation);
    }
    public void display(float x) {
        this.startPositionX = x;
        this.lastPositionY = x;
    }
    private float getOffset(View v,float x) {
        return ViewCompat.getLayoutDirection(v) == ViewCompat.LAYOUT_DIRECTION_LTR ?
                -Math.max(0, this.startPositionX - x) : Math.max(0, x - this.startPositionX);
    }

    private int getWidthAdjustment(View v) {
        int width = v.getHeight() / 4;
        return ViewCompat.getLayoutDirection(v) == ViewCompat.LAYOUT_DIRECTION_LTR ? -width : width;
    }

    public void hide(float x) {
        this.lastPositionY = x;

        float offset          = getOffset(swipeText,x)/1.4f;
        int   widthAdjustment = getWidthAdjustment(swipeText);

        AnimationSet animation = new AnimationSet(false);
        Animation scaleAnimation = new ScaleAnimation(0.5f,1,0.5f, 1,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);

        Animation translateAnimation = new TranslateAnimation( Animation.RELATIVE_TO_SELF, 0,
                Animation.RELATIVE_TO_SELF, 0,
                Animation.ABSOLUTE, offset + widthAdjustment,
                Animation.ABSOLUTE, widthAdjustment
               );

        scaleAnimation.setInterpolator(new AnticipateOvershootInterpolator(1.5f));
        translateAnimation.setInterpolator(new DecelerateInterpolator());
        animation.addAnimation(scaleAnimation);
        animation.addAnimation(translateAnimation);
        animation.setDuration(300);
        animation.setFillBefore(true);
        animation.setFillAfter(false);
        animation.setInterpolator(new AnticipateOvershootInterpolator(1.5f));

       // recordButtonFab.setVisibility(View.GONE);
        swipeText.clearAnimation();
        swipeText.startAnimation(animation);
    }


}