package com.hash.around.Views.CustomMakers;

import android.content.Context;
import android.support.v7.widget.SearchView;

/**
 * Created by Febin on 7/20/2018.
 */

public class AroundSearchView extends SearchView {

    OnSearchViewEventListener mSearchViewEventListener;


    public AroundSearchView(Context context) {
        super(context);
    }

    @Override
    public void onActionViewCollapsed() {
        if (mSearchViewEventListener != null)
            mSearchViewEventListener.onSearchViewCollapsed();
        super.onActionViewCollapsed();
    }

    @Override
    public void onActionViewExpanded() {
        if (mSearchViewEventListener != null)
            mSearchViewEventListener.onSearchViewExpanded();
        super.onActionViewExpanded();
    }

    public interface OnSearchViewEventListener {
        public void onSearchViewCollapsed();
        public void onSearchViewExpanded();
    }


    public void setOnSearchViewEventListener(OnSearchViewEventListener eventListener) {
        mSearchViewEventListener = eventListener;
    }


}