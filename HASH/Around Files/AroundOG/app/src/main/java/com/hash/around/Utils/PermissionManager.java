package com.hash.around.Utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

/**
 * Created by Febin on 6/28/2018.
 */

public class PermissionManager {

    public static int CAMERA_PERMISSION=9877;
    public static boolean checkHasPermission(Activity act)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (act.checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED  ) {
                    act.requestPermissions(new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION);

                    return false;
                }
        }

            return true;

    }

    public static int EXTERNAL_STORAGE_READ_WRITE_PERMISSION_CODE = 9876;
    public static boolean checkExternalStoragePermission(Activity act)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (act.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || act.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                //Toast.makeText(SetProfile.this, "Not granted", Toast.LENGTH_SHORT).show();
                act.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.INTERNET}, EXTERNAL_STORAGE_READ_WRITE_PERMISSION_CODE);

                return false;
            }

        }
            return true;

    }
    public static int LOCATION_ACCESS_PERMISSION_CODE = 9875;

    public static boolean checkLocationPermission(Activity act)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (act.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || act.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(act,
                        Manifest.permission.ACCESS_FINE_LOCATION)) {
                } else {
                    act.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_ACCESS_PERMISSION_CODE);
                }
                return false;
            }
        }
        return true;
    }

    public static int RECORD_AUDIO_PERMISSION_CODE = 9874;

    public static boolean checkRecordAudioPermission(Activity act)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (act.checkSelfPermission(Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED ) {
               // if (ActivityCompat.shouldShowRequestPermissionRationale(act,
               //         Manifest.permission.RECORD_AUDIO)) {
               // } else {
                    act.requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO}, RECORD_AUDIO_PERMISSION_CODE);
               // }
               return false;
            }
        }
        return true;
    }

    public static int CONTACTS_PERMISSION_CODE = 9874;
    public static boolean checkContactsPermission(Activity act)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (act.checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED ) {
              //  if (ActivityCompat.shouldShowRequestPermissionRationale(act,
                 //       Manifest.permission.READ_CONTACTS)) {
                //} else {
                    act.requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, CONTACTS_PERMISSION_CODE);
                //}
                return false;
            }
        }
        return true;
    }

}
