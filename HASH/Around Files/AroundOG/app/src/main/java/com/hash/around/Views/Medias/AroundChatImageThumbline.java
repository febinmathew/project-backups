package com.hash.around.Views.Medias;

/**
 * Created by Febin on 5/27/2018.
 */

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

/*import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.FitCenter;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader.DecryptableUri;
import org.thoughtcrime.securesms.mms.GlideRequest;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.mms.SlideClickListener;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.whispersystems.libsignal.util.guava.Optional;*/

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnPausedListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.hash.around.MainServiceThread;
import com.hash.around.OtherClasses.DataTypes.ChatData;
import com.hash.around.OtherClasses.DataTypes.DownloadUploadData;
import com.hash.around.OtherClasses.DataTypes.GroupChatData;
import com.hash.around.OtherClasses.Interfaces.ChatSentListner;
import com.hash.around.OtherClasses.Interfaces.FileDownloadListner;
import com.hash.around.OtherClasses.Interfaces.FileUploadListner;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.R;
import com.hash.around.TabbedActivity;
import com.hash.around.Utils.ChatSender;
import com.hash.around.Utils.FeedSender;
import com.hash.around.Utils.MediaManager;
import com.hash.around.Views.CustomMakers.AroundDownloadView;
import com.pitt.library.fresh.FreshDownloadView;

import java.io.File;
import java.io.IOException;


//import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class AroundChatImageThumbline extends FrameLayout {

    private static final String TAG = AroundChatImageThumbline.class.getSimpleName();


    private ImageView       image;
    AroundDownloadView downloadView;
    private ImageView       playOverlay,gif_overlay;
    //private int             backgroundColorHint;
    private int             radius;
    TextView sizeText;
    ChatSentListner uploadListner = null;
    JobCompletionListner jobComplete = null;

    LinearLayout downloadViewBG;



    public AroundChatImageThumbline(Context context) {
        this(context, null);
    }

    public AroundChatImageThumbline(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AroundChatImageThumbline(final Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        inflate(context, R.layout.around_image_thumbline_view, this);

        this.radius      =getResources().getDimensionPixelSize(R.dimen.default_corner_radius);
        this.image       = findViewById(R.id.thumbnail_image);
        this.playOverlay = findViewById(R.id.play_overlay);
        this.gif_overlay=findViewById(R.id.gif_overlay);
        sizeText=findViewById(R.id.sizeText);
        downloadView=findViewById(R.id.downloadView);
        downloadViewBG=findViewById(R.id.downloadViewBG);

        if (attrs != null) {
            TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.AroundChatImageThumbline, 0, 0);
            //backgroundColorHint   = typedArray.getColor(R.styleable.AroundChatImageThumbline_backgroundColorHint, Color.BLACK);

            typedArray.recycle();
        }

        /*FreshDownloadView ddownloadView=findViewById(R.id.downloadView);
        Log.e("MSG", "download ready  "+downloadView);
        ddownloadView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                downloadFile("https://firebasestorage.googleapis.com/v0/b/aroundsocialmedia77.appspot.com/o/profile_images%2Fxxwzvdmnzbfpflfinkbt.png?alt=media&token=b49efc28-3cc1-4361-87c1-de52b57db3cb");
            }
        });*/
    }

    public void setImageUri(Uri uri)
    {

        this.image.setImageURI(uri);
    }

    public ImageView getImageView()
    {return this.image;}

    public void setImageScaleType(boolean isChat)
    {
        if(isChat)
            this.image.setScaleType(ImageView.ScaleType.CENTER_CROP);
        else
            this.image.setScaleType(ImageView.ScaleType.FIT_CENTER);
    }

    public void setSizeText(String text)
    {
        sizeText.setText(text);
    }

    int mediaType;
    public void setMediaType(int type)
    {

        this.mediaType=type;
    }
    public void updateMideaWithType(int visibility)
    {
        switch (mediaType)
        {
            case 1:
                playOverlay.setVisibility(GONE);
                gif_overlay.setVisibility(GONE);
                break;
            case 3:
                gif_overlay.setVisibility(GONE);
                playOverlay.setVisibility(visibility);

                break;
            case 5:
                playOverlay.setVisibility(GONE);
                gif_overlay.setVisibility(visibility);
                break;
        }

    }

    public void enableDownloadClick()
    {
        downloadViewBG.setVisibility(VISIBLE);
        //downloadView.setVisibility(VISIBLE);

        //sizeText.setVisibility(VISIBLE);
        //playOverlay.setVisibility(GONE);
    }
    public void disableDownloadClick() {
        downloadViewBG.setVisibility(GONE);
        Log.e("MSG", "disable");
        //downloadView.setVisibility(GONE);
        downloadView.setOnClickListener(null);
        //sizeText.setVisibility(GONE);

        updateMideaWithType(VISIBLE);

    }
boolean autodownload=false;
    public void setMediaAsFeed(boolean isFeed)
    {

        type=isFeed?1:0;
    }

    int type=0;
    public void setDownloadClickListner(String uri,int mediaType) {
        enableDownloadClick();
        downloadView.setIsDownload();
        final String url = uri;

        SingleViewDownloadHandler.setDownloadClickListner(getContext(),uri,mediaType,type,downloadView,jobComplete);
        /*Log.e("GGG","autodownload "+"trueee3");

        if(type==0) {
            if (MainServiceThread.checkSearchMultiSelectPref(getContext(), "data_usage_chat", mediaType)) //vvvv
            {
                Log.e("GGG", "autodownload " + "trueee");
                autodownload = true;
                downloadFile(url);
            }
            if (!autodownload) {
                downloadView.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        downloadFile(url);
                    }
                });
            }
        }
        else if(type==1)
        {
            if (MainServiceThread.checkSearchMultiSelectPref(getContext(), "data_usage_feed", TabbedActivity.PREF_IMAGE))
            {
                Log.e("GGG", "autodownload " + "trueee");
                autodownload = true;
                //downloadFile(url);
                SingleViewDownloadHandler.downloadFeedFileFromSingleView(getContext(),url,downloadView,jobComplete);
            }

            if (!autodownload) {
                downloadView.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SingleViewDownloadHandler.downloadFeedFileFromSingleView(getContext(),url,downloadView,jobComplete);
                    }
                });
            }
        }*/
    }
    public void setOnJobCompletionListner(JobCompletionListner job) {
        this.jobComplete = job;
    }

    public void setOnFileUploadListener(ChatSentListner job) {
        this.uploadListner = job;
    }

    public void setUploadClickListner(String uri, final GroupChatData data,final long mediaRow,final StorageReference ref) {
        enableDownloadClick();
        downloadView.setIsUpload();
        final String url = uri;

        /*if(type==0) {

            downloadView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    uploadFile(url, ref, data, mediaRow, null);
                }
            });
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    DownloadUploadData data22 = MediaManager.getDownloadListItemArrayFromUri(url);
                    if (data22 != null) {

                        uploadFile(url, ref, data, mediaRow, data22);
                    }
                }
            }, 1000);
        }*/
        SingleViewDownloadHandler.setUploadChatClickListner(getContext(),uri,0,data,downloadView,ref,uploadListner);

    }
    //GroupChatData thisChatData;
    void downloadFile(String urlToDownload) {
        downloadView.startDownload();
        Log.e("GGG","autodownload "+"startdownload");
       /* MainServiceThread.mainService.downloadFileAndReturnUriString(urlToDownload, new FileDownloadListner() {
            @Override
            public void onProgressListener(int percent) {
                downloadView.upDateProgress( percent);
            }

            @Override
            public void onSuccessListener(String localFilename) {
                disableDownloadClick();
                if (jobComplete != null) {
                    jobComplete.onJobCompletion(localFilename);
                }
            }

            @Override
            public void onFailureListener(int percent) {
                downloadView.showDownloadError();
            }

            @Override
            public void onPausedListener(int percent) {
                downloadView.reset();
            }
        });*/
        /*Log.e("MSG", "download "+urlToDownload);


        File localFile = null;
        final String localFilename;
        try {
            localFile = File.createTempFile("images", "png");

        } catch (IOException e) {
            e.printStackTrace();
        }
        localFilename = localFile.toURI().toString();
        StorageReference httpsReference = FirebaseStorage.getInstance().getReferenceFromUrl(urlToDownload);
        httpsReference.getFile(localFile).addOnProgressListener(new OnProgressListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onProgress(FileDownloadTask.TaskSnapshot taskSnapshot) {
                float percent = ((float) taskSnapshot.getBytesTransferred() / (float) taskSnapshot.getTotalByteCount()) * 100;
                // Log.e("MSG",((float)taskSnapshot.getBytesTransferred()/(float)taskSnapshot.getTotalByteCount())*100+"//\\"+
                //((float)taskSnapshot.getBytesTransferred()/(float)taskSnapshot.getTotalByteCount())+"//\\"+percent);
                downloadView.upDateProgress((int) percent);
            }
        }).addOnFailureListener(new OnFailureListener() {

            @Override
            public void onFailure(@NonNull Exception e) {
                downloadView.showDownloadError();
            }
        }).addOnPausedListener(new OnPausedListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onPaused(FileDownloadTask.TaskSnapshot taskSnapshot) {
                downloadView.reset();
                Log.e("MSG", "paused");

            }
        }).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                // downloadView.showDownloadOk();
                Log.e("MSG", "succeded");
                disableDownloadClick();
                if (jobComplete != null) {
                    jobComplete.onJobCompletion(localFilename);
                }
                //downloadView.clearAnimation();

            }
        })

        ;*/
    }

    void uploadFile(String urlToUpload,StorageReference ref,final GroupChatData data,long mediaID,DownloadUploadData data22) {
        //Log.e("MSGG","uri is : "+urlToUpload );
        //int type=mediaType;

        FileUploadListner uploadListner2=new FileUploadListner() {
            @Override
            public void onFileUploaded(String global, String globalLite, String liteLocal) {
                disableDownloadClick();
                Log.e("GGG","downloadme1 COMPKETE DISABLE ");
                if (uploadListner != null) {
                    uploadListner.onChatUploaded(data);
                }
            }

            int prePercent=0;
            @Override
            public void onProgressListener(int percent) {
                Log.e("GGG","downloadme1 PERCENT "+percent);
                if(percent==100 && prePercent==100)
                    return;

                downloadView.upDateProgress(percent);
                prePercent=percent;
            }

            @Override
            public void onFailureListener(int percent) {
                downloadView.showDownloadError();
            }

            @Override
            public void onPausedListener(int percent) {
                downloadView.reset();
            }
        };
        if(data22!=null &&data22.progress>=0 ) {
            Log.e("GGG","downloadme1 with pro "+data22.progress);
            downloadView.startDownload();
            //downloadView.upDateProgress(data22.progress);
        }
        else {
            Log.e("GGG", "downloadme1 ");
            //downloadView.startDownload();}
        }
        Log.e("GGG","uploadpro "+(MediaManager.getDownloadListItemArrayFromUri(urlToUpload)!=null));

        if(MediaManager.getDownloadListItemArrayFromUri(urlToUpload)!=null)
        {
           // Log.e("GGG","downloadme1 ");
            MediaManager.uploadResourceAndReturnUrls(getContext(),data.temp_url,data.type,urlToUpload,ref,uploadListner2);}
        else {
            Log.e("GGG","uploadpro SENTING FILE");
            ChatSender.uploadMediaData(urlToUpload, data, null, uploadListner,uploadListner2 ); }

        /*uploadResourceAndReturnUrls(numberType, urlToUpload, ref,new FileUploadListner() {
            @Override
            public void onFileUploaded(String global, String globalLite) {

                uploadListner.onFileUploaded(global,globalLite);
            }
        });*/
    }

    public void setLocallyAvailable(boolean locallyAvailable)
    {
        if(locallyAvailable)
        {
            disableDownloadClick();
        }
        else
        {
            enableDownloadClick();
        }
    }


    private final RectF drawRect = new RectF();
    private final Path clipPath = new Path();
    int dimen=getResources().getDimensionPixelSize(R.dimen.default_corner_radius);

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        drawRect.left = 0;
        drawRect.top = 0;
        drawRect.right = getWidth();
        drawRect.bottom = getHeight();
        clipPath.reset();
        clipPath.addRoundRect(drawRect, dimen, dimen, Path.Direction.CCW);
        clipPath.close();
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        int save = canvas.save();
        canvas.clipPath(clipPath);
        super.dispatchDraw(canvas);
        canvas.restoreToCount(save);
    }

}
