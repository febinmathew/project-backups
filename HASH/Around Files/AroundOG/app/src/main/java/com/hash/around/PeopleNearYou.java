package com.hash.around;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.cleveroad.fanlayoutmanager.FanLayoutManager;
import com.cleveroad.fanlayoutmanager.FanLayoutManagerSettings;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.hash.around.OtherClasses.AdapterPeopleNearby;
import com.hash.around.OtherClasses.DataTypes.ContactListData;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.OtherClasses.Interfaces.LocationReceiver;
import com.hash.around.OtherClasses.Interfaces.ProcessCompletedListner;
import com.hash.around.OtherClasses.DataTypes.UserPrivacyData;
import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.hash.around.Utils.AppOnlineStatusManager;
import com.hash.around.Utils.AroundStaticInfo;
import com.hash.around.Utils.NetworkUtils;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;


import static com.hash.around.MainServiceThread.UserFollowApproved;
import static com.hash.around.MainServiceThread.fetchUserDetailsbyIDOrPhone;
import static com.hash.around.MainServiceThread.getPrivacyFromuserSnapshot;
import static com.hash.around.MainServiceThread.lightningEasyUserUpdateListner;
import static com.hash.around.MainServiceThread.newUserFollowRequestWithDoubleCheck;
import static com.hash.around.MainServiceThread.updateOrInsertUserIfNone;
import static com.hash.around.TabbedActivity.fDatabase;

import static com.hash.around.TabbedActivity.fetchFusedLocation;
import static com.hash.around.TabbedActivity.userdetail;

public class PeopleNearYou extends Activity {
    ArrayList<ContactListData> finaldata=new ArrayList<ContactListData>();
    ArrayList<UserPrivacyData> finaldata2=new ArrayList<UserPrivacyData>();
    AdapterPeopleNearby adap;
    RecyclerView rView;
    private RecyclerView.LayoutManager mLayoutManager;

    ProgressBar peopleNearProgressBar;

    public ArrayList<ContactListData> generateDummy()
    {
        ArrayList<ContactListData> list=new ArrayList<>();

        ContactListData x=new ContactListData();

        x.latitude=11.3445;
        x.longitude=54.3445;
        list.add(x);

        x=new ContactListData();

        x.latitude=11.3445;
        x.longitude=54.3445;
        list.add(x);
        x=new ContactListData();

        x.latitude=11.3445;
        x.longitude=54.3445;
        list.add(x);
        x=new ContactListData();

        x.latitude=11.3445;
        x.longitude=54.3445;
        list.add(x);
        x=new ContactListData();

        x.latitude=11.3445;
        x.longitude=54.3445;
        list.add(x);
        x=new ContactListData();

        x.latitude=11.3445;
        x.longitude=54.3445;
        list.add(x);
        x=new ContactListData();

        x.latitude=11.3445;
        x.longitude=54.3445;
        list.add(x);
        x=new ContactListData();

        x.latitude=11.3445;
        x.longitude=54.3445;
        list.add(x);




        return list;
    }
    GeoFire geoFire;
    GeoQuery geoQuery;
    ValueEventListener postListener;
    FanLayoutManager fanLayoutManager;
    LinearLayout noFeedsAvailable;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        setContentView(R.layout.people_near_activity);
        setFinishOnTouchOutside(true);
        DisplayMetrics dm=new DisplayMetrics();

        getWindowManager().getDefaultDisplay().getMetrics(dm);

        getWindow().setLayout((int)(dm.widthPixels*1),(int)(dm.heightPixels));
        getWindow().setGravity(Gravity.BOTTOM);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        //finaldata=generateDummy();
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("People near by");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        rView=(RecyclerView)findViewById(R.id.rViewPeopleNear);
        peopleNearProgressBar=(ProgressBar) findViewById(R.id.peopleNearProgressBar);

        /*DEAFULT*/
      /* mLayoutManager=new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false);
        rView.setLayoutManager(mLayoutManager);*/

        /*FAN  LAYOUT*/
        FanLayoutManagerSettings fanLayoutManagerSettings = FanLayoutManagerSettings
                .newBuilder(getApplicationContext())
                .withFanRadius(true)
                .withAngleItemBounce(3)

                .build();
        fanLayoutManager = new FanLayoutManager(getApplicationContext(),fanLayoutManagerSettings);
        rView.setLayoutManager(fanLayoutManager);


        noFeedsAvailable=findViewById(R.id.noFeedsAvailable);
        showNoFeedsAvailable(false,null);
        adap=new AdapterPeopleNearby(this,finaldata,finaldata2,rView,fanLayoutManager );
        rView.setAdapter(adap);
       // adap.notifyDataSetChanged();
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("user_location");
        geoFire = new GeoFire(ref);
        captureLocationAndUpdateContents();
        registerForContextMenu(rView);


        refreshButton=findViewById(R.id.refreshButton);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("GGG","peoplenear button click");
                showNoFeedsAvailable(false,null);
                captureLocationAndUpdateContents();
            }
        });

    }
    ImageButton refreshButton;
    @Override
    public boolean onContextItemSelected(MenuItem item) {


        int pos= AdapterPeopleNearby.optionPosition;
        final String selectedUser = finaldata.get(pos).user_id;
        final MenuItem chageItem=item;
        switch (item.getItemId()) {

            case R.id.action_follow_user:
                if(adap.privacydata.get(pos).anyonefollow<=AdapterPeopleNearby.PRIVACYTHRESHOLD)
                {
                    UserFollowApproved(getApplicationContext(),adap.data.get(pos).user_id);
                }
                else
                {
                    newUserFollowRequestWithDoubleCheck(this,adap.data.get(pos).user_id,new JobCompletionListner(){
                        @Override
                        public void onJobCompletion(String txt1) {
                            chageItem.setTitle(txt1);
                        }
                    });
                }
                break;
            case R.id.view_profile:
                    if(AdapterPeopleNearby.userInDB==1)
                    {
                        Intent intent=new Intent(this,Profile_Activity.class);
                        intent.putExtra("userid",selectedUser);
                        startActivity(intent);
                    }
                    else
                    {
                        lightningEasyUserUpdateListner(getApplicationContext(),selectedUser,null,new JobCompletionListner(){
                            @Override
                            public void onJobCompletion(String txt1) {
                                Intent intent=new Intent(PeopleNearYou.this,Profile_Activity.class);
                                intent.putExtra("userid",selectedUser);
                                startActivity(intent);
                            }
                        });
                    }
                break;
            case R.id.chat_message:
                //if(adap.privacydata.get(pos).anyonefollow<=AdapterPeopleNearby.PRIVACYTHRESHOLD)
                //{
                if(AdapterPeopleNearby.userInDB==1)
                {
                    Intent intent=new Intent(this,ChatWindow.class);
                    intent.putExtra("userid",finaldata.get(pos).user_id);
                    intent.putExtra("username",finaldata.get(pos).user_name);
                    intent.putExtra("user_type",finaldata.get(pos).type);
                    startActivity(intent);
                }
                else
                {
                    saveuserIfNeed(selectedUser);
                }



                break;
        }
        return super.onContextItemSelected(item);

    }

    void saveuserIfNeed(String user)
    {
        fetchUserDetailsbyIDOrPhone(user,true,false,new ProcessCompletedListner(){
            @Override
            public void processCompleteAction() {}

            @Override
            public void newUserAdded(UserWholeData data) {

                updateOrInsertUserIfNone(getApplicationContext(),data, new JobCompletionListner() {
                    @Override
                    public void onJobCompletion(String txt1) {


                    }
                });
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        AppOnlineStatusManager.OnlineStatus.setActive(true);

    }
    @Override
    public void onStop() {
        AppOnlineStatusManager.OnlineStatus.setActive(false);
        //overridePendingTransition(R.anim.no_change_anim, R.anim.slide_down_anim);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(geoQuery!=null)
        geoQuery.removeAllListeners();
    }


    @Override
    protected void onPause() {
        overridePendingTransition(R.anim.no_change_anim, R.anim.slide_down_anim);
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        if(fanLayoutManager.getSelectedItemPosition()>=0)
        {
            fanLayoutManager.switchItem(rView,fanLayoutManager.getSelectedItemPosition());
            return;
        }
        super.onBackPressed();
    }

    void captureLocationAndUpdateContents()
    {
        Log.e("GGG","peoplenear update2 content ");
        if(!NetworkUtils.networkIsAvailable(getApplicationContext()))
        {
            Log.e("GGG","peoplenear show feeds available content ");
            showNoFeedsAvailable(true,R.string.no_network_connection);
            return;
        }
        fetchFusedLocation(new LocationReceiver() {
            @Override
            public void onLocationReceived(Location data) {
                updateContent(data);
            }
        }, this);
    }

    boolean listenForNoFeeds=true;

    void showNoFeedsAvailable(boolean visible,Integer text)
    {
        if(visible){
            if(text!=null)
                ((TextView)noFeedsAvailable.findViewById(R.id.nofeedText)).setText(text);
        noFeedsAvailable.setVisibility(View.VISIBLE);
        peopleNearProgressBar.setVisibility(View.GONE);
        }
        else
        {
            noFeedsAvailable.setVisibility(View.GONE);
            peopleNearProgressBar.setVisibility(View.VISIBLE);
        }
    }

    void updateContent(Location loc)
    {
        AdapterPeopleNearby.deafultLocation=loc;
        Double lat=loc.getLatitude();
        Double lng=loc.getLongitude();


        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                if(listenForNoFeeds)

                 showNoFeedsAvailable(true,R.string.no_nearbypeople_available);
            }
        }, 10000);


        geoQuery = geoFire.queryAtLocation(new GeoLocation(lat, lng), 4);
        geoQuery.addGeoQueryEventListener(new GeoQueryEventListener() {
            @Override
            public void onKeyEntered(String key, GeoLocation location) {

                Log.e("GGG","peoplenear key enter" +key);
                if (key.equals(userdetail.getUid()))
                    return;
                showNoFeedsAvailable(false,null);
                listenForNoFeeds=false;

                Log.e("MSGG", "got an IDIOT "+key);
                final GeoLocation loc=location;
               DatabaseReference newPostRef = fDatabase.child("user_profile").child(key);

                postListener = new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // Get Post object and use the values to update the UI
                        final ContactListData post = dataSnapshot.getValue(ContactListData.class);
                        /*new changes*/
                        post.latitude=loc.latitude;
                        post.longitude=loc.longitude;


                        /*new changes*/
                       //dataSnapshot.getChildren();

                      final  UserPrivacyData postPrivacy =getPrivacyFromuserSnapshot(dataSnapshot);

                        Log.e("MSGG","Threshhold is + "+postPrivacy.anyonefollow);

                        Log.e("MSGG",postPrivacy.toString());

                        Log.e("MSGG",postPrivacy.profile_picture+" User status");
                            StorageReference httpsReference = FirebaseStorage.getInstance().getReferenceFromUrl(post.img_url);
                            httpsReference.getBytes(1024 * 1024).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                                @Override
                                public void onSuccess(byte[] bytes) {
                                    // Data for "images/island.jpg" is returns, use this as needed

                                    //String output = ;

                                    try {

                                        File file = new File(AroundStaticInfo.getDefaultPeopleNearDirectory(getApplicationContext()));
                                        if (!file.exists()) {
                                            file.mkdirs();
                                        }
                                        //File tempFile = new File(file.getAbsolutePath(),"contact.vcf");

                                        String name = post.user_name + ".png";
                                        FileOutputStream fos = new FileOutputStream(new File(file.getAbsolutePath(), name));
                                        fos.write(bytes);
                                        fos.close();

                                        //Toast.makeText(getActivity(),  , Toast.LENGTH_SHORT).show();

                                        post.img_url = file.toURI().toString() + name;


                                        peopleNearProgressBar.setVisibility(View.GONE);
                                        finaldata.add(post);
                                        finaldata2.add(postPrivacy);
                                        adap.notifyItemInserted(finaldata.size() - 1);

                                        // adap.notifyItemChanged(finaldata.size() - 1);
                                        // rView.scrollToPosition(finaldata.size() - 1);

                                        //savePostToDatabase(post);


                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    // Handle any errors
                                }
                            });


//                      Toast.makeText(getActivity(), "hurray", Toast.LENGTH_SHORT).show();
                            // ...

                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        // Getting Post failed, log a message
                        // Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
                        // ...
                    }
                };

                newPostRef.addListenerForSingleValueEvent(postListener);


                //System.out.println(String.format("Key %s entered the search area at [%f,%f]", key, location.latitude, location.longitude));
            }

            @Override
            public void onKeyExited(String key) {
                //System.out.println(String.format("Key %s is no longer in the search area", key));
            }

            @Override
            public void onKeyMoved(String key, GeoLocation location) {
                //System.out.println(String.format("Key %s moved within the search area to [%f,%f]", key, location.latitude, location.longitude));
            }

            @Override
            public void onGeoQueryReady() {
                //System.out.println("All initial data has been loaded and events have been fired!");
            }

            @Override
            public void onGeoQueryError(DatabaseError error) {
                //System.err.println("There was an error with this query: " + error);
            }
        });


    }

}
