package com.hash.around;

import android.Manifest;
import android.app.Activity;
import android.app.LoaderManager;
import android.app.SearchManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.hash.around.OtherClasses.Adapter_Contacts;
import com.hash.around.OtherClasses.DataTypes.ChatData;
import com.hash.around.OtherClasses.DataTypes.ContactListData;
import com.hash.around.OtherClasses.DataTypes.GroupChatData;
import com.hash.around.OtherClasses.DataTypes.MediaData;
import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.Utils.AppOnlineStatusManager;
import com.hash.around.Utils.MediaManager;
import com.hash.around.Views.GalleryFolderSquareView;
import com.hash.around.Views.RecentPhotosLoader;
import com.hash.around.Views.SquareFrameLayout;

import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;


import static com.hash.around.TabbedActivity.fDatabase;
import static com.hash.around.TabbedActivity.saveContactToDatabase;
import static com.hash.around.TabbedActivity.userdetail;

public class GalleryActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>{


    /*DatabaseReference newPostRef;
    ChildEventListener childEventListener;


    RecyclerView rView;
    Adapter_Contacts adap;
    ArrayList<UserWholeData> finaldata;
    private LinearLayoutManager mLayoutManager;

    ArrayList<ChatData> forwardchatData;

    boolean chatforwardActivity=false;
    boolean dataBackwardActivity=false;*/
    public ArrayList<MediaData> generateDummy() {
        ArrayList<MediaData> list = new ArrayList<MediaData>();

        list.add(new MediaData());
        list.add(new MediaData());
        list.add(new MediaData());
        list.add(new MediaData());
        list.add(new MediaData());
        list.add(new MediaData());
        list.add(new MediaData());
        list.add(new MediaData());
        list.add(new MediaData());
        list.add(new MediaData());
        list.add(new MediaData());
        list.add(new MediaData());
        list.add(new MediaData());
        list.add(new MediaData());
        list.add(new MediaData());
        list.add(new MediaData());
        list.add(new MediaData());
        list.add(new MediaData());
        list.add(new MediaData());




        return list;
    }

    RecyclerView rView;
    RecyclerView.LayoutManager layoutManager;
    GalleryAdapter adap;
    ArrayList<MediaData> finaldata = new ArrayList<MediaData>();
    Integer GALLERY_MEDIA_TYPE;
    int receiverAcitvity;
    String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.galleryactivity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        receiverAcitvity=getIntent().getIntExtra("forChat",0);
        userId=getIntent().getStringExtra("userid");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.select_media);
        GALLERY_MEDIA_TYPE=getIntent().getIntExtra("mediaType",0);

        FloatingActionButton contactFloatButton = (FloatingActionButton) findViewById(R.id.openNativeGallery);
        contactFloatButton.setImageResource(R.drawable.ic_gallery_white);


        rView = (RecyclerView) findViewById(R.id.rView);
        rView.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(getApplicationContext(), 3,LinearLayoutManager.HORIZONTAL,false);
        //new GridLayoutManager()
        rView.setLayoutManager(layoutManager);

        //finaldata = generateDummy();


        adap = new GalleryAdapter(GalleryActivity.this, finaldata);
        rView.setAdapter(adap);
        //getData();

        getLoaderManager().initLoader(0, null, this);

    }
    private ArrayList getData() {
        final ArrayList imageItems = new ArrayList();

        File path = Environment.getExternalStorageDirectory();

        checkDirectory(path);
        return imageItems;
    }


    private boolean checkDirectory( File dir ) {
        if ( !true ) {
            return false;
        } else {
            final ArrayList<File> subDirs = new ArrayList<File>();
            int songNumb = dir.listFiles( new FileFilter() {

                @Override
                public boolean accept(File file) {
                    if ( file.isFile() )
                    {
                        if ( file.getName().equals( ".nomedia" ) )
                            return false;

                        return checkFileExtension( file.getName() );
                    } else if ( file.isDirectory() ){
                        subDirs.add( file );
                        return false;
                    } else
                        return false;
                }
            } ).length;

            if ( songNumb > 0 ) {
                Log.e("MSG", "checkDirectory: dir== " + dir.getName() + " -> " + songNumb );
                //return true;
            }

            for( File subDir: subDirs ) {
                //Log.e("MSG", "checkDirectory [for]: subDir " + subDir.toString());
                if ( checkDirectory( subDir ) ) {

                    //return true;
                }
            }
            return false;
        }
    }

    private boolean checkFileExtension( String fileName ) {
        String ext = getFileExtension(fileName);
        if ( ext == null) return false;
        try {
            if ( ext.equals("jpg") ) {
                return true;
            }
        } catch(IllegalArgumentException e) {
            //Not known enum value
            return false;
        }
        return false;
    }

    public String getFileExtension( File f ) {
        return getFileExtension( f.getName() );
    }

    public String getFileExtension( String fileName ) {
        int i = fileName.lastIndexOf('.');
        if (i > 0) {
            return fileName.substring(i+1);
        } else
            return null;
    }

    Uri BASE_URL;
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Log.e("MGG","kola status ");
        //String[] projection = {MediaStore.Images.Media.DATA,MediaStore.Images.Media.BUCKET_ID,MediaStore.Images.Media.BUCKET_DISPLAY_NAME};
        String[] PROJECTION  = {
                MediaStore.Files.FileColumns._ID,
                MediaStore.Files.FileColumns.DATA,
                MediaStore.Files.FileColumns.DATE_ADDED,
                MediaStore.Files.FileColumns.MEDIA_TYPE,
                MediaStore.Files.FileColumns.MIME_TYPE,
                MediaStore.Files.FileColumns.TITLE,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME
        };

         String SELECTION = getSelectionStringFromMediaType(GALLERY_MEDIA_TYPE);
        BASE_URL=MediaStore.Files.getContentUri("external");



        return new CursorLoader(this, BASE_URL, PROJECTION, SELECTION/* +") GROUP BY ("+ MediaStore.Images.Media.BUCKET_DISPLAY_NAME*/ , null, MediaStore.Files.FileColumns.DATE_MODIFIED+" DESC");


    }
    @Override
    protected void onStart() {
        super.onStart();
        AppOnlineStatusManager.OnlineStatus.setActive(true);

    }
    @Override
    public void onStop() {
        AppOnlineStatusManager.OnlineStatus.setActive(false);

        super.onStop();
    }

    public static String getSelectionStringFromMediaType(int GALLERY_MEDIA_TYPE)
    {
        String selection="";
        if(GALLERY_MEDIA_TYPE==0) {
            selection = MediaStore.Files.FileColumns.MEDIA_TYPE + "="
                    + MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE
                    + " OR "
                    + MediaStore.Files.FileColumns.MEDIA_TYPE + "="
                    + MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO;
        }
        else
        {
            switch(GALLERY_MEDIA_TYPE) {
                case 1:
                    selection = MediaStore.Files.FileColumns.MEDIA_TYPE + "="
                            + MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
                    break;
                case 3:
                    selection =  MediaStore.Files.FileColumns.MEDIA_TYPE + "="
                            + MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO;
                    break;
                case 5:

                    selection = MediaStore.Files.FileColumns.MIME_TYPE + "='"
                            + MimeTypeMap.getSingleton().getMimeTypeFromExtension("gif")+"'";
                    break;
            }
        }
        return selection;
    }

    int checkIfFolderExists(String folderName)
    {
        for(int i=0;i<finaldata.size();i++)
        {
            if(finaldata.get(i).folderName.equals(folderName))
                return i;
        }
        return -1;
    }
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        //Log.e("MGG","kola status ");
        Log.e("MGG","count is "+data.getCount());
        //ArrayList<MediaData> media=new ArrayList<>();
        finaldata.clear();
        String currentFolder="";
        int folderCount=0;
        while (data.moveToNext())
        {
            String fileid = data.getString(0);
            String fileuri = data.getString(1);
            String mimeType = data.getString(4);
            Integer mediaType = data.getInt(3);
            String folderName=data.getString(6);

            int no=checkIfFolderExists(folderName);
            if(no!=-1)
            {
                if(finaldata.get(no).type!=-1)
                if(finaldata.get(no).type!=MediaManager.getMediTypeFromMime(mimeType,mediaType))
                    finaldata.get(no).type=-1;

                finaldata.get(no).mediaSize++;
            }
            else
            {
                MediaData temp=new MediaData();
                temp.folderName=folderName;
                temp.uri=fileid;
                temp.mediaSize=1;
                temp.type=MediaManager.getMediTypeFromMime(mimeType,mediaType);
                finaldata.add(temp);
            }


            /*if(!folderName.equals(currentFolder))
            {

                MediaData temp=new MediaData();
                temp.folderName=folderName;
                temp.uri=fileid;
                Log.e("MGG",folderName+" =uri is "+fileuri);
                finaldata.add(temp);
                currentFolder=folderName;
                if(folderCount>0)
                {
                    finaldata.get(finaldata.size()-2).mediaSize=folderCount;
                    folderCount=0;
                }
            }
            folderCount++;*/

        }
        /*if(folderCount>0)
        {
            finaldata.get(finaldata.size()-2).mediaSize=folderCount;
        }*/
        Log.e("MGG","size is is "+finaldata.size());
       // finaldata=media;
        adap.notifyDataSetChanged();

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {

        Context mContext;
        ArrayList<MediaData> data;

        public GalleryAdapter(Context mContext, ArrayList<MediaData> data) {
            this.mContext = mContext;
            this.data = data;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.gallery_view_item, parent, false);

            //v.getLayoutParams().height = (int) getResources().getDimension(R.dimen.gallery_items);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {

            Uri uri=Uri.withAppendedPath(BASE_URL,data.get(position).uri);
            Log.e("MGG","uri NOWW " + uri);

            holder.folderView.setFolderDetails(mContext,data.get(position).type,uri,data.get(position).folderName,data.get(position).mediaSize);

            holder.folderView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                        Intent intent=new Intent(GalleryActivity.this,GallerySubDirectoryActivity.class);
                        intent.putExtra("bucket",data.get(position).folderName);
                        intent.putExtra("mediaType",GALLERY_MEDIA_TYPE);
                    Log.e("GGG","uploadreceive GAL "+receiverAcitvity+" / "+userId);
                    intent.putExtra("forChat", receiverAcitvity);
                    intent.putExtra("userid", userId);
                        startActivityForResult(intent,2000);

                }
            });

        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            //ImageView imageView;
            //TextView windowTag;
            GalleryFolderSquareView folderView;


            //public Drawable temp=null;
            public ViewHolder(View item) {
                super(item);
                //this.imageView = itemView.findViewById(R.id.thumbnail);
                //this.windowTag=itemView.findViewById(R.id.windowTag);
                this.folderView=itemView.findViewById(R.id.wholeView);


            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //if(resultCode==2000)
            finish();
    }
}