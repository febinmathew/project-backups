package com.hash.around.Views;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Shader;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.hash.around.R;

/**
 * Created by Febin on 7/21/2018.
 */

public class TestView1 extends View {
    public TestView1(Context context) {
        super(context);
        init();
    }
    private int[]   colors;
    public TestView1(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        init();

    }

    public TestView1(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public TestView1(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }
    private Paint  paint;
    protected final float[] srcPoints = new float[10];
    private void init() {
       // borderWidth = /*a.getDimension(R.styleable.VerticalSlideColorPicker_pickerBorderWidth, */10f;
        int colorsResourceId = /*a.getResourceId(R.styleable.VerticalSlideColorPicker_pickerColors, */R.array.scribble_colors;
        colors      = getContext().getResources().getIntArray(colorsResourceId);


        int strokeSize = getResources().getDimensionPixelSize(R.dimen.scribble_stroke_size);
        Paint borderPaint = new Paint();
        borderPaint.setStrokeWidth(strokeSize);
        borderPaint.setAntiAlias(true);
        borderPaint.setColor(getContext().getResources().getColor(R.color.colorPrimary/*sticker_selected_color*/));


        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);

        Shader gradient = new LinearGradient(0, 20, 0,20, colors, null, Shader.TileMode.CLAMP);
        //Log.e("GGG","colorpallet: color size is  "+colors.length);
        paint.setShader(gradient);

        /*Bitmap newBmp = createBitmap(getLayer(), bitmap);

        // recycle previous bitmap (if not reused) as soon as possible
        if (bitmap != null && bitmap != newBmp && !bitmap.isRecycled()) {
            bitmap.recycle();
        }

        this.bitmap = newBmp;

        float width = bitmap.getWidth();
        float height = bitmap.getHeight();
        */

    }

    @Override
    protected void onDraw(Canvas canvas) {

        float w, h, cx, cy, radius;
        w = getWidth();
        h = getHeight();
        cx = w/2;
        cy = h/2;

        if(w > h){
            radius = h/4;
        }else{
            radius = w/4;
        }

        canvas.drawRect(0, 0, w, h, paint);

        Paint MyPaint = new Paint();
        MyPaint.setStyle(Paint.Style.FILL);

        float shaderCx = cx;
        float shaderCy = cy;
        int shaderColor0 = Color.RED;
        int shaderColor1 = Color.BLUE;
        MyPaint.setAntiAlias(true);
        Shader linearGradientShader;

        linearGradientShader = new LinearGradient(
                0, 0, w, h,
                shaderColor1, shaderColor0, Shader.TileMode.MIRROR);

        MyPaint.setShader(linearGradientShader);
        canvas.drawRect(0, 0, w, h, MyPaint);

        linearGradientShader = new LinearGradient(
                cx, cy, cx+radius, cy+radius,
                shaderColor0, shaderColor1, Shader.TileMode.MIRROR);

        MyPaint.setShader(linearGradientShader);
        canvas.drawCircle(cx, cy, radius, MyPaint);

    };
}
