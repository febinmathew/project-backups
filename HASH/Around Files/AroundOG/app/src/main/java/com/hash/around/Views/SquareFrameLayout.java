package com.hash.around.Views;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Build.VERSION_CODES;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

//import org.thoughtcrime.securesms.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.hash.around.OtherClasses.Interfaces.JobCompletionWithFailureListner;
import com.hash.around.R;
import com.hash.around.Utils.MediaManager;
import com.hash.around.Utils.StringUtils;
import com.hash.around.Views.Glide.GlideApp;

public class SquareFrameLayout extends FrameLayout {

  private final boolean squareHeight;
    private final boolean squareAsWidth;

  @SuppressWarnings("unused")
  public SquareFrameLayout(Context context) {
    this(context, null);
  }

  @SuppressWarnings("unused")
  public SquareFrameLayout(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  @TargetApi(VERSION_CODES.HONEYCOMB) @SuppressWarnings("unused")
  public SquareFrameLayout(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
      //Log.e("GGG","railrail here00");
    if (attrs != null) {
      TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.SquareFrameLayout, 0, 0);
      this.squareHeight = typedArray.getBoolean(R.styleable.SquareFrameLayout_square_height, false);
      this.squareAsWidth=typedArray.getBoolean(R.styleable.SquareFrameLayout_square_width, false);
      typedArray.recycle();
    }
    else {
      this.squareHeight = false;
      this.squareAsWidth=false;
    }

      //Log.e("GGG","railrail here11");
  }
    @Override
    public void setOnClickListener(@Nullable OnClickListener l) {
        //clickableArea.setBackground(getContext().getResources().getDrawable(R.attr.selectableItemBackground));
        clickableArea.setOnClickListener(l);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        //Log.e("GGG","railrail here22");
        initViews();
    }

    public void doubleSelctionSize(int doubleSize)
    {
        FrameLayout checkBoxArea= findViewById(R.id.checkBoxArea);
        //checkBoxArea.measure(checkBoxArea.getMeasuredWidth()*doubleSize,checkBoxArea.getMeasuredHeight()*doubleSize);
        checkBoxArea.setLayoutParams(new LayoutParams(
                (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, getResources().getDisplayMetrics()), (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, getResources().getDisplayMetrics())));
        //checkBoxArea.measure(checkBoxArea.getMeasuredWidth()*doubleSize,checkBoxArea.getMeasuredHeight()*doubleSize);

    }
  ImageView itemSelection;
  FrameLayout checkBoxArea;
  JobCompletionWithFailureListner listner;
    RelativeLayout clickableArea;
  boolean selection;

 public  void setJobListner(JobCompletionWithFailureListner listnerTemp)
  {
    this.listner=listnerTemp;
  }

  public void setChecked(boolean checked)
  {
    this.itemSelection.setVisibility(checked?VISIBLE:GONE);
    selection=checked;
  }

  public void disableSelection()
  {
      if(this.checkBoxArea!=null)
          this.checkBoxArea.setVisibility(GONE);
  }

  public void initViews()
  {

      clickableArea=findViewById(R.id.clickableArea);
      typeIndicator=findViewById(R.id.typeIndicator);
      thumbnail=findViewById(R.id.thumbnail);
    this.itemSelection = findViewById(R.id.itemSelection);
    this.checkBoxArea = findViewById(R.id.checkBoxArea);
      sizeText= findViewById(R.id.sizeText);


    this.checkBoxArea.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        if(listner!=null)
        {
            //selection=!selection;
          if(selection) {
              //selection=false;
            //itemSelection.setVisibility(INVISIBLE);
            listner.onJobFailed("");
          }
          else {
              //selection=true;
            //itemSelection.setVisibility(VISIBLE);
            listner.onJobCompletion("");
          }

        }
        invalidate();
      }
    });

    //Log.e("GGG","railrail here00");

  }
  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    //noinspection SuspiciousNameCombination
    if (squareHeight) super.onMeasure(heightMeasureSpec, heightMeasureSpec);
    else              super.onMeasure(widthMeasureSpec, widthMeasureSpec);


  }
    ImageView typeIndicator,thumbnail;
    TextView sizeText;
    public void setMediaDetails(Context context, Integer type, Uri uri, long mediaSize)
    {
        if(type==-1)
            typeIndicator.setVisibility(GONE);
        else{
            typeIndicator.setVisibility(VISIBLE);
            typeIndicator.setImageResource(MediaManager.getIconForResourseType(type));}

        thumbnail.setImageDrawable(null);
        Glide.with(context).load(uri)
                .thumbnail(0.4f)
                .into(thumbnail);

        /*GlideApp.with(this).load(uri)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .fitCenter()
                .into(thumbnail);*/

        if(mediaSize==-1)
            sizeText.setVisibility(GONE);
        else
            sizeText.setText(StringUtils.mediaSizeToString(mediaSize));
    }
}
