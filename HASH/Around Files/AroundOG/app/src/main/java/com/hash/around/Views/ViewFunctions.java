package com.hash.around.Views;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;

import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.R;

/**
 * Created by Febin on 5/20/2018.
 */

public class ViewFunctions {
    public static void rotateAnimation(final Context context, final View v, final float startDegree, final float endDegree, int duration)
    {
        /*if(startDegree==endDegree)
            return;*/
        Log.e("MGG","start is: "+startDegree+" end is: "+endDegree);
        RotateAnimation rotate = new RotateAnimation(startDegree, endDegree, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(duration);
        rotate.setStartOffset(0);
        //rotate.setDuration(500);
        rotate.setFillAfter(true);
        rotate.setInterpolator(context, android.R.anim.decelerate_interpolator);

        rotate.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
               // rotateAnimation(context,v,endDegree,startDegree,1);
                //v.setRotation(endDegree);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        //ImageView image= (ImageView) findViewById(R.id.imageView);

        v.startAnimation(rotate);
    }

    public static void animateButtonBurstIn(final View button, int delay,boolean fromTop) {

        AnimationSet animation = new AnimationSet(true);
        Animation scale = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, fromTop?0.0f:1.0f);
        button.setVisibility(View.VISIBLE);
        animation.addAnimation(scale);
        animation.setInterpolator(new OvershootInterpolator(1));
        animation.setDuration(300);
        animation.setStartOffset(delay);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        button.startAnimation(animation);
    }
    public static void animateButtonBurstOut(final View button, int delay,boolean fromTop) {

        AnimationSet animation = new AnimationSet(true);
        Animation scale = new ScaleAnimation(1.0f,0.0f, 1.0f, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, fromTop ? 0.0f : 1.0f);
        button.setVisibility(View.GONE);
        animation.addAnimation(scale);
        animation.setInterpolator(new OvershootInterpolator(1));
        animation.setDuration(300);
        animation.setStartOffset(delay);
        button.startAnimation(animation);
    }
    public static void animateViewScale(final View view, float scale, Boolean toTop) {

        AnimationSet animation = new AnimationSet(true);
        Animation scaling = new ScaleAnimation(view.getScaleX(),scale, view.getScaleY(), scale,
        Animation.RELATIVE_TO_SELF,toTop==null?0.5f:( toTop ? 0.0f : 1.0f), Animation.RELATIVE_TO_SELF, toTop==null?0.5f:( toTop ? 0.0f : 1.0f));

        view.setScaleX(scale);
        view.setScaleY(scale);
        animation.addAnimation(scaling);
        animation.setInterpolator(new OvershootInterpolator(1));
        animation.setDuration(300);
        view.startAnimation(animation);
    }


    public static  void animateOut(final @NonNull View view, final @NonNull Animation animation, final int visibility) {
        //final SettableFuture future = new SettableFuture();
        view.setVisibility(visibility);
            view.clearAnimation();
            animation.reset();
            animation.setStartTime(0);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {}

                @Override
                public void onAnimationRepeat(Animation animation) {}

                @Override
                public void onAnimationEnd(Animation animation) {

                    //future.set(true);
                }
            });
            view.startAnimation(animation);

        //return future;
    }

    public static void animateIn(final @NonNull View view, final @NonNull Animation animation) {
        if (view.getVisibility() == View.VISIBLE) return;

        view.clearAnimation();
        animation.reset();
        animation.setStartTime(0);
        view.setVisibility(View.VISIBLE);
        view.startAnimation(animation);
    }

    public static  void independantAnimateOut(final @NonNull View view, Context context) {
        //final SettableFuture future = new SettableFuture();

        Animation animation=AnimationUtils.loadAnimation(context, R.anim.toggle_animation_out);
        animation.setInterpolator(new FastOutSlowInInterpolator());

        view.clearAnimation();
        animation.reset();
        animation.setStartTime(0);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationRepeat(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.GONE);
                //future.set(true);
            }
        });
        view.startAnimation(animation);

        //return future;
    }
    public static void independantAnimateIn(final @NonNull View view,Context context) {
        if (view.getVisibility() == View.VISIBLE) return;

        Animation animation= AnimationUtils.loadAnimation(context, R.anim.toggle_animation_in);
        animation.setInterpolator(new FastOutSlowInInterpolator());
        view.clearAnimation();
        animation.reset();
        animation.setStartTime(0);
        view.setVisibility(View.VISIBLE);
        view.startAnimation(animation);
    }
    public static void independantScaleIn(final View view,boolean yAxis,boolean fromTop) {
        if (view.getVisibility() == View.VISIBLE) return;

        AnimationSet animation = new AnimationSet(true);
        Animation scale = new ScaleAnimation(yAxis?1f:0.0f, 1.0f, yAxis?0.0f:1f, 1.0f,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, fromTop?0.0f:1.0f);
        view.setVisibility(View.VISIBLE);
        animation.addAnimation(scale);
        //animation.setInterpolator(new OvershootInterpolator(1));
        animation.setDuration(300);
        animation.setStartOffset(0);
        view.startAnimation(animation);
    }

    public static void independantScaleOut(final View button,boolean yAxis,boolean fromTop) {

        AnimationSet animation = new AnimationSet(true);
        Animation scale = new ScaleAnimation(1.0f,1.0f, 1.0f, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 1.0f);
        //button.setVisibility(View.GONE);
        animation.addAnimation(scale);
        //animation.setInterpolator(new OvershootInterpolator(1));
        animation.setDuration(300);
        //animation.setStartOffset(0);
        button.startAnimation(animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationRepeat(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                button.setVisibility(View.GONE);
                //future.set(true);
            }
        });
        //button.setVisibility(View.GONE);
    }
    public static void hideAnyView(final @NonNull View view)
    {

        float     offset    = 0;
        Animation animation = new TranslateAnimation(Animation.ABSOLUTE, offset,
                Animation.ABSOLUTE, offset,
                Animation.RELATIVE_TO_SELF, 0,
                Animation.RELATIVE_TO_SELF, 0);

        animation.setDuration(0);
        animation.setFillAfter(true);
        animation.setFillBefore(true);

        view.startAnimation(animation);

    }

    public static void fadeOut(final @NonNull View view) {
        view.clearAnimation();
        final Animation anim = new AlphaAnimation(1, 0);
        anim.setInterpolator(new FastOutSlowInInterpolator());
        anim.setDuration(150);
        animateOut(view, anim, View.INVISIBLE);
    }

    public static void fadeIn(final @NonNull View view) {
        Log.e("MGG","fade in");

        final Animation anim = new AlphaAnimation(0, 1);
        anim.setInterpolator(new FastOutSlowInInterpolator());
        anim.setDuration(150);
        animateOut(view, anim, View.VISIBLE);

    }

    public static void scaleView(View v, float startScale, float endScale) {

        v.setVisibility(View.VISIBLE);
        float translation = ViewCompat.getLayoutDirection(v) ==
                ViewCompat.LAYOUT_DIRECTION_LTR ? -.25f : .25f;
        AnimationSet animation = new AnimationSet(true);
       /* animation.addAnimation(new TranslateAnimation(Animation.RELATIVE_TO_SELF, translation,
                Animation.RELATIVE_TO_SELF, translation,
                Animation.RELATIVE_TO_SELF, -.25f,
                Animation.RELATIVE_TO_SELF, -.25f));*/


        animation.addAnimation(new ScaleAnimation(
                startScale, endScale, // Start and end values for the X axis scaling
                startScale, endScale, // Start and end values for the Y axis scaling
                Animation.RELATIVE_TO_SELF, 0.5f, // Pivot point of X scaling
                Animation.RELATIVE_TO_SELF, 0.5f)); // Pivot point of Y scaling
        animation.setFillAfter(true); // Needed to keep the result of the animation
        animation.setFillBefore(true);
        animation.setInterpolator(new OvershootInterpolator());
        animation.setDuration(200);
        v.startAnimation(animation);
    }


    public static void hideView(View view, float x, final JobCompletionListner listner) {
        //lastPositionX = x;

       // float offset          = getOffset(view,x);
        //int   widthAdjustment = getWidthAdjustment(view);

        AnimationSet animation = new AnimationSet(false);
        Animation scaleAnimation = new ScaleAnimation(2f, 1f, 2f, 1f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);

      /*  Animation translateAnimation = new TranslateAnimation(Animation.ABSOLUTE, offset + widthAdjustment,
                Animation.ABSOLUTE, widthAdjustment,
                Animation.RELATIVE_TO_SELF, -.25f,
                Animation.RELATIVE_TO_SELF, -.25f);*/

        scaleAnimation.setInterpolator(new AnticipateOvershootInterpolator(1.5f));
        //translateAnimation.setInterpolator(new DecelerateInterpolator());
        animation.addAnimation(scaleAnimation);
       // animation.addAnimation(translateAnimation);
        animation.setDuration(200);
        animation.setFillBefore(true);
        animation.setFillAfter(false);
        animation.setInterpolator(new AnticipateOvershootInterpolator(1.5f));

        //view.setVisibility(View.INVISIBLE);
        view.clearAnimation();
        view.startAnimation(animation);

        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                    listner.onJobCompletion("");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }
    public static  void slideDown(final @NonNull View view, Context context, @Nullable Integer duration, final JobCompletionListner listner) {
        //final SettableFuture future = new SettableFuture();

        Animation animation=AnimationUtils.loadAnimation(context, R.anim.slide_down_anim);
        //animation.setInterpolator(new FastOutSlowInInterpolator());
        animation.setInterpolator(new OvershootInterpolator(1));
        view.clearAnimation();
        animation.reset();
        animation.setStartTime(0);
        if(duration!=null)
            animation.setDuration(duration);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationRepeat(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.VISIBLE);
                if(listner!=null)
                    listner.onJobCompletion("");
                //future.set(true);
            }
        });
        view.startAnimation(animation);

        //return future;
    }
    public static  void slideUp(final @NonNull View view, Context context, @Nullable Integer duration, final JobCompletionListner listner) {
        //final SettableFuture future = new SettableFuture();

        Animation animation=AnimationUtils.loadAnimation(context, R.anim.slide_up_anim);
        //animation.setInterpolator(new FastOutSlowInInterpolator());
        animation.setInterpolator(new OvershootInterpolator(1));
        view.clearAnimation();
        animation.reset();
        animation.setStartTime(0);
        if(duration!=null)
            animation.setDuration(duration);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationRepeat(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.GONE);
                if(listner!=null)
                    listner.onJobCompletion("");
                //future.set(true);
            }
        });
        view.startAnimation(animation);

        //return future;
    }

   /* public static  void slideLeft(final @NonNull View view, Context context, @Nullable Integer duration, final JobCompletionListner listner) {
        //final SettableFuture future = new SettableFuture();

        Animation animation=AnimationUtils.loadAnimation(context, R.anim.slide_left);
        animation.setInterpolator(new FastOutSlowInInterpolator());

        view.clearAnimation();
        animation.reset();
        animation.setStartTime(0);
        if(duration!=null)
        animation.setDuration(duration);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationRepeat(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.INVISIBLE);
                if(listner!=null)
                listner.onJobCompletion("");
                //future.set(true);
            }
        });
        view.startAnimation(animation);

        //return future;
    }*/

    public static  void AIanimation(final @NonNull View view, Context context, int animation2,  Integer duration, @Nullable Integer delay, final int visibility) {

       // if(visibility==View.VISIBLE)
            view.setVisibility(View.VISIBLE);
        Animation animation=AnimationUtils.loadAnimation(context, animation2);
        animation.setInterpolator(new FastOutSlowInInterpolator());
        //animation.setInterpolator(new OvershootInterpolator(1));
        view.clearAnimation();
        animation.reset();
        animation.setStartTime(delay);
        if(duration!=null)
            animation.setDuration(duration);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationRepeat(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(visibility);
            }
        });
        view.startAnimation(animation);
    }


      /*  public static  void slideRight(final @NonNull View view, Context context, @Nullable Integer duration, final JobCompletionListner listner) {
        //final SettableFuture future = new SettableFuture();

        Animation animation=AnimationUtils.loadAnimation(context, R.anim.slide_down_anim);
        animation.setInterpolator(new FastOutSlowInInterpolator());

        view.clearAnimation();
        animation.reset();
        animation.setStartTime(100);
        if(duration!=null)
            animation.setDuration(duration);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationRepeat(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.INVISIBLE);
                if(listner!=null)
                    listner.onJobCompletion("");
                //future.set(true);
            }
        });
        view.startAnimation(animation);

        //return future;
    }*/


}
