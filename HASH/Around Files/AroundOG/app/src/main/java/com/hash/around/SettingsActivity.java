package com.hash.around;


import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.preference.ListPreference;
import android.support.v14.preference.MultiSelectListPreference;
import android.support.v7.preference.Preference;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.preference.SwitchPreferenceCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.Utils.AppOnlineStatusManager;
import com.hash.around.Utils.LocationUtils;
import com.hash.around.Utils.NetworkUtils;
import com.hash.around.Utils.PrefManager;
import com.hash.around.Utils.UserManager;
import com.hash.around.Views.CustomMakers.AroundListPreferenceView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;


public class SettingsActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener{


int stat;
    SettingsPage setingspage;
static boolean languageOnly;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);



        stat=getIntent().getIntExtra("stat",0);
        //Toast.makeText(this, stat+"", Toast.LENGTH_SHORT).show();
        setContentView(R.layout.individual_settings_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
       // getSupportActionBar().setTitle("Settings");
        //getSupportActionBar().setTitle(getString(R.string.notification_settings));
        FragmentManager fragmentManager= getFragmentManager();

        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();

        switch (stat)
        {
            case 0:
                getSupportActionBar().setTitle(getString(R.string.notification));
                break;
            case 1:
                getSupportActionBar().setTitle(getString(R.string.account_privacy_settings));
                break;
            case 2:
                languageOnly=getIntent().getBooleanExtra("languageonly",false);
                if(languageOnly)
                    getSupportActionBar().setTitle(getString(R.string.choose_language));
                else
                    getSupportActionBar().setTitle(getString(R.string.data_usage_settings));

                break;
            case 3:
                getSupportActionBar().setTitle(getString(R.string.feed_settings));
                break;
            case 4:
                getSupportActionBar().setTitle(getString(R.string.chat_settings));
                break;

        }

        SettingsPage.status=stat;
        setingspage= new SettingsPage();
        //SettingsPage page=setingspage;

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.linearlay, setingspage)
                .commit();
        //fragmentTransaction.add(R.id.linearlay,page);
       // fragmentTransaction.commit();




    }
    @Override
    protected void onStart() {
        super.onStart();
        AppOnlineStatusManager.OnlineStatus.setActive(true);

    }
    @Override
    public void onStop() {
        AppOnlineStatusManager.OnlineStatus.setActive(false);
        super.onStop();
    }


    protected void onResume() {
        super.onResume();
        setingspage.getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(setingspage);
    }

    protected void onPause() {
        super.onPause();
        setingspage.getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(setingspage);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Log.e("GGG","shared pref called");
        recreate();
    }

    public static class SettingsPage extends PreferenceFragmentCompat implements
            SharedPreferences.OnSharedPreferenceChangeListener
    {

        SharedPreferences sp;
        ListPreference editListPref;
        MultiSelectListPreference editMultiListPref;
        SwitchPreferenceCompat editSwitchPref;
        Preference editPref;
        AroundListPreferenceView editAroundListPref;
        Set<String> selections;
        String[] selected;

        DatabaseReference databaseRef;
        public static int status;
        FirebaseUser userdetail;


        void setRingTone(String key,String notificationUri)
        {
            editPref=findPreference(key);
           String title;
            if(notificationUri!=null && !notificationUri.equals("") ){
                Ringtone ringtone = RingtoneManager.getRingtone(getActivity(), Uri.parse(notificationUri));
                title = ringtone.getTitle(getContext());

                title = Uri.parse(notificationUri).equals(Settings.System.DEFAULT_NOTIFICATION_URI) ? getString(R.string.default_text) : title;

                //Log.e("MGG", "notificationUri " + Uri.parse(notificationUri));
                //Log.e("MGG", "DEFAULT_NOTIFICATION_URI  " + Settings.System.DEFAULT_NOTIFICATION_URI);
                // Ringtone ringtone=notificationSummery

            }
            else
            {
                title=getString(R.string.default_text);
            }
            editPref.setSummary(title);
        }
        HashMap<String,String> currentPrefs=new HashMap<>();
        HashMap<String,Boolean> currentPrefsBool=new HashMap<>();
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey)

        {
            userdetail = FirebaseAuth.getInstance().getCurrentUser();

            switch(status)
            {
                case 0:
                    //getActivity().getActionBar().setTitle("Notification settings");
                    addPreferencesFromResource(R.xml.notification_settings);

                    String notificationUri=PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("pref_chat_notification_sound",null);
                    setRingTone(PrefManager.PREF_CHAT_NOTIFICATION_SOUND,notificationUri);

                    editAroundListPref=(AroundListPreferenceView) findPreference("pref_chat_vibration");
                    editAroundListPref.setSummary(editAroundListPref.getEntry());
                    editAroundListPref=(AroundListPreferenceView) findPreference("pref_chat_notification_light");
                    editAroundListPref.setSummary(editAroundListPref.getEntry());

                    break;
                case 1:
                    addPreferencesFromResource(R.xml.privacy_settings);

                    sp = getPreferenceScreen().getSharedPreferences();
                    // sp.getString("key","deaful_tstring");


                    editAroundListPref = (AroundListPreferenceView) findPreference("profile_image_privacy");
                    editAroundListPref.setSummary(editAroundListPref.getEntry());
                    currentPrefs.put("profile_image_privacy",editAroundListPref.getValue());

                    editAroundListPref = (AroundListPreferenceView) findPreference("profile_status_privacy");
                    editAroundListPref.setSummary(editAroundListPref.getEntry());
                    currentPrefs.put("profile_status_privacy",editAroundListPref.getValue());

                    editAroundListPref = (AroundListPreferenceView) findPreference("profile_number_privacy");
                    editAroundListPref.setSummary(editAroundListPref.getEntry());
                    currentPrefs.put("profile_number_privacy",editAroundListPref.getValue());

                    editAroundListPref = (AroundListPreferenceView) findPreference("profile_activity_status_privacy");
                    editAroundListPref.setSummary(editAroundListPref.getEntry());
                    currentPrefs.put("profile_activity_status_privacy",editAroundListPref.getValue());

                    editAroundListPref = (AroundListPreferenceView) findPreference("profile_music_privacy");
                    editAroundListPref.setSummary(editAroundListPref.getEntry());
                    currentPrefs.put("profile_music_privacy",editAroundListPref.getValue());

                    editAroundListPref = (AroundListPreferenceView) findPreference("profile_location_privacy");
                    editAroundListPref.setSummary(editAroundListPref.getEntry());
                    currentPrefs.put("profile_location_privacy",editAroundListPref.getValue());

                    editSwitchPref =(SwitchPreferenceCompat) findPreference("anyonefollow");
                    currentPrefsBool.put("anyonefollow",editSwitchPref.isChecked());

                    editSwitchPref =(SwitchPreferenceCompat) findPreference("anyonemessage");
                    currentPrefsBool.put("anyonemessage",editSwitchPref.isChecked());

                    break;
                case 2:
                    addPreferencesFromResource(R.xml.data_usage2);
                    sp = getPreferenceScreen().getSharedPreferences();



                    editAroundListPref = (AroundListPreferenceView) findPreference("pref_language");
                    editAroundListPref.setSummary(editAroundListPref.getEntry());

                    if(!languageOnly) {
                        editMultiListPref = (MultiSelectListPreference) findPreference("data_usage_chat");

                        selections = sp.getStringSet("data_usage_chat", null);
                        selected = selections.toArray(new String[]{});
                        editMultiListPref.setSummary(customStringMaker(selected, R.array.data_usage_chat_entry) + "");

                        //CODE X- CHNAGE data_usage_feedd to data_usage_feed
                        editMultiListPref = (MultiSelectListPreference) findPreference("data_usage_feedd");
                        selections = sp.getStringSet("data_usage_feedd", null);
                        selected = selections.toArray(new String[]{});
                        // Toast.makeText(getActivity().getApplicationContext(), selected[0]+selected[1]+selected[2]+selected[3]+selected[4]+selected[5]+selected[6], Toast.LENGTH_SHORT).show();
                        editMultiListPref.setSummary(customStringMaker(selected, R.array.data_usage_feed_entry) + "");
                    }
                    else
                    {
                        findPreference("data_usage_chat").setVisible(false);
                        findPreference("data_usage_feedd").setVisible(false);
                        findPreference("media_autodownload_tag").setVisible(false);
                        //findPreference("media_autodownload_tag_divider").setVisible(false);

                    }


                    break;
                case 3:
                    addPreferencesFromResource(R.xml.feed_settings);
                    sp = getPreferenceScreen().getSharedPreferences();
                    editMultiListPref = (MultiSelectListPreference) findPreference("filter_feed");
                    selections =sp.getStringSet("filter_feed",null);
                    selected = selections.toArray(new String[] {});
                    editMultiListPref.setSummary(customStringMaker(selected,R.array.feed_filter_options)+ "");

                    editMultiListPref = (MultiSelectListPreference) findPreference("pref_filter_feed_notification");
                    selections =sp.getStringSet("pref_filter_feed_notification",null);
                    selected = selections.toArray(new String[] {});
                    editMultiListPref.setSummary(customStringMaker(selected,R.array.feed_filter_options)+ "");

                    editAroundListPref=(AroundListPreferenceView) findPreference("pref_feed_font_size");
                    editAroundListPref.setSummary(editAroundListPref.getEntry());

                    break;
                case 4:
                    addPreferencesFromResource(R.xml.preference_chat);
                    sp = getPreferenceScreen().getSharedPreferences();
                    editAroundListPref=(AroundListPreferenceView) findPreference("pref_chat_font_size");
                    editAroundListPref.setSummary(editAroundListPref.getEntry());
                    break;
            }
        }


        String customStringMaker(String[] selections,int stringResource)
        {
            String[] arrayValues = getResources().getStringArray(stringResource);
            String text=new String();
            text="";
            //Object[] selectedItems=selections.toArray();
            ArrayList<Integer> data=new ArrayList<Integer>();

            for (int i=0;i<selections.length;i++)
            {
                try
                {
                    int xx=Integer.parseInt(selections[i]);
                    data.add(xx);
                }
                catch(Exception e)
                {
                    //break;
                }
            }

            for (int i=data.size()-1;i>=0;i--)
            {
                if(text.isEmpty()) {
                    text += "Choosen : ";
                }
                if(i<(data.size()-1))
                {
                    text +=", ";
                }

                text += arrayValues[data.get(i)];
            }
            if(text.isEmpty())
            {
                text="None Selected";
            }
            //Toast.makeText(getActivity().getApplicationContext(), text, Toast.LENGTH_SHORT).show();
            return text;
        }


        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            // findPreference(key).setSummary(sharedPreferences.getString(key, "Some Default Text"));
            Preference pref = findPreference(key);
            if (pref instanceof ListPreference)
            {
                ListPreference etp = (ListPreference) pref;
                etp.setSummary(etp.getEntry());
                String content;//=new String();
                String contentKey=sharedPreferences.getString(key, "0");
            switch(key)
            {
                case "profile_image_privacy":
                    content="profile_picture";
                    userPrivacyUpdate(contentKey,content,key);
                    break;
                case "profile_status_privacy":
                    content="profile_status";
                    userPrivacyUpdate(contentKey,content,key);
                    break;
                case "profile_number_privacy":
                    content="number";
                    userPrivacyUpdate(contentKey,content,key);
                    break;
                case "profile_activity_status_privacy":
                    content="activity_status";
                    userPrivacyUpdate(contentKey,content,key);
                    break;
                case "profile_location_privacy":
                    content="location";
                    userPrivacyUpdate(contentKey,content,key);
                    break;
                case "profile_music_privacy":
                    content="profile_music";
                    userPrivacyUpdate(contentKey,content,key);
                    break;
                case "pref_language":
                   // String languageCode=sharedPreferences.getString(key, "zz");
                    Log.e("GGG","language select "+sharedPreferences.getString(key, "zz"));
                    LocationUtils.setContextLocale(getActivity(),LocationUtils.getSelectedLocaleFromLanguage(getActivity()));
                    //recreate();
                    getActivity().recreate();
                    break;
                default:
                    content="";



            }
            }

            else if (pref instanceof MultiSelectListPreference)
            {
                MultiSelectListPreference etp = (MultiSelectListPreference) pref;

                switch(key) {
                    case "filter_feed":
                    case "pref_filter_feed_notification":
                        selections = sp.getStringSet(key, null);
                        selected = selections.toArray(new String[]{});
                        etp.setSummary(customStringMaker(selected,R.array.feed_filter_options) + "");
                        break;
                    case "data_usage_feed":
                        selections = sp.getStringSet(key, null);
                        selected = selections.toArray(new String[]{});
                        etp.setSummary(customStringMaker(selected,R.array.data_usage_feed_entry) + "");
                        break;
                    default:
//yyy
                        selections = sp.getStringSet(key, null);
                        selected = selections.toArray(new String[]{});
                        etp.setSummary(customStringMaker(selected,R.array.data_usage_chat_entry) + "");
                }

            }

            else if (pref instanceof SwitchPreferenceCompat)
            {
                SwitchPreferenceCompat etp = (SwitchPreferenceCompat) pref;
                //etp.setSummary(etp.isChecked());

                switch(key)
                {
                    case "anyonefollow":

                        userPrivacyUpdate(etp.isChecked()?"0":"2","anyonefollow",key);

                        break;
                    case "anyonemessage":
                        userPrivacyUpdate(etp.isChecked()?"0":"2","anyonemessage",key);
                        break;
                }
            }
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {

            if(resultCode==RESULT_OK && requestCode==1)
            {
                Uri uri = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
                if(uri!=null)
                {
                    PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putString("pref_chat_notification_sound",uri.toString()).clear().commit();
                    setRingTone(PrefManager.PREF_CHAT_NOTIFICATION_SOUND,uri.toString());
                    //notificationUri=uri.toString();
                    //updateData();
                    //UserManager.updateUserChatNotification(userId,uri.toString(),null,null,null);
                }
            }
            super.onActivityResult(requestCode, resultCode, data);

        }
        @Override
        public boolean onPreferenceTreeClick(Preference preference) {
            if (preference.getKey().equals("pref_chat_notification_sound")) {
                Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_NOTIFICATION);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, true);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_DEFAULT_URI, Settings.System.DEFAULT_NOTIFICATION_URI);

               String notificationUri =PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                       "pref_chat_notification_sound",null);
               if (notificationUri != null && !notificationUri.equals("")) {
                    intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, Uri.parse(notificationUri));
                } else {
                    intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, Settings.System.DEFAULT_NOTIFICATION_URI);
                }

                startActivityForResult(intent, 1);


/*AroundAlertBoxManager.showRingtoneDialog(getActivity(), new AroundAlertBoxManager.MuteSelectionListener() {
    @Override
    public void onMuted(long until, boolean notificationAlso, String userID) {

    }
});*/


                return true;
            }
            else if (preference.getKey().equals("pref_chat_auto_delete_now"))

            {
                MainServiceThread.mainService.updateFeedPersistanceAndChatPersistance(1, new JobCompletionListner() {
                    @Override
                    public void onJobCompletion(String txt1) {
                        Toast.makeText(getContext(), txt1, Toast.LENGTH_SHORT).show();
                    }
                });
                return true;
            }
            else if (preference.getKey().equals("pref_feed_auto_delete_now"))
            {
                MainServiceThread.mainService.updateFeedPersistanceAndChatPersistance(0, new JobCompletionListner() {
                    @Override
                    public void onJobCompletion(String txt1) {
                        Toast.makeText(getContext(), txt1, Toast.LENGTH_SHORT).show();
                    }
                });
                return true;
            }
            return super.onPreferenceTreeClick(preference);
        }

        void nearByPeoplePrivilageUpdate(Set<String> valuess)
        {
            String finaltext="";
            String[] values=valuess.toArray(new String[]{});

            for (int i=0;i<values.length;i++)
            {
                if(values[i].length()==1) {
                    finaltext += values[i];
                    Log.e("MSGG", values[i]);
                }

            }
            databaseRef = FirebaseDatabase.getInstance().getReference().child("user_profile").child(userdetail.getUid()).child("user_privacy");
            databaseRef.child("nearby_user").setValue(finaltext).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {


                    }

                }
            });
        }

        void setResetPref(String key,boolean reset)
        {

            if(key!=null) {
                Preference pref = findPreference(key);
                if (pref instanceof ListPreference) {


                    ListPreference etp = (ListPreference) pref;
                    etp.setSummary(etp.getEntry());

                    if (reset)
                        etp.setValue(currentPrefs.get(key));
                    else
                        currentPrefs.put(key, etp.getValue());

                } else if (pref instanceof SwitchPreferenceCompat) {
                    SwitchPreferenceCompat etp = (SwitchPreferenceCompat) pref;

                    if (reset)
                        etp.setChecked(currentPrefsBool.get(key));
                    else
                        currentPrefsBool.put(key, etp.isChecked());

                }

            }
        }

        public void userPrivacyUpdate(String value,String contentText,final String key)
        {

            if(!contentText.equals(""))
            {
                AroundAlertBoxManager.showProgress(getActivity(),null,R.string.updating_privacy,true);

                if(!NetworkUtils.networkIsAvailable(getActivity()))
                {
                    setResetPref(key,true);
                }

                //Log.e("GGG"," prefrevert EXE "+currentPrefs.get("profile_image_privacy"));
                FirebaseDatabase.getInstance().getReference().child("user_profile").child(userdetail.getUid()).child("user_privacy")/*databaseRef*/.child(contentText).setValue(value).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        //Log.e("GGG"," prefrevert COMPLETE "+currentPrefs.get("profile_image_privacy")+"");
                    }
                }).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        AroundAlertBoxManager.cancelProgress();
                        setResetPref(key,false);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        setResetPref(key,true);
                        AroundAlertBoxManager.cancelProgress();
                        Toast.makeText(getActivity(), R.string.failed_to_update_profile, Toast.LENGTH_SHORT).show();
                    }
                });

            }
        }



    }
}