package com.hash.around.OtherClasses.DataTypes;

import android.util.Log;

import com.hash.around.Utils.ByteUtils;
import com.hash.around.Utils.StringUtils;

import java.io.Serializable;

public class ChatReplayData implements Serializable{


   public String userID;
   public String userName;
    public String description;
    public String contentUri;
    public int type;
    public long content_sl=-1;
    public String content_ID="null";


    public ChatReplayData()
   {}

    public static ChatReplayData getReplayDataFromFeedData(GroupFeedData feed)
    {
        ChatReplayData dat=new ChatReplayData();

        dat.userName=feed.username;
        dat.userID=feed.userid;
        dat.type=2;
        dat.content_sl=feed.feedRow;
        dat.content_ID=feed.feedID;

        String type= StringUtils.convertMediaTypeToString(feed.type);
        dat.description=(!type.equals("")?"("+type+") ":"")+feed.text;
        //code 120
        if(feed.semi_uri.contains("http")|| feed.semi_uri.length()<5)
            dat.contentUri="null";
        else
            dat.contentUri= ByteUtils.byteToString(ByteUtils.bitmapToByteArray(ByteUtils.getBitmapFromuri(feed.semi_uri),20));


        Log.e("GGG","chatreplay string is>> "+dat.contentUri);
        return dat;
    }


}
