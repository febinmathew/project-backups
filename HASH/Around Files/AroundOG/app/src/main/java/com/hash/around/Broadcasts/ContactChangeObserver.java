package com.hash.around.Broadcasts;

import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;

/**
 * Created by Febin on 8/2/2018.
 */

public class ContactChangeObserver extends ContentObserver {
    /**
     * Creates a content observer.
     *
     * @param handler The handler to run {@link #onChange} on, or null if none.
     */
    Context mContext;
    public ContactChangeObserver(Handler handler,Context context) {

        super(handler);
        mContext=context;
    }
    @Override
    public void onChange(boolean selfChange) {
        this.onChange(selfChange, null);
        Log.e("GGG","change change2 change");
    }

    @Override
    public void onChange(boolean selfChange, Uri uri) {
        Log.e("GGG","change change3 change");
        Intent intent = new Intent("GET_CONTACTS_UPDATED");
        mContext.sendBroadcast(intent);
    }
}
