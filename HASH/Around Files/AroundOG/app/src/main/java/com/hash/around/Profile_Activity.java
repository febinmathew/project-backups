package com.hash.around;

import android.*;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Point;
import android.location.Location;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.SwitchPreferenceCompat;
import android.support.v7.widget.AppCompatSeekBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.hash.around.OtherClasses.Adapter_GroupMembers;
import com.hash.around.OtherClasses.Adapter_NoificationData;
import com.hash.around.OtherClasses.DataTypes.ContactListData;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.OtherClasses.DataTypes.GroupMemberData;
import com.hash.around.OtherClasses.Interfaces.FileDownloadListner;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.OtherClasses.DataTypes.NotificationData;
import com.hash.around.OtherClasses.DataTypes.UserPrivacyData;
import com.hash.around.OtherClasses.Interfaces.JobCompletionWithFailureListner;
import com.hash.around.OtherClasses.Interfaces.UserPrivacyStatusListner;
import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.hash.around.Utils.AppOnlineStatusManager;
import com.hash.around.Utils.DatabaseManager;
import com.hash.around.Utils.LocationUtils;
import com.hash.around.Utils.MediaManager;
import com.hash.around.Utils.PermissionManager;
import com.hash.around.Utils.StringUtils;
import com.hash.around.Utils.UserManager;
import com.hash.around.Views.AroundFeedRailView;
import com.hash.around.Views.AroundUserMediaRailView;
import com.hash.around.Views.CircleColorImageView;
import com.hash.around.Views.CustomMakers.AroundListPreferenceView;
import com.hash.around.Views.CustomMakers.AroundPreferenceView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static android.support.v4.graphics.TypefaceCompatUtil.getTempFile;
import static android.view.View.GONE;
import static com.hash.around.ChatWindow.checkGroupOnlineStatusWithListners;
import static com.hash.around.ChatWindow.checkSingleOnlineStatusWithListners;
//import static com.hash.around.MainServiceThread.downloadFileAndReturnUriString;
import static com.hash.around.MainServiceThread.newUserFollowRequestWithDoubleCheck;

import static com.hash.around.MainServiceThread.removeSingleUserFromGroup;

import static com.hash.around.MainServiceThread.updateUserFollowStatus;

import static com.hash.around.OtherClasses.Adapter_Contacts.checkIfPrivacyShouldBeActivie;
import static com.hash.around.TabbedActivity.saveContactMusicToDatabase;
import static com.hash.around.TabbedActivity.userdetail;

public class Profile_Activity extends AppCompatActivity {
    Toolbar toolbar;
    float WindowHeight;
    ScrollView sView;
    static String userId;
    int userType;
    ImageView imgView;
    TextView userStatus,statusHeading,musicTitle,chatsettingsTitle;
    TextView profileName;
    RecyclerView notificationRview;
    Adapter_NoificationData adap;
    Adapter_GroupMembers memberadap;
    private LinearLayoutManager mLayoutManager;
//String defaultProfileImageUri;
    FloatingActionButton userFloatingButtonFollow;
    ImageButton statusEditButton,changeProfileImage,profileNameEdit,bgmusicEditButton;

    ArrayList<NotificationData> finaldata=new ArrayList<>();
    ArrayList<GroupMemberData> memberdata=new ArrayList<>();
    ArrayList<ContactListData> contactdata=new ArrayList<>();


    TextView quitGroup;
    TextView reportButton,blockButton,timeofbgmusic,timeofstatus;
    public Integer userPrivilage=null;
    UserWholeData userData;

   TextView lastSeenText,userLcoationText;
AroundFeedRailView feedRail;
AroundUserMediaRailView sharedMediaRail;

    LinearLayout newMemberSection;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profilepage_activity);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
// DO NOT ever call getBaseContext() unless you know what you're doing. "this" or "getApplicationContext() is perfect. Google the difference.
        String ringtonePreference = prefs.getString("ringtonePref", "DEFAULT_RINGTONE_URI");
// The key of preference was "@string/ringtonePref" which is useless since you're hardcoding the string here anyway.
        Uri ringtoneuri = Uri.parse(ringtonePreference);
        Log.e("MGG","ringtone is "+ringtoneuri);


        toolbar= (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        toolbar.findViewById(R.id.up_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Profile_Activity.super.onBackPressed();
            }
        });

        userId=getIntent().getStringExtra("userid");

       /* FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
       getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //getSupportActionBar().setLogo(R.mipmap.logo1);


        //getSupportActionBar().setTitle("Around");
        //getSupportActionBar().setSubtitle("online");
        //getSupportActionBar().setTitle("Alan");



        Point x= new Point();
        getWindowManager().getDefaultDisplay().getSize(x);
        WindowHeight=x.y;
        //sView= (ScrollView)findViewById(R.id.sScrollView);
        imgView =(ImageView) findViewById(R.id.imgView);
        profileName=(TextView)findViewById(R.id.profileName);
        userStatus=(TextView)findViewById(R.id.userStatus);
        statusHeading=(TextView)findViewById(R.id.statusHeading);
        musicTitle=(TextView)findViewById(R.id.musicTitle);
        chatsettingsTitle=(TextView)findViewById(R.id.chatsettingsTitle);
        notificationRview=(RecyclerView) findViewById(R.id.notificationRview);
        quitGroup=(TextView) findViewById(R.id.quitGroup);
        reportButton=(TextView) findViewById(R.id.reportButton);
        blockButton=(TextView) findViewById(R.id.blockButton);
        userFloatingButtonFollow=(FloatingActionButton)findViewById(R.id.userFloatingButtonFollow);
        profileNameEdit=(ImageButton)findViewById(R.id.profileNameEdit);
        changeProfileImage=(ImageButton)findViewById(R.id.changeProfileImage);
        statusEditButton=(ImageButton)findViewById(R.id.statusEditButton);
        bgmusicEditButton=(ImageButton)findViewById(R.id.bgmusicEditButton);
        lastSeenText=(TextView)findViewById(R.id.lastSeenText);
        timeofbgmusic=findViewById(R.id.timeofbgmusic);
        timeofstatus=findViewById(R.id.timeofstatus);
        userLcoationText=(TextView)findViewById(R.id.userLcoationText);
        feedRail=findViewById(R.id.feedRail);
        sharedMediaRail=findViewById(R.id.sharedMediaRail);
        lastSeenText.setVisibility(View.INVISIBLE);
        userLcoationText.setVisibility(View.INVISIBLE);

        newMemberSection=findViewById(R.id.newMemberSection);

        AppBarLayout mAppBarLayout = (AppBarLayout) findViewById(R.id.appBar);
        mAppBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    isShow = true;
                    //showOption(R.id.action_info);
                    //Log.e("MGG","show now");        // heavy code do something CODE 47
                    if(userData.followType!=1)
                    followItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                } else if (isShow) {
                    isShow = false;
                    Log.e("MGG","disable now");
                    followItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
                    //hideOption(R.id.action_info);
                }
            }
        });


        userData=ChatWindow.returnSingleUserDetails(getApplicationContext(),userId);
        userType=userData.type;
        profileName.setText(userData.user_name);

        Bundle railBundle=new Bundle();
        railBundle.putString("userid",userData.user_id);
        this.getLoaderManager().restartLoader(2, railBundle, sharedMediaRail);

        if(userType==15||userType==16)
            setUpGroupAndChannelProfile();
        else
            setUpUserProfile();


        setFloatingButton();


        statusEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent editText = new Intent(Profile_Activity.this, EditAnyTextActivity.class);
                editText.putExtra("text_type", 1);
                editText.putExtra("userid",userData.user_id);
                startActivityForResult(editText,11);

            }
        });

        profileNameEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent editText = new Intent(Profile_Activity.this, EditAnyTextActivity.class);
                editText.putExtra("text_type", 0);
                editText.putExtra("userid",userData.user_id);
                startActivityForResult(editText,12);
            }
        });

        changeProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!PermissionManager.checkExternalStoragePermission(Profile_Activity.this))
                {
                    return;
                }
                boolean cameraPermission=PermissionManager.checkHasPermission(Profile_Activity.this);

                File photoFile=null;
                try {
                    photoFile= MediaManager.createImageFile(getApplicationContext());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                photoCameraURI = FileProvider.getUriForFile(getApplicationContext(),
                        "com.hash.around.fileprovider",
                        photoFile);
                Intent chooserIntent = CreateNewGroup.createAvatarSelectionIntent(Profile_Activity.this,photoCameraURI, !userData.img_url.equals("null"), cameraPermission);
                startActivityForResult(chooserIntent, 1);
            }
        });

        bgmusicEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //getPickImageIntent(getApplicationContext());
                //Intent intent = new Intent(Intent.ACTION_PICK,
                // android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                Intent chooserIntent = CreateNewGroup.createMusicSelectionIntent(Profile_Activity.this, !userData.music_url.equals("null"));
                startActivityForResult(chooserIntent, 20);

            }
        });

        profile_settings=new ProfileNotificationSettings();
        Bundle args = new Bundle();
        args.putString("notificationUri", userData.not_uri);
        args.putInt("vibration", userData.not_vibration);
        args.putInt("light", userData.not_light);
        args.putSerializable("userData",userData);
        profile_settings.setArguments(args);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.profile_fragment, profile_settings)
                .commit();
        IntentFilter intentFilter = new IntentFilter("GROUP_MEMBER_UPDATED");
        registerReceiver(locationUpdateReceiver, intentFilter);

        updateBlockViews();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(locationUpdateReceiver);
    }

    private BroadcastReceiver locationUpdateReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {



           // fillGroupMemberdetails(userId);
           // memberadap.notifyDataSetChanged();
            if(userType==15||userType==16)
                setUpGroupAndChannelProfile();

        }
    };
    void setUpGroupAndChannelProfile()
    {
        setupGroupInWindow();

        if(userType==15) //group
        {
            profileName.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_group_white),null,null,null);
            quitGroup.setText(R.string.group_exit);
            statusHeading.setText(R.string.group_description);
            musicTitle.setText(R.string.group_music);
            chatsettingsTitle.setText(R.string.group_settings);
            ((TextView)findViewById(R.id.notificationExtraText)).setText(R.string.group_members);
            ((TextView)newMemberSection.findViewById(R.id.newMemberText)).setText(R.string.add_member);
        }
        else if(userType==16)
        {
            profileName.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_speak_white),null,null,null);
            quitGroup.setText(R.string.channel_exit);
            statusHeading.setText(R.string.channel_description);
            musicTitle.setText(R.string.channel_music);
            chatsettingsTitle.setText(R.string.channel_settings);
            ((TextView)findViewById(R.id.notificationExtraText)).setText(R.string.channel_listeners);
            ((TextView)newMemberSection.findViewById(R.id.newMemberText)).setText(R.string.add_listerner);
        }

        if(userPrivilage!=null) {
            if (userPrivilage == 1) {

                newMemberSection.setVisibility(View.VISIBLE);
                newMemberSection.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ArrayList<String> groupMemberList=new ArrayList<>();
                        for (GroupMemberData temp:
                        memberdata) {
                            groupMemberList.add(temp.userid);
                        }

                        Intent intent = new Intent(getApplicationContext(), AllUsersList.class);
                        intent.putExtra("selectedpeople",groupMemberList);
                        intent.putExtra("returnData",true);
                        startActivityForResult(intent,200);
                    }
                });
                defineAccountEditable(true);
            } else{
                newMemberSection.setVisibility(View.GONE);
                defineAccountEditable(false);}

            checkGroupOnlineStatusProfile();
            quitGroup.setVisibility(View.VISIBLE);
            blockButton.setVisibility(View.VISIBLE);
            reportButton.setVisibility(View.VISIBLE);
        }
        else
        {
            newMemberSection.setVisibility(View.GONE);
            defineAccountEditable(false);
            quitGroup.setVisibility(View.GONE);
            blockButton.setVisibility(GONE);
            reportButton.setVisibility(GONE);

        }


        showProfileImage(true);
        showProfileStatus(true);
        showProfileNumber(false);
        showProfileLocation(false,false);
        setUpBgMusicPlayback(true);
        feedRail.setVisibility(GONE);

    }
    void setUpUserProfile()
    {
        newMemberSection.setVisibility(GONE);
        Bundle railBundle=new Bundle();
        railBundle.putString("userid",userData.user_id);
        this.getLoaderManager().restartLoader(1, railBundle, feedRail);

        if(userId.equals(TabbedActivity.userdetail.getUid()) && userType==0)
        { //self user
            defineAccountEditable(true);
            showProfileImage(true);
            showProfileStatus(true);
            showProfileNumber(true);
            showProfileLocation(true,true);
            setUpBgMusicPlayback(true);
            findViewById(R.id.chatSettingsSection).setVisibility(GONE);
            reportButton.setVisibility(GONE);
            blockButton.setVisibility(GONE);
        }
        else//normal user
        {
            defineAccountEditable(false);

            if (checkIfPrivacyShouldBeActivie(userData.priv_profile_picture,userData.remoteContact))
            {
                showProfileImage(true);
            }
            else
            {
                showProfileImage(false);
            }
            if (checkIfPrivacyShouldBeActivie(userData.priv_profile_status,userData.remoteContact))
            {
                showProfileStatus(true);
            }
            else
            {
                showProfileStatus(false);
            }

            if (checkIfPrivacyShouldBeActivie(userData.priv_number,userData.remoteContact))
            {
                showProfileNumber(true);
            }
            else
            {
                showProfileNumber(false);
            }
            if (checkIfPrivacyShouldBeActivie(userData.priv_location,userData.remoteContact))
            {
                showProfileLocation(true,false);
            }
            else
            {
                showProfileLocation(false,false);
            }



            if (checkIfPrivacyShouldBeActivie(userData.priv_profile_music,userData.remoteContact))
            {
                setUpBgMusicPlayback(true);
            }
            else
            {
                setUpBgMusicPlayback(false);
            }

            if(userType!=15 && userType!=16)
                if (checkIfPrivacyShouldBeActivie(userData.priv_activity_status,userData.remoteContact))
                {
                    checkSingleOnlineStatusWithListners(getApplicationContext(),userData.user_id, new JobCompletionListner() {
                        @Override
                        public void onJobCompletion(String txt1) {
                            lastSeenText.setVisibility(View.VISIBLE);
                            lastSeenText.setText(txt1);
                        }
                    });
                }
           /* if (checkIfPrivacyShouldBeActivie(userData.priv_location,userData.remoteContact))
            {
                double lat = Double.parseDouble(TabbedActivity.prefs.getString("location_latitude", "0.0"));
                double lng = Double.parseDouble(TabbedActivity.prefs.getString("location_longitude", "0.0"));


                if(userData.latitude==0 || userData.longitude==0 || lat==0 || lng==0)
                {
                    Log.e("MGG","0000000000000");
                }
                else
                {
                    Location target=new Location("target");
                    target.setLatitude(userData.latitude);
                    target.setLatitude(userData.longitude);
                    Location source=new Location("source");
                    source.setLatitude(lat);
                    source.setLongitude(lng);
                    lastSeenText.setVisibility(View.VISIBLE);
                    lastSeenText.setText(locationDistanceToString(source.distanceTo(target)));
                }
            }*/
        }

        quitGroup.setVisibility(GONE);
        notificationRview.setVisibility(GONE);
        findViewById(R.id.notificationDisplayInfo).setVisibility(GONE);
    }

    void updateBlockViews()
    {
        if(!blockButton.hasOnClickListeners()){
        blockButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                blockButtonHit();
            }
        });
        }
        if(userData.blockType==1)
            blockButton.setText(R.string.unblock_tag);
        else
            blockButton.setText(R.string.block_tag);


    }

    void blockButtonHit()
    {
        if(userData.blockType==1)
        {
            newUserUnBlock(this,userData.user_id, new JobCompletionListner() {
                @Override
                public void onJobCompletion(String txt1) {
                    userData.blockType=0;
                    updateBlockViews();
                    invalidateOptionsMenu();
                }
            });
        }
        else
        {
            newUserBlock(this,userData.user_id, new JobCompletionListner() {
                @Override
                public void onJobCompletion(String txt1) {
                    userData.blockType=1;
                    updateBlockViews();
                    invalidateOptionsMenu();
                }
            });
        }
    }



    ProfileNotificationSettings profile_settings;
    SparseBooleanArray userOnline=new SparseBooleanArray();
    void checkGroupOnlineStatusProfile()
    {
        ArrayList<String> memberList=new ArrayList<>();
       for(int i=0;i<memberdata.size();i++)
       {
           memberList.add(memberdata.get(i).userid);
       }
        checkGroupOnlineStatusWithListners(memberList,userOnline,new JobCompletionListner(){
            @Override
            public void onJobCompletion(String txt1) {
                showGroupOnlineStatus();
            }
        });
    }
    void showGroupOnlineStatus()
    {
        int count=0;
        for(int i=0;i<userOnline.size();i++)
        {
            if(userOnline.get(userOnline.keyAt(i))==true)
            {
                count++;
            }

        }
        lastSeenText.setVisibility(View.VISIBLE);
        lastSeenText.setText(count+" online");
    }

    void showProfileImage(boolean show)
    {
        if (userData.img_url.equals("null") || !show)
        {
            imgView.setImageResource(R.drawable.default_user);
        } else {
            imgView.setImageURI(Uri.parse(userData.img_url));
        }

        if(!imgView.hasOnClickListeners())
        imgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent solophoto=new Intent(Profile_Activity.this,SoloImageViewActivity.class);
                solophoto.setData(Uri.parse(userData.img_url));
                startActivity(solophoto);
            }
        });
    }

    void showProfileStatus(boolean show)
    {
        if (!show)
        {
            findViewById(R.id.statusSection).setVisibility(GONE);
        } else
            {
            if(userData.status.equals("null"))
                userData.status=getResources().getString(R.string.default_user_status);;
            userStatus.setText(userData.status);

            timeofstatus.setText(ChatWindow.convertTimeStampToText(getApplicationContext(),userData.status_timestamp,false));
        }
    }
    void showProfileNumber(boolean show)
    {
        if (!show)
        {
            findViewById(R.id.numberSection).setVisibility(GONE);
        } else
        {
           /* if(userData.status.equals("null"))
                userData.status=getResources().getString(R.string.default_user_status);;*/
           TextView number=(TextView)  findViewById(R.id.userNumber);
            number.setText(userData.number);

        }
    }
    void showProfileLocation(boolean show,boolean selfUser)
    {
        //Log.e("GGG","users locations is "+ userData.user_name+" / "+userData.latitude+" / "+userData.longitude);

        if (!show || (userData.latitude==0 && userData.longitude==0))
        {
            findViewById(R.id.locationViewOnMap).setVisibility(GONE);
            userLcoationText.setVisibility(View.INVISIBLE);
        } else
        {
            if(!selfUser){
                userLcoationText.setVisibility(View.VISIBLE);
                Location checkLocation=new Location("userlocation");
                checkLocation.setLatitude(userData.latitude);
                checkLocation.setLongitude(userData.longitude);

                Location deafultLocation= LocationUtils.getUserLocation(getApplicationContext());
                userLcoationText.setText(StringUtils.locationDistanceToString(deafultLocation.distanceTo(checkLocation)));
            }

            findViewById(R.id.locationViewOnMap).setVisibility(View.VISIBLE);
            findViewById(R.id.locationViewOnMap).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(getApplicationContext(),MapsActivity.class);
                    startActivity(intent);
                }
            });

        }
    }
    void setFloatingButton()
    {
        if(userData.type==15|userData.type==16)
        {       if(userPrivilage!=null){
            userFloatingButtonFollow.setImageResource(R.drawable.ic_chat_bubble_white);
            userFloatingButtonFollow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    Intent indi = new Intent(Profile_Activity.this, ChatWindow.class);
                    indi.putExtra("userid",userData.user_id);
                    indi.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    finish();
                }
            });}
            else
            userFloatingButtonFollow.setVisibility(GONE);
            return ;
        }
        else if(userData.type==0)
        {userFloatingButtonFollow.setVisibility(GONE);
            return ;}

       // Log.e("MGG","user follwo "+ userData.followType);
        if (userData.followType==1)
        {
            userFloatingButtonFollow.setImageResource(R.drawable.ic_chat_bubble_white);
            userFloatingButtonFollow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    Intent indi = new Intent(Profile_Activity.this, ChatWindow.class);
                    indi.putExtra("userid",userData.user_id);
                    indi.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    //indi.putExtra("username",data.get(pos).name);
                    //indi.putExtra("user_type",data.get(pos).numberType);
                    finish();

                }
            });
        }
        else if(userData.followType==3)
            {
                userFloatingButtonFollow.setImageResource(R.drawable.ic_unfollow_white);
                userFloatingButtonFollow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        MainServiceThread.updateuserFollowStatusWithRequestedCheck(Profile_Activity.this, userData.followType, userData.user_id, new JobCompletionListner() {
                            @Override
                            public void onJobCompletion(String txt1) {
                                userData.followType=0;
                                updateFollowDetails();
                            }
                        });

                    }
                });
            }

            else {
                userFloatingButtonFollow.setImageResource(R.drawable.ic_follow_white);
                userFloatingButtonFollow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        newUserFollowRequestWithDoubleCheck(Profile_Activity.this,userData.user_id, new JobCompletionListner() {
                            @Override
                            public void onJobCompletion(String txt1) {
                                userData.followType = Integer.parseInt(txt1);
                                updateFollowDetails();
                            }
                        });
                    }
                });
            }
           /* if(userData.status.equals("null"))
                userData.status=getResources().getString(R.string.default_user_status);;
            userStatus.setText(userData.status);*/

    }


    void defineAccountEditable(boolean editablePrivilage)
    {

        if(editablePrivilage)
        {
            statusEditButton.setVisibility(View.VISIBLE);
            profileNameEdit.setVisibility(View.VISIBLE);
            changeProfileImage.setVisibility(View.VISIBLE);
            bgmusicEditButton.setVisibility(View.VISIBLE);
        }
        else
        {
            statusEditButton.setVisibility(View.GONE);
            profileNameEdit.setVisibility(View.GONE);
            changeProfileImage.setVisibility(View.GONE);
            bgmusicEditButton.setVisibility(View.GONE);
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == 1 && resultCode == RESULT_OK) {
                if (data != null && data.getBooleanExtra("delete", false)) {

                    userData.img_url="null";
                    imgView.setImageDrawable(getApplicationContext().getResources().getDrawable(R.drawable.default_user));
                    uploadProfileImageAndUpdateOnDatabase(userData,null);
                }
                else {

                    Uri inputFile  = (data != null ? data.getData() : null);
                    if (inputFile == null && photoCameraURI != null) {
                        inputFile = photoCameraURI;
                    }

                    Intent cropIntent = new Intent(this, ImageCropActivity.class);
                    cropIntent.putExtra("uri", inputFile.toString());
                    cropIntent.putExtra("mode", 1);
                    startActivityForResult(cropIntent, 5);

                }

                /*Uri directory = data.getData();

                Intent cropIntent=new Intent(this,ImageCropActivity.class);
                cropIntent.putExtra("uri",directory.toString());
                startActivityForResult(cropIntent,1);*/

            }
            else  if (requestCode == 5 && resultCode == RESULT_OK) {
                Uri uricropped=data.getData();;
                //imgView.setImageURI(Uri.parse(uricropped));
                uploadProfileImageAndUpdateOnDatabase(userData,uricropped);
            }

           else if (requestCode == 11 && resultCode == RESULT_OK) {

                String staus=data.getStringExtra("result");
                userStatus.setText(staus);
            }

        else if (requestCode == 12 && resultCode == RESULT_OK) {

                profileName.setText(data.getStringExtra("result"));
        }

        else  if (requestCode == 20 && resultCode == RESULT_OK) {

                Uri directory = data.getData();

                uploadProfileMusicAndUpdateOnDatabase(directory);

               // Intent cropIntent=new Intent(this,ImageCropActivity.class);
                //cropIntent.putExtra("uri",directory.toString());
               // startActivityForResult(cropIntent,1);

            }
        else if (requestCode == 200 && resultCode == RESULT_OK) {

                Bundle args = data.getBundleExtra("BUNDLE");
                if(args!=null)
                {
                    final ArrayList<String> groupMemberList = (ArrayList<String>) args.getSerializable("ARRAYLIST");

                    if(groupMemberList.size()>0)
                    {

                        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                        dialog.setTitle(getString(R.string.add)+"?")
                                .setMessage(R.string.add_new_member_to_group_warning)
                                .setPositiveButton(getString(R.string.add), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                                        for (String temp:
                                        groupMemberList) {
                                            Log.e("GGG","groupadd 1 size is "+groupMemberList.size());
                                            MainServiceThread.addNewUserToGroup(getApplicationContext(),temp,userId);
                                        }

                                    }
                                })
                                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                                        paramDialogInterface.dismiss();
                                    }
                                });
                        dialog.show();

                    }

                }

        }





    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        if(photoCameraURI!=null)
            savedInstanceState.putString("contentUri", photoCameraURI.toString());

    }
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        String mCurrentPhotoPath=savedInstanceState.getString("contentUri");
        if(mCurrentPhotoPath!=null)
            photoCameraURI=Uri.parse(mCurrentPhotoPath);
    }
    Uri photoCameraURI;

    void uploadProfileMusicAndUpdateOnDatabase(final Uri newurl)
    {
        AroundAlertBoxManager.showProgress(this,null,R.string.updating_profile_music,true);

        StorageReference uploadRef;

        Log.e("MSGG","uploading NOW " + newurl.toString());
        uploadRef = FirebaseStorage.getInstance().getReference().child("profile_musics").child(StringUtils.randomStringGenerator() + ".mp3");
        uploadRef.putFile(newurl)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        AroundAlertBoxManager.cancelProgress();
                        Log.e("MSGG","success NOW");
                        final String downloadUrl = taskSnapshot.getDownloadUrl().toString();
                        Log.e("MSGG","usrl NOW "+downloadUrl.toString());
                        DatabaseReference updateRef2 = FirebaseDatabase.getInstance().getReference("user_profile").child(userId).child("music_url");
                        Log.e("MSGG","pushing NOW ");
                        updateRef2.setValue(downloadUrl);
                        Log.e("MSGG","pushed NOW");
                        //imgView.setImageURI(newurl);
                        userData.music_url=newurl.toString();
                        resetMediaPlayer();
                        TabbedActivity.saveContactMusicToDatabase(userdetail.getUid(),newurl.toString(),downloadUrl,System.currentTimeMillis());
                    }

                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(Profile_Activity.this, R.string.failed_to_update_profile, Toast.LENGTH_SHORT).show();
                AroundAlertBoxManager.cancelProgress();
               // Log.e("MSGG","failed NOW");
                //imgView.setImageURI(Uri.parse(userData.img_url));
            }
        });

       // setingspage=

    }

    public static class ProfileNotificationSettings extends PreferenceFragmentCompat implements
            SharedPreferences.OnSharedPreferenceChangeListener
    {
        @Override
        public void setArguments(@Nullable Bundle args) {
            notificationUri=args.getString("notificationUri");
            vibration=args.getInt("vibration");
            light=args.getInt("light");
            userData=(UserWholeData) args.getSerializable("userData");
            //updateData();
        }

        String notificationUri;
        int vibration=0;
        int light=0;
        int mute=0;
        UserWholeData userData;
        public ProfileNotificationSettings()
        {

        }

        AroundPreferenceView notificationSoundPreference;
        SwitchPreferenceCompat mutePreference;
        AroundListPreferenceView vibrationPreference;
        AroundListPreferenceView notificationLightreference;
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            notificationUri=getString(R.string.default_text);
            addPreferencesFromResource(R.xml.userprofile_preference);
             notificationSoundPreference = (AroundPreferenceView) this.findPreference("profile_pref_chat_notification_sound");
             mutePreference = (SwitchPreferenceCompat) this.findPreference("profile_pref_chat_mute");
             vibrationPreference = (AroundListPreferenceView) this.findPreference("profile_pref_chat_vibration");
             notificationLightreference = (AroundListPreferenceView) this.findPreference("profile_pref_chat_notification_light");


            notificationSoundPreference.setIcon(R.drawable.ic_notifications_white);
            //mutePreference.setIcon(R.id.sou);
            notificationLightreference.setIcon(R.drawable.ic_brightness_white);
            vibrationPreference.setIcon(R.drawable.ic_vibration_primary);
            vibrationPreference.setSummary(getActivity().getString(R.string.default_text) );
            //mutePreference
            updateData();

        }
        void updateData()
        {
            String title;
            if(notificationUri!=null && !notificationUri.equals("") ){
            Ringtone ringtone = RingtoneManager.getRingtone(getActivity(), Uri.parse(notificationUri));
            title = ringtone.getTitle(getContext());

            title = Uri.parse(notificationUri).equals(Settings.System.DEFAULT_NOTIFICATION_URI) ? getString(R.string.default_text)  : title;

            //Log.e("MGG", "notificationUri " + Uri.parse(notificationUri));
            //Log.e("MGG", "DEFAULT_NOTIFICATION_URI  " + Settings.System.DEFAULT_NOTIFICATION_URI);
            // Ringtone ringtone=notificationSummery

        }
        else
            {
                title=getString(R.string.default_text) ;
            }

            notificationSoundPreference.setSummary(title);

            vibrationPreference.setValueIndex(vibration);
            vibrationPreference.setSummary(vibrationPreference.getEntry());

            notificationLightreference.setValueIndex(light);
            notificationLightreference.setSummary(notificationLightreference.getEntry());

            if(mute!=0 && mute>System.currentTimeMillis()+1000*30)
            mutePreference.setChecked(true);
            else
                mutePreference.setChecked(false);


        }
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            Preference pref = findPreference(key);

            if(key.equals("profile_pref_chat_vibration"))
            {
               // Log.e("MGG","sharedPreferences "+);

                    vibration=Integer.parseInt(sharedPreferences.getString(key, "0"));
                    updateData();
                    UserManager.updateUserChatNotification(userId,null,vibration,null,null);



            }
            else if(key.equals("profile_pref_chat_notification_light"))
            {
                //Log.e("MGG","sharedPreferences "+sharedPreferences.getString(key, "0"));
                light=Integer.parseInt(sharedPreferences.getString(key, "0"));
                updateData();
                UserManager.updateUserChatNotification(userId,null,null,light,null);
            }
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {

            if(resultCode==RESULT_OK && requestCode==2)
            {
                Uri uri = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
                if(uri!=null)
                {
                    notificationUri=uri.toString();
                updateData();
                UserManager.updateUserChatNotification(userId,uri.toString(),null,null,null);
                }
            }
            super.onActivityResult(requestCode, resultCode, data);

        }

        @Override
        public boolean onPreferenceTreeClick(Preference preference) {
            if (preference.getKey().equals("profile_pref_chat_notification_sound")) {
                Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_NOTIFICATION);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, true);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_DEFAULT_URI, Settings.System.DEFAULT_NOTIFICATION_URI);


                if (notificationUri != null && !notificationUri.equals("")) {
                    intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, Uri.parse(notificationUri));
                } else {
                    intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, Settings.System.DEFAULT_NOTIFICATION_URI);
                }

                startActivityForResult(intent, 2);
                return true;

                /*Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_NOTIFICATION);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, true);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_DEFAULT_URI, Settings.System.DEFAULT_NOTIFICATION_URI);

                String existingValue = null;//getRingtonePreferenceValue(); // TODO
                if (existingValue != null) {
                    if (existingValue.length() == 0) {
                        // Select "Silent"
                        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, (Uri) null);
                    } else {
                        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, Uri.parse(existingValue));
                    }
                } else {
                    // No ringtone has been selected, set to the default
                    intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, Settings.System.DEFAULT_NOTIFICATION_URI);
                }

                startActivityForResult(intent, 2);*/
            }
            else if(preference.getKey().equals("profile_pref_chat_mute"))
            {

                final SwitchPreferenceCompat mutePref=(SwitchPreferenceCompat)findPreference("pref_chat_mute");

                Log.e("MGG","switch  "+mutePref.isChecked());
                if(mutePref.isChecked()) {
                    mutePref.setChecked(false);
                    AroundAlertBoxManager.showConversationMuteDialog(getActivity(),userData, new AroundAlertBoxManager.MuteSelectionListener() {
                        @Override
                        public void onMuted(long until, boolean notificationAlso,String userID) {
                            Log.e("MGG", "switch switched " + until);
                            mutePref.setChecked(true);
                            UserManager.updateUserChatNotification(userID,null,null,null,until*1000L);
                            userData.muteTime=until*1000L;
                        }
                    });
                }
                else
                {
                    UserManager.updateUserChatNotification(userData.user_id,null,null,null,0L);
                    userData.muteTime=0;
                    //invalidateOptionsMenu();
                }
                return true;
            }
            return super.onPreferenceTreeClick(preference);
        }
    }
    void uploadProfileImageAndUpdateOnDatabase(final UserWholeData dat,final Uri newurl)
    {
        AroundAlertBoxManager.showProgress(this,null,R.string.updating_profile_image,true);
        if(newurl==null)
        {
            DatabaseReference updateRef2 = FirebaseDatabase.getInstance().getReference("user_profile").child(dat.user_id).child("img_url");
            //Log.e("MSGG","pushing NOW ");
            updateRef2.setValue("null").addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful())
                    {
                        AroundAlertBoxManager.cancelProgress();
                        EditAnyTextActivity.updateNameOrStatusOrProfileImage(dat.user_id,null,null,"null");
                        imgView.setImageResource(R.drawable.default_user);
                        dat.img_url="null";
                    }
                    else {
                        Toast.makeText(Profile_Activity.this, R.string.failed_to_update_profile, Toast.LENGTH_SHORT).show();
                        AroundAlertBoxManager.cancelProgress();
                    }
                }
            });
            return;

        }
        StorageReference uploadRef;

        Log.e("MSGG","uploading NOW " + newurl.toString());
        uploadRef = FirebaseStorage.getInstance().getReference().child("profile_images").child(StringUtils.randomStringGenerator() + ".png");
        uploadRef.putFile(newurl)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        AroundAlertBoxManager.cancelProgress();

                        Log.e("MSGG","success NOW");
                        final String downloadUrl = taskSnapshot.getDownloadUrl().toString();
                        Log.e("MSGG","usrl NOW "+downloadUrl.toString());
                        DatabaseReference updateRef2 = FirebaseDatabase.getInstance().getReference("user_profile").child(dat.user_id).child("img_url");
                        Log.e("MSGG","pushing NOW ");
                        updateRef2.setValue(downloadUrl);
                        Log.e("MSGG","pushed NOW");
                        imgView.setImageURI(newurl);
                        userData.img_url=newurl.toString();
                        EditAnyTextActivity.updateNameOrStatusOrProfileImage(dat.user_id,null,null,newurl.toString());
                    }

                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(Profile_Activity.this, R.string.failed_to_update_profile, Toast.LENGTH_SHORT).show();
                AroundAlertBoxManager.cancelProgress();
                imgView.setImageURI(Uri.parse(userData.img_url));
            }
        });
    }
    public static Intent getPickImageIntent(Context context) {
        Intent chooserIntent = null;

        List<Intent> intentList = new ArrayList<>();

        Intent pickIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePhotoIntent.putExtra("return-data", true);
        takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(getTempFile(context)));
        intentList = addIntentsToList(context, intentList, pickIntent);
        intentList = addIntentsToList(context, intentList, takePhotoIntent);

        if (intentList.size() > 0) {
            chooserIntent = Intent.createChooser(intentList.get(intentList.size() - 1),"GET FILE");
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentList.toArray(new Parcelable[]{}));
        }

        return chooserIntent;
    }
    private static List<Intent> addIntentsToList(Context context, List<Intent> list, Intent intent) {
        List<ResolveInfo> resInfo = context.getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo resolveInfo : resInfo) {
            String packageName = resolveInfo.activityInfo.packageName;
            Intent targetedIntent = new Intent(intent);
            targetedIntent.setPackage(packageName);
            list.add(targetedIntent);
        }
        return list;
    }


    void setupGroupInWindow()
    {
        quitGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeSingleUserFromGroup(getApplicationContext(),userdetail.getUid(),userId,true);
            }
        });
        //findViewById(R.id.numberSection).setVisibility(GONE);

        findViewById(R.id.notificationExtraButton).setVisibility(View.INVISIBLE);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);


        notificationRview.setLayoutManager(mLayoutManager);
        notificationRview.setNestedScrollingEnabled(false);

        fillGroupMemberdetails(userId);
        Log.e("MSG","Size of memberdata :"+memberdata.size());
        memberadap = new Adapter_GroupMembers(this,userPrivilage,contactdata,memberdata);
        notificationRview.setAdapter(memberadap);

        registerForContextMenu(notificationRview);



    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {


        int pos= Adapter_GroupMembers.optionPosition;
        String selectedUser = memberadap.contactdata.get(pos).user_id;
        switch (item.getItemId()) {
            case R.id.member_remove:
                //Log.e("MSGG","removing another user");
                removeSingleUserFromGroup(getApplicationContext(),selectedUser,userId,true);
                break;
            case R.id.view_profile:
                Intent indi=new Intent(getApplicationContext(),Profile_Activity.class);
                indi.putExtra("userid",selectedUser);
                startActivity(indi);
                break;
            case R.id.chat_message:
                Intent indi2 = new Intent(getApplicationContext(), ChatWindow.class);
                indi2.putExtra("userid",selectedUser);
                finish();
                startActivity(indi2);
                break;
            case R.id.member_admin:
                if(memberadap.contactdata.get(pos).type==1)
                    MainServiceThread.revokeAdminStatusServer(getApplicationContext(),userId,selectedUser,0);
                else
                    MainServiceThread.revokeAdminStatusServer(getApplicationContext(),userId,selectedUser,1);
                break;

        }
        return super.onContextItemSelected(item);

    }

    void fillGroupMemberdetails(String uID)
    {
        memberdata.clear();
        contactdata.clear();
        SQLiteDatabase db = DatabaseManager.getDatabase(getApplicationContext()).getReadableDatabase();

        Cursor cursor=db.rawQuery(
                "SELECT * FROM "+DatabaseAccesser.groupsmembers_table+" INNER JOIN "+DatabaseAccesser.user_table+" ON "+
                        DatabaseAccesser.groupsmembers_table+"."+DatabaseAccesser.groupsmembers_user_id+" = "+ DatabaseAccesser.user_table+"."+DatabaseAccesser.user_user_id
                        +" WHERE "+DatabaseAccesser.groupsmembers_table+"."+DatabaseAccesser.groupsmembers_group_id +" =?" ,
               new String[]{uID});

        //+" WHERE "+DatabaseAccesser.groups_participant_table+"."+DatabaseAccesser.group_user_id +" =?"

userPrivilage=null;

        while (cursor.moveToNext()) {


            GroupMemberData temp = new GroupMemberData();
            ContactListData temp2=new ContactListData();

            temp.userid = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.groupsmembers_user_id));
            temp2.user_id = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_user_id));
            temp2.user_name = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_name));

            temp.member_type = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.groupsmembers_member_type));
            Log.e("GGG","member_type count : "+temp2.user_name+" / "+temp.member_type);
            temp2.status = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_status));
            temp2.img_url=cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_localUrl));

            temp2.type= cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.groupsmembers_member_type));
            if(temp2.user_id.equals(userdetail.getUid()))
            {
                userPrivilage = temp2.type;
            }
            memberdata.add(temp);
            contactdata.add(temp2);
        }
        cursor.close();

    }


    void accessDatabseForContent()
    {

        SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();

        Cursor cursor = db.query(DatabaseAccesser.user_table,
                new String[]{
                        DatabaseAccesser.user_user_id,
                        DatabaseAccesser.user_name,
                        DatabaseAccesser.user_user_type,
                        DatabaseAccesser.user_status,
                        DatabaseAccesser.user_localUrl,
                        DatabaseAccesser.user_globalUrl},
                DatabaseAccesser.user_user_id + " =?",
                new String[]{userId},
                null, null, null,null);


        //Toast.makeText(this, cursor.getCount(), Toast.LENGTH_SHORT).show();

        while (cursor.moveToNext()) {

        userType=cursor.getInt(
        cursor.getColumnIndexOrThrow(DatabaseAccesser.user_user_type));

            String pro=cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_name));
            profileName.setText(pro);

            pro=cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_status));

            if(pro.equals("null"))
                pro="Wanna know an App called Around?";

            userStatus.setText(pro);


            userData.img_url=cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_localUrl));
            String status=cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_status));
            if( userData.img_url.equals("null"))
            {
                imgView.setImageResource(R.drawable.default_user);
            }
            else
            {
                imgView.setImageURI(Uri.parse( userData.img_url));
            }


            if(status.equals("null"))
            {
               // userStatus.setText();
            }
            else
            {
                userStatus.setText(status);
            }
            userId=cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_user_id));







        }
        cursor.close();
    }

    MenuItem followItem;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.user_profile_menu, menu);
        profileMenu=menu;

        followItem = menu.findItem(R.id.action_follow_user);
        if(userType==15||userType==16)
        {
            menu.findItem(R.id.action_view_contact_addressbook).setVisible(false);
            menu.findItem(R.id.action_poke_user).setVisible(false);


            //if(userPrivilage==null)
                menu.findItem(R.id.action_follow_user).setVisible(false);

            /*else{ menu.findItem(R.id.action_follow_user).setTitle("View chats");
                menu.findItem(R.id.action_follow_user).setIcon(R.drawable.ic_chat_bubble_white);
            }*/
            menu.findItem(R.id.action_view_chats).setVisible(true);
        }
        else
        {
            followItem.setEnabled(true);
            if(userId.equals(userdetail.getUid()))
            {
                menu.findItem(R.id.action_poke_user).setVisible(false);
                menu.findItem(R.id.action_follow_user).setVisible(false);
                menu.findItem(R.id.action_view_chats).setVisible(false);
                menu.findItem(R.id.action_view_contact_addressbook).setVisible(false);
                menu.findItem(R.id.action_chat_block_user).setVisible(false);
                menu.findItem(R.id.action_chat_mute_user).setVisible(false);
                menu.findItem(R.id.more_menu).setVisible(false);

            }
            else
            {

                //menu.findItem(R.id.action_view_chats).setVisible(true);
            }



            if(userData.followType==1||userData.followType==3)
            {//yyyyy
                menu.findItem(R.id.action_follow_user).setTitle(R.string.unfollow);
                menu.findItem(R.id.action_follow_user).setIcon(R.drawable.ic_unfollow_white);
                menu.findItem(R.id.action_follow_user).setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
            }
            else
            {
                menu.findItem(R.id.action_follow_user).setTitle(R.string.follow);
                menu.findItem(R.id.action_follow_user).setIcon(R.drawable.ic_follow_white);
                menu.findItem(R.id.action_follow_user).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            }

            if(userData.type==2)
            {
                menu.findItem(R.id.action_view_contact_addressbook).setTitle(R.string.view_in_phonebook);
            }
            else
            {
                menu.findItem(R.id.action_view_contact_addressbook).setTitle(R.string.add_to_phonebook);
            }

            if (checkIfPrivacyShouldBeActivie(userData.priv_number,userData.remoteContact))
            {
                //menu.findItem(R.id.action_view_contact_addressbook).setEnabled(false);
            }
            else
            {
                menu.findItem(R.id.action_view_contact_addressbook).setEnabled(false);
            }

        }

        if(userData.blockType==1)
        {
            menu.findItem(R.id.action_chat_block_user).setTitle(R.string.unblock_tag);
        }
        else
        {
            menu.findItem(R.id.action_chat_block_user).setTitle(R.string.block_tag);
        }

        if((userData.type==15||userData.type==16)&&userPrivilage==null)
            menu.findItem(R.id.action_chat_block_user).setVisible(false);
        else
            menu.findItem(R.id.action_chat_block_user).setVisible(true);

  /*     if(userData.muteTime>System.currentTimeMillis()) {
            menu.findItem(R.id.action_chat_mute_user).setIcon(getApplicationContext().getResources().getDrawable(R.drawable.ic_volume_on));
            menu.findItem(R.id.action_chat_mute_user).setTitle("Unmute chat");
        }
        else{
            menu.findItem(R.id.action_chat_mute_user).setIcon(getApplicationContext().getResources().getDrawable(R.drawable.ic_volume_off));
            menu.findItem(R.id.action_chat_mute_user).setTitle("Mute chat");
        }
        menu.findItem(R.id.action_chat_mute_user).getIcon().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.red), android.graphics.PorterDuff.Mode.SRC_IN);
*/


        return true;
    }

    void updateFollowDetails()
    {
        setFloatingButton();
        if(userData.followType==1||userData.followType==3)
        { followItem.setIcon(R.drawable.ic_unfollow_white);
        followItem.setTitle(R.string.unfollow);}
        else
        { followItem.setIcon(R.drawable.ic_follow_white);
        followItem.setTitle(R.string.follow);}
    }

    Menu profileMenu;
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        final MenuItem chageItem=item;
        switch (item.getItemId()) {

            case R.id.action_view_chats:
                Intent indi = new Intent(Profile_Activity.this, ChatWindow.class);
                indi.putExtra("userid",userData.user_id);
                indi.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                finish();
                return true;
            case R.id.action_follow_user:

                if(userData.followType==1||userData.followType==3)
                {
                    MainServiceThread.updateuserFollowStatusWithRequestedCheck(this,userData.followType,
                            userData.user_id, new JobCompletionListner() {
                        @Override
                        public void onJobCompletion(String txt1) {
                            userData.followType=Integer.parseInt(txt1);
                            updateFollowDetails();
                        }
                    });
                    /*updateUserFollowStatus(userData.user_id,0);
                    userData.followType=0;
                    updateFollowDetails();*/
                }
                else {
                    newUserFollowRequestWithDoubleCheck(this,userId, new JobCompletionListner() {
                        @Override
                        public void onJobCompletion(String txt1) {
                            userData.followType=Integer.parseInt(txt1);
                            updateFollowDetails();

                        }
                    });
                }

                return true;
            case R.id.action_poke_user:
                newUserPoke(getApplicationContext(),userId,null);
                return true;
            case R.id.action_view_contact_addressbook:
                if(userData.type==2)
                {
                    // menu.findItem(R.id.action_add_contact).setTitle("View in Phonebook");
                    ChatWindow.chatwindowInstance.viewSingleContactOnPhoneBook(userData.number);
                }
                else
                {
                    ChatWindow.insertRawContact(getApplicationContext(),userData.user_name,new String[]{userData.number});
                }
                return true;
            case R.id.action_chat_block_user:
                blockButtonHit();

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static void newUserPoke(final Context context, String uID, final JobCompletionWithFailureListner listener)
    {

        NotificationData x=new NotificationData();

        UserWholeData dat=ChatWindow.returnSingleUserDetails(context,userdetail.getUid());
        final UserWholeData targetUser=ChatWindow.returnSingleUserDetails(context,uID);
        x.userid=userdetail.getUid();
        x.type=1;
        x.timestamp=System.currentTimeMillis();
        x.username=dat.user_name;

        //AroundAlertBoxManager.showProgress();
        DatabaseReference chatRef = FirebaseDatabase.getInstance().getReference().child("notifi_data").child(uID).push();
        chatRef.setValue(x).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid)
            {
                Toast.makeText(context, context.getString(R.string.you_poked,targetUser.user_name!=null?targetUser.user_name:""), Toast.LENGTH_SHORT).show();
                if(listener!=null)
                listener.onJobCompletion("");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if(listener!=null)
                listener.onJobFailed("");
            }
        });
    }
    public static void sentCustomNotificationToUser(Context context,String receiver_uID, int type2, String text2, final JobCompletionListner listner) {
        NotificationData x=new NotificationData();

        UserWholeData dat=ChatWindow.returnSingleUserDetails(context,FirebaseAuth.getInstance().getUid());

        x.type=type2;
        x.timestamp= System.currentTimeMillis();
        x.username=dat.user_name;
        x.userid=FirebaseAuth.getInstance().getUid();
        x.text=text2;

        DatabaseReference chatRef = FirebaseDatabase.getInstance().getReference().child("notifi_data").child(receiver_uID).push();
        chatRef.setValue(x).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid)
            {
                // Toast.makeText(Profile_Activity.this, "You have poked", Toast.LENGTH_SHORT).show();
                if(listner!=null)
                listner.onJobCompletion("null");
            }
        });
    }

    MediaPlayer mediaPlayer;
    AppCompatSeekBar bfmusicSeekbar;
    CircleColorImageView playPauseButton;
    int mediaFileLengthInMilliseconds;
    Timer mediaPlayerTimer;
    TimerTask mediaPlayertask;



    public void setUpBgMusicPlayback(boolean show)
    {
        if(show) {
            playPauseButton =  findViewById(R.id.playPauseButton);
            bfmusicSeekbar = (AppCompatSeekBar) findViewById(R.id.bfmusicSeekbar);
            mediaFileLengthInMilliseconds = 0;
            timeofbgmusic.setText(ChatWindow.convertTimeStampToText(getApplicationContext(),userData.music_timestamp,false));

        }
        else
        {
            findViewById(R.id.bgmusicSection).setVisibility(GONE);
        }
    }


    void resetMediaPlayer()
    {
        if(mediaPlayer!=null)
        {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer=null;
        }
        if(mediaPlayerTimer!=null)
        {
            mediaPlayerTimer.cancel();
            mediaPlayerTimer.purge();
        }
        playPauseButton.setImageResource(R.drawable.ic_play_arrow_accent);
    }

    void setUpMediaPlayer()
    {
        if(mediaPlayer==null)
        {
        mediaPlayer=new MediaPlayer();
        if(userData.music_url.equals("null"))
            mediaPlayer = MediaPlayer.create(this, R.raw.music_file);
        else
            mediaPlayer = MediaPlayer.create(this, Uri.parse(userData.music_url));

        //mediaPlayer.prepare();
        mediaFileLengthInMilliseconds=mediaPlayer.getDuration();

        mediaPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
            @Override
            public void onBufferingUpdate(MediaPlayer mp, int percent) {
                bfmusicSeekbar.setSecondaryProgress(percent);
            }
        });

        mediaPlayertask = new TimerTask() {
            @Override
            public void run() {

                Log.e("MGG","changing  now");
                if (mediaPlayer.isPlaying()) {
                    float pos=((float)mediaPlayer.getCurrentPosition()/mediaFileLengthInMilliseconds)*100;
                    bfmusicSeekbar.setProgress((int)pos);
                    Log.e("MGG","installing to "+mediaPlayer.getCurrentPosition()+ " scroll is "+bfmusicSeekbar.getProgress());
                }

            }
        };
        mediaPlayerTimer = new Timer();

        mediaPlayerTimer.schedule(mediaPlayertask, 0, 400);


        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                playPauseButton.setImageResource(R.drawable.ic_play_arrow_accent);
                bfmusicSeekbar.setProgress(100);
            }
        });

        /*bfmusicSeekbar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
              int playPositionInMillisecconds = (mediaFileLengthInMilliseconds / 100) * bfmusicSeekbar.getProgress();
                // mediaPlayer.pause();

                //if(!mediaPlayer.isPlaying())
                pauseMediaPlayer();
                    mediaPlayer.seekTo(playPositionInMillisecconds);
                    Log.e("MGG","seeked to "+playPositionInMillisecconds+ " scroll is "+bfmusicSeekbar.getProgress());
                return false;
            }
        });*/





        bfmusicSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                //int playPositionInMillisecconds = (mediaFileLengthInMilliseconds / 100) * bfmusicSeekbar.getProgress();
                // mediaPlayer.pause();
                // if(!mediaPlayer.isPlaying())
                // mediaPlayer.seekTo(playPositionInMillisecconds);
                // mediaPlayer.start();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {


            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int playPositionInMillisecconds = (mediaFileLengthInMilliseconds / 100) * bfmusicSeekbar.getProgress();
                //pauseMediaPlayer();
                mediaPlayer.seekTo(playPositionInMillisecconds);
                Log.e("MGG","seeked to "+playPositionInMillisecconds+ " scroll is "+bfmusicSeekbar.getProgress());
            }
        });

    }
        if(mediaPlayer.isPlaying())
    {
        pauseMediaPlayer();}
        else{
        startMediaPlayer();
    }
    }
    public void playLocalAudio(View c) throws IOException {


            if(userData.music_url.contains("https"))
            {
                MainServiceThread.downloadFileAndReturnUriString(getApplicationContext(),userData.music_url,false,2,null, new FileDownloadListner() {
                    @Override
                    public void onProgressListener(int percent) {

                    }

                    @Override
                    public void onSuccessListener(String localFilename) {
                        userData.music_url=localFilename;
                        saveContactMusicToDatabase(userData.user_id,null,localFilename,null);
                        setUpMediaPlayer();
                    }

                    @Override
                    public void onFailureListener(int percent) {

                    }

                    @Override
                    public void onPausedListener(int percent) {

                    }
                });
            }
            else
            {
                setUpMediaPlayer();
            }

    }
    @Override
    protected void onStart() {
        super.onStart();
        AppOnlineStatusManager.OnlineStatus.setActive(true);

    }
    @Override
    public void onStop() {
        AppOnlineStatusManager.OnlineStatus.setActive(false);
        super.onStop();
    }
    void pauseMediaPlayer()
    {
        playPauseButton.setImageResource(R.drawable.ic_play_arrow_accent);
        mediaPlayer.pause();
        //mediaPlayerTimer.cancel();
    }
    void startMediaPlayer()
    {
        playPauseButton.setImageResource(R.drawable.ic_pause_accent);
        mediaPlayer.start();

    }

    private void primarySeekBarProgressUpdater() {
        bfmusicSeekbar.setProgress((int)(((float)mediaPlayer.getCurrentPosition()/mediaFileLengthInMilliseconds)*100)); // This math construction give a percentage of "was playing"/"song length"
        if (mediaPlayer.isPlaying()) {
            Runnable notification = new Runnable() {
                public void run()
                {
                    primarySeekBarProgressUpdater();
                }
            };
           // handler.postDelayed(notification,1000);
        }
    }


    protected void onResume() {
        super.onResume();
        profile_settings.getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(profile_settings);
    }

    protected void onPause() {
        super.onPause();
        profile_settings.getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(profile_settings);
        resetMediaPlayer();
    }

    static void fecthUserPrivacyStatus(String user, final UserPrivacyStatusListner lstn) {
        FirebaseDatabase.getInstance().getReference().child("user_profile").child(user).child("user_privacy")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        UserPrivacyData post=new UserPrivacyData();
                        if(dataSnapshot==null)
                            return;

                        post.profile_picture=Integer.parseInt(dataSnapshot.child("profile_picture").getValue().toString());
                        post.profile_status=Integer.parseInt(dataSnapshot.child("profile_status").getValue().toString());
                        post.number=Integer.parseInt(dataSnapshot.child("number").getValue().toString());
                        post.activity_status=Integer.parseInt(dataSnapshot.child("activity_status").getValue().toString());
                        post.location=Integer.parseInt(dataSnapshot.child("location").getValue().toString());
                        post.anyonefollow=Integer.parseInt(dataSnapshot.child("anyonefollow").getValue().toString());
                        post.anyonemessage=Integer.parseInt(dataSnapshot.child("anyonemessage").getValue().toString());


                        //xxx
                           if(lstn!=null)
                               lstn.onUserStatusReceieved(post);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }




    public static void newUserBlock(final Context context,final String userID,final JobCompletionListner listener)
    {
        //Log.e("GGG","block_unblock BLOCK");
        AroundAlertBoxManager.showProgress((Activity) context,null,R.string.blocking,true);

        sentCustomNotificationToUser(context,userID, 21, "null", new JobCompletionListner() {
            @Override
            public void onJobCompletion(String txt1) {

                FirebaseDatabase.getInstance().getReference().child("user_profile").child(userdetail.getUid()).child("contacts").child(userID).child("block").setValue("1").addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        AroundAlertBoxManager.cancelProgress();
                        Log.e("GGG","block_unblock BLOCK SUCCESS");
                        SQLiteDatabase db = DatabaseManager.getDatabase(context).getWritableDatabase();
                        ContentValues values = new ContentValues();
                        values.put(DatabaseAccesser.user_user_block_type, 1);
                        long newRowId = db.update(DatabaseAccesser.user_table, values, DatabaseAccesser.user_user_id + " =? ", new String[]{userID});
                        listener.onJobCompletion("");
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        AroundAlertBoxManager.cancelProgress();
                    }
                });

              //  FirebaseDatabase.getInstance().getReference().child(FirebaseAuth.getInstance().getUid()).child("contacts").
                       // child(userID).setValue("1");

                /*if (newRowId == 0) {
                    fetchAndSaveNewUser(userID,null,3);
                }*/
            }
        });
    }

    public static void newUserUnBlock(final Context context,final String userID,final JobCompletionListner listener)
    {
        AroundAlertBoxManager.showProgress((Activity) context,null,R.string.unblocking,true);

        sentCustomNotificationToUser(context,userID, 22, "null", new JobCompletionListner() {
            @Override
            public void onJobCompletion(String txt1) {

                /*FirebaseDatabase.getInstance().getReference().child("user_blocking").child(userId).
                        child(userID).child(userdetail.getUid()).removeValue();*/

                FirebaseDatabase.getInstance().getReference().child("user_profile").child(userdetail.getUid()).child("contacts").child(userID).child("block").removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        AroundAlertBoxManager.cancelProgress();
                        SQLiteDatabase db = DatabaseManager.getDatabase(context).getWritableDatabase();
                        ContentValues values = new ContentValues();
                        values.put(DatabaseAccesser.user_user_block_type, 0);

                        long newRowId = db.update(DatabaseAccesser.user_table, values, DatabaseAccesser.user_user_id + " =? ", new String[]{userID});

                        listener.onJobCompletion("");
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        AroundAlertBoxManager.cancelProgress();
                    }
                });

            }
        });




    }


}
