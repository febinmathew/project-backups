package com.hash.around.Views.CustomMakers;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hash.around.OtherClasses.ChatDataAdapter;
import com.hash.around.R;

public class ChatView_UnseenHeader extends ChatView_ItemHeader{

    @Override
    public boolean hasHeader(int adapPos,RecyclerView par)
    {

        if(this.lastSeenTimestamp==0)
            return false;
        Log.e("MSG","this.lastSeenTimestamp "+this.lastSeenTimestamp);
        return adapter.hasUnseenHeader(adapPos,isReverseLayout(par),this.lastSeenTimestamp);
    }
    static long lastSeenTimestamp=0;

    public ChatView_UnseenHeader(Context context, ChatDataAdapter adap,long lastSeenTime) {
        super(context, adap);

        this.lastSeenTimestamp=lastSeenTime;
        Log.e("MSG","this.lastSeenTimestamp123 "+this.lastSeenTimestamp);
    }


    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        int position     = parent.getChildAdapterPosition(view);
        int headerHeight = 0;

        if (position != RecyclerView.NO_POSITION && hasHeader(position,parent)) {
            View header = getHeader(parent, mContext, position,true).itemView;
            headerHeight = header.getHeight();
        }
        outRect.set(0, headerHeight, 0, 0);
    }



    @Override
    protected int getHeaderTop(RecyclerView parent, View child, View header, int adapterPos,
                               int layoutPos)
    {
        int headerHeight = header.getHeight();
        int top = getChildY(parent, child) - headerHeight;


        return parent.getLayoutManager().getDecoratedTop(child);
    }



    @Override
    RecyclerView.ViewHolder getHeader(RecyclerView parent, Context context, int position,boolean forHeight)
    {
        final RecyclerView.ViewHolder holder = new ChatView_DateHeader.HeaderViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chat_tag_unseen, parent, false));
        final View header = holder.itemView;

        RecyclerView.ViewHolder  viewHolder= new ChatView_DateHeader.HeaderViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chat_tag_unseen, parent, false));
       this.adapter.onBindUnseenItemDecorator(viewHolder,position);

        int widthSpec  = View.MeasureSpec.makeMeasureSpec(parent.getWidth(), View.MeasureSpec.EXACTLY);
        int heightSpec = View.MeasureSpec.makeMeasureSpec(parent.getHeight(), View.MeasureSpec.UNSPECIFIED);

        int childWidth  = ViewGroup.getChildMeasureSpec(widthSpec, parent.getPaddingLeft() + parent.getPaddingRight(), viewHolder.itemView.getLayoutParams().width);
        int childHeight = ViewGroup.getChildMeasureSpec(heightSpec, parent.getPaddingTop() + parent.getPaddingBottom(), viewHolder.itemView.getLayoutParams().height);

        viewHolder.itemView.measure(childWidth, childHeight);
        viewHolder.itemView.layout(0, 0, viewHolder.itemView.getMeasuredWidth(), viewHolder.itemView.getMeasuredHeight());


        //headerCache.put(key, holder);

        return viewHolder;

    }


    }

