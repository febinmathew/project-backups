package com.hash.around.Views;

import android.annotation.TargetApi;
import android.content.Context;
import android.net.Uri;
import android.os.Build.VERSION_CODES;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.hash.around.R;
import com.hash.around.Views.Medias.CircularRelativeLayout;
import com.vanniktech.emoji.EmojiTextView;

//import org.thoughtcrime.securesms.R;

public class FeedRailItemView extends CircularRelativeLayout {

  private final boolean squareHeight=true;

  ImageView imgView;
  EmojiTextView textView;
  RelativeLayout bodyView;

  public FeedRailItemView(Context context) {
    this(context, null);
  }


  public FeedRailItemView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }


  public FeedRailItemView(Context context, AttributeSet attrs, int defStyleAttr) {

    super(context, attrs, defStyleAttr);
    Log.e("GGG","initial");

    /*if (attrs != null) {
      TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.SquareFrameLayout, 0, 0);
      this.squareHeight = typedArray.getBoolean(R.styleable.SquareFrameLayout_square_height, false);
      this.squareAsWidth=typedArray.getBoolean(R.styleable.SquareFrameLayout_square_width, false);
      typedArray.recycle();
    }
    else {
      this.squareHeight = false;
      this.squareAsWidth=false;
    }*/
    //
  }
  @Override
  protected void onFinishInflate() {
    super.onFinishInflate();

    initViews();
  }

  void initViews()
  {
    imgView=findViewById(R.id.imgView);
    textView=findViewById(R.id.textView);
    bodyView=findViewById(R.id.bodyView);
  }
  void setOnClickListner(OnClickListener listner)
  {
    bodyView.setOnClickListener(listner);
  }
  void setText(String tx)
  {
    textView.setVisibility(VISIBLE);
    textView.setText(tx);
    imgView.setVisibility(GONE);
  }
  void setImgResource(Uri uri)
  {
    imgView.setVisibility(VISIBLE);
  imgView.setImageURI(uri);
    textView.setVisibility(GONE);
  }
  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    //noinspection SuspiciousNameCombination
    if (squareHeight) super.onMeasure(heightMeasureSpec, heightMeasureSpec);
    else              super.onMeasure(widthMeasureSpec, widthMeasureSpec);


  }
}
