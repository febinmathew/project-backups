package com.hash.around.Broadcasts;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.hash.around.OtherClasses.DataTypes.GroupChatData;
import com.hash.around.Utils.ChatSender;
import com.hash.around.Utils.DatabaseManager;


public class AroundBroadcastReceiver extends BroadcastReceiver {
    GroupChatData data;
    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            Log.d("GGG", "booooooorComplete 1");
        } else if (intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")) {
            Log.d("GGG", "BROADCAST connectvity change 1");
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            //should check null because in airplane mode it will be null
            if (netInfo != null && netInfo.isConnected()) {

            /*final ConnectivityManager connMgr = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            final android.net.NetworkInfo wifi = connMgr
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            final android.net.NetworkInfo mobile = connMgr
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if (wifi.isAvailable() || mobile.isAvailable()) {*/
                // Do something

                Log.d("GGG", "net work available 1");
            }
        } else if (intent.getAction().equals("com.hash.around.MESSAGE_SCHEDULE")) {
            Long row = intent.getLongExtra("chatrow", 0);
            int intent_type = intent.getIntExtra("intent_type", 0);

            String userid = intent.getStringExtra("userid");
            int usertype = intent.getIntExtra("usertype", 0);
            if (row != 0) {
                if (intent_type == 0) {
                    data = DatabaseManager.getSingleChatFromRowID(context, row);
                    Log.e("MGG", "BroadCAST RECEIVED ");
                    if (userid != null) {
                        Log.e("MGG", "BroadCAST MSG " + data.msg);
                        if(data.temp_url.equals("null"))
                        ChatSender.uploadTextData(data, userid, usertype, null);
                        else
                            ChatSender.uploadMediaData(data.temp_url,data, null);
                    }
                } else if (intent_type == 1) {
                    long rowsRemoved = DatabaseManager.removeSingleChatFromRowID(context, row);
                    Log.e("GGG", "deleted roes: " + rowsRemoved);
                    if (rowsRemoved == 1)
                        ChatSender.updateChatWindow(context, 2, row, userid,null);
                }
            }
            Log.e("GGG", "BroadCAST NULL");
        }
        else
        {
            Log.e("GGG", "BroadCAST UNKNOWN");
        }
    }
}