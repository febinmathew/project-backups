package com.hash.around.Views.VideoTrimmer;

/**
 * Created by Febin on 5/25/2018.
 */


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.TouchDelegate;
import android.view.ViewConfiguration;
import android.view.ViewGroup;

import com.hash.around.R;

/**
 * Create On 16/10/2016
 * @author wayne
 */
public class AroundRangeSlider extends ViewGroup {

    private static final int DEFAULT_LINE_SIZE = 1;
    private static final int DEFAULT_THUMB_WIDTH = 20;
    private static final int DEFAULT_TICK_START = 0;
    private static final int DEFAULT_TICK_END = 100;
    private static final int DEFAULT_TICK_INTERVAL = 1;
    private static final int DEFAULT_MASK_BACKGROUND = 0xB0000000;
    private static final int DEFAULT_LINE_COLOR = 0xFFFFFFFF;

    private final Paint mLinePaint, mBgPaint;
    private final ThumbView mLeftThumb, mRightThumb,seekerYhumb;

    private int mTouchSlop;
    private int mOriginalX, mLastX;

    private int mThumbWidth;

    private int mTickStart = DEFAULT_TICK_START;
    private int mTickEnd = DEFAULT_TICK_END;
    private int mTickInterval = DEFAULT_TICK_INTERVAL;
    private int mTickCount = (mTickEnd - mTickStart) / mTickInterval;

    private float mLineSize;

    private boolean mIsDragging;

    private RangeSliderListner mRangeSliderListner;

    public AroundRangeSlider(Context context) {
        this(context, null);
    }

    public AroundRangeSlider(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AroundRangeSlider(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.AroundRangleSlider, 0, 0);
        mThumbWidth = array.getDimensionPixelOffset(R.styleable.AroundRangleSlider_thumbWidth, DEFAULT_THUMB_WIDTH);

        int tempLineColor=array.getColor(R.styleable.AroundRangleSlider_lineColor, DEFAULT_LINE_COLOR);
        mLineSize = array.getDimensionPixelOffset(R.styleable.AroundRangleSlider_lineHeight, DEFAULT_LINE_SIZE);
        mBgPaint = new Paint();
        mBgPaint.setColor(array.getColor(R.styleable.AroundRangleSlider_maskColor, DEFAULT_MASK_BACKGROUND));

        mLinePaint = new Paint();
        mLinePaint.setColor(tempLineColor);

        mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();

        Drawable lDrawable = array.getDrawable(R.styleable.AroundRangleSlider_leftThumbDrawable);
        Drawable rDrawable = array.getDrawable(R.styleable.AroundRangleSlider_rightThumbDrawable);
        mLeftThumb = new ThumbView(context, mThumbWidth, lDrawable == null ? new ColorDrawable(tempLineColor) : lDrawable,true);
        mRightThumb = new ThumbView(context, mThumbWidth, lDrawable == null ? new ColorDrawable(tempLineColor) : lDrawable,true);

        seekerYhumb =new ThumbView(context, 5, lDrawable == null ? new ColorDrawable(Color.RED) : lDrawable,false);


        setTickCount(array.getInteger(R.styleable.AroundRangleSlider_tickCount, DEFAULT_TICK_END));
        setRangeIndex(array.getInteger(R.styleable.AroundRangleSlider_leftThumbIndex, DEFAULT_TICK_START),
                array.getInteger(R.styleable.AroundRangleSlider_rightThumbIndex, mTickCount));
        array.recycle();

        addView(seekerYhumb);
        addView(mLeftThumb);
        addView(mRightThumb);

        setWillNotDraw(false);

        /*final Rect rect = new Rect();
        mRightThumb.getHitRect(rect);
        rect.top -= 100;    // increase top hit area
        rect.left -= 200;   // increase left hit area
        rect.bottom += 100; // increase bottom hit area
        rect.right += 100;  // increase right hit area
        setTouchDelegate( new TouchDelegate( rect , mRightThumb));*/
    }

    public void setThumbWidth(int thumbWidth) {
        mThumbWidth = thumbWidth;
        mLeftThumb.setThumbWidth(thumbWidth);
        mRightThumb.setThumbWidth(thumbWidth);
    }

    public void setLeftThumbDrawable(Drawable drawable) {
        mLeftThumb.setThumbDrawable(drawable);
    }

    public void setRightThumbDrawable(Drawable drawable) {
        mRightThumb.setThumbDrawable(drawable);
    }

    public void setLineColor(int color) {
        mLinePaint.setColor(color);
    }

    public void setLineSize(float lineSize) {
        mLineSize = lineSize;
    }

    public void setMaskColor(int color) {
        mBgPaint.setColor(color);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        widthMeasureSpec = MeasureSpec.makeMeasureSpec(MeasureSpec.getSize(widthMeasureSpec), MeasureSpec.EXACTLY);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        mLeftThumb.measure(widthMeasureSpec, heightMeasureSpec);
        mRightThumb.measure(widthMeasureSpec, heightMeasureSpec);
        //seekerYhumb.measure(widthMeasureSpec,heightMeasureSpec);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        final int lThumbWidth = mLeftThumb.getMeasuredWidth();
        final int lThumbHeight = mLeftThumb.getMeasuredHeight();
        mLeftThumb.layout(0, 0, lThumbWidth, lThumbHeight);
        mRightThumb.layout(0, 0, lThumbWidth, lThumbHeight);
       // seekerYhumb.layout(0,0,seekerYhumb.getMeasuredWidth(),lThumbHeight);
       // setSeekerIndex(50);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        moveThumbByIndex(mLeftThumb, mLeftThumb.getRangeIndex());
        moveThumbByIndex(mRightThumb, mRightThumb.getRangeIndex());
        moveThumbByIndex(seekerYhumb,50);

    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        final int width = getMeasuredWidth();
        final int height = getMeasuredHeight();

        final int lThumbWidth = mLeftThumb.getMeasuredWidth();
        final float lThumbOffset = mLeftThumb.getX();
        final float rThumbOffset = mRightThumb.getX();

        final float lineTop =  mLineSize;
        final float lineBottom = height - mLineSize;


        // top line
        canvas.drawRect(lThumbWidth/2 + lThumbOffset,
                0,
                rThumbOffset+lThumbWidth/2,
                lineTop, mLinePaint);

        // bottom line
        canvas.drawRect(lThumbWidth/2 + lThumbOffset,
                lineBottom,
                rThumbOffset+lThumbWidth/2,
                height, mLinePaint);


    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        final float lThumbOffset = mLeftThumb.getX();
        final int height = getMeasuredHeight();
        final float rThumbOffset = mRightThumb.getX();
        final int width = getMeasuredWidth();

        if (lThumbOffset > 0) {
            canvas.drawRect(0, 0, lThumbOffset + mThumbWidth/2, height, mBgPaint);

        }
        if (rThumbOffset < width - mThumbWidth) {
            canvas.drawRect(rThumbOffset+ mThumbWidth/2, 0, width, height, mBgPaint);
        }
        //canvas.drawRect(50, 0, width, height, mBgPaint);

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (!isEnabled()) {
            return false;
        }

        boolean handle = false;

        switch (event.getAction()) {

            case MotionEvent.ACTION_DOWN:
                int x = (int) event.getX();
                int y = (int) event.getY();

                mLastX = mOriginalX = x;
                mIsDragging = false;

                if (!mLeftThumb.isPressed() && mLeftThumb.inInTarget(x, y)) {
                    mLeftThumb.setPressed(true);
                    handle = true;
                }
                else if (!mRightThumb.isPressed() && mRightThumb.inInTarget(x, y)) {
                    mRightThumb.setPressed(true);
                    handle = true;
                }
                else if(!seekerYhumb.isPressed() && seekerYhumb.inInTarget(x, y))
                {
                    //seekerYhumb.setPressed(true);
                    handle = true;
                }
                else
                {
                    mLeftThumb.setPressed(true);
                    mRightThumb.setPressed(true);
                    //seekerYhumb.setPressed(true);
                    handle = true;
                    // moveSeekerThumbByPixel(x);
                }
                break;

            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                mIsDragging = false;
                mOriginalX = mLastX = 0;
                getParent().requestDisallowInterceptTouchEvent(false);
                if (mLeftThumb.isPressed()&&!mRightThumb.isPressed()) {
                    releaseLeftThumb();
                    invalidate();
                    handle = true;

                }
                else if(mRightThumb.isPressed()&&!mLeftThumb.isPressed()) {
                    releaseRightThumb();
                    invalidate();
                    handle = true;
                }
                else if(seekerYhumb.isPressed()) {
                    releaseSeekerThumb();
                    invalidate();
                    handle = true;
                }
                else
                {
                    releaseLeftThumb();
                    releaseRightThumb();
                    //releaseSeekerThumb();
                    handle=true;
                }
                break;

            case MotionEvent.ACTION_MOVE:
                x = (int) event.getX();

                if (!mIsDragging && Math.abs(x - mOriginalX) > mTouchSlop) {
                    mIsDragging = true;
                }
                if(mIsDragging) {
                    int moveX = x - mLastX;
                    if (mLeftThumb.isPressed()&&!mRightThumb.isPressed()) {
                        getParent().requestDisallowInterceptTouchEvent(true);
                        moveLeftThumbByPixel(moveX);
                        handle = true;
                        invalidate();
                    }
                    else if (mRightThumb.isPressed()&&!mLeftThumb.isPressed()) {
                        getParent().requestDisallowInterceptTouchEvent(true);
                        moveRightThumbByPixel(moveX);
                        handle = true;
                        invalidate();
                    }
                    else if (seekerYhumb.isPressed()) {
                        getParent().requestDisallowInterceptTouchEvent(true);
                        moveSeekerThumbByPixel(moveX);
                        handle = true;
                        invalidate();
                    }
                    else
                    {
                        // getParent().requestDisallowInterceptTouchEvent(true);
                        moveWholeViewByPixel(moveX);
                        handle = true;
                        invalidate();
                    }
                }

                mLastX = x;
                break;
        }

        return handle;
    }

    private boolean isValidTickCount(int tickCount) {
        return (tickCount > 1);
    }

    private boolean indexOutOfRange(int leftThumbIndex, int rightThumbIndex) {
        return (leftThumbIndex < 0 || leftThumbIndex > mTickCount
                || rightThumbIndex < 0
                || rightThumbIndex > mTickCount);
    }

    private float getRangeLength() {
        int width = getMeasuredWidth();
        if (width < mThumbWidth) {
            return 0;
        }
        return width - mThumbWidth;
    }

    private float getIntervalLength() {
        return getRangeLength() / mTickCount;
    }

    public int getNearestIndex(float x) {
        return Math.round(x / getIntervalLength());
    }

    public int getLeftIndex() {
        return mLeftThumb.getRangeIndex();
    }

    public int getRightIndex() {
        return mRightThumb.getRangeIndex();
    }

    private void notifyRangeChange() {
        if (mRangeSliderListner != null) {

            float interval = getIntervalLength();

            float x =  mLeftThumb.getWidth();
            float pos=mLeftThumb.getX()+mLeftThumb.getWidth();
            float end = mTickEnd / mTickInterval * interval+ mLeftThumb.getWidth() - mLeftThumb.getWidth();
            float distance=end-x;
            float seekPos=((pos-x)/distance)*100;

            float seekPosY=((mRightThumb.getX()-x)/distance)*100;


            //final int width = getMeasuredWidth();

            //float val=mLeftThumb.getX()+mLeftThumb.getWidth()
            mRangeSliderListner.onRangeChange(this,seekPos , seekPosY);

        }
    }

    public void addOnRangeSlideListener(RangeSliderListner rangeChangeListener) {
        mRangeSliderListner = rangeChangeListener;
    }

    /**
     * Sets the tick count in the RangeSlider.
     *
     * @param count Integer specifying the number of ticks.
     */
    public void setTickCount(int count) {
        int tickCount = (count - mTickStart) / mTickInterval;
        if (isValidTickCount(tickCount)) {
            mTickEnd = count;
            mTickCount = tickCount;
            mRightThumb.setTickIndex(mTickCount);
        } else {
            throw new IllegalArgumentException("tickCount less than 2; invalid tickCount.");
        }
    }

    /**
     * The location of the thumbs according by the supplied index.
     * Numbered from 0 to mTickCount - 1 from the left.
     * @param leftIndex Integer specifying the index of the left thumb
     * @param rightIndex Integer specifying the index of the right thumb
     */
    public void setRangeIndex(int leftIndex, int rightIndex) {
        if (indexOutOfRange(leftIndex, rightIndex)) {
            throw new IllegalArgumentException(
                    "Thumb index left " + leftIndex + ", or right " + rightIndex
                            + " is out of bounds. Check that it is greater than the minimum ("
                            + mTickStart + ") and less than the maximum value ("
                            + mTickEnd + ")");
        } else {
            if(mLeftThumb.getRangeIndex() != leftIndex) {
                mLeftThumb.setTickIndex(leftIndex);
            }
            if(mRightThumb.getRangeIndex() != rightIndex) {
                mRightThumb.setTickIndex(rightIndex);
            }
        }
    }

    private boolean moveThumbByIndex(ThumbView view, int index) {
        view.setX(index * getIntervalLength());
        if(view.getRangeIndex() != index) {
            view.setTickIndex(index);
            return true;
        }
        return false;
    }
    public void setSeekerIndex(float pos)
    {
        //pos=100;
        float interval = getIntervalLength();

        float x =  mLeftThumb.getWidth();
        float end = mTickEnd / mTickInterval * interval+ mLeftThumb.getWidth() - mLeftThumb.getWidth();
        float distance=end-x;
        float seekPos=x+(pos*distance)/100;

        final int width = getMeasuredWidth();

        seekerYhumb.setX(seekPos-seekerYhumb.getWidth()/2);

    }
    private void releaseSeekerThumb() {
        int index = getNearestIndex(seekerYhumb.getX());
        int endIndex = seekerYhumb.getRangeIndex();
        if (index <= endIndex) {
            index = endIndex + 1;
        }
        if (moveThumbByIndex(seekerYhumb, index)) {
            //notifyRangeChange();
        }
        seekerYhumb.setPressed(false);
    }
    private void moveSeekerThumbByPixel(int pixel) {
        float x = seekerYhumb.getX() + pixel;
        float interval = getIntervalLength();
        float start = mTickStart / mTickInterval * interval;
        float end = mTickEnd / mTickInterval * interval;

        if (x > start && x < end && x > mLeftThumb.getX() + mThumbWidth && x < mRightThumb.getX()) {
            seekerYhumb.setX(x);
            int index = getNearestIndex(x);
            if (seekerYhumb.getRangeIndex() != index) {
                seekerYhumb.setTickIndex(index);
                //notifyRangeChange();
            }
        }
    }

    private void moveWholeViewByPixel(int pixel)
    {
        float x = mRightThumb.getX() + pixel;
        float y = mLeftThumb.getX() + pixel;
        float interval = getIntervalLength();
        float start = mTickStart / mTickInterval * interval;
        float end = mTickEnd / mTickInterval * interval;

        if((x > start && x < end && x > mLeftThumb.getX() + mThumbWidth)&&(y > start && y < end && y < mRightThumb.getX() - mThumbWidth))
        {
            moveRightThumbByPixel(pixel);
            moveLeftThumbByPixel(pixel);

        }
    }

    private void moveLeftThumbByPixel(int pixel) {
        ///seekerYhumb.setTickIndex(75);
        //xxx
        float x = mLeftThumb.getX() + pixel;
        float interval = getIntervalLength();
        float start = mTickStart / mTickInterval * interval;
        float end = mTickEnd / mTickInterval * interval;

        if (x > start && x < end && x < mRightThumb.getX() - mThumbWidth) {
            mLeftThumb.setX(x);
            //seekerYhumb.setX(x);
            int index = getNearestIndex(x);
            if (mLeftThumb.getRangeIndex() != index) {
                mLeftThumb.setTickIndex(index);
                //seekerYhumb.setTickIndex(index);
                notifyRangeChange();
            }
        }
    }

    private void moveRightThumbByPixel(int pixel) {
        float x = mRightThumb.getX() + pixel;
        float interval = getIntervalLength();
        float start = mTickStart / mTickInterval * interval;
        float end = mTickEnd / mTickInterval * interval;

        if (x > start && x < end && x > mLeftThumb.getX() + mThumbWidth) {
            mRightThumb.setX(x);
            int index = getNearestIndex(x);
            if (mRightThumb.getRangeIndex() != index) {
                mRightThumb.setTickIndex(index);
                notifyRangeChange();
            }
        }
    }

    private void releaseLeftThumb() {
        int index = getNearestIndex(mLeftThumb.getX());
        int endIndex = mRightThumb.getRangeIndex();
        if (index >= endIndex) {
            index = endIndex - 1;
        }
        if (moveThumbByIndex(mLeftThumb, index)) {
            notifyRangeChange();
        }
        mLeftThumb.setPressed(false);
    }

    private void releaseRightThumb() {
        int index = getNearestIndex(mRightThumb.getX());
        int endIndex = mLeftThumb.getRangeIndex();
        if (index <= endIndex) {
            index = endIndex + 1;
        }
        if (moveThumbByIndex(mRightThumb, index)) {
            notifyRangeChange();
        }
        mRightThumb.setPressed(false);
    }

    /*public interface OnRangeChangeListener {
        void onRangeChange(AroundRangleSlider view, int leftPinIndex, int rightPinIndex);
    }*/

}