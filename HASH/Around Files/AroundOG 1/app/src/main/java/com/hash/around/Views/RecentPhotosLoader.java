package com.hash.around.Views;


import android.Manifest;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.content.CursorLoader;

//import org.thoughtcrime.securesms.permissions.Permissions;

public class RecentPhotosLoader extends CursorLoader {

  public static Uri BASE_URL = MediaStore.Files.getContentUri("external");

  /*private static final String[] PROJECTION = new String[] {
      MediaStore.Images.ImageColumns._ID,
      MediaStore.Images.ImageColumns.DATE_TAKEN,
      MediaStore.Images.ImageColumns.DATE_MODIFIED,
      //MediaStore.Images.ImageColumns.ORIENTATION,
      MediaStore.Images.ImageColumns.MIME_TYPE
  };*/
  private static final String[] PROJECTION  = {
          MediaStore.Files.FileColumns._ID,
          MediaStore.Files.FileColumns.DATA,
          MediaStore.Files.FileColumns.DATE_ADDED,
          MediaStore.Files.FileColumns.MEDIA_TYPE,
          MediaStore.Files.FileColumns.MIME_TYPE,
          MediaStore.Files.FileColumns.TITLE
  };

  private static final String SELECTION = MediaStore.Files.FileColumns.MEDIA_TYPE + "="
          + MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE
          + " OR "
          + MediaStore.Files.FileColumns.MEDIA_TYPE + "="
          + MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO;


  private final Context context;

  public RecentPhotosLoader(Context context) {
    super(context);
    this.context = context.getApplicationContext();
  }

  @Override
  public Cursor loadInBackground() {
   // if (Permissions.hasAll(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
      return context.getContentResolver().query(BASE_URL,
                                                PROJECTION, SELECTION, null,
              MediaStore.Files.FileColumns.DATE_MODIFIED + " DESC");

    //} else {
      //return null;
   // }
  }


}
