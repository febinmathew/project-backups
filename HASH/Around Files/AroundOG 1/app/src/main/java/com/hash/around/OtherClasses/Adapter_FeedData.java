package com.hash.around.OtherClasses;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.hash.around.OtherClasses.DataTypes.FeedData;
import com.hash.around.OtherClasses.DataTypes.FeedDataList;
import com.hash.around.OtherClasses.DataTypes.GroupFeedData;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.OtherClasses.Interfaces.ProcessCompletedListner;
import com.hash.around.R;
import com.hash.around.TabFragments.FeedFullScreen;
import com.hash.around.TabbedActivity;
import com.hash.around.Utils.DatabaseManager;
import com.hash.around.Utils.FeedSender;
import com.hash.around.Utils.StringUtils;
import com.hash.around.Views.Medias.AroundChatAudioThumbline;
import com.hash.around.Views.Medias.AroundChatImageThumbline;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;




/**
 * Created by Febin on 26-08-2017.
 */

public class Adapter_FeedData extends RecyclerView.Adapter implements View.OnCreateContextMenuListener{

    public static int optionPosition=-1;
    Activity mContext;
    public ArrayList<GroupFeedData> data;
    ArrayList<FeedDataList> data2;
    private ProcessCompletedListner onLoadMoreListener;
    private int visibleThreshold = 1;
    private int lastVisibleItem,prevlastVisibleItem=-2, totalItemCount;
    private boolean loading;
    public Adapter_FeedData(Activity mcont, ArrayList<GroupFeedData> list,ArrayList<FeedDataList> list2,RecyclerView recyclerView)
    {
        mContext=mcont;
        Log.e("GGG","feed size is "+list.size());
        data=list;
        data2=list2;


        final Context ccc=mContext;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                .getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrolled(RecyclerView recyclerView,
                                           int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);

                        totalItemCount = linearLayoutManager.getItemCount();
                        lastVisibleItem = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
                        if ( lastVisibleItem == 0) {
                            // End has been reached
                            if(!loading) {
                                prevlastVisibleItem = lastVisibleItem;

                                Toast.makeText(ccc, "feeds: " + totalItemCount + " " + lastVisibleItem, Toast.LENGTH_SHORT).show();
                                // Do something
                                if (onLoadMoreListener != null) {
                                    onLoadMoreListener.processCompleteAction();
                                }
                                loading = false;
                            }
                        }
                    }
                });
    }

    public void setOnLoadMoreListener(ProcessCompletedListner processListner) {
        this.onLoadMoreListener = processListner;
    }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v=null;

        switch(viewType)
        {
            case 0:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.feed_text, parent, false);
                // set the view's size, margins, paddings and layout parameters

                ViewHolder0 vh = new ViewHolder0(v);
                return vh;

            case 1:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.feed_image, parent, false);
                // set the view's size, margins, paddings and layout parameters

                ViewHolder1 vh1 = new ViewHolder1(v);
                return vh1;
            case 2:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.feed_audio, parent, false);
                // set the view's size, margins, paddings and layout parameters

                AudioFeedView vh2 = new AudioFeedView(v);
                return vh2;

            /*case 3:

                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.feed_image, parent, false);
                // set the view's size, margins, paddings and layout parameters

                ViewHolder2 vh_video = new ViewHolder2(v);
                return vh_video;*/

        }

return null;

    }




   /* public static void updateMediaTableFromMediaRowID(long rowID,long global)
    {

        SQLiteDatabase db= TabbedActivity.db.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseAccesser.media_global_uri,global);
        values.put(DatabaseAccesser.media_local_uri,locallite);
        int rowln=db.update(DatabaseAccesser.media_table,
                values,
                " media_lite_local"+ " = ?",
                new String[]{locallite});
    }*/


   void setTime(TextView text,long timestamp)
   {

   }

   void openFullFeedView(String userID, String userName, Long targetFeed)
   {
       Intent indi=new Intent(mContext,FeedFullScreen.class);
       indi.putExtra("username",userName);
       indi.putExtra("userid",userID);
       indi.putExtra("targetFeed",targetFeed);
       mContext.startActivity(indi);
   }
    public void TextViewResizable(final TextView tv,final int maxLine, final String expandText) {
        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                //xxx
                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine <= 0) {
                    int lineEndIndex = tv.getLayout().getLineEnd(0);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, expandText), TextView.BufferType.SPANNABLE);
                } else if (tv.getLineCount() >= maxLine) {
                    int lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, expandText), TextView.BufferType.SPANNABLE);
                }
            }
        });
    }
    private  SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned, final TextView tv, final String expandText) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(expandText)) {
            ssb.setSpan(new Spannable(Color.BLUE, true) {
                @Override
                public void onClick(View widget) {
                    tv.setLayoutParams(tv.getLayoutParams());
                    tv.setText(tv.getTag().toString(),TextView.BufferType.SPANNABLE);
                    tv.invalidate();
                }
            }, str.indexOf(expandText), str.indexOf(expandText)+ expandText.length(), 0);

        }
        return ssb;

    }
    class Spannable extends ClickableSpan {

        private int color = -1;
        private float fontSize = -1;
        private boolean isUnderline = true;

        /**
         * Constructor
         */
        public Spannable() {
        }

        /**
         * Constructor
         */
        public Spannable(int color) {
            this.color = color;
        }

        /**
         * Constructor
         */
        public Spannable(float fontSize) {
            this.fontSize = fontSize;
        }

        /**
         * Constructor
         */
        public Spannable(boolean isUnderline) {
            this.isUnderline = isUnderline;
        }

        /**
         * Constructor
         */
        public Spannable(int color, boolean isUnderline) {
            this.isUnderline = isUnderline;
            this.color = color;
        }

        /**
         * Constructor
         */
        public Spannable(int color, float fontSize) {
            this.color = color;
            this.fontSize = fontSize;
        }

        /**
         * Overrides methods
         */
        @Override
        public void updateDrawState(TextPaint ds) {

            if (color != -1) {
                ds.setColor(color);
            }
            if (fontSize > 0) {
                ds.setTextSize(fontSize);
            }

            ds.setUnderlineText(isUnderline);

        }

        @Override
        public void onClick(View widget) {

        }
    }
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final int pos=position;


        String profurl="null";

        SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();


        Cursor cursor = db.rawQuery(" select " +
                DatabaseAccesser.user_localUrl + " from " + DatabaseAccesser.user_table + " where " +
                DatabaseAccesser.user_user_id + " = ?"+" limit 1", new String[]{data.get(pos).userid});

        Log.e("MSG","counts of user "+cursor.getCount());
        while (cursor.moveToNext()) {

            profurl = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_localUrl));
        }
        Log.e("MSG","profile url "+profurl);

        cursor.close();

        //setupprogress((LinearLayout) holder.itemView.findViewById(R.id.progressLayout),data2.get(pos).seen,data2.get(pos).unseen);
        //((TextView) holder.itemView.findViewById(R.id.cTimeText)).setText(getTimeFromMils(data.get(pos).timestamp));

        switch(data.get(pos).type)
        {
            case 0:
                if(profurl.equals("null"))
                    ((ViewHolder0) holder).proimg.setImageResource(R.drawable.default_user);
                else
                    ((ViewHolder0) holder).proimg.setImageURI(Uri.parse(profurl));
                ((ViewHolder0) holder).userName.setText(data.get(pos).username);

                ((ViewHolder0) holder).tx.setText(data.get(position).text);
                ((ViewHolder0) holder).cTimeText.setText(getTimeFromMils(data.get(pos).timestamp));

                setupprogress(((ViewHolder0) holder).progressLayout,data2.get(pos).seen,data2.get(pos).unseen);

                /*final TextView pq=((ViewHolder0)holder).tx;
                ((ViewHolder0) holder).imgexpand.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        pq.setMaxLines(10);
                    }
                });*/


                //Log.e("GGG", "ellipsized "+pq.getEllipsize());

                setUpContextMenu(((ViewHolder0)holder).cv,((ViewHolder0)holder).popup_menu,pos);

                break;

            case 1:
                /*Image Starts Here*/
                final ViewHolder1 feedView=((ViewHolder1) holder);
                   if(profurl.equals("null"))
                   feedView.proimg.setImageResource(R.drawable.default_user);
                else
                       ((ViewHolder1) holder).proimg.setImageURI(Uri.parse(profurl));
                feedView.userName.setText(data.get(pos).username);

                Log.e("MSGG","SECOND: "+ data.get(pos).uri+" value :"+data.get(pos).text );
                ((ViewHolder1) holder).aroundImageView.setMediaType(data.get(pos).type);
                if(data.get(position).uri.contains("https"))
                {
                    ((ViewHolder1)holder).img.setImageURI(Uri.parse(data.get(position).semi_uri));
                    ((ViewHolder1) holder).aroundImageView.setSizeText(StringUtils.mediaSizeToString(data.get(pos).mediaSize));
                    ((ViewHolder1)holder).aroundImageView.setDownloadClickListner(data.get(position).uri);
                    ((ViewHolder1)holder).aroundImageView.setOnJobCompletionListner(new JobCompletionListner() {
                        @Override
                        public void onJobCompletion(String text1) {

                            DatabaseManager.updateMediaTableFromGlobalUrl(data.get(pos).uri,text1);

                            data.get(pos).uri=text1;
                            //((ViewHolder1)holder).aroundImageView.disableDownloadClick();
                            feedView.aroundImageView.setLocallyAvailable(true);

                        }
                    });
                    feedView.aroundImageView.setLocallyAvailable(false);
                }
                else
                {

                    ((ViewHolder1)holder).img.setImageURI(Uri.parse(data.get(position).uri));
                    ((ViewHolder1)holder).aroundImageView.setLocallyAvailable(true);
                    }
                ((ViewHolder1)holder).aroundImageView.setImageScaleType(false);


                if(data.get(position).text.equals("")||data.get(position).text.equals("null"))
                    ((ViewHolder1) holder).textSection.setVisibility(View.GONE);
                else {
                    ((ViewHolder1) holder).textSection.setVisibility(View.VISIBLE);
                    ((ViewHolder1) holder).tx.setText(data.get(position).text);
                    final TextView p=((ViewHolder1)holder).tx;
                    ((ViewHolder1) holder).imgexpand.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            p.setMaxLines(10);
                        }
                    });
                    Log.e("GGG","feed lines is "+p.getLineCount());//CODE 47
                    //TextViewResizable(((ViewHolder1)holder).tx,1,"See More");
                }


                setUpContextMenu(((ViewHolder1)holder).cv,((ViewHolder1)holder).popup_menu,pos);

                ((ViewHolder1) holder).cTimeText.setText(getTimeFromMils(data.get(pos).timestamp));
                setupprogress(((ViewHolder1) holder).progressLayout,data2.get(pos).seen,data2.get(pos).unseen);
                setUpRespondPanel(((ViewHolder1) holder).respondPanel,data.get(pos).userid,pos);

                break;
            case 2:
                final AudioFeedView feedView3=((AudioFeedView) holder);
                if(profurl.equals("null"))
                    feedView3.proimg.setImageResource(R.drawable.default_user);
                else
                    ((AudioFeedView) holder).proimg.setImageURI(Uri.parse(profurl));
                feedView3.userName.setText(data.get(pos).username);



                if(data.get(position).uri.contains("https"))
                {
                    //((AudioFeedView)holder).audioView.setAudioUri(data.get(position).uri);
                    ((AudioFeedView) holder).audioView.setSizeText(StringUtils.mediaSizeToString(data.get(pos).mediaSize));
                    ((AudioFeedView)holder).audioView.setDownloadClickListner(data.get(position).uri);
                    ((AudioFeedView)holder).audioView.setOnJobCompletionListner(new JobCompletionListner() {
                        @Override
                        public void onJobCompletion(String text1) {

                            DatabaseManager.updateMediaTableFromGlobalUrl(data.get(pos).uri,text1);

                            data.get(pos).uri=text1;
                            //((ViewHolder1)holder).aroundImageView.disableDownloadClick();
                            feedView3.audioView.setLocallyAvailable(true);

                        }
                    });
                    feedView3.audioView.setLocallyAvailable(false);
                }
                else
                {

                    ((AudioFeedView)holder).audioView.setAudioUri(data.get(position).uri);
                    ((AudioFeedView)holder).audioView.setLocallyAvailable(true);
                }



                if(data.get(position).text.equals("")||data.get(position).text.equals("null"))
                    ((AudioFeedView) holder).textSection.setVisibility(View.GONE);
                else {
                    ((AudioFeedView) holder).textSection.setVisibility(View.VISIBLE);
                    ((AudioFeedView) holder).tx.setText(data.get(position).text);
                    final TextView p=((AudioFeedView)holder).tx;
                    ((AudioFeedView) holder).imgexpand.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            p.setMaxLines(10);
                        }
                    });
                    Log.e("GGG","feed lines is "+p.getLineCount());//CODE 47
                    //TextViewResizable(((ViewHolder1)holder).tx,1,"See More");
                }

                setUpContextMenu(((AudioFeedView)holder).cv,((AudioFeedView)holder).popup_menu,pos);

                ((AudioFeedView) holder).cTimeText.setText(getTimeFromMils(data.get(pos).timestamp));
                setupprogress(((AudioFeedView) holder).progressLayout,data2.get(pos).seen,data2.get(pos).unseen);
                setUpRespondPanel(((AudioFeedView) holder).respondPanel,data.get(pos).userid,pos);

                Log.e("MSGG","SECOND: "+ data.get(pos).semi_uri+" value :"+data.get(pos).text );
                break;
            case 3:
            case 5:
                final ViewHolder1 feedView2=((ViewHolder1) holder);
                if(profurl.equals("null"))
                    feedView2.proimg.setImageResource(R.drawable.default_user);
                else
                    ((ViewHolder1) holder).proimg.setImageURI(Uri.parse(profurl));
                feedView2.userName.setText(data.get(pos).username);


                Log.e("MSGG","SECOND: "+ data.get(pos).uri+" value :"+data.get(pos).text );
                ((ViewHolder1)holder).img.setImageURI(Uri.parse(data.get(position).semi_uri));
                ((ViewHolder1) holder).aroundImageView.setMediaType(data.get(pos).type);

                if(data.get(position).uri.contains("https"))
                {
                    ((ViewHolder1) holder).aroundImageView.setSizeText(StringUtils.mediaSizeToString(data.get(pos).mediaSize));

                    ((ViewHolder1)holder).aroundImageView.setDownloadClickListner(data.get(position).uri);
                    ((ViewHolder1)holder).aroundImageView.setOnJobCompletionListner(new JobCompletionListner() {
                        @Override
                        public void onJobCompletion(String text1) {

                            DatabaseManager.updateMediaTableFromGlobalUrl(data.get(pos).uri,text1);
                            data.get(pos).uri=text1;
                            feedView2.aroundImageView.setLocallyAvailable(true);

                        }
                    });
                    ((ViewHolder1) holder).aroundImageView.setLocallyAvailable(false);
                }
                else
                {
                    feedView2.aroundImageView.setLocallyAvailable(true);
                }
                ((ViewHolder1)holder).aroundImageView.setImageScaleType(false);


                if(data.get(position).text.equals("")||data.get(position).text.equals("null"))
                    ((ViewHolder1) holder).textSection.setVisibility(View.GONE);
                else {
                    ((ViewHolder1) holder).textSection.setVisibility(View.GONE);
                    ((ViewHolder1) holder).tx.setText(data.get(position).text);
                    final TextView p=((ViewHolder1)holder).tx;
                    ((ViewHolder1) holder).imgexpand.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            p.setMaxLines(10);
                        }
                    });
                }

                setUpContextMenu(((ViewHolder1)holder).cv,((ViewHolder1)holder).popup_menu,pos);
                ((ViewHolder1) holder).cTimeText.setText(getTimeFromMils(data.get(pos).timestamp));
                setupprogress(((ViewHolder1) holder).progressLayout,data2.get(pos).seen,data2.get(pos).unseen);
                setUpRespondPanel(((ViewHolder1) holder).respondPanel,data.get(pos).userid,pos);

                break;

        }

    }
    void setUpContextMenu(View wholeView,View optionMenu,final int pos)
    {
        wholeView.setOnCreateContextMenuListener(this);

        wholeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFullFeedView(data.get(pos).userid,data.get(pos).username,null);

            }
        });

        wholeView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                optionPosition= pos;
                view.showContextMenu();
                return true;
            }
        });
        wholeView.findViewById(R.id.commentFeedButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFullFeedView(data.get(pos).userid,data.get(pos).username,data.get(pos).feedRow);
            }
        });
        optionMenu.setOnCreateContextMenuListener(this);

        optionMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                optionPosition= pos;
                view.showContextMenu();
            }
        });
    }
    void setUpRespondPanel(LinearLayout view,String userID,final int pos)
    {
        if(userID.equals(TabbedActivity.userdetail.getUid()))
        {
            view.setVisibility(View.GONE);
        }
        else
        {
            view.setVisibility(View.VISIBLE);
            view.findViewById(R.id.sharePost).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shareSelectedPost(data.get(pos));
                }
            });
        }
    }

    /*boolean checkContentIsLocalyAvailable(String val)
    {
        if(val.split(">>").length>1)
            return true;
        else
            return false;
    }
    String convertUrlsToUri(String val)
    {
        if(val.split(">>").length>1)
            return val.split(">>")[1];
        else
            return val.split(">>")[0];
    }*/

    void shareSelectedPost(final FeedData feed)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage("Share post?");
        // builder.setTitle("Delete feed?");
        builder.setCancelable(true);
        builder.setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("SHARE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                FeedSender.sharePost(feed,mContext);

                dialog.dismiss();

            }
        });

        AlertDialog alert = builder.create();
        alert.show();


    }

    void setupprogress(LinearLayout progressLayout,int seen,int unseen)
    {
        LayoutInflater vi = (LayoutInflater) mContext.getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ProgressBar progressView;
        progressLayout.removeAllViews();

        for(int i=0;i<seen;i++)
        {


            View v = vi.inflate(R.layout.progress_bar_round, null);
            progressView=(ProgressBar) v.findViewById(R.id.feedProgress);
            //ProgressBar pp=progressView;
            progressView.setLayoutParams(new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,1.0f));

            progressView.setProgress(1);


            progressLayout.addView(progressView);

        }
        for(int i=0;i<unseen;i++)
        {


            View v = vi.inflate(R.layout.progress_bar_round, null);
            progressView=(ProgressBar) v.findViewById(R.id.feedProgress);
            //ProgressBar pp=progressView;
            progressView.setLayoutParams(new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,1.0f));

            progressView.setProgress(0);


           progressLayout.addView(progressView);

        }
    }
    String getTimeFromMils(long msgTime)
    {
        long curTime=System.currentTimeMillis();
        Date dt=new Date(msgTime);
        SimpleDateFormat format;
        if((curTime-86400000)<msgTime) {
            format = new SimpleDateFormat("hh:mm a");
            return format.format(dt);
        }
        else if((curTime-172800000)<msgTime)
        {
            // format = new SimpleDateFormat("EEE MMM dd hh:mm:ss zzz yyyy a");
            return "YESTERDAY";
        }
        else
        {
            format = new SimpleDateFormat("EEE MMM dd");
            return format.format(dt);
        }
       // return "";
    }


    @Override
    public int getItemViewType(int position) {
        switch(data.get(position).type)
        {
            case 0:
                return 0;

            case 1:
                return 1;
            case 2:
                return 2;

            case 3:
                return 1;
            case 5:
                return 1;

        }
        return 4;
    }

    @Override
    public int getItemCount() {
        if(data!=null) {
            return data.size();
        }
        return 0;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo contextMenuInfo) {

//mContext.getApplicationContext()
        menu.clear();
        MenuInflater inflater = new MenuInflater(mContext);
        inflater.inflate(R.menu.feed_pop_up, menu);
        menu.setHeaderTitle(data.get(optionPosition).username);
        menu.setQwertyMode(true);
        if(data.get(optionPosition).userid.equals(TabbedActivity.userdetail.getUid())) {

            menu.findItem(R.id.block_post).setVisible(false);
            menu.findItem(R.id.report_post).setVisible(false);
            menu.findItem(R.id.comment_now).setVisible(false);
            menu.findItem(R.id.view_profile).setTitle("My profile");
        }
       /* menu..setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                Toast.makeText(mContext, "hey", Toast.LENGTH_SHORT).show();
                return true;
            }
        });*/
        //menu.setHeaderTitle("Select The Action");
        //menu.add(0, v.getId(), 0, "Call");
       // menu.add(0, v.getId(), 0, "SMS");
    }




    public static class ViewHolder0 extends RecyclerView.ViewHolder {
        public CardView cv;
        public TextView tx;
        public CircularImageView proimg;
        public TextView userName;
        public TextView cTimeText;
        LinearLayout progressLayout;
        ImageButton popup_menu;
        public ImageButton imgexpand;
        public ViewHolder0(View item) {
            super(item);
            popup_menu=(ImageButton)item.findViewById(R.id.popup_menu);
            cv=(CardView)item.findViewById(R.id.cView);
            tx=(TextView)item.findViewById(R.id.cText);
            proimg=(CircularImageView)item.findViewById(R.id.cProfileImage);
            userName=(TextView)item.findViewById(R.id.cUserNameText);
            cTimeText=(TextView)item.findViewById(R.id.cTimeText);

            progressLayout=(LinearLayout)item.findViewById(R.id.progressLayout);
            imgexpand=(ImageButton)item.findViewById(R.id.button_toggle);
        }
    }


    public static class ViewHolder1 extends RecyclerView.ViewHolder {
        public CardView cv;
        public TextView tx;
        public ImageView img;
        public CircularImageView proimg;
        public ImageButton imgexpand;
        public TextView userName;
        public TextView cTimeText;
        LinearLayout progressLayout;
        LinearLayout textSection;
        AroundChatImageThumbline aroundImageView;


        LinearLayout respondPanel;

        ImageButton popup_menu;
        public ViewHolder1(View item) {
            super(item);
            cv=(CardView)item.findViewById(R.id.cView);
            tx=(TextView)item.findViewById(R.id.cText);

            proimg=(CircularImageView) item.findViewById(R.id.cProfileImage);
            imgexpand=(ImageButton)item.findViewById(R.id.button_toggle);
            userName=(TextView)item.findViewById(R.id.cUserNameText);
            cTimeText=(TextView)item.findViewById(R.id.cTimeText);
            progressLayout=(LinearLayout)item.findViewById(R.id.progressLayout);
            popup_menu=(ImageButton)item.findViewById(R.id.popup_menu);
            textSection=(LinearLayout) item.findViewById(R.id.textSection);

            respondPanel=(LinearLayout) item.findViewById(R.id.respondPanel);


            //mElasticDownloadView=(ElasticDownloadView)item.findViewById(R.id.elastic_download_view);
           // downloadProgressBar = (DownloadProgressBar)item.findViewById(R.id.download_progress_view);
            aroundImageView=(AroundChatImageThumbline) item.findViewById(R.id.ccImage);
            img=aroundImageView.getImageView();
            //fhView=aroundImageView.getDownloadView();


        }
    }


    public static class AudioFeedView extends RecyclerView.ViewHolder {

        public CardView cv;
        public TextView tx;

        public CircularImageView proimg;
        public ImageButton imgexpand;
        public TextView userName;
        public TextView cTimeText;
        LinearLayout progressLayout;
        LinearLayout respondPanel;
        ImageButton popup_menu;
        LinearLayout textSection;

        AroundChatAudioThumbline audioView;


        public AudioFeedView(View item) {
            super(item);
            cv=(CardView)item.findViewById(R.id.cView);
            tx=(TextView)item.findViewById(R.id.cText);

            proimg=(CircularImageView) item.findViewById(R.id.cProfileImage);
            imgexpand=(ImageButton)item.findViewById(R.id.button_toggle);
            userName=(TextView)item.findViewById(R.id.cUserNameText);
            cTimeText=(TextView)item.findViewById(R.id.cTimeText);
            progressLayout=(LinearLayout)item.findViewById(R.id.progressLayout);
            popup_menu=(ImageButton)item.findViewById(R.id.popup_menu);
            textSection=(LinearLayout) item.findViewById(R.id.textSection);

            respondPanel=(LinearLayout) item.findViewById(R.id.respondPanel);

            audioView=(AroundChatAudioThumbline)item.findViewById(R.id.audioSection);


        }
    }

}


