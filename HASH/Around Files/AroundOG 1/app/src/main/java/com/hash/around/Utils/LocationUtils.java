package com.hash.around.Utils;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.format.Time;
import android.util.Log;
import android.view.View;

import com.hash.around.OtherClasses.DataTypes.LocationData;
import com.hash.around.OtherClasses.DataTypes.VCardData;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.R;
import com.hash.around.TabbedActivity;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Created by Febin on 6/13/2018.
 */

public class LocationUtils {

    public static ArrayList<LocationData> getLocationAsList(Context context,boolean active_only)
    {
        SQLiteDatabase db = DatabaseManager.getDatabase().getReadableDatabase();

        Cursor cursor;
        if(active_only)
        {
            cursor = db.query(DatabaseAccesser.location_table, null,
                    DatabaseAccesser.loc_col_status+" =?",
                    new String[]{"1"},
                    null, null, null,null);
        }
        else {
            cursor = db.query(DatabaseAccesser.location_table, new String[]{DatabaseAccesser.loc_col_location_name,
                            DatabaseAccesser.loc_col_status, DatabaseAccesser.loc_col_latitude, DatabaseAccesser.loc_col_longitude},
                    null,
                    null,
                    null, null, null, null);
        }
        ArrayList<LocationData> finalData=new  ArrayList<LocationData>();
        while (cursor.moveToNext()) {

            LocationData temp = new LocationData();

            temp.location_name = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.loc_col_location_name));
            temp.status = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.loc_col_status));
            temp.latitude = cursor.getDouble(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.loc_col_latitude));
            temp.longitude = cursor.getDouble(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.loc_col_longitude));

            finalData.add(temp);
        }
        cursor.close();

        return finalData;
    }
}
