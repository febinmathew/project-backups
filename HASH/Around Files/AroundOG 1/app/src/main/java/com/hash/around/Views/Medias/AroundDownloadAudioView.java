package com.hash.around.Views.Medias;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.audiofx.Visualizer;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.TextureView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnPausedListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.R;
import com.hash.around.Utils.PermissionManager;
import com.hash.around.Views.ViewFunctions;
import com.pitt.library.fresh.FreshDownloadView;

import java.io.File;
import java.io.IOException;


public class AroundDownloadAudioView extends RelativeLayout {



    TextView sizeText;

    View rootView;
    Context mContext;
    ImageView imageContent;

    FreshDownloadView downloadView;
    ImageView playButton;
    AroundVisualizerView visualizerView;
    JobCompletionListner jobComplete=null;



    private MediaPlayer mMediaPlayer;
    private Visualizer mVisualizer;


    public AroundDownloadAudioView(Context context) {
        super(context);
        init(context,null);
    }

    public AroundDownloadAudioView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context,attrs);
    }

    public AroundDownloadAudioView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context,attrs);
    }


    private void init(Context context, @Nullable AttributeSet attrs) {

        mContext=context;


        rootView = inflate(context, R.layout.around_audioview, this);

        imageContent = (ImageView) rootView.findViewById(R.id.imageContent);
        downloadView =(FreshDownloadView) rootView.findViewById(R.id.downloadView);
        visualizerView=rootView.findViewById(R.id.visualizerView);

        sizeText=rootView.findViewById(R.id.sizeText);
        playButton=(ImageView) rootView.findViewById(R.id.playButton);


    }

    public void setLocallyAvailable(boolean locallyAvailable)
    {
        if(locallyAvailable)
        {
            disableDownloadClick();
        }
        else
        {
            enableDownloadClick();
        }
    }

    public void setUpAudioUri(Uri url)
    {

       // setVolumeControlStream(AudioManager.STREAM_MUSIC);
        mMediaPlayer = MediaPlayer.create(mContext, url);

        if(PermissionManager.checkHasPermission(getContext(),((Activity)getContext()),new String[]{
                Manifest.permission.RECORD_AUDIO
        })){
        setupVisualizerFxAndUI();}

        // Make sure the visualizer is enabled only when you actually want to
        // receive data, and
        // when it makes sense to receive data.

        // When the stream ends, we don't need to collect any more data. We
        // don't do this in
        // setupVisualizerFxAndUI because we likely want to have more,
        // non-Visualizer related code
        // in this callback.
        mMediaPlayer
                .setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mediaPlayer) {
                        mVisualizer.setEnabled(false);
                        mMediaPlayer.seekTo(1);
                        pauseControl();
                    }
                });

        playButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mMediaPlayer.isPlaying())
                {
                    pauseControl();
                }
                    else
                {
                    mVisualizer.setEnabled(true);
                    playButton.setImageResource(R.drawable.ic_pause_circle_light_grey);
                    mMediaPlayer.start();
                }
            }
        });
    }

    void pauseControl()
    {
        playButton.setImageResource(R.drawable.ic_play_circle);
        mMediaPlayer.pause();
    }
    private void setupVisualizerFxAndUI() {

        // Create the Visualizer object and attach it to our media player.
        mVisualizer = new Visualizer(mMediaPlayer.getAudioSessionId());
        mVisualizer.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
        mVisualizer.setDataCaptureListener(
                new Visualizer.OnDataCaptureListener() {
                    public void onWaveFormDataCapture(Visualizer visualizer,
                                                      byte[] bytes, int samplingRate) {
                        visualizerView.updateVisualizer(bytes);
                    }

                    public void onFftDataCapture(Visualizer visualizer,
                                                 byte[] bytes, int samplingRate) {
                    }
                }, Visualizer.getMaxCaptureRate() / 2, true, false);
    }

    public void disableDownloadClick()
    {
        Log.e("MSG","disable");
        downloadView.setVisibility(GONE);
        imageContent.setVisibility(GONE);
        playButton.setVisibility(VISIBLE);
        visualizerView.setVisibility(VISIBLE);
        sizeText.setVisibility(GONE);


    }
    public void enableDownloadClick()
    {
        Log.e("MSG","disable");
        downloadView.setVisibility(VISIBLE);
        imageContent.setVisibility(VISIBLE);
        playButton.setVisibility(GONE);
        visualizerView.setVisibility(GONE);
        sizeText.setVisibility(VISIBLE);


    }

}
