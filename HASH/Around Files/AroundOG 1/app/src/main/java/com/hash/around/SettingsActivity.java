package com.hash.around;


import android.annotation.TargetApi;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.preference.ListPreference;
import android.support.v14.preference.MultiSelectListPreference;
import android.support.v7.preference.Preference;

import android.app.ActionBar;


import android.preference.PreferenceManager;
import android.preference.RingtonePreference;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import android.preference.PreferenceFragment;

import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.SwitchPreferenceCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.support.v4.app.NavUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


public class SettingsActivity extends AppCompatActivity {


int stat;
    SettingsPage setingspage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);



        stat=getIntent().getIntExtra("stat",0);
        //Toast.makeText(this, stat+"", Toast.LENGTH_SHORT).show();
        setContentView(R.layout.individual_settings_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Settings");

        FragmentManager fragmentManager= getFragmentManager();

        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();


        SettingsPage.status=stat;
        setingspage= new SettingsPage();
        SettingsPage page=setingspage;

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.linearlay, page)
                .commit();
        //fragmentTransaction.add(R.id.linearlay,page);
       // fragmentTransaction.commit();




    }



    protected void onResume() {
        super.onResume();
        setingspage.getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(setingspage);
    }

    protected void onPause() {
        super.onPause();
        setingspage.getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(setingspage);
    }

    public static class SettingsPage extends PreferenceFragmentCompat implements
            SharedPreferences.OnSharedPreferenceChangeListener
    {

        SharedPreferences sp;
        ListPreference editListPref;
        MultiSelectListPreference editMultiListPref;
        Set<String> selections;
        String[] selected;

        DatabaseReference databaseRef;
        public static int status;
        FirebaseUser userdetail;


        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey)

        {
            userdetail = FirebaseAuth.getInstance().getCurrentUser();

            switch(status)
            {
                case 0:
                    addPreferencesFromResource(R.xml.profile_settings);

                    break;
                case 1:
                    addPreferencesFromResource(R.xml.privacy_settings);

                    sp = getPreferenceScreen().getSharedPreferences();
                    // sp.getString("key","deaful_tstring");
                    editListPref = (ListPreference) findPreference("profile_image_privacy");
                    editListPref.setSummary(editListPref.getEntry());

                    editListPref = (ListPreference) findPreference("profile_status_privacy");
                    editListPref.setSummary(editListPref.getEntry());

                    editListPref = (ListPreference) findPreference("profile_number_privacy");
                    editListPref.setSummary(editListPref.getEntry());

                    editListPref = (ListPreference) findPreference("profile_activity_status_privacy");
                    editListPref.setSummary(editListPref.getEntry());

                    editListPref = (ListPreference) findPreference("profile_music_privacy");
                    editListPref.setSummary(editListPref.getEntry());

                    editListPref = (ListPreference) findPreference("profile_location_privacy");
                    editListPref.setSummary(editListPref.getEntry());


                    break;
                case 2:
                    addPreferencesFromResource(R.xml.data_usage2);
                    sp = getPreferenceScreen().getSharedPreferences();


                    editMultiListPref = (MultiSelectListPreference) findPreference("data_usage_chat");
                    selections =sp.getStringSet("data_usage_chat",null);
                    selected = selections.toArray(new String[] {});
                    editMultiListPref.setSummary(customStringMaker(selected)+ "");

                    editMultiListPref = (MultiSelectListPreference) findPreference("data_usage_feed");
                    selections =sp.getStringSet("data_usage_feed",null);
                    selected = selections.toArray(new String[] {});
                    // Toast.makeText(getActivity().getApplicationContext(), selected[0]+selected[1]+selected[2]+selected[3]+selected[4]+selected[5]+selected[6], Toast.LENGTH_SHORT).show();
                    editMultiListPref.setSummary(customStringMaker(selected)+ "");



                    break;
                case 3:
                    addPreferencesFromResource(R.xml.feed_settings);
                    sp = getPreferenceScreen().getSharedPreferences();
                    break;
            }
        }


        String customStringMaker(String[] selections)
        {
            String[] arrayValues = getResources().getStringArray(R.array.data_usage_status_title);
            String text=new String();
            text="";
            //Object[] selectedItems=selections.toArray();
            ArrayList<Integer> data=new ArrayList<Integer>();

            for (int i=0;i<selections.length;i++)
            {
                try
                {
                    int xx=Integer.parseInt(selections[i]);
                    data.add(xx);
                }
                catch(Exception e)
                {
                    //break;
                }
            }

            for (int i=data.size()-1;i>=0;i--)
            {
                if(text.isEmpty()) {
                    text += "Choosen : ";
                }
                if(i<(data.size()-1))
                {
                    text +=", ";
                }

                text += arrayValues[data.get(i)];
            }
            if(text.isEmpty())
            {
                text="None Selected";
            }
            //Toast.makeText(getActivity().getApplicationContext(), text, Toast.LENGTH_SHORT).show();
            return text;
        }


        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            // findPreference(key).setSummary(sharedPreferences.getString(key, "Some Default Text"));
            Preference pref = findPreference(key);
            if (pref instanceof ListPreference)
            {
                ListPreference etp = (ListPreference) pref;
                etp.setSummary(etp.getEntry());
                String content;//=new String();
                String contentKey=sharedPreferences.getString(key, "0");
            switch(key)
            {
                case "profile_image_privacy":
                    content="profile_picture";
                    userPrivacyUpdate(contentKey,content);
                    break;
                case "profile_status_privacy":
                    content="profile_status";
                    userPrivacyUpdate(contentKey,content);
                    break;
                case "profile_number_privacy":
                    content="number";
                    userPrivacyUpdate(contentKey,content);
                    break;
                case "profile_activity_status_privacy":
                    content="activity_status";
                    userPrivacyUpdate(contentKey,content);
                    break;
                case "profile_location_privacy":
                    content="location";
                    userPrivacyUpdate(contentKey,content);
                    break;
                case "profile_music_privacy":
                    content="profile_music";
                    userPrivacyUpdate(contentKey,content);
                    break;
                default:
                    content="";



            }
            }

            else if (pref instanceof MultiSelectListPreference)
            {
                MultiSelectListPreference etp = (MultiSelectListPreference) pref;

                switch(key) {
                    case "null":

                        break;
                    default:

                        selections = sp.getStringSet(key, null);
                        selected = selections.toArray(new String[]{});
                        etp.setSummary(customStringMaker(selected) + "");
                }

            }

            else if (pref instanceof SwitchPreferenceCompat)
            {
                SwitchPreferenceCompat etp = (SwitchPreferenceCompat) pref;
                //etp.setSummary(etp.isChecked());

                switch(key)
                {
                    case "anyonefollow":

                        userPrivacyUpdate(etp.isChecked()?"0":"2","anyonefollow");

                        break;
                    case "anyonemessage":
                        userPrivacyUpdate(etp.isChecked()?"0":"2","anyonemessage");
                        break;
                }
            }
        }

        void nearByPeoplePrivilageUpdate(Set<String> valuess)
        {
            String finaltext="";
            String[] values=valuess.toArray(new String[]{});

            for (int i=0;i<values.length;i++)
            {
                if(values[i].length()==1) {
                    finaltext += values[i];
                    Log.e("MSGG", values[i]);
                }

            }
            databaseRef = FirebaseDatabase.getInstance().getReference().child("user_profile").child(userdetail.getUid()).child("user_privacy");
            databaseRef.child("nearby_user").setValue(finaltext).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {


                    }

                }
            });
        }

        public void userPrivacyUpdate(String value,String contentText)
        {
            if(!contentText.equals(""))
            {
                databaseRef = FirebaseDatabase.getInstance().getReference().child("user_profile").child(userdetail.getUid()).child("user_privacy");

                databaseRef.child(contentText).setValue(value).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {


                        }

                    }
                });

            }
        }



    }
}