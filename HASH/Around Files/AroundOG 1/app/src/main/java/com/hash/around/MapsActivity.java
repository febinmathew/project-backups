package com.hash.around;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.OtherClasses.GeofenceTrasitionService;
import com.hash.around.OtherClasses.DataTypes.LocationData;
import com.hash.around.OtherClasses.Interfaces.LocationReceiver;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.hash.around.TabbedActivity.formatLocationDataIntoDecimal;
import static com.hash.around.TabbedActivity.saveLocationToDatabase;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener, ResultCallback<Status> {

    private GoogleMap mMap;
    Marker lastMarker;
    private String mLastQuery = "";
FrameLayout defaultFrame;
    String locName;
    LatLng ltlg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        createGoogleApi();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.maps_activity);

        defaultFrame=(FrameLayout)findViewById(R.id.defaultFrame);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);



        final FloatingSearchView mSearchView;
        mSearchView = (FloatingSearchView) findViewById(R.id.floating_search_view);
        mSearchView.setOnQueryChangeListener(new FloatingSearchView.OnQueryChangeListener() {
            @Override
            public void onSearchTextChanged(String oldQuery, final String newQuery) {

                //get suggestions based on newQuery

                //pass them on to the search view

            }
        });

        mSearchView.setOnMenuItemClickListener(new FloatingSearchView.OnMenuItemClickListener() {


            @Override
            public void onActionMenuItemSelected(MenuItem item) {

                Toast.makeText(MapsActivity.this, item.toString(), Toast.LENGTH_SHORT).show();
            }

        });



        mSearchView.setOnHomeActionClickListener(
                new FloatingSearchView.OnHomeActionClickListener() {

                    @Override
                    public void onHomeClicked() {
                        Toast.makeText(MapsActivity.this, "Home Clicked", Toast.LENGTH_SHORT).show();
                    }
                });



        mSearchView.setOnSearchListener(new FloatingSearchView.OnSearchListener() {
            @Override
            public void onSuggestionClicked(SearchSuggestion searchSuggestion) {
                //Toast.makeText(MapsActivity.this, searchSuggestion.getBody(), Toast.LENGTH_SHORT).show();
                mLastQuery = searchSuggestion.getBody();
                mSearchView.setSearchText(mLastQuery);
            }

            @Override
            public void onSearchAction(String location) {



                //Toast.makeText(MapsActivity.this, currentQuery, Toast.LENGTH_SHORT).show();
                mLastQuery = location;

                List<Address> addressList = null;
                if(location != null || !location.equals(""))
                {
                    Geocoder geocoder = new Geocoder(getApplicationContext());
                    try {

                        addressList = geocoder.getFromLocationName(location , 1);


                    } catch (IOException e) {
                        //e.printStackTrace();
                    }
if(addressList!=null )
{
                    Address address = addressList.get(0);
                    ltlg = new LatLng(address.getLatitude() , address.getLongitude());
    if(lastMarker!=null)
    {
        lastMarker.remove();
    }
    lastMarker=mMap.addMarker(new MarkerOptions().position(ltlg).title(address.getLocality()+", "+address.getCountryName()));
    locName=lastMarker.getTitle();

    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(ltlg,9.0f));
    startGeofence(ltlg);

                }
                else
                    {
                        Toast.makeText(MapsActivity.this, "No results found!", Toast.LENGTH_SHORT).show();
                    }
                }



            }
        });
        //mSearchView.sugges

        mSearchView.setOnFocusChangeListener(new FloatingSearchView.OnFocusChangeListener() {
            @Override
            public void onFocus() {

            }

            @Override
            public void onFocusCleared() {


                mSearchView.setSearchBarTitle(mLastQuery);


            }
        });

    }
    ArrayList<Marker> selectedMarkers=new ArrayList<>();
    ArrayList<Circle> selectedCircles=new ArrayList<>();
    //xxx
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                if(selectedMarkers.contains(marker))
                {
                    Log.e("MGG","contacts "+marker.getTitle());
                    success_proceed(marker,true);
                }
                else
                {
                    success_proceed(marker,false);
                }
                return false;
            }
        });
        //googleApiClient.registerListener();


        double defaultLatitide=Double.parseDouble(TabbedActivity.prefs.getString("location_latitude", "0.0"));
        double defaultLongitude=Double.parseDouble(TabbedActivity.prefs.getString("location_longitude", "0.0"));

        LatLng myLoc=new LatLng(defaultLatitide,defaultLongitude);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLoc,4.0f));




        //LatLng sydney = new LatLng(-34, 151);

        Toast.makeText(this, "Long press areas on map to select location. Click on the marker to remove them.", Toast.LENGTH_SHORT).show();
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},10);
            }

        }
        else
            {
                mMap.setMyLocationEnabled(true);

                mMap.getUiSettings().setMyLocationButtonEnabled(true);

                mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                    @Override
                    public void onMapLongClick(LatLng point) {
                        searchLocation(point);
                    }
                });

        }
    }




    public void searchLocation(LatLng point)
    {
        Geocoder gcd = new Geocoder(getApplicationContext());

        List<Address> addresses = null;
        try {
            addresses = gcd.getFromLocation(
                    point.latitude,
                    point.longitude,
                    // In this sample, get just a single address.
                    1);
        } catch (Exception ioException) {
            // Catch network or other I/O problems.
            //Toast.makeText(SetProfile.this, "Service no avaialble", Toast.LENGTH_SHORT).show();
        }
            if (addresses.size() >0) {
                Address address = addresses.get(0);
                if (addresses.get(0).getLocality() == null) {
                    Toast.makeText(this, "Could not locate the place. Try again!", Toast.LENGTH_SHORT).show();
                } else {
                    if (lastMarker != null) {
                        lastMarker.remove();
                    }
                    ltlg = new LatLng(address.getLatitude(), address.getLongitude());
                    showLocationOnMap(ltlg,address.getLocality() + ", " + address.getCountryName());
                }
            }

    }

    void showLocationOnMap(LatLng latlong,String locName)
    {
        lastMarker = mMap.addMarker(new MarkerOptions().position(latlong).title(locName));

        // Toast.makeText(this, "starting", Toast.LENGTH_SHORT).show();
        //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latlong,5.0f));
        //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 12.0f));
       // startGeofence(latlong);
        drawCustomGeofence(latlong);
    }

    @Override
    protected void onStart() {
        super.onStart();
        googleApiClient.connect();

    }

    @Override
    protected void onStop() {
        super.onStop();
        googleApiClient.disconnect();
    }

    /***** CODE FROM https://code.tutsplus.com/tutorials/how-to-work-with-geofences-on-android--cms-26639 */
    private static final long GEO_DURATION = 60 * 60 * 1000;
    private static final String GEOFENCE_REQ_ID = "My Geofence";
    private static final String TAG="MSG";
    private static final float GEOFENCE_RADIUS = 2000.0f;
    private PendingIntent geoFencePendingIntent;
    private final int GEOFENCE_REQ_CODE = 0;
    private GoogleApiClient googleApiClient;
    private Circle geoFenceLimits;
    LatLng geofencePos;

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        getSelectedLocationData(new LocationReceiver() {
            @Override
            public void onLocationReceived(Location data)
            {
                ltlg =new LatLng(data.getLatitude(),data.getLongitude());
                Marker templastMarker=mMap.addMarker(new MarkerOptions().position(ltlg).title(data.getProvider()));
                selectedMarkers.add(templastMarker);
                //startGeofence(ltlg);

                //templastMarker.

                Log.e("MGG","loc: "+data.getProvider()+" / "+new MarkerOptions());

                selectedCircles.add(drawCustomGeofence(ltlg));
                geoFenceLimits=null;
            }
        });
    }

    private Circle drawCustomGeofence(LatLng latLng) {
        Log.d(TAG, "drawGeofence()");

        if ( geoFenceLimits != null )
            geoFenceLimits.remove();

        CircleOptions circleOptions = new CircleOptions()
                .center( latLng)
                .strokeColor(Color.argb(150,244, 65, 65))
                .fillColor( Color.argb(100,65, 190, 244) )
                .radius( GEOFENCE_RADIUS );
        geoFenceLimits = mMap.addCircle( circleOptions );
        return geoFenceLimits;

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(@NonNull Status status) {
        Log.d(TAG, "RESULTS OBTAINED");
        drawGeofence();

    }

    private void drawGeofence() {
        Log.d(TAG, "drawGeofence()");

        if ( geoFenceLimits != null )
            geoFenceLimits.remove();

        CircleOptions circleOptions = new CircleOptions()
                .center( geofencePos)
                .strokeColor(Color.argb(150,244, 65, 65))
                .fillColor( Color.argb(100,65, 190, 244) )
                .radius( GEOFENCE_RADIUS );
        geoFenceLimits = mMap.addCircle( circleOptions );

    }
    private void createGoogleApi() {
        Log.d(TAG, "createGoogleApi()");
        if ( googleApiClient == null ) {
            googleApiClient = new GoogleApiClient.Builder( this )
                    .addConnectionCallbacks( this )
                    .addOnConnectionFailedListener( this )
                    .addApi( LocationServices.API )
                    .build();
        }
    }

    private Geofence createGeofence(LatLng latLng, float radius ) {
        Log.d(TAG, "createGeofence");
        return new Geofence.Builder()
                .setRequestId(GEOFENCE_REQ_ID)
                .setCircularRegion( latLng.latitude, latLng.longitude, radius)
                .setExpirationDuration( GEO_DURATION )
                .setTransitionTypes( Geofence.GEOFENCE_TRANSITION_ENTER
                        | Geofence.GEOFENCE_TRANSITION_EXIT )
                .build();
    }
    private GeofencingRequest createGeofenceRequest(Geofence geofence ) {
        Log.d(TAG, "createGeofenceRequest");
        return new GeofencingRequest.Builder()
                .setInitialTrigger( GeofencingRequest.INITIAL_TRIGGER_ENTER )
                .addGeofence( geofence )
                .build();
    }

    private void startGeofence(LatLng pos) {
        Log.i(TAG, "startGeofence()");
        geofencePos=pos;
        Geofence geofence = createGeofence(pos, GEOFENCE_RADIUS);
        GeofencingRequest geofenceRequest = createGeofenceRequest(geofence);
        addGeofence(geofenceRequest);
    }
    private void addGeofence(GeofencingRequest request) {
        Log.d(TAG, "addGeofence");
        if (checkPermission())
            LocationServices.GeofencingApi.addGeofences(
                    googleApiClient,
                    request,
                    createGeofencePendingIntent()
            ).setResultCallback(this);
    }
    private PendingIntent createGeofencePendingIntent()
    {
        Log.d(TAG, "createGeofencePendingIntent");
        if ( geoFencePendingIntent != null )
            return geoFencePendingIntent;

        Intent intent = new Intent( this, GeofenceTrasitionService.class);
        return PendingIntent.getService(
                this, GEOFENCE_REQ_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT );
    }

    private boolean checkPermission() {
        Log.d(TAG, "checkPermission()");
        // Ask for permission if it wasn't granted yet
        return (android.support.v4.app.ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED );
    }


    public void success_proceed(final Marker markerTemp ,boolean forRemoval)
    {
        Snackbar sBar=Snackbar.make(defaultFrame,markerTemp.getTitle(),Snackbar.LENGTH_INDEFINITE);
        if(forRemoval)
        {

            locName=markerTemp.getTitle();
            sBar.setAction("REMOVE",  new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Toast.makeText(MapsActivity.this, "Data Taken", Toast.LENGTH_SHORT).show();

                    /*Intent intent = new Intent();
                    intent.putExtra("locname",locName);
                    intent.putExtra("lat",ltlg.latitude);
                    intent.putExtra("lng",ltlg.longitude);
                    setResult(RESULT_OK, intent);
                    finish();*/
                    removeSingleLcationDataBasedOnName(markerTemp);
                    selectedCircles.get(selectedMarkers.indexOf(markerTemp)).remove();
                    selectedCircles.remove(selectedMarkers.indexOf(markerTemp));
                    selectedMarkers.remove(markerTemp);


                    markerTemp.remove();

                    Log.e("MGG","size isnow : "+selectedCircles.size()+" / "+selectedMarkers.size());


                }
            });
        }
        else
        {

            locName=markerTemp.getTitle();
            sBar.setAction("ADD",  new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Toast.makeText(MapsActivity.this, "Data Taken", Toast.LENGTH_SHORT).show();
                    LocationData data=new LocationData();
                    data.location_name=markerTemp.getTitle();
                    data.status=1;
                    data.latitude=markerTemp.getPosition().latitude;
                    data.longitude=markerTemp.getPosition().longitude;
                    saveLocationToDatabase(formatLocationDataIntoDecimal(data));
                    selectedMarkers.add(markerTemp);
                    selectedCircles.add(geoFenceLimits);
                    lastMarker=null;
                    geoFenceLimits=null;
                    /*Intent intent = new Intent();
                    intent.putExtra("locname",locName);
                    intent.putExtra("lat",ltlg.latitude);
                    intent.putExtra("lng",ltlg.longitude);
                    setResult(RESULT_OK, intent);
                    finish();*/
                    Log.e("MGG","size isnow : "+selectedCircles.size()+" / "+selectedMarkers.size());

                }
            });
        }


        View sbView = sBar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        sBar.show();




    }

    void getSelectedLocationData(LocationReceiver receiver)
    {
        SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();

        Cursor cursor = db.query(DatabaseAccesser.location_table, new String[]{DatabaseAccesser.loc_col_location_name,
                        DatabaseAccesser.loc_col_status, DatabaseAccesser.loc_col_latitude, DatabaseAccesser.loc_col_longitude},
                DatabaseAccesser.loc_col_status+" =?",
                new String[]{"1"},
                null, null, null,null);

        while (cursor.moveToNext()) {


            Location temp = new Location(cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.loc_col_location_name)));


            temp.setLatitude(cursor.getDouble(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.loc_col_latitude)));
            temp.setLongitude(cursor.getDouble(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.loc_col_longitude)));

            //Toast.makeText(getActivity(), temp.uri, Toast.LENGTH_SHORT).show();
            // long itemId =
            receiver.onLocationReceived(temp);
        }
        cursor.close();

    }

    int removeSingleLcationDataBasedOnName(Marker mark)
    {
        LatLng latLng=mark.getPosition();
        SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();

        int num=db.delete(DatabaseAccesser.location_table,DatabaseAccesser.loc_col_latitude+" =? AND "+DatabaseAccesser.loc_col_longitude+" =? ",new String[]{latLng.latitude+"",latLng.longitude+""});
        return num;
    }
}