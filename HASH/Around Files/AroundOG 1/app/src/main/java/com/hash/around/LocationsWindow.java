package com.hash.around;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;

import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.hash.around.TabFragments.MapsFragment;
import com.hash.around.TabFragments.TabFragment3;

//import com.hash.around.TabFragments.TabFragment2.get;


public class LocationsWindow extends AppCompatActivity {

    FloatingActionButton mapExpandMinimizeFloating,mapFragmentExpand;

    public TabFragment3 tabFragment3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.locations_window);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);


        setSupportActionBar(toolbar);


        //getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        //getSupportActionBar().setDisplayShowHomeEnabled(false);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        //getSupportActionBar().setLogo(R.drawable.ic_icon_chat_bubble_green);

        ActionBar ab=getSupportActionBar();

        if (ab != null) {
           // ab.setLogo(R.mipmap.ic_launcher);

            //ab.setDisplayUseLogoEnabled(true);
            ab.setTitle(null);
            ab.setDisplayShowTitleEnabled(true);
            ab.setTitle("Choose Locations");
            //ab.setDisplayShowCustomEnabled(false);
            ab.setDisplayHomeAsUpEnabled(true);
            //ab.setLogo(R.mipmap.logo1);

           // ab.setDisplayHomeAsUpEnabled(false);
            //ab.setDisplayShowHomeEnabled(true);
        }

        //mapFragmentAnchoredExpandFloating=(FloatingActionButton)findViewById(R.id.mapFragmentAnchoredExpandFloating);
        FragmentManager fragmentManager= getFragmentManager();

        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
        //final FragmentTransaction fragmentTransaction2=fragmentManager.beginTransaction();

        tabFragment3=new TabFragment3();
        fragmentTransaction
                .replace(R.id.fragmnetLocationsListSection, tabFragment3)
                .commit();


        mapExpandMinimizeFloating =(FloatingActionButton)findViewById(R.id.mapExpandMinimizeFloating);
        //mapFragmentExpand=(FloatingActionButton)findViewById(R.id.mapFragmentExpand);
        mapExpandMinimizeFloating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(getFragmentManager().findFragmentByTag("MAP_FRAGMENT")==null)
                {
                    getFragmentManager().beginTransaction()
                            .replace(R.id.fragmnetMapSection, new MapsFragment(), "MAP_FRAGMENT")
                            .commit();
//                    mapFragmentExpand.setVisibility(View.VISIBLE);
                    mapExpandMinimizeFloating.setImageResource(R.drawable.ic_arrow_up_white);
                }
                else
                {
                    getFragmentManager().beginTransaction().remove(getFragmentManager().findFragmentByTag("MAP_FRAGMENT")).commit();
                   // mapFragmentExpand.setVisibility(View.GONE);
                    mapExpandMinimizeFloating.setImageResource(R.drawable.ic_arrow_down_white);

                }
            }
        });







    }




public void setupRecyclerlistner (RecyclerView rView)
{
    rView.addOnScrollListener(new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            Log.e("MGG","position is "+dy);
            if(dy>5 && mapFragmentExpand.getVisibility()==View.GONE)
            {
                mapExpandMinimizeFloating.setVisibility(View.GONE);
            }
            else
            {
                mapExpandMinimizeFloating.setVisibility(View.VISIBLE);
            }
        }
    });
}


    @Override
    protected void onStop() {

        super.onStop();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.single_user_chat_window, menu);



 this.chatMenu=menu;




       /* MenuItem sItem=menu.findItem(R.id.action_search_chat);
        SearchView searchView =
                (SearchView) sItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // return false;
                // Toast.makeText(AllUsersList.this, "Submit- "+query, Toast.LENGTH_SHORT).show();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                Log.e("MGG","search id: "+newText);
                searchOnList(newText);
                return false;
            }
        });*/

        //searchView.setIconified(true);
        //searchView.setIconifiedByDefault(false);


        return true;
    }
    //SparseBooleanArray selectedChats=new SparseBooleanArray();
    int currentVisibleSearch;
    Menu chatMenu;
    void searchOnList(String searchTerm)
    {
        /*adap.selectedItems.clear();
        adap.notifyDataSetChanged();

        if(searchTerm.equals(""))
        {

            //return;
        }
        else {
            for (int i = 0; i < finaldata.size(); i++) {

                if (finaldata.get(i).msg.toLowerCase().contains(searchTerm.toLowerCase())) {
                    adap.selectedItems.put(i, true);
                    adap.notifyItemChanged(i);
                }
            }
        }



        if(adap.selectedItems.size()>0)
        {
            currentVisibleSearch=adap.selectedItems.size()-1;
            chatView.scrollToPosition(adap.selectedItems.keyAt(currentVisibleSearch));
            chatMenu.findItem(R.id.action_search_chat_moveup).setVisible(true);
            chatMenu.findItem(R.id.action_search_chat_movedown).setVisible(true);

        }
        else
        {
            chatMenu.findItem(R.id.action_search_chat_moveup).setVisible(false);
            chatMenu.findItem(R.id.action_search_chat_movedown).setVisible(false);
        }*/
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

       // switch (item.getItemId()) {


              //  return true;


        return super.onOptionsItemSelected(item);
    }


}
