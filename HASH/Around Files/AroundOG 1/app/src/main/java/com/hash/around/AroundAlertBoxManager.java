package com.hash.around;

import android.app.Activity;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

//import org.thoughtcrime.securesms.util.ExpirationUtil;

import android.app.DatePickerDialog;

import com.hash.around.OtherClasses.Interfaces.ChatSchedulerListener;
import com.hash.around.OtherClasses.Interfaces.TimerListner;
import com.hash.around.Utils.StringUtils;
import com.hash.around.Views.Medias.FeedFilterOptionItem;

import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import cn.carbswang.android.numberpickerview.library.NumberPickerView;

public class AroundAlertBoxManager {
 static  AlertDialog currentAlertDialog;


  public static void showChatSchedulerDiolog(final Context context,final ChatSchedulerListener listener)
  {
    final View view = createScheduleChatView(context, 2);
    AlertDialog.Builder builder;// = new AlertDialog.Builder(context);

    builder = new AlertDialog.Builder(context,R.style.CorneredDialog);
    builder.setTitle("Schedule Messages");
    builder.setView(view);
    builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {

        if(calenderTimestam!=null && clockTimestamp!=null)
        {
          if(calenderTimestam+clockTimestamp>System.currentTimeMillis())
          {
            if(listener!=null)
            listener.onSchedule(calenderTimestam+clockTimestamp);
          }
        }

       // int selected = ((NumberPickerView)view.findViewById(R.id.expiration_number_picker)).getValue();
        //listener.onClick(context.getResources().getIntArray(R.array.expiration_times)[selected]);
      }
    });
    builder.setNegativeButton(android.R.string.cancel, null);
     currentAlertDialog =builder.show();

  }

  public static void showChatExpireDialog(final Context context,
                          final int currentExpiration,
                          final @NonNull OnClickListener listener)
  {
    final View view = createChatExpireView(context, currentExpiration);

    AlertDialog.Builder builder;// = new AlertDialog.Builder(context);
    //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
     // builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
   // } else {
     builder = new AlertDialog.Builder(context);
    //}
    builder.setTitle("Message lifespan");
    builder.setView(view);
    builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        int selected = ((NumberPickerView)view.findViewById(R.id.expiration_number_picker)).getValue();
        listener.onClick(context.getResources().getIntArray(R.array.expiration_times)[selected]);
      }
    });
    builder.setNegativeButton(android.R.string.cancel, null);
    currentAlertDialog=builder.show();
  }

  public static void showReminderDialog(final Context context)
  {

  }
static Integer muteSelection=0;
  public static void showConversationMuteDialog(final Context context, final @NonNull MuteSelectionListener listener) {
    AlertDialog.Builder builder = new AlertDialog.Builder(context);
    builder.setTitle("Mute chat for..");
    final View view = createMuteConversationView(context);

    builder.setSingleChoiceItems(R.array.mute_durations, 0, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        muteSelection=which;
      }
    });
    builder.setView(view);
    builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {

            listener.onMuted(context.getResources().getIntArray(R.array.mute_durations_values)[muteSelection],false);
      }
    });
    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
      @Override
      public void onDismiss(DialogInterface dialog) {

      }
    });
    //builder.set
    /*builder.setItems(R.array.mute_durations, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, final int which) {
        final long muteUntil;

        switch (which) {
          case 0:  muteUntil = System.currentTimeMillis() + TimeUnit.HOURS.toMillis(1);  break;
          case 1:  muteUntil = System.currentTimeMillis() + TimeUnit.HOURS.toMillis(2);  break;
          case 2:  muteUntil = System.currentTimeMillis() + TimeUnit.DAYS.toMillis(1);   break;
          case 3:  muteUntil = System.currentTimeMillis() + TimeUnit.DAYS.toMillis(7);   break;
          case 4:  muteUntil = System.currentTimeMillis() + TimeUnit.DAYS.toMillis(365); break;
          default: muteUntil = System.currentTimeMillis() + TimeUnit.HOURS.toMillis(1);  break;
        }
        if(listener!=null)
        listener.onMuted(muteUntil);
      }
    });*/

    builder.show();

  }

  public static void showFeedFilterDialog(final Context context, final  OnClickListener  listener) {
    AlertDialog.Builder builder = new AlertDialog.Builder(context);
    builder.setTitle("Show feeds from");
    final View view = createFeedFilterView(context);
    setFeedFilterOptions(view,context);
    builder.setView(view);
    builder.setPositiveButton("FILTER", new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int which) {

                SharedPreferences pref= PreferenceManager.getDefaultSharedPreferences(context);
                SharedPreferences.Editor editor=pref.edit();
                editor.putStringSet("filter_feed",getFeedFilterOptions(view,false));
                editor.clear().commit();
                if (listener != null)
                listener.onClick(1);
                    }
            });
    builder.setNeutralButton("RESET FILTER", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        SharedPreferences pref= PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor=pref.edit();
        editor.putStringSet("filter_feed",getFeedFilterOptions(view,true));
        editor.clear().commit();
        if (listener != null)
          listener.onClick(1);
      }
    });
    builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        if (listener != null)
          listener.onClick(0);
      }
    });
    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
      @Override
      public void onDismiss(DialogInterface dialog) {

      }
    });
    builder.show();
  }
  static boolean []filterOptions=new boolean[3];
  private static View createFeedFilterView(final Context context) {





    filterOptions=new boolean[3];
    final LayoutInflater inflater = LayoutInflater.from(context);
    final View view = inflater.inflate(R.layout.feed_filter_dialogview, null);

    return view;
  }
  static Set<String>  filterSet;
  private static void setFeedFilterOptions(View view,Context context) {
    //filterOptions=checkVals;
    SharedPreferences pref= PreferenceManager.getDefaultSharedPreferences(context);
    filterSet =pref.getStringSet("filter_feed", null);

    if(filterSet==null)
    {filterSet = new HashSet<String>();
      filterSet.add(MainServiceThread.PEOPLE_NEAR_FILTER+"");
      filterSet.add(MainServiceThread.FOLLOWING_PEOPLE_FILTER+"");
      filterSet.add(MainServiceThread.SELECTED_LOCATION_FILTER+"");
    }
    //if(filterSet!=null)

      /*for (String x:selections
           ) {

      }*/
        ((FeedFilterOptionItem)view.findViewById(R.id.peopleNearFilter)).setChecked(filterSet.contains(MainServiceThread.PEOPLE_NEAR_FILTER+""));
        ((FeedFilterOptionItem)view.findViewById(R.id.followingPeopleFilter)).setChecked(filterSet.contains(MainServiceThread.FOLLOWING_PEOPLE_FILTER+""));
        ((FeedFilterOptionItem)view.findViewById(R.id.seletedLocationsFilter)).setChecked(filterSet.contains(MainServiceThread.SELECTED_LOCATION_FILTER+""));




  }
  private static Set<String> getFeedFilterOptions(View view,boolean reset) {


    filterOptions[0]=((FeedFilterOptionItem)view.findViewById(R.id.peopleNearFilter)).getChecked();
    filterOptions[1]=((FeedFilterOptionItem)view.findViewById(R.id.followingPeopleFilter)).getChecked();
    filterOptions[2]=((FeedFilterOptionItem)view.findViewById(R.id.seletedLocationsFilter)).getChecked();
    for (int i=0;i<filterOptions.length;i++)
    {
      if(filterOptions[i] || reset)
        filterSet.add(i+"");
      else
        filterSet.remove(i+"");
    }
    return filterSet;
  }
  public static void showFeedSortDialog(final Context context, final @NonNull MuteSelectionListener listener) {
    AlertDialog.Builder builder = new AlertDialog.Builder(context);
    builder.setTitle("Sort feeds by:");
    //final View view = createMuteConversationView(context);

    builder.setMultiChoiceItems(R.array.feed_sort_options, new boolean[]{true,true}, new DialogInterface.OnMultiChoiceClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which, boolean isChecked) {

      }
    });

    builder.setPositiveButton("SORT", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        if (listener != null)
          listener.onMuted(context.getResources().getIntArray(R.array.mute_durations_values)[muteSelection], false);
      }
    });
    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
      @Override
      public void onDismiss(DialogInterface dialog) {

      }
    });
    builder.show();
  }
  static View createMuteConversationView(Context context)
  {
    final LayoutInflater   inflater                = LayoutInflater.from(context);
    final View             view                    = inflater.inflate(R.layout.timer_muteconversation_dialogview, null);
    return view;
  }


  private static View createChatExpireView(final Context context, final int currentExpiration) {
    final LayoutInflater   inflater                = LayoutInflater.from(context);
    final View             view                    = inflater.inflate(R.layout.timer_expirationchat_dialogview, null);
    final NumberPickerView numberPickerView        = view.findViewById(R.id.expiration_number_picker);
    final TextView         textView                = view.findViewById(R.id.expiration_details);
    final int[]            expirationTimes         = context.getResources().getIntArray(R.array.expiration_times);
    final String[]         expirationDisplayValues = new String[expirationTimes.length];

    int selectedIndex = expirationTimes.length - 1;

    for (int i=0;i<expirationTimes.length;i++) {
      expirationDisplayValues[i] = StringUtils.getExpirationDisplayValue(context, expirationTimes[i]);

      if ((currentExpiration >= expirationTimes[i]) &&
          (i == expirationTimes.length -1 || currentExpiration < expirationTimes[i+1])) {
        selectedIndex = i;
      }
    }

    numberPickerView.setDisplayedValues(expirationDisplayValues);
    numberPickerView.setMinValue(0);
    numberPickerView.setMaxValue(expirationTimes.length-1);


    NumberPickerView.OnValueChangeListener listener= new NumberPickerView.OnValueChangeListener() {
      @Override
      public void onValueChange(NumberPickerView picker, int oldVal, int newVal) {
        if (newVal == 0) {
          textView.setText("Your messages will last as long as the recipient keeps them.");
        } else {
          textView.setText(context.getString(R.string.message_expiration_text, picker.getDisplayedValues()[newVal]));
        }
      }
    };
    numberPickerView.setOnValueChangedListener(listener);
    numberPickerView.setValue(selectedIndex);
    listener.onValueChange(numberPickerView, selectedIndex, selectedIndex);

    return view;
  }
static Long calenderTimestam=null,clockTimestamp=null;
  static TextView         windowMessage;
  static void updateScheduledWindowMessage(Context context)
  {
    currentAlertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
            .setEnabled(false);
    //windowMessage.setTextColor(context.getResources().getColor(android.R.color.primary_text_dark));


    if(calenderTimestam==null)
      windowMessage.setText("Select a date first.");
    else if(clockTimestamp==null)
      windowMessage.setText("Choose a time.");
    else if(getSelectedTtimestamp()<System.currentTimeMillis())
    { windowMessage.setText("Pick a time in the future!");
     // windowMessage.setTextColor(Color.RED);

    }
    else {
      windowMessage.setText(convertTimestampScheduleMessage(getSelectedTtimestamp(), context));
      currentAlertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
              .setEnabled(true);

    }
  }

  static long getSelectedTtimestamp()
  {
    return calenderTimestam+clockTimestamp;
  }
  public static String convertTimestampScheduleMessage(long timestampData,Context context)
  {

    Long difference=timestampData-System.currentTimeMillis();

    int minuteValue=1000*60;
    int hourValue=minuteValue*60;
    long dayValue= TimeUnit.DAYS.toMillis(1);

    int totalDays=(int)(difference/dayValue);
    difference=difference%dayValue;
    int totalHours=(int)(difference/hourValue);
    difference=difference%hourValue;
    int totalMinutes=(int)(difference/minuteValue);
    //difference=difference%hourValue;

    String finalText="Timer set for ";


   if(totalDays>1)
    {
      finalText+=context.getResources().getQuantityString(R.plurals.expiration_days, totalDays, totalDays)+", ";
    }

   if(totalHours>1)
    {
      finalText+=context.getResources().getQuantityString(R.plurals.expiration_hours, totalHours, totalHours)+" and ";
    }
    if(totalMinutes>1)
    {
      finalText+=context.getResources().getQuantityString(R.plurals.expiration_minutes, totalMinutes, totalMinutes)+" minutes ";
    }

    finalText+="from now.";
     return finalText;


  }
  public  static View createScheduleChatView(final Context context, final int currentExpiration) {
    final LayoutInflater   inflater                = LayoutInflater.from(context);
    final View             view                    = inflater.inflate(R.layout.timer_schedulerchat_dialogview, null);
    windowMessage                = view.findViewById(R.id.scheduling_details);
    final TextView         dateText                = view.findViewById(R.id.dateText);
    final TextView         timeText                = view.findViewById(R.id.timeText);
    final ImageView        editTime                = view.findViewById(R.id.editTime);
    final ImageView        editDate                = view.findViewById(R.id.editDate);

    LinearLayout datelayout = view.findViewById(R.id.datelayout);
    LinearLayout timerLayout = view.findViewById(R.id.timerLayout);

    View.OnClickListener editTimeListner= new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog = new TimePickerDialog(context,
                new TimePickerDialog.OnTimeSetListener() {

                  @Override
                  public void onTimeSet(TimePicker view, int hourOfDay,
                                        int minute) {

                    long hourTimestamp=hourOfDay*1000*60*60;
                    long minuteTimestamp=minute*1000*60;
                    clockTimestamp=hourTimestamp+minuteTimestamp;
                    Log.e("MGG","hourOfDay "+hourOfDay);
                    String hr=hourOfDay>=12?"PM":"AM";
                    hourOfDay=hourOfDay>12?hourOfDay-12:hourOfDay;
                    if(hourOfDay==0)
                    {
                      hourOfDay=12;}
                    timeText.setText(doubleDigitNumber(hourOfDay)+":"+doubleDigitNumber(minute)+" "+hr);
                    timeText.setTextColor(context.getResources().getColor(R.color.orange_500));

                    Log.e("MGG","current timestamp "+clockTimestamp);
                    updateScheduledWindowMessage(context);

                  }
                }, mHour, mMinute, false);
        timePickerDialog.show();
      }};
    View.OnClickListener editDateListner=new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int  mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {

                  @Override
                  public void onDateSet(DatePicker view, int year,
                                        int monthOfYear, int dayOfMonth) {
                    dateText.setText(dayOfMonth+"/"+monthOfYear+"/"+year);
                    Calendar calendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);

                    calenderTimestam=calendar.getTimeInMillis();
                    //Log.e("MGG","calenderTimestam: "+calenderTimestam);
                    dateText.setTextColor(context.getResources().getColor(R.color.orange_500));
                    updateScheduledWindowMessage(context);

                  }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
      }
    };

    editTime.setOnClickListener(editTimeListner);
    timerLayout.setOnClickListener(editTimeListner);
    editDate.setOnClickListener(editDateListner);
    datelayout.setOnClickListener(editDateListner);

    return view;
  }

  static String doubleDigitNumber(int num)
  {
    return num<10?"0"+num:num+"";
  }

  public interface OnClickListener {
    public void onClick(int expirationTime);
  }
  public interface MuteSelectionListener {
    public void onMuted(long until,boolean notificationAlso);
  }
}
