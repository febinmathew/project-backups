package com.hash.around.OtherClasses.DataTypes;

/**
 * Created by Febin on 26-08-2017.
 */
import java.io.Serializable;

public class FeedData implements  Serializable {

    public long sl;
    public String userid;
    public String username;
    public String location; //NEW ENTRY
    public long timestamp;
    public int type;
    public int visibility; //NEW ENTRY
    public String text;
    public String semi_uri="null";
    public String uri;
    public long mediaSize=0;
   /* public Double latitude;
    public Double longitude;*/
   public FeedData()
   {

   }
   public FeedData(GroupFeedData data)
   {
       this.sl=data.sl;
       this.userid=data.userid;
       this.username=data.username;
       this.location=data.location;
       this.timestamp=data.timestamp;
       this.type=data.type;
       this.visibility=data.visibility;
       this.text=data.text;
       this.semi_uri=data.semi_uri;
       this.uri=data.uri;
       this.mediaSize=data.mediaSize;
   }


}


