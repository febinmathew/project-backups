package com.hash.around.Utils;


import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.hash.around.CountriesListActivity;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Locale;

import java.util.Set;

public class CountryListLoader extends AsyncTaskLoader<ArrayList<CountriesListActivity.CountryData>> {

  public CountryListLoader(Context context) {
    super(context);
  }

  @Override
  public ArrayList<CountriesListActivity.CountryData> loadInBackground() {
    ArrayList<CountriesListActivity.CountryData> finaldata=new ArrayList<>();
    Set<String> regions                    = PhoneNumberUtil.getInstance().getSupportedRegions();
    int count=0;
    //Log.e("MGG","received data size "+regions.size() );
    Log.e("MGG","goooooooooooot data size "+finaldata.size() );

    for (String region : regions) {

      count++;
      CountriesListActivity.CountryData data = new CountriesListActivity.CountryData();
      data.country_name=getRegionDisplayName(region);
      data.country_code=PhoneNumberUtil.getInstance().getCountryCodeForRegion(region);

      data.country_nickName=region;
      //Log.e("MGG","country: "+region+" =name: "+getRegionDisplayName(region)+" =code: "+PhoneNumberUtil.getInstance().getCountryCodeForRegion(region));
      finaldata.add(data);

    }
    Log.e("MGG","goooooooooooot data size "+finaldata.size() );
    Collections.sort(finaldata, new RegionComparator());

    return finaldata;
  }
  private static class RegionComparator implements Comparator<CountriesListActivity.CountryData> {
    @Override
    public int compare(CountriesListActivity.CountryData lhs, CountriesListActivity.CountryData rhs) {
      return rhs.country_name.compareTo(lhs.country_name);
    }
  }
  public static String getRegionDisplayName(String regionCode) {
    return (regionCode == null || regionCode.equals("ZZ") || regionCode.equals(PhoneNumberUtil.REGION_CODE_FOR_NON_GEO_ENTITY))
            ? "Unknown country" : new Locale("", regionCode).getDisplayCountry(Locale.getDefault());
  }


}
