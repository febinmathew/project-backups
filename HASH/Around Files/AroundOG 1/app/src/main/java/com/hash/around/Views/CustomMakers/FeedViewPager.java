package com.hash.around.Views.CustomMakers;

/**
 * Created by Febin on 5/25/2018.
 */


import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.ViewDragHelper;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.Scroller;

import java.lang.reflect.Field;

/**
 * Create On 16/10/2016
 * @author wayne
 */
public class FeedViewPager extends ViewPager {


    DisplayMetrics dm;

    public FeedViewPager(@NonNull Context context) {
        super(context);
    }

    public FeedViewPager(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        dm=new DisplayMetrics();

        ((Activity)context). getWindowManager() .getDefaultDisplay().getMetrics(dm);
    }


    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        // Never allow swiping to switch between pages
        final int action = event.getAction();
       // return true;


            if (action == MotionEvent.ACTION_DOWN || action == MotionEvent.ACTION_UP) {
                if(event.getX()<(dm.widthPixels/100)*30) {
                    if(!upActive && action == MotionEvent.ACTION_UP)
                        return  super.onInterceptTouchEvent(event);

                    upActive=true;
                    super.onInterceptTouchEvent(event);
                    return true;
            }
                else if(event.getX()>dm.widthPixels-(dm.widthPixels/100)*30) {
                    if(!upActive && action == MotionEvent.ACTION_UP)
                        return  super.onInterceptTouchEvent(event);

                    super.onInterceptTouchEvent(event);
                    return true;
                }
        }
        else if(action == MotionEvent.ACTION_MOVE)
            upActive=false;

       return  super.onInterceptTouchEvent(event);
    }


    void movemove(int action,int pos)
    {
       if (upActive) {
           upActive=false;
          //  Log.e("GGG","touch Right ACTIVE "+pos+" / "+getCurrentItem());
          setCurrentItem(pos,false);
       }
    }
    boolean upActive;
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction()==MotionEvent.ACTION_UP) {
            if (event.getX() < (dm.widthPixels / 100) * 30) {
                //Log.e("GGG","touch left");
                if (getCurrentItem() > 0)
                    movemove(event.getAction(), getCurrentItem() - 1);
               // return false;
            } else if (event.getX() > dm.widthPixels - (dm.widthPixels / 100) * 30) {
                //Log.e("GGG","touch Right");
                if (getCurrentItem() < (getAdapter().getCount() - 1))
                    movemove(event.getAction(), getCurrentItem() + 1);
                //return super.onTouchEvent(event);
            }
        }

        return super.onTouchEvent(event);
        //return false;
    }

    //down one is added for smooth scrolling


}