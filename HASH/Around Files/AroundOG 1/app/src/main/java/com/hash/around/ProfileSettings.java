package com.hash.around;

import android.app.ActivityOptions;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Pair;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.HashMap;

import static android.view.View.GONE;

public class ProfileSettings extends AppCompatActivity {

    String userId;
    CircularImageView profileimage;
    TextView profile_username,profile_status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_settings_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        profileimage=(CircularImageView)findViewById(R.id.profileimage);
        profile_username=(TextView)findViewById(R.id.profile_username);
        profile_status=(TextView)findViewById(R.id.profile_status);

        userId=getIntent().getStringExtra("userid");



        accessDatabseForContent(userId);
        //getActionBar().setDisplayHomeAsUpEnabled(true);



    }




    void accessDatabseForContent(String uID)
    {

        SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();

        Cursor cursor = db.query(DatabaseAccesser.user_table,
                new String[]{
                        DatabaseAccesser.user_user_id,
                        DatabaseAccesser.user_name,
                        DatabaseAccesser.user_status,
                        DatabaseAccesser.user_localUrl,
                        DatabaseAccesser.user_globalUrl},
                DatabaseAccesser.user_user_id + " =?",
                new String[]{uID},
                null, null, null,null);

        //Toast.makeText(this, cursor.getCount(), Toast.LENGTH_SHORT).show();

        while (cursor.moveToNext()) {



            String pro=cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_name));
            profile_username.setText(pro);


            pro=cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_status));

            if(pro.equals("null"))
                pro="Wanna know an App called SpotIt?";

            profile_status.setText(pro);


            String dir=cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_localUrl));
            if(dir.equals("null"))
            {
                profileimage.setImageResource(R.drawable.default_user);
            }
            else
            {
                profileimage.setImageURI(Uri.parse(dir));
            }



            //Toast.makeText(getActivity(), temp.uri, Toast.LENGTH_SHORT).show();
            // long itemId =
            // finaldata.add(temp);

        }
        cursor.close();
    }





    public void onclickprofile(View view)
    {
        Intent indi=new Intent(getApplicationContext(),Profile_Activity.class);
        indi.putExtra("userid",userId);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // Apply activity transition

            Pair contact_image = Pair.create(profileimage, "contact_image");
            Pair contact_name = Pair.create(profile_username, "contact_name");

            ActivityOptions compat=ActivityOptions.makeSceneTransitionAnimation(
                    this,contact_image,contact_name);
            startActivity(indi,compat.toBundle());
        }
        else
        {

            startActivity(indi);
        }

    }

    public void profileSettings(View view)
    {
        Intent intent=new Intent(this,SettingsActivity.class);
        intent.putExtra("stat",0);
        startActivity(intent);
    }

    public void privacySettings(View view)
    {
        Intent intent=new Intent(this,SettingsActivity.class);
        intent.putExtra("stat",1);
        startActivity(intent);
    }

    public void dataUsageSettings(View view)
    {
        Intent intent=new Intent(this,SettingsActivity.class);
        intent.putExtra("stat",2);
        startActivity(intent);
    }
    public void feedSettings(View view)
    {
        Intent intent=new Intent(this,SettingsActivity.class);
        intent.putExtra("stat",3);
        startActivity(intent);
    }
    /*public void testSettings(View view)
    {
        Intent intent=new Intent(this,SettingsActivity.class);
        intent.putExtra("stat",2);
        startActivity(intent);
    }*/


}
