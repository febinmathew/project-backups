package com.hash.around.Utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.hash.around.OtherClasses.DataTypes.LocationData;
import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.OtherClasses.Interfaces.JobCompletionWithFailureListner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Febin on 6/13/2018.
 */

public class UserManager {
    static FirebaseUser userInfo=null;

    public static FirebaseUser getUserInfo() {
        if(userInfo==null)
            userInfo= FirebaseAuth.getInstance().getCurrentUser();
        return userInfo;
    }

    public static void updateUserChatDestroAndSchedule(String userID,Integer destroTime,Long scheduleTime)
    {
        Log.e("MGG","schedule time "+scheduleTime);
        ContentValues values=new ContentValues();
        if(destroTime!=null)
            values.put(DatabaseAccesser.user_chat_expiretime,destroTime);
        if(scheduleTime!=null)
            values.put(DatabaseAccesser.user_chat_scheduletime,scheduleTime);

       long rowln= DatabaseManager.getDatabase().getWritableDatabase().update(DatabaseAccesser.user_table,values,
                DatabaseAccesser.user_user_id+"=?",new String[]{userID});
        Log.e("MGG","rows affect "+rowln);

    }
    public static void updateUserChatNotification(String userID,String not_uri,Integer vibration,Integer not_light,Long muteVal)
    {
        ContentValues values=new ContentValues();
        if(not_uri!=null)
        values.put(DatabaseAccesser.user_notification_sound,not_uri);
        if(vibration!=null)
            values.put(DatabaseAccesser.user_notification_vibration,vibration);
        if(not_light!=null)
            values.put(DatabaseAccesser.user_notification_light,not_light);
        if(muteVal!=null)
            values.put(DatabaseAccesser.user_mutetime,muteVal);


        DatabaseManager.getDatabase().getWritableDatabase().update(DatabaseAccesser.user_table,values,
                DatabaseAccesser.user_user_id+"=?",new String[]{userID});
    }
    public static void getuserChatDestro()
    {

    }

    public static void updateLastReceiveTime(int type, long sl, DatabaseReference dbRef, final JobCompletionWithFailureListner listener)

    {
        HashMap statusMap = new HashMap();
        statusMap.put("numberType",type);
        if(type==0)
        statusMap.put("last_msg",sl);
        else
            statusMap.put("last_msg",sl);
        statusMap.put("timestamp",System.currentTimeMillis());

        dbRef.push().setValue(statusMap).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                if (listener!=null)
                    listener.onJobCompletion("");
            }
        });
    }
    public static ArrayList<UserWholeData> getUsersList(Context context, boolean following_only)
    {
        SQLiteDatabase db = DatabaseManager.getDatabase().getReadableDatabase();

        Cursor cursor;
        if(following_only)
        {
            cursor = db.query(DatabaseAccesser.user_table, null,
                    DatabaseAccesser.user_user_follow_type+" =?",
                    new String[]{"1"},
                    null, null, null,null);
        }
        else {
            cursor = db.query(DatabaseAccesser.user_table, null,
                    null,
                    null,
                    null, null, null, null);
        }
        ArrayList<UserWholeData> finalData=new  ArrayList<UserWholeData>();
        while (cursor.moveToNext()) {

            UserWholeData temp = new UserWholeData();

            temp.user_id = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_user_id));
            temp.user_name = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_name));
            temp.status = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_status));
            temp.img_url = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_localUrl));
            temp.priv_profile_picture = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_priv_profile_picture_status));
            temp.remoteContact = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_remote_contact));
            temp.followType = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_user_follow_type));

            finalData.add(temp);
        }
        cursor.close();

        return finalData;
    }
}
