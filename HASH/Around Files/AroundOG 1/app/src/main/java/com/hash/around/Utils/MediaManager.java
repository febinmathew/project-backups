package com.hash.around.Utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ImageReader;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.TimedText;
import android.media.audiofx.Visualizer;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.text.format.DateUtils;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.hash.around.OtherClasses.DataTypes.MediaData;
import com.hash.around.OtherClasses.Interfaces.AudioPlayerListener;
import com.hash.around.OtherClasses.Interfaces.FileUploadListner;
import com.hash.around.R;
import com.hash.around.TabbedActivity;
import com.hash.around.Views.Medias.AroundVisualizerView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import pl.droidsonroids.gif.GifImageView;

import static com.hash.around.TabFragments.TabFragment2.getResizedBitmap;
import static com.hash.around.TabbedActivity.mTabbedContext;

/**
 * Created by Febin on 6/13/2018.
 */

public class MediaManager {
    public static long getMediaSize(Context context, Uri uri) throws Exception {
        InputStream in=null;
        long size = 0;
        try {
            in = context.getContentResolver().openInputStream(uri);
            if (in == null) throw new IOException("Couldn't obtain input stream.");


            byte[] buffer = new byte[4096];
            int read;

            while ((read = in.read(buffer)) != -1) {
                size += read;
            }
        }
        catch (IOException ex)
        {
            Log.e("MGG","first exception: ");
        }
        finally {
            if(in!=null)
                in.close();
        }
        in.close();

        Log.e("MGG","size is : "+size);
        return size;
    }

    public static String getFileExtention(Uri data)
    {
        return MimeTypeMap.getFileExtensionFromUrl(data.toString());
    }
    public static String getFileExtention(String data)
    {
        return MimeTypeMap.getFileExtensionFromUrl(data);
    }
    public static String getFileName(String data)
    {
        return Uri.parse(data).getLastPathSegment();
    }

    public static void openAnyFile(Context context,String fileUri)
    {
        //String url=Environment.getExternalStorageDirectory().getAbsolutePath()+"/Contacts Generator/"+file_name;
        Log.e("MSG","file uri "+URI.create(fileUri));
        Log.e("MSG","file uri "+fileUri);
        File file = new File(fileUri);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(fileUri));
        intent.setDataAndType(Uri.fromFile(file), mimeType);
        Intent intent1 = Intent.createChooser(intent, "Open With");
        context.startActivity(intent1);
    }

    public static String getLiteLocalMediaFromOrginalMedia(Context context,String uri,int type) throws Exception
    {
       // boolean bitmapcreated;
        Bitmap bitmap = null;
        Bitmap videoBitmap = null;
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        final byte[] byteArray;

        String lite_localUri=null;

        if (type == 1) {
            try {
                bitmap = MediaStore.Images.Media.getBitmap(TabbedActivity.mTabbedContext.getContentResolver(), Uri.parse(uri));
               // bitmapcreated = true;
            } catch (IOException e) {
                e.printStackTrace();
                //bitmapcreated = false;
            }
            //Log.e("MSG",bitmapcreated+" : bitmap");
            getResizedBitmap(bitmap, 120).compress(Bitmap.CompressFormat.PNG, 1, bytes);
            byteArray = bytes.toByteArray();
            lite_localUri=saveByteIntoCacheAsFile(byteArray,StringUtils.randomStringGenerator() + ".png");
          }
        else if (type == 3) {


            MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();

            mediaMetadataRetriever.setDataSource(context, Uri.parse(uri));

            bitmap = mediaMetadataRetriever.getFrameAtTime(10, MediaMetadataRetriever.OPTION_NEXT_SYNC);


            getResizedBitmap(bitmap, 1000).compress(Bitmap.CompressFormat.PNG, 1, bytes);
            byteArray = bytes.toByteArray();
            lite_localUri=saveByteIntoCacheAsFile(byteArray,StringUtils.randomStringGenerator() + ".png");


        }
        else if(type==5)
        {
            GifImageView ffff=new GifImageView(context);
            ffff.setImageURI(Uri.parse(uri));

            Drawable xxx=ffff.getDrawable();
            if(xxx.getIntrinsicWidth() <= 0 || xxx.getIntrinsicHeight() <= 0) {
                bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
            } else {
                bitmap = Bitmap.createBitmap(xxx.getIntrinsicWidth(), xxx.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);

            }
            Canvas canvas = new Canvas(bitmap);
            xxx.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            xxx.draw(canvas);


            getResizedBitmap(bitmap, 500).compress(Bitmap.CompressFormat.PNG, 1, bytes);

            byteArray = bytes.toByteArray();
            lite_localUri=saveByteIntoCacheAsFile(byteArray,StringUtils.randomStringGenerator() + ".png");


        }


        return lite_localUri;

    }
    public static void uploadByteFileAndReturnUrl(Context context, byte[] byteArray, final StorageReference storageRef, final FileUploadListner listner) {
        storageRef.child(StringUtils.randomStringGenerator() + ".bin").putBytes(byteArray).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> taskSnapshot) {
                if (taskSnapshot.isSuccessful()) {
                    String global = taskSnapshot.getResult().getDownloadUrl().toString();
                    //Log.e("MSGG", "locallite is is " + finalLitelocal);

                    listner.onFileUploaded(global, "null", "null");
                } else {
                    // Failed
                }
            }
        });
    }


        public static void uploadResourceAndReturnUrls(Context context, final int type, String uri, final StorageReference storageRef, final FileUploadListner listner) {
        boolean bitmapcreated;
        Bitmap bitmap = null;
        Bitmap videoBitmap = null;


        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        final byte[] byteArray;

       String lite_localUri=null;

        String name=null,name2=null;
        if (type == 1) {
            try {
                bitmap = MediaStore.Images.Media.getBitmap(TabbedActivity.mTabbedContext.getContentResolver(), Uri.parse(uri));
                bitmapcreated = true;
            } catch (IOException e) {
                e.printStackTrace();
                bitmapcreated = false;
            }
            //Log.e("MSG",bitmapcreated+" : bitmap");
            getResizedBitmap(bitmap, 120).compress(Bitmap.CompressFormat.PNG, 1, bytes);
            name = StringUtils.randomStringGenerator() + ".png";
            byteArray = bytes.toByteArray();
            //name2 = StringUtils.randomStringGenerator() + ".png";
        } else if (type==2)
        {
            byteArray=null;
            name = StringUtils.randomStringGenerator() + "."+MediaManager.getFileExtention(uri);

        }
        else if (type == 3) {


            MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();

            mediaMetadataRetriever.setDataSource(context, Uri.parse(uri));

            Bitmap bitmap2 = null;//
            try {
                bitmap2 =mediaMetadataRetriever.getFrameAtTime(10, MediaMetadataRetriever.OPTION_NEXT_SYNC);
                // MediaStore.Images.Media.getBitmap(context.getContentResolver(), Uri.parse(uri));
            } catch (Exception e) {
                e.printStackTrace();
            }


            getResizedBitmap(bitmap2, 1000).compress(Bitmap.CompressFormat.PNG, 1, bytes);

            name = StringUtils.randomStringGenerator() + (type==3?".mp4":".gif");
            byteArray = bytes.toByteArray();

            lite_localUri=saveByteIntoCacheAsFile(byteArray,StringUtils.randomStringGenerator() + ".png");


        }
        else if(type==5)
        {
            GifImageView ffff=new GifImageView(context);
            ffff.setImageURI(Uri.parse(uri));

            Drawable xxx=ffff.getDrawable();
            /*BitmapDrawable bitmapDrawable = (BitmapDrawable) xxx.get;
            Bitmap bit=bitmapDrawable.getBitmap();*/
            Bitmap bit=null;

            if(xxx.getIntrinsicWidth() <= 0 || xxx.getIntrinsicHeight() <= 0) {
                bit = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
            } else {
                bit = Bitmap.createBitmap(xxx.getIntrinsicWidth(), xxx.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);

            }
            Canvas canvas = new Canvas(bit);
            xxx.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            xxx.draw(canvas);


            getResizedBitmap(bit, 500).compress(Bitmap.CompressFormat.PNG, 1, bytes);

            name = StringUtils.randomStringGenerator() + ".gif";
            byteArray = bytes.toByteArray();

            lite_localUri=saveByteIntoCacheAsFile(byteArray,StringUtils.randomStringGenerator() + ".png");

        }
        else if(type==6)
        {
            byteArray=null;
            name = StringUtils.randomStringGenerator() + "."+MediaManager.getFileExtention(uri);
            Log.e("MGG","file name is "+name);
        }
        else
        {
           // return ;
            byteArray=null;
            name = StringUtils.randomStringGenerator() + ".png";
        }



        final String finalLitelocal=lite_localUri;


        Log.e("MGG","audio name "+name);
        storageRef.child(name).putFile(Uri.parse(uri)).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                Log.e("MGG","audio failed ");
                if (task.isSuccessful()) {

                    final String global = task.getResult().getDownloadUrl().toString();

                    Log.e("MSGG","global is "+global);
                    Log.e("MGG","audio uploadcomplete ");
                    if(type==1||type==3||type==5) {
                        storageRef.child(StringUtils.randomStringGenerator() + ".png").putBytes(byteArray).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> taskSnapshot) {
                                if (taskSnapshot.isSuccessful()) {
                                    String global_lite = taskSnapshot.getResult().getDownloadUrl().toString();
                                    Log.e("MSGG", "locallite is is " + finalLitelocal);

                                    listner.onFileUploaded(global, global_lite, finalLitelocal);
                                } else {
                                    // Failed
                                }
                            }
                        });
                    }
                    else
                    {Log.e("MGG","audio calling next step ");
                        listner.onFileUploaded(global, "null"+StringUtils.randomStringGenerator(), finalLitelocal);
                    }



                }

            }});

    }

    public static String saveByteIntoCacheAsFile(byte[] data,String name)
    {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "Around Files/Media/cache");
        if (!file.exists()) {
            file.mkdirs();
        }
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(new File(file.getAbsolutePath(), name));
            fos.write(data);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String path=new File(file,name).toURI().toString();
        Log.e("MGG","new file : "+path+" / "+name);
        return path;
    }


    private static MediaPlayer mediaPlayer;
    private static Timer mediaPlayerTimer;
    private static TimerTask mediaPlayertask;
    private static int mediaFileLengthInMilliseconds;
    private static SeekBar bfmusicSeekbar;
    private static String audioMediauri="";
    private static AudioPlayerListener audioListener;
    private static TextView updateText;

    public static void resetMediaPlayer()
    {

        if(mediaPlayer!=null)
        {
            try{
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer=null;}
            catch (Exception e)
            {}
        }
        if(mediaPlayerTimer!=null)
        {
            try{
            mediaPlayerTimer.cancel();
            mediaPlayerTimer.purge();}
             catch (Exception e)
            {}
        }

    }
    public static long getMediaDuration(Context context,String mediaUri)
    {

        Uri uri = Uri.parse(mediaUri);
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        mmr.setDataSource(context,uri);
        String durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        int millSecond = Integer.parseInt(durationStr);
        return millSecond;
    }

   public static void playAudio(Context context, String audioUri, SeekBar bfmusicSeek, TextView updateTxt, AroundVisualizerView visualizerView,AudioPlayerListener listner)
    {
        if(mediaPlayer!=null)
        {
            audioListener.onPaused();
        }
        if(bfmusicSeekbar!=null)
            bfmusicSeekbar=null;
       bfmusicSeekbar=bfmusicSeek;
        Log.e("MGG","audioo starting "+audioUri);
        audioListener=listner;

        if(!audioMediauri.equals(audioUri))
        {
            resetMediaPlayer();
            audioMediauri=audioUri;
        }
        updateText=updateTxt;

        setUpMediaPlayer(context,audioUri,visualizerView);
    }

    private static  void setUpMediaPlayer(Context context,String audioUri,AroundVisualizerView visualizerView)
    {
        if(mediaPlayer==null)
        {
            Log.e("MGG","audioo creating mediaplayer ");
            mediaPlayer=new MediaPlayer();
            if(audioUri.equals("null"))
                mediaPlayer = MediaPlayer.create(context, R.raw.music_file);
            else
                mediaPlayer = MediaPlayer.create(context, Uri.parse(audioUri));

            //mediaPlayer.prepare();
            mediaFileLengthInMilliseconds=mediaPlayer.getDuration();
            Log.e("MGG","audioo duration "+mediaFileLengthInMilliseconds);

            mediaPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                @Override
                public void onBufferingUpdate(MediaPlayer mp, int percent) {
                   // bfmusicSeekbar.setSecondaryProgress(percent);  /****/
                }
            });
           /* mediaPlayer.setOnTimedTextListener(new MediaPlayer.OnTimedTextListener() {
                @Override
                public void onTimedText(MediaPlayer mp, TimedText text) {
                    Log.e("MSG","media timer "+text);
                    updateText.setText(text.getText());
                }
            });*/
            mediaPlayertask = new TimerTask() {
                @Override
                public void run() {

                    Log.e("MGG","changing  now");
                    if (mediaPlayer.isPlaying()) {

                        float pos=((float)mediaPlayer.getCurrentPosition()/mediaFileLengthInMilliseconds)*100;
                        bfmusicSeekbar.setProgress((int)pos);
                        Log.e("MGG","installing to "+mediaPlayer.getCurrentPosition()+ " scroll is "+bfmusicSeekbar.getProgress());
                       // if(updateText!=null)

                        audioListener.onIntervelUpdate(mediaPlayer.getCurrentPosition());

                    }

                }
            };

            mediaPlayerTimer = new Timer();

            mediaPlayerTimer.schedule(mediaPlayertask, 0, 400);


            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    //playPauseButton.setImageResource(R.drawable.ic_play_arrow_accent);
                    if (mVisualizer!=null)
                    mVisualizer.setEnabled(false);
                    bfmusicSeekbar.setProgress(0);
                    if(audioListener!=null)
                        audioListener.onCompleted();
                }
            });



            bfmusicSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    //int playPositionInMillisecconds = (mediaFileLengthInMilliseconds / 100) * bfmusicSeekbar.getProgress();
                    // mediaPlayer.pause();
                    // if(!mediaPlayer.isPlaying())
                    // mediaPlayer.seekTo(playPositionInMillisecconds);
                    // mediaPlayer.start();
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {


                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    int playPositionInMillisecconds = (mediaFileLengthInMilliseconds / 100) * bfmusicSeekbar.getProgress();
                    //pauseMediaPlayer();
                    mediaPlayer.seekTo(playPositionInMillisecconds);
                    Log.e("MGG","seeked to "+playPositionInMillisecconds+ " scroll is "+bfmusicSeekbar.getProgress());
                }
            });


            if(visualizerView!=null && PermissionManager.checkHasPermission(context,((Activity)context),new String[]{
                    Manifest.permission.RECORD_AUDIO
            })) {
                Log.e("GGG","visualizer SETTING");
                setupVisualizerFxAndUI(visualizerView);
            }


        }
        if(mediaPlayer.isPlaying())
        {
            pauseMediaPlayer();
        }
        else
            {

                Log.e("MGG","audioo medi is  playing ");
            startMediaPlayer();
        }
    }
    private static Visualizer mVisualizer;

    private static void setupVisualizerFxAndUI(final AroundVisualizerView visualizerView) {

        // Create the Visualizer object and attach it to our media player.
        mVisualizer = new Visualizer(mediaPlayer.getAudioSessionId());
        mVisualizer.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
        mVisualizer.setDataCaptureListener(
                new Visualizer.OnDataCaptureListener() {
                    public void onWaveFormDataCapture(Visualizer visualizer,
                                                      byte[] bytes, int samplingRate) {
                        visualizerView.updateVisualizer(bytes);
                    }

                    public void onFftDataCapture(Visualizer visualizer,
                                                 byte[] bytes, int samplingRate) {
                    }
                }, Visualizer.getMaxCaptureRate() / 2, true, false);
    }
    static void pauseMediaPlayer()
    {
        //playPauseButton.setImageResource(R.drawable.ic_play_arrow_accent);

        mediaPlayer.pause();
        if(audioListener!=null)
            audioListener.onPaused();
        //mediaPlayerTimer.cancel();
    }
    static void startMediaPlayer()
    {
        //playPauseButton.setImageResource(R.drawable.ic_pause_accent);
        if (mVisualizer!=null)
            mVisualizer.setEnabled(true);


        mediaPlayer.start();

        if(audioListener!=null)
            audioListener.onStarted();

    }
    public static Bitmap getSquareBitmap(Bitmap bitmap)
    {
        Bitmap dstBmp;
        if (bitmap.getWidth() >= bitmap.getHeight()){

            //Log.e("MGG","width greter ");
            dstBmp = Bitmap.createBitmap(
                    bitmap,
                    bitmap.getWidth()/2 - bitmap.getHeight()/2,
                    0,
                    bitmap.getHeight(),
                    bitmap.getHeight()
            );


        }else{

            dstBmp = Bitmap.createBitmap(
                    bitmap,
                    0,
                    bitmap.getHeight()/2 - bitmap.getWidth()/2,
                    bitmap.getWidth(),
                    bitmap.getWidth()
            );
        }
        return dstBmp;
    }



}
