package com.hash.around.Views.Medias;

/**
 * Created by Febin on 5/27/2018.
 */

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

/*import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.FitCenter;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader.DecryptableUri;
import org.thoughtcrime.securesms.mms.GlideRequest;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.mms.SlideClickListener;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.whispersystems.libsignal.util.guava.Optional;*/

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnPausedListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.hash.around.OtherClasses.DataTypes.ChatData;
import com.hash.around.OtherClasses.DataTypes.GroupChatData;
import com.hash.around.OtherClasses.Interfaces.ChatSentListner;
import com.hash.around.OtherClasses.Interfaces.FileUploadListner;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.R;
import com.hash.around.Utils.ChatSender;
import com.hash.around.Views.CustomMakers.AroundDownloadView;
import com.pitt.library.fresh.FreshDownloadView;

import java.io.File;
import java.io.IOException;


//import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class AroundChatImageThumbline extends RelativeLayout {

    private static final String TAG = AroundChatImageThumbline.class.getSimpleName();


    private ImageView       image;
    AroundDownloadView downloadView;
    private ImageView       playOverlay,gif_overlay;
    //private int             backgroundColorHint;
    private int             radius;
    TextView sizeText;
    ChatSentListner uploadListner = null;
    JobCompletionListner jobComplete = null;

LinearLayout downloadViewBG;



    public AroundChatImageThumbline(Context context) {
        this(context, null);
    }

    public AroundChatImageThumbline(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AroundChatImageThumbline(final Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        inflate(context, R.layout.around_image_thumbline_view, this);

        this.radius      =getResources().getDimensionPixelSize(R.dimen.default_corner_radius);
        this.image       = findViewById(R.id.thumbnail_image);
        this.playOverlay = findViewById(R.id.play_overlay);
        this.gif_overlay=findViewById(R.id.gif_overlay);
        sizeText=findViewById(R.id.sizeText);
        downloadView=findViewById(R.id.downloadView);
        downloadViewBG=findViewById(R.id.downloadViewBG);

        if (attrs != null) {
            TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.AroundChatImageThumbline, 0, 0);
            //backgroundColorHint   = typedArray.getColor(R.styleable.AroundChatImageThumbline_backgroundColorHint, Color.BLACK);

            typedArray.recycle();
        }

        /*FreshDownloadView ddownloadView=findViewById(R.id.downloadView);
        Log.e("MSG", "download ready  "+downloadView);
        ddownloadView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                downloadFile("https://firebasestorage.googleapis.com/v0/b/aroundsocialmedia77.appspot.com/o/profile_images%2Fxxwzvdmnzbfpflfinkbt.png?alt=media&token=b49efc28-3cc1-4361-87c1-de52b57db3cb");
            }
        });*/
    }

    public void setImageUri(Uri uri)
    {
        this.image.setImageURI(uri);
    }

    public ImageView getImageView()
    {return this.image;}

    public void setImageScaleType(boolean isChat)
    {
        if(isChat)
            this.image.setScaleType(ImageView.ScaleType.CENTER_CROP);
        else
            this.image.setScaleType(ImageView.ScaleType.FIT_CENTER);
    }

    public void setSizeText(String text)
    {
        sizeText.setText(text);
    }

    int mediaType;
    public void setMediaType(int type)
    {

        this.mediaType=type;
    }
    public void updateMideaWithType(int visibility)
    {
        switch (mediaType)
        {
            case 1:
                playOverlay.setVisibility(GONE);
                gif_overlay.setVisibility(GONE);
                break;
            case 3:
                gif_overlay.setVisibility(GONE);
                playOverlay.setVisibility(visibility);

                break;
            case 5:
                playOverlay.setVisibility(GONE);
                gif_overlay.setVisibility(visibility);
                break;
        }

    }

    public void enableDownloadClick()
    {
        downloadViewBG.setVisibility(VISIBLE);
        //downloadView.setVisibility(VISIBLE);

        //sizeText.setVisibility(VISIBLE);
        //playOverlay.setVisibility(GONE);
    }
    public void disableDownloadClick() {
        downloadViewBG.setVisibility(GONE);
        Log.e("MSG", "disable");
        //downloadView.setVisibility(GONE);
        downloadView.setOnClickListener(null);
        //sizeText.setVisibility(GONE);

        updateMideaWithType(VISIBLE);

    }

    public void setDownloadClickListner(String uri) {
        enableDownloadClick();
        downloadView.setIsDownload();
        final String url = uri;
        downloadView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadFile(url);
            }
        });
    }
    public void setOnJobCompletionListner(JobCompletionListner job) {
        this.jobComplete = job;
    }

    public void setOnFileUploadListener(ChatSentListner job) {
        this.uploadListner = job;
    }

    public void setUploadClickListner(String uri, final GroupChatData data,final long mediaRow,final StorageReference ref) {
        enableDownloadClick();
        downloadView.setIsUpload();
        final String url = uri;
        downloadView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadFile(url,ref,data,mediaRow);
            }
        });
    }
    //GroupChatData thisChatData;
    void downloadFile(String urlToDownload) {
        Log.e("MSG", "download "+urlToDownload);
        downloadView.startDownload();

        File localFile = null;
        final String localFilename;
        try {
            localFile = File.createTempFile("images", "png");

        } catch (IOException e) {
            e.printStackTrace();
        }
        localFilename = localFile.toURI().toString();
        StorageReference httpsReference = FirebaseStorage.getInstance().getReferenceFromUrl(urlToDownload);
        httpsReference.getFile(localFile).addOnProgressListener(new OnProgressListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onProgress(FileDownloadTask.TaskSnapshot taskSnapshot) {
                float percent = ((float) taskSnapshot.getBytesTransferred() / (float) taskSnapshot.getTotalByteCount()) * 100;
                // Log.e("MSG",((float)taskSnapshot.getBytesTransferred()/(float)taskSnapshot.getTotalByteCount())*100+"//\\"+
                //((float)taskSnapshot.getBytesTransferred()/(float)taskSnapshot.getTotalByteCount())+"//\\"+percent);
                downloadView.upDateProgress((int) percent);
            }
        }).addOnFailureListener(new OnFailureListener() {

            @Override
            public void onFailure(@NonNull Exception e) {
                downloadView.showDownloadError();
            }
        }).addOnPausedListener(new OnPausedListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onPaused(FileDownloadTask.TaskSnapshot taskSnapshot) {
                downloadView.reset();
                Log.e("MSG", "paused");

            }
        }).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                // downloadView.showDownloadOk();
                Log.e("MSG", "succeded");
                disableDownloadClick();
                if (jobComplete != null) {
                    jobComplete.onJobCompletion(localFilename);
                }
                //downloadView.clearAnimation();

            }
        })

        ;
    }

    void uploadFile(String urlToUpload,StorageReference ref,GroupChatData data,long mediaID) {
        Log.e("MSGG","uri is : "+urlToUpload );
        int type=mediaType;


        ChatSender.uploadMediaData(urlToUpload,data,uploadListner);
        /*uploadResourceAndReturnUrls(numberType, urlToUpload, ref,new FileUploadListner() {
            @Override
            public void onFileUploaded(String global, String globalLite) {

                uploadListner.onFileUploaded(global,globalLite);
            }
        });*/
    }

    public void setLocallyAvailable(boolean locallyAvailable)
    {
        if(locallyAvailable)
        {
            disableDownloadClick();
        }
        else
        {
            enableDownloadClick();
        }
    }


    private final RectF drawRect = new RectF();
    private final Path clipPath = new Path();
    int dimen=getResources().getDimensionPixelSize(R.dimen.default_corner_radius);

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        drawRect.left = 0;
        drawRect.top = 0;
        drawRect.right = getWidth();
        drawRect.bottom = getHeight();
        clipPath.reset();
        clipPath.addRoundRect(drawRect, dimen, dimen, Path.Direction.CCW);
        clipPath.close();
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        int save = canvas.save();
        canvas.clipPath(clipPath);
        super.dispatchDraw(canvas);
        canvas.restoreToCount(save);
    }

}
