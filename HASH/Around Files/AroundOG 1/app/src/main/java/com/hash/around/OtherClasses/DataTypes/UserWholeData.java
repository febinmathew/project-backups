package com.hash.around.OtherClasses.DataTypes;

/**
 * Created by Febin on 26-08-2017.
 */

public class UserWholeData extends  ContactListData {

    public int chat_expiretime =0;
    public long chat_scheduletime =0;
    public String not_uri;
    public int not_vibration=0;
    public int not_light=0;
    public int muteTime=0;


    public int followType=0;
    public int chatType=0;
    public int remoteContact=0;

    public int priv_activity_status=0;
    public int priv_location=0;
    public int priv_profile_picture=0;
    public int priv_profile_status=0;
    public int priv_number=0;

    public int priv_follow_privacy;
    public int priv_chat_privacy;
    public int priv_profile_music;



    UserPrivacyData privs=null;   //real way remove others




    public UserWholeData()
    {

    }
    public UserWholeData(ContactListData data1)
    {
        super(data1);
    }
    public UserWholeData(ContactListData data1,UserPrivacyData data2)
    {
        super(data1);
        mergeUserPrivacy(data2);
    }
    public void mergeUserPrivacy(UserPrivacyData data)
    {
        priv_activity_status=data.activity_status;
        priv_location=data.location;
        priv_profile_picture=data.profile_picture;
        priv_profile_status=data.profile_status;
        priv_number=data.number;
        priv_follow_privacy=data.anyonefollow;
        priv_chat_privacy=data.anyonemessage;
        priv_profile_music=data.profile_music;
        privs=data;
    }

    public UserPrivacyData getUserPrivacyData()
    {
        return privs;
    }

}



