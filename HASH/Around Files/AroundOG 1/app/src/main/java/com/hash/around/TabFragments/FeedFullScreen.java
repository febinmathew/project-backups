package com.hash.around.TabFragments;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
//import android.widget.Toast;
import android.widget.VideoView;

import com.carlosmuvi.segmentedprogressbar.SegmentedProgressBar;
import com.hash.around.ChatWindow;
import com.hash.around.OtherClasses.DataTypes.ChatReplayData;
import com.hash.around.OtherClasses.DataTypes.GroupFeedData;
import com.hash.around.OtherClasses.DataTypes.MediaData;
import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.OtherClasses.DataTypes.FeedData;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.Profile_Activity;
import com.hash.around.R;
import com.hash.around.TabbedActivity;
import com.hash.around.Views.AroundChatInputSection;

import com.hash.around.Views.CustomMakers.FeedViewPager;
import com.hash.around.Views.CustomMakers.SwipeToCommentView;
import com.hash.around.Views.Medias.AroundCircularImageView;
import com.hash.around.Views.Medias.AroundDownloadAudioView;
import com.hash.around.Views.Medias.AroundDownloadGifView;
import com.hash.around.Views.Medias.AroundDownloadImageView;
import com.hash.around.Views.Medias.AroundDownloadVideoView;
import com.hash.around.Views.ViewFunctions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class FeedFullScreen extends AppCompatActivity {

Toolbar toolbar;
    VideoView videoView;
    SegmentedProgressBar sBar;
    FeedViewPager viewPager;
    ProgressBar progressView;
String userName,userId;
//int initFeedNumner;
    int check=0,pos=0,lastPage;
    ArrayList<GroupFeedData> finaldata=new ArrayList<GroupFeedData>();
    TextView feeduserNameFull;
    LinearLayout progressLayout;
    SQLiteDatabase db;
    TextView timeText;
    UserWholeData userData;

    AroundChatInputSection chatTypeSection;
    AroundCircularImageView feedUserImage;
    SwipeToCommentView swipeView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feed_full_screen);
        check = 1;
        toolbar = (Toolbar) findViewById(R.id.comtoolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        toolbar.findViewById(R.id.up_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FeedFullScreen.super.onBackPressed();
            }
        });


        feeduserNameFull = (TextView) findViewById(R.id.feeduserNameFull);

        timeText = (TextView) findViewById(R.id.feedFullTime);
        feedUserImage = findViewById(R.id.feedUserImage);
        swipeView = findViewById(R.id.swipeView);
        swipeView.setJobListener(new JobCompletionListner() {
            @Override
            public void onJobCompletion(String txt1) {
                showChatBox();
            }
        });

        userName = getIntent().getStringExtra("username");

        userId = getIntent().getStringExtra("userid");

        //initFeedNumner= getIntent().getIntExtra("feednumber",0);

        Long feedTarget = getIntent().getLongExtra("targetFeed", 0);

        chatTypeSection = findViewById(R.id.chatTypeSection);
        userData = ChatWindow.returnSingleUserDetails(userId);
        if (userData != null)
            chatTypeSection.setUserData(this.userData);
        if (userData.img_url == null || userData.img_url.equals("null"))
            feedUserImage.setImageResource(R.drawable.default_user);
        else
            feedUserImage.setImageURI(Uri.parse(userData.img_url));

        chatTypeSection.getSendMsgButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                MediaData dat = new MediaData();
                dat.type = 0;
                dat.uri = null;
                dat.text = chatTypeSection.getEditTextView().getText().toString();

                chatTypeSection.getEditTextView().setText("");

                /*ChatReplayData replayData=new ChatReplayData();
                replayData.userName=userData!=null?userData.user_name:userName;
                replayData.userID=userData!=null?userData.user_id:userId;
                replayData.type=2;
                replayData.description=finaldata.get(viewPager.getCurrentItem()).text;
                replayData.contentUri=finaldata.get(viewPager.getCurrentItem()).semi_uri;
                replayData.content_sl=finaldata.get(viewPager.getCurrentItem()).feedRow;*/

                Log.e("GGG", "senting new chat");
                Intent broadcast = new Intent("around.NEWCHAT_FROMOUT");
                broadcast.putExtra("mediaarray", dat);
                broadcast.putExtra("replayData", chatTypeSection.replaySection.getReplayData());
                broadcast.putExtra("userData", userData);
                sendBroadcast(broadcast);

            }
        });


        feeduserNameFull.setText(userName);
        db = TabbedActivity.db.getReadableDatabase();


        Cursor cursor = db.query(DatabaseAccesser.feed_table, new String[]{
               DatabaseAccesser.feed_table+".rowid AS rowID"
                ,"*"},
                DatabaseAccesser.feed_col_user_id + " =?",
                new String[]{userId},
                null, null, DatabaseAccesser.feed_col_seen + " desc", null);


        int p = cursor.getCount();
        // Toast.makeText(this, p+" "+userId, Toast.LENGTH_SHORT).show();

        while (cursor.moveToNext()) {
            GroupFeedData temp = new GroupFeedData();
            //FeedDataList temp2 = new FeedDataList();
            temp.feedRow = cursor.getLong(
                    cursor.getColumnIndexOrThrow("rowID"));

            temp.userid = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_user_id));
            temp.username = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_username));
            temp.timestamp = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_timestamp));
            temp.type = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_type));

            temp.ifseen = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_seen));


            temp.text = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_text));


            long type = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_media_id));

            if (type == 1) {
                temp.semi_uri = temp.uri = "null";
            } else {

                Cursor cursor2 = db.query(DatabaseAccesser.media_table,
                        new String[]{
                                "rowid," +
                                        DatabaseAccesser.media_global_uri,
                                DatabaseAccesser.media_local_uri,
                                DatabaseAccesser.media_lite_global_uri,
                                DatabaseAccesser.media_lite_local_uri,
                        },
                        "rowid " + " =?",
                        new String[]{type + ""},
                        null, null, null, null);
                if (cursor2.getCount() == 1) {
                    cursor2.moveToNext();

                    temp.uri = cursor2.getString(
                            cursor2.getColumnIndexOrThrow(DatabaseAccesser.media_local_uri));
                    if (temp.uri.equals("null")) {
                        temp.uri = cursor2.getString(
                                cursor2.getColumnIndexOrThrow(DatabaseAccesser.media_global_uri));
                    }

                    temp.semi_uri = cursor2.getString(
                            cursor2.getColumnIndexOrThrow(DatabaseAccesser.media_lite_local_uri));

                }
            }

            finaldata.add(temp);
            if (check == 1 && temp.ifseen == 0) {
                pos = finaldata.size() - 1;
            }
            check = temp.ifseen;

        }
        /*Uri videouri = Uri.parse("android.resource://com.hash.around" + "/"
                + R.raw.movie);*/
        /// videoView.setMediaController(new MediaController(getApplicationContext()));
        //  videoView.setVideoURI(videouri);

        // final VideoView p=videoView;
       /* videoView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                p.start();
                return true;
            }
        });*/
      /* Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        viewPager = (FeedViewPager) findViewById(R.id.feedViewPager);
        MyPagerAdapter mMyPagerAdapter = new MyPagerAdapter();

        viewPager.setAdapter(mMyPagerAdapter);

        if (feedTarget != 0){
            for (int i = 0; i < finaldata.size(); i++) {
                if (finaldata.get(i).feedRow == feedTarget) {
                    pos = i;
                    break;
                }
            }
    }


        viewPager.setCurrentItem(pos);
        lastPage=pos;

        timeText.setText(ChatWindow.convertTimeStampToText(finaldata.get(pos).timestamp,false));


        /*LayoutInflater vi = (LayoutInflater) getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View v = vi.inflate(R.layout.progress_bar_round, null);
        progressView=(ProgressBar)v.findViewById(R.id.feedProgress);*/

        progressLayout=(LinearLayout)findViewById(R.id.progressLayout);

        LayoutInflater vi = (LayoutInflater) getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for(int i=0;i<finaldata.size();i++)
        {


            View v = vi.inflate(R.layout.progress_bar_round, null);
            progressView=(ProgressBar) v.findViewById(R.id.feedProgress);
            //ProgressBar pp=progressView;
            progressView.setLayoutParams(new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,1.0f));
            if(finaldata.get(i).ifseen==1)
                progressView.setSecondaryProgress(0);
            else
                progressView.setSecondaryProgress(1);
            progressLayout.addView(progressView);

        }

        ((ProgressBar)progressLayout.getChildAt(pos)).setProgress(1);




        //sBar.setSegmentCount(mMyPagerAdapter.getCount());
        viewPager.setPageTransformer(true, new DepthPageTransformer());
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                //updateFeedViewed(position);
            }

            @Override
            public void onPageSelected(int position) {
                //Toast.makeText(FeedFullScreen.this, "mm  "+lastPage, Toast.LENGTH_SHORT).show();
                if(lastPage>position)
                {
                    updateFeedViewed(lastPage);
                }
                else if(lastPage<position)
                        {
                            updateFeedViewed(lastPage);
                }

                ((ProgressBar)progressLayout.getChildAt(lastPage)).setProgress(0);
                ((ProgressBar)progressLayout.getChildAt(lastPage)).setSecondaryProgress(0);
                lastPage=position;
                ((ProgressBar)progressLayout.getChildAt(lastPage)).setProgress(1);



                timeText.setText(ChatWindow.convertTimeStampToText(finaldata.get(position).timestamp,false));


                chatTypeSection.replaySection.setReplayDataFromFeed(finaldata.get(position));

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                //if(state==ViewPAger.)
                hideChatBox();
            }
        });
    }

    void showChatBox()
    {
        chatTypeSection.replaySection.setReplayDataFromFeed(finaldata.get(viewPager.getCurrentItem()));
        chatTypeSection.getEditTextView().requestFocus();
        ViewFunctions.animateButtonBurstIn(chatTypeSection,200,false);
        swipeView.setVisibility(View.GONE);
    }
    void hideChatBox()
    {
        Log.e("GGG","hide called");
        if(chatTypeSection.getVisibility()==View.VISIBLE)
            ViewFunctions.animateButtonBurstOut(chatTypeSection,200,false);
        //ViewFunctions.independantAnimateOut(chatTypeSection,getApplicationContext());

        swipeView.setVisibility(View.VISIBLE);
        //ViewFunctions.fadeIn(swipeView);
    }
    public void post_userDetailsClicked(View v)
    {
        Intent intent=new Intent(this, Profile_Activity.class);
        intent.putExtra("userid",userId);
        finish();
        startActivity(intent);
    }


    void updateFeedViewed(final int position) {

        if (finaldata.get(position).ifseen == 0) {

            Profile_Activity.sentCustomNotificationToUser(finaldata.get(position).userid, 100, TabbedActivity.userdetail.getUid()+">><<"+finaldata.get(position).sl, new JobCompletionListner() {
                @Override
                public void onJobCompletion(String txt1) {

                    ContentValues values2 = new ContentValues();
                    values2.put(DatabaseAccesser.feed_col_seen, 1);
                    int rowln = db.update(DatabaseAccesser.feed_table, values2, DatabaseAccesser.feed_col_user_id + " =? AND " +
                            DatabaseAccesser.feed_col_timestamp + " = ?", new String[]{finaldata.get(position).userid, finaldata.get(position).timestamp + ""});

                }
            });
            }
    }

    public static class DepthPageTransformer implements ViewPager.PageTransformer {
        private static final float MIN_SCALE = 0.75f;

        public void transformPage(View view, float position) {
            int pageWidth = view.getWidth();

            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.setAlpha(0);

            } else if (position <= 0) { // [-1,0]
                // Use the default slide transition when moving to the left page
                view.setAlpha(1);
                view.setTranslationX(0);
                view.setScaleX(1);
                view.setScaleY(1);

            } else if (position <= 1) { // (0,1]
                // Fade the page out.
                view.setAlpha(1 - position);

                // Counteract the default slide transition
                view.setTranslationX(pageWidth * -position);

                // Scale the page down (between MIN_SCALE and 1)
                float scaleFactor = MIN_SCALE
                        + (1 - MIN_SCALE) * (1 - Math.abs(position));
                view.setScaleX(scaleFactor);
                view.setScaleY(scaleFactor);

            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.setAlpha(0);
            }
        }
    }
Menu feedMenu;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.feed_full_view_activity, menu);
        this.feedMenu=menu;
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
      // videoView.start();
    }

    @Override
    protected void onDestroy() {
        updateFeedViewed(lastPage);
        Intent new_intent = new Intent();
        new_intent.setAction("FEED_UPDATED");
        sendBroadcast(new_intent);
        super.onDestroy();
    }

    class MyPagerAdapter extends PagerAdapter
    {

        public Object instantiateItem(ViewGroup collection, int position) {
            ViewGroup layout;
            int resId = 0;

           switch (finaldata.get(position).type) {
               case 0:

                   layout = (ViewGroup) getLayoutInflater().inflate(R.layout.view_text, null);
                   TextView viewTextTxt=(TextView) layout.findViewById(R.id.viewTextTxt);
                   viewTextTxt.setText(finaldata.get(position).text);
                   /*ImageView img=(ImageView)layout.findViewById(R.id.singleImage);
                    img.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(FeedFullScreen.this, "Clicked", Toast.LENGTH_SHORT).show();
                        }
                    });*/
                   break;
                case 1:


                    layout = (ViewGroup) getLayoutInflater().inflate(R.layout.view_image, null);
                    AroundDownloadImageView aroundImage;
                    aroundImage=layout.findViewById(R.id.singleAroundImageView);
                    aroundImage.setLocallyAvailable(true);
                    if(finaldata.get(position).uri==null||finaldata.get(position).uri.contains("null"))
                    {
                        aroundImage.setLocallyAvailable(false);
                        aroundImage.setImageViewUri(Uri.parse(finaldata.get(position).semi_uri));
                    }
                    else{
                        aroundImage.setLocallyAvailable(true);
                        aroundImage.setImageViewUri(Uri.parse(finaldata.get(position).uri));}

//                    aroundImage.setImageViewUri(Uri.parse(finaldata.get(position).uri));

                    break;
                case 2:
                    layout = (ViewGroup) getLayoutInflater().inflate(R.layout.view_audio,null);
                    AroundDownloadAudioView aroundAudio;

                    aroundAudio=layout.findViewById(R.id.singleAroundAudioView);
                    aroundAudio.setLocallyAvailable(true);

                    if(finaldata.get(position).uri==null||finaldata.get(position).uri.contains("null"))
                    {
                        aroundAudio.setLocallyAvailable(false);
                    }
                    else{
                        aroundAudio.setLocallyAvailable(true);
                        aroundAudio.setUpAudioUri(Uri.parse(finaldata.get(position).uri));}

                    break;
                case 3:
                    layout = (ViewGroup) getLayoutInflater().inflate(R.layout.view_video, null);

                    AroundDownloadVideoView aroundVideo;

                    aroundVideo=layout.findViewById(R.id.singleAroundVideoView);
                    aroundVideo.setLocallyAvailable(true);

                    if(finaldata.get(position).uri==null||finaldata.get(position).uri.contains("null"))
                    {
                        aroundVideo.setUpVideoUri(Uri.parse(finaldata.get(position).semi_uri));
                    }
                    else{
                        aroundVideo.setLocallyAvailable(true);
                        aroundVideo.setUpVideoUri(Uri.parse(finaldata.get(position).uri));
                    }

                    break;
               case 5:
                   layout = (ViewGroup) getLayoutInflater().inflate(R.layout.view_gif,null); AroundDownloadGifView aroundGif;

                   aroundGif=layout.findViewById(R.id.singleAroundGifView);
                   aroundGif.setLocallyAvailable(true);



                   if(finaldata.get(position).uri==null||finaldata.get(position).uri.contains("null"))
                   {
                       aroundGif.setLocallyAvailable(false);
                       //aroundGif.setUpGifUri(Uri.parse(finaldata.get(position).semi_uri));
                   }
                   else{
                       aroundGif.setLocallyAvailable(true);
                       aroundGif.setUpGifUri(Uri.parse(finaldata.get(position).uri));
                   }

                   break;
                default:
                    layout = null;


            }


          //  LayoutInflater inflater = LayoutInflater.from(mContext);
           // ViewGroup layout = (ViewGroup) inflater.inflate(customPagerEnum.getLayoutResId(), collection, false);
           // collection.addView(layout);
           // layout = (ViewGroup) getLayoutInflater().inflate(R.layout.view_gif,null);

            collection.addView(layout);
            return layout; // return selected view.
        }

        @Override
        public int getCount() {
            return finaldata.size(); // number of maximum views in View Pager.
        }
        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((View) view);
        }



        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1; // return true if both are equal.
        }
    }


    public class SectionsPagerAdapter extends PagerAdapter {

        Context mContext;
        public SectionsPagerAdapter(Context mContext) {
           this.mContext=mContext;
        }

       /* @Override
        public Object instantiateItem(ViewGroup collection, int position) {
            View v = getLayoutInflater().inflate(R.layout.view_image, null);

            collection.addView(v);
            return v;
        }*/

        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((View) view);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return false;
        }


        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "CHATS";
                case 1:
                    return "FEEDS";
                case 2:
                    return "LOCATION";
            }
            return null;
        }

    }

    /*String getTimeFromMils(long msgTime)
    {
        long curTime=System.currentTimeMillis();
        Date dt=new Date(msgTime);
        SimpleDateFormat format;
        if((curTime-86400000)<msgTime) {
            format = new SimpleDateFormat("hh:mm a");
            return format.format(dt);
        }
        else if((curTime-172800000)<msgTime)
        {
            // format = new SimpleDateFormat("EEE MMM dd hh:mm:ss zzz yy a");
            return "YESTERDAY";
        }
        else
        {
            format = new SimpleDateFormat("EEE MMM dd");
            return format.format(dt);
        }
        // return "";
    }*/

}
