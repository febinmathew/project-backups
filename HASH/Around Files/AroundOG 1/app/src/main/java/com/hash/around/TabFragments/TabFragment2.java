package com.hash.around.TabFragments;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.hash.around.AroundAlertBoxManager;
import com.hash.around.ChatWindow;
import com.hash.around.GalleryActivity;
import com.hash.around.MainServiceThread;
import com.hash.around.OtherClasses.DataTypes.GroupFeedData;
import com.hash.around.OtherClasses.DataTypes.MediaData;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.OtherClasses.DataTypes.FeedDataList;
import com.hash.around.OtherClasses.Interfaces.FeedSentListner;
import com.hash.around.OtherClasses.Interfaces.LocationReceiver;
import com.hash.around.OtherClasses.Interfaces.ProcessCompletedListner;
import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.hash.around.Profile_Activity;
import com.hash.around.R;
import com.hash.around.OtherClasses.Adapter_FeedData;
import com.hash.around.OtherClasses.DataTypes.FeedData;
import com.hash.around.TabbedActivity;
import com.hash.around.UploadActivity;
import com.hash.around.Utils.DatabaseManager;
import com.hash.around.Utils.FeedSender;
import com.hash.around.Utils.MediaManager;
import com.hash.around.Utils.UserManager;
import com.hash.around.Views.AroundFeedInputSection;
import com.hash.around.Views.AroundFeedRailView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import static android.app.Activity.RESULT_OK;
import static com.hash.around.TabbedActivity.AROUND_FEED_RADIUS;
import static com.hash.around.TabbedActivity.UserName;
import static com.hash.around.TabbedActivity.fDatabase;
import static com.hash.around.TabbedActivity.fetchFusedLocation;

import static com.hash.around.TabbedActivity.mainStorageReference;
import static com.hash.around.TabbedActivity.prefs;
import static com.hash.around.TabbedActivity.tab_pendingTexts;
import static com.hash.around.TabbedActivity.userdetail;

public class TabFragment2 extends Fragment {

    class FeedReceivers extends BroadcastReceiver{

        public FeedReceivers()
        {
            getActivity().registerReceiver(this, new IntentFilter("around.NEWFEED"));
        }
        ArrayList<MediaData> receiverData;
        @Override
        public void onReceive(Context context, Intent intent) {
            receiverData=(ArrayList<MediaData>)intent.getSerializableExtra("mediaarray");
            for(int i=0;i<receiverData.size();i++)
            {

                quickFeedSent(receiverData.get(i));
            }
            Log.e("MGG","listner recievedd "+receiverData.size());
        }

    }
    public static double latitude, longitude;

    ProgressDialog progDialog;
    RecyclerView rView;
    CardView cView;
    static String key;
    LocationManager locationManager;
    LocationListener listner;
    private LinearLayoutManager mLayoutManager;
    Adapter_FeedData adap;
    ArrayList<GroupFeedData> finaldata;
    ArrayList<FeedDataList> finaldatalist;
    SwipeRefreshLayout swipeRefreshFeedList;
    GeoFire geoFire;
    TextView locationText;
    GeoQuery geoQuery;
    ValueEventListener postListener;
    static String haxa = " ";
    LinearLayout noFeedsAvailable;

    public static Context mTab2Context;
    private BroadcastReceiver feedReciever = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            //Toast.makeText(getApplicationContext(), "received message in activity..!", Toast.LENGTH_SHORT).show();
            fillDataFromDatabase();
            adap.notifyDataSetChanged();

        }
    };

    //DatabaseReference newPostRef;
    /* public ArrayList<FeedData> generateDummy()
     {
         ArrayList<FeedData> list=new ArrayList<>();

         FeedData x=new FeedData();
         x.id=1;
         x.userid=1;
         x.text="Check out this game 99 Miles Runner!! When I first played it, I felt it like a grumpy game, Now it is what I love the most <3 <3 <3";
         x.imgDir="danial";
         x.numberType=1;
         list.add(x);

         x=new FeedData();
         x.id=4;
         x.userid=2;
         x.text="Accedent in the town.. Any one with O+ve blood? HURRY !";
         x.imgDir="acce";
         x.numberType=0;
         list.add(x);




         x=new FeedData();
         x.id=1;
         x.userid=1;
         x.text="Time on marine drive!!!....fun with besties #fun #around_mood #gamersconnect :)";
         x.imgDir="profile";
         x.numberType=2;
         list.add(x);

         x=new FeedData();
         x.id=3;
         x.userid=1;
         x.text="Kid playing Blue whale on the PC ! WOW! Kinda Funny!";
         x.imgDir="blue";
         x.numberType=0;
         list.add(x);


         x=new FeedData();
         x.id=2;
         x.userid=2;
         x.text="Time on marine drive!!!....fun with besties #fun #around_mood #gamersconnect :)";
         x.imgDir="profile";
         x.numberType=0;
         list.add(x);



         x=new FeedData();
         x.id=2;
         x.userid=2;
         x.text="Check out this game 99 Miles Runner!! When I first played it, I felt it like a grumpy game, Now it is what I love the most <3 <3 <3";
         x.imgDir="danial";
         x.numberType=2;
         list.add(x);

         x=new FeedData();
         x.id=4;
         x.userid=2;
         x.text="Accedent in the town.. Any one with O+ve blood? HURRY !";
         x.imgDir="acce";
         x.numberType=0;
         list.add(x);



         return list;



     }*/
    public String randomStringGenerator() {
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (
                int i = 0;
                i < 20; i++)

        {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();

    }
ImageButton feedFilterButton,feedSortButton;
     AroundFeedRailView feedRail;
     AroundFeedInputSection feedInputSection;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.feed_list_fragment, container, false);
        new FeedReceivers();
        //TextView textView = (TextView) rootView.findViewById(R.id.section_label);
        //textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
        rView = (RecyclerView) rootView.findViewById(R.id.rView);
        rView.setHasFixedSize(true);
        rView.setNestedScrollingEnabled(false);
        //cView=(CardView)rootView.findViewById(R.id.cView);
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);
        //  mLayoutManager=new GridLayoutManager(this,2,LinearLayoutManager.HORIZONTAL,false);
        // mLayoutManager=new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);
        feedInputSection=rootView.findViewById(R.id.feedInputSection);
        rView.setLayoutManager(mLayoutManager);
        progDialog = new ProgressDialog(getActivity());
        finaldata = new ArrayList<GroupFeedData>();
        finaldatalist = new ArrayList<FeedDataList>();
        noFeedsAvailable=(LinearLayout) rootView.findViewById(R.id.noFeedsAvailable);
        feedFilterButton=rootView.findViewById(R.id.feedFilterButton);
        this.feedRail     = rootView.findViewById(R.id.feedRail);
        feedRail.setVisibility(View.VISIBLE);
        this.getLoaderManager().restartLoader(1, null, feedRail);

        feedFilterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterFeeds();
            }
        });
        feedSortButton=rootView.findViewById(R.id.feedSortButton);
        feedSortButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sortFeeds();
            }
        });
        /*if(true)
        {
            noFeedsAvailable.setVisibility(View.VISIBLE);
        }*/
        feedInputSection.setUserData(ChatWindow.returnSingleUserDetails(TabbedActivity.userdetail.getUid()));

        locationText = (TextView) rootView.findViewById(R.id.locationStatus);
        adap = new Adapter_FeedData(getActivity(), finaldata, finaldatalist, rView);
        rView.setAdapter(adap);
        fillDataFromDatabase();
        //Toast.makeText(getActivity(), "total size : "+ finaldata.size(), Toast.LENGTH_SHORT).show();
        registerForContextMenu(rView);

        mTab2Context=getActivity();

        adap.setOnLoadMoreListener(new ProcessCompletedListner() {
            @Override
            public void processCompleteAction() {

            }

            @Override
            @Nullable
            public void newUserAdded(UserWholeData data) {

            }


        });


        //adap.notifyDataSetChanged();

        rootView.findViewById(R.id.upload_audio).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("audio/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_DEFAULT);
                startActivityForResult(Intent.createChooser(intent, "GET FILE"), 5);


            }
        });

        rootView.findViewById(R.id.upload_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_DEFAULT);
                startActivityForResult(Intent.createChooser(intent, "GET FILE"), 5);*/
                UploadActivity.receiverAcitvity=2;
                Intent intent=new Intent(getActivity(),GalleryActivity.class);
                intent.putExtra("mediaType",1);
                startActivityForResult(intent,PICK_IMAGE);

            }
        });

        rootView.findViewById(R.id.upload_video).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent = new Intent();
                intent.setType("video/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_DEFAULT);
                startActivityForResult(Intent.createChooser(intent, "GET FILE"), 6);*/
                UploadActivity.receiverAcitvity=2;
                Intent intent=new Intent(getActivity(),GalleryActivity.class);
                intent.putExtra("mediaType",3);
                startActivityForResult(intent,PICK_IMAGE);

            }
        });


        rootView.findViewById(R.id.upload_gif).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  Intent intent = new Intent();
                intent.setType("image/gif");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                startActivityForResult(Intent.createChooser(intent, "GET FILE"), 7);*/
                UploadActivity.receiverAcitvity=2;
                Intent intent=new Intent(getActivity(),GalleryActivity.class);
                intent.putExtra("mediaType",5);
                startActivityForResult(intent,PICK_IMAGE);
            }
        });


        rootView.findViewById(R.id.upload_text).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                UploadActivity.receiverAcitvity=2;
                Intent uploadIntent = new Intent(getActivity().getApplicationContext(), UploadActivity.class);
                //uploadIntent.putExtra("numberType", 4);
                //uploadIntent.putExtra("mediaarray", );
                startActivityForResult(uploadIntent, 15);
            }
        });


        swipeRefreshFeedList = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshFeedList);
        swipeRefreshFeedList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                updateContent();
                swipeRefreshFeedList.setRefreshing(false);
            }
        });


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    && getActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
               requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 10);

                return rootView;
            }
        }
        updateContent();
        IntentFilter intentFilter = new IntentFilter("FEED_UPDATED");
        getActivity().registerReceiver(feedReciever, intentFilter);

        return rootView;
    }

    public void sortFeeds()
    {
        AroundAlertBoxManager.showFeedSortDialog(getActivity(),null);
    }
    public void filterFeeds()
    {
        AroundAlertBoxManager.showFeedFilterDialog(getActivity(), new AroundAlertBoxManager.OnClickListener() {
            @Override
            public void onClick(int expirationTime) {
                if(expirationTime==1)
                {
                    Intent new_intent = new Intent();
                    new_intent.setAction("FEED_UPDATED");
                    getActivity().sendBroadcast(new_intent);
                }
                else;
            }
        });
    }
    /*@Override
    public void onStart() {
        super.onStart();

        // updateContent();
    }*/

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().unregisterReceiver(feedReciever);
//        geoQuery.removeAllListeners();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 10:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //locationok();
                    updateContent();
                }
                break;
        }
    }


    void updateContent() {
        Log.e("GGG","total active starTINGGGG ");
        Intent intent = new Intent("GET_FEED_UPDATED");
        intent.putExtra("lat", 0+"");
        intent.putExtra("lng", 0+"");
        getActivity().sendBroadcast(intent);
/*

        ArrayList<Location> source=new ArrayList<>();

        SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();

        Cursor cursor1 = db.rawQuery(" select * from " + DatabaseAccesser.location_table + " where " +
                DatabaseAccesser.loc_col_status + " = ? ", new String[]{"1"});
        if(cursor1.getCount()<1)
        {
            cursor1.close();
            return;
        }
        else
        {
            while (cursor1.moveToNext()) {

                Location temp=new Location("source");
                temp.setLatitude(cursor1.getDouble(
                        cursor1.getColumnIndexOrThrow(DatabaseAccesser.loc_col_latitude)));
                temp.setLongitude(cursor1.getDouble(
                        cursor1.getColumnIndexOrThrow(DatabaseAccesser.loc_col_longitude)));

                source.add(temp);


            }
        }
        cursor1.close();

        locationText.setText(source.size()+" locations");

        fillDataFromDatabase();

*/


    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.tabbed_feed_window, menu);
    }


    public void grabImage(View v) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, "GET FILE"), 5);
       /* CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);

                */

    }

    public void grabVideo(View v) {
        Intent intent = new Intent();
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, "GET FILE"), 6);

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {


        int position = -1;
        String selectedUser = "null";
        String usersName = "null";
        position = Adapter_FeedData.optionPosition;
        selectedUser = adap.data.get(position).userid;
        //Toast.makeText(getActivity(), selectedUser, Toast.LENGTH_SHORT).show();
        usersName = adap.data.get(position).username;
        //adap.data.get(optionPosition)
       /* try {

        } catch (Exception e) {
            //Log.d(TAG, e.getLocalizedMessage(), e);

        }*/
        switch (item.getItemId()) {
            case R.id.view_profile:
                Intent indi = new Intent(getActivity().getApplicationContext(), Profile_Activity.class);
                indi.putExtra("userid", selectedUser);
                startActivity(indi);
                break;
            case R.id.comment_now:
                // do your stuff
                Intent indi2 = new Intent(getActivity().getApplicationContext(), ChatWindow.class);
                indi2.putExtra("userid", selectedUser);
                indi2.putExtra("username", usersName);
                startActivity(indi2);
                break;
            case R.id.delete_post:
                // Toast.makeText(getActivity().getApplicationContext(), "ffofofof", Toast.LENGTH_SHORT).show();
                long timestamp = adap.data.get(position).timestamp;
                deleteFeedPosts(selectedUser, timestamp);
                // do your stuff
                break;
            case R.id.report_post:
                // do your stuff
                break;
            case R.id.block_post:
                // do your stuff
                break;
        }
        //Toast.makeText(getActivity().getApplicationContext(), ""+position, Toast.LENGTH_SHORT).show();


        return super.onContextItemSelected(item);
    }

    void deleteFeedPosts(final String userId, final long timestamp) {

        // Context tempContext=getActivity().getApplicationContext();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Delete feed?");
        // builder.setTitle("Delete feed?");
        builder.setCancelable(true);
        builder.setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        if (userId.equals(TabbedActivity.userdetail.getUid())) {
            builder.setPositiveButton("DELETE FEED", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    deleteFeedLocal(userId, timestamp);
                    dialog.dismiss();
                }
            });
        }
        builder.setNegativeButton("DELETE FROM PHONE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteFeedLocal(userId, timestamp);

                dialog.dismiss();
               /* processcompleted(false);
                Toast.makeText(Process_Activity.this, "Press Back Again To Exit", Toast.LENGTH_SHORT).show();*/

            }
        });


        AlertDialog alert = builder.create();
        //alert.getButton(2).setEnabled(false);
        alert.show();


    }

    void deleteFeedLocal(String userId, long timestamp) {
        SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();
        db.delete(DatabaseAccesser.feed_table, DatabaseAccesser.feed_col_user_id + "=?",
                new String[]{userId});
        //String query = "DELETE FROM " + DatabaseAccesser.feed_table + " WHERE " + DatabaseAccesser.feed_col_user_id + " = ? ";
        //Cursor cursor = db.rawQuery(query, new String[]{userId});
        //finaldata.remove(Adapter_FeedData.optionPosition);
        //adap.notifyItemChanged(Adapter_FeedData.optionPosition);
        fillDataFromDatabase();
        adap.notifyDataSetChanged();
        //Toast.makeText(getActivity(), "cursor amount "+ cursor.getCount(), Toast.LENGTH_SHORT).show();

    }

    public void grabGif(View v) {
        Intent intent = new Intent();
        intent.setType("image/gif");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, "GET FILE"), 7);

    }

    private String getRealPathFromURI(Context c, Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(c, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }

    public static final int PICK_IMAGE      = 21;
    public static final int NEW_TEXT      = 27;
    public static final int PICK_VIDEO     = 28;
    public static final int PICK_GIF          = 29;
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 5 && resultCode == RESULT_OK) {

            ArrayList<MediaData> finalSelection=new ArrayList<>();
            MediaData xx=new MediaData();
            xx.type=2;
            xx.uri=data.getData().toString();
            finalSelection.add(xx);
            UploadActivity.receiverAcitvity=2;
            Intent uploadIntent = new Intent(getActivity(), UploadActivity.class);
            //uploadIntent.putExtra("numberType", 6);
            //uploadIntent.putExtra("uri", url.uri);
            uploadIntent.putExtra("mediaarray", finalSelection);
            //uploadIntent.putExtra("forChat", 1);


            uploadIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(uploadIntent);


            /*Uri directory = data.getData();

            Intent uploadIntent = new Intent(getActivity().getApplicationContext(), UploadActivity.class);
            uploadIntent.putExtra("numberType", 5);
            uploadIntent.putExtra("uri", directory.toString());
            startActivityForResult(uploadIntent, 15);*/


        }
        if (requestCode == 6 && resultCode == RESULT_OK) {

            //Toast.makeText(this, "Working", Toast.LENGTH_SHORT).show();

            final Uri directory = data.getData();
            //Bitmap bmp = BitmapFactory.decodeFile(directory.getEncodedPath());

            Intent cropIntent = new Intent(getActivity().getApplicationContext(), UploadActivity.class);
            cropIntent.putExtra("numberType", 6);
            cropIntent.putExtra("uri", directory.toString());
            startActivityForResult(cropIntent, 15);
        }
        if (requestCode == 7 && resultCode == RESULT_OK) {

            //Toast.makeText(this, "Working", Toast.LENGTH_SHORT).show();

            final Uri directory = data.getData();

            Intent cropIntent = new Intent(getActivity().getApplicationContext(), UploadActivity.class);
            cropIntent.putExtra("numberType", 7);
            cropIntent.putExtra("uri", directory.toString());
            startActivityForResult(cropIntent, 1);
        }


        if (requestCode == 15 && resultCode == RESULT_OK) {
            // Toast.makeText(getActivity().getApplicationContext(), "Got Data", Toast.LENGTH_SHORT).show();

            String feed_text = data.getStringExtra("text");
            final String uri = data.getStringExtra("uri");
            String place = data.getStringExtra("locname");

            int type = data.getIntExtra("numberType", 0);
            int visi = data.getIntExtra("visibility", 4);
            long timestamp = System.currentTimeMillis();



            }


    }
public void quickFeedSent(MediaData dat)
{
    FeedSender.sentFeed(getActivity(), dat, ChatWindow.returnSingleUserDetails(userdetail.getUid()), new FeedSentListner() {
        @Override
        public void onFeedAdded(FeedData data) {
            Log.e("MGG","feed added");
        }

        @Override
        public void onFeedUploaded(FeedData data) {
            Log.e("MGG","feed uploaded");
        }
    });
}




    public static Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }



    private double meterDistanceBetweenPoints(float lat_a, float lng_a, float lat_b, float lng_b) {
        float pk = (float) (180.f / Math.PI);

        float a1 = lat_a / pk;
        float a2 = lng_a / pk;
        float b1 = lat_b / pk;
        float b2 = lng_b / pk;

        double t1 = Math.cos(a1) * Math.cos(a2) * Math.cos(b1) * Math.cos(b2);
        double t2 = Math.cos(a1) * Math.sin(a2) * Math.cos(b1) * Math.sin(b2);
        double t3 = Math.sin(a1) * Math.sin(b1);
        double tt = Math.acos(t1 + t2 + t3);

        return 6366000 * tt;
    }

    /*private double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return (dist);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }*/

    boolean checkUserInList(ArrayList<UserWholeData> source,String ID)
    {
        for(int i=0;i<source.size();i++)
        {
            if(source.get(i).user_id.equals(ID))
                return true;
        }
           return false;
    }
boolean checkDistanceToAllLocation(ArrayList<Location> source,Location destination)
{
    for(int l=0;l<source.size();l++)
    {
        if((source.get(l).distanceTo(destination) / 1000)<=AROUND_FEED_RADIUS)
            return true;
    }
    return false;
}



    void fillDataFromDatabase() {

        getFeedsFromDatabase();
        if(finaldata.size()==0)
        {
            noFeedsAvailable.setVisibility(View.VISIBLE);
        }
        else
            noFeedsAvailable.setVisibility(View.GONE);

    }

    void setTabPendingText(int count)
    {
        if (tab_pendingTexts[1] != null)
        {
            if (count == 0)
                tab_pendingTexts[1].setVisibility(View.GONE);
            else {
                tab_pendingTexts[1].setVisibility(View.VISIBLE);
                tab_pendingTexts[1].setText(count + "");
            }
        }
    }
    void getFeedsFromDatabase() {
        finaldata.clear();
        finaldatalist.clear();
        SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();
        //DatabaseReference ref = FirebaseDatabase.getInstance().getReference("feeds_location");
        //geoFire = new GeoFire(ref);
       /*Cursor cursor = db.query(DatabaseAccesser.feed_table, new String[]{DatabaseAccesser.feed_col_user_id,
                        DatabaseAccesser.feed_col_username,
                        DatabaseAccesser.feed_col_timestamp, DatabaseAccesser.feed_col_type, DatabaseAccesser.feed_col_text,
                        DatabaseAccesser.feed_col_semi_url, DatabaseAccesser.feed_col_url},
                null,
                null,
                null, null, null,null);*/

        Double lat = Double.parseDouble(prefs.getString("location_latitude", "0.0"));
        Double lng = Double.parseDouble(prefs.getString("location_longitude", "0.0"));

        SharedPreferences pref= PreferenceManager.getDefaultSharedPreferences(getActivity());
        Set<String> selections =pref.getStringSet("filter_feed",null);
        if(selections==null)
        {selections = new HashSet<String>();
        selections.add(MainServiceThread.PEOPLE_NEAR_FILTER+"");
            selections.add(MainServiceThread.FOLLOWING_PEOPLE_FILTER+"");
            selections.add(MainServiceThread.SELECTED_LOCATION_FILTER+"");
        }

        if(!selections.contains(MainServiceThread.PEOPLE_NEAR_FILTER+""))
            lat=lng=0.0;


        ArrayList<Location> source=new ArrayList<>();

        if(lat!=0 &&lng!=0){
        Location tempX=new Location("source");
        tempX.setLatitude(lat);
        tempX.setLongitude(lng);
        source.add(tempX);}

        ArrayList<UserWholeData> followingUserList=null;
        if(selections.contains(MainServiceThread.FOLLOWING_PEOPLE_FILTER+""))
        followingUserList= UserManager.getUsersList(getActivity(),true);

        if(selections.contains(MainServiceThread.SELECTED_LOCATION_FILTER+"")) {
            Cursor cursor1 = db.rawQuery(" SELECT * FROM " + DatabaseAccesser.location_table + " WHERE " +
                    DatabaseAccesser.loc_col_status + " = ? ", new String[]{"1"});


                while (cursor1.moveToNext()) {

                    Location temp = new Location("source");
                    temp.setLatitude(cursor1.getDouble(
                            cursor1.getColumnIndexOrThrow(DatabaseAccesser.loc_col_latitude)));
                    temp.setLongitude(cursor1.getDouble(
                            cursor1.getColumnIndexOrThrow(DatabaseAccesser.loc_col_longitude)));

                    source.add(temp);
                }
            cursor1.close();
        }

      //  LatLngBounds

        //double cc=Location.distanceBetween((double)source.getLatitude(),(double)source.getLongitude(),(double)destination.getLatitude(),(double)destination.getLongitude());
        //double b=meterDistanceBetweenPoints((float)source.getLatitude(),(float)source.getLongitude(),(float)destination.getLatitude(),(float)destination.getLongitude());

        //Toast.makeText(getActivity(), "dist: "+a+" / "+b+" / ", Toast.LENGTH_LONG).show();

        Cursor cursor;

       cursor=db.rawQuery("select CAST(count(*) AS INTEGER) AS "+DatabaseAccesser.count_value+","+DatabaseAccesser.feed_table+".rowid AS rowID"+",* FROM "+DatabaseAccesser.feed_table+" INNER JOIN "+DatabaseAccesser.media_table+" ON "+
               DatabaseAccesser.feed_table+"."+DatabaseAccesser.feed_col_media_id+" = "+ DatabaseAccesser.media_table+".rowid"+" group by "+DatabaseAccesser.feed_col_user_id+","+DatabaseAccesser.feed_col_seen + " order by "+DatabaseAccesser.feed_col_timestamp,null);

        ArrayList<String> arrString=new ArrayList<String>();
        ArrayList<String> arrInt=new ArrayList<String>();
        //Log.e("GGG","total feed size "+ cursor.getCount());
        int unSeenCount = 0;
        while (cursor.moveToNext()) {
            GroupFeedData temp = new GroupFeedData();
            FeedDataList temp2 = new FeedDataList();

            temp.feedRow =cursor.getLong(
                    cursor.getColumnIndexOrThrow("rowID"));
            temp.sl = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_sl_no));
            temp.mediaSize= cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.media_size));
            temp.userid = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_user_id));
            temp.username = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_username));
            temp.timestamp = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_timestamp));
            temp.type = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_type));

            temp.ifseen = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_seen));

            if(temp.ifseen==0)
                unSeenCount++;

            temp.text = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_text));

            long type = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_media_id));
            if(type==1)
            {
                temp.semi_uri=temp.uri="null";
            }
            else
            {

                    temp.uri = cursor.getString(
                            cursor.getColumnIndexOrThrow(DatabaseAccesser.media_local_uri));
                    if(temp.uri.equals("null"))
                    {
                        temp.uri = cursor.getString(
                                cursor.getColumnIndexOrThrow(DatabaseAccesser.media_global_uri));
                    }
                    temp.semi_uri = cursor.getString(
                            cursor.getColumnIndexOrThrow(DatabaseAccesser.media_lite_local_uri));

            }

            //Log.e("MSGG","FIRST: "+ temp.uri +" value : "+temp.text );

            double latitude = cursor.getDouble(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_latitude));
            double longitude = cursor.getDouble(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.feed_col_longitude));

            int count = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.count_value));


            Location destination = new Location("destination");
            destination.setLatitude(latitude);
            destination.setLongitude(longitude);


            //float distance = ;

//yyy

            temp2.seen = 0;
            temp2.unseen = 0;

            if ((selections.contains(MainServiceThread.SELECTED_LOCATION_FILTER+"")?checkDistanceToAllLocation(source,destination):false) ||
                    (selections.contains(MainServiceThread.FOLLOWING_PEOPLE_FILTER+"")?checkUserInList(followingUserList,temp.userid):false)) {

            if (temp.ifseen == 0) {
                if (arrString.contains(temp.userid)) {
                    int pos = arrString.indexOf(temp.userid);
                    int c = Integer.parseInt(arrInt.get(pos));
                    int x = finaldatalist.get(c).seen;
                    temp2.seen = x;
                    finaldatalist.get(c).seen = 0;
                    finaldatalist.get(c).unseen = 0;
                    arrString.remove(pos);
                    arrInt.remove(pos);
                    //finaldata.remove();
                    //get final list data
                    //finaldatalist.remove();
                    temp2.unseen = count;
                    finaldata.add(temp);
                    finaldatalist.add(temp2);
                } else {
                    temp2.unseen = count;
                    finaldata.add(temp);
                    finaldatalist.add(temp2);
                    arrString.add(temp.userid);
                    arrInt.add((finaldatalist.indexOf(temp2) + ""));
                }
            } else {
                if (arrString.contains(temp.userid)) {
                    int c = Integer.parseInt(arrInt.get(arrString.indexOf(temp.userid)));
                    finaldatalist.get(c).seen = count;
                    //update final list data
                } else {
                    temp2.seen = count;
                    finaldata.add(temp);
                    finaldatalist.add(temp2);
                    arrString.add(temp.userid);
                    arrInt.add((finaldatalist.indexOf(temp2) + ""));
                }
            }

        }


        }

        setTabPendingText(unSeenCount);
        //Toast.makeText(getActivity(), finaldata.size()+"/"+finaldatalist.size(), Toast.LENGTH_SHORT).show();
        for(int i=finaldatalist.size()-1;i>=0;i--)
        {
            if(finaldatalist.get(i).unseen==0&&finaldatalist.get(i).seen==0)
            {
                finaldatalist.remove(i);
                finaldata.remove(i);
            }
        }
        cursor.close();

        adap.notifyDataSetChanged();



    }


}


