package com.hash.around.TabFragments;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;


import com.github.clans.fab.FloatingActionButton;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.hash.around.AllUsersList;
import com.hash.around.MainServiceThread;
import com.hash.around.OtherClasses.Adapter_ChatList;
import com.hash.around.OtherClasses.DataTypes.ChatListData;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.OtherClasses.DataTypes.GroupChatData;
import com.hash.around.PeopleNearYou;
import com.hash.around.R;
import com.hash.around.TabbedActivity;


import java.util.ArrayList;

import static com.hash.around.TabbedActivity.fDatabase;
import static com.hash.around.TabbedActivity.tab_pendingTexts;
import static com.hash.around.TabbedActivity.userdetail;

public class TabFragment1 extends Fragment {

    RecyclerView rView;
    CardView cView;
    public Adapter_ChatList adap;
    private LinearLayoutManager mLayoutManager;
    FloatingActionButton floatButtonNewChat;
    ChildEventListener childEventListener;
    DatabaseReference newPostRef;
    ArrayList<ChatListData> finaldata=new ArrayList<ChatListData>();
    FloatingActionButton chatPeopleNearby;
public static boolean showHiddenChats;
    private BroadcastReceiver chatReciever = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            //Toast.makeText(getApplicationContext(), "received message in activity..!", Toast.LENGTH_SHORT).show();
            getDataFromDatabase();
            adap.notifyDataSetChanged();
        }
    };

    public ArrayList<ChatListData> generateDummy()
    {
        ArrayList<ChatListData> list=new ArrayList<ChatListData>();

        /*ChatListData x=new ChatListData();
        x.id=1;
        x.userid=1;
        x.text="Check out this game 99 Miles Runner!! When I first played it, I felt it like a grumpy game, Now it is what I love the most <3 <3 <3";
        x.imgDir="danial";
        list.add(x);


        x=new ChatListData();
        x.id=2;
        x.userid=2;
        x.text="Time on marine drive!!!....fun with besties #fun #around_mood #gamersconnect :)";
        x.imgDir="profile";
        list.add(x);

        x=new ChatListData();
        x.id=3;
        x.userid=1;
        x.text="Kid playing Blue whale on the PC !!!";
        x.imgDir="blue";
        list.add(x);

        x=new ChatListData();
        x.id=4;
        x.userid=2;
        x.text="Accedent in the town.. Any one with O+ve blood? HURRY !";
        x.imgDir="acce";
        list.add(x);*/

        return list;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //newPostRef.removeEventListener(childEventListener);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //update_content();

    }
    LinearLayout noFeedsAvailable;
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.chat_list_fragment, container, false);
        //TextView textView = (TextView) rootView.findViewById(R.id.section_label);
        //textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
        rView=(RecyclerView)rootView.findViewById(R.id.rView);

        noFeedsAvailable=(LinearLayout) rootView.findViewById(R.id.noFeedsAvailable);


        rView.setHasFixedSize(true);
        //cView=(CardView)rootView.findViewById(R.id.cView);
        mLayoutManager=new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,true);
        //  mLayoutManager=new GridLayoutManager(this,2,LinearLayoutManager.HORIZONTAL,false);
        // mLayoutManager=new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);
        mLayoutManager.setStackFromEnd(true);
        rView.setLayoutManager(mLayoutManager);

        //ArrayList<ChatListData> finaldata=generateDummy();
        getDataFromDatabase();

        adap=new Adapter_ChatList(getActivity(),finaldata,getActivity());
        rView.setAdapter(adap);



        floatButtonNewChat=(FloatingActionButton)rootView.findViewById(R.id.floatButtonNewChat);
        floatButtonNewChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mapIntent=new Intent(getActivity().getApplicationContext(),AllUsersList.class);
                //  uploadIntent.putExtra("numberType",5);
                //uploadIntent.putExtra("uri",directory.toString());
                startActivity(mapIntent);

                /*ChromePopUpHelper chromeHelpPopup = new ChromePopUpHelper(getActivity(),"Hello!");
                chromeHelpPopup.show(v);*/


            }
        });


       /****************IMPORTANT CODE */////////////

/*rView.addOnItemTouchListener(new RecyclerViewTouchListner(getActivity(), rView, new RecyclerViewTouchListner.RecyclerClick_Listener()
        {
            @Override
            public void onClick(View view, int position) {
                //Toast.makeText(getActivity(), position+" ", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));*/


        //setHasOptionsMenu(true);

        IntentFilter intentFilter = new IntentFilter("CHAT_UPDATED");
//Map the intent filter to the receiver
        getActivity().registerReceiver(chatReciever, intentFilter);


        final LayoutInflater factory = getActivity().getLayoutInflater();

        final View textEntryView = factory.inflate(R.layout.people_near_activity, null);

        //landmarkEditNameView = (EditText) textEntryView.findViewById(R.id.landmark_name_dialog_edit);

       //View slideView=textEntryView.findViewById(R.id.slideView);
/*if(slideView==null)
    Toast.makeText(getActivity(), "doomed", Toast.LENGTH_SHORT).show();*/
       /*slideUp = new SlideUpBuilder(slideView)
               .withStartState(SlideUp.State.HIDDEN)
               .withStartGravity(Gravity.BOTTOM)

               //.withSlideFromOtherView(anotherView)
               //.withGesturesEnabled()
               //.withHideSoftInputWhenDisplayed()
               //.withInterpolator()
               //.withAutoSlideDuration()
               //.withLoggingEnabled()
               //.withTouchableAreaPx()
               //.withTouchableAreaDp()
               //.withListeners()
               //.withSavedState()
               .build();
        slideUp.hideImmediately();*/

        chatPeopleNearby=(FloatingActionButton)rootView.findViewById(R.id.chatPeopleNearby);
        chatPeopleNearby.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().overridePendingTransition(R.anim.slide_up_anim,R.anim.no_change_anim);

                Intent allUsers=new Intent(getActivity(),PeopleNearYou.class);
                //  uploadIntent.putExtra("numberType",5);
                //uploadIntent.putExtra("uri",directory.toString());

                startActivity(allUsers);



            }
        });

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.all_user_activity, menu);
       // adap.mActionMode.finish();
    }


    public void updateActionMode() {
       /* if(adap.mActionMode !=null)
        adap.mActionMode.finish();*/
    }








    void update_content()
    {
        newPostRef=fDatabase.child("chat_data").child(userdetail.getUid());
        final Context p=getActivity();
        // if(childEventListener==null)


        if(childEventListener==null) {

            childEventListener = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                    GroupChatData post = dataSnapshot.getValue(GroupChatData.class);
                    post.sender = 1;
                    //finaldata.add(post);
                    dataSnapshot.getRef().removeValue();
                    MainServiceThread.mainService.saveChatToDatabase(post, null);
                    //Toast.makeText(getActivity(), post.msg, Toast.LENGTH_SHORT).show();
                    //adap.notifyItemInserted(finaldata.size()-1);
                    //chatView.scrollToPosition(finaldata.size() - 1);

               /* SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();

                Cursor cursor = db.rawQuery(" select " + DatabaseAccesser.user_user_id + " from " + DatabaseAccesser.user_table + " where " +
                        DatabaseAccesser.user_user_id + " = ? ", new String[]{post.user_id + ""});*/

                /*if (cursor.getCount() == 0) {

                    StorageReference httpsReference = FirebaseStorage.getInstance().getReferenceFromUrl(post.img_url);
                    httpsReference.getBytes(1024 * 1024).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                        @Override
                        public void onSuccess(byte[] bytes) {
                            // Data for "images/island.jpg" is returns, use this as needed

                            //String output = ;

                            try {

                                File file = new File(Environment.getExternalStorageDirectory().getPath(), "Around Files/Profile Pictures");
                                if (!file.exists()) {
                                    file.mkdirs();
                                }
                                //File tempFile = new File(file.getAbsolutePath(),"contact.vcf");

                                String name = post.user_name + ".png";
                                FileOutputStream fos = new FileOutputStream(new File(file.getAbsolutePath(), name));
                                fos.write(bytes);
                                fos.close();

                                //Toast.makeText(getActivity(),  , Toast.LENGTH_SHORT).show();

                                post.img_url = file.toURI().toString() + name;
                                finaldata.add(0,post);
                                adap.notifyItemInserted(0);

                                // adap.notifyItemChanged(finaldata.size() - 1);
                                // rView.scrollToPosition(finaldata.size() - 1);

                                savePostToDatabase(post);


                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            // Handle any errors
                        }
                    });
                }*/

                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            };


//                      Toast.makeText(getActivity(), "hurray", Toast.LENGTH_SHORT).show();
            // ...
            //newPostRef.goOffline();
            //newPostRef.removeEventListener(childEventListener);


            newPostRef.addChildEventListener(childEventListener);

        }

    }

   /* void saveChatToDatabase(GroupChatData post, Context p)
    {
        SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();

        //  Toast.makeText(getActivity(), "Now saving to database", Toast.LENGTH_SHORT).show();
        ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.chat_userid, post.userid);
        values.put(DatabaseAccesser.chat_sender, post.sender);
        values.put(DatabaseAccesser.chat_type, post.numberType);
        values.put(DatabaseAccesser.chat_timestamp, post.timestamp);
        values.put(DatabaseAccesser.chat_text, post.chatMsg);
        values.put(DatabaseAccesser.chat_temp_url, post.temp_url);
        values.put(DatabaseAccesser.chat_final_url, post.final_url);


        final long newRowId = db.insert(DatabaseAccesser.chat_table, null, values);

        ContentValues values2 = new ContentValues();

        values2.put(DatabaseAccesser.chatList_lastmsg, post.chatMsg);
        values2.put(DatabaseAccesser.chatList_lastmsgRowId, newRowId);
        values2.put(DatabaseAccesser.chatList_lastmsg_type, post.numberType);
        values2.put(DatabaseAccesser.chatList_msgqueue, 1);
        values2.put(DatabaseAccesser.chatList_timestamp, System.currentTimeMillis());

        if(ChatWindow.uId!=null)
        {
        if(ChatWindow.uId.equals(post.userid))
        {

            ChatWindow.finaldata.add(post);
            ChatWindow.adap.notifyItemInserted(ChatWindow.finaldata.size()-1);
            ChatWindow.chatView.scrollToPosition(ChatWindow.finaldata.size() - 1);
            //Toast.makeText(p, "Iteminserted", Toast.LENGTH_SHORT).show();

        }
        }



        int rowln=db.update(DatabaseAccesser.chatList_table,values2, DatabaseAccesser.chatList_userid+" =?",new String[]{post.userid});


       // Toast.makeText(getActivity(), ""+rowln, Toast.LENGTH_SHORT).show();
        if(rowln>=1)
        {


            LocalNotificationManager mNotificationManager =
                    (LocalNotificationManager) p.getSystemService(Context.NOTIFICATION_SERVICE);

            Intent intenttest = new Intent(p, TabbedActivity.class);
            PendingIntent pIntent = PendingIntent.getActivity(p, (int) System.currentTimeMillis(), intenttest, 0);

            Notification.InboxStyle inboxStyle = new Notification.InboxStyle();
            inboxStyle.setBigContentTitle("Messages:");
            inboxStyle.setSummaryText(rowln+ " New chats");
            inboxStyle.addLine(post.chatMsg);



            Notification n  =  new Notification.Builder(p)
                    .setContentTitle("SpotIt")
                    .setContentText(rowln+ " New chats")
                    .setSmallIcon(R.drawable.nick)
                    .setContentIntent(pIntent)
                    .setStyle(inboxStyle)
                    .setPriority(Notification.PRIORITY_MAX)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true).build();

            mNotificationManager.notify(0,n);

            getDataFromDatabase();
            adap.notifyDataSetChanged();
        }
        else if(rowln<1)
        {
            final String chatMsg=post.chatMsg;
            final String UserId=post.userid;
            final String postType=post.userid;

            FirebaseDatabase.getInstance().getReference().child("user_profile").child(post.userid).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {


                    String name=dataSnapshot.child("user_name").getValue().toString();
                    String url=dataSnapshot.child("img_url").getValue().toString();
                    long currTimeStamp=System.currentTimeMillis() / 1000L;
                    ContentValues values3 = new ContentValues();
                    values3.put(DatabaseAccesser.chatList_userid, UserId);
                    values3.put(DatabaseAccesser.chatList_name, name);
                    values3.put(DatabaseAccesser.chatList_lastmsg, chatMsg);
                    values3.put(DatabaseAccesser.chatList_lastmsgRowId, newRowId);
                    values3.put(DatabaseAccesser.chatList_lastmsg_type, postType);
                    values3.put(DatabaseAccesser.chatList_timestamp,currTimeStamp);
                    values3.put(DatabaseAccesser.chatList_msgqueue, 1);
                    values3.put(DatabaseAccesser.chatList_url, url);

                    SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();
                    long rowln=db.insert(DatabaseAccesser.chatList_table, null, values3);



                    //finaldata=null;
                    //finaldata.clear();
                    sdsd
                    getDataFromDatabase();
                    adap.notifyDataSetChanged();

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });




        }



//        Toast.makeText(getActivity(),newRowId+ " row inserter", Toast.LENGTH_SHORT).show();

    }*/


    public void getDataFromDatabase() {
        finaldata.clear();
        SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();

        /*Cursor cursor = db.query(DatabaseAccesser.chatList_table, new String[]{DatabaseAccesser.chatList_userid,
                        DatabaseAccesser.chatList_name,
                        DatabaseAccesser.chatList_lastmsgbyme,
                        DatabaseAccesser.chatList_lastmsg,
                        DatabaseAccesser.chatList_last_msgseen,
                        DatabaseAccesser.chatList_lastmsgRowId,
                        DatabaseAccesser.chatList_lastmsg_type,
                        DatabaseAccesser.chatList_timestamp,
                        DatabaseAccesser.chatList_msgqueue,
                        DatabaseAccesser.chatList_url},
                null,
                null,
                null, null, DatabaseAccesser.chatList_timestamp+"",null);*/


        Cursor cursor=null;
        if(showHiddenChats==false)
        {
            //Log.e("MSGG","SHow public ");
            cursor = db.rawQuery(
                    "SELECT * FROM " + DatabaseAccesser.chatList_table + " INNER JOIN " + DatabaseAccesser.user_table + " ON " +
                            DatabaseAccesser.chatList_table + "." + DatabaseAccesser.chatList_userid + " = " + DatabaseAccesser.user_table + "." + DatabaseAccesser.user_user_id +
                           " WHERE "+DatabaseAccesser.chatList_table + "."+DatabaseAccesser.chatList_chat_hidden +" =? "+ " ORDER BY " + DatabaseAccesser.chatList_table + "." + DatabaseAccesser.chatList_timestamp,
                    new String[]{""+0});
        }
        else
        {
            Log.e("MSGG","SHow hidden ");
            cursor = db.rawQuery(
                    "SELECT * FROM " + DatabaseAccesser.chatList_table + " INNER JOIN " + DatabaseAccesser.user_table + " ON " +
                            DatabaseAccesser.chatList_table + "." + DatabaseAccesser.chatList_userid + " = " + DatabaseAccesser.user_table + "." + DatabaseAccesser.user_user_id +
                            " ORDER BY " + DatabaseAccesser.chatList_table + "." + DatabaseAccesser.chatList_timestamp,
                    null);
        }




        int count = 0;
        while (cursor.moveToNext()) {


            ChatListData temp = new ChatListData();

            temp.userid = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chatList_userid));
            temp.name = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chatList_name));

            temp.msgFromMe = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chatList_lastmsgbyme));

            temp.lastMessage = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chatList_lastmsg));
            temp.lastMessagetype = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chatList_lastmsg_type));
            temp.lastMessageSeen = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chatList_last_msgseen));
            temp.rowId = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chatList_lastmsgRowId));
            //Toast.makeText(getActivity(), "post_type "+post.numberType, Toast.LENGTH_SHORT).show();

            Toast.makeText(getActivity(), "last msGG " + temp.lastMessagetype, Toast.LENGTH_SHORT).show();
            temp.timestamp = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chatList_timestamp));

            //Log.e("MSGG","rtetrivrd : "+temp.timestamp);
            temp.msgQueue = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chatList_msgqueue));
            count += temp.msgQueue;
            temp.url = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_localUrl));

            temp.type = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.user_user_type));

            temp.chat_pinned = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chatList_chat_pinned));
            temp.chat_hidden = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.chatList_chat_hidden));

            //Toast.makeText(getActivity(), temp.uri, Toast.LENGTH_SHORT).show();
            // long itemId =
            finaldata.add(temp);
        }
        cursor.close();


        if (tab_pendingTexts[0] != null)
        {
            if (count == 0)
                tab_pendingTexts[0].setVisibility(View.GONE);
            else {
                tab_pendingTexts[0].setVisibility(View.VISIBLE);
                tab_pendingTexts[0].setText(count + "");
            }
    }
        if(finaldata.size()==0)
        {
            noFeedsAvailable.setVisibility(View.VISIBLE);
        }
        else
        {
            noFeedsAvailable.setVisibility(View.GONE);
        }

    }
}


