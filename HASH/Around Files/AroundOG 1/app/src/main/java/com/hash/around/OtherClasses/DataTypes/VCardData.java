package com.hash.around.OtherClasses.DataTypes;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import com.hash.around.Utils.MediaManager;
import com.hash.around.Utils.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import static com.hash.around.TabFragments.TabFragment2.getResizedBitmap;

public class VCardData implements Serializable{

    public String name=null;
    public ArrayList<Integer> numberType =new ArrayList<>();
    public ArrayList<String> number=new ArrayList<>();
    public ArrayList<String> numberLabel=new ArrayList<>();
    public String photo=null;

   public VCardData()
   {}

    public void setPhoto(Bitmap bitmap)
    {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        final byte[] byteArray;
        getResizedBitmap(bitmap, 600).compress(Bitmap.CompressFormat.PNG, 1, bytes);
        byteArray=bytes.toByteArray();
        photo= Base64.encodeToString(byteArray,Base64.DEFAULT);
    }
    public String getPhotoString()
    {
        return photo;
    }
    public Bitmap getPhotoBitmap()
    {
        if(photo!=null){
            Log.e("MGG","contact message "+photo);
            byte[] bitmapdata=Base64.decode(photo,Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(bitmapdata, 0, bitmapdata.length);}
        else
        return null;
    }
    public byte[] getPhotoByte()
    {
        byte[] bitmapdata=null;

        if(photo!=null)
        bitmapdata = Base64.decode(photo, Base64.DEFAULT);
            return bitmapdata;
    }
    public void encodeVcard(String vCard)
    {
        name=vCard.split("FN:")[1].split("\n")[0];

        if(vCard.split("PHOTO;BASE64:").length>1)
        photo=vCard.split("PHOTO;BASE64:")[1].split("END")[0];

        for(int i=0;i<vCard.split("item1.TEL;waid=").length-1;i++)
        {
            String numberTemp=vCard.split("item1.TEL;waid=")[i+1].split(":")[1].split("\n")[0];
            String numberLabelTemp=vCard.split("item1.X-ABLabel:")[i+1].split("\n")[0];
            Log.e("MGG","contact number "+numberTemp+" / "+numberLabelTemp);
            number.add(numberTemp);
            numberLabel.add(numberLabelTemp);
        }
    }

    public static String savePhotoReturnUri(Bitmap bitmap)
    {

        if(bitmap==null)
            return null;

        //String localUri= MediaManager.saveByteIntoCacheAsFile(byteArray, StringUtils.randomStringGenerator() + ".png");
        return "";
       // return vCard.split("FN:")[1].split("\n")[0];
    }
   public static String getNameFromVcard(String vCard)
    {

        return vCard.split("FN:")[1].split("\n")[0];
    }
   }


