package com.hash.around.OtherClasses;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hash.around.ChatWindow;
import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.R;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;

import static com.hash.around.MainServiceThread.newUserFollowRequestWithDoubleCheck;
import static com.hash.around.MainServiceThread.updateUserFollowStatus;

//import android.support.v7.view.ActionMode;

/**
 * Created by Febin on 26-08-2017.
 */

public class Adapter_Contacts extends RecyclerView.Adapter<Adapter_Contacts.ViewHolder> {

    Context mContext;
    ArrayList<UserWholeData> data;


    public SparseBooleanArray selectedUsers;

     boolean slectionActivity=false;
    Snackbar snack;

   static  Drawable temp=null;
    public Adapter_Contacts(Context mcont, ArrayList<UserWholeData> list,boolean selection)
    {
        mContext=mcont;
        data=list;
        selectedUsers=new SparseBooleanArray();
        slectionActivity=selection;

    }

    public void updateDataList(ArrayList<UserWholeData> list)
    {
        data=list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        /*FUUUUUUULL CHANGE*/
        switch(viewType) {
            case 0:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.single_contact_view, parent, false);
                // set the view's size, margins, paddings and layout parameters

                ViewHolder vh = new ViewHolder(v);
                return vh;
            case 1:
             v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.single_contact_view, parent, false);
            // set the view's size, margins, paddings and layout parameters

           /* ViewHolder2 vh2 = new ViewHolder(v);
            return vh2;*/
        }
        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        final int pos = position;
        final RelativeLayout rView = holder.wholeView;
        final Drawable dr = holder.temp;

        if (selectedUsers.get(position))
        {
            rView.setBackgroundColor(Color.parseColor("#20000000"));
            holder.imageSelectionTick.setVisibility(View.VISIBLE);
        }
        else
        {
            rView.setBackground(dr);
            holder.imageSelectionTick.setVisibility(View.GONE);
        }



        holder.name.setText(data.get(position).user_name);

        if (checkIfPrivacyShouldBeActivie(data.get(pos).priv_profile_status,data.get(pos).remoteContact)) {
            String status = data.get(position).status;
            if (status.equals("null"))
                status = "Wanna know an App called Around?";

            holder.status.setText(status);
        }
        else
        {
            holder.status.setText("");
        }
        String dir=data.get(position).img_url;

        if (checkIfPrivacyShouldBeActivie(data.get(pos).priv_profile_picture,data.get(pos).remoteContact)) {
            if (dir.equals("null")) {
                holder.proimg.setImageResource(R.drawable.default_user);
            } else {
                holder.proimg.setImageURI(Uri.parse(dir));
            }
        }
        else
        {
            holder.proimg.setImageResource(R.drawable.default_user);
        }



        final SwitchCompat compatSwitch=holder.switchFollow;
        compatSwitch.setChecked(data.get(pos).followType==1);
        if(data.get(pos).followType==3)
            compatSwitch.setEnabled(false);
        compatSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.e("MSGG","changed called");
                if(isChecked)
                    newUserFollowRequestWithDoubleCheck(data.get(pos).user_id, new JobCompletionListner() {
                        @Override
                        public void onJobCompletion(String txt1) {
                            data.get(pos).followType=Integer.parseInt(txt1);
                            if(data.get(pos).followType==1)
                            compatSwitch.setChecked(true);
                            else
                                compatSwitch.setEnabled(false);
                            //notifyItemChanged(pos);

                        }
                    });
                else {
                    updateUserFollowStatus(data.get(pos).user_id, 0);
                    data.get(pos).followType=0;
                    compatSwitch.setChecked(false);
                }

            }
        });


        holder.wholeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(slectionActivity)
                {
                    if (selectedUsers.get(pos)) {
                        selectedUsers.delete(pos);
                        //rView.setBackground(dr);
                        notifyItemChanged(pos);
                    }
                    else {
                        selectedUsers.put(pos,true);
                        notifyItemChanged(pos);
                        //rView.setBackgroundColor(Color.parseColor("#20000000"));
                    }
                    //update snackbar
                    if(selectedUsers.size()>0)
                    {
                        String snackText="";
                        for (int i = selectedUsers.size()-1; i>=0; i--) {

                            if(i==selectedUsers.size()-1)
                                 snackText+=data.get(selectedUsers.keyAt(i)).user_name;
                            else
                                snackText+=", "+data.get(selectedUsers.keyAt(i)).user_name;
                        }

                        if(snack==null) {
                            snack = Snackbar.make(v, snackText, Snackbar.LENGTH_INDEFINITE);
                            snack.show();

                        }
                        else
                        {
                            snack.setText(snackText);
                        }
                    }
                    else {
                        snack.dismiss();
                        snack=null;
                        //remove snackbar
                    }
                }
                else {
                    Context cont = mContext;
                    Intent indi = new Intent(cont, ChatWindow.class);
                    indi.putExtra("userid", data.get(pos).user_id);
                    indi.putExtra("username", data.get(pos).user_name);
                    ((AppCompatActivity) mContext).finish();
                    cont.startActivity(indi);
                }
            }
        });

        ///if(data.get(position).) checkfor privacy  CODE-01
        checkSingleOnlineStatus(data.get(position).user_id,holder.userOnlineStatus);

    }

    public static boolean checkIfPrivacyShouldBeActivie(int privacy,int remoteContact)
    {
        if(privacy<2)
        {
            return true;
        }
        else if (privacy==2)
        {
            if(remoteContact==1)
            { Log.e("MGG","USER PRIVACY SET TO CONTACTS ONLY....SINCE THE USER IS REMOTE CONTACT");
                return true;}
            else {
                Log.e("MGG","USER PRIVACY SET TO CONTACTS ONLY..");
                return false;
            }

        }
        else
        {
            return false;
        }
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        // write code to remove listners to the online status CODE-02
    }

    void checkSingleOnlineStatus(String userId, final TextView updateText)
    {
        final long currentTimestamp=System.currentTimeMillis();
        FirebaseDatabase.getInstance().getReference().child("user_profile").child(userId).
                child("online_status").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String status=dataSnapshot.child("status").getValue().toString();
                long timeData=Long.parseLong(dataSnapshot.child("timestamp").getValue().toString());
                if(status.equals("online") && timeData+60000<currentTimestamp)
                {
                    updateText.setVisibility(View.VISIBLE);
                }
                else
                {
                    updateText.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }



    @Override
    public int getItemCount() {
        if(data!=null) {
            return data.size();
        }
        return 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name;
        RelativeLayout wholeView;

        public TextView status;
        public ImageView proimg;
        public Drawable temp=null;

        public CircularImageView imageSelectionTick;
        SwitchCompat switchFollow;

        TextView userOnlineStatus;
        public ViewHolder(View item)
        {
            super(item);
            name=(TextView)item.findViewById(R.id.contactName);
            status=(TextView)item.findViewById(R.id.contactStatus);
            proimg=(ImageView) item.findViewById(R.id.contactImg);

            imageSelectionTick=(CircularImageView) item.findViewById(R.id.imageSelectionTick);
            wholeView=(RelativeLayout)item.findViewById(R.id.singleContact);

            userOnlineStatus=(TextView)item.findViewById(R.id.userOnlineStatus);
            switchFollow=(SwitchCompat) item.findViewById(R.id. switchFollow);
            if(temp==null) {
                temp = wholeView.getBackground();
            }

        }
    }

}


