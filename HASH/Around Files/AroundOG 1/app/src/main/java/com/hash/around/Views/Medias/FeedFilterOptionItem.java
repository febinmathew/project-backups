package com.hash.around.Views.Medias;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.hash.around.R;


public class FeedFilterOptionItem extends LinearLayout {

   CheckBox checkBox;

   public FeedFilterOptionItem(Context context) {
        this(context, null);
    }

    public FeedFilterOptionItem(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FeedFilterOptionItem(final Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        initViews();
    }

    void initViews()
    {
        checkBox=findViewById(R.id.checkBoxItem);
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                checkBox.setChecked(!checkBox.isChecked());
            }
        });
    }

    public boolean getChecked()
    {
        return checkBox.isChecked();
    }
    public void setChecked(boolean check)
    {
        checkBox.setChecked(check);
    }
}
