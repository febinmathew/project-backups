package com.hash.around.TabFragments;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.github.clans.fab.FloatingActionButton;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DatabaseReference;
import com.hash.around.OtherClasses.Adapter_NoificationData;
import com.hash.around.OtherClasses.DataTypes.ChatListData;
import com.hash.around.OtherClasses.DatabaseAccesser;
import com.hash.around.OtherClasses.DataTypes.NotificationData;
import com.hash.around.R;
import com.hash.around.TabbedActivity;

import java.util.ArrayList;

public class NotificationList_Fragment extends Fragment {

    RecyclerView rView;
    CardView cView;

    private LinearLayoutManager mLayoutManager;
    FloatingActionButton floatButtonNewChat;
    ChildEventListener childEventListener;
    DatabaseReference newPostRef;
    ArrayList<NotificationData> finaldata=new ArrayList<NotificationData>();
    FloatingActionButton chatPeopleNearby;

    private BroadcastReceiver notificationReciever = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            //Toast.makeText(getApplicationContext(), "received message in activity..!", Toast.LENGTH_SHORT).show();
            getNotificationsFromDatabase(isRequests);
            adap.notifyDataSetChanged();
        }
    };



    public ArrayList<ChatListData> generateDummy()
    {
        ArrayList<ChatListData> list=new ArrayList<ChatListData>();

        /*ChatListData x=new ChatListData();
        x.id=1;
        x.userid=1;
        x.text="Check out this game 99 Miles Runner!! When I first played it, I felt it like a grumpy game, Now it is what I love the most <3 <3 <3";
        x.imgDir="danial";
        list.add(x);


        x=new ChatListData();
        x.id=2;
        x.userid=2;
        x.text="Time on marine drive!!!....fun with besties #fun #around_mood #gamersconnect :)";
        x.imgDir="profile";
        list.add(x);

        x=new ChatListData();
        x.id=3;
        x.userid=1;
        x.text="Kid playing Blue whale on the PC !!!";
        x.imgDir="blue";
        list.add(x);

        x=new ChatListData();
        x.id=4;
        x.userid=2;
        x.text="Accedent in the town.. Any one with O+ve blood? HURRY !";
        x.imgDir="acce";
        list.add(x);*/

        return list;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //newPostRef.removeEventListener(childEventListener);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //update_content();

    }
    LinearLayout noFeedsAvailable;
    boolean isRequests;
    Adapter_NoificationData adap;
    ImageButton clearNotification,notificationMArasRead;
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.notificationlist_fragment, container, false);



        isRequests=getArguments().getBoolean("requests");



        rView=(RecyclerView)rootView.findViewById(R.id.rView);

        noFeedsAvailable=(LinearLayout) rootView.findViewById(R.id.noFeedsAvailable);
        clearNotification=(ImageButton) rootView.findViewById(R.id.notification_clear);
        notificationMArasRead=(ImageButton) rootView.findViewById(R.id.notification_markas_reed);

        rView.setHasFixedSize(true);
        mLayoutManager=new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,true);
        mLayoutManager.setStackFromEnd(true);
        rView.setLayoutManager(mLayoutManager);


        getNotificationsFromDatabase(isRequests);

        adap=new Adapter_NoificationData(getActivity(),finaldata);
        rView.setAdapter(adap);

        clearNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cleanAllNotifications(isRequests);
            }
        });


        //TextView textView = (TextView) rootView.findViewById(R.id.section_label);
        //textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
        /*


        //ArrayList<ChatListData> finaldata=generateDummy();
        getDataFromDatabase();

        adap=new Adapter_ChatList(getActivity(),finaldata,getActivity());
        rView.setAdapter(adap);



        floatButtonNewChat=(FloatingActionButton)rootView.findViewById(R.id.floatButtonNewChat);
        floatButtonNewChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                ChromePopUpHelper chromeHelpPopup = new ChromePopUpHelper(getActivity(),"Hello!");
                chromeHelpPopup.show(v);
            }
        });





        //setHasOptionsMenu(true);

        IntentFilter intentFilter = new IntentFilter("CHAT_UPDATED");
//Map the intent filter to the receiver
        getActivity().registerReceiver(chatReciever, intentFilter);


        final LayoutInflater factory = getActivity().getLayoutInflater();

        final View textEntryView = factory.inflate(R.layout.people_near_activity, null);

        //landmarkEditNameView = (EditText) textEntryView.findViewById(R.id.landmark_name_dialog_edit);

       //View slideView=textEntryView.findViewById(R.id.slideView);
/*if(slideView==null)
    Toast.makeText(getActivity(), "doomed", Toast.LENGTH_SHORT).show();*/
       /*slideUp = new SlideUpBuilder(slideView)
               .withStartState(SlideUp.State.HIDDEN)
               .withStartGravity(Gravity.BOTTOM)

               //.withSlideFromOtherView(anotherView)
               //.withGesturesEnabled()
               //.withHideSoftInputWhenDisplayed()
               //.withInterpolator()
               //.withAutoSlideDuration()
               //.withLoggingEnabled()
               //.withTouchableAreaPx()
               //.withTouchableAreaDp()
               //.withListeners()
               //.withSavedState()
               .build();
        slideUp.hideImmediately();*/

        /*chatPeopleNearby=(FloatingActionButton)rootView.findViewById(R.id.chatPeopleNearby);
        chatPeopleNearby.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                Intent editText = new Intent(getActivity(), PopUp_Notification.class);

                startActivity(editText);

            }
        });*/

        return rootView;
    }

    void cleanAllNotifications(boolean isRequests)
    {
        SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();
        Cursor cursor=null;
        int rowln;
        if(!isRequests) {

             rowln = db.delete(DatabaseAccesser.notification_table,
                    DatabaseAccesser.notification_type+" BETWEEN ? AND ?",
                    new String[]{"1","4"});
        }
        else
        {
             rowln = db.delete(DatabaseAccesser.notification_table,
                    DatabaseAccesser.notification_type+" BETWEEN ? AND ?",
                    new String[]{"11","14"});
        }
        Log.e("MSGG","cleaned ROWS "+rowln);
    }

   /* @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.tabbed_chat_window, menu);
       // adap.mActionMode.finish();
    }*/


    public void updateActionMode() {
       /* if(adap.mActionMode !=null)
        adap.mActionMode.finish();*/
    }




    void getNotificationsFromDatabase(boolean isRequests) {


        finaldata.clear();

        SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();
        Cursor cursor=null;
        if(!isRequests) {

            cursor = db.query(DatabaseAccesser.notification_table, new String[]{"rowid", DatabaseAccesser.notification_user_id,
                            DatabaseAccesser.notification_timestamp,
                            DatabaseAccesser.notification_type,
                            DatabaseAccesser.notification_status,
                            DatabaseAccesser.notification_text,
                            DatabaseAccesser.notification_image_url,
                            DatabaseAccesser.notification_link},
                    DatabaseAccesser.notification_type+" BETWEEN ? AND ?",
                    new String[]{"1","4"},
                    null, null, DatabaseAccesser.notification_timestamp + "", null);
        }
        else
        {
            cursor = db.query(DatabaseAccesser.notification_table, new String[]{"rowid", DatabaseAccesser.notification_user_id,
                            DatabaseAccesser.notification_timestamp,
                            DatabaseAccesser.notification_type,
                            DatabaseAccesser.notification_status,
                            DatabaseAccesser.notification_text,
                            DatabaseAccesser.notification_image_url,
                            DatabaseAccesser.notification_link},
                    DatabaseAccesser.notification_type+" BETWEEN ? AND ?",
                    new String[]{"11","14"},
                    null, null, DatabaseAccesser.notification_timestamp + "", null);
        }
        Log.e("MSG","Size of cursor :"+cursor.getCount());
        while (cursor.moveToNext()) {


            NotificationData temp = new NotificationData();

            temp.rowId= cursor.getLong(
                    cursor.getColumnIndexOrThrow("rowid"));
            temp.userid = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.notification_user_id));
            temp.timestamp = cursor.getLong(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.notification_timestamp));
            temp.type = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.notification_type));
            temp.status = cursor.getInt(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.notification_status));

            temp.text = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.notification_text));
            temp.image = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.notification_image_url));
            temp.link = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.notification_link));

            finaldata.add(temp);
        }
        cursor.close();


        if(finaldata.size()==0)
        {
            noFeedsAvailable.setVisibility(View.VISIBLE);
        }
        else
        {
            noFeedsAvailable.setVisibility(View.GONE);
        }

    }
}


