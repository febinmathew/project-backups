package com.hash.around.Utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;

import static com.hash.around.TabFragments.TabFragment2.getResizedBitmap;

/**
 * Created by Febin on 6/30/2018.
 */

public class ByteUtils {

    public static byte[] serialize(Object obj) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ObjectOutputStream os = null;
        try {
            os = new ObjectOutputStream(out);
            os.writeObject(obj);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return out.toByteArray();
    }

    public static Object deserialize(byte[] data)  {

        ByteArrayInputStream in = new ByteArrayInputStream(data);
        ObjectInputStream is = null;
        try {
            is = new ObjectInputStream(in);
            return is.readObject();
        } catch (Exception e) {
            Log.e("GGG","ezception2 "+e.getMessage());
        }

        return null;
    }
    public static Object deserializeFile(String uri)  {

        File file = new File(uri);
        //int size = (int) file.length();
       // byte[] bytes = new byte[size];
        byte[] bytesArray = new byte[(int) file.length()];
        //Log.e("GGG","file starting "+(bytesArray[0]!=null?0:bytesArray[0]));
        FileInputStream ss=null;
        try {

            FileInputStream fis = new FileInputStream(file);
            fis.read(bytesArray); //read file into bytes[]
            fis.close();
            Log.e("GGG","file written "+bytesArray[0]);
            /*ss=new FileInputStream(uri);
            BufferedInputStream buf = new BufferedInputStream(ss);
            buf.read(bytes, 0, bytes.length);
            buf.close();*/
            return deserialize(bytesArray);
        } catch (Exception e) {
            Log.e("GGG","ezception1 "+e.getMessage());
            return null;
        }

    }

    public static  byte[] photoToByteArray(Bitmap bitmap,int maxQuality)
    {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        final byte[] byteArray;
        getResizedBitmap(bitmap, maxQuality).compress(Bitmap.CompressFormat.PNG, 1, bytes);
        byteArray=bytes.toByteArray();
        return byteArray;
    }
    public static String byteToString(byte[] byteArray)
    {
        return Base64.encodeToString(byteArray,Base64.DEFAULT);
    }
    public static byte[] stringToByte(String byteArray)
    {
        return Base64.decode(byteArray,Base64.DEFAULT);
    }

    public static  Bitmap byteArrayToPhoto(byte[] bitmapdata)
    {
        return BitmapFactory.decodeByteArray(bitmapdata, 0, bitmapdata.length);
    }
}
