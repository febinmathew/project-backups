package com.hash.around.Views.Medias;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.hash.around.R;

import java.util.Locale;


public class CircularRelativeLayout extends RelativeLayout {

int radius;
    public CircularRelativeLayout(Context context) {
        this(context, null);
    }

    public CircularRelativeLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CircularRelativeLayout(final Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);


        if (attrs != null) {
            TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CircularRelativeLayout, 0, 0);
            //this.radius   = typedArray.getDimension(R.styleable.CircularRelativeLayout_radius, 20);
            dimen=typedArray.getDimensionPixelSize(R.styleable.CircularRelativeLayout_radius, 4);

            typedArray.recycle();
        }
        //init(context);
    }
    private Bitmap maskBitmap;
    private Paint paint, maskPaint;
    private float cornerRadius;
    private void init(Context context) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        cornerRadius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, metrics);

        paint = new Paint(Paint.ANTI_ALIAS_FLAG);

        maskPaint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG);
        maskPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));

        //setWillNotDraw(false);
    }

    boolean widthSameAsHeight=false;

    public void setWidthSameAsHeight(boolean set)
    {
        widthSameAsHeight=set;
        invalidate();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if(widthSameAsHeight)
            super.onMeasure(heightMeasureSpec, heightMeasureSpec);
        else
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    private Bitmap createMask(int width, int height) {
        Bitmap mask = Bitmap.createBitmap(width, height, Bitmap.Config.ALPHA_8);
        Canvas canvas = new Canvas(mask);

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.WHITE);

        canvas.drawRect(0, 0, width, height, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        canvas.drawRoundRect(new RectF(0, 0, width, height), cornerRadius, cornerRadius, paint);

        return mask;
    }

    /*@Override
    public void draw(Canvas canvas) {
        Bitmap offscreenBitmap = Bitmap.createBitmap(canvas.getWidth(), canvas.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas offscreenCanvas = new Canvas(offscreenBitmap);

        super.draw(offscreenCanvas);

        if (maskBitmap == null) {
            maskBitmap = createMask(canvas.getWidth(), canvas.getHeight());
        }

        offscreenCanvas.drawBitmap(maskBitmap, 0f, 0f, maskPaint);
        canvas.drawBitmap(offscreenBitmap, 0f, 0f, paint);
    }*/
    private final RectF drawRect = new RectF();
    private final Path clipPath = new Path();
    int dimen=getResources().getDimensionPixelSize(R.dimen.default_corner_radius);

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        drawRect.left = 0;
        drawRect.top = 0;
        drawRect.right = getWidth();
        drawRect.bottom = getHeight();
        clipPath.reset();
        clipPath.addRoundRect(drawRect, dimen, dimen, Path.Direction.CCW);
        clipPath.close();
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        int save = canvas.save();
        canvas.clipPath(clipPath);
        super.dispatchDraw(canvas);
        canvas.restoreToCount(save);
    }


}
