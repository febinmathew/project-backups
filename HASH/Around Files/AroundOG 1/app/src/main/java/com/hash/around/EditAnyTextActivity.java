package com.hash.around;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.hash.around.OtherClasses.Adapter_EditAnyText;
import com.hash.around.OtherClasses.DatabaseAccesser;

import com.hash.around.OtherClasses.RecyclerViewTouchListner;

import java.util.ArrayList;

import static com.hash.around.TabbedActivity.userdetail;

public class EditAnyTextActivity extends Activity {
    ArrayList<String> finaldata=new ArrayList<String>();
    Adapter_EditAnyText adap;
    RecyclerView rView;
    private RecyclerView.LayoutManager mLayoutManager;

    EditText editableTextContent;
    DatabaseReference updateRef;
    int textType;
    TextView heading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        setContentView(R.layout.edit_any_text);
        setFinishOnTouchOutside(true);
        DisplayMetrics dm=new DisplayMetrics();

        getWindowManager().getDefaultDisplay().getMetrics(dm);

        //getWindow().setLayout((int)(dm.widthPixels*1),(int)(dm.heightPixels));
       // getWindow().setGravity(Gravity.BOTTOM);
       // getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        heading=(TextView) findViewById(R.id.editTextHeading);
        rView=(RecyclerView)findViewById(R.id.rViewTextList);
        editableTextContent=(EditText) findViewById(R.id.editableTextContent);
        textType=getIntent().getIntExtra("text_type",0);

        FloatingActionButton uploadButton;

        uploadButton=(FloatingActionButton) findViewById(R.id.uploadButton);



        switch (textType)
        {
            case 0:
                heading.setText("New Username");
                rView.setVisibility(View.GONE);
                updateRef = FirebaseDatabase.getInstance().getReference("user_profile").child(userdetail.getUid()).child("user_name");
                break;
            case 1:
                heading.setText("New Status");
                finaldata=retriveSearchTextFromDtabase(1);
                updateRef = FirebaseDatabase.getInstance().getReference("user_profile").child(userdetail.getUid()).child("profile_status");
                setUpListViews();

                break;


        }

        uploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String finalText=editableTextContent.getText().toString();
                switch (textType) {
                    case 0:
                        updateRef.setValue(finalText).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                updateNameOrStatusOrProfileImage(userdetail.getUid(),finalText,null,null);
                                finishWithReturnValue(finalText);
                            }
                        });
                        break;
                    case 1:
                        updateRef.setValue(finalText).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                updateNameOrStatusOrProfileImage(userdetail.getUid(),null,finalText,null);
                                finishWithReturnValue(finalText);
                            }
                        });

                        insertSearchTextToDtabase(finalText, textType);


                        break;

                }

            }
        });










    }

    void finishWithReturnValue(String value)
    {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result",value);
        setResult(RESULT_OK,returnIntent);
        finish();
    }

    void setUpListViews()
    {
        mLayoutManager=new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false);
        rView.setLayoutManager(mLayoutManager);
        adap=new Adapter_EditAnyText(this,finaldata);
        rView.setAdapter(adap);
        registerForContextMenu(rView);
        rView.addOnItemTouchListener(new RecyclerViewTouchListner(this, rView, new RecyclerViewTouchListner.RecyclerClick_Listener()
        {
            @Override
            public void onClick(View view, int position) {

                editableTextContent.setText(finaldata.get(position));
            }

            @Override
            public void onLongClick(View view, int position) {


                //Toast.makeText(getApplicationContext(), position+" Long Press", Toast.LENGTH_SHORT).show();
            }
        }));

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int pos= Adapter_EditAnyText.optionPosition;
        switch (item.getItemId()) {
            case R.id.action_delete_text:
                removeSearchTextFromDtabase(adap.data.get(Adapter_EditAnyText.optionPosition));
                finaldata.remove(Adapter_EditAnyText.optionPosition);
                adap.notifyDataSetChanged();
                break;
        }
        return super.onContextItemSelected(item);

    }





    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    protected void onStop() {
       // overridePendingTransition(R.anim.no_change_anim, R.anim.slide_down_anim);
        super.onStop();

    }

    @Override
    protected void onPause() {
        overridePendingTransition(R.anim.no_change_anim, R.anim.slide_down_anim);
        super.onPause();
    }

    @Override
    public void onBackPressed() {
          super.onBackPressed();
    }

    public static ArrayList<String>  retriveSearchTextFromDtabase(int type)
    {
        ArrayList<String> data=new ArrayList<String>();

          SQLiteDatabase db = TabbedActivity.db.getReadableDatabase();

                Cursor cursor = db.rawQuery(" select * from " + DatabaseAccesser.search_table + " where " +
                        DatabaseAccesser.search_type + " = ? ", new String[]{type + ""});
        while (cursor.moveToNext()) {

            String temp = cursor.getString(
                    cursor.getColumnIndexOrThrow(DatabaseAccesser.search_text));
            data.add(temp);
        }


        return data;
    }

    public static void updateNameOrStatusOrProfileImage(String userID,@Nullable String name,@Nullable String status,@Nullable String image)
    {
        SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();

        ContentValues values = new ContentValues();

        if(name!=null)
        values.put(DatabaseAccesser.user_name, name);
        if(status!=null)
        values.put(DatabaseAccesser.user_status, status);
        if(image!=null)
        values.put(DatabaseAccesser.user_localUrl,image);

        int rowln=db.update(DatabaseAccesser.user_table,values,DatabaseAccesser.user_user_id+" =?",new String[]{userID});


    }

    public static void removeSearchTextFromDtabase(String searchPhrase)
    {
        SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();

        int rowln = db.delete(DatabaseAccesser.search_table,
                DatabaseAccesser.search_text + " =?", new String[]{searchPhrase});
    }

    public static void insertSearchTextToDtabase(String searchPhrase,int type)
    {
        SQLiteDatabase db = TabbedActivity.db.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(DatabaseAccesser.search_text, searchPhrase);
        values.put(DatabaseAccesser.search_type, type);
        values.put(DatabaseAccesser.search_time,System.currentTimeMillis());

        long newRowId = db.insert(DatabaseAccesser.search_table, null, values);
    }


}
