package com.hash.around.Views.Medias;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnPausedListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.R;
import com.hash.around.Views.ViewFunctions;
import com.pitt.library.fresh.FreshDownloadView;

import java.io.File;
import java.io.IOException;


public class AroundDownloadVideoView extends RelativeLayout {

    private boolean centerCrop;
    private int imageMaxHeight;
    MediaPlayer mMediaPlayer;
    VideoView defaultVideoView;
    TextView sizeText;

    boolean layoutChanged=false;
    View rootView;
    Context mContext;
    ImageView imageContent;
    TextureView videotexture;
    FreshDownloadView downloadView;
    ImageButton playButton;
    JobCompletionListner jobComplete=null;


    public AroundDownloadVideoView(Context context) {
        super(context);
        init(context,null);
    }

    public AroundDownloadVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context,attrs);
    }

    public AroundDownloadVideoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context,attrs);
    }

    public void setOnJobCompletionListner(JobCompletionListner job)
    {
        this.jobComplete=job;
    }

    public void setDownloadClickListner(String uri)
    {
        downloadView.setVisibility(VISIBLE);
        imageContent.setVisibility(VISIBLE);
        playButton.setVisibility(GONE);
        defaultVideoView.setVisibility(GONE);
       // videotexture.setVisibility(GONE);

        final String url=uri;
        downloadView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadFile(url);
            }
        });
    }
    public void disableDownloadClick()
    {
        Log.e("MSG","disable");
        downloadView.setVisibility(GONE);
        imageContent.setVisibility(GONE);
        playButton.setVisibility(VISIBLE);
        sizeText.setVisibility(GONE);
        defaultVideoView.setVisibility(VISIBLE);

    }
    public void enableDownloadClick()
    {
        Log.e("MSG","disable");
        downloadView.setVisibility(VISIBLE);
        imageContent.setVisibility(VISIBLE);
        playButton.setVisibility(GONE);
        sizeText.setVisibility(VISIBLE);
        defaultVideoView.setVisibility(GONE);

    }
    //AroundMinimalMediaController mediaControls;
    public void setUpVideoUri(Uri url)
    {
       //=new MediaController(mContext);
//mediaControls.setAnchorView(this);
        //mediaControls.se
        downloadView.setOnClickListener(null);


        defaultVideoView.setVideoURI(url);
        //defaultVideoView.setMediaController(mediaControls);
        defaultVideoView.requestFocus();



        defaultVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                      @Override
                       public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {

                       }
                         }
                );
               // mediaControls = new AroundMinimalMediaController(getContext(),false);




               /* FrameLayout viewGroupLevel1 = (FrameLayout) mediaControls. getRootView();
                Log.e("MGG","controlelr1: "+viewGroupLevel1+" / "+viewGroupLevel1.getChildCount());
                LinearLayout viewGroupLevel2 = (LinearLayout)  viewGroupLevel1.getChildAt(0);
                Log.e("MGG","controlelr2: "+viewGroupLevel2+" / "+viewGroupLevel2.getChildCount());

                FrameLayout viewGroupLevel3 = (FrameLayout)  viewGroupLevel2.getChildAt(1);
                Log.e("MGG","controlelr3: "+viewGroupLevel3+" / "+viewGroupLevel3.getChildCount());

                MediaController viewGroupLevel4 = (MediaController)  viewGroupLevel3.getChildAt(0);
                Log.e("MGG","controlelr4: "+viewGroupLevel4+" / "+viewGroupLevel4.getChildCount());*/

               //viewGroupLevel4.getChildAt(0).setVisibility(INVISIBLE);
               // Log.e("MGG","controlelr3: "+viewGroupLevel2.getChildAt(0).getId());
                //viewGroupLevel2.getChildAt(0).setVisibility(INVISIBLE);
                //viewGroupLevel2.getChildAt(1).getId();
                //viewGroupLevel1.getChildAt(0).setVisibility(GONE);
                /*View view = viewGroupLevel2.getChildAt(2);
                view.setVisibility(View.GONE);*/

                //mediaControls.setMediaPlayer(defaultVideoView);
               // defaultVideoView.setMediaController( mediaControls);
                //mediaControls.setEnabled(false);

                //mediaControls.setAnchorView(defaultVideoView);
                defaultVideoView.seekTo(5);
            }});

        playButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                defaultVideoView.start();
                ViewFunctions.independantAnimateOut(playButton,mContext);
                ViewFunctions.fadeOut(findViewById(R.id.minimalMediaController));


            }
        });
        defaultVideoView.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(defaultVideoView.isPlaying())
                {pauseVideo();}
                return false;
            }
        });
        defaultVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                defaultVideoView.seekTo(5);
                pauseVideo();
            }
        });
        //vid.start();
        layoutChanged=true;

    }

    void pauseVideo()
    {
        if(defaultVideoView!=null)
        {defaultVideoView.pause();
        ViewFunctions.independantAnimateIn(playButton,mContext);
            ViewFunctions.fadeIn(findViewById(R.id.minimalMediaController));
        }

    }


    private void init(Context context, @Nullable AttributeSet attrs) {

        mContext=context;
        rootView = inflate(context, R.layout.around_videoview, this);
        imageContent = (ImageView) rootView.findViewById(R.id.imageContent);
        downloadView =(FreshDownloadView) rootView.findViewById(R.id.downloadView);

        defaultVideoView=(VideoView) rootView.findViewById(R.id.defaultVideoView);
        sizeText=rootView.findViewById(R.id.sizeText);
        playButton=(ImageButton) rootView.findViewById(R.id.playButton);
        /*if(attrs!=null)
        {
            TypedArray ta = context.getTheme().obtainStyledAttributes(attrs, R.styleable.AroundDownloadImageView, 0, 0);
            centerCrop = ta.getBoolean(R.styleable.AroundDownloadImageView_imageCenterCrop, false);
            if(centerCrop)
            imageContent.setScaleType(ImageView.ScaleType.CENTER_CROP);

            imageMaxHeight = ta.getDimensionPixelSize(R.styleable.AroundDownloadImageView_imageMaxHeight, imageContent.getHeight());
            imageContent.setMaxHeight(imageMaxHeight);

        }*/

    }
    public void setLocallyAvailable(boolean locallyAvailable)
    {
        if(locallyAvailable)
        {
            disableDownloadClick();
        }
        else
        {
            enableDownloadClick();
        }
    }


    TextureView getTextureView()
    {
        return videotexture;
    }
    public ImageView getImageView()
    {
        return imageContent;
    }
    FreshDownloadView getDownloadView()
    {
        return downloadView;
    }

    void downloadFile(String urlToDownload)
    {
        downloadView.startDownload();

        File localFile = null;
        final String localFilename;
        try {
            localFile = File.createTempFile("videos", "mp4");

        } catch (IOException e) {
            e.printStackTrace();
        }
        localFilename=localFile.toURI().toString();
        StorageReference httpsReference = FirebaseStorage.getInstance().getReferenceFromUrl(urlToDownload);
        httpsReference.getFile(localFile).addOnProgressListener(new OnProgressListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onProgress(FileDownloadTask.TaskSnapshot taskSnapshot) {
                float percent=((float)taskSnapshot.getBytesTransferred()/(float)taskSnapshot.getTotalByteCount())*100;
               // Log.e("MSG",((float)taskSnapshot.getBytesTransferred()/(float)taskSnapshot.getTotalByteCount())*100+"//\\"+
                        //((float)taskSnapshot.getBytesTransferred()/(float)taskSnapshot.getTotalByteCount())+"//\\"+percent);
                downloadView.upDateProgress((int)percent);
            }
        }).addOnFailureListener(new OnFailureListener() {

            @Override
            public void onFailure(@NonNull Exception e) {
                downloadView.showDownloadError();
            }
        }).addOnPausedListener(new OnPausedListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onPaused(FileDownloadTask.TaskSnapshot taskSnapshot) {
                downloadView.reset();
                Log.e("MSG","paused");

            }
        }).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
               // downloadView.showDownloadOk();
                Log.e("MSG","succeded");
                //disableDownloadClick();
                if(jobComplete!=null)
                {
                    jobComplete.onJobCompletion(localFilename);
                }
                //downloadView.clearAnimation();

            }
        })

        ;
    }
}
