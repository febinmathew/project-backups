package com.hash.around.Utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;

/**
 * Created by Febin on 6/28/2018.
 */

public class PermissionManager {

    public static boolean checkHasPermission (Context context, Activity act,String[] permissions)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            for(int i=0;i<permissions.length;i++) {
                if (context.checkSelfPermission(permissions[i]) != PackageManager.PERMISSION_GRANTED ) {
                    act.requestPermissions(permissions, 11);
                    return false;
                }
            }
            return true;
        }
        else
            return true;

    }
}
