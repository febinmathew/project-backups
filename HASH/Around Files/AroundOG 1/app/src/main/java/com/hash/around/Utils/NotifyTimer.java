package com.hash.around.Utils;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.text.format.DateUtils;
import android.util.Log;
import android.widget.TextView;

import com.hash.around.OtherClasses.Interfaces.TimerListner;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Febin on 6/20/2018.
 */

public class NotifyTimer implements Runnable {

    static ArrayList<TimerListner> listeners=new ArrayList<TimerListner>();
    static ArrayList<Integer> listenerIntervels=new ArrayList<Integer>();
    static ArrayList<Long> lastUpdateTimestamp=new ArrayList<Long>();
    static ArrayList<Long> totalTime=new ArrayList<Long>();
    static ArrayList<Boolean> pastTime=new ArrayList<Boolean>();

    static boolean handler=false;
    static boolean stop=false;

    public void addNewListnerWithIntervel(boolean past,Long totalTim,TimerListner list)
    {
        past=false;

        Log.e("MSGG","listner sixe is "+listeners.size());
        //int pos;

            listeners.add(list);
            listenerIntervels.add(calculateIntervel(totalTim,past));
            lastUpdateTimestamp.add(System.currentTimeMillis());
            totalTime.add(totalTim);
            pastTime.add(past);


        list.onSpecifiedIntervel(null,null);
        if(!handler) {
            stop=false;
            handler = new Handler(Looper.getMainLooper()).postDelayed(this, 1000);
        }

    }

    int calculateIntervel(Long currentTime,boolean past)
    {
        if(getDistance(currentTime,past)<=120000) return  1000;
        else return 60000;
    }

    void removeAListner(int pos)
    {
        listeners.remove(pos);
        listenerIntervels.remove(pos);
        lastUpdateTimestamp.remove(pos);
        totalTime.remove(pos);
        pastTime.remove(pos);
        //rowID.remove(pos);
    }
    public static void removeAllListner()
    {
        listeners.clear();
        listenerIntervels.clear();
        lastUpdateTimestamp.clear();
        totalTime.clear();
        pastTime.clear();
        //rowID.clear();
        //stop=true;
    }
    static int count=0;
    long getDistance(Long time,boolean past)
    {
        if(past)
            return System.currentTimeMillis() - time;
        else
            return time-System.currentTimeMillis();
    }
    @Override
    public void run() {
        for(int i=listeners.size()-1;i>=0;i--) {
            if ((System.currentTimeMillis()-lastUpdateTimestamp.get(i)) > listenerIntervels.get(i)) {
                //if()
               // Log.e("MSGG","execiting  "+count++);
               // Log.e("MSGG","remaining  "+totalTime.get(i)+" / "+getDistance(totalTime.get(i),false)+" / "+
                       // (totalTime.get(i)-System.currentTimeMillis()));
                if(listeners.get(i)!=null ) {

                    listenerIntervels.set(i,calculateIntervel(totalTime.get(i),pastTime.get(i)));
                    listeners.get(i).onSpecifiedIntervel(null, null);
                }
                else
                {
                    removeAListner(i);
                }
                lastUpdateTimestamp.set(i, System.currentTimeMillis());

                if( !pastTime.get(i) && getDistance(totalTime.get(i),pastTime.get(i))<=0 )
                {
                    Log.e("MSGG","removing "+i);

                    if(listeners.get(i)!=null ) {
                        listeners.get(i).onTimerEnd(null,null);
                    }
                    removeAListner(i);
                }
            }
        }
        if(!stop && listeners.size()>0)

            handler= new Handler(Looper.getMainLooper()).postDelayed(this, 1000);
        else
            handler=false;
    }
}