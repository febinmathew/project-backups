package com.hash.around.Views.Medias;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.RectF;
import android.media.audiofx.Visualizer;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnPausedListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.hash.around.OtherClasses.ChatDataAdapter;
import com.hash.around.OtherClasses.DataTypes.GroupChatData;
import com.hash.around.OtherClasses.Interfaces.AudioPlayerListener;
import com.hash.around.OtherClasses.Interfaces.ChatSentListner;
import com.hash.around.OtherClasses.Interfaces.JobCompletionListner;
import com.hash.around.R;
import com.hash.around.Utils.ChatSender;
import com.hash.around.Utils.MediaManager;
import com.hash.around.Views.CircleColorImageView;
import com.hash.around.Views.CustomMakers.AroundDownloadView;
import com.vanniktech.emoji.EmojiTextView;

import java.io.File;
import java.io.IOException;


public class AroundChatAudioThumbline extends RelativeLayout {

    private static final String TAG = AroundChatAudioThumbline.class.getSimpleName();

AroundVisualizerView visualizerView;
    public TextView sizeText,timeIndication;
    public CircleColorImageView playPause;
    public SeekBar audioSeek;
    AroundDownloadView downloadView;
    LinearLayout audioDowloadSection;
    RelativeLayout audioDetailSection;
    JobCompletionListner jobComplete = null;
    ChatSentListner uploadListner = null;

    CircularRelativeLayout circularLayout;

    public AroundChatAudioThumbline(Context context) {
        this(context, null);
    }

    public AroundChatAudioThumbline(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AroundChatAudioThumbline(final Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        inflate(context, R.layout.around_audio_thumbline_view, this);


        sizeText=findViewById(R.id.sizeText);
        playPause=findViewById(R.id.playPause);
        audioSeek=findViewById(R.id.audioSeek);
        timeIndication=findViewById(R.id.timeIndication);
        audioDowloadSection=findViewById(R.id.audioDowloadSection);
        audioDetailSection=findViewById(R.id.audioDetailSection);
        downloadView=findViewById(R.id.downloadView);
        circularLayout=findViewById(R.id.circularLayout);
        visualizerView=findViewById(R.id.visualizerView);
        /*if (attrs != null) {
            TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.AroundChatImageThumbline, 0, 0);
            //backgroundColorHint   = typedArray.getColor(R.styleable.AroundChatImageThumbline_backgroundColorHint, Color.BLACK);

            typedArray.recycle();
        }*/

    }

    public void setAudioUri(final String uri)
    {
        timeIndication.setText(DateUtils.formatElapsedTime(
                MediaManager.getMediaDuration(getContext(),uri)/1000));
        playPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaManager.playAudio(getContext(), uri,
                        audioSeek, timeIndication,visualizerView,new AudioPlayerListener() {
                            @Override
                            public void onStarted() {
                                //Log.e("MGG","audioo startssssss ");
                                Log.e("GGG","visualizer SETTING2");
                                playPause.setImageResource(R.drawable.ic_pause_accent);
                                visualizerView.setVisibility(VISIBLE);
                            }

                            @Override
                            public void onPaused() {
                                Log.e("MGG","audioo paused ");
                               playPause.setImageResource(R.drawable.ic_play_arrow_white);
                            }

                            @Override
                            public void onCompleted() {
                                Log.e("MGG","audioo complted ");
                                playPause.setImageResource(R.drawable.ic_play_arrow_white);
                            }

                            @Override
                            public void onIntervelUpdate(final long timestamp) {
                                ((Activity)getContext()).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        timeIndication.setText(DateUtils.formatElapsedTime(timestamp/1000));
                                    }
                                });

                            }
                        });
            }
        });
    }


    public void setSizeText(String text)
    {
        sizeText.setText(text);
    }


    public void enableDownloadClick()
    {
        audioDowloadSection.setVisibility(VISIBLE);
        audioDetailSection.setVisibility(GONE);
        circularLayout.setWidthSameAsHeight(false);
    }
    public void disableDownloadClick() {
        audioDowloadSection.setVisibility(GONE);
        audioDetailSection.setVisibility(VISIBLE);
        downloadView.setOnClickListener(null);
        circularLayout.setWidthSameAsHeight(true);
    }

    public void setDownloadClickListner(String uri) {
        enableDownloadClick();
        downloadView.setIsDownload();
        final String url = uri;
        downloadView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadFile(url);
            }
        });
    }
    public void setOnJobCompletionListner(JobCompletionListner job) {
        this.jobComplete = job;
    }

    public void setOnFileUploadListener(ChatSentListner job) {
        this.uploadListner = job;
    }

    public void setUploadClickListner(String uri, final GroupChatData data,final long mediaRow,final StorageReference ref) {
        enableDownloadClick();
        downloadView.setIsUpload();
        final String url = uri;
        downloadView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadFile(url,ref,data,mediaRow);
            }
        });
    }
    //GroupChatData thisChatData;
    void downloadFile(String urlToDownload) {
        Log.e("MSG", "download "+urlToDownload);
        downloadView.startDownload();

        File localFile = null;
        final String localFilename;
        try {
            localFile = File.createTempFile("images", "png");

        } catch (IOException e) {
            e.printStackTrace();
        }
        localFilename = localFile.toURI().toString();
        StorageReference httpsReference = FirebaseStorage.getInstance().getReferenceFromUrl(urlToDownload);
        httpsReference.getFile(localFile).addOnProgressListener(new OnProgressListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onProgress(FileDownloadTask.TaskSnapshot taskSnapshot) {
                float percent = ((float) taskSnapshot.getBytesTransferred() / (float) taskSnapshot.getTotalByteCount()) * 100;
                // Log.e("MSG",((float)taskSnapshot.getBytesTransferred()/(float)taskSnapshot.getTotalByteCount())*100+"//\\"+
                //((float)taskSnapshot.getBytesTransferred()/(float)taskSnapshot.getTotalByteCount())+"//\\"+percent);
                downloadView.upDateProgress((int) percent);
            }
        }).addOnFailureListener(new OnFailureListener() {

            @Override
            public void onFailure(@NonNull Exception e) {
                downloadView.showDownloadError();
            }
        }).addOnPausedListener(new OnPausedListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onPaused(FileDownloadTask.TaskSnapshot taskSnapshot) {
                downloadView.reset();
                Log.e("MSG", "paused");

            }
        }).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                // downloadView.showDownloadOk();
                Log.e("MSG", "succeded");
                disableDownloadClick();
                if (jobComplete != null) {
                    jobComplete.onJobCompletion(localFilename);
                }
                //downloadView.clearAnimation();

            }
        })

        ;
    }

    void uploadFile(String urlToUpload,StorageReference ref,GroupChatData data,long mediaID) {

        ChatSender.uploadMediaData(urlToUpload,data,uploadListner);
        /*uploadResourceAndReturnUrls(numberType, urlToUpload, ref,new FileUploadListner() {
            @Override
            public void onFileUploaded(String global, String globalLite) {

                uploadListner.onFileUploaded(global,globalLite);
            }
        });*/
    }

    public void setLocallyAvailable(boolean locallyAvailable)
    {
        if(locallyAvailable)
        {
            disableDownloadClick();
        }
        else
        {
            enableDownloadClick();
        }
    }


    private final RectF drawRect = new RectF();
    private final Path clipPath = new Path();
    int dimen=getResources().getDimensionPixelSize(R.dimen.default_corner_radius);

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        drawRect.left = 0;
        drawRect.top = 0;
        drawRect.right = getWidth();
        drawRect.bottom = getHeight();
        clipPath.reset();
        clipPath.addRoundRect(drawRect, dimen, dimen, Path.Direction.CCW);
        clipPath.close();
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        int save = canvas.save();
        canvas.clipPath(clipPath);
        super.dispatchDraw(canvas);
        canvas.restoreToCount(save);
    }

}
