package com.hash.around.Broadcasts;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.hash.around.OtherClasses.DataTypes.ChatReplayData;
import com.hash.around.OtherClasses.DataTypes.MediaData;
import com.hash.around.OtherClasses.DataTypes.UserWholeData;
import com.hash.around.Utils.ChatSender;

import java.util.ArrayList;

/**
 * Created by Febin on 6/28/2018.
 */

public class ChatReceiver extends BroadcastReceiver {
    MediaData receiverData;
    ChatReplayData replayData;
    UserWholeData userData;
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("GGG","listner recievedd ");
        receiverData=(MediaData)intent.getSerializableExtra("mediaarray");
        replayData=(ChatReplayData)intent.getSerializableExtra("replayData");

        userData=(UserWholeData) intent.getSerializableExtra("userData");

            ChatSender.AIMessageSender(context,receiverData.type,receiverData.uri,receiverData.text,replayData,userData,null);



    }
}
