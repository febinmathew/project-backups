package com.hash.around.OtherClasses.Interfaces;


public interface AudioPlayerListener {
    void onStarted();
    void onPaused();
    void onCompleted();
    void onIntervelUpdate(long timestamp);
}


