﻿using UnityEngine;
using System.Collections;

public class mainfileScript : MonoBehaviour {


	//public GameObject baseground;
	public float camposx,score;
	public int basestat;
	//GameObject[] basepool=new GameObject[4];
	int i;
	float []size=new float[4];
	public Transform[] trans=new Transform[4];
	basegroundscript[] gscript=new basegroundscript[4];
	public Transform character;
	public static mainfileScript MainFile{get;set;}
	float scale;
	public bool scoreupdate;
	int poolstatus;
	public GameObject magicparticle;
	GameObject []particlelist=new GameObject[10];

	public bool Gameover;

	public SpriteRenderer []clrsprite=new SpriteRenderer[2];
	Color[] two = new Color[3];

	Color[] bgclr=new Color[10];
	Color[] baseclr=new Color[10];
	Color[] ovclr=new Color[10];

	int temp,prevtemp;
	float timecount,timestat;

	Color c1,c2,c3;
	float pp;
	//Color32 n=new Color32;
	void Awake()
	{
		timestat = 1;
		score = 0;


		bgclr [0] = new Color (0.2509f,0.4039f,0.7960f,1f);
		baseclr [0] = new Color (0.078431f,0.019607f,0.019607f,1f);
		ovclr [0] = new Color (0.572549f,0f,0.937254f,1f);

		bgclr [1] = new Color (0f,0.576470f,0.380392f,1f);
		baseclr [1] = new Color (0f,0.352941f,0.576470f,1f);
		ovclr [1] = new Color (0f,0f,0f,1f);

		bgclr [2] = new Color (0.2f,0.2f,0.2f,1f);
		baseclr [2] = new Color (0.2f,0f,0f,1f);
		ovclr [2] = new Color (0.6f,0.4f,0.8f,1f);

		bgclr [3] = GetColorto1 (153,153,153);
		baseclr [3] = GetColorto1 (0,0,0);
		ovclr [3] = GetColorto1(0,206,194);

		bgclr [4] = GetColorto1 (153,51,153);
		baseclr [4] = GetColorto1 (0,0,0);
		ovclr [4] = GetColorto1 (255,255,255);

		bgclr [5] = GetColorto1 (66,66,66);
		baseclr[5] = GetColorto1 (4,182,117);
		ovclr [5] = GetColorto1(150,0,255);

		bgclr [6] = GetColorto1 (0,131,147);
		baseclr [6] = GetColorto1 (48,2,27);
		ovclr [6] = GetColorto1(72,224,158);

		bgclr [7] = GetColorto1 (51,51,51);
		baseclr [7] = GetColorto1 (51,0,0);
		ovclr [7] = GetColorto1(151,102,204);

		bgclr [8] = GetColorto1 (153,51,153);
		baseclr [8] = GetColorto1 (248,248,248);
		ovclr [8] = GetColorto1(227,64,0);

		bgclr [9] = GetColorto1 (157,133,133);
		baseclr [9] = GetColorto1 (9,15,35);
		ovclr [9] = GetColorto1(101,176,154);


		temp = Random.Range (0, bgclr.Length);
		prevtemp = temp;

		basegroundscript.clr = baseclr [temp];
		two[0]=clrsprite[0].color= bgclr [temp];
		two[1]=clrsprite[1].color= ovclr [temp];
		two [2] = baseclr [temp];



		c1=new Color (0.2509f,0.4039f,0.7960f,1f);
		c2=new Color (0f,0.576470f,0.380392f,1f);
		c3 = new Color ();
		//Debug.Log (bgclr [0]);
		//Color a = Color.RGBToHSV (0.2509f,0.4039f,0.7960f,);

		//Debug.Log (a);


		//clrsprite [0].color = bgclr [0];
	}

	Color GetColorto1(int a,int b,int c)
	{
		return new Color (a/255.0f,b/255.0f,c/255.0f);
	}


	// Use this for initialization
	void Start () {
		pp=Time.time;
		timecount = 0;

		Gameover = false;

		//SleepTimeout=0;
		basestat = 0;
		size [0] = 4.49f;
		gscript [0] = trans [0].GetComponent<basegroundscript> ();
		MainFile = this;
		poolstatus = 0;
		for (i = 1; i < 4; i++) {

			gscript [i] = trans [i].GetComponent<basegroundscript> ();
			//size [i] = 0.5f;
			scale = Random.Range (0.2f,0.7f);
			trans [i].localScale = new Vector2 (scale,trans [i].localScale.y);
			size [i] = scale * 8.98f/2;
			//Debug.Log (size[i-1]+"/"+size[i]);
			trans [i].position = new Vector3 (trans[i-1].position.x+size[i-1]+size[i]+Random.Range(0.6f,2.0f),trans [0].position.y,trans [0].position.z);


			//basepool[i]= (GameObject)Instantiate (baseground);
		}

		for (i = 0; i < 10; i++) 
		{
			particlelist [i] = (GameObject)Instantiate (magicparticle);
			particlelist [i].SetActive (false);		
		}

	
	}
	void FixedUpdate()
	{
		if (scoreupdate) 
		{
			score += 0.1f;
		}


		timecount += Time.deltaTime;
		//Debug.Log (score);
	}
	
	// Update is called once per frame
	void Update () {




		if (timecount > 17) 
		{

			timecount = 0;


			temp=Random.Range (0, bgclr.Length);

			while (temp == prevtemp) 
			{
				temp=Random.Range (0, bgclr.Length);
			}
			prevtemp = temp;
			timestat = 0;
			//basegroundscript.clr = baseclr [temp];
			two[0]= bgclr [temp];
			two[1]= ovclr [temp];
			two[2]= baseclr [temp];


		}

		if (timestat <= 1) 
		{
			timestat += 0.02f / 15f;
			clrsprite[0].color = Color.Lerp(clrsprite[0].color,two[0],timestat);
			clrsprite[1].color = Color.Lerp(clrsprite[1].color,two[1],timestat);
			basegroundscript.clr=Color.Lerp(basegroundscript.clr,two[2],timestat);
		}
		//Debug.Log (Time.time);

		/*c3 = Color.Lerp (c1, c2,Time.deltaTime);
		Debug.Log(c3);*/
		
		//clrsprite[0].color = Color.Lerp(clrsprite[0].color,two[0],1f);
		//clrsprite[1].color = Color.Lerp(clrsprite[1].color,two[1],1f);
		//clrsprite[0].color = two[0];
		//clrsprite [1].color = two [1];

		transform.position = new Vector3(character.position.x+1,0,-10f);


		//Debug.Log (basestat);
		camposx = transform.position.x;

		if (trans[basestat].position.x + size[basestat] < camposx - 4) 
		{
			if (basestat == 0) {

				scale = Random.Range (0.15f,0.7f);
				trans [0].localScale = new Vector2 (scale,trans [0].localScale.y);
				size [0] = scale * 8.98f/2;
				trans [0].position = new Vector3 (trans[3].position.x+size[3]+size[0]+Random.Range(1.1f,4.2f),trans [0].position.y,trans [0].position.z);
				gscript [0].reset ();

			} 
			else 
			{
				scale = Random.Range (0.15f,0.7f);
				trans [basestat].localScale = new Vector2 (scale,trans [basestat].localScale.y);
				size [basestat] = scale * 8.98f/2;
				gscript [basestat].reset ();
				trans [basestat].position = new Vector3 (trans[basestat-1].position.x+size[basestat-1]+size[basestat]+Random.Range(1.1f,4.2f),trans [0].position.y,trans [0].position.z);
			}
			basestat++;
			if (basestat == 4)
				basestat = 0;
			
		}

		/*for (i = 0; i < 4; i++) 
		{
		
			if (basestat [i]) {
				if (i == 0) {

					trans [0].position = new Vector3 (trans[3].position.x+7,trans [0].position.y,trans [0].position.z);

				} 
				else {
					trans [i].position = new Vector3 (trans[i-1].position.x+7,trans [0].position.y,trans [0].position.z);
				}

				basestat [i] = false;
			}
				

		}*/

	}

	public void getobjno(float a)
	{
		particlelist [poolstatus].GetComponent<Rigidbody2D> ().Sleep ();
		particlelist [poolstatus].transform.position = new Vector3 (a,-1.55f,particlelist [poolstatus].transform.position.z);
		particlelist [poolstatus].SetActive (true);
		float p = Random.Range (0.12f,0.50f);
		particlelist [poolstatus].transform.localScale = new Vector3 (p,p,p);
		if(Random.Range(0,100)>50)
		particlelist [poolstatus].GetComponent<SpriteRenderer> ().sortingOrder = 4;
		else
		particlelist [poolstatus].GetComponent<SpriteRenderer> ().sortingOrder = 4;

		float temp = Random.Range (40f, 150f);

		particlelist [poolstatus].GetComponent<Rigidbody2D> ().AddForce (new Vector2(-temp/2,temp));


		particlelist [poolstatus].GetComponent<Rigidbody2D> ().AddTorque (Random.Range(-2.0f,20f));

		poolstatus++;
		if (poolstatus == 10) {
			poolstatus = 0;
		}
	}
}
