﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class CanvasScript : MonoBehaviour {
	public Text scoretext;
	public GameObject Menu,GameOver;
	Animator MenuAnim,GameOverAnim;
	public GameObject chara;

	public static CanvasScript canscript{get;set;}
	// Use this for initialization
	void Start () {
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		canscript = this;
		MenuAnim = Menu.GetComponent<Animator> ();
		GameOverAnim=GameOver.GetComponent<Animator> ();
		taptoplay ();
	}
	
	// Update is called once per frame
	void Update () {





		/*if ((int)mainfileScript.MainFile.score==0)
		scoretext.text = "";
		else
		scoretext.text = (int)mainfileScript.MainFile.score + "";*/
	}

	public void taptoplay()
	{
		MenuAnim.SetBool ("menuload",false);
		chara.GetComponent<Rigidbody2D> ().isKinematic = false;
		//chara.GetComponent<sidemovement> ().enabled = true;
	}

	public void restartgame()
	{
		if (mainfileScript.MainFile.Gameover) {
			chara.transform.position = new Vector3 (chara.transform.position.x, charscript.ypos , 0);
			chara.transform.rotation = Quaternion.Euler (0, 0, 0);

			mainfileScript.MainFile.Gameover = false;
			mainfileScript.MainFile.score = 0;
			GameOverAnim.SetBool ("gameover", false);
			chara.GetComponent<charscript> ().jump = true;

			chara.GetComponent<sidemovement> ().enabled = true;
			chara.GetComponent<Rigidbody2D> ().isKinematic = false;
			chara.GetComponent<Rigidbody2D> ().AddForce(new Vector2(0,800));
			chara.GetComponent<Rigidbody2D> ().AddTorque(Random.Range(-20f,-30f));
		}

	}

	public void gameOver()
	{
		GameOver.SetActive (true);
		GameOverAnim.SetBool ("gameover", true);
	}
}
