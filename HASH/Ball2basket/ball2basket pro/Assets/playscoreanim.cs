﻿using UnityEngine;
using System.Collections;

public class playscoreanim : MonoBehaviour {
private Animator anim; 
private gamescript bscript;
	
	void Start () {
	anim = GetComponent<Animator>(); 
	bscript=GameObject.Find("bg").GetComponent<gamescript>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
	if(bscript.score >=1)
	{
	anim.SetBool("fire", true);
	}
	}
}
