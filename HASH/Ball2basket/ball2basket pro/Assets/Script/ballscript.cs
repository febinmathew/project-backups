﻿using UnityEngine;
using System.Collections;

public class ballscript : MonoBehaviour {
public GameObject ballbody;
public float ballcount;
public bool failed,won;
Vector3 pos;
float sizerand,forcerand;
private music_on_off scriptstop;
public float  score;
public GameObject sounds;

	// Use this for initialization
	void Start () {
	ballcount=0;
	failed=false;
	won=false;
	score=30;
	}
	
	// Update is called once per frame
	void Update () {

	if(won || failed)
	{
	scriptstop = GameObject.Find("soundonoff").GetComponent<music_on_off>();
	music_on_off.soundon=false;
	
	}
	if(failed)
	{
	Instantiate(sounds);
	}
	if(GetComponent<Renderer>().enabled)
	{
	if(Mathf.Abs(transform.position.x) > 5	|| Mathf.Abs(transform.position.y) > 3)
	{
	failed=true;}}

	if(!won)
	{
	score -= Time.deltaTime;
	}
	
	if(failed){

	ballcount++;
	
	}
	 
	if(ballcount<10  && failed)
	{
	
	pos =transform.position;
	pos.x=Random.Range(-0.2f,0.2f);
	pos.x =transform.position.x +pos.x;
	GameObject ballinstance =(GameObject)Instantiate(ballbody, pos, transform.rotation); 
	sizerand=Random.Range(0.25f,0.6f);
    ballinstance.transform.localScale = new Vector3(sizerand, sizerand, sizerand); 
	forcerand=Random.Range(-100f,100f);
	ballinstance.GetComponent<Rigidbody2D>().AddForce(new Vector2(forcerand, forcerand));
	

	}
	else if(ballcount>9  && failed)
	{
	
	Destroy(gameObject);
	
	}
	
	

	}

	void OnTriggerEnter2D (Collider2D col)

	{
	if(col.tag == "basketblock")
	{
	failed=true;
	GetComponent<Renderer>().enabled = false;
	
	}
	if(col.tag == "wonblock")
	{
	score /= 10;
	won=true;
	
	}

	}
}
