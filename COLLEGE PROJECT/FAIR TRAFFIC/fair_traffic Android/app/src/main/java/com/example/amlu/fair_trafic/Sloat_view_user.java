package com.example.amlu.fair_trafic;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Sloat_view_user extends AppCompatActivity implements AdapterView.OnItemClickListener, View.OnClickListener {
    ListView l1;
    TextView t;
    static final String ACTION_SCAN = "com.google.zxing.client.android.SCAN";

    String imgid="";
    public static String check="Invalid QR code";

    String [] type,id,status,full,name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sloat_view_user);
        l1=(ListView)findViewById(R.id.slot1);
        try {

            if(Build.VERSION.SDK_INT>9){
                StrictMode.ThreadPolicy policy=new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
        }catch (Exception e){
            Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
        }

        t=(TextView)findViewById(R.id.textView5);

        t.setOnClickListener(this);

        try
        {

            SharedPreferences sharedPreferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

            String url="http://"+sharedPreferences.getString("ip","")+"/fair_trafic/android/slot_view.php";

            String lid=sharedPreferences.getString("lid","");
            List<NameValuePair> list=new ArrayList<NameValuePair>();

            list.add(new BasicNameValuePair("id",""+View_parking_user.id[View_parking_user.pos]+""));

            JSONParser jsonParser=new JSONParser();
            JSONObject jb = jsonParser.makeHttpRequest(url,"GET",list);
            String res=jb.getString("status");
            Log.d("url========",url);
            if (res.equalsIgnoreCase("1"))

            {
                l1.setOnItemClickListener(this);

                //Toast.makeText(getApplicationContext(), "aaaaa", Toast.LENGTH_SHORT).show();

                JSONArray ja = new JSONArray();
                ja = jb.getJSONArray("data");
                type= new String[ja.length()];

                status = new String[ja.length()];
                id = new String[ja.length()];
                name = new String[ja.length()];



                full = new String[ja.length()];


                for (int i = 0; i < ja.length(); i++) {
                    JSONObject jo = new JSONObject();
                    jo = ja.getJSONObject(i);
                    type[i] = jo.getString("type");
                    status[i] = jo.getString("status");
                    id[i] = jo.getString("id");
                    name[i] = jo.getString("name");



                    full[i] = "Type : " + type[i] + "\nStatus :" + name[i];


                }
            }
            else
            {
                full=new String[1];
                full[0]="no slots";
            }
                ArrayAdapter<String> add = new ArrayAdapter<>(getApplicationContext(), R.layout.custom_simple_text1, full);
                l1.setAdapter(add);


        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage().toString(), Toast.LENGTH_SHORT).show();
        }








    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view,final int i, long l) {

        imgid=id[i];

        final AlertDialog.Builder alert=new AlertDialog.Builder(Sloat_view_user.this);
        alert.setTitle("Do you want");
        alert.setMessage("Request");
        alert.setPositiveButton("Check", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {



                final AlertDialog.Builder alert1=new AlertDialog.Builder(Sloat_view_user.this);
                alert1.setTitle("Scan QR code");


                alert1.setNegativeButton("Scan", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {





                            try {
                                Intent intent = new Intent(ACTION_SCAN);
                                intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
                                startActivityForResult(intent, 0);
                            } catch (ActivityNotFoundException anfe) {
                                Toast.makeText(Sloat_view_user.this, "No Scanner Found", Toast.LENGTH_SHORT).show();

                                showDialog(Sloat_view_user.this, "No Scanner Found", "Download a scanner code activity?", "Yes", "No").show();
                            }




                    }
                });
                alert1.setPositiveButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                         Intent it=new Intent(getApplicationContext(),Sloat_view_user.class);
                        startActivity(it);


                    }
                });
                AlertDialog al1=alert1.create();
                al1.show();



            }
        });
        alert.setNegativeButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int list) {


                try {

                      {
                        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        String ip = sharedPreferences.getString("ip", "");
                        String ridd = id[i];

                          String url = "http://" + ip + "/fair_trafic/android/dlt_slot.php";

                          List<NameValuePair> list1 = new ArrayList<NameValuePair>();
                        list1.add(new BasicNameValuePair("rid", ridd));

                        JSONParser jn = new JSONParser();
                        JSONObject jsonObject = jn.makeHttpRequest(url, "GET", list1);
                        String res = jsonObject.getString("status");
                        //Toast.makeText(this, "ex="+ex+" qq="+qq , Toast.LENGTH_SHORT).show();
                        if (res.equalsIgnoreCase("1"))


                        {

                            Toast.makeText(Sloat_view_user.this, "Sucess", Toast.LENGTH_SHORT).show();
                            Intent it = new Intent(getApplicationContext(), Sloat_view_user.class);
                            startActivity(it);

                        } else {
                            Toast.makeText(Sloat_view_user.this, "Error", Toast.LENGTH_SHORT).show();

                        }


                    }
                }catch (Exception ex) {
                    Toast.makeText(Sloat_view_user.this,ex.getMessage().toString(), Toast.LENGTH_SHORT).show();

                }
            }
        });
        AlertDialog al=alert.create();
        al.show();


    }

    @Override
    public void onClick(View view) {
        final SharedPreferences sh=PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        final AlertDialog.Builder alert=new AlertDialog.Builder(Sloat_view_user.this);
        alert.setTitle("Add new Slot");
        alert.setMessage("Add new Slot in this parking Area");
        final EditText input = new EditText(Sloat_view_user.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        input.setHint("which type (2,4,other)");
        input.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        alert.setView(input);
        alert.setPositiveButton("Add slot", new DialogInterface.OnClickListener() {
            @Override

            public void onClick(DialogInterface dialogInterface, int ijk) {
                String url1 = "http://" + sh.getString("ip","") + "/fair_trafic/android/slot_reg.php";
                if(input.getText().toString().equals(""))
                {
                    Toast.makeText(Sloat_view_user.this, "Please Enter Type!", Toast.LENGTH_SHORT).show();

                }else {
                    String pid=View_parking_user.id[View_parking_user.pos];
                    try {
                        List<NameValuePair> list = new ArrayList<>();
                        list.add(new BasicNameValuePair("aid",pid));
                        //list.add(new BasicNameValuePair("lid", sh.getString("lid", "")));
                        list.add(new BasicNameValuePair("type", input.getText().toString()));
                        JSONParser jsonParser = new JSONParser();
                      JSONObject  jsonObject12 = jsonParser.makeHttpRequest(url1, "GET", list);
                        String response = jsonObject12.getString("status");
                        if (response.equals("1")) {
                            Toast.makeText(Sloat_view_user.this, "Successfully Added!", Toast.LENGTH_SHORT).show();
                            refresh_activity();
                        } else {
                            Toast.makeText(Sloat_view_user.this, "Error Occured!", Toast.LENGTH_SHORT).show();
                        }


                    } catch (Exception n) {
                        Toast.makeText(Sloat_view_user.this, n.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }



            }
        });
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int list) {

            }
        });
        AlertDialog al=alert.create();
        al.show();

    }
    void refresh_activity()
    {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    private static AlertDialog showDialog(final Activity act, CharSequence title, CharSequence message, CharSequence buttonYes, CharSequence buttonNo) {
        AlertDialog.Builder downloadDialog = new AlertDialog.Builder(act);
        downloadDialog.setTitle(title);
        downloadDialog.setMessage(message);
        downloadDialog.setPositiveButton(buttonYes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                Uri uri = Uri.parse("market://search?q=pname:" + "com.google.zxing.client.android");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    act.startActivity(intent);
                } catch (ActivityNotFoundException anfe) {

                }
            }
        });
        downloadDialog.setNegativeButton(buttonNo, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        return downloadDialog.show();
    }
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                String contents = intent.getStringExtra("SCAN_RESULT");
                String format = intent.getStringExtra("SCAN_RESULT_FORMAT");



                Toast toast = Toast.makeText(this,"QR Code Value"+ contents, Toast.LENGTH_LONG);
                toast.show();



               if(contents.equalsIgnoreCase(imgid))
               {
                   Toast.makeText(this, "Verification Sucessfully completed", Toast.LENGTH_SHORT).show();

                   check="Sucess";
                   Intent it=new Intent(getApplicationContext(),Qr_check.class);
                   startActivity(it);

               }
               else
               {
                   Toast.makeText(this, "invalid QR code", Toast.LENGTH_SHORT).show();

                   check="invalid QR code";
                   Intent it=new Intent(getApplicationContext(),Qr_check.class);
                   startActivity(it);


               }



            }
        }
    }
}
