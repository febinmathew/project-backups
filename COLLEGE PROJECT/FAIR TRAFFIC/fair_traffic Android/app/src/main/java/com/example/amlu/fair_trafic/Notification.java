package com.example.amlu.fair_trafic;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Notification extends AppCompatActivity {
    ListView l1;
    String[] id,not,title,timr,validity,full;
SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
getSupportActionBar().setTitle("Notifications");
        l1=(ListView)findViewById(R.id.notification);



        try {
            if (Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
        }
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        try {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            String ip = sharedPreferences.getString("ip", "");
            String lid = sharedPreferences.getString("lid", "");
            //Toast.makeText(this, "lid=" + lid, Toast.LENGTH_SHORT).show();

            //  String path = url + "jobtree/android/reg.php";
            String url = "http://" + ip + "/fair_trafic/android/notifcation.php";

            List<NameValuePair> list = new ArrayList<>();
            list.add(new BasicNameValuePair("lati",Locationservice.lati));
            list.add(new BasicNameValuePair("longi",Locationservice.logi));
       
            JSONParser jn = new JSONParser();
           JSONObject jsonObject = jn.makeHttpRequest(url, "GET", list);
            String res = jsonObject.getString("status");
            //Toast.makeText(this, "res=" + res, Toast.LENGTH_SHORT).show();
            if (res.equalsIgnoreCase("1"))

            {
                Toast.makeText(getApplicationContext(), "aaaaa", Toast.LENGTH_SHORT).show();

                JSONArray ja = new JSONArray();
                ja = jsonObject.getJSONArray("data");
                not = new String[ja.length()];

                timr = new String[ja.length()];
                id = new String[ja.length()];
                title = new String[ja.length()];

                validity = new String[ja.length()];


                full = new String[ja.length()];


                for (int i = 0; i < ja.length(); i++) {
                    JSONObject jo = new JSONObject();
                    jo = ja.getJSONObject(i);
                    not[i] = jo.getString("msg");
                    title[i] = jo.getString("title");
                    id[i] = jo.getString("id");
                    timr[i] = jo.getString("time");
                    validity[i] = jo.getString("validity");


                    full[i] = "Date and Time : " + timr[i] + "\nTitle : " + title[i] + "\nMessage : " + not[i];
                    //Toast.makeText(this,full[i], Toast.LENGTH_SHORT).show();

                }
            }
                 else
                {
                    full=new String[1];
                    full[0]="No notifications!";
                }
//                ArrayAdapter<String> adptr=new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_list_item_1,day);
//                l1.setAdapter(adptr);
                ArrayAdapter<String> add = new ArrayAdapter<>(getApplicationContext(), R.layout.custom_simple_text1, full);
                l1.setAdapter(add);


        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage().toString(), Toast.LENGTH_SHORT).show();
        }




    }
}
