package com.example.amlu.fair_trafic;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Poll_option extends AppCompatActivity implements View.OnClickListener {
    TextView t1,t2,t3,t5,t7,t6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        try {
            if(Build.VERSION.SDK_INT>9){
                StrictMode.ThreadPolicy policy=new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
        }catch (Exception e){
            Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
        }


        t1=(TextView)findViewById(R.id.textView10);

        t2=(TextView)findViewById(R.id.textView13);

        //t4=(TextView)findViewById(R.id.textView15);


        t3=(TextView)findViewById(R.id.textView14);
        t5=(TextView)findViewById(R.id.textView16);
        t6=(TextView)findViewById(R.id.textView17);
        t7=(TextView)findViewById(R.id.textView18);

       // t4.setText("Please choose any option");

        t1.setText(Poll_view.title[Poll_view.pos]);
        t2.setText(Poll_view.discri[Poll_view.pos]);
        t3.setText(Poll_view.op1[Poll_view.pos]);
        t5.setText(Poll_view.op2[Poll_view.pos]);
        t6.setText(Poll_view.op3[Poll_view.pos]);
        t7.setText(Poll_view.op4[Poll_view.pos]);

        t3.setOnClickListener(this);
        t5.setOnClickListener(this);
        t6.setOnClickListener(this);
        t7.setOnClickListener(this);



    }

    @Override
    public void onClick(View view) {
        String op="";

        if(view==t3) {
            op="1";
        }
        else if(view==t5) {
            op="2";
        }
        else if(view==t6) {
            op="3";
        }
        else if(view==t7) {
            op="4";
        }
            try {


                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

                String url = "http://" + sharedPreferences.getString("ip", "") + "/fair_trafic/android/poll_option.php";

                String lid = sharedPreferences.getString("lid", "");
                List<NameValuePair> list = new ArrayList<NameValuePair>();

                list.add(new BasicNameValuePair("pid", Poll_view.id[Poll_view.pos]));
                list.add(new BasicNameValuePair("op", op));
                list.add(new BasicNameValuePair("lid", lid));
                JSONParser jsonParser = new JSONParser();
                JSONObject jb = jsonParser.makeHttpRequest(url, "GET", list);
                String res = jb.getString("status");
                Log.d("url========", url);
                if (res.equalsIgnoreCase("1"))

                {
                    Toast.makeText(this, "Successful", Toast.LENGTH_SHORT).show();
                        finish();
                }
                else
                {
                    Toast.makeText(this, "Sorry, Already polled", Toast.LENGTH_SHORT).show();

                }
            }
            catch (Exception e)
            {
                Toast.makeText(this,e.getMessage().toString(), Toast.LENGTH_SHORT).show();

            }

        }

    }

