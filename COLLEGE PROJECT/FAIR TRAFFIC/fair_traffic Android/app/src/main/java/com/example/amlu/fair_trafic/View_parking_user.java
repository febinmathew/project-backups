package com.example.amlu.fair_trafic;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class View_parking_user extends AppCompatActivity implements AdapterView.OnItemClickListener, View.OnClickListener {
    ListView l1;
    TextView t1;
    static  public String [] id,full,name,price,user,image,status,contact;
    static public int pos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_parking_user);
        getSupportActionBar().setTitle("My Parking Areas");
        l1=(ListView)findViewById(R.id.parking1);

        try {
            if(Build.VERSION.SDK_INT>9){
                StrictMode.ThreadPolicy policy=new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
        }catch (Exception e){
            Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
        }
        t1=(TextView)findViewById(R.id.textView2);

        t1.setOnClickListener(this);

        try
        {

            SharedPreferences sharedPreferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

            String url="http://"+sharedPreferences.getString("ip","")+"/fair_trafic/android/area_view_user.php";

            String lid=sharedPreferences.getString("lid","");
            List<NameValuePair> list=new ArrayList<NameValuePair>();


            list.add(new BasicNameValuePair("lid",lid));
            JSONParser jsonParser=new JSONParser();
            JSONObject jb = jsonParser.makeHttpRequest(url,"GET",list);
            String res=jb.getString("status");
            Log.d("url========",url);
            if (res.equalsIgnoreCase("ok"))

            {
                l1.setOnItemClickListener(this);

                //Toast.makeText(getApplicationContext(), "aaaaa", Toast.LENGTH_SHORT).show();

                JSONArray ja = new JSONArray();
                ja = jb.getJSONArray("data");
                name= new String[ja.length()];

                price = new String[ja.length()];
                id = new String[ja.length()];
                image = new String[ja.length()];
                user = new String[ja.length()];
                status = new String[ja.length()];
                contact= new String[ja.length()];


                full = new String[ja.length()];


                for (int i = 0; i < ja.length(); i++) {
                    JSONObject jo = new JSONObject();
                    jo = ja.getJSONObject(i);
                    name[i] = ""+jo.getString("ar_name");
                    user[i] = jo.getString("uname");
                    price[i] = ""+jo.getString("amnt");
                    id[i] = jo.getString("id");
                    image[i] = jo.getString("img");
                    status[i] = jo.getString("status");
                    contact[i]= jo.getString("mob");




                    full[i] = "Place : " + name[i] + "\nUsername " + user[i]+"\nAmount :"+price[i] +"\nContact :"+image[i];


                }
                l1.setAdapter(new View_dtr(getApplicationContext(),name,price,user,image,status,contact));

            }
            else
            {
                full=new String[1];
                full[0]="no parking area";
                ArrayAdapter<String> add = new ArrayAdapter<>(getApplicationContext(), R.layout.custom_simple_text1, full);
                l1.setAdapter(add);
            }

               // ArrayAdapter<String> add = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, full);
                //l1.setAdapter(add);


        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage().toString(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view,final int i, long l) {
      pos=i;

        final AlertDialog.Builder alert=new AlertDialog.Builder(View_parking_user.this);
        alert.setTitle("Do you want");
        alert.setMessage("Delete or View slot");
        alert.setPositiveButton("View slot", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int j) {



                Intent it=new Intent(getApplicationContext(),Sloat_view_user.class);
                startActivity(it);



            }
        });
        alert.setNegativeButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int list) {



                try {

                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    String ip = sharedPreferences.getString("ip", "");
                    String ridd = id[i];

                    String url = "http://" + ip + "/fair_trafic/android/dlt_area.php";

                    List<NameValuePair> list1 = new ArrayList<NameValuePair>();
                    list1.add(new BasicNameValuePair("id", ridd));

                    JSONParser jn = new JSONParser();
                    JSONObject jsonObject = jn.makeHttpRequest(url, "GET", list1);
                    String res = jsonObject.getString("status");
                    //Toast.makeText(this, "ex="+ex+" qq="+qq , Toast.LENGTH_SHORT).show();
                    if (res.equalsIgnoreCase("1"))


                    {

                        Toast.makeText(View_parking_user.this, "Sucess", Toast.LENGTH_SHORT).show();
                        Intent it=new Intent(getApplicationContext(),View_parking_user.class);
                        startActivity(it);

                    } else {
                        Toast.makeText(View_parking_user.this, "Error", Toast.LENGTH_SHORT).show();

                    }


                } catch (Exception ex) {
                    Toast.makeText(View_parking_user.this,ex.getMessage().toString(), Toast.LENGTH_SHORT).show();

                }
            }
        });
        AlertDialog al=alert.create();
        al.show();



    }

    @Override
    public void onClick(View view) {
        Intent it=new Intent(getApplicationContext(),Add_parking_area.class);
        startActivity(it);
    }
}
