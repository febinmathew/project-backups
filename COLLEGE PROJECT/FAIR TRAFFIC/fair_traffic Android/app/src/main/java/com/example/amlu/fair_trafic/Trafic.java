package com.example.amlu.fair_trafic;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Trafic extends AppCompatActivity {
    ListView l1;
    String [] id,full,place,traficcc,time,km;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trafic);
        getSupportActionBar().setTitle("Traffic Status");
        l1=(ListView)findViewById(R.id.traficc);




        try {
            if(Build.VERSION.SDK_INT>9){
                StrictMode.ThreadPolicy policy=new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
        }catch (Exception e){
            Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
        }

        try
        {
            String lati=Locationservice.lati;
            String longi=Locationservice.logi;
            if(lati=="" || longi=="" || lati==null || longi==null)
            {
                throw new Exception("Could not locate your position ");
            }
            SharedPreferences sharedPreferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

            String url="http://"+sharedPreferences.getString("ip","")+"/fair_trafic/android/trafic.php";
            //String url = "http://+"+ip+"jobtree/android/edit.php";

            String lid=sharedPreferences.getString("lid","");
            List<NameValuePair> list=new ArrayList<NameValuePair>();

            list.add(new BasicNameValuePair("lati",lati));
            list.add(new BasicNameValuePair("longi",longi));
            list.add(new BasicNameValuePair("lid",lid));
            JSONParser jsonParser=new JSONParser();
           JSONObject jb = jsonParser.makeHttpRequest(url,"GET",list);
            String res=jb.getString("status");
            Log.d("url========",url);
            if (res.equalsIgnoreCase("1"))

            {
                Toast.makeText(getApplicationContext(), "aaaaa", Toast.LENGTH_SHORT).show();

                JSONArray ja = new JSONArray();
                ja = jb.getJSONArray("data");
                 place= new String[ja.length()];

                traficcc = new String[ja.length()];
                time = new String[ja.length()];
                id = new String[ja.length()];
                km = new String[ja.length()];


                full = new String[ja.length()];


                for (int i = 0; i < ja.length(); i++) {
                    JSONObject jo = new JSONObject();
                    jo = ja.getJSONObject(i);



                    traficcc[i] = jo.getString("size");

                    if(Integer.parseInt(traficcc[i])>6)
                    {
                        traficcc[i]="High";
                    }
                    else if(Integer.parseInt(traficcc[i])>3)
                    {
                        traficcc[i]="Medium";
                    }
                    else
                    {
                        traficcc[i]="Low";
                    }
                    place[i] = jo.getString("place");
                    id[i] = jo.getString("id");
                    time[i] = jo.getString("time");
                    km[i] = jo.getString("distance");
                    km[i]=new DecimalFormat("0.00").format(Float.parseFloat( km[i]))+"";

                    full[i] = "Place : " + place[i] + "\nTraffic status : " + traficcc[i]+"\nDistance :"+km[i]+" km";
                    //Toast.makeText(this,full[i], Toast.LENGTH_SHORT).show();

                }


            }
            else
            {
                full=new String[1];
                full[0]="No Traffic status available!";
            }
            ArrayAdapter<String> add = new ArrayAdapter<>(getApplicationContext(), R.layout.custom_simple_text1, full);
            l1.setAdapter(add);
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage().toString(), Toast.LENGTH_SHORT).show();
        }







    }
}
