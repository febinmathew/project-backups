package com.example.amlu.fair_trafic;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

public class User_home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Home");
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.user_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent=new Intent(this,SettingsActivity.class);

            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_cmp) {
            Intent it=new Intent(getApplicationContext(),Send_Complaint.class);
            startActivity(it);

        } else if (id == R.id.nav_feedback) {
            Intent it=new Intent(getApplicationContext(),Feedback.class);
            startActivity(it);

        } else if (id == R.id.nav_notification) {
            Intent it=new Intent(getApplicationContext(),Notification.class);
            startActivity(it);

        } else if (id == R.id.nav_Parking) {
            Intent it=new Intent(getApplicationContext(),View_parking.class);
            startActivity(it);

        } else if (id == R.id.nav_jurney) {
            Intent it=new Intent(getApplicationContext(),Shake.class);
            startActivity(it);

        }
        else if (id == R.id.nav_logout) {
            Intent it=new Intent(getApplicationContext(),Home.class);
            startActivity(it);

        }
        else if (id == R.id.nav_poll) {
            Intent it=new Intent(getApplicationContext(),Poll_view.class);
            startActivity(it);

        }
        else if (id == R.id.nav_Trafic) {
            Intent it=new Intent(getApplicationContext(),Trafic.class);
            startActivity(it);

        }
        else if (id == R.id.nav_change_pass) {
            Intent it=new Intent(getApplicationContext(),Change_pass.class);
            startActivity(it);

        }
        else if (id == R.id.nav_accident) {
            Intent it=new Intent(getApplicationContext(),Accident_view.class);
            startActivity(it);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
