package com.example.amlu.fair_trafic;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Poll_view extends AppCompatActivity implements AdapterView.OnItemClickListener {
    ListView l1;

    static public String [] id,full,discri,title,amount,validity,priority,op1,op2,op3,op4;
    static  public int pos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poll_view);
        getSupportActionBar().setTitle("Polls");
        l1=(ListView)findViewById(R.id.poll);


        try {
            if(Build.VERSION.SDK_INT>9){
                StrictMode.ThreadPolicy policy=new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
        }catch (Exception e){
            Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
        }

        try
        {
            String langi=Locationservice.lati;
            String longi=Locationservice.logi;
            SharedPreferences sharedPreferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

            String url="http://"+sharedPreferences.getString("ip","")+"/fair_trafic/android/poll_view.php";

            String lid=sharedPreferences.getString("lid","");
            List<NameValuePair> list=new ArrayList<NameValuePair>();

            list.add(new BasicNameValuePair("laty",langi));
            list.add(new BasicNameValuePair("longi",longi));
            list.add(new BasicNameValuePair("lid",lid));
            JSONParser jsonParser=new JSONParser();
            JSONObject jb = jsonParser.makeHttpRequest(url,"GET",list);
            String res=jb.getString("status");
            Log.d("url========",url);
            if (res.equalsIgnoreCase("1"))

            {
              //  Toast.makeText(getApplicationContext(), "aaaaa", Toast.LENGTH_SHORT).show();

                l1.setOnItemClickListener(this);

                JSONArray ja = new JSONArray();
                ja = jb.getJSONArray("data");
                discri = new String[ja.length()];

                title = new String[ja.length()];
                id = new String[ja.length()];
                amount = new String[ja.length()];
                validity = new String[ja.length()];
                priority = new String[ja.length()];
                op1 = new String[ja.length()];
                op2 = new String[ja.length()];
                op3 = new String[ja.length()];
                op4 = new String[ja.length()];


                full = new String[ja.length()];


                for (int i = 0; i < ja.length(); i++) {
                    JSONObject jo = new JSONObject();
                    jo = ja.getJSONObject(i);
                    discri[i] = jo.getString("msg");
                    title[i] = jo.getString("title");
                    id[i] = jo.getString("id");
                    validity[i] = jo.getString("validity");
                    priority[i] = jo.getString("priority");
                    op1[i] = jo.getString("op1");
                    op2[i] = jo.getString("op2");
                    op3[i] = jo.getString("op3");
                    op4[i] = jo.getString("op4");



                    full[i] = "Title : " + title[i] + "\nDescription : " + discri[i] + "\nValidity : " + validity[i];


                }
            }
                 else
                {
                    full=new String[1];
                    full[0]="no data";
                }
                ArrayAdapter<String> add = new ArrayAdapter<>(getApplicationContext(), R.layout.custom_simple_text1, full);
                l1.setAdapter(add);


        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage().toString(), Toast.LENGTH_SHORT).show();
        }




    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view,final int i, long l) {
        pos=i;
        Intent it=new Intent(getApplicationContext(),Poll_option.class);
        startActivity(it);
    }



}
