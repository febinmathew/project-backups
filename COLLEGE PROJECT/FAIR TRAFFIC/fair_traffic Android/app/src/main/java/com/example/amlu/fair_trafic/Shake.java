package com.example.amlu.fair_trafic;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;


import android.media.MediaPlayer;
import android.speech.tts.TextToSpeech;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.SensorListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

@SuppressWarnings("deprecation")
@TargetApi(Build.VERSION_CODES.GINGERBREAD)
public class Shake extends AppCompatActivity implements SensorListener,TextToSpeech.OnInitListener {
    // For shake motion detection.
    private SensorManager sensorMgr;
    private long lastUpdate = -1;
    private float x, y, z;
    private float last_x, last_y, last_z;
    private static final int SHAKE_THRESHOLD = 3500;
	private static final int ACCIDENT_THRESHOLD = 6400;
    String phoneid="";
    private static String tmpplc="";
    Handler handler=new Handler();
    JSONObject jsonObject=new JSONObject();
    JSONParser jsonParser=new JSONParser();
    String url="";
    SharedPreferences sp;
    TextToSpeech ttobj;
	long lastmsgTime=0;
    private TextToSpeech textToSpeech;
//    
	TextView ta;
//    Button b,b1;
//    Button bclr;
    ImageView img;
    String uid;

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);	
	    setContentView(R.layout.activity_shakeactivity);
	    getSupportActionBar().setTitle("Accident detection");
	    sp=PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	    String ip=sp.getString("ip","");
        url="http://"+ip+"/fair_trafic/android/shake.php";
		SharedPreferences sp= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	    
	    if(Build.VERSION.SDK_INT>9){
	    	StrictMode.ThreadPolicy policy=new StrictMode.ThreadPolicy.Builder().permitAll().build();
	    	StrictMode.setThreadPolicy(policy);
	    }
	    
	    textToSpeech = new TextToSpeech(this, this);
	    
	    handler.post(AlertFinder);	    
	    
	    ta=(TextView)findViewById(R.id.textView1);

	    img=(ImageView)findViewById(R.id.imageView1);
	    //img.setVisibility(View.INVISIBLE);
	  
	   
	    
	    // start motion detection
	    sensorMgr = (SensorManager) getSystemService(SENSOR_SERVICE);
	    boolean accelSupported = sensorMgr.registerListener(this,SensorManager.SENSOR_ACCELEROMETER,SensorManager.SENSOR_DELAY_GAME);
	
	    if (!accelSupported) {
	        // on accelerometer on this device
	        sensorMgr.unregisterListener((SensorListener) this,
	                SensorManager.SENSOR_ACCELEROMETER);
	    }
	    
//	    TelephonyManager telephonyManager  = (TelephonyManager)getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
//	    phoneid=telephonyManager.getDeviceId().toString();
	    Log.d("phoneid", phoneid);	
	   // Toast.makeText(getApplicationContext(), phoneid, Toast.LENGTH_LONG).show();

//	    Intent in=new Intent(getApplicationContext(),LocationService.class);
//	    startService(in);
    }
    
String tmplat="0.0",tmplon="0.0";    

public Runnable AlertFinder = new Runnable(){
		
		public void run()
		{
				handler.postDelayed(AlertFinder,10000);
		}
};
 
    protected void onPause() {
    if (sensorMgr != null) {
        sensorMgr.unregisterListener(this,
                SensorManager.SENSOR_ACCELEROMETER);
        sensorMgr = null;
        }
    super.onPause();
    }

    public void onAccuracyChanged(int arg0, int arg1) {
    // TODO Auto-generated method stub
    }

    public void onSensorChanged(int sensor, float[] values) {
    if (sensor == SensorManager.SENSOR_ACCELEROMETER) {
        long curTime = System.currentTimeMillis();
        // only allow one update every 100ms.
        if ((curTime - lastUpdate) > 100) {
        long diffTime = (curTime - lastUpdate);
        lastUpdate = curTime;

        x = values[SensorManager.DATA_X];
        y = values[SensorManager.DATA_Y];
        z = values[SensorManager.DATA_Z];

        if(Round(x,4)>10.0000){
            Log.d("sensor", "X Right axis: " + x);
       //     Toast.makeText(this, "Right shake detected", Toast.LENGTH_SHORT).show();
        }
        else if(Round(y,4)>10.0000){
            Log.d("sensor", "X Right axis: " + x);
        //    Toast.makeText(this, "Top shake detected", Toast.LENGTH_SHORT).show();
        }
        else if(Round(y,4)>-10.0000){
            Log.d("sensor", "X Right axis: " + x);
       //     Toast.makeText(this, "Bottom shake detected", Toast.LENGTH_SHORT).show();
        }
        else if(Round(x,4)<-10.0000){
            Log.d("sensor", "X Left axis: " + x);
        //    Toast.makeText(this, "Left shake detected", Toast.LENGTH_SHORT).show();
        }

        float speed = Math.abs(x+y+z - last_x - last_y - last_z) / diffTime * 10000;

        // Log.d("sensor", "diff: " + diffTime + " - speed: " + speed);

			if (speed > ACCIDENT_THRESHOLD && lastmsgTime<(System.currentTimeMillis()-4000))
       		 {

				 lastmsgTime = System.currentTimeMillis();
            convertTextToSpeech("Accident Confirmed. Now sending alert to the Traffic and your emergency contact. Don't panic the help is on your way.");

				 String messageToSend = "Help! I have with an accident! Location : "+Locationservice.place
						 +", Latitude : "+Locationservice.lati+", Longitude : "+Locationservice.logi;
				 String number = sp.getString("emergency_contact","");
				 if(!number.equals(""))
					 SmsManager.getDefault().sendTextMessage(number, null, messageToSend, null,null);
				 else
					 Toast.makeText(this, "No emergency contact defined.", Toast.LENGTH_SHORT).show();


            Log.d("sensor", "Shake detected w/ speed: " + speed);
            cn=0;
           // handler.post(rn);
   	
        	//aaaaaaaaaa
            try
			{
			String lid=sp.getString("lid","");
			String place=Locationservice.place;
			String lati=Locationservice.lati;
			String logi=Locationservice.logi;
			List<NameValuePair> para=new ArrayList<NameValuePair>();
			para.add(new BasicNameValuePair("lid",lid));
			para.add(new BasicNameValuePair("place", place));
			para.add(new BasicNameValuePair("lati",lati));
			para.add(new BasicNameValuePair("logi",logi));
			jsonObject=jsonParser.makeHttpRequest(url,"GET",para);
			String res=jsonObject.getString("success");






				if(res.equalsIgnoreCase("ok"))
				{
					Toast.makeText(this, "Accident confirmed", Toast.LENGTH_SHORT).show();
				}
			/*
			SharedPreferences sh=PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
			String number=sh.getString("phone", "");
			if(number.length()>=10){
				SmsManager sms=SmsManager.getDefault();
				sms.sendTextMessage(number, null, "Accident Occured", null, null);
			}
			if(res.equalsIgnoreCase("invalid"))
			{
				Toast.makeText(getApplicationContext(), "Not report",Toast.LENGTH_LONG).show();
			}
			else if(res.equalsIgnoreCase("valid"))
			{
				Toast.makeText(getApplicationContext(), "Success",Toast.LENGTH_LONG).show();
			}
			else
			{
				Toast.makeText(getApplicationContext(), "Some problems",Toast.LENGTH_LONG).show();
			}
			*/
			}
			catch(Exception ex)
			{
				Toast.makeText(getApplicationContext(),ex.toString(),Toast.LENGTH_LONG).show();
			}

    		
        }
        else if(speed > SHAKE_THRESHOLD && lastmsgTime<(System.currentTimeMillis()-4000))
		{
			lastmsgTime = System.currentTimeMillis();
			convertTextToSpeech("Avoid rash driving. Please drive carefully");
		}

        ta.setText(speed+"");
        
        last_x = x;
        last_y = y;
        last_z = z;
        }
    }
    }
static int cn=0;
    Runnable rn=new Runnable() {		
		@Override
		public void run() {
			if(cn<10){
				img.setBackgroundResource(R.drawable.car);
	            img.setVisibility(View.VISIBLE); 	          
			}
			else{
				img.setVisibility(View.INVISIBLE);
			}
			cn++;
			handler.postDelayed(rn, 1000);
		}
	};
    
    public static float Round(float Rval, int Rpl) {
    float p = (float)Math.pow(10,Rpl);
    Rval = Rval * p;
    float tmp = Math.round(Rval);
    return (float)tmp/p;
    }

 
	
	private void convertTextToSpeech(String text) {
		
		if (null == text || "".equals(text))
		{
			text = "Hi there, I will be assisting you in this journey.";
		}
		//text="Please Drive carefully      Safe Drive Pleace    and        control your speed";
		textToSpeech.setPitch(.2f);
		textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null);
	}

	
	
	@Override
	public void onInit(int status) {
		if (status == TextToSpeech.SUCCESS) {
			int result = textToSpeech.setLanguage(Locale.US);
			if (result == TextToSpeech.LANG_MISSING_DATA
					|| result == TextToSpeech.LANG_NOT_SUPPORTED) {
				Log.e("error", "This Language is not supported");
			} else {
				convertTextToSpeech("Starting your journey. Stay alert on road and drive safe. Good luck!");
			}
		} else {
			Log.e("error", "Initilization Failed!");
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		handler.removeCallbacks(AlertFinder);
		
		//Intent j=new Intent(getApplicationContext(),User_home.class);
		//stopService(j);
		//Toast.makeText(getApplicationContext(), "stopped......", Toast.LENGTH_LONG).show();
		
	}







}
