package com.example.amlu.fair_trafic;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static android.R.attr.data;

public class Register extends AppCompatActivity implements View.OnClickListener {



    EditText et1, et2, et3, et4, et5, et6, et7,et8,et9,et10;
    Button b1;
  //  TextView t1;


    RadioButton r1,r2;


    JSONObject jsonObject1=new JSONObject();
    JSONArray ja3 = new JSONArray();


    JSONObject jsonObject = new JSONObject();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().setTitle("New Registration");

        try {
            if (Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
        }







        b1 = (Button) findViewById(R.id.button4);

        et1 = (EditText) findViewById(R.id.editText8);

        et2 = (EditText) findViewById(R.id.editText13);
        et3 = (EditText) findViewById(R.id.editText141);

        et8= (EditText) findViewById(R.id.editText142);

        et9 = (EditText) findViewById(R.id.editText143);
        et10 = (EditText) findViewById(R.id.editText144);



        et4 = (EditText) findViewById(R.id.editText23);
        et5 = (EditText) findViewById(R.id.editText24);
        et6 = (EditText) findViewById(R.id.editText25);
        et7 = (EditText) findViewById(R.id.editText145);




        b1.setOnClickListener(this);






//        t1.setOnClickListener(this);









        }

    @Override
    public void onClick(View view) {

         if (view == b1)
        {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            String ip = sharedPreferences.getString("ip", "");


            String url = "http://"+ip+"/alljobinfo/android/reg.php";







            String uname = et1.getText().toString();
            String pass = et5.getText().toString();
            String cnfirm = et6.getText().toString();
            String pin  = et7.getText().toString();
            String email = et4.getText().toString();
            String phonenumber = et2.getText().toString();
            String adress1 = et3.getText().toString();
            String adress2 = et8.getText().toString();
            String state = et9.getText().toString();
            String city = et10.getText().toString();





            if (et1.getText().equals("")) {
                et1.setError("*****");
                et1.setFocusable(true);
            } else if (et2.getText().equals("")) {
                et2.setError("*****");
                et2.setFocusable(true);
            } else if (et3.getText().equals("")) {
                et3.setError("*****");
                et3.setFocusable(true);
            } else if (et4.getText().equals("")) {
                et4.setError("*****");
                et4.setFocusable(true);
            } else if (et5.getText().equals("")) {
                et5.setError("*****");
                et5.setFocusable(true);
            } else if (et6.getText().equals("")) {
                et6.setError("*****");
                et6.setFocusable(true);
            } else if (et7.getText().equals("")) {
                et7.setError("*****");
                et7.setFocusable(true);

            }
            else if (et8.getText().equals("")) {
                et8.setError("*****");
                et8.setFocusable(true);
            } else if (et9.getText().equals("")) {
                et9.setError("*****");
                et9.setFocusable(true);
            } else if (et10.getText().equals("")) {
                et10.setError("*****");
                et10.setFocusable(true);

            }

            else if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                et4.setError("invalide email");
                et4.setFocusable(true);
            }
            else if(pass.length()<5)
            {
                et4.setError("password too short");
                et4.setFocusable(true);
            }
            else if(!pass.equalsIgnoreCase(cnfirm))
            {
                et5.setError("password didint match");
                et5.setFocusable(true);

            }

            else {



                try {
                    String laty=Locationservice.lati;
                    String longi=Locationservice.logi;


                     url = "http://" + ip+"/fair_trafic/android/register.php";
                    Log.d("url===========", url);
                    List<NameValuePair> list = new ArrayList<NameValuePair>();



                    list.add(new BasicNameValuePair("uname", uname));
                    list.add(new BasicNameValuePair("pass", pass));
                    list.add(new BasicNameValuePair("pin", pin));
                    list.add(new BasicNameValuePair("email", email));
                    list.add(new BasicNameValuePair("mob", phonenumber));
                    list.add(new BasicNameValuePair("adress1", adress1));
                    list.add(new BasicNameValuePair("adress2", adress2));
                    list.add(new BasicNameValuePair("state", state));
                    list.add(new BasicNameValuePair("city", city));
                    list.add(new BasicNameValuePair("laty", laty));
                    list.add(new BasicNameValuePair("longi", longi));

                    JSONParser jsonParser = new JSONParser();

                    JSONObject jb = jsonParser.makeHttpRequest(url, "GET", list);
                    String res = jb.getString("status");
                    if (res.equalsIgnoreCase("ok")) {

                        Toast.makeText(Register.this, "Success...!!", Toast.LENGTH_SHORT).show();
                        finish();


                    } else {
                        //Toast.makeText(this, "Invalid username or password", Toast.LENGTH_SHORT).show();
                        Toast.makeText(Register.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {

                    Toast.makeText(Register.this, e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                }




            }



        }
    }







}