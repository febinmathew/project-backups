package com.example.amlu.fair_trafic;

import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

public class Add_parking_area extends AppCompatActivity implements View.OnClickListener {
    EditText e1, e2, e3, e4;
    TextView t1;
    Button b1;
    ImageView img;
    byte[] bitmapdata;
    String base64img;
    public String encodedImage;
    private static final int CAMERA_PIC_REQUEST = 2500;
    String photoName;
    byte[] photoBytes;
    String data;
    Uri mUriPhotoTaken;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_parking_area);
        e1 = (EditText) findViewById(R.id.editText3);
        e2 = (EditText) findViewById(R.id.editText4);
        e3 = (EditText) findViewById(R.id.editText5);
        e4 = (EditText) findViewById(R.id.editText6);
        img = (ImageView) findViewById(R.id.imageView3);
        t1 = (TextView) findViewById(R.id.textView4);
        b1 = (Button) findViewById(R.id.buttonn);
        t1.setVisibility(View.INVISIBLE);
        img.setOnClickListener(this);
        b1.setOnClickListener(this);
        try {
            if (Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
           }} catch (Exception e) {
            Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();}
    }
    @Override
    public void onClick(View view) {
        if (view == img) {
            e4.setText(Locationservice.lati);
            e2.setText(Locationservice.logi);
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST);
        } else {
            if (view == b1) {
                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                String ip = sp.getString("ip", "");
                String url = "http://" + ip + "/fair_trafic/android/area_reg.php";
                if (e1.getText().equals("")) {
                    e1.setError("*****");
                    e1.setFocusable(true);
                } else if (e2.getText().equals("")) {
                    e2.setError("*****");
                    e2.setFocusable(true);
                } else if (e3.getText().equals("")) {
                    e3.setError("*****");
                    e3.setFocusable(true);
                } else if (e4.getText().equals("")) {
                    e4.setError("*****");
                    e4.setFocusable(true);
                } else {
                    try {
                        String name = e1.getText().toString();
                        String longi = e2.getText().toString();
                        String lati = e4.getText().toString();
                        String amnt = e3.getText().toString();
                        String lid = sp.getString("lid", "");
                        Log.d("url=====", url);
                        FileUpload client = new FileUpload(url);
                        client.connectForMultipart();
                        client.addFormPart("name", name);
                        client.addFormPart("lati", lati);
                        client.addFormPart("lid", lid);
                        client.addFormPart("longi", longi);
                        client.addFormPart("amount", amnt);
                        client.addFormPart("photo", encodedImage);
                        client.finishMultipart();
                        data = client.getResponse();
                       // Toast.makeText(this, data, Toast.LENGTH_SHORT).show();
                        Log.d("data==========", data);
                        JSONObject js = new JSONObject(data);
                        if (js.getString("status").equalsIgnoreCase("ok")) {
                            Toast.makeText(getApplication(), "Uploaded Sucessfully", Toast.LENGTH_LONG).show();
                            finish();
                        } else {
                            Toast.makeText(this, "file not uploades", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception cv) {
                        Toast.makeText(getApplicationContext(), cv.getMessage(), Toast.LENGTH_LONG).show();

                    }
                }}}
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_PIC_REQUEST)
        {
            if (resultCode == RESULT_OK)
            {
                Uri uri = data.getData();
                try {
                    if (uri == null) {
                        //Toast.makeText(this, "nullllll", Toast.LENGTH_SHORT).show();
                    }
                    Toast.makeText(this, "aaaaa", Toast.LENGTH_SHORT).show();
                    Bitmap image = (Bitmap) data.getExtras().get("data");
                    img.setImageBitmap(image);
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    image.compress(Bitmap.CompressFormat.JPEG, 100, bos);
                    bitmapdata = bos.toByteArray();
                    encodedImage = Base64.encodeToString(bitmapdata, Base64.NO_WRAP);
                } catch (Exception e) {
                    Toast.makeText(this, e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                }}
        }}
}