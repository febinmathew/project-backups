package com.example.amlu.fair_trafic;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Change_pass extends AppCompatActivity implements View.OnClickListener {
    EditText e1,e2,e3;
    Button b1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pass);

        b1=(Button)findViewById(R.id.buttonc);
        e1=(EditText)findViewById(R.id.editText7);

        e2=(EditText)findViewById(R.id.editText10);
        e3=(EditText)findViewById(R.id.editText11);


        b1.setOnClickListener(this);

        try {
            if(Build.VERSION.SDK_INT>9){
                StrictMode.ThreadPolicy policy=new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
        }catch (Exception e){
            Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onClick(View view) {

        if (e1.getText().equals("")) {
            e1.setError("*****");
            e1.setFocusable(true);
        } else if (e2.getText().equals("")) {
            e2.setError("*****");
            e2.setFocusable(true);
        } else if (e3.getText().equals("")) {
            e3.setError("*****");
            e3.setFocusable(true);
        }
        else if(!e2.getText().toString().equalsIgnoreCase(e3.getText().toString()))
        {
            e3.setError("password mismatch");
            e3.setFocusable(true);

        }
        else
        {
            String old=e1.getText().toString();
            String new1=e2.getText().toString();
            try {
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

                String url = "http://" + sharedPreferences.getString("ip", "") + "/fair_trafic/android/change_pass.php";
                //String url = "http://+"+ip+"jobtree/android/edit.php";

                String lid = sharedPreferences.getString("lid", "");
                List<NameValuePair> list = new ArrayList<NameValuePair>();

                list.add(new BasicNameValuePair("old",old ));
                list.add(new BasicNameValuePair("new1",new1 ));
                list.add(new BasicNameValuePair("lid", lid));
                JSONParser jsonParser = new JSONParser();
                JSONObject jb = jsonParser.makeHttpRequest(url, "GET", list);
                String res = jb.getString("status");
                Log.d("url========", url);
                if (res.equalsIgnoreCase("ok")) {
                    Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, res, Toast.LENGTH_SHORT).show();
                }


            } catch (Exception v) {
                Toast.makeText(this, v.getMessage().toString(), Toast.LENGTH_SHORT).show();

            }

        }

    }
}
