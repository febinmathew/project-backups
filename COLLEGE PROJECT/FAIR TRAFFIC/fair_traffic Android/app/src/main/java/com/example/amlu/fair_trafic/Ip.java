package com.example.amlu.fair_trafic;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.sax.StartElementListener;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Ip extends Activity {
	EditText e1;
	Button b1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ip);
		e1=(EditText)findViewById(R.id.editText1);
		b1=(Button)findViewById(R.id.button1);
		SharedPreferences sp= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		e1.setText(sp.getString("ip",""));
		
		b1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

if(e1.getText().toString().equalsIgnoreCase(""))
{
	e1.setError("****");
}
else {

	String ip = e1.getText().toString();

	SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	SharedPreferences.Editor edd = sp.edit();
	edd.putString("ip", ip);
	edd.commit();


	Intent i = new Intent(getApplicationContext(), Home.class);

	startActivity(i);
}		}
		});
	}

	@Override
	public void onBackPressed() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder.setTitle("Exit Application?");
		alertDialogBuilder
				.setMessage("Click yes to exit!")
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								moveTaskToBack(true);
								android.os.Process.killProcess(android.os.Process.myPid());
								System.exit(1);
							}
						})

				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						dialog.cancel();
					}
				});

		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}
}
