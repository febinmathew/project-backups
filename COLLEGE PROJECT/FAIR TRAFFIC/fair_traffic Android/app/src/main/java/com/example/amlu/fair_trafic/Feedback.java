package com.example.amlu.fair_trafic;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class Feedback extends AppCompatActivity implements View.OnClickListener {
    EditText et1;
    RatingBar rb1;
    Button b1;
    JSONObject jb=new JSONObject();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        getSupportActionBar().setTitle("Submit Feedback");

        SharedPreferences sh= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String aa=sh.getString("re","");

        Toast.makeText(this, aa, Toast.LENGTH_SHORT).show();

        et1=(EditText)findViewById(R.id.editText27);
        rb1=(RatingBar)findViewById(R.id.ratingBar);
        b1=(Button)findViewById(R.id.button12);

        b1.setOnClickListener(this);

        try {
            if(Build.VERSION.SDK_INT>9){
                StrictMode.ThreadPolicy policy=new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
        }catch (Exception e){
            Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public void onClick(View view) {
        if(view==b1)
        {
            String feedback,rating;

            feedback=et1.getText().toString();
            if(et1.getText().toString().equalsIgnoreCase(""))
            {

          et1.setError("*****");
            }
            else if(rb1.getRating()==0)
            {
                Toast.makeText(this, "please choose rating", Toast.LENGTH_SHORT).show();
            }
            else {

                rating = rb1.getRating() + "";
                try {
                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

                    String url = "http://" + sharedPreferences.getString("ip", "") + "/fair_trafic/android/feedback.php";
                    //String url = "http://+"+ip+"jobtree/android/edit.php";

                    String lid = sharedPreferences.getString("lid", "");
                    List<NameValuePair> list = new ArrayList<NameValuePair>();

                    list.add(new BasicNameValuePair("feed", feedback));
                    list.add(new BasicNameValuePair("rate", rating));
                    list.add(new BasicNameValuePair("lid", lid));
                    JSONParser jsonParser = new JSONParser();
                    jb = jsonParser.makeHttpRequest(url, "GET", list);
                    String res = jb.getString("status");
                    Log.d("url========", url);
                    if (res.equalsIgnoreCase("ok")) {
                        Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(this, "Failure", Toast.LENGTH_SHORT).show();
                    }


                } catch (Exception v) {
                    Toast.makeText(this, v.getMessage().toString(), Toast.LENGTH_SHORT).show();

                }
            }
        }
    }
    @Override
    public void onBackPressed()
    {
        /*Intent it=new Intent(getApplicationContext(),User_home.class);
        startActivity(it);*/
        super.onBackPressed();

    }
}
