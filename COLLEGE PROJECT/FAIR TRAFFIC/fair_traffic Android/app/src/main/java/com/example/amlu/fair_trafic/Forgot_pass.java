package com.example.amlu.fair_trafic;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Forgot_pass extends AppCompatActivity implements View.OnClickListener {
    Button b1;

    EditText e1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pass);
        getSupportActionBar().setTitle("Forgot Password?");
        b1=(Button)findViewById(R.id.button77);
        e1=(EditText)findViewById(R.id.editText9);

        try {
            if (Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
        }


        b1.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        String email=e1.getText().toString();

        if(e1.getText().equals(""))
        {
            e1.setError("Enter mail");

            e1.setFocusable(true);

        }
        else {
            SharedPreferences sp= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            String ip=sp.getString("ip","");
            try {



                String url = "http://" + ip + "/fair_trafic/android/code_mail.php";
                Log.d("url===========", url);
                List<NameValuePair> list = new ArrayList<NameValuePair>();

                list.add(new BasicNameValuePair("uname", email));

                JSONParser jsonParser = new JSONParser();

                JSONObject jb = jsonParser.makeHttpRequest(url, "GET", list);
                String res = jb.getString("status");
                if (res.equalsIgnoreCase("ok")) {

                    Toast.makeText(Forgot_pass.this, "Success... yor password is sent to mail!!", Toast.LENGTH_SHORT).show();

                    //Toast.makeText(this, "aaaaa", Toast.LENGTH_SHORT).show();
                    Intent ho = new Intent(getApplicationContext(), Login.class);
                    startActivity(ho);

                } else {
                    //Toast.makeText(this, "Invalid username or password", Toast.LENGTH_SHORT).show();
                    Toast.makeText(Forgot_pass.this, "error", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {

                Toast.makeText(Forgot_pass.this, e.getMessage().toString(), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
