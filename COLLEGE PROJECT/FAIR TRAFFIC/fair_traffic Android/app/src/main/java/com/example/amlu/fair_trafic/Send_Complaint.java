package com.example.amlu.fair_trafic;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class Send_Complaint extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
    EditText et1;
    Button b1;
    ListView lv1;
    ImageButton im1;
    String asd="pending";
    JSONObject jsonObject = new JSONObject();
    String[] date, cmp, id, full, reply,ff;

    String photoName;
    byte[] photoBytes;

    String data="";
    int capture=100;
    ImageView img;
    String encodedImage;

    private static final int CAMERA_PIC_REQUEST = 2500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send__complaint);
        getSupportActionBar().setTitle("Complaints");
        et1 = (EditText) findViewById(R.id.editText);
        im1 = (ImageButton) findViewById(R.id.imageButton);
        b1 = (Button) findViewById(R.id.button11);
        lv1 = (ListView) findViewById(R.id.cmp);
        img=(ImageView)findViewById(R.id.imageView4);

        lv1.setOnItemClickListener(this);
        b1.setOnClickListener(this);
        im1.setOnClickListener(this);


        try {
            if (Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
        }


        // ArrayAdapter<String> ad=new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_list_item_1,day);
        //l1.setAdapter(ad);

        try {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            String ip = sharedPreferences.getString("ip", "");
            String lid = sharedPreferences.getString("lid", "");
            //Toast.makeText(this, "lid=" + lid, Toast.LENGTH_SHORT).show();

            //  String path = url + "jobtree/android/reg.php";
            String url = "http://" + ip + "/fair_trafic/android/cmp_view.php";

            List<NameValuePair> list = new ArrayList<>();
            list.add(new BasicNameValuePair("lid", lid));
            JSONParser jn = new JSONParser();
            jsonObject = jn.makeHttpRequest(url, "GET", list);
            String res = jsonObject.getString("status");
            //Toast.makeText(this, "res=" + res, Toast.LENGTH_SHORT).show();
            if (res.equalsIgnoreCase("1"))

            {
                //Toast.makeText(getApplicationContext(), "aaaaa", Toast.LENGTH_SHORT).show();

                JSONArray ja = new JSONArray();
                ja = jsonObject.getJSONArray("data");
                cmp = new String[ja.length()];

                date = new String[ja.length()];
                id = new String[ja.length()];
                reply = new String[ja.length()];

                ff = new String[ja.length()];


                full = new String[ja.length()];


                for (int i = 0; i < ja.length(); i++) {
                    JSONObject jo = new JSONObject();
                    jo = ja.getJSONObject(i);
                    cmp[i] = jo.getString("cmp");
                    date[i] = jo.getString("time");
                    id[i] = jo.getString("id");

                    ff[i] = jo.getString("file");

                    reply[i] = jo.getString("status");


                    full[i] = "Complaint : " + cmp[i] + "\nStatus : " + reply[i]+"\nDate & Time: " + date[i];
                    //full[i]= Html.fromHtml( full[i]).toString();
                    //Toast.makeText(this,full[i], Toast.LENGTH_SHORT).show();

                }

                ArrayAdapter<String> add = new ArrayAdapter<>(getApplicationContext(), R.layout.custom_simple_text1, full);
                lv1.setAdapter(add);

            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage().toString(), Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public void onClick(View view) {
        if (view == b1) {
            if (et1.getText().toString().equalsIgnoreCase("")) {
                et1.setError("Enter complaint");
                et1.setFocusable(true);
            } else {

                    try {
                        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        String ip = sharedPreferences.getString("ip", "");
                        String lid = sharedPreferences.getString("lid", "");

                       // Toast.makeText(this, "aaaass", Toast.LENGTH_SHORT).show();

                        String url1 = "http://" + ip + "/fair_trafic/android/cmp_reg.php";

                        Toast.makeText(Send_Complaint.this, "ll", Toast.LENGTH_SHORT).show();
                        Log.d("url=====", url1);
                        FileUpload client = new FileUpload(url1);
                        client.connectForMultipart();

                        client.addFormPart("lid", lid);
                        client.addFormPart("cmp", et1.getText().toString());
                        client.addFormPart("laty", Locationservice.lati);
                        client.addFormPart("longi", Locationservice.logi);
                        client.addFormPart("asd", asd);
                 if(asd.equalsIgnoreCase("ok")) {
              client.addFilePart("file", photoName, photoBytes);
                }
                        client.finishMultipart();
                        data = client.getResponse();
                        data = client.getResponse();
                        JSONObject js=new JSONObject(data);
                        if(js.getString("status").equalsIgnoreCase("ok")) {
                            Toast.makeText(this, "Complaint Successful", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            Toast.makeText(this, "Something went wrong!", Toast.LENGTH_SHORT).show();

                        }

                        //Toast.makeText(Send_Complaint.this, data, Toast.LENGTH_LONG).show();
                    } catch (Exception cv) {
                        Toast.makeText(getApplicationContext(), cv.getMessage(), Toast.LENGTH_LONG).show();

                    }
                refresh_activity();

            }
        } else if (view == im1) {

           /* Intent v=new Intent(Intent.ACTION_GET_CONTENT);
            v.setType("file/*");
            startActivityForResult(v,capture);*/
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

            startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST);


        }
    }

    void refresh_activity()
    {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

      //  super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_PIC_REQUEST)

        {

            if (resultCode == RESULT_OK)

            {
                b1.setVisibility(View.VISIBLE);
                //infoText.setVisibility(View.INVISIBLE);

                //Uri mImageCaptureUri = data.getData();
                Uri uri = data.getData();
                try {
                    if (uri == null) {
                       // Toast.makeText(this, "nullllll", Toast.LENGTH_SHORT).show();
                    }
                    //  Toast.makeText(this, "aaaaa", Toast.LENGTH_SHORT).show();

                    Bitmap image = (Bitmap) data.getExtras().get("data");

                    // ImageView imageview = (ImageView) findViewById(R.id.imageView1);
                    img.setVisibility(View.VISIBLE);
                    img.setImageBitmap(image);
                    //img.setImageBitmap(image);

                    ByteArrayOutputStream bos = new ByteArrayOutputStream();

                    image.compress(Bitmap.CompressFormat.JPEG, 100, bos);

                    asd="ok";
                    photoBytes = bos.toByteArray();


                   // encodedImage = Base64.encodeToString(bitmapdata, Base64.NO_WRAP);
                } catch (Exception e) {
                    Toast.makeText(this, e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                }


            }

        }


      /*  if (requestCode == capture && resultCode == RESULT_OK && null != data) {



            Uri uri = data.getData();
            try {
                String photo = FileUtils.getPath(this, uri);
                File fl = new File(photo);
                int ln = (int) fl.length();
                photoName = fl.getName();
                Bitmap bm = BitmapFactory.decodeFile(photo);

                InputStream inputStream = new FileInputStream(fl);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                byte[] b = new byte[ln];
                int bytesRead = 0;
                while ((bytesRead = inputStream.read(b)) != -1)
                {
                    bos.write(b, 0, bytesRead);
                }
                inputStream.close();
                photoBytes = bos.toByteArray();
                asd="ok";

            } catch (Exception e) {

                Toast.makeText(this, "Error_Here_Upload!!!"+e, Toast.LENGTH_SHORT).show();

            }
        }*/

    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view,final int i, long l) {



        final AlertDialog.Builder alert=new AlertDialog.Builder(Send_Complaint.this);
        alert.setTitle("Do you want View Uploaded Data");
        alert.setPositiveButton("view", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int j) {


                String filee=ff[i];
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                String ip = sharedPreferences.getString("ip", "");

                String url1 = "http://" + ip + "/fair_trafic/android/cmp/"+filee;
                Intent tt = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(url1));
                startActivity(tt);





            }
        });
        AlertDialog al=alert.create();
        al.show();




    }
}