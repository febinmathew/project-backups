package com.example.amlu.fair_trafic;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.MultiSelectListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import java.util.Set;


public class SettingsActivity extends AppCompatActivity {


int stat;
    SettingsPage setingspage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.individual_settings_activity);



        FragmentManager fragmentManager= getFragmentManager();

        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();

        setingspage= new SettingsPage();
        SettingsPage page=setingspage;
        fragmentTransaction.add(R.id.linearlay,page);
        fragmentTransaction.commit();




    }



    protected void onResume() {
        super.onResume();
        setingspage.getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(setingspage);
    }

    protected void onPause() {
        super.onPause();
        setingspage.getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(setingspage);
    }

    public static class SettingsPage extends PreferenceFragment implements
            SharedPreferences.OnSharedPreferenceChangeListener
    {

        SharedPreferences sp;
        SharedPreferences deafultPref;
        ListPreference editListPref;
        MultiSelectListPreference editMultiListPref;
        Set<String> selections;
        String[] selected;



        @Override
        public void onCreate(@Nullable Bundle savedInstanceState)

        {
            super.onCreate(savedInstanceState);

            deafultPref = PreferenceManager.getDefaultSharedPreferences(getActivity());

                    addPreferencesFromResource(R.xml.profile_settings);
                    sp = getPreferenceScreen().getSharedPreferences();

            EditTextPreference ex;
            ex=(EditTextPreference) findPreference("server_address");
            ex.setSummary(ex.getText());
            ex=null;
            ex=(EditTextPreference) findPreference("emergency_cont");
            ex.setSummary(ex.getText());

//            Log.e("MSG", ex.getText());//, Toast.LENGTH_SHORT).show();


        }


        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            // findPreference(key).setSummary(sharedPreferences.getString(key, "Some Default Text"));
            Preference pref = findPreference(key);
            if (pref instanceof EditTextPreference) {
                EditTextPreference etp = (EditTextPreference) pref;
                SharedPreferences.Editor edd = deafultPref.edit();
                switch (key)
                {
                    case "server_address":

                        edd.putString("ip", etp.getText());
                        edd.apply();
                        etp.setSummary(etp.getText());
                        break;
                    case "emergency_cont":
                        edd.putString("emergency_contact", etp.getText());
                        edd.apply();
                        etp.setSummary(etp.getText());
                        break;
                }

            }

        }





    }
}