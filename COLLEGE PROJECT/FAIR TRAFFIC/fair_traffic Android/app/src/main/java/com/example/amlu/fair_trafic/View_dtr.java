package com.example.amlu.fair_trafic;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class View_dtr extends BaseAdapter{

	private Context Context;

	String[] name;
	String[] user;
	String[] price;
	String []image;
	String[] status;
	String []contact;


	public View_dtr(Context applicationContext, String[] name, String[] price1, String[] user1,String[] image1,String[] status1,String[] contact1)
	{

		this.Context=applicationContext;
		this.name=name;
		this.user=user1;
		this.price=price1;
		this.image=image1;
		this.status=status1;
		this.contact=contact1;

	}

	@Override
	public int getCount() {
		
		return name.length;
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertview, ViewGroup parent) {

		
		LayoutInflater inflator=(LayoutInflater)Context.getSystemService(android.content.Context.LAYOUT_INFLATER_SERVICE);
		
		View gridView;
		if(convertview==null)
		{
			gridView=new View(Context);
			gridView=inflator.inflate(R.layout.view_doctor, null);
		}
		else
		{
			gridView=(View)convertview;
			
		}

		SharedPreferences ss = PreferenceManager.getDefaultSharedPreferences(Context);
		String ip = ss.getString("ip", "");
		//String url = "http://" + ip + "/mediclient/WebService.asmx";


		TextView tvname=(TextView)gridView.findViewById(R.id.textView76);

		TextView tvuser=(TextView)gridView.findViewById(R.id.textView63);
		TextView tvprice=(TextView)gridView.findViewById(R.id.textView80);

		TextView tvstatus=(TextView)gridView.findViewById(R.id.textView81);
		TextView tvcontact=(TextView)gridView.findViewById(R.id.textView82);



		ImageView img=(ImageView)gridView.findViewById(R.id.imageView5);


		String url="http://"+ip+"/fair_trafic/android/area/"+image[position];


		tvname.setText("Name: "+name[position]);
		tvuser.setText("User: "+user[position]);

		tvstatus.setText("Status: "+status[position]);
		tvprice.setText("Price: "+price[position]);
		tvcontact.setText("Contact: "+contact[position]);

		//Log.d("img url=====",url);

		Picasso.with(Context).load(url).transform(new CircleTransform()).into(img);
		
		return gridView;
	}



}
