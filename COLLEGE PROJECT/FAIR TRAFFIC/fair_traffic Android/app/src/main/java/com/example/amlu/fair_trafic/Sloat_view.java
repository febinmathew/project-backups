package com.example.amlu.fair_trafic;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class Sloat_view extends AppCompatActivity implements AdapterView.OnItemClickListener {
    ListView l1;
String st="pending";
    String [] type,id,status,full,name;
    SharedPreferences sharedPreferences;
    SharedPreferences sp;
    String imgid="";

    String imgurl="";
    private ProgressDialog pDialog;
    public static final int progress_bar_type = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sloat_view);
        getSupportActionBar().setTitle("View Slots");
        l1=(ListView)findViewById(R.id.slot);
        try {
            if(Build.VERSION.SDK_INT>9){
                StrictMode.ThreadPolicy policy=new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
        }catch (Exception e){
            Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
        }


        try
        {

            SharedPreferences sharedPreferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

            String url="http://"+sharedPreferences.getString("ip","")+"/fair_trafic/android/slot_view.php";

            String lid=sharedPreferences.getString("lid","");
            List<NameValuePair> list=new ArrayList<NameValuePair>();

            list.add(new BasicNameValuePair("id",""+View_parking.id[View_parking.pos]+""));

            JSONParser jsonParser=new JSONParser();
            JSONObject jb = jsonParser.makeHttpRequest(url,"GET",list);
            String res=jb.getString("status");
            Log.d("url========",url);
            if (res.equalsIgnoreCase("1"))

            {
                l1.setOnItemClickListener(this);

                Toast.makeText(getApplicationContext(), "aaaaa", Toast.LENGTH_SHORT).show();

                JSONArray ja = new JSONArray();
                ja = jb.getJSONArray("data");
                type = new String[ja.length()];

                status = new String[ja.length()];
                id = new String[ja.length()];
                name = new String[ja.length()];


                full = new String[ja.length()];


                for (int i = 0; i < ja.length(); i++) {
                    JSONObject jo = new JSONObject();
                    jo = ja.getJSONObject(i);
                    type[i] = jo.getString("type");
                    status[i] = jo.getString("status");
                    id[i] = jo.getString("id");
                    name[i] = jo.getString("name");

                    full[i] = "Type : " + type[i]+" wheeler" + "\nStatus :" + name[i];


                }
            }
                else
                {
                    full=new String[1];
                    full[0]="no slots";
                }
                ArrayAdapter<String> add = new ArrayAdapter<>(getApplicationContext(), R.layout.custom_simple_text1, full);
                l1.setAdapter(add);


        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage().toString(), Toast.LENGTH_SHORT).show();
        }








    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view,final int i, long l) {


       final SharedPreferences sp=PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        final AlertDialog.Builder alert=new AlertDialog.Builder(Sloat_view.this);
        alert.setTitle("Do you want");

        String stst="Request";
        if(status[i].equalsIgnoreCase(sp.getString("lid","")))
        {
            stst="Delete Request";

            alert.setNeutralButton("View QR", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int list) {
                    String url=Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+sp.getString("lid","")+".png";
                    File file = new File(url);
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(url));
                    intent.setDataAndType(Uri.fromFile(file), mimeType);
                    Intent intent1 = Intent.createChooser(intent, "Open With");
                    startActivity(intent1);
                }
            });
        }
        alert.setMessage(stst);


        alert.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialogInterface.dismiss();


            }
        });


        alert.setNegativeButton(stst, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int list) {


                try {
                    String lid=sp.getString("lid","");
                    if (status[i].equalsIgnoreCase(lid)) {

                        final AlertDialog.Builder alert1=new AlertDialog.Builder(Sloat_view.this);
                        alert1.setTitle("Do you want to delete");
                        alert1.setMessage("Request");
                        alert1.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                dialogInterface.dismiss();


                            }
                        });
                        alert1.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                Toast.makeText(Sloat_view.this, "You will Remove allocation request sucess fully", Toast.LENGTH_SHORT).show();

                           st="pending";


                                //slot_remove.php
                            }
                        });
                        AlertDialog al1=alert1.create();
                        al1.show();

                    }


                   else if (!status[i].equalsIgnoreCase("pending")) {
                        st="alocated";
                        Toast.makeText(Sloat_view.this, "this slot is already allocated", Toast.LENGTH_SHORT).show();

                    } else if(status[i].equalsIgnoreCase("pending")){

                        st=sp.getString("lid","");
                        imgid=id[i]+".png";

                        //Log.e("MSG",imgid);

                        new DownloadFileFromURL().execute();

                       //  = PreferenceManager.getDefaultSharedPreferences(getApplicationContext())
                    }
                    String ip = sp.getString("ip", "");
                    String ridd = id[i];

                    String url = "http://" + ip + "/fair_trafic/android/slot_update.php";

                    List<NameValuePair> list1 = new ArrayList<NameValuePair>();
                    list1.add(new BasicNameValuePair("id", ridd));
                    list1.add(new BasicNameValuePair("lid", st));

                    JSONParser jn = new JSONParser();
                    JSONObject jsonObject = jn.makeHttpRequest(url, "GET", list1);
                    String res = jsonObject.getString("status");
                    //Toast.makeText(this, "ex="+ex+" qq="+qq , Toast.LENGTH_SHORT).show();

                    if (res.equalsIgnoreCase("exist"))
                    {
                        Toast.makeText(Sloat_view.this, "you have alredy booked sloat", Toast.LENGTH_SHORT).show();

                    }

                    if (res.equalsIgnoreCase("1"))


                    {
                        String qwe=jsonObject.getString("asd");
                        if(qwe.equalsIgnoreCase("asd"))
                        {
                            Toast.makeText(Sloat_view.this, imgid, Toast.LENGTH_SHORT).show();
                            new DownloadFileFromURL().execute();


                            Toast.makeText(Sloat_view.this, "Thank you for choosing this sloat", Toast.LENGTH_SHORT).show();
                            Toast.makeText(Sloat_view.this, "your QR code is downloaded to your Phone ", Toast.LENGTH_SHORT).show();

                        }
                        Toast.makeText(Sloat_view.this, "Success", Toast.LENGTH_SHORT).show();
                        refresh_activity();

                    } else {
                        Toast.makeText(Sloat_view.this, "Error", Toast.LENGTH_SHORT).show();

                    }
                }catch (Exception ex) {
                    Toast.makeText(Sloat_view.this,ex.getMessage().toString(), Toast.LENGTH_SHORT).show();

                }
            }
        });
        AlertDialog al=alert.create();
        al.show();


    }
    void refresh_activity()
    {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type: // we set this to 0
                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Downloading file. Please wait...");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }

    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Bar Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        /**
         * Downloading file in background thread
         * */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                sp=PreferenceManager.getDefaultSharedPreferences(Sloat_view.this);
                URL url = new URL("http://"+sp.getString("ip","")+"/fair_trafic/android/temp/"+imgid);
                Log.d("==========","http://"+sp.getString("ip","")+"/fair_trafic/android/temp/"+imgid);
                URLConnection conection = url.openConnection();
                conection.connect();

                // this will be useful so that you can show a tipical 0-100%
                // progress bar
                int lenghtOfFile = conection.getContentLength();

                // download the file
                InputStream input = new BufferedInputStream(url.openStream(),
                        8192);

                // Output stream
                OutputStream output = new FileOutputStream(Environment
                        .getExternalStorageDirectory().toString()+"/"
                        +imgid);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         * */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            dismissDialog(progress_bar_type);
            Toast.makeText(Sloat_view.this, "downloaded..!!!", Toast.LENGTH_SHORT).show();
        }

    }
}




