package com.example.amlu.fair_trafic;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;

import static android.R.attr.data;

public class Accident_view extends AppCompatActivity implements View.OnClickListener {

    ImageView img;
    Button b1;
    byte[] bitmapdata;
    String data;
    String base64img;

    public String encodedImage;
    private static final int CAMERA_PIC_REQUEST = 2500;
TextView infoText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accident_view);

        img=(ImageView)findViewById(R.id.imageView4);
        b1=(Button)findViewById(R.id.buttons);

        infoText=(TextView) findViewById(R.id.infoText);
        img.setOnClickListener(this);
        b1.setOnClickListener(this);

        try {
            if (Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
        }






    }

    @Override
    public void onClick(View view) {

        if(view==img)
        {
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

            startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST);


        }
        else if(view==b1)
        {
            try {
                SharedPreferences sp= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

                String longi = Locationservice.logi;
                String lati =Locationservice.lati;

                String lid = sp.getString("lid", "");
                // Toast.makeText(getApplicationContext(), "sucess", Toast.LENGTH_SHORT).show();
                String ip = sp.getString("ip", "");
                String url = "http://" + ip + "/fair_trafic/android/accident1.php";
                Log.d("url=====", url);
                FileUpload client = new FileUpload(url);
                client.connectForMultipart();


                client.addFormPart("lati", lati);
                client.addFormPart("lid", lid);

                client.addFormPart("longi", longi);
                client.addFormPart("place", Locationservice.place);


                client.addFormPart("photo", encodedImage);

                // client.addFilePart("photo", encodedImage, bitmapdata);

                client.finishMultipart();
                data = client.getResponse();
                Toast.makeText(this, data, Toast.LENGTH_SHORT).show();
                Log.d("data==========", data);
                JSONObject js = new JSONObject(data);
                if (js.getString("status").equalsIgnoreCase("ok")) {
                    //  String id = js.getString("id");
                    Toast.makeText(getApplication(), "Submission successful", Toast.LENGTH_LONG).show();
                    finish();


                } else {
                    Toast.makeText(this, "File not uploaded", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception cv) {
                Toast.makeText(getApplicationContext(), cv.getMessage(), Toast.LENGTH_LONG).show();

            }

        }

    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CAMERA_PIC_REQUEST)

        {

            if (resultCode == RESULT_OK)

            {
                b1.setVisibility(View.VISIBLE);
                infoText.setVisibility(View.INVISIBLE);

                //Uri mImageCaptureUri = data.getData();
                Uri uri = data.getData();
                try {
                    if (uri == null) {
                        //Toast.makeText(this, "nullllll", Toast.LENGTH_SHORT).show();
                    }
                  //  Toast.makeText(this, "aaaaa", Toast.LENGTH_SHORT).show();

                    Bitmap image = (Bitmap) data.getExtras().get("data");

                    // ImageView imageview = (ImageView) findViewById(R.id.imageView1);

                    img.setImageBitmap(image);

                    ByteArrayOutputStream bos = new ByteArrayOutputStream();

                    image.compress(Bitmap.CompressFormat.JPEG, 100, bos);

                    bitmapdata = bos.toByteArray();


                    encodedImage = Base64.encodeToString(bitmapdata, Base64.NO_WRAP);
                } catch (Exception e) {
                    Toast.makeText(this, e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                }


            }

        }
    }
}
