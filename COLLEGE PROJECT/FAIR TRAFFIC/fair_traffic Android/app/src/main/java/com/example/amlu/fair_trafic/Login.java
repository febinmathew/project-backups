package com.example.amlu.fair_trafic;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Login extends Activity implements View.OnClickListener {
	EditText ed1, ed2;
	Button b1;
	TextView t1,t2;

	SharedPreferences sharedPreferences;
	ImageView img;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login1);
		ed1 = (EditText) findViewById(R.id.editText8);
		ed2 = (EditText) findViewById(R.id.editText2);
		b1 = (Button) findViewById(R.id.button12);
		img = (ImageView) findViewById(R.id.imageView2);

		t1 = (TextView) findViewById(R.id.textView6);
		t2 = (TextView) findViewById(R.id.textView12);


		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

		//String ip = sharedPreferences.getString("ip", "");
		//String url1 = "http://" + ip + "/fair_trafic/android/logo.png";

		//Picasso.with(getApplicationContext()).load(url1).into(img);

		t1.setOnClickListener(this);
		t2.setOnClickListener(this);

		b1.setOnClickListener(this);

		try {
			if (Build.VERSION.SDK_INT > 9) {
				StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
				StrictMode.setThreadPolicy(policy);
			}
		} catch (Exception e) {
			Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
		}


	}


	@Override
	public void onClick(View view) {
		if(view==t2)
		{
			Intent it = new Intent(getApplicationContext(), Forgot_pass.class);
			startActivity(it);
			startActivity(it);

		}
		else if (view == t1) {
			Intent it = new Intent(getApplicationContext(), Register.class);
			startActivity(it);
		} else if (view == b1) {
			sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
			final String ip = sharedPreferences.getString("ip", "");
			String uname = ed1.getText().toString();
			String pword = ed2.getText().toString();
			if (ed1.getText().equals("")) {
				ed1.setError("Enter username");
				ed1.setFocusable(true);
			} else if (ed1.getText().equals("")) {
				ed2.setError("Enter password");

				ed2.setFocusable(true);

			} else {
				try {


					String url = "http://" + ip + "/fair_trafic/android/login.php";
					Log.d("url===========", url);
					List<NameValuePair> list = new ArrayList<NameValuePair>();

					list.add(new BasicNameValuePair("uname", uname));
					list.add(new BasicNameValuePair("pass", pword));
					JSONParser jsonParser = new JSONParser();

					JSONObject jb = jsonParser.makeHttpRequest(url, "GET", list);
					String res = jb.getString("status");
					if (res.equalsIgnoreCase("ok")) {

						Toast.makeText(Login.this, "Success...!!", Toast.LENGTH_SHORT).show();
						String lid = jb.getString("lid");
						SharedPreferences.Editor edt = sharedPreferences.edit();
						edt.putString("lid", lid);
						edt.commit();
						//Toast.makeText(this, "aaaaa", Toast.LENGTH_SHORT).show();
						Intent ho = new Intent(getApplicationContext(), User_home.class);
						startActivity(ho);
						finish();

					} else {
						//Toast.makeText(this, "Invalid username or password", Toast.LENGTH_SHORT).show();
						Toast.makeText(Login.this, "Invalid username or password", Toast.LENGTH_SHORT).show();
					}
				} catch (Exception e) {

					Toast.makeText(Login.this, e.getMessage().toString(), Toast.LENGTH_SHORT).show();
				}

			}
		}
	}
}
