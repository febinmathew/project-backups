-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 16, 2018 at 07:15 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `fairtraffic`
--

-- --------------------------------------------------------

--
-- Table structure for table `accidents`
--

CREATE TABLE IF NOT EXISTS `accidents` (
  `accident_id` int(11) NOT NULL,
  `user_id` varchar(20) DEFAULT NULL,
  `sender_type` varchar(55) NOT NULL,
  `location_name` varchar(30) DEFAULT NULL,
  `timestamp` varchar(55) NOT NULL,
  `latitude` varchar(20) NOT NULL,
  `longitude` varchar(20) NOT NULL,
  `image` varchar(200) DEFAULT NULL,
  `accident_status` varchar(55) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=185 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accidents`
--

INSERT INTO `accidents` (`accident_id`, `user_id`, `sender_type`, `location_name`, `timestamp`, `latitude`, `longitude`, `image`, `accident_status`) VALUES
(158, '83', 'other', '', '2018-04-14 12:39:54', '', '', '158.jpg', 'checked'),
(171, '83', 'user', '673603', '2018-04-14 13:58:06', '11.3372373', '76.0346578', 'img', 'pending'),
(172, '83', 'user', 'Mukkam - Koodaranji Road', '2018-04-14 13:58:22', '11.3388091', '76.0241405', 'img', 'pending'),
(173, '83', 'user', 'Mukkam - Koodaranji Road', '2018-04-14 13:58:36', '11.3388091', '76.0241405', 'img', 'pending'),
(174, '83', 'user', 'Mukkam - Koodaranji Road', '2018-04-14 13:58:59', '11.33906411', '76.0247652', 'img', 'pending'),
(175, '83', 'user', 'Mukkam - Koodaranji Road', '2018-04-14 13:59:11', '11.3388091', '76.0241405', 'img', 'pending'),
(176, '83', 'user', 'Mukkam - Koodaranji Road', '2018-04-14 19:07:11', '11.3380996', '76.0237778', 'img', 'pending'),
(177, '83', 'user', 'Mukkam - Koodaranji Road', '2018-04-14 19:08:36', '11.3388091', '76.0241405', 'img', 'pending'),
(178, '83', 'user', 'Mukkam - Koodaranji Road', '2018-04-16 22:14:51', '11.3388091', '76.0241405', 'img', 'pending'),
(179, '83', 'user', 'Kalappurakkal', '2018-04-16 22:27:56', '11.3381572', '76.0284229', 'img', 'pending'),
(180, '83', 'user', '673604', '2018-04-16 22:28:32', '11.3378634', '76.0305475', 'img', 'pending'),
(181, '83', 'user', 'Kalappurakkal', '2018-04-16 22:33:26', '11.3381572', '76.0284229', 'img', 'pending'),
(182, '83', 'user', 'Kalappurakkal', '2018-04-16 22:35:38', '11.3381572', '76.0284229', 'img', 'pending'),
(183, '83', 'user', 'Kalappurakkal', '2018-04-16 22:37:14', '11.3381572', '76.0284229', 'img', 'pending'),
(184, '83', 'user', 'Kalappurakkal', '2018-04-16 22:39:36', '11.3381572', '76.0284229', 'img', 'pending');

-- --------------------------------------------------------

--
-- Table structure for table `complaint`
--

CREATE TABLE IF NOT EXISTS `complaint` (
  `comp_no` int(11) NOT NULL,
  `user_id` varchar(20) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `time_stamp` varchar(55) NOT NULL,
  `latitude` varchar(30) NOT NULL,
  `longitude` varchar(30) NOT NULL,
  `image_url` varchar(200) DEFAULT NULL,
  `complaint_status` varchar(55) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `complaint`
--

INSERT INTO `complaint` (`comp_no`, `user_id`, `description`, `time_stamp`, `latitude`, `longitude`, `image_url`, `complaint_status`) VALUES
(32, '83', 'Gg', '2018-04-14 12:05:26', '11.3350077', '76.0458995', '1.jpg', 'checked'),
(33, '83', 'Jjj', '2018-04-14 12:16:17', '11.3350077', '76.0458995', '33.png', 'checked'),
(34, '83', 'Laptop', '2018-04-14 12:30:23', '', '', '34.png', 'checked'),
(35, '83', 'Aaccc', '2018-04-14 12:37:29', '', '', '35.png', 'checked'),
(36, '83', 'Sddfff', '2018-04-14 19:09:40', '11.3388091', '76.0241405', '36.png', 'pending');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE IF NOT EXISTS `feedback` (
  `id` int(11) NOT NULL,
  `lid` int(11) DEFAULT NULL,
  `feedback` varchar(55) DEFAULT NULL,
  `rating` varchar(55) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `loginid` int(11) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `usertype` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`loginid`, `username`, `password`, `usertype`) VALUES
(6, 'admin', 'admin', 'admin'),
(82, 'mukkam', 'mukkam', 'police'),
(83, 'fmfebinmathew@gmail.com', '1234567', 'user'),
(84, 'fmfebinmathew@gmail.con', '1234567', 'user'),
(85, 'nadakkavu', 'nadakkavu', 'police');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE IF NOT EXISTS `notification` (
  `not_id` int(11) NOT NULL,
  `not_title` varchar(30) NOT NULL,
  `not_msg` varchar(500) DEFAULT NULL,
  `timestamp` datetime NOT NULL,
  `latitude` varchar(30) NOT NULL,
  `longitude` varchar(30) NOT NULL,
  `validity` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `parkingarea`
--

CREATE TABLE IF NOT EXISTS `parkingarea` (
  `parking_id` int(11) NOT NULL,
  `parking_name` varchar(20) DEFAULT NULL,
  `latitude` varchar(30) NOT NULL,
  `longitude` varchar(30) NOT NULL,
  `price` int(11) NOT NULL,
  `image` varchar(200) DEFAULT NULL,
  `user_id` varchar(20) DEFAULT NULL,
  `status` varchar(55) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parkingarea`
--

INSERT INTO `parkingarea` (`parking_id`, `parking_name`, `latitude`, `longitude`, `price`, `image`, `user_id`, `status`) VALUES
(24, 'febin', '11.3350077', '76.0458995', 20, '.jpg', '83', 'pending'),
(25, 'parking 2', '11.3388091', '76.0241405', 50, '25.jpg', '83', 'approoved'),
(26, 'kaithamattathil', '11.338897', '76.0241405', 10, '26.jpg', '84', 'approoved');

-- --------------------------------------------------------

--
-- Table structure for table `polls`
--

CREATE TABLE IF NOT EXISTS `polls` (
  `poll_no` int(11) NOT NULL,
  `poll_title` varchar(50) NOT NULL,
  `poll_description` varchar(500) DEFAULT NULL,
  `latitude` varchar(30) DEFAULT NULL,
  `longitude` varchar(30) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `validity` datetime DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `op1` varchar(55) DEFAULT NULL,
  `op2` varchar(55) DEFAULT NULL,
  `op3` varchar(55) DEFAULT NULL,
  `op4` varchar(55) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `polls`
--

INSERT INTO `polls` (`poll_no`, `poll_title`, `poll_description`, `latitude`, `longitude`, `timestamp`, `validity`, `priority`, `op1`, `op2`, `op3`, `op4`) VALUES
(9, 'Do you life this app?', 'Do you like it?', '11.33729952080036', '76.02245836330553', '2018-02-04 01:00:00', '2018-05-16 02:01:00', 5, 'Yes', 'No', 'Maybe', 'not Now');

-- --------------------------------------------------------

--
-- Table structure for table `poll_options`
--

CREATE TABLE IF NOT EXISTS `poll_options` (
  `pollopt_no` int(11) NOT NULL,
  `poll_id` int(11) NOT NULL,
  `lid` int(11) DEFAULT NULL,
  `value` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poll_options`
--

INSERT INTO `poll_options` (`pollopt_no`, `poll_id`, `lid`, `value`) VALUES
(12, 9, 83, '1');

-- --------------------------------------------------------

--
-- Table structure for table `slot`
--

CREATE TABLE IF NOT EXISTS `slot` (
  `slot_id` int(11) NOT NULL,
  `parking_id` int(11) DEFAULT NULL,
  `type` varchar(55) DEFAULT NULL,
  `status` varchar(30) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slot`
--

INSERT INTO `slot` (`slot_id`, `parking_id`, `type`, `status`) VALUES
(30, 25, '2', 'pending'),
(31, 25, '4', 'pending'),
(32, 26, '4', 'pending'),
(34, 26, '2', '83');

-- --------------------------------------------------------

--
-- Table structure for table `traffic_status`
--

CREATE TABLE IF NOT EXISTS `traffic_status` (
  `trf_id` int(11) NOT NULL,
  `location_name` varchar(100) DEFAULT NULL,
  `timestamp` varchar(44) NOT NULL,
  `latitude` varchar(30) NOT NULL,
  `longitude` varchar(30) NOT NULL,
  `volume_status` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `traffic_status`
--

INSERT INTO `traffic_status` (`trf_id`, `location_name`, `timestamp`, `latitude`, `longitude`, `volume_status`) VALUES
(13, 'mukkam', '2018-04-14 12:43:30', '11.333224204152001', '76.02243142121279', 7),
(14, 'mavoor road', '2018-04-14 12:44:21', '11.260390415507894', '75.80772280677036', 8),
(15, 'koodaranji', '2018-04-14 12:52:09', '11.339926832230548', '76.02666117641957', 4);

-- --------------------------------------------------------

--
-- Table structure for table `trafic`
--

CREATE TABLE IF NOT EXISTS `trafic` (
  `id` int(11) NOT NULL,
  `tid` int(11) NOT NULL,
  `name` varchar(55) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `phone` varchar(55) DEFAULT NULL,
  `email` varchar(55) DEFAULT NULL,
  `lati` varchar(55) DEFAULT NULL,
  `longi` varchar(55) DEFAULT NULL,
  `uname` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trafic`
--

INSERT INTO `trafic` (`id`, `tid`, `name`, `location`, `phone`, `email`, `lati`, `longi`, `uname`) VALUES
(7, 82, 'Mukkam', 'Mukkam', '9633323461', 'fmfebinmathew@gmail.com', '11.320042811603802', '75.99413986828483', 0),
(8, 85, 'Nadakkavu', 'Nadakkavu', '919633323461', 'fmfebinmathew@gmail.com', '11.268397174850902', '75.77667270927589', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL,
  `login_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `address1` varchar(100) NOT NULL,
  `address2` varchar(100) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `pincode` varchar(6) NOT NULL,
  `phone` varchar(13) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `latitude` varchar(30) DEFAULT NULL,
  `longitude` varchar(30) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `login_id`, `name`, `address1`, `address2`, `city`, `state`, `pincode`, `phone`, `email`, `latitude`, `longitude`) VALUES
(8, 83, 'Febin Mathew', 'kaithamattathil', 'koodranji', 'kozhikode', 'kerala', '673604', '9633323461', 'fmfebinmathew@gmail.com', '', ''),
(9, 84, 'febin2', '', '', '', '', '', '9633323461', 'fmfebinmathew@gmail.con', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accidents`
--
ALTER TABLE `accidents`
  ADD PRIMARY KEY (`accident_id`);

--
-- Indexes for table `complaint`
--
ALTER TABLE `complaint`
  ADD PRIMARY KEY (`comp_no`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`loginid`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`not_id`);

--
-- Indexes for table `parkingarea`
--
ALTER TABLE `parkingarea`
  ADD PRIMARY KEY (`parking_id`);

--
-- Indexes for table `polls`
--
ALTER TABLE `polls`
  ADD PRIMARY KEY (`poll_no`);

--
-- Indexes for table `poll_options`
--
ALTER TABLE `poll_options`
  ADD PRIMARY KEY (`pollopt_no`);

--
-- Indexes for table `slot`
--
ALTER TABLE `slot`
  ADD PRIMARY KEY (`slot_id`);

--
-- Indexes for table `traffic_status`
--
ALTER TABLE `traffic_status`
  ADD PRIMARY KEY (`trf_id`);

--
-- Indexes for table `trafic`
--
ALTER TABLE `trafic`
  ADD PRIMARY KEY (`id`,`tid`,`uname`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accidents`
--
ALTER TABLE `accidents`
  MODIFY `accident_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=185;
--
-- AUTO_INCREMENT for table `complaint`
--
ALTER TABLE `complaint`
  MODIFY `comp_no` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `loginid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=86;
--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `not_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `parkingarea`
--
ALTER TABLE `parkingarea`
  MODIFY `parking_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `polls`
--
ALTER TABLE `polls`
  MODIFY `poll_no` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `poll_options`
--
ALTER TABLE `poll_options`
  MODIFY `pollopt_no` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `slot`
--
ALTER TABLE `slot`
  MODIFY `slot_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `traffic_status`
--
ALTER TABLE `traffic_status`
  MODIFY `trf_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `trafic`
--
ALTER TABLE `trafic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
