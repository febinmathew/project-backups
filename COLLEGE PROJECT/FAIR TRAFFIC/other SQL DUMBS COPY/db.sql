/*
SQLyog Professional v12.09 (64 bit)
MySQL - 5.1.53-community-log : Database - fairtraffic
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`fairtraffic` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `fairtraffic`;

/*Table structure for table `accidents` */

DROP TABLE IF EXISTS `accidents`;

CREATE TABLE `accidents` (
  `accident_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(20) DEFAULT NULL,
  `sender_type` varchar(55) NOT NULL,
  `location_name` varchar(30) DEFAULT NULL,
  `timestamp` varchar(55) NOT NULL,
  `latitude` varchar(20) NOT NULL,
  `longitude` varchar(20) NOT NULL,
  `image` varchar(200) DEFAULT NULL,
  `accident_status` varchar(55) NOT NULL,
  PRIMARY KEY (`accident_id`)
) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=latin1;

/*Data for the table `accidents` */

insert  into `accidents`(`accident_id`,`user_id`,`sender_type`,`location_name`,`timestamp`,`latitude`,`longitude`,`image`,`accident_status`) values (1,'8','0','aaa','14','33','44','img','checked'),(2,'8','user','','14:06:10','','','img','checked'),(3,'8','user','','14:06:11','','','img','checked'),(4,'8','user','Mofusil Bus Stand Building','14:13:15','11.2582121','75.7867469','img','pending'),(5,'8','user','Mofusil Bus Stand Building','14:13:16','11.2582121','75.7867469','img','pending'),(6,'8','user','City Tower','14:13:32','11.25916111','75.78431343','img','pending'),(7,'8','user','City Tower','14:13:32','11.25916111','75.78431343','img','pending'),(8,'8','user','City Tower','14:13:32','11.25916111','75.78431343','img','pending'),(9,'8','user','City Tower','14:13:32','11.25916111','75.78431343','img','pending'),(10,'8','user','Mofusil Bus Stand Building','14:13:40','11.2582121','75.7867469','img','pending'),(11,'8','user','Mofusil Bus Stand Building','14:13:40','11.2582121','75.7867469','img','pending'),(12,'8','user','Mofusil Bus Stand Building','14:13:41','11.2582121','75.7867469','img','pending'),(13,'8','user','Mofusil Bus Stand Building','14:13:44','11.2582121','75.7867469','img','checked'),(14,'8','user','Mofusil Bus Stand Building','14:13:44','11.2582121','75.7867469','img','pending'),(15,'8','user','Mofusil Bus Stand Building','14:13:55','11.2582121','75.7867469','img','pending'),(16,'8','user','Mofusil Bus Stand Building','14:13:56','11.2582121','75.7867469','img','pending'),(17,'8','user','SK Temple Road','14:14:27','11.25897523','75.78398282','img','pending'),(18,'8','user','SK Temple Road','14:14:27','11.25897523','75.78398282','img','pending'),(19,'8','user','SK Temple Road','14:14:40','11.25873619','75.78377371','img','pending'),(20,'8','user','SK Temple Road','14:14:41','11.25873619','75.78377371','img','pending'),(21,'8','user','SK Temple Road','14:14:41','11.25873619','75.78377371','img','pending'),(22,'8','user','SK Temple Road','14:14:41','11.25873619','75.78377371','img','pending'),(23,'8','user','Mofusil Bus Stand Building','14:14:47','11.2582121','75.7867469','img','pending'),(24,'8','user','Mofusil Bus Stand Building','14:14:48','11.2582121','75.7867469','img','pending'),(25,'8','user','Mofusil Bus Stand Building','14:14:53','11.2582121','75.7867469','img','pending'),(26,'8','user','Mofusil Bus Stand Building','14:14:53','11.2582121','75.7867469','img','pending'),(27,'8','user','SK Temple Road','14:14:54','11.25882528','75.78395784','img','pending'),(28,'8','user','SK Temple Road','14:14:58','11.25882528','75.78395784','img','pending'),(29,'8','user','SK Temple Road','14:14:59','11.25882528','75.78395784','img','pending'),(30,'8','user','Mofusil Bus Stand Building','14:14:59','11.2582121','75.7867469','img','pending'),(31,'8','user','Mofusil Bus Stand Building','14:15:00','11.2582121','75.7867469','img','pending'),(32,'8','user','Mofusil Bus Stand Building','14:15:05','11.2582121','75.7867469','img','pending'),(33,'8','user','Mofusil Bus Stand Building','14:15:05','11.2582121','75.7867469','img','pending'),(34,'8','user','Mofusil Bus Stand Building','14:15:05','11.2582121','75.7867469','img','pending'),(35,'$lid','user','$place','2018-03-26 15:20:44','$lati','$longi','img','pending'),(36,'8','user','Mofusil Bus Stand Building','2018-03-27 09:20:05','11.2582121','75.7867469','img','pending'),(37,'8','user','Mofusil Bus Stand Building','2018-03-27 09:20:06','11.2582121','75.7867469','img','pending'),(38,'8','user','Mofusil Bus Stand Building','2018-03-27 09:20:14','11.2582121','75.7867469','img','pending'),(39,'8','user','Mofusil Bus Stand Building','2018-03-27 09:20:14','11.2582121','75.7867469','img','pending'),(40,'8','user','Mofusil Bus Stand Building','2018-03-27 09:20:14','11.2582121','75.7867469','img','pending'),(41,'8','user','Mofusil Bus Stand Building','2018-03-27 09:20:38','11.2582121','75.7867469','img','pending'),(42,'8','user','Mofusil Bus Stand Building','2018-03-27 09:20:39','11.2582121','75.7867469','img','pending'),(43,'8','user','Mofusil Bus Stand Building','2018-03-27 09:20:39','11.2582121','75.7867469','img','pending'),(44,'8','user','Mofusil Bus Stand Building','2018-03-27 09:20:58','11.2582121','75.7867469','img','pending'),(45,'8','user','Mofusil Bus Stand Building','2018-03-27 09:20:59','11.2582121','75.7867469','img','pending'),(46,'8','user','Mofusil Bus Stand Building','2018-03-27 09:21:50','11.2582121','75.7867469','img','pending'),(47,'8','user','Mofusil Bus Stand Building','2018-03-27 09:21:50','11.2582121','75.7867469','img','pending'),(48,'8','user','Mofusil Bus Stand Building','2018-03-27 09:21:50','11.2582121','75.7867469','img','pending'),(49,'8','user','Mofusil Bus Stand Building','2018-03-27 09:21:50','11.2582121','75.7867469','img','checked'),(50,'8','user','Mofusil Bus Stand Building','2018-03-27 09:22:14','11.2582121','75.7867469','img','pending'),(51,'8','user','Mofusil Bus Stand Building','2018-03-27 09:22:14','11.2582121','75.7867469','img','pending'),(52,'8','user','Mofusil Bus Stand Building','2018-03-27 09:22:15','11.2582121','75.7867469','img','pending'),(53,'8','user','Mofusil Bus Stand Building','2018-03-27 09:22:16','11.2582121','75.7867469','img','pending'),(54,'8','user','Mofusil Bus Stand Building','2018-03-27 09:22:16','11.2582121','75.7867469','img','pending'),(55,'8','user','Mofusil Bus Stand Building','2018-03-27 09:22:16','11.2582121','75.7867469','img','pending'),(56,'8','user','Mofusil Bus Stand Building','2018-03-27 09:22:17','11.2582121','75.7867469','img','pending'),(57,'8','user','Mofusil Bus Stand Building','2018-03-27 09:22:17','11.2582121','75.7867469','img','pending'),(58,'8','user','Mofusil Bus Stand Building','2018-03-27 09:22:17','11.2582121','75.7867469','img','pending'),(59,'8','user','Mofusil Bus Stand Building','2018-03-27 09:22:17','11.2582121','75.7867469','img','pending'),(60,'9','user','Mofusil Bus Stand Building','2018-03-27 17:30:30','11.2582121','75.7867469','img','pending'),(61,'9','user','Mofusil Bus Stand Building','2018-03-27 17:30:31','11.2582121','75.7867469','img','pending'),(62,'9','user','Mofusil Bus Stand Building','2018-03-27 17:30:31','11.2582121','75.7867469','img','pending'),(63,'9','user','Mofusil Bus Stand Building','2018-03-27 17:30:42','11.2582121','75.7867469','img','pending'),(64,'5','user','Mofusil Bus Stand Building','2018-04-01 15:40:09','11.2582121','75.7867469','img','0'),(65,'5','user','Mofusil Bus Stand Building','2018-04-01 15:40:09','11.2582121','75.7867469','img','0'),(66,'5','user','Mofusil Bus Stand Building','2018-04-01 15:40:10','11.2582121','75.7867469','img','0'),(67,'5','user','Mofusil Bus Stand Building','2018-04-01 15:40:10','11.2582121','75.7867469','img','0'),(68,'5','user','Mofusil Bus Stand Building','2018-04-01 15:40:31','11.2582121','75.7867469','img','0'),(69,'5','user','Mofusil Bus Stand Building','2018-04-01 15:40:32','11.2582121','75.7867469','img','0'),(70,'5','user','Mofusil Bus Stand Building','2018-04-01 15:40:34','11.2582121','75.7867469','img','0'),(71,'5','user','Mofusil Bus Stand Building','2018-04-01 15:40:34','11.2582121','75.7867469','img','0'),(72,'5','user','Mofusil Bus Stand Building','2018-04-01 15:40:34','11.2582121','75.7867469','img','0'),(73,'5','user','Mofusil Bus Stand Building','2018-04-01 15:40:35','11.2582121','75.7867469','img','0'),(74,'5','user','Mofusil Bus Stand Building','2018-04-01 15:40:48','11.2582121','75.7867469','img','0'),(75,'9','user','Mofusil Bus Stand Building','2018-04-04 13:52:31','11.2582121','75.7867469','img','0'),(76,'9','user','Mofusil Bus Stand Building','2018-04-04 13:52:32','11.2582121','75.7867469','img','0'),(77,'9','user','Mofusil Bus Stand Building','2018-04-04 13:52:32','11.2582121','75.7867469','img','0'),(78,'$lid','other','$place','2018-04-04 15:19:45','$laty','$longi','$pic','pending'),(79,'$lid','other','$place','2018-04-04 15:19:46','$laty','$longi','$pic','pending'),(80,'9','other','Mofusil Bus Stand Building','2018-04-04 15:51:43','11.2582121','75.7867469','80.jpg','pending'),(81,'9','other','Mofusil Bus Stand Building','2018-04-04 15:52:10','11.2582121','75.7867469','81.jpg','pending'),(82,'9','user','Mofusil Bus Stand Building','2018-04-08 10:29:32','11.2581769','75.7860203','img','0'),(83,'9','user','Mofusil Bus Stand Building','2018-04-08 10:29:32','11.2581769','75.7860203','img','0'),(84,'9','user','Mofusil Bus Stand Building','2018-04-08 10:29:50','11.2581769','75.7860203','img','0'),(85,'9','user','Mofusil Bus Stand Building','2018-04-08 10:29:51','11.2581769','75.7860203','img','0'),(86,'9','user','Mofusil Bus Stand Building','2018-04-08 10:30:05','11.2581769','75.7860203','img','0'),(87,'9','other','Mofusil Bus Stand Building','2018-04-08 10:31:08','11.2581769','75.7860203','87.jpg','pending'),(88,'8','user','','2018-04-08 11:18:02','11.25886347','75.78388071','img','0'),(89,'8','user','','2018-04-08 11:18:03','11.25886347','75.78388071','img','0'),(90,'8','user','','2018-04-08 11:18:07','11.25886347','75.78388071','img','0'),(91,'9','user','Mofusil Bus Stand Building','2018-04-11 10:08:22','11.2581769','75.7860203','img','pending'),(92,'9','user','Mofusil Bus Stand Building','2018-04-11 10:08:23','11.2581769','75.7860203','img','pending'),(93,'9','user','Mofusil Bus Stand Building','2018-04-11 10:08:26','11.2581769','75.7860203','img','pending'),(94,'9','user','Mofusil Bus Stand Building','2018-04-11 10:08:39','11.2581769','75.7860203','img','pending'),(95,'9','user','Mofusil Bus Stand Building','2018-04-11 10:08:39','11.2581769','75.7860203','img','pending'),(96,'9','user','Mofusil Bus Stand Building','2018-04-11 10:12:06','11.2581769','75.7860203','img','pending'),(97,'9','user','Mofusil Bus Stand Building','2018-04-11 10:12:06','11.2581769','75.7860203','img','pending'),(98,'9','user','Mofusil Bus Stand Building','2018-04-11 10:12:14','11.2581769','75.7860203','img','pending'),(99,'9','user','Mofusil Bus Stand Building','2018-04-11 10:13:18','11.2581769','75.7860203','img','pending'),(100,'9','user','Mofusil Bus Stand Building','2018-04-11 10:13:18','11.2581769','75.7860203','img','pending'),(101,'9','user','Mofusil Bus Stand Building','2018-04-11 10:13:35','11.2581769','75.7860203','img','pending'),(102,'9','user','Mofusil Bus Stand Building','2018-04-11 10:13:53','11.2581769','75.7860203','img','pending'),(103,'9','user','Mofusil Bus Stand Building','2018-04-11 10:13:53','11.2581769','75.7860203','img','pending'),(104,'9','user','Mofusil Bus Stand Building','2018-04-11 10:13:54','11.2581769','75.7860203','img','pending'),(105,'9','user','Mofusil Bus Stand Building','2018-04-11 10:17:14','11.2581769','75.7860203','img','pending'),(106,'9','user','Mofusil Bus Stand Building','2018-04-11 10:17:14','11.2581769','75.7860203','img','pending'),(107,'9','user','Mofusil Bus Stand Building','2018-04-11 10:17:18','11.2581769','75.7860203','img','pending'),(108,'9','user','Mofusil Bus Stand Building','2018-04-11 10:17:19','11.2581769','75.7860203','img','pending'),(109,'9','user','Mofusil Bus Stand Building','2018-04-11 10:18:01','11.2581769','75.7860203','img','pending'),(110,'9','user','Mofusil Bus Stand Building','2018-04-11 10:18:26','11.2581769','75.7860203','img','pending'),(111,'9','user','Mofusil Bus Stand Building','2018-04-11 10:18:27','11.2581769','75.7860203','img','pending'),(112,'9','other','Mofusil Bus Stand Building','2018-04-11 16:26:09','11.2581769','75.7860203','112.jpg','pending'),(113,'9','user','Mofusil Bus Stand Building','2018-04-11 16:32:24','11.2581769','75.7860203','img','pending'),(114,'9','user','Mofusil Bus Stand Building','2018-04-11 16:32:25','11.2581769','75.7860203','img','pending'),(115,'9','user','Cee Cee Centre','2018-04-11 16:32:32','11.25908918','75.7840004','img','pending'),(116,'9','user','Cee Cee Centre','2018-04-11 16:32:32','11.25908918','75.7840004','img','pending'),(117,'9','user','Cee Cee Centre','2018-04-11 16:32:33','11.25908918','75.7840004','img','pending'),(118,'9','user','Cee Cee Centre','2018-04-11 16:32:33','11.25908918','75.7840004','img','pending'),(119,'9','user','Cee Cee Centre','2018-04-11 16:32:33','11.25908918','75.7840004','img','pending'),(120,'9','user','Cee Cee Centre','2018-04-11 16:32:47','11.25908914','75.7840067','img','pending'),(121,'9','user','Cee Cee Centre','2018-04-11 16:32:47','11.25908914','75.7840067','img','pending'),(122,'9','user','Cee Cee Centre','2018-04-11 16:33:03','11.25908511','75.78400412','img','pending'),(123,'9','user','Cee Cee Centre','2018-04-11 16:33:03','11.25908511','75.78400412','img','pending'),(124,'9','user','Cee Cee Centre','2018-04-11 16:33:08','11.25908504','75.78400429','img','pending'),(125,'9','user','Cee Cee Centre','2018-04-11 16:33:09','11.25908504','75.78400429','img','pending'),(126,'9','user','Cee Cee Centre','2018-04-11 16:33:21','11.25908524','75.78400444','img','pending'),(127,'9','user','Cee Cee Centre','2018-04-11 16:33:22','11.25908524','75.78400444','img','pending'),(128,'9','user','Cee Cee Centre','2018-04-11 16:33:26','11.25908533','75.7840045','img','pending'),(129,'9','user','Cee Cee Centre','2018-04-11 16:33:27','11.25908533','75.7840045','img','pending'),(130,'9','user','Cee Cee Centre','2018-04-11 16:33:27','11.25908533','75.7840045','img','pending'),(131,'9','user','Cee Cee Centre','2018-04-11 16:33:29','11.25908533','75.7840045','img','pending'),(132,'9','user','Cee Cee Centre','2018-04-11 16:33:31','11.25908536','75.78400452','img','pending'),(133,'9','user','Cee Cee Centre','2018-04-11 16:33:32','11.25908536','75.78400452','img','pending'),(134,'9','user','Cee Cee Centre','2018-04-11 16:33:47','11.25908537','75.78400462','img','pending'),(135,'9','user','Cee Cee Centre','2018-04-11 16:33:48','11.25908537','75.78400462','img','pending'),(136,'9','user','Cee Cee Centre','2018-04-11 16:33:54','11.25908536','75.78400463','img','pending'),(137,'9','user','Cee Cee Centre','2018-04-11 16:33:58','11.25908536','75.78400463','img','pending'),(138,'9','user','Cee Cee Centre','2018-04-11 16:34:00','11.25908536','75.78400463','img','pending'),(139,'9','user','Mofusil Bus Stand Building','2018-04-11 16:46:10','11.2581769','75.7860203','img','pending'),(140,'9','user','Mofusil Bus Stand Building','2018-04-11 16:46:13','11.2581769','75.7860203','img','pending'),(141,'9','user','SK Temple Road','2018-04-11 16:46:25','11.259059','75.7840194','img','pending'),(142,'9','user','SK Temple Road','2018-04-11 16:46:35','11.25896273','75.78397512','img','pending');

/*Table structure for table `complaint` */

DROP TABLE IF EXISTS `complaint`;

CREATE TABLE `complaint` (
  `comp_no` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(20) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `time_stamp` varchar(55) NOT NULL,
  `latitude` varchar(30) NOT NULL,
  `longitude` varchar(30) NOT NULL,
  `image_url` varchar(200) DEFAULT NULL,
  `complaint_status` varchar(55) NOT NULL,
  PRIMARY KEY (`comp_no`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

/*Data for the table `complaint` */

insert  into `complaint`(`comp_no`,`user_id`,`description`,`time_stamp`,`latitude`,`longitude`,`image_url`,`complaint_status`) values (1,'asda','cfgvbhj','0','dtfygvhbnj','fcvgb','sdfcgvh','checked'),(2,'$lid','$discri','2018-04-01 16:19:35','$lati','$longi','$file','pending'),(3,'9','Aaaa','2018-04-01 16:35:35','','75.78412504','.','pending'),(4,'9','Ffgg','2018-04-01 16:36:04','','75.78392564','.','pending'),(5,'9','Aaaaa','2018-04-01 16:44:09','','75.7867469','.pdf','pending'),(6,'9','Aass','2018-04-01 16:44:29','','75.7867469','.png','pending'),(7,'9','Aaaa','2018-04-01 16:45:45','','75.7867469','7.jpg','pending'),(8,'9','Aaaa','2018-04-01 16:46:24','','75.7867469','8.pdf','checked'),(9,'9','Aaaaaas','2018-04-01 16:55:19','11.25893865','75.78342011','9.avi','checked'),(10,'99','Dfgfdtt','2018-04-03 11:30:11','11.2582121','75.7867469','10.pdf','pending'),(11,'8','vsvsgs','2018-04-08 11:42:27','','','11.vcf','pending'),(12,'9','Asdasd','2018-04-10 15:07:30','11.2581769','75.7860203','no data','pending'),(13,'9','','2018-04-10 15:08:28','11.2581769','75.7860203','no data','pending'),(14,'9','','2018-04-10 15:08:39','11.2581769','75.7860203','no data','pending'),(15,'9','Hhhhh','2018-04-10 15:09:08','11.2581769','75.7860203','15.html','pending'),(16,'9','New Cmp','2018-04-11 16:10:24','11.25914316','75.78370223','no data','pending'),(17,'9','Complaint','2018-04-11 16:11:06','11.25914386','75.78370343','no data','pending'),(18,'9','This App','2018-04-11 16:11:50','11.25892036','75.78386836','18.apk','pending'),(19,'9','Assddd','2018-04-11 16:12:42','11.25912849','75.78415596','19.png','pending'),(20,'9','Vshxhjd','2018-04-11 16:13:06','11.25921101','75.78421926','20.png','pending'),(21,'9','Vshxhjd','2018-04-11 16:13:07','11.25921101','75.78421926','21.png','pending'),(22,'9','Hshdhdjd','2018-04-11 16:13:31','11.2591251','75.7842038','22.apk','pending'),(23,'9','Apkkkk','2018-04-11 16:16:23','11.25908911','75.78374687','23.apk','pending'),(24,'9','Sbsjdd','2018-04-11 16:18:17','11.25909622','75.78388401','no data','pending');

/*Table structure for table `feedback` */

DROP TABLE IF EXISTS `feedback`;

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lid` int(11) DEFAULT NULL,
  `feedback` varchar(55) DEFAULT NULL,
  `rating` varchar(55) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `feedback` */

insert  into `feedback`(`id`,`lid`,`feedback`,`rating`) values (1,0,'',''),(2,5,'aaaa','4'),(3,9,'','2.5'),(4,9,'Fffff','2.5'),(5,99,'Aass','3.5'),(6,99,'Aass','3.5'),(7,9,'Aasaa','3.0'),(8,8,'yhh','5.0'),(9,9,'Fgdd','3.0');

/*Table structure for table `login` */

DROP TABLE IF EXISTS `login`;

CREATE TABLE `login` (
  `loginid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `usertype` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`loginid`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=latin1;

/*Data for the table `login` */

insert  into `login`(`loginid`,`username`,`password`,`usertype`) values (6,'admin','admin','admin'),(7,'asd','asd','police'),(8,'fmfebinmathew@gmail.com','aa','user'),(9,'amal','143','user'),(10,'asd','asd','police'),(77,'Amalkavil143@gmail.com','Amalamlu','user');

/*Table structure for table `notification` */

DROP TABLE IF EXISTS `notification`;

CREATE TABLE `notification` (
  `not_id` int(11) NOT NULL AUTO_INCREMENT,
  `not_title` varchar(30) NOT NULL,
  `not_msg` varchar(500) DEFAULT NULL,
  `timestamp` datetime NOT NULL,
  `latitude` varchar(30) NOT NULL,
  `longitude` varchar(30) NOT NULL,
  `validity` datetime NOT NULL,
  PRIMARY KEY (`not_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `notification` */

insert  into `notification`(`not_id`,`not_title`,`not_msg`,`timestamp`,`latitude`,`longitude`,`validity`) values (1,'asadasd','asda','0000-00-00 00:00:00','afswf','wadas','0000-00-00 00:00:00'),(2,'$tile','$msg','2018-03-29 17:26:02','$lati','$longi','0000-00-00 00:00:00'),(3,'aaa','ssss','0000-00-00 00:00:00','10.313999826958598','77.95532265625002','0000-00-00 00:00:00'),(4,'aaa','bbbbbaas','0000-00-00 00:00:00','10.573303386192986','76.50512734375002','0000-00-00 00:00:00'),(5,'hhh','kkkkl','0000-00-00 00:00:00','7.971272183946189','81.20727578125002','0000-00-00 00:00:00'),(6,'new notification','asdfasdfasdf','2018-04-03 13:59:00','11.253332246263437','75.78145980834961','2018-04-14 13:59:00');

/*Table structure for table `parkingarea` */

DROP TABLE IF EXISTS `parkingarea`;

CREATE TABLE `parkingarea` (
  `parking_id` int(11) NOT NULL AUTO_INCREMENT,
  `parking_name` varchar(20) DEFAULT NULL,
  `latitude` varchar(30) NOT NULL,
  `longitude` varchar(30) NOT NULL,
  `price` int(11) NOT NULL,
  `image` varchar(200) DEFAULT NULL,
  `user_id` varchar(20) DEFAULT NULL,
  `status` varchar(55) DEFAULT NULL,
  PRIMARY KEY (`parking_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

/*Data for the table `parkingarea` */

insert  into `parkingarea`(`parking_id`,`parking_name`,`latitude`,`longitude`,`price`,`image`,`user_id`,`status`) values (5,'Kozhikode ','11.2582121','75.7867469',666,'8.jpg','9','approoved'),(9,'Kozhikode ','11.2582121','75.7867469',666,'9.jpg','9','pending'),(10,'Kozhikode ','11.2582121','75.7867469',666,'10.jpg','9','approoved'),(11,'Aaaa','11.2582121','75.7867469',20,'11.','9','pending'),(12,'Aaaa','11.2582121','75.7867469',20,'12.','9','pending'),(13,'Bbbb','11.2582121','75.7867469',55,'13.','9','approoved'),(14,'Bbbb','11.2582121','75.7867469',55,'14.','9','pending'),(15,'Cccc','11.2582121','75.7867469',20,'15jpg','9','pending'),(16,'Cccc','11.2582121','75.7867469',20,'16.jpg','9','approoved'),(17,'Cccc','11.2582121','75.7867469',20,'17.jpg','9','approoved'),(18,'Cccc','11.2582121','75.7867469',20,'18.jpg','9','approoved'),(19,'New','11.2581769','75.7860203',100,'19.jpg','9','approoved'),(20,'good pqrking','11.25907753','75.78398513',20,'20.jpg','8','pending');

/*Table structure for table `poll_options` */

DROP TABLE IF EXISTS `poll_options`;

CREATE TABLE `poll_options` (
  `pollopt_no` int(11) NOT NULL AUTO_INCREMENT,
  `poll_id` int(11) NOT NULL,
  `lid` int(11) DEFAULT NULL,
  `value` varchar(30) NOT NULL,
  PRIMARY KEY (`pollopt_no`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `poll_options` */

insert  into `poll_options`(`pollopt_no`,`poll_id`,`lid`,`value`) values (1,3,9,'2'),(2,3,9,'2'),(3,1,9,'1'),(4,3,9,'2'),(5,3,9,'2'),(6,3,9,'2'),(7,4,8,'1'),(8,6,8,'1'),(9,6,6,'5'),(10,6,9,'4'),(11,8,9,'4');

/*Table structure for table `polls` */

DROP TABLE IF EXISTS `polls`;

CREATE TABLE `polls` (
  `poll_no` int(11) NOT NULL AUTO_INCREMENT,
  `poll_title` varchar(50) NOT NULL,
  `poll_description` varchar(500) DEFAULT NULL,
  `latitude` varchar(30) DEFAULT NULL,
  `longitude` varchar(30) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `validity` datetime DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `op1` varchar(55) DEFAULT NULL,
  `op2` varchar(55) DEFAULT NULL,
  `op3` varchar(55) DEFAULT NULL,
  `op4` varchar(55) DEFAULT NULL,
  PRIMARY KEY (`poll_no`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `polls` */

insert  into `polls`(`poll_no`,`poll_title`,`poll_description`,`latitude`,`longitude`,`timestamp`,`validity`,`priority`,`op1`,`op2`,`op3`,`op4`) values (1,'','','','','0000-00-00 00:00:00','0000-00-00 00:00:00',0,NULL,NULL,NULL,NULL),(2,'','fgvhbjnkm,l','44','444','0000-00-00 00:00:00','0000-00-00 00:00:00',45,NULL,NULL,NULL,NULL),(3,'$question','$description','$latitude','$longitude','0000-00-00 00:00:00','0000-00-00 00:00:00',0,'$op1','$op2',NULL,NULL),(4,'1+1','+1212','10.43219883904904','78.21899453125002','0000-00-00 00:00:00','0000-00-00 00:00:00',3,'1','2',NULL,NULL),(5,'asd','asdfasf','11.255436751119444','75.78472137451172','2018-04-05 13:59:00','2018-04-08 12:00:00',5,'123','321',NULL,NULL),(6,'1+2+3+4','answer fast','11.725172691937129','75.79899826965334','2018-04-09 00:12:00','2018-04-12 00:12:00',4,'5','6','4','10'),(7,'a+b','asd','12.725172691937129','75.53833046875002','2018-04-12 12:59:00','2018-04-12 00:59:00',5,'a','b','op3','op4'),(8,'r u intrested to ride','asdasd','11.255436751119444','75.78472137451172','2018-04-10 15:04:00','2018-04-13 14:03:00',4,'yes','no','op3','op4');

/*Table structure for table `slot` */

DROP TABLE IF EXISTS `slot`;

CREATE TABLE `slot` (
  `slot_id` int(11) NOT NULL AUTO_INCREMENT,
  `parking_id` int(11) DEFAULT NULL,
  `type` varchar(55) DEFAULT NULL,
  `status` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`slot_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*Data for the table `slot` */

insert  into `slot`(`slot_id`,`parking_id`,`type`,`status`) values (1,5,'4','pending'),(2,5,'2','pending'),(4,5,'2','pending'),(5,5,'2','pending'),(11,5,'2','pending'),(13,5,'4','9'),(14,5,'4','8'),(16,19,'4','pending'),(17,20,'2','pending'),(18,19,'4','pending');

/*Table structure for table `traffic_status` */

DROP TABLE IF EXISTS `traffic_status`;

CREATE TABLE `traffic_status` (
  `trf_id` int(11) NOT NULL AUTO_INCREMENT,
  `location_name` varchar(100) DEFAULT NULL,
  `timestamp` varchar(44) NOT NULL,
  `latitude` varchar(30) NOT NULL,
  `longitude` varchar(30) NOT NULL,
  `volume_status` int(11) NOT NULL,
  PRIMARY KEY (`trf_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `traffic_status` */

insert  into `traffic_status`(`trf_id`,`location_name`,`timestamp`,`latitude`,`longitude`,`volume_status`) values (1,'kozhikode','2018-04-04 16:24:11','11.12312','75.456456',6),(4,'kavil','2018-04-04 16:18:38','55','66',7),(5,'Pollachi Main Rd, Kinathukadavu, Tamil Nadu 642109, India','2018-04-04 16:18:27','10.830364838660937','77.03247109375002',7),(6,'kozhikode','2018-04-10 15:28:46','11.2881904','75.7535662',5),(7,'SK Temple Rd, Tazhekkod, Kozhikode, Kerala 673004, India','2018-04-10 15:35:17','11.2572993','75.7821884',6),(8,'Koothuparamba â€“ Chovva Bypass Rd, Naduvannur, Kerala 673614, India','2018-04-10 15:49:46','12.210266773241047','75.93383828125002',88),(9,'Koothuparamba â€“ Chovva Bypass Rd, Naduvannur, Kerala 673614, India','2018-04-10 15:49:47','12.210266773241047','75.93383828125002',8),(10,'Thozhupadam- Ottapalam Rd, Mayannur, Kerala 679105, India','2018-04-10 15:52:53','10.87554722010535','76.06567421875002',6),(11,'NCERC Admin Bldg Rd, Pambadi, Kerala 680588, India','2018-04-10 17:09:32','10.741815818981859','76.43438818566119',8),(12,'Kavunthara - Mannankavu Rd, Kavumthara, Kerala 673614, India','2018-04-10 17:11:17','11.501573261254446','75.75077567621258',5);

/*Table structure for table `trafic` */

DROP TABLE IF EXISTS `trafic`;

CREATE TABLE `trafic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL,
  `name` varchar(55) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `phone` varchar(55) DEFAULT NULL,
  `email` varchar(55) DEFAULT NULL,
  `lati` varchar(55) DEFAULT NULL,
  `longi` varchar(55) DEFAULT NULL,
  `uname` int(11) NOT NULL,
  PRIMARY KEY (`id`,`tid`,`uname`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `trafic` */

insert  into `trafic`(`id`,`tid`,`name`,`location`,`phone`,`email`,`lati`,`longi`,`uname`) values (2,10,'asd','8888','87988','sdfghj','99','77',0);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `login_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `address1` varchar(100) NOT NULL,
  `address2` varchar(100) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `pincode` varchar(6) NOT NULL,
  `phone` varchar(13) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `latitude` varchar(30) DEFAULT NULL,
  `longitude` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`user_id`,`login_id`,`name`,`address1`,`address2`,`city`,`state`,`pincode`,`phone`,`email`,`latitude`,`longitude`) values (4,7,'test1','adress1','address2','KANNUR','kerala','670621','07025003451','akhinaap@gmail.com','453','345345'),(5,0,'$nmae','$adress1','$adres2','$city','$state','$pin','$mob','$email','$lati','$longi'),(6,8,'febi','','','','','','','','',''),(7,9,'Amal','Kavil','Naduvannur','Kozhikode ','Kerala ','673614','9947477638','Amalkavil143@gmail.com','','');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
