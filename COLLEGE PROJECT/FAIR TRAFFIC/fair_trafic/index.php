<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>FairTraffic - Ease your life on roads</title>
  <meta name="description" content="Ease your life on roads from the nasty traffics, accidents, parking issues and much more. Don't let the city life screw your happiness!">
  <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">

<link rel="shortcut icon" href="img/logo.ico" />
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans|Candal|Alegreya+Sans">
  <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/imagehover.min.css">
  <link rel="stylesheet" type="text/css" href="css/style_login.css">
  <script type="text/javascript">
function valid()
{
	
		
		if(document.getElementById("loginid").value=="")
		{
			alert("Enter Username!");
			document.getElementById("loginid").focus();
			return false;
		}
		if(document.getElementById("loginpsw").value=="")
		{
			alert("Enter Password!");
			document.getElementById("loginpsw").focus();
			return false;
		}
	
	}
	</script>
  <?php
include("connection.php");
?>
</head>

<body>

<?php
if(isset($_POST["login_post"]))

{
  $username=$_POST["username"];
  $pass=$_POST["pass"];
  session_start();
  $qry=mysql_query("SELECT * FROM `login` WHERE `username`='$username' AND `password`='$pass'");
  if(mysql_num_rows($qry)>0)
  {
  $as=mysql_fetch_array($qry);
  $type=$as[3];
  $lid=$as[0];
  $_SESSION["type"]=$type;
  $_SESSION["lid"]=$lid;
  if($type=="police") {
    header("location:traffic_home.php");
  }
  else if($type=="admin")
  {
    
    header("location:admin_home.php");
    
  }
  else if($type=="user")
  {
    
   ?>
        <script>
    alert("Please use mobile App to login.");
    </script>
        <?php
    
  }
  }
  else
  {
    ?>
        <script>
    alert("Invalide username or password");
    </script>
        <?php
  }
}
?>
  <!--Navigation bar-->
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php">Fair<span>Traffic</span></a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="#feature">Features</a></li>
          
          <li class="btn-trial"><a href="#" data-target="#login" data-toggle="modal">Sign in</a></li>
          
        </ul>
      </div>
    </div>
  </nav>
  <!--/ Navigation bar-->
  <!--Modal box-->
  <div class="modal fade" id="login" role="dialog">
    <div class="modal-dialog modal-sm">

      <!-- Modal content no 1-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center form-title">Login</h4>
        </div>
        <div class="modal-body padtrbl">

          <div class="login-box-body">
            <p class="login-box-msg">Sign in to get started!</p>
            <div class="form-group">
              <form action="" name="" method="post" id="loginForm">

                <div class="form-group has-feedback">
                  <!----- username -------------->
                  <input class="form-control" placeholder="Username" id="loginid" type="text" autocomplete="on" name="username" />
                  <span style="display:none;font-weight:bold; position:absolute;color: red;position: absolute;padding:4px;font-size: 11px;background-color:rgba(128, 128, 128, 0.26);z-index: 17;  right: 27px; top: 5px;" id="span_loginid"></span>
                  <!---Alredy exists  ! -->
                  <span class="form-control-feedback"><i class="fa fa-user"></i></span>
                </div>
                <div class="form-group has-feedback">
                  <!----- password -------------->
                  <input class="form-control" placeholder="Password" id="loginpsw" type="password" autocomplete="off" name="pass" />
                  <span style="display:none;font-weight:bold; position:absolute;color: grey;position: absolute;padding:4px;font-size: 11px;background-color:rgba(128, 128, 128, 0.26);z-index: 17;  right: 27px; top: 5px;" id="span_loginpsw"></span>
                  <!---Alredy exists  ! -->
                  <span class="form-control-feedback"><i class="fa fa-lock"></i></span>
                </div>

                <div class="row">
                  <div class="col-xs-12">
                    <div class="checkbox icheck">
                      <label style="display:none">
                                <input  type="checkbox" id="loginrem" > Remember Me
                              </label>
                    </div>
                  </div>
                  <div class="col-xs-12">
                    <button type="submit" class="btn btn-green btn-block btn-flat" name="login_post" onclick="return valid()">Sign In </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
  <!--/ Modal box-->
  <!--Banner-->
  <div style="background: url(img/new2.jpg) no-repeat center top;background-size: " class="banner">
    <div class="bg-color">
      <div class="container">
        <div class="row">
          <div class="banner-text text-center">
            <div class="text-border">
              <h2 class="text-dec">FAIR TRAFFIC</h2>
            </div>
            <div class="intro-para text-center quote">
              <p class="big-text">All In One Traffic Application</p>
              <p class="small-text">Ease your life on roads from the nasty traffics, detect accidents, avoid parking issues and much more. <br/>Don't let the city life screw your day!</p>
              <a href="#" data-target="#login" data-toggle="modal" class="btn get-quote">Sign In Now !</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--/ Banner-->
  <!--Feature-->
  <section id="feature" class="section-padding">
    <div class="container">
      <div class="row">
        <div class="header-section text-center">
          <h2>Features</h2>
          <p>FairTraffic allows you to enjoy a bunch of features including Accident Detection,<br> Easy complaint submission, Instant Notification etc.</p>
          <hr class="bottom-line">
        </div>
        <div class="feature-info">
          <div class="fea">
            <div class="col-md-4">
              <div class="heading pull-right">
                <h4>Auto-Accident Detection</h4>
                <p>The accident is detected with the Accilerometer sensor on mobile devices and are instantly updated in the database.</p>
              </div>
              <div class="fea-img pull-left">
                <i class="fa fa-tachometer"></i>
              </div>
            </div>
          </div>
          <div class="fea">
            <div class="col-md-4">
              <div class="heading pull-right">
                <h4>Parking Management</h4>
                <p>Parking has never been so simple. You can easily convert your personal lands into parking spaces and even earn from them.</p>
              </div>
              <div class="fea-img pull-left">
                <i class="fa fa-car"></i>
              </div>
            </div>
          </div>
          <div class="fea">
            <div class="col-md-4">
              <div class="heading pull-right">
                <h4>Traffic Status</h4>
                <p>Get live updates on traffic status of different region and plan your day according to it. Don't waste your valuable time in traffic blocks.</p>
              </div>
              <div class="fea-img pull-left">
                <i class="fa fa-road"></i>
              </div>
            </div>
          </div>

           <div class="fea">
            <div class="col-md-4">
              <div class="heading pull-right">
                <h4>Easy Notification</h4>
                <p>Information is usefull only if we get it on right time. The instant Notification updates the user with informations from the Traffic Police.</p>
              </div>
              <div class="fea-img pull-left">
                <i class="fa fa-send "></i>
              </div>
            </div>
          </div>

           <div class="fea">
            <div class="col-md-4">
              <div class="heading pull-right">
                <h4>Polling</h4>
                <p>With polling the Traffic Police get more closer and connected to the people.</p>
              </div>
              <div class="fea-img pull-left">
                <i class="fa fa-thumbs-up"></i>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </section>
  <!--/ feature-->
  
  
  
 
<?php
include("f.php");
?>
 

</body>

</html>
