<?php

if($_SESSION["lid"]=="logout")
{
	?>
    <script>
	alert("please login ");
	window.location="index.php";
	</script>
    <?php
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Traffic - Home</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="" />
<meta name="author" content="http://bootstraptaste.com" />
<!-- css -->
<link rel="shortcut icon" href="img/logo.ico" />
<link href="css/bootstrap.min.css" rel="stylesheet" />
<link href="css/fancybox/jquery.fancybox.css" rel="stylesheet">
<link href="css/jcarousel.css" rel="stylesheet" />
<link href="css/flexslider.css" rel="stylesheet" />

<link href="css/style_login.css" rel="stylesheet" />
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
<style type="text/css">

.input-section td{text-align: left;}
.input-section td{padding: 10px;}
.input-section_whole {
    margin: 10px; background-color: #f0f0f0;
    padding: 30px;
    border-radius: 15px;
    box-shadow: 0 2px 4px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12)!important;
}
th,td{text-align: center;vertical-align:middle!important;}
.flex-active-slide{height: 300px;}
#wrapper{
    margin-top: 30px;
    padding: 20px;
}
#hero {

    width: 100%;
    height: 70vh;
    background: url(img/new2.jpg) top center;
    background-size: cover;
    position: relative;

}

    .hero-container{    position: absolute;

 background: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.6));
    background-color: ;
    bottom: 0;
    padding: 30px;
    left: 0;
    right: 0;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    text-align: center;}

    #hero h1 {
  margin: 30px 0 10px 0;
  font-size: 48px;
  font-weight: 700;
  line-height: 56px;
  text-transform: uppercase;
  color: #fff;
}

@media (max-width: 768px) {
  #hero h1 {
    font-size: 28px;
    line-height: 36px;
  }
}

#hero h2 {
  color: #eee;
  margin-bottom: 50px;
  font-size: 24px;
}
.flex-caption h3{ color: #eee;
     margin-bottom: 25px;
     font-size: 24px;}

.flex-caption p{ color: #eee;
     margin-bottom: 10px;
     font-size: 22px;}

@media (max-width: 768px) {
  #hero h2 {
    font-size: 18px;
    line-height: 24px;
    margin-bottom: 30px;
  }


}
</style>

</head>
 <script src="js/custom_login.js"></script>
<body>
 <!--Navigation bar-->
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="traffic_home.php">TRAFFIC<span> POLICE</span></a>

      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-right">
        
        <li><a href="view_accident.php">Accidents</a></li>
                        
          <li><a href="complaint_view.php">Complaints</a></li>

          <li class="dropdown">
                            <a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Manage Traffic<b class=" icon-angle-down"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="Trafic_status.php">Update Traffic Status</a></li>
                                <li><a href="trafic_add_status.php">Add New Status</a></li>                             
                            </ul>
                     </li>
          
          <li><a href="view_feedback.php">Feedbacks</a></li>
        
        <li class="dropdown">
                            <a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Others<b class=" icon-angle-down"></b></a>
                            <ul class="dropdown-menu">
                                
                                <li><a href="pushnotification.php">Post Notification</a></li>
                                <li><a href="view_notification.php">View Notification</a></li>
                                 <li><a href="postpolling.php">Post Poll</a></li>                                
                            </ul>
                        </li>

         
          <li class="btn-trial"><a href="logout.php">Sign Out</a></li>
          
        </ul>
      </div>
    </div>
  </nav>

  
  