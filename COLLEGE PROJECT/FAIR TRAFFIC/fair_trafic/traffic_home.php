<?php
session_start();
$type=$_SESSION["type"];
if($type=="admin")
{
	include("h1.php");
	
}
else if($type=="police")
{
	include("h2.php");
}
?>
<section id="hero">
    <div class="hero-container">
      
       <h1>Traffic - Home</h1>
      <h2>This is the home page for Traffic Users. You can move to different pages though the navigation buttons!</h2>
    </div>
 </section>

<div class="container" id="wrapper">
<h2>Home page</h2>



         <ul class="nav navbar-nav navbar-right" style="float: left!important;font-style: bold">
        
        <li><a href="view_accident.php" class="btn btn-warning">Accidents</a></li>
                        
          <li><a href="complaint_view.php" class="btn btn-warning">Complaints</a></li>

          <li class="dropdown">
                            <a href="#" class="dropdown-toggle btn btn-warning" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" >Manage Traffic<b class=" icon-angle-down"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="Trafic_status.php" class="btn btn-info">Update Traffic Status</a></li>
                                <li><a href="trafic_add_status.php" class="btn btn-info">Add New Status</a></li>                             
                            </ul>
                     </li>
          
          <li><a href="view_feedback.php" class="btn btn-warning">Feedbacks</a></li>
        
        <li class="dropdown">
                            <a href="#" class="dropdown-toggle btn btn-warning" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Others<b class=" icon-angle-down"></b></a>
                            <ul class="dropdown-menu">
                                
                                <li><a href="pushnotification.php" class="btn btn-info">Post Notification</a></li>
                                <li><a href="view_notification.php" class="btn btn-info">View Notification</a></li>
                                 <li><a href="postpolling.php" class="btn btn-info">Post Poll</a></li>                                
                            </ul>
                        </li>

         
          
          
        </ul>


</div>
<?php
include("f.php");

?>

</body>
</html>