<?php
session_start();
$type=$_SESSION["type"];
if($type=="admin")
{
	include("h1.php");
	
}
else if($type=="police")
{
	include("h2.php");
}
?>
<section id="hero">
    <div class="hero-container">
      
       <h1>Admin - Home</h1>
      <h2>This is the home page for ADMIN. You can move to different pages though the navigation buttons!</h2>
      
    </div>
 </section>

<div class="container" id="wrapper">
<h2>Home page <a href="<?php echo $_SERVER['REQUEST_URI']; ?>" class="btn btn-info" role="button">Refresh</a></h2>
</div>

<?php
include("f.php");
?>
</body>
</html>
