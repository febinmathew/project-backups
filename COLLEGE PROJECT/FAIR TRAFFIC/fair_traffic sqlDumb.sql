/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.6.24 : Database - fairtraffic
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`fairtraffic` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `fairtraffic`;

/*Table structure for table `accidents` */

DROP TABLE IF EXISTS `accidents`;

CREATE TABLE `accidents` (
  `accident_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(20) DEFAULT NULL,
  `sender_type` varchar(55) NOT NULL,
  `location_name` varchar(30) DEFAULT NULL,
  `timestamp` varchar(55) NOT NULL,
  `latitude` varchar(20) NOT NULL,
  `longitude` varchar(20) NOT NULL,
  `image` varchar(200) DEFAULT NULL,
  `accident_status` varchar(55) NOT NULL,
  PRIMARY KEY (`accident_id`)
) ENGINE=InnoDB AUTO_INCREMENT=190 DEFAULT CHARSET=latin1;

/*Data for the table `accidents` */

insert  into `accidents`(`accident_id`,`user_id`,`sender_type`,`location_name`,`timestamp`,`latitude`,`longitude`,`image`,`accident_status`) values (158,'83','other','','2018-04-14 12:39:54','','','158.jpg','checked'),(171,'83','user','673603','2018-04-14 13:58:06','11.3372373','76.0346578','img','checked'),(172,'83','user','Mukkam - Koodaranji Road','2018-04-14 13:58:22','11.3388091','76.0241405','img','checked'),(173,'83','user','Mukkam - Koodaranji Road','2018-04-14 13:58:36','11.3388091','76.0241405','img','pending'),(174,'83','user','Mukkam - Koodaranji Road','2018-04-14 13:58:59','11.33906411','76.0247652','img','pending'),(175,'83','user','Mukkam - Koodaranji Road','2018-04-14 13:59:11','11.3388091','76.0241405','img','pending'),(176,'83','user','Mukkam - Koodaranji Road','2018-04-14 19:07:11','11.3380996','76.0237778','img','pending'),(177,'83','user','Mukkam - Koodaranji Road','2018-04-14 19:08:36','11.3388091','76.0241405','img','pending'),(178,'83','user','Mukkam - Koodaranji Road','2018-04-16 22:14:51','11.3388091','76.0241405','img','pending'),(179,'83','user','Kalappurakkal','2018-04-16 22:27:56','11.3381572','76.0284229','img','pending'),(180,'83','user','673604','2018-04-16 22:28:32','11.3378634','76.0305475','img','pending'),(181,'83','user','Kalappurakkal','2018-04-16 22:33:26','11.3381572','76.0284229','img','pending'),(182,'83','user','Kalappurakkal','2018-04-16 22:35:38','11.3381572','76.0284229','img','pending'),(183,'83','user','Kalappurakkal','2018-04-16 22:37:14','11.3381572','76.0284229','img','pending'),(184,'83','user','Kalappurakkal','2018-04-16 22:39:36','11.3381572','76.0284229','img','pending'),(185,'86','user','','2018-04-17 09:23:54','11.26543438','75.77999871','img','pending'),(186,'86','user','','2018-04-17 09:26:42','','','img','pending'),(187,'87','user','','2018-04-17 09:29:48','11.26542234','75.77995661','img','pending'),(188,'89','user','PO SV colony','2018-04-17 09:48:01','11.26544602','75.78000618','img','pending'),(189,'83','user','6/221','2018-04-17 10:24:13','11.2671866','75.7794809','img','pending');

/*Table structure for table `complaint` */

DROP TABLE IF EXISTS `complaint`;

CREATE TABLE `complaint` (
  `comp_no` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(20) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `time_stamp` varchar(55) NOT NULL,
  `latitude` varchar(30) NOT NULL,
  `longitude` varchar(30) NOT NULL,
  `image_url` varchar(200) DEFAULT NULL,
  `complaint_status` varchar(55) NOT NULL,
  PRIMARY KEY (`comp_no`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

/*Data for the table `complaint` */

insert  into `complaint`(`comp_no`,`user_id`,`description`,`time_stamp`,`latitude`,`longitude`,`image_url`,`complaint_status`) values (32,'83','Gg','2018-04-14 12:05:26','11.3350077','76.0458995','1.jpg','checked'),(33,'83','Jjj','2018-04-14 12:16:17','11.3350077','76.0458995','33.png','checked'),(34,'83','Laptop','2018-04-14 12:30:23','','','34.png','checked'),(35,'83','Aaccc','2018-04-14 12:37:29','','','35.png','checked'),(36,'83','Sddfff','2018-04-14 19:09:40','11.3388091','76.0241405','36.png','pending'),(37,'86','Problm','2018-04-17 09:24:59','11.2654769','75.7798442','37.png','pending'),(38,'88','  ','2018-04-17 10:26:48','11.2648037','75.7802075','no data','pending');

/*Table structure for table `feedback` */

DROP TABLE IF EXISTS `feedback`;

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lid` int(11) DEFAULT NULL,
  `feedback` varchar(55) DEFAULT NULL,
  `rating` varchar(55) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `feedback` */

insert  into `feedback`(`id`,`lid`,`feedback`,`rating`) values (12,83,'I like it','5.0');

/*Table structure for table `login` */

DROP TABLE IF EXISTS `login`;

CREATE TABLE `login` (
  `loginid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `usertype` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`loginid`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=latin1;

/*Data for the table `login` */

insert  into `login`(`loginid`,`username`,`password`,`usertype`) values (6,'admin','admin','admin'),(82,'mukkam','mukkam','police'),(83,'fmfebinmathew@gmail.com','1234567','user'),(84,'fmfebinmathew@gmail.con','1234567','user'),(85,'nadakkavu','nadakkavu','police'),(86,'sreesree@gmail.com','sreesree','user'),(87,'sruthisruthi@gmail.com','sruthi123','user'),(88,'aswathisasi@gmail.com','aswathi123','user'),(89,'vyshagh123@gmail.com','1234567','user');

/*Table structure for table `notification` */

DROP TABLE IF EXISTS `notification`;

CREATE TABLE `notification` (
  `not_id` int(11) NOT NULL AUTO_INCREMENT,
  `not_title` varchar(30) NOT NULL,
  `not_msg` varchar(500) DEFAULT NULL,
  `timestamp` datetime NOT NULL,
  `latitude` varchar(30) NOT NULL,
  `longitude` varchar(30) NOT NULL,
  `validity` datetime NOT NULL,
  PRIMARY KEY (`not_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `notification` */

insert  into `notification`(`not_id`,`not_title`,`not_msg`,`timestamp`,`latitude`,`longitude`,`validity`) values (1,'Title','hi there','2018-04-01 01:00:00','11.240191411534825','75.834527915114','2018-05-01 01:00:00');

/*Table structure for table `parkingarea` */

DROP TABLE IF EXISTS `parkingarea`;

CREATE TABLE `parkingarea` (
  `parking_id` int(11) NOT NULL AUTO_INCREMENT,
  `parking_name` varchar(20) DEFAULT NULL,
  `latitude` varchar(30) NOT NULL,
  `longitude` varchar(30) NOT NULL,
  `price` int(11) NOT NULL,
  `image` varchar(200) DEFAULT NULL,
  `user_id` varchar(20) DEFAULT NULL,
  `status` varchar(55) DEFAULT NULL,
  PRIMARY KEY (`parking_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

/*Data for the table `parkingarea` */

insert  into `parkingarea`(`parking_id`,`parking_name`,`latitude`,`longitude`,`price`,`image`,`user_id`,`status`) values (25,'parking 2','11.3388091','76.0241405',50,'25.jpg','83','approoved'),(26,'kaithamattathil','11.338897','76.0241405',10,'26.jpg','84','approoved'),(27,'ccc','11.2668411','75.7794809',0,'27.jpg','83','approoved'),(28,'civil','11.2651015','75.7797796',20,'28.jpg','87','approoved');

/*Table structure for table `poll_options` */

DROP TABLE IF EXISTS `poll_options`;

CREATE TABLE `poll_options` (
  `pollopt_no` int(11) NOT NULL AUTO_INCREMENT,
  `poll_id` int(11) NOT NULL,
  `lid` int(11) DEFAULT NULL,
  `value` varchar(30) NOT NULL,
  PRIMARY KEY (`pollopt_no`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `poll_options` */

insert  into `poll_options`(`pollopt_no`,`poll_id`,`lid`,`value`) values (12,9,83,'1');

/*Table structure for table `polls` */

DROP TABLE IF EXISTS `polls`;

CREATE TABLE `polls` (
  `poll_no` int(11) NOT NULL AUTO_INCREMENT,
  `poll_title` varchar(50) NOT NULL,
  `poll_description` varchar(500) DEFAULT NULL,
  `latitude` varchar(30) DEFAULT NULL,
  `longitude` varchar(30) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `validity` datetime DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `op1` varchar(55) DEFAULT NULL,
  `op2` varchar(55) DEFAULT NULL,
  `op3` varchar(55) DEFAULT NULL,
  `op4` varchar(55) DEFAULT NULL,
  PRIMARY KEY (`poll_no`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `polls` */

insert  into `polls`(`poll_no`,`poll_title`,`poll_description`,`latitude`,`longitude`,`timestamp`,`validity`,`priority`,`op1`,`op2`,`op3`,`op4`) values (9,'Do you life this app?','Do you like it?','11.33729952080036','76.02245836330553','2018-02-04 01:00:00','2018-05-16 02:01:00',5,'Yes','No','Maybe','not Now');

/*Table structure for table `slot` */

DROP TABLE IF EXISTS `slot`;

CREATE TABLE `slot` (
  `slot_id` int(11) NOT NULL AUTO_INCREMENT,
  `parking_id` int(11) DEFAULT NULL,
  `type` varchar(55) DEFAULT NULL,
  `status` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`slot_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

/*Data for the table `slot` */

insert  into `slot`(`slot_id`,`parking_id`,`type`,`status`) values (30,25,'2','pending'),(31,25,'4','pending'),(32,26,'4','pending'),(34,26,'2','83'),(35,27,'4','87');

/*Table structure for table `traffic_status` */

DROP TABLE IF EXISTS `traffic_status`;

CREATE TABLE `traffic_status` (
  `trf_id` int(11) NOT NULL AUTO_INCREMENT,
  `location_name` varchar(100) DEFAULT NULL,
  `timestamp` varchar(44) NOT NULL,
  `latitude` varchar(30) NOT NULL,
  `longitude` varchar(30) NOT NULL,
  `volume_status` int(11) NOT NULL,
  PRIMARY KEY (`trf_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

/*Data for the table `traffic_status` */

insert  into `traffic_status`(`trf_id`,`location_name`,`timestamp`,`latitude`,`longitude`,`volume_status`) values (14,'mavoor road','2018-04-17 10:12:55','11.260390415507894','75.80772280677036',5),(15,'koodaranji','2018-04-14 12:52:09','11.339926832230548','76.02666117641957',4),(16,'manachira','2018-04-17 09:41:18','11.258806375923534','75.80761541262098',8),(17,'koduvally','2018-04-17 10:14:01','11.351700555577764','75.93703992321764',7);

/*Table structure for table `trafic` */

DROP TABLE IF EXISTS `trafic`;

CREATE TABLE `trafic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL,
  `name` varchar(55) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `phone` varchar(55) DEFAULT NULL,
  `email` varchar(55) DEFAULT NULL,
  `lati` varchar(55) DEFAULT NULL,
  `longi` varchar(55) DEFAULT NULL,
  `uname` int(11) NOT NULL,
  PRIMARY KEY (`id`,`tid`,`uname`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `trafic` */

insert  into `trafic`(`id`,`tid`,`name`,`location`,`phone`,`email`,`lati`,`longi`,`uname`) values (7,82,'Mukkam','Mukkam','9633323461','fmfebinmathew@gmail.com','11.320042811603802','75.99413986828483',0),(8,85,'Nadakkavu','Nadakkavu','919633323461','fmfebinmathew@gmail.com','11.268397174850902','75.77667270927589',0);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `login_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `address1` varchar(100) NOT NULL,
  `address2` varchar(100) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `pincode` varchar(6) NOT NULL,
  `phone` varchar(13) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `latitude` varchar(30) DEFAULT NULL,
  `longitude` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`user_id`,`login_id`,`name`,`address1`,`address2`,`city`,`state`,`pincode`,`phone`,`email`,`latitude`,`longitude`) values (8,83,'Febin Mathew','kaithamattathil','koodranji','kozhikode','kerala','673604','9633323461','fmfebinmathew@gmail.com','',''),(9,84,'febin2','','','','','','9633323461','fmfebinmathew@gmail.con','',''),(10,86,'Sree','','','','','','','sreesree@gmail.com','11.2617299','75.7809341'),(11,87,'sruthi','','','','','','','sruthisruthi@gmail.com','11.26541117','75.77994718'),(12,88,'aswathi','','','','','','','aswathisasi@gmail.com','11.2648037','75.7802075'),(13,89,'vyshagh','','','','','','','vyshagh123@gmail.com','11.26545163','75.78000789');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
