<!DOCTYPE html>

<html lang="en-US" ><head>
    <title>Drona 2k18 - Show Your Support</title>


    <meta name="viewport" content="width=device-width, initial-scale=1">

<?php 

$domain_name= $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
?>

    <meta property="og:url"           content="<?php echo $domain_name; ?>" />
  <meta property="og:type"          content="website" />
  <meta property="og:title"         content="Drona 2k18 - Image Frame for Support" />
  <meta property="og:description"   content="Show your support for Drona 2k18 by applying frames to your images. Let the world know you are one of them !" />
  <meta property="og:image"  itemprop="image" <?php echo 'content="',htmlspecialchars($domain_name, ENT_QUOTES, 'UTF-8'),'last_generated.png"' ?>  />

   
    <link href="css/style.css" rel="stylesheet">
    <link href="favicon.png" rel="icon" type="image/png" />
    <script src="js/jquery.js"></script>
    <script src="js/js.js">

</script>
<style type="text/css">

.btn-success {
    color: #fff;
    background-color: #5cb85c;
    border-color: #4cae4c;
}

  .btn-info {
    color: #fff;
    background-color: #5bc0de;
    border-color: #46b8da;
}

.btn-fb {
   margin-left: 10px;
    color: #fff;
    background-color: #5bc0de;
    border-color: #46b8da;
}


.btn {
    display: inline-block;
    padding: 8px 14px;
    margin-bottom: 0;
    font-size: 19px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
}

 

@media only screen and (max-width: 700px) {
    #content{ width: 100%;}
}

@media only screen and (min-width: 700px) {
    #content{width:600px;}

}

.loader {
     display:block;
    border: 7px solid #f3f3f3; /* Light grey */
    border-top: 7px solid #3498db; /* Blue */
    border-radius: 50%;
    width: 30px;
    height: 30px;
    animation: spin 0.3s linear infinite;
}

@keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
}

.design{margin:5px;}

</style>
  </head>
  <body style="background: url(src/drona_bg.png);background-repeat: repeat;">
  
    <div id="wrapper">
      <div id="content" style="box-shadow: 0 2px 4px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12)!important;background-color: #f1f1f1;">
      <div style="display:block">
         <img src="src/drona.png" height="100" />
         </div>
         <div style="box-shadow: 0 2px 4px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12)!important;width: 420px;display: inline-block;padding-top: 10px">
        <div id="preview" style="background:none;height: 375px;">
          
          <img src="last_generated.png" id="fg" data-design="0" style="box-shadow: 0 2px 4px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12)!important;"/>

        </div>
        <p id="recent_tag" style="font-style: italic;padding-top: 0px;margin-top: 0px">Most recently ganerated image 👏</p>
        </div>
        
        <?php 
         //echo $domain_name
         ?>
        <h3>👇 Choose an image</h3>
        
       <!--<input id="valueofframe" type="number" name="frame" hidden=true>    -->
        <input id="sortpicture" type="file" name="file" accept="image/*">
        
       

       <!-- <h2>OR</h2>
        <div class="fb-login-button" data-scope="public_profile,publish_actions" onlogin="checkLoginState();" data-max-rows="5" data-size="large" data-show-faces="false" data-auto-logout-link="false"></div>-->
        <div id="status"></div>
        <h3>👇 Choose the frame</h3>
        <div id="designs">
        <?php   

        //$dir =  $domain_name.'frames'.'/';

$fileList = glob('frames/*.png');
  
//Loop through the array that glob returned.
$count=0;
foreach($fileList as $filename){
   //Simply print them out onto the screen.
  echo  '<img class="design active" src="',htmlspecialchars($filename),'" data-design="0" onclick="frameclick(',$count,')">';
  $count++;
 //  echo $filename, '<br>'; 
}

        

         ?>
          <!--  <img class="design active" src="frames/frameonly.png" data-design="0" onclick="frameclick(1)">-->
        <!-- <img class="design" src="frames/frame-1.png" data-design="1"> -->

         
        </div>
        <br/><input id="public_checkbox" value="value1" type="checkbox" name="public" checked> Make it public?(Image will be shown as recent)<br/>
        <center style="margin-top: 10px"><div class="loader"></div></center>
		
		
		<p id="sharebutton">
          <a style="margin-top: 10px; font-size: 19px;
    font-weight: 400;" id="download_btn"><button class="btn-success btn">Download Image</button></a>
    <a style="margin-top: 10px; font-size: 19px;
    font-weight: 400;" id="share_btn" ><button class="btn-fb btn">Share</button></a>


         <!-- <button id="fb-set-pic" disabled>Set As <b>Facebook</b> Profile Picture</button> -->
        </p>
		
		
        <button id="upload" class="btn btn-info" style="margin-top: 25px">💪Generate</button>
        
            </div>
    </div>
</div></body></html>